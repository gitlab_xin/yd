<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetRender extends Model
{
    public static function build()
    {
        return new self();
    }

    public static function saveRender($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['render_id' => $id], true);
    }
}
