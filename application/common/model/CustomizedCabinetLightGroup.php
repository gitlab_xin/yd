<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetLightGroup extends Model
{
    protected $autoWriteTimestamp = true;

    public static function build()
    {
        return new self();
    }

    public static function saveGroup($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['light_group_id' => $id], true);
    }

}
