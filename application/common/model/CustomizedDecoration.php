<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedDecoration extends Model
{
    protected $table = "yd_customized_decoration";
    protected $pk = 'decoration_id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedDecoration
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList()
    {
        $result = self::build()->select()->toArray();
        if (!empty($result)) {
            $items = CustomizedDecorationDetail::build()->whereIn('decoration_id', array_column($result, 'decoration_id'))->select()->toArray();
            array_walk($result, function (&$v) use ($items) {
                $decoration_id = $v['decoration_id'];
                $v['detail'] = array_values(array_filter($items, function ($v) use ($decoration_id) {
                    return $v['decoration_id'] == $decoration_id;
                }));
                foreach ($v['detail'] as &$item) {
                    $item['position'] = $item['position'] != '' ? explode(',', $item['position']) : [];
                    $item['type'] = $v['type'];
                }
            });
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveDecoration($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['decoration_id' => $id], true);
    }
}
