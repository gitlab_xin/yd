<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 16:17
 */

namespace app\common\model;

use think\Model;

class ShopOrder extends Model
{
    protected $table = "yd_shop_order";
    protected $pk = 'order_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopOrder
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function logistics()
    {
        return $this->hasOne('shop_order_logistics','logistics_id','logistics_id',[],'LEFT');
    }
}
