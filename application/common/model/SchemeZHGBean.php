<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 */

namespace app\common\model;

class SchemeZHGBean extends SchemeBean
{
    public function mainInfo()
    {
        $result = [
            'search_index' => $this->getSearch_index(),
            'scheme_name' => $this->getScheme_name(),
            'scheme_pic' => $this->getScheme_pic() . '?' . getRandomInt(),

            'scheme_width' => $this->getScheme_width(),
            'scheme_height' => $this->getScheme_height(),
            'scheme_deep' => $this->getScheme_deep(),

            'border_color' => $this->getBorder_color(),
            'handle_color' => $this->getHandle_color(),
            'alloy_color' => $this->getAlloy_color(),
            'material_color' => $this->getMaterial_color(),
            'door_border_color' => $this->getDoor_border_color(),
            'door_alloy_color' => $this->getDoor_alloy_color(),
            'color_level' => $this->getColor_level(),
            'is_monochrome' => $this->getIs_monochrome(),

//            'm_left_mm' => $this->getM_left_mm(),
//            'm_top_mm' => $this->getM_top_mm(),
            'scheme_type' => $this->getScheme_type(),
            'scheme_b_type' => $this->getScheme_b_type(),
            'scheme_s_type' => $this->getScheme_s_type(),
            'scheme_color_no' => $this->getScheme_color_no(),
            'scheme_color_name' => $this->getScheme_color_name(),
            'scheme_error_range' => $this->getScheme_error_range(),
            'scheme_tv_size' => $this->getScheme_tv_size(),
            'scheme_tv_type' => $this->getScheme_tv_type(),

            'scheme_door_color_no' => $this->getScheme_door_color_no(),

            'scheme_schemes' => $this->getScheme_schemes_array(),
            'scheme_g_products' => $this->getScheme_g_products_array(),
            'scheme_wcb_products' => $this->getScheme_wcb_products_array(),
            'scheme_ncb_products' => $this->getScheme_ncb_products_array(),
        ];

        return $result;
    }

    public function getScheme_schemes_array($schemes = null)
    {
        $result = [];
        $schemeSchemes = $this->getScheme_schemes();
        foreach ($schemeSchemes as $schemeBean) {
            $schemeBean = change2SchemeBean($schemeBean);
            $result[] = [
                'scheme_products' => $schemeBean->getScheme_products_array(),
                'scheme_name' => $schemeBean->getScheme_name(),
                'scheme_no' => $schemeBean->getScheme_no(),
//                'scheme_color_no' => $schemeBean->getScheme_color_no(),
//                'scheme_color_name' => $schemeBean->getScheme_color_name(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_b_type' => $schemeBean->getScheme_b_type(),
                'scheme_s_type' => $schemeBean->getScheme_s_type(),
                'm_top_mm' => $schemeBean->getM_top_mm() === null ? 0 : $schemeBean->getM_top_mm(),
                'm_left_mm' => $schemeBean->getM_left_mm() === null ? 0 : $schemeBean->getM_left_mm(),
            ];
        }
        return $result;
    }

    public static function build($data)
    {
        $bean = new self();
        $bean->setScheme_no(isset($data['scheme_no']) && !empty($data['scheme_no']) ? $data['scheme_no'] : '');
        $bean->setScheme_name(isset($data['scheme_name']) && !empty($data['scheme_name']) ? $data['scheme_name'] : '');
        $bean->setScheme_type(isset($data['scheme_type']) && !empty($data['scheme_type']) ? $data['scheme_type'] : '');
        $bean->setScheme_pic(isset($data['scheme_pic']) && !empty($data['scheme_pic']) ? $data['scheme_pic'] : '');
        $bean->setScheme_width(isset($data['scheme_width']) && !empty($data['scheme_width']) ? $data['scheme_width'] : 0);
        $bean->setScheme_height(isset($data['scheme_height']) && !empty($data['scheme_height']) ? $data['scheme_height'] : 0);
        $bean->setScheme_hole_width(isset($data['scheme_hole_width']) && !empty($data['scheme_hole_width']) ? $data['scheme_hole_width'] : 0);
        $bean->setM_left_mm(isset($data['m_left_mm']) && !empty($data['m_left_mm']) ? $data['m_left_mm'] : 0);
        $bean->setM_top_mm(isset($data['m_top_mm']) && !empty($data['m_top_mm']) ? $data['m_top_mm'] : 0);
        $bean->setScheme_color_no(isset($data['scheme_color_no']) && !empty($data['scheme_color_no']) ? $data['scheme_color_no'] : '');
        $bean->setScheme_color_name(isset($data['scheme_color_name']) && !empty($data['scheme_color_name']) ? $data['scheme_color_name'] : '');
        $bean->setScheme_sk_color_no(isset($data['scheme_sk_color_no']) && !empty($data['scheme_sk_color_no']) ? $data['scheme_sk_color_no'] : '');
        $bean->setScheme_door_count(isset($data['scheme_door_count']) && !empty($data['scheme_door_count']) ? $data['scheme_door_count'] : 0);
        $bean->setScheme_door_color_no(isset($data['scheme_door_color_no']) ? $data['scheme_door_color_no'] : '');
        $bean->setScheme_door_direction(isset($data['scheme_door_direction']) ? $data['scheme_door_direction'] : '');
        $bean->setScheme_tv_size(isset($data['scheme_tv_size']) ? $data['scheme_tv_size'] : 52);
        $bean->setScheme_tv_type(isset($data['scheme_tv_type']) ? $data['scheme_tv_type'] : 'GS');
        // scheme_schemes
        if (isset($data['scheme_schemes']) && is_array($data['scheme_schemes'])) {
            $schemeSchemesList = [];

            foreach ($data['scheme_schemes'] as $item) {
                $colScheme = new self();
                $schemeProductsList = $schemedoorsList = [];
                foreach ($item['scheme_products'] as $productData) {
                    $schemeProductsList[] = ProductBean::build($productData);
                }
                if (isset($item['scheme_door_schemes'])) {
                    foreach ($item['scheme_door_schemes'] as $doorData) {
                        $schemedoorsList[] = self::build($doorData);
                    }
                }
                $colScheme->setScheme_products($schemeProductsList);
                $colScheme->setScheme_door_schemes($schemedoorsList);
                $colScheme->setScheme_name(isset($item['scheme_name']) ? $item['scheme_name'] : '');
                $colScheme->setScheme_no(isset($item['scheme_no']) ? $item['scheme_no'] : '');
//                $colScheme->setScheme_color_no(isset($item['scheme_color_no']) ? $item['scheme_color_no'] : '');
//                $colScheme->setScheme_color_name(CommonMethod::getSchemeColorName($colScheme->getScheme_color_no()));
                $colScheme->setScheme_b_type(isset($item['scheme_b_type']) ? $item['scheme_b_type'] : '');
                $colScheme->setM_top_mm(isset($item['m_top_mm']) ? $item['m_top_mm'] : '');
                $colScheme->setM_left_mm(isset($item['m_left_mm']) ? $item['m_left_mm'] : '');
                $colScheme->setScheme_width(isset($item['scheme_width']) ? $item['scheme_width'] : '');
                $colScheme->setScheme_height(isset($item['scheme_height']) ? $item['scheme_height'] : '');

                $schemeSchemesList[] = $colScheme;
            }

            $bean->setScheme_schemes($schemeSchemesList);
        }
        // wcb
        if (isset($data['scheme_wcb_products']) && is_array($data['scheme_wcb_products'])) {
            $wcbList = [];

            foreach ($data['scheme_wcb_products'] as $wcbData) {
                $wcbList[] = ProductBean::build($wcbData);
            }

            $bean->setScheme_wcb_products($wcbList);
        }
        // ncb
        if (isset($data['scheme_ncb_products']) && is_array($data['scheme_ncb_products'])) {
            $ncbList = [];

            foreach ($data['scheme_ncb_products'] as $ncbData) {
                $ncbList[] = ProductBean::build($ncbData);
            }

            $bean->setScheme_ncb_products($ncbList);
        }
        // g 盖板
        if (isset($data['scheme_g_products']) && is_array($data['scheme_g_products'])) {
            $gList = [];

            foreach ($data['scheme_g_products'] as $gData) {
                $gList[] = ProductBean::build($gData);
            }

            $bean->setScheme_g_products($gList);
        }

        return $bean;
    }
}
