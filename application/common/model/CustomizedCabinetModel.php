<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedCabinetModel extends Model
{
    protected $table = "yd_customized_cabinet_model";
    protected $pk = 'model_id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedCabinetModel
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveModel($data, $id = 0)
    {
        $obj_tem = explode('/', $data['objName']);
        $mtl_tem = explode('/', $data['mtlName']);
        $data['objName'] = array_pop($obj_tem);
        $data['mtlName'] = array_pop($mtl_tem);
        $data['pics'] = isset($data['pics']) ? implode(',', $data['pics']) : '';
        return $id == 0 ? self::create($data, true) : self::update($data, ['model_id' => $id], true);
    }

}
