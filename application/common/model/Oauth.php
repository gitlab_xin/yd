<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\model;

use think\Model;

class Oauth extends Model
{
    protected $table = "yd_user_oauth";
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return Oauth
     */
    public static function build()
    {
        return new self();
    }

    public static function oauthLogin($type, $oid, $username, $avatar, $union_id, $device_type)
    {
        if ($device_type == 'app') {
            $whereMap = [
                'oauth_type' => $type,
                'oauth_id' => $oid,
            ];
        } else {
            $whereMap = [
                'oauth_type' => $type,
                'pc_oauth_id' => $oid,
            ];
        }

        $oauthModel = self::where($whereMap)->find();

        if (empty($oauthModel) && !empty($union_id)) {
            $whereMap = [
                'oauth_type' => $type,
                'union_id' => $union_id,
            ];
            $oauthModel = self::where($whereMap)->find();
        }
        if (empty($oauthModel)) {
            $userId = User::addThirdPartyUser($username, $avatar);
            if (empty($userId)) {
                return false;
            }
            $oauthData = ['user_id' => $userId, 'oauth_type' => $type,'union_id'=>$union_id];
            $device_type == 'app' ? $oauthData['oauth_id'] = $oid : $oauthData['pc_oauth_id'] = $oid;
            $oauthModel = self::create($oauthData);
        } else {
            $device_type == 'app' ? $updateDate['oauth_id'] = $oid : $updateDate['pc_oauth_id'] = $oid;
            self::update($updateDate,$whereMap);
        }
        return $oauthModel['user_id'];
    }

    public static function checkOauthType($user_id)
    {
        $whereMap = [
            'user_id' => $user_id,
        ];

        $oauthModel = self::where($whereMap)->find();

        return empty($oauthModel) ? "" : $oauthModel['oauth_type'];
    }
}
