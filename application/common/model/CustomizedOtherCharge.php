<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/15
 * Time: 17:00
 */

namespace app\common\model;

use think\Model;

class CustomizedOtherCharge extends Model
{
    protected $table = "yd_customized_other_charge";
    protected $pk = 'id';
    protected $createTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return CustomizedOtherCharge
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList()
    {
        return self::select();
    }

    public static function getChargeByType($scheme_type)
    {
        return static::build()->field(['freight_charge', 'processing_charge', 'home_charge', 'service_charge'])
            ->where(['scheme_type' => $scheme_type])->find()->toArray();
    }
}
