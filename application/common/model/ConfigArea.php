<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/24
 * Time: 18:05
 */

namespace app\common\model;

use think\Model;

class ConfigArea extends Model
{
    protected $table = "yd_config_area";
    protected $pk = 'id';

    /**
     * @return ConfigArea
     */
    public static function build()
    {
        return new self();
    }
}
