<?php
namespace app\common\model;

use think\Model;

class MyAssemblyHouseType extends Model
{
    protected $table = "yd_my_assembly_house_type";

    public static function build()
    {
        return new self();
    }

    public function getDecorateCountAttr($value){
        return MyAssemblyHouseDecorate::build()
            ->where(['house_id' => $value,'is_deleted' => '0'])
            ->count();
    }

    public function getConfigNameAttr($value){
        //获取最后一个子分类id
        $config_id = MyAssemblyHouseConfig::build()
            ->where(['house_id' => $value])
            ->value('config_id');
        $config_name = '';
        self::getConfigName($config_id, $config_name);
        return $config_name;
    }

    public static function getConfigName($config_id, &$config_name){
        $info = MyAssemblyBuildingsConfig::build()
            ->where(['id' =>$config_id])
            ->field(['parent_id','name'])
            ->find();

        if (empty($info)){
            return $config_name;
        }

        $config_name = $info['name'] .' '. $config_name;

        //父
        if (!empty($info) && $info['parent_id'] != '0'){
            self::getConfigName($info['parent_id'], $config_name);
        }

        return $config_name;
    }

    public static function saveHouse($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('house_id', $id)->update($data);
        }
    }
}