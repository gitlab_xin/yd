<?php
namespace app\common\model;

use app\common\model\CustomizedScheme as SchemeModel;
use think\Config;
use think\Model;

class MyDecorateProduct extends Model
{
    protected $table = "yd_my_decorate_product";

    public static function build()
    {
        return new self();
    }


    public function getProductStandardInfoAttr($value, $data){
        $info = [];
        if ($data['type'] == '3'){
            $info['standard_name'] = '/';
            $info['color_name'] = '/';
        } else{
            $info = SchemeModel::build()
                ->alias('p')
                ->join('yd_customized_color c','c.color_no = p.scheme_color_no')
                ->field([
                   "CONCAT_WS('*',p.scheme_width,p.scheme_height,p.scheme_deep) as 'standard_name'",'c.color_name'
                ])
                ->order('c.is_default ASC,c.color_no ASC')
                ->where(['p.id' => $value, 'c.is_deleted' => 0,'c.is_show' => 1])
                ->find();
        }
        return $info;
    }

    public function getProductColorIdAttr($value, $data){
        return $value;
    }

    public function getRoomNameAttr($value, $data){
        return MyDecorateRoom::build()
            ->where(['id' => $value])
            ->value('name');
        return $value;
    }

    public function getSupplierNameAttr($value, $data){
        return ShopSupplier::build()
            ->where(['supplier_id' => $value])
            ->value('name');
        return $value;
    }

    public function getProductInfoAttr($value, $data){
        if ($data['type'] == '3'){
            $info = ShopProduct::build()
                ->alias('p')
                ->join('yd_shop_product_standard p_s','p_s.product_id = p.product_id')
                ->field([
                    'p.product_id','p.name','p_s.img'
                ])
                ->where(['p.product_id' => $value, 'p_s.is_deleted' => '0','p_s.status' => '1'])
                ->find();
            $info['standard_name'] = '/';
            $info['product_img'] = $info['img'].'?imageView2/0/w/50/h/50';
            $info['color_name'] = '/';
            $info['scheme_type'] = '';
            $info['scheme_b_type'] = '';
        } else {
            $info = SchemeModel::build()
                ->alias('p')
                ->join('yd_customized_color c','c.color_no = p.scheme_color_no')
                ->field([
                    'p.id product_id','p.scheme_name name','p.scheme_pic product_img',"CONCAT_WS('*',p.scheme_width,p.scheme_height,p.scheme_deep) as 'standard_name'",
                    'c.color_name', 'c.color_id', 'p.scheme_b_type', 'p.scheme_type',
                ])
                ->order('c.is_default ASC,c.color_no ASC')
                ->where(['p.id' => $value, 'c.is_deleted' => 0,'c.is_show' => 1])
                ->find();
        }

        return $info;
    }

    public function getStandardInfoAttr($value, $data){
        $info = [];
        if ($data['type'] == '2'){
            $list = MyDecorateProductStandard::build()
                ->where(['decorate_product_id' => $data['id']])
                ->field([
                    'id','color_id','product_img'
                ])
                ->select();

            foreach ($list as $k => $v){
                $temp = [
                    'id' => $v['id'],
                    'product_img' => $v['product_img'],
                ];
                $info[$v['color_id']] =  $temp;
            }
        }


        return $info;
    }

}