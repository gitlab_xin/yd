<?php
namespace app\common\model;

use think\Model;

class MyAssembleBuildingsLabel extends Model
{
    protected $table = "yd_my_assembly_buildings_label";

    public static function build()
    {
        return new self();
    }

    public function getAnswerAttr($value){
        return self::build()
            ->where(['is_deleted' => '0','parent_id' => $value])
            ->field(['id','name'])
            ->select();
    }
}