<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 17:59
 */

namespace app\common\model;

use think\Model;

class Withdraw extends Model
{
    protected $table = "yd_withdraw";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return Withdraw
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function admin()
    {
        return $this->hasOne('\app\admin\model\Admin','id','admin_id',[],'LEFT');
    }
}
