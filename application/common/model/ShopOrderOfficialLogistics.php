<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 10:09
 */

namespace app\common\model;

use think\Model;
use app\common\tools\JPush;

class ShopOrderOfficialLogistics extends Model
{
    protected $table = "yd_shop_order_official_logistics";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:极光推送
     */
    protected static function init()
    {
        self::afterInsert(function ($model) {
            if ($model->user->device_type != null && $model->user->push_id != null) {
                $alert = '您有新的物流动态，请滑动查看';
                $msg = ['protocol' => 'logistics://id=' . $model->logistics_id];
                $config = config('jpush');
                $push = new JPush($config['AppKey'], $config['MasterSecret']);
                $push->push($model->user->push_id, $model->user->device_type, $alert, $msg);
            }
            ShopOrderLogistics::update(['update_time' => time()], ['logistics_id' => $model->logistics_id]);
        });
    }

    /**
     * @return ShopOrderOfficialLogistics
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function user()
    {
        return $this->hasOne('user', 'user_id', 'user_id', [], 'LEFT');
    }
}
