<?php

namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class CustomizedCabinetLight extends Model
{
    protected $autoWriteTimestamp = true;

    public static function build()
    {
        return new self();
    }

    public static function saveLight($data)
    {
        Db::startTrans();
        $res = true;
        try {
            if (!(self::create($data, true))) {
                throw new Exception();
            }
            switch ($data['type']) {
                case 'AmbientLight':
                    $model = new CustomizedCabinetLightAmbinet();
                    break;
                case 'DirectionalLight':
                    $model = new CustomizedCabinetLightDirection();
                    break;
                case 'PointLight':
                    $model = new CustomizedCabinetLightPoint();
                    break;
                case 'HemisphereLight':
                    $model = new CustomizedCabinetLightHemisphere();
                    break;
                case 'SpotLight':
                    $model = new CustomizedCabinetLightSpot();
                    break;
                case 'RectAreaLight':
                    $model = new CustomizedCabinetLightRectArea();
                    break;
                default:
                    $model = '';
                    break;
            }
            $data['light_id'] = Db::getLastInsID();
            if (!$model->insert($data)) {
                throw new Exception();
            }
            Db::commit();
        } catch (\Exception $e) {
            $res = false;
            Db::rollback();
        }
        return $res;
    }

    public static function updateLight($data)
    {
        $model = self::get($data['id']);
        switch ($model['type']) {
            case 'AmbientLight':
                $model = CustomizedCabinetLightAmbinet::get($data['id']);
                break;
            case 'DirectionalLight':
                $model = CustomizedCabinetLightDirection::get($data['id']);
                break;
            case 'PointLight':
                $model = CustomizedCabinetLightPoint::get($data['id']);
                break;
            case 'HemisphereLight':
                $model = new CustomizedCabinetLightHemisphere();
                break;
            case 'SpotLight':
                $model = new CustomizedCabinetLightSpot();
                break;
            case 'RectAreaLight':
                $model = new CustomizedCabinetLightRectArea();
                break;
            default:
                $model = '';
                break;
        }
        unset($data['id']);
        return $model->save($data);
    }
}
