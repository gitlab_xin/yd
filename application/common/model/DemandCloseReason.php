<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:48
 */

namespace app\common\model;

use think\Model;

class DemandCloseReason extends Model
{
    protected $table = "yd_demand_close_reason";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return DemandCloseReason
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
