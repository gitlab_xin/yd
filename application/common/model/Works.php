<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 17:31
 */

namespace app\common\model;

use think\Model;

class Works extends Model
{
    protected $table = "yd_works";
    protected $pk = 'works_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return Works
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
