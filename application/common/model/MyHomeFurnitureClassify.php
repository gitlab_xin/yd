<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */

namespace app\common\model;

use think\Model;

class MyHomeFurnitureClassify extends Model
{
    protected $table = "yd_my_home_furniture_classify";
    protected $pk = 'classify_id';

    /**
     * @return MyHomeFurnitureClassify
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:新增或修改分类
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveClassify($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('classify_id', $id)->update($data);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:检查分类下是否存在商品
     * @param $id
     * @return bool
     */
    public static function checkFurnitureExist($id)
    {
        $count = MyHomeFurniture::build()
            ->where(function ($query) {
                $query->where(['is_deleted' => '0']);
            })
            ->where(function ($query) use ($id) {
                $query->whereOr(['classify_id' => $id]);
            })
            ->count();
        return $count > 0 ? true : false;

    }
}
