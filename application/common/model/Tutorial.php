<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:03
 */
namespace app\common\model;

use think\Model;

class Tutorial extends Model
{
    protected $table = "yd_tutorial";
    protected $pk = 'tutorial_id';

    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return Tutorial
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:保存或更新教程
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveTutorial($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['tutorial_id'=>$id],true);
    }
}
