<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\model;

use think\Model;

class Product extends Model
{
    protected $table = "yd_product";

    /**
     * @return Product
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
