<?php
namespace app\common\model;

use think\Model;

class MyDecorateProductStandard extends Model
{
    protected $table = "yd_my_decorate_product_standard";

    public static function build()
    {
        return new self();
    }

}