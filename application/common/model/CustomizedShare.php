<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedShare extends Model
{
    protected $table = "yd_customized_share";
    protected $pk = 'share_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return CustomizedShare
     */
    public static function build()
    {
        return new self();
    }

}
