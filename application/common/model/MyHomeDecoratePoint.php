<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */

namespace app\common\model;

use think\Model;

class MyHomeDecoratePoint extends Model
{
    protected $table = "yd_my_home_decorate_point";
    protected $pk = 'decorate_id';

    /**
     * @return MyHomeDecoratePoint
     */
    public static function build()
    {
        return new self();
    }

    public static function saveDecorate($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('decorate_id', $id)->update($data);
        }
    }
}
