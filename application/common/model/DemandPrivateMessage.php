<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Model;
use app\common\model\Demand as DemandModel;
use app\common\model\User as UserModel;
use app\common\tools\JPush;

class DemandPrivateMessage extends Model
{
    protected $table = "yd_demand_private_message";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:极光推送
     */
    protected static function init()
    {
        self::afterInsert(function ($model) {
            switch ($model->status) {
                case 'guest':
                    //承接方发送的,则需要获取承接方的推送信息
                    $user = DemandModel::build()->alias('d')->field(['u.username', 'u.push_id', 'u.device_type'])
                        ->join('user u', 'd.user_id = u.user_id', 'LEFT')
                        ->where(['demand_id' => $model->demand_id])->find();
                    break;
                case 'master':
                    //需求方发送的,则需要获取承接方的推送信息
                    $user = UserModel::get($model->user_id);
                    break;
                default:
                    break;
            }

            if (isset($user) && $user->device_type != null && $user->push_id != null) {
                $alert = '有人给你发来了私信';
                $msg = ['protocol' => 'custom://id=' . $model->demand_id];
                $config = config('jpush');
                $push = new JPush($config['AppKey'], $config['MasterSecret']);
                $push->push($user->push_id, $user->device_type, $alert, $msg);
            }
        });
    }

    /**
     * @return DemandPrivateMessage
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
