<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */

namespace app\common\model;

use think\Model;

class MyHomeHouseDecorate extends Model
{
    protected $table = "yd_my_home_house_decorate";

    /**
     * @return MyHomeHouseDecorate
     */
    public static function build()
    {
        return new self();
    }
}
