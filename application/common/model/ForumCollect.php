<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 17:16
 */

namespace app\common\model;

use think\Model;

class ForumCollect extends Model
{
    protected $table = "yd_forum_collect";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ForumCollect
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * 收藏验证
     * @author: Jun
     * @time: 2017年7月26日
     * description: 验证是否已经收藏
     * @param int $user_id 用户ID
     * @param int $article_id 帖子ID
     * @return array|false|\PDOStatement|string|Model
     */
    public static function isExists($user_id, $article_id)
    {
        return self::build()->where([
            'user_id' => $user_id,
            'article_id' => $article_id
        ])->find();
    }

    public static function collect($user_id, $article_id)
    {
        $data['user_id'] = $user_id;
        $data['article_id'] = $article_id;
        $bool = self::build()->where($data)->count();
        if (empty($bool)) {
            $data['create_time'] = time();
            return self::build()->insert($data);
        } else {
            return true;
        }
    }

    public static function cancel($user_id, $article_id)
    {
        $data['user_id'] = $user_id;
        $data['article_id'] = $article_id;
        return self::build()->where($data)->delete();
    }

    public static function check($user_id, $article_id)
    {
        $data['user_id'] = $user_id;
        $data['article_id'] = $article_id;
        return self::build()->where($data)->find();
    }
}
