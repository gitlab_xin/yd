<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedCabinetTexture extends Model
{
    protected $table = "yd_customized_cabinet_texture";
    protected $pk = 'texture_id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedCabinetTexture
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveTexture($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['texture_id' => $id], true);
    }

}
