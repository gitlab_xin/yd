<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class WebglScene extends Model
{
    protected $table = "yd_webgl_scene";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;


    /**
     * @return WebglScene
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function saveScene($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['id' => $id], true);
    }
}
