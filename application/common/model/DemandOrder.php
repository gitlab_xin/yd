<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Db;
use think\Model;

class DemandOrder extends Model
{
    protected $table = "yd_demand_order";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    public $redirectType = null;
    public $redirect = null;

    /**
     * @return DemandOrder
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
