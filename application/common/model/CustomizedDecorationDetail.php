<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedDecorationDetail extends Model
{
    protected $table = "yd_customized_decoration_detail";
    protected $pk = 'id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedDecorationDetail
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveDetail($data, $id = 0)
    {
        $data['position'] = isset($data['position']) ? implode(',', $data['position']) : '';
        return $id == 0 ? self::create($data, true) : self::update($data, ['id' => $id], true);
    }
}
