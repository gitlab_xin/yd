<?php

namespace app\common\model;

use think\Model;

class UserLabel extends Model
{
    protected $table = "yd_user_label";

    /**
     * User: zhaoxin
     * Date: 2020/2/13
     * Time: 2:47 下午
     * description
     * @return UserLabel
     */
    public static function build()
    {
        return new self();
    }
}