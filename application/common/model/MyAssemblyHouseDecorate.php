<?php
namespace app\common\model;

use app\common\model\CustomizedColor as ColorModel;
use think\Model;

class MyAssemblyHouseDecorate extends Model
{
    protected $table = "yd_my_assembly_house_decorate";

    public static function build()
    {
        return new self();
    }

    public function getConfigNameAttr($value){
        //获取最后一个子分类id
        $config_id = MyAssemblyHouseConfig::build()
            ->where(['house_id' => $value])
            ->value('config_id');
        $config_name = '';
        self::getConfigName($config_id, $config_name);
        return $config_name;
    }

    public static function getConfigName($config_id, &$config_name){
        $info = MyAssemblyBuildingsConfig::build()
            ->where(['id' =>$config_id])
            ->field(['parent_id','name'])
            ->find();
        if (empty($info)){
            return $config_name;
        }

        $config_name = $info['name'] .' '. $config_name;

        //父
        if ($info['parent_id'] != '0'){
            self::getConfigName($info['parent_id'], $config_name);
        }
    }

    public function getLabelTextAttr($value){
        if (empty($value)){
            return '';
        }
        $value = json_decode($value, true);
        $text = '';
        $array = MyAssembleBuildingsLabel::build()->column('name','id');
        foreach ($value as $k => $v){
            $text .= $array[$v].',';
        }

        return $text;
    }

    public function getLabelListAttr($value){
        if (empty($value)){
            return '';
        }
//        $value = json_decode($value, true);
//        $text = [];
//        $array = MyAssembleBuildingsLabel::build()->column('name','id');
        $array = explode(',',$value);

        foreach ($array as &$v){
            $v = trim($v);
        }

        return $array;
    }

    public static function addOrEdit($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('program_id', $id)->update($data);
        }
    }

    public static function getProductRoomListAttr($value){
        return MyDecorateRoom::build()
            ->where(['decorate_id' => $value, 'is_delete' => '0'])
            ->field([
                'id','name'
            ])
            ->select();
    }

    public static function getColorListAttr($value){
        return ColorModel::build()
            ->where(['is_deleted' => 0,'is_show' => 1])
            ->field(['color_id','origin_img_src','color_name'])
            ->order(['is_default' => 'DESC'])
            ->select();
    }

    public static function getMatchCountAttr($value, $data){
        return count(array_intersect(json_decode($data['label'], true), $value));
    }
}