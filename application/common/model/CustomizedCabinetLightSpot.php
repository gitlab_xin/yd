<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetLightSpot extends Model
{
    public static function build()
    {
        return new self();
    }

    public static function saveLight($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['light_id' => $id], true);
    }
}
