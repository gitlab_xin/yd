<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\model;

use think\Model;

class UserToken extends Model
{
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    public function getTokenInfo($token)
    {
        if (empty($token)) {
            return false;
        }

        $findData = $this
            ->where('token', $token)
            ->where('is_logout', 'no')
            ->where('expiry_time', '>', time())
            ->find();
        return $findData;
    }

    public static function disableToken($token)
    {
        $whereMap = [
            'token' => $token,
            'is_logout' => 'no',
            'expiry_time' => ['>', time()],
        ];
        self::update(['is_logout' => 'yes'], $whereMap);
    }

    public static function setPushId($token)
    {
        $whereMap = [
            'token' => $token,
        ];
        $token_info = self::where($whereMap)->find();
        if (!empty($token_info)) {
            $user_id = $token_info['user_id'];
            $data['push_id'] = "";
            $data['device_type'] = "";
            User::build()->update($data, ['user_id' => $user_id]);
        }
    }

    public function getExpiryTimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }

    /**
     * @return UserToken
     */
    public static function build()
    {
        return new self();
    }
}
