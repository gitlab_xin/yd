<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/17
 * Time: 10:18
 */

namespace app\common\model;

use think\Model;

class ForumSearchKey extends Model
{
    protected $table = "yd_forum_search_key";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ForumSearchKey
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
