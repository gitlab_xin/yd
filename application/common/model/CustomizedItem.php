<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedItem extends Model
{
    protected $table = "yd_customized_item";
    protected $pk = 'id';
    protected $createTime = false;
    protected $updateTime = false;

    /**
     * @return CustomizedItem
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList($classify_id)
    {
        $result = self::where(['classify_id' => $classify_id])->field(['classify_id'], true)->select();
        foreach ($result as &$v) {
            $v['position'] = $v['position'] != '' ? explode(',', $v['position']):[];
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveItem($data, $id = 0)
    {
        $data['position'] = isset($data['position']) ? implode(',', $data['position']) : '';
        return $id == 0 ? self::create($data, true) : self::update($data, ['id' => $id], true);
    }
}
