<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/23
 * Time: 17:22
 */

namespace app\common\model;

use think\Model;

class CooperativePartner extends Model
{
    protected $table = "yd_cooperative_partner";
    protected $pk = 'id';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return CooperativePartner
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList()
    {
        return self::select();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月23日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function savePartner($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['id'=>$id],true);
    }
}
