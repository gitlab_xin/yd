<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 16:17
 */

namespace app\common\model;

use think\Model;

class ShopOrderCharge extends Model
{
    protected $table = "yd_shop_order_charge";

    /**
     * @return ShopOrderCharge
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
