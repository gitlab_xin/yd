<?php
namespace app\common\model;

use think\Model;

class MyDecorateRoom extends Model
{
    protected $table = "yd_my_decorate_room";

    public static function build()
    {
        return new self();
    }

}