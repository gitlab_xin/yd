<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\model;

use Dankal\DkAlidayu\AlidayuSms;
use think\Model;

class Sms extends Model
{
    protected $pk = 'sms_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return Sms
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * 发送短信
     * @param String $mobile
     * @param String $code
     * @param String $smsType 短信类型,enum:'reg','pass' 'bind'
     * @return bool
     */
    public static function sendSms($mobile, $code, $smsType)
    {
        // 调用sdk发送短信
        $aliConfig = config('alidayu');
        $param = $aliConfig['smsType'][$smsType]['smsParam'];
        $param['code'] = $code;
        $sendSms = new AlidayuSms($aliConfig['AccessKey'], $aliConfig['SecretKey']);
        $sendSms->setRecNum($mobile);
        $sendSms->setSignName($aliConfig['smsType'][$smsType]['signName']);
        $sendSms->setTemplateCode($aliConfig['smsType'][$smsType]['templateCode']);
        $sendSms->setSmsParam(
            $param
        );
        $result = $sendSms->send();
        // 写入数据库
        self::create([
            'mobile' => $mobile,
            'sms_type' => $smsType,
            'sms_code' => $code,
            'send_result' => $result ? 'ok' : $sendSms->getErrorCode() . ":" . $sendSms->getErrorMessage(),
        ]);
        return $result;
    }

    /**
     * 验证短信验证码
     * @param $mobile
     * @param $code
     * @param $smsType
     * @return bool
     */
    public static function validateCode($mobile, $code, $smsType)
    {
        $smsModel = self::build()->where([
            'mobile' => $mobile,
            'sms_type' => $smsType,
            'is_effected' => '1',
        ])->order(['create_time' => 'desc'])->find();

        // 验证码不存在
        if (empty($smsModel)) {
            return false;
        }

        // 验证码已过期
        if (($smsModel['create_time'] + config('sys.sms_expiry_time') ) < time()) {
            self::build()->where([ 'sms_id' => $smsModel['sms_id'] ])->update([ 'is_effected' => '0' ]);
            return false;
        }

        // 验证码错误，
        if ($smsModel['sms_code'] != $code) {
            // 尝试次数+1, 尝试次数大于10次，销毁
            if ($smsModel['try_times'] >= 10) {
                self::build()->where([ 'sms_id' => $smsModel['sms_id'] ])->update([ 'is_effected' => '0' ]);
            } else {
                self::build()->where([ 'sms_id' => $smsModel['sms_id'] ])->inc('try_times')->update();
            }
            return false;
        }

        return true;
    }
}
