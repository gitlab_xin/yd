<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\model;

use think\Db;
use think\Model;

class User extends Model
{
    protected $table = "yd_user";
    protected $pk = 'user_id';
    protected $createTime = 'reg_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return User
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function userMiniField()
    {
        return [
            'user_id',
            'username',
            'avatar',
            'gender',
        ];
    }

    public static function usersInList(array $userIdList, array $userField = [])
    {
        $userField = $userField ?: self::userMiniField();
        $modelList = self::build()->field($userField)->select($userIdList);
        $resultMap = [];
        foreach ($modelList as $item) {
            $resultMap[$item['user_id']] = $item->toArray();
        }
        unset($item);
        return $resultMap;
    }

    /**
     * 注册用户
     * @param $mobile
     * @param $password
     * @return bool
     */
    public static function register($mobile, $password)
    {
        // 新建数据
        $userModel = self::create([
            'username' => config('sys.user_default_name'),
            'mobile' => $mobile,
            'password' => $password,
        ]);
        if (empty($userModel)) {
            return false;
        }
        return true;
    }

    /**
     * @author: Airon
     * @time: 2017年7月17日
     * description:第三方登录 添加用户
     * @param $username
     * @param $avatar
     * @return mixed
     */
    public static function addThirdPartyUser($username, $avatar)
    {
        $userData = [
            'username' => $username,
            'avatar' => $avatar,
        ];
        $userModel = self::create($userData);


        return $userModel['user_id'];
    }

    public static function bindUser($uid, $mobile, $password)
    {
        $updateData = [
            'mobile' => $mobile,
            'password' => $password,
        ];
        $res = self::where([ 'user_id' => $uid ])->update($updateData);
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:新增或修改用户
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveUser($data, $id = 0)
    {
        if(isset($data['password'])){
            $data['password'] = md5($data['password'].'dankal');
        }
        $data['mobile'] = $data['add_mobile'];

        return $id == 0?self::create($data,true):self::update($data,['user_id'=>$id],true);

    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:建立与黑名单的关联
     * @return \think\model\relation\HasOne
     */
    public function blackList()
    {
        return $this->hasOne('user_black_list','user_id','user_id');
    }
}
