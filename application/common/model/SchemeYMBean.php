<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 */

namespace app\common\model;

class SchemeYMBean extends SchemeBean
{
    public function mainInfo()
    {
        $result = [
            'scheme_name' => $this->getScheme_name(),
            'scheme_pic' => $this->getScheme_pic() . '?' . getRandomInt(),

            'scheme_width' => $this->getScheme_width(),
            'scheme_height' => $this->getScheme_height(),

            'scheme_hole_type' => $this->getScheme_hole_type(),
            'scheme_hole_width' => $this->getScheme_hole_width(),
            'scheme_hole_height' => $this->getScheme_hole_height(),
            'scheme_hole_deep' => $this->getScheme_hole_deep(),
            'scheme_hole_sl_height' => $this->getScheme_hole_sl_height(),

            'm_left_mm' => $this->getM_left_mm() === null ? 0 : $this->getM_left_mm(),
            'm_top_mm' => $this->getM_top_mm() === null ? 0 : $this->getM_top_mm(),

            'scheme_type' => $this->getScheme_type(),
//            'scheme_b_type' => $this->getScheme_b_type(),
//            'scheme_s_type' => $this->getScheme_s_type(),
            'scheme_color_no' => $this->getScheme_color_no(),
            'scheme_color_name' => $this->getScheme_color_name(),
            'scheme_sk_color_no' => $this->getScheme_sk_color_no(),
            'scheme_door_count' => $this->getScheme_door_count(),
            'scheme_door_height' => $this->getScheme_door_height(),
            'scheme_door_width_one' => $this->getScheme_door_width_one(),

            'scheme_door_color_no' => $this->getScheme_door_color_no(),
            'scheme_door_have_hcq' => $this->getScheme_door_haveHCQ(),

            'scheme_schemes' => $this->getScheme_schemes_array(),
            'scheme_sk_products' => $this->getScheme_sk_products_array(),
        ];

        return $result;
    }

    public function getScheme_schemes_array($schemes = null)
    {
        $result = [];
        $schemeSchemes = $this->getScheme_schemes();
        foreach ($schemeSchemes as $schemeBean) {
            $schemeBean = change2SchemeBean($schemeBean);
            $result[] = [
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
//                'scheme_color_no' => $schemeBean->getScheme_color_no(),
//                'scheme_name' => $schemeBean->getScheme_name(),
                'm_top_mm' => $schemeBean->getM_top_mm() === null ? 0 : $schemeBean->getM_top_mm(),
                'm_left_mm' => $schemeBean->getM_left_mm() === null ? 0 : $schemeBean->getM_left_mm(),
            ];
        }
        return $result;
    }

    public static function build($data)
    {
        $bean = new self();
        $bean->setScheme_name(isset($data['scheme_name']) && !empty($data['scheme_name']) ? $data['scheme_name'] : '');
        $bean->setScheme_type(isset($data['scheme_type']) && !empty($data['scheme_type']) ? $data['scheme_type'] : '');
        $bean->setScheme_pic(isset($data['scheme_pic']) && !empty($data['scheme_pic']) ? $data['scheme_pic'] : '');
        $bean->setScheme_width(isset($data['scheme_width']) && !empty($data['scheme_width']) ? $data['scheme_width'] : 0);
        $bean->setScheme_height(isset($data['scheme_height']) && !empty($data['scheme_height']) ? $data['scheme_height'] : 0);
        $bean->setScheme_hole_type(isset($data['scheme_hole_type']) && !empty($data['scheme_hole_type']) ? $data['scheme_hole_type'] : '');
        $bean->setScheme_hole_width(isset($data['scheme_hole_width']) && !empty($data['scheme_hole_width']) ? $data['scheme_hole_width'] : 0);
        $bean->setScheme_hole_height(isset($data['scheme_hole_height']) && !empty($data['scheme_hole_height']) ? $data['scheme_hole_height'] : 0);
        $bean->setScheme_hole_sl_height(isset($data['scheme_hole_sl_height']) && !empty($data['scheme_hole_sl_height']) ? $data['scheme_hole_sl_height'] : 0);
        $bean->setM_left_mm(isset($data['m_left_mm']) && !empty($data['m_left_mm']) ? $data['m_left_mm'] : 0);
        $bean->setM_top_mm(isset($data['m_top_mm']) && !empty($data['m_top_mm']) ? $data['m_top_mm'] : 0);
        $bean->setScheme_color_no(isset($data['scheme_color_no']) && !empty($data['scheme_color_no']) ? $data['scheme_color_no'] : '');
        $bean->setScheme_color_name(isset($data['scheme_color_name']) && !empty($data['scheme_color_name']) ? $data['scheme_color_name'] : '');
        $bean->setScheme_sk_color_no(isset($data['scheme_sk_color_no']) && !empty($data['scheme_sk_color_no']) ? $data['scheme_sk_color_no'] : '');
        $bean->setScheme_door_count(isset($data['scheme_door_count']) && !empty($data['scheme_door_count']) ? $data['scheme_door_count'] : 0);
        $bean->setScheme_door_height(isset($data['scheme_door_height']) && !empty($data['scheme_door_height']) ? $data['scheme_door_height'] : 0);
        $bean->setScheme_door_width_one(isset($data['scheme_door_width_one']) && !empty($data['scheme_door_width_one']) ? $data['scheme_door_width_one'] : 0);
        $bean->setScheme_door_color_no(isset($data['scheme_door_color_no']) ? $data['scheme_door_color_no'] : '');
        $bean->setScheme_door_haveHCQ(isset($data['scheme_door_have_hcq']) ? $data['scheme_door_have_hcq'] : '');

        // scheme_schemes
        $schemeSchemesList = [];
        if (isset($data['scheme_schemes']) && is_array($data['scheme_schemes'])) {
            foreach ($data['scheme_schemes'] as $item) {
                $colScheme = new self();
                $colScheme->setScheme_width(isset($item['scheme_width']) ? $item['scheme_width'] : '');
                $colScheme->setScheme_height(isset($item['scheme_height']) ? $item['scheme_height'] : '');
//                $colScheme->setScheme_color_no(isset($item['scheme_color_no']) ? $item['scheme_color_no'] : '');
//                $colScheme->setScheme_name(isset($item['scheme_name']) ? $item['scheme_name'] : '');
                $colScheme->setM_top_mm(isset($item['m_top_mm']) ? $item['m_top_mm'] : '');
                $colScheme->setM_left_mm(isset($item['m_left_mm']) ? $item['m_left_mm'] : '');
                $schemeSchemesList[] = $colScheme;
            }
        }
        $bean->setScheme_schemes($schemeSchemesList);

        // sk
        $skList = [];
        if (isset($data['scheme_sk_products']) && is_array($data['scheme_sk_products'])) {
            foreach ($data['scheme_sk_products'] as $skData) {
                $skList[] = ProductBean::build($skData);
            }
        }
        $bean->setScheme_sk_products($skList);

        return $bean;
    }
}
