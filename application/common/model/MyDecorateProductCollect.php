<?php
namespace app\common\model;

use think\Model;

class MyDecorateProductCollect extends Model
{
    protected $table = "yd_my_decorate_product_collect";

    public static function build()
    {
        return new self();
    }

}