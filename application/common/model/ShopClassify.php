<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 9:41
 */

namespace app\common\model;

use think\Model;
use think\Db;
use app\common\model\ShopProduct as ProductModel;

class ShopClassify extends Model
{
    protected $table = "yd_shop_classify";
    protected $pk = 'classify_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopClassify
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:获取商品分类一二三级
     * @param $ids
     * @param bool $line(默认连线返回)
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getClassifysForProduct($ids,$line = true)
    {
        if($line){
            $res = Db::name('shop_classify')->field(['GROUP_CONCAT(name SEPARATOR "--")'=>'line'])->where(['classify_id'=>['in',$ids]])->find();
        }else{
            $res = objToArray(self::build()->field(['img_src','create_time','parent_id'],true)->where(['classify_id'=>['in',$ids]])->select());
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:获取下拉单分类
     * @return array
     */
    public static function getSelectClassify()
    {
        $tmpArr = nodeTree(objToArray(self::all()), 0, 0, 'classify_id',2);
        $data = array();
        foreach ($tmpArr as $k => $v) {
            $name = $v['level'] == 0 ? '<b>' . $v['name'] . '</b>' : '├─' . $v['name'];
            $name = str_repeat("│        ", $v['level']) . $name;
            $data[$v['classify_id']] = $name;
        }
        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:新增或修改分类
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveClassify($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            return $userModel = self::where('classify_id', $id)->update($data);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:检查是否存在子分类
     * @param $id
     * @return bool
     */
    public static function checkChildExist($id)
    {
        return self::build()->where(['parent_id' => $id])->count() ? true : false;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:检查分类下是否存在商品
     * @param $id
     * @return bool
     */
    public static function checkProductsExist($id)
    {
        $count = ProductModel::build()
            ->where(function($query){
                $query->where(['is_deleted'=>'0']);
            })
            ->where(function($query) use($id){
                $query->whereOr(['classify_one_id'=>$id,'classify_two_id'=>$id,'classify_three_id'=>$id]);
            })
            ->count();
        return $count > 0? true : false;
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取一级分类列表
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selectLevelOne()
    {
        $filed = [
            'img_src',
            'parent_id',
            'create_time'
        ];
        return self::build()->where(['parent_id' => 0])->field($filed, true)->order('create_time')->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月18日
     * description:检查是否为一级分类
     * @param $classify_id
     * @return bool
     */
    public static function checkIsParent($classify_id)
    {
        $where = ['classify_id' => $classify_id, 'parent_id' => 0];
        return (self::build()->where($where)->find()) ? true : false;
    }
}