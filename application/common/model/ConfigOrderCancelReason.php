<?php
/**
 * Created by PhpStorm.
 * User: xiezhixian
 * Date: 2018/4/29
 * Time: 下午3:26
 */

namespace app\common\model;

use think\Model;

class ConfigOrderCancelReason extends Model
{
    protected $table = "yd_config_order_cancel_reason";
    protected $pk = 'id';

    /**
     * @return ConfigOrderCancelReason
     */
    public static function build()
    {
        return new self();
    }
}
