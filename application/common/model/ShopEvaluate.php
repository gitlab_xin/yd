<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/20
 * Time: 16:32
 */

namespace app\common\model;

use think\Model;

class ShopEvaluate extends Model
{
    protected $table = "yd_shop_evaluate";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopEvaluate
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:用于商品详情内 仅获取一条
     * @param $product_id
     * @param $user_id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function productInfo($product_id,$user_id = 0)
    {
        $field = [
            'e.user_id',
            'u.avatar',
            'u.username',
            'e.is_anonymous',
            'e.star',
            'ps.name as standard_name',
            'e.create_time as evaluate_time',
            'e.content',
            'e.img_src_list'
        ];
        $info = self::build()->alias('e')
            ->join('yd_user u', "u.user_id = e.user_id AND u.status = '1'")
            ->join('yd_shop_product_standard ps', 'ps.standard_id = e.standard_id')
            ->where(['e.product_id' => $product_id])
            ->field($field)
            ->order('e.create_time DESC')->find();//先看看有没有好评的

        if (empty($info)) {
            $info = self::build()->alias('e')
                ->join('yd_user u', 'u.user_id = e.user_id')
                ->join('yd_shop_product_standard ps', 'ps.standard_id = e.standard_id')
                ->where(['e.product_id' => $product_id])
                ->field($field)
                ->order('e.star DESC')
                ->order('e.create_time DESC')->find();//没有好评的只能拿差评的了
        }
        if (!empty($info) && $info['is_anonymous'] == 1 && $info['user_id'] != $user_id) {
            $info['username'] = config('config')['shop_anonymous']['username'];
            $info['avatar'] = config('config')['shop_anonymous']['avatar'];
        }
        $data['count'] = self::build()->where(['product_id' => $product_id])->count();
        $data['info'] = $info;
        return $data;
    }
}
