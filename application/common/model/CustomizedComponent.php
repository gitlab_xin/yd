<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/11/2
 * Time: 11:15
 */

namespace app\common\model;

use app\api\logic\ProductLogic;
use think\Model;

class CustomizedComponent extends Model
{
    protected $table = "yd_customized_component";
    protected $pk = 'component_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return CustomizedComponent
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList($where)
    {
        $model = static::build()->field(['type', 'component_id', 'create_time', 'update_time'], true);
        if (isset($where['scheme_type'])) {
            switch ($where['scheme_type']) {
                case 'RQS':
                case 'QW':
                    $model->where('min_height', 'elt', $where['height'])->where('max_height', 'egt', $where['height']);
                    unset($where['height']);
                    break;
                default:
                    break;
            }
        }
        $result = $model->where($where)->select()->toArray();
        if (!empty($result)) {
            foreach ($result as &$v) {
                $v['name'] = $where['type'] == 'combine' ? '组合' : '模块';
            }
            if ($where['type'] != 'combine') {
                unset($v);
                //交叉排序
                $arr1 = $arr2 = [];
                foreach ($result as $v) {
                    ($v['width'] == 784 || $v['width'] == 424) ? $arr1[] = $v : $arr2[] = $v;
                }
                $result = [];
                for ($i = 0, $size = count($arr1) > count($arr2) ? count($arr1) : count($arr2); $i < $size; $i++) {
                    if(isset($arr1[$i])){
                        array_push($result, $arr1[$i]);
                    }
                    if(isset($arr2[$i])){
                        array_push($result, $arr2[$i]);
                    }
                }
            }
        }

        return $result;
    }

    public function getProductsAttr($val, $data, $rel)
    {
        return json_decode($val);
    }

    public function setWidthRange(&$data)
    {
        $map = ['has_zz' => 0, 'has_com' => 0];
        foreach ($data['products'] as $product) {
            if ($product['product_type'] == 'ZZ') {
                $map['has_zz'] = 1;
            }
            if (!empty($product['com_products'])) {
                foreach ($product['com_products'] as $com_product) {
                    if ($com_product['product_type'] == 'L00' || $com_product['product_type'] == 'K') {
                        $map['has_com'] = 1;
                    }
                }
            }
        }
        if ($map['has_zz']) {
            $data['min_width'] = 816;
            $data['max_width'] = 1200;
        } else {
            if ($map['has_com']) {
                $data['min_width'] = 400;
                $data['max_width'] = 976;
            } else {
                $data['min_width'] = 400;
                $data['max_width'] = 1200;
            }
        }
        if ($data['scheme_type'] == 'BZ' || $data['type'] == 'combine') {
            $data['min_height'] = $data['max_height'] = $data['height'];
        }
        $data['products'] = json_encode($data['products']);
        return $this;
    }

    public static function setImgSrcOriginal(&$data,$id)
    {
        return self::update($data, ['component_id' => $id], true);
    }
}
