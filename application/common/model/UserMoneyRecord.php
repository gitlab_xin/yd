<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 17:59
 */

namespace app\common\model;

use think\Model;

class UserMoneyRecord extends Model
{
    protected $table = "yd_user_money_record";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return UserMoneyRecord
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
