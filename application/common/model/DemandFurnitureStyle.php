<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Db;
use think\Model;

class DemandFurnitureStyle extends Model
{
    protected $table = "yd_demand_furniture_style";
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    public $redirectType = null;
    public $redirect = null;

    /**
     * @return DemandFurnitureStyle
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getName($id)
    {
        $data = self::where(['furniture_style_id' => $id])->field(['name'])->find();
        return empty($data) ? false : $data['name'];
    }

    public static function getList()
    {
        $data = self::select()->toArray();
        return $data;
    }
}
