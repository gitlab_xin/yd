<?php
namespace app\common\model;

use think\Model;

class MyAssemblyBuildingsConfig extends Model
{
    public $display = array('1' => '显示', '2' => '不显示');
    protected $table = "yd_my_assembly_buildings_config";

    public static function build()
    {
        return new self();
    }

    public function getListAttr($value){
        $count = self::build()
            ->where(['parent_id' => $value])
            ->count();

        if ($count <= 0){
            return [];
        }

        $list = self::build()
            ->where(['parent_id' => $value])
            ->field([
                'id',
                'name',
                'id list'
            ])
            ->select();
        return  collection($list)->toArray();
    }

    public function getConfigNameAttr($value){
        //获取最后一个子分类id
//        $config_id = MyAssemblyHouseConfig::build()
//            ->where(['house_id' => $value])
//            ->value('config_id');
        $config_name = '';
        self::getConfigName($value, $config_name);
        return $config_name;
    }

    public static function getConfigName($config_id, &$config_name){
        $info = MyAssemblyBuildingsConfig::build()
            ->where(['id' =>$config_id])
            ->field(['parent_id','name'])
            ->find();

        if (empty($info)){
            return $config_name;
        }

        $config_name = $info['name'] .' '. $config_name;

        //父
        if (!empty($info) && $info['parent_id'] != '0'){
            self::getConfigName($info['parent_id'], $config_name);
        }
        return $config_name;

    }

    public static function saveMenu($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('id', $id)->update($data);
        }

    }
}