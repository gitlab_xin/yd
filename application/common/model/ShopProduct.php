<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 10:35
 */

namespace app\common\model;

use think\Config;
use think\Model;

class ShopProduct extends Model
{
    protected $table = "yd_shop_product";
    protected $pk = 'product_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopProduct
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:检查商品是否存在
     * @param int $product_id
     * @return bool
     */
    public function check($product_id)
    {
        $data = self::where(['product_id' => $product_id, 'is_deleted' => '0','is_sale'=>'1'])->find();
        return empty($data) ? false : true;
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:获取产品参数
     * @param $product_id
     * @return array|mixed
     */
    public static function getParam($product_id)
    {
        $data = self::where(['product_id' => $product_id, 'is_deleted' => '0'])
            ->value('product_param');
        if(!empty($data)){
            return unserialize($data);
        }
        return array();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:保存或编辑商品
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveProduct($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            return self::create($data, true);
        } else {
            //同时更新易家整配数据
            MyDecorateProduct::build()
                ->where(['product_id' => $id])
                ->update([
                    'name' => $data['name'],
                    'supplier_id' => $data['supplier_id'],
                    'intro' => $data['content'],
                    'update_time' => time(),
                ]);
            //修改
            return self::update($data, ['product_id' => $id], true);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:建立和商家的关系
     * @return \think\model\relation\HasOne
     */
    public function supplier()
    {
        return $this->hasOne('shop_supplier', 'supplier_id', 'supplier_id', [], 'LEFT');
    }

    public function getProductImgAttr($value){
//        $img = explode("|", $value);
//        $img_head = Config::get('qiniu.BucketDomain');
//
//        return $img_head.$img['0'].'?imageView2/0/w/50/h/50';

        $img_head = Config::get('qiniu.BucketDomain');

        return $img_head.$value;
    }
}
