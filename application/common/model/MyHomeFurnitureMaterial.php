<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */
namespace app\common\model;

use think\Model;

class MyHomeFurnitureMaterial extends Model
{
    protected $table = "yd_my_home_furniture_material";
    protected $pk = 'material_id';

    /**
     * @return MyHomeFurnitureMaterial
     */
    public static function build()
    {
        return new self();
    }
}
