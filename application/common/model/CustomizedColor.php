<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedColor extends Model
{
    protected $table = "yd_customized_color";
    protected $pk = 'color_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return CustomizedColor
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList()
    {
        return static::build()->field(['color_name', 'color_no', 'origin_img_src', 'img_src_row_light', 'img_src_row_dark'])
            ->where(['is_deleted' => 0])->select();
    }

    public static function getDoorList()
    {
        $result = static::build()
            ->field(['color_id as id', 'color_name as name', 'color_no', 'border_color', 'handle_color', 'alloy_color', 'material_color',
                'door_border_color', 'door_alloy_color', 'color_level', 'is_monochrome', 'origin_img_src', 'img_src_row_light', 'door_img_src', 'is_show','material'])
            ->where(['is_deleted' => 0, 'is_door' => 1])
            ->order('is_default ASC,color_no ASC')
            ->select()->toArray();
        if (!empty($result)) {
            array_walk($result, function (&$v) {
                $v['door_img_src'] != '' ? $v['door_img_src'] = explode(',', $v['door_img_src']) : [];
            });
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveColor($data, $id = 0)
    {
        $data['door_img_src'] = isset($data['door_img_src']) ? implode(',', $data['door_img_src']) : '';
        if ($data['border_color'] == '') {
            unset($data['border_color']);
        }
        if ($data['handle_color'] == '') {
            unset($data['handle_color']);
        }
        if ($data['alloy_color'] == '') {
            unset($data['alloy_color']);
        }
        if ($data['material_color'] == '') {
            unset($data['material_color']);
        }
        if ($data['door_border_color'] == '') {
            unset($data['door_border_color']);
        }
        if ($data['door_alloy_color'] == '') {
            unset($data['door_alloy_color']);
        }
        return $id == 0 ? self::create($data, true) : self::update($data, ['color_id' => $id], true);
    }

}
