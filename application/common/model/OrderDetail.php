<?php
namespace app\common\model;

use think\Model;

class OrderDetail extends Model
{
    protected $table = "yd_order_detail";

    public static function build()
    {
        return new self();
    }
}