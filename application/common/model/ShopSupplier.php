<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/18
 * Time: 9:54
 */

namespace app\common\model;

use think\Model;

class ShopSupplier extends Model
{
    protected $table = "yd_shop_supplier";
    protected $pk = 'supplier_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopSupplier
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:API 供应商详情
     * @param $supplier_id
     * @return array|bool|false|\PDOStatement|string|Model
     */
    public static function getInfo($supplier_id)
    {
        $info = self::build()
            ->where(['supplier_id' => $supplier_id,'is_deleted' => 0])
            ->field(['after_sale_service', 'create_time', 'update_time'], true)
            ->find();
        if (empty($info)) {
            return false;
        }
        // $info['after_sale_service'] = empty($info['after_sale_service']) ? array() : unserialize($info['after_sale_service']);
        $info['qualification'] = empty($info['qualification']) ? array() : explode('|', $info['qualification']);
        return $info;
    }
}
