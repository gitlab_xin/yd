<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/31
 * Time: 11:39
 */

namespace app\common\model;

use think\Model;

class ShopSupplierParams extends Model
{
    protected $table = "yd_shop_supplier_params";
    protected $pk = 'id';

    /**
     * @return ShopSupplierParams
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
