<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/20
 * Time: 10:46
 */

namespace app\common\model;

use think\Db;
use think\Model;

class ShopSearchKey extends Model
{
    protected $table = "yd_shop_search_key";
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopSearchKey
     */
    public static function build()
    {
        return new self();
    }
}