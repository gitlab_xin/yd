<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 10:40
 */

namespace app\common\model;

use think\Model;

class ShopSupplierBadRecord extends Model
{
    protected $table = "yd_shop_supplier_bad_record";
    protected $pk = 'br_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopSupplierBadRecord
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:保存或更新黑历史
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveRecord($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['br_id'=>$id],true);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:关联shopsupplier表
     * @return \think\model\relation\HasOne
     */
    public function supplier()
    {
        //hasOne(‘关联模型名’,‘外键名’,‘主键名’,[‘模型别名定义’],‘join类型’);
        return $this->hasOne('ShopSupplier','supplier_id','supplier_id',[],'LEFT')
            ->field('supplier_id,name');
    }

    public static function getList($supplierId,$page,$size){
        return self::build()
            ->where(['supplier_id'=>$supplierId])
            ->field(['br_id','title','intro','create_time'])
            ->page($page,$size)
            ->select();
    }

    public static function webGetList($supplierId,$page,$size){
        return self::build()
            ->where(['supplier_id'=>$supplierId])
            ->field(['update_time'],true)
            ->page($page,$size)
            ->select();
    }

    public static function getInfo($br_id){
        return self::build()
            ->where(['br_id'=>$br_id])
            ->field(['update_time'],true)
            ->find();
    }
}