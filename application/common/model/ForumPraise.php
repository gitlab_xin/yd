<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 17:16
 */

namespace app\common\model;

use think\Model;

class ForumPraise extends Model
{
    protected $table = "yd_forum_praise";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ForumPraise
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * 点赞验证
     * @author: Jun
     * @time: 2017年7月26日
     * description: 验证是否已经点赞
     * @param int $user_id 用户ID
     * @param int $article_id 帖子ID
     * @return array|false|\PDOStatement|string|Model
     */
    public static function isExists($user_id, $article_id)
    {
        return self::build()->where([
            'user_id' => $user_id,
            'article_id' => $article_id
        ])->find();
    }

    /**
     * 点赞
     * @author: Jun
     * @time: 2017年7月26日
     * description: 点赞
     * @param array $data 需要插入到数据库的信息
     * @return int|string
     */
    public static function addPraise($data)
    {
        ForumArticle::build()->where(['article_id'=>$data['article_id']])->setInc('praise_count');
        return self::build()->insert($data);
    }

    /**
     * 取消点赞
     * @author: Jun
     * @time: 2017年7月26日
     * description: 取消点赞
     * @param int $user_id 用户ID
     * @param int $article_id 帖子ID
     * @return int
     */
    public static function delPraise($user_id, $article_id)
    {
        ForumArticle::build()->where(['article_id'=>$article_id])->setDec('praise_count');
        return self::build()->where([
            'user_id' => $user_id,
            'article_id' => $article_id
        ])->delete();
    }
}
