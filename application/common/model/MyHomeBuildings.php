<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * 我家-楼盘
 */

namespace app\common\model;

use think\Model;

class MyHomeBuildings extends Model
{
    protected $table = "yd_my_home_buildings";

    /**
     * @return MyHomeBuildings
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:新增或修改
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function addOrEdit($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('buildings_id', $id)->update($data);
        }
    }
}
