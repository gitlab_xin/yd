<?php
// 七牛model

namespace app\common\model;

use Qiniu\Storage\BucketManager;
use think\Model;
use Qiniu\Auth;
use think\Config;

class Qiniu extends Model
{
    private $_AccessKey;
    private $_SecretKey;
    private $_BucketDomain;
    private $_BucketName;

    public function __construct($data = [])
    {
        parent::__construct($data);

        $this->_AccessKey = Config::get('qiniu.AccessKey');
        $this->_SecretKey = Config::get('qiniu.SecretKey');
        $this->_BucketDomain = Config::get('qiniu.BucketDomain');
        $this->_BucketName = Config::get('qiniu.BucketName');
    }


    public function getToken($key = null)
    {
        // 构建鉴权对象
        $auth = $this->getAuth();
        // 生成上传 Token
        $token = $auth->uploadToken($this->getBucketName(), $key);

        return $token;
    }

    public function getBucketDomain()
    {
        return $this->_BucketDomain;
    }

    public function getBucketName()
    {
        return $this->_BucketName;
    }

    public function getAuth()
    {
        return new Auth($this->_AccessKey, $this->_SecretKey);
    }

    public function delPic($key)
    {
        $auth = $this->getAuth();

        //初始化BucketManager
        $bucketMgr = new BucketManager($auth);

        //你要测试的空间， 并且这个key在你空间中存在
        $bucket = $this->getBucketName();

        //删除$bucket 中的文件 $key
        $err = $bucketMgr->delete($bucket, $key);
        if ($err !== null) {
            return $err;
        } else {
            return true;
        }
    }

    public function uploadUrl($url, $key = "")
    {
        $bucketManager = new BucketManager($this->getAuth());
        list($ret, $err) = $bucketManager->fetch($url, $this->_BucketName, $key);
        if ($err === null) {
            return $ret['key'];
        } else {
            return false;
        }
    }
}