<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class UserInviteCode extends Model
{
    protected $table = "yd_user_invite_code";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return UserInviteCode
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:添加或修改banner
     * @param $postData
     * @param int $id
     * @return $this
     */
    public static function saveCode($postData,$id = 0)
    {
        return $id == 0?self::create($postData,true):self::update($postData,['banner_id'=>$id],true);
    }
}
