<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/21
 * Time: 17:37
 */

namespace app\common\model;

use think\Model;

class Feedback extends Model
{
    protected $table = "yd_feedback";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return Feedback
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function user()
    {
        return $this->hasOne('user','user_id','user_id',[],'left');
    }
}
