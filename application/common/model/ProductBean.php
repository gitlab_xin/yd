<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\model;

use app\common\tools\CommonMethod;

class ProductBean
{
    public function __get($name)
    {
        return $this->$name;
    }

    public function toArray()
    {
        $reflect = new \ReflectionClass($this);
        $pros = $reflect->getProperties();

        $res = [];
        foreach ($pros as $ref) {
            $key = $ref->getName();
            $value = $this->__get($key);
            if (!is_array($value)) {
                $res[$key] = $value;
            } else {
                $res2 = [];
                foreach ($value as $obj) {
                    $res2[] = $obj->toArray();
                }
                $res[$ref->getName()] = $res2;
            }
        }
        return $res;
    }

    public function mainInfo()
    {
        $info = [
            'product_name' => $this->getProduct_name(),
            'product_type' => $this->getProduct_type(),
//            'r_left' => $this->getR_left(),
//            'r_top' => $this->getR_top(),
//            'm_left' => $this->getM_left(),
//            'm_top' => $this->getM_top(),
            'm_left_mm' => $this->getM_left_mm(),
            'm_top_mm' => $this->getM_top_mm(),
            'deep' => $this->getProduct_deep(),
            'height' => $this->getProduct_height(),
            'width' => $this->getProduct_width(),
//            's_height' => $this->getProduct_s_height(),
//            's_width' => $this->getProduct_s_width(),
            's_height_mm' => $this->getProduct_s_height_mm(),
            's_width_mm' => $this->getProduct_s_width_mm(),
            'color_no' => $this->getProduct_color_no(),
            'product_no' => $this->getProduct_no(),
            'product_pic' => '',//暂不删除该字段
            'product_direction' => $this->getProduct_direction(),
            'product_move_type' => $this->getProduct_move_type(),
            'z_axis' => $this->getZ_axis(),
            'can_delete' => $this->getCan_delete(),
            'com_products' => $this->getCom_products_array(),
            'acc_products' => $this->getAcc_products_array()
        ];
        return $info;
    }

    public function scoreInfo()
    {
        $productContent = [];
        if($this->getProductContent() != ''){
            foreach ($this->getProductContent() as $items) {
                $items = change2ProductBean($items);
                $productContent[] = $items->productCotentInfo();
            }
        }
        $info = [
            'product_no' => $this->getProduct_no(),
            'product_name' => $this->getProduct_name(),
            'product_height' => $this->getProduct_height(),
            'product_width' => $this->getProduct_width(),
            'product_deep' => $this->getProduct_deep(),
            'product_count' => $this->getProduct_count(),
            'product_area' => $this->getProduct_area(),
            'product_unit' => $this->getProduct_unit(),
            'product_spec' => $this->getProduct_spec(),
            'product_price' => $this->getProduct_price(),
            'product_dis_price' => $this->getProduct_dis_price(),
            'product_total_price' => $this->getProduct_total_price(),
            'product_dis_total_price' => $this->getProduct_dis_total_price(),
            'color_no' => $this->getProduct_color_no(),
            'color_name' => (in_array($this->getProduct_name(),['裤抽','拉篮','综合柜铝合金门'])) ? '/' :  CommonMethod::getSchemeColorName($this->getProduct_color_no()),
            'product_content' => $productContent
        ];
        return $info;
    }

    public function productCotentInfo()
    {
        $info = [
            'product_no' => $this->getProduct_no(),
            'product_name' => $this->getProduct_name(),
            'product_height' => $this->getProduct_height(),
            'product_width' => $this->getProduct_width(),
            'product_deep' => $this->getProduct_deep(),
            'product_count' => $this->getProduct_count(),
            'product_area' => $this->getProduct_area(),
            'product_unit' => $this->getProduct_unit(),
            'product_spec' => $this->getProduct_spec(),
            'product_price' => $this->getProduct_price(),
            'product_dis_price' => $this->getProduct_dis_price(),
            'product_total_price' => $this->getProduct_total_price(),
            'product_dis_total_price' => $this->getProduct_dis_total_price(),
            'color_no' => $this->getProduct_color_no(),
            'color_name' => CommonMethod::getSchemeColorName($this->getProduct_color_no()),
        ];
        return $info;
    }

    public static function build($data)
    {
        $bean = new self();
        $bean->setProduct_name($data['product_name']);
        $bean->setProduct_type($data['product_type']);
//        $bean->setR_left($data['r_left']);
//        $bean->setR_top($data['r_top']);
//        $bean->setM_left($data['m_left']);
//        $bean->setM_top($data['m_top']);
        $bean->setM_left_mm($data['m_left_mm']);
        $bean->setM_top_mm($data['m_top_mm']);
        $bean->setProduct_deep($data['deep']);
        $bean->setProduct_height($data['height']);
        $bean->setProduct_width($data['width']);
//        $bean->setProduct_s_height($data['s_height']);
//        $bean->setProduct_s_width($data['s_width']);
        $bean->setProduct_s_height_mm($data['s_height_mm']);
        $bean->setProduct_s_width_mm($data['s_width_mm']);
        $bean->setProduct_color_no($data['color_no']);
        $bean->setProduct_no($data['product_no']);
        $bean->setProduct_pic($data['product_pic']);
        $bean->setProduct_direction(isset($data['product_direction']) ? $data['product_direction'] : '');
        $bean->setProduct_move_type(isset($data['product_move_type']) ? $data['product_move_type'] : 3);
        $bean->setZ_axis(isset($data['z_axis']) ? $data['z_axis'] : 0);
        $bean->setCan_delete(isset($data['can_delete']) ? $data['can_delete'] : 1);
        $comProducts = [];
        if (!empty($data['com_products'])) {
            foreach ($data['com_products'] as $item) {
                $comProducts[] = self::build($item);
            }
        }
        $bean->setCom_products($comProducts);
        return $bean;
    }

    public function getCom_products_array()
    {
        $result = [];
        $products = $this->getCom_products();
        if (empty($products)) {
            return [];
        }
        foreach ($products as $productBean) {
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getAcc_products_array()
    {
        $result = [];
        $products = $this->getAcc_products();
        if (empty($products)) {
            return [];
        }
        foreach ($products as $productBean) {
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    /**
     * 排序属性
     */
    public $seq = 0;

    /**
     * 排序类型
     * 100侧板(竖向板)、200层板(横向板)、300底担、400背板、500抽屉(左510、右520、底530、后540)、
     * 550拉篮、560 裤抽、600 桌子(垂直610)、700-800-900门板、1000视听柜
     */
    public $seq_type = 0;

    // 产品编码
    private $product_no = "";

    // 产品索引值
    private $product_index = "";

    // 产品Map Key
    private $product_key = "";

    // 产品名称
    private $product_name = "";
    // 花色编码
    private $product_color_no = "";
    // 花色名称
    private $product_color_name = "";
    // 饰面花色名称
    private $product_sm_color_name = "";
    // 饰面花色代码
    private $product_sm_color_no = "";
    // 饰面数量
    private $product_sm_count = 0;
    // 产品类别
    private $product_category = "";
    // 宽度
    private $product_width = 0;
    // 高度
    private $product_height = 0;
    // 厚度
    private $product_deep = 0;
    // 产品数量
    private $product_count = 0;
    // 单位
    private $product_unit = "";
    // 规格
    private $product_spec = "";
    // 0不能移动 1上下 2左右 3随便移动
    private $product_move_type = 3;
    // 1可以删 0不可删
    private $can_delete = 1;
    // 能放东西吗 0都不可以 1上下都可以 2上面可以 3下面可以
    private $product_put_type = 0;
    // 上面是否有东西
    private $product_up_have = 0;
    // 下面是否有东西
    private $product_down_have = 0;
    // 是否移动
    private $product_is_move = 0;
    // 显示宽度
    private $product_s_width = 0;
    // 显示高度
    private $product_s_height = 0;
    // 门把手方向
    private $product_direction = 0;
    // 产品图片
    private $product_pic = "";
    // 产品图片1
    private $product_pic1 = "";
    // 产品图片列表
    private $product_pic_list;
    // 产品类型
    private $product_type = "";
    // 饰品类别 0挂件 1摆件
    private $product_accType = 0;
    // 产品是否有衣通杆 0无1是
    private $product_is_ytg = 0;
    // 是否是门 0无1是
    private $product_is_door = 0;
    // 面积
    private $product_area = 0;
    // 单价
    private $product_price = 0;
    // 折扣价
    private $product_dis_price = 0;
    // 折扣
    private $product_discount = 0.68;
    // 总价
    private $product_total_price = 0;
    // 折后总价
    private $product_dis_total_price = 0;
    // 绑定在此产品上的饰品列表
    private $acc_products;
    // 绑定在此产品上的饰品Map
    private $acc_products_map;
    // 与此产品组合关联的产品列表
    private $com_products;
    // 与此产品组合关联的产品Map
    private $com_products_map;
    // 组件生成产品列表
    private $f_products;
    // 饰品生成产品列表
    private $a_products;
    // 是否被统计 0无1已统计
    private $product_is_score = 0;
    // 产品所在的方案编号
    private $scheme_no = "";
    // 产品所在的组件明细
    private $product_content = "";


    // 订单保存相关
    private $order_no = "";
    private $scheme_index = "";
    private $scheme_col_index = 0;

    /*
     * 定位相关
     */
    private $r_left;
    private $r_top;
    private $m_left;
    private $m_top;
    private $z_index;
    private $z_axis = 0;

    // 其他
    private $is_error;
    private $error_no = "";
    private $error_text = "";

    private $m_left_mm;
    private $m_top_mm;
    private $product_s_width_mm;
    private $product_s_height_mm;

    public function getM_left_mm()
    {
        return $this->m_left_mm;
    }

    public function setM_left_mm($m_left_mm)
    {
        $this->m_left_mm = $m_left_mm;
    }

    public function getM_top_mm()
    {
        return $this->m_top_mm;
    }

    public function setM_top_mm($m_top_mm)
    {
        $this->m_top_mm = $m_top_mm;
    }

    public function getProduct_s_width_mm()
    {
        return $this->product_s_width_mm;
    }

    public function setProduct_s_width_mm($product_s_width_mm)
    {
        $this->product_s_width_mm = $product_s_width_mm;
    }

    public function getProduct_s_height_mm()
    {
        return $this->product_s_height_mm;
    }

    public function setProduct_s_height_mm($product_s_height_mm)
    {
        $this->product_s_height_mm = $product_s_height_mm;
    }

    public function getSeq()
    {
        return $this->seq;
    }

    public function setSeq($seq)
    {
        $this->seq = $seq;
    }

    public function getSeq_type()
    {
        return $this->seq_type;
    }

    public function setSeq_type($seqType)
    {
        $this->seq_type = $seqType;
    }

    public function getProduct_no()
    {
        return $this->product_no;
    }

    public function setProduct_no($product_no)
    {
        $this->product_no = $product_no;
    }

    public function getProduct_color_no()
    {
        return $this->product_color_no;
    }

    public function setProduct_color_no($product_color_no)
    {
        $this->product_color_no = $product_color_no;
    }

    public function getProduct_color_name()
    {
        return $this->product_color_name;
    }

    public function setProduct_color_name($product_color_name)
    {
        $this->product_color_name = $product_color_name;
    }

    public function getProduct_width()
    {
        return $this->product_width;
    }

    public function setProduct_width($product_width)
    {
        $this->product_width = $product_width;
    }

    public function getProduct_height()
    {
        return $this->product_height;
    }

    public function setProduct_height($product_height)
    {
        $this->product_height = $product_height;
    }

    public function getProduct_deep()
    {
        return $this->product_deep;
    }

    public function setProduct_deep($product_deep)
    {
        $this->product_deep = $product_deep;
    }

    public function getProduct_pic()
    {
        return $this->product_pic;
    }

    public function setProduct_pic($product_pic)
    {
        $this->product_pic = $product_pic;
    }

    public function getProduct_type()
    {
        return $this->product_type;
    }

    public function setProduct_type($product_type)
    {
        $this->product_type = $product_type;
    }

    public function getM_left()
    {
        return $this->m_left;
    }

    public function setM_left($m_left)
    {
        $this->m_left = $m_left;
    }

    public function getM_top()
    {
        return $this->m_top;
    }

    public function setM_top($m_top)
    {
        $this->m_top = $m_top;
    }

    public function getZ_index()
    {
        return $this->z_index;
    }

    public function setZ_index($z_index)
    {
        $this->z_index = $z_index;
    }

    public function getProduct_s_width()
    {
        return $this->product_s_width;
    }

    public function setProduct_s_width($product_s_width)
    {
        $this->product_s_width = $product_s_width;
    }

    public function getProduct_s_height()
    {
        return $this->product_s_height;
    }

    public function setProduct_s_height($product_s_height)
    {
        $this->product_s_height = $product_s_height;
    }

    public function getProduct_is_ytg()
    {
        return $this->product_is_ytg;
    }

    public function setProduct_is_ytg($product_is_ytg)
    {
        $this->product_is_ytg = $product_is_ytg;
    }

    public function getProduct_is_door()
    {
        return $this->product_is_door;
    }

    public function setProduct_is_door($product_is_door)
    {
        $this->product_is_door = $product_is_door;
    }

    public function getF_products()
    {
        return $this->f_products;
    }

    public function setF_products($f_products)
    {
        $this->f_products = $f_products;
    }

    public function getProduct_name()
    {
        return $this->product_name;
    }

    public function setProduct_name($product_name)
    {
        $this->product_name = $product_name;
    }

    public function getProduct_move_type()
    {
        return $this->product_move_type;
    }

    public function setProduct_move_type($product_move_type)
    {
        $this->product_move_type = $product_move_type;
    }

    public function getCan_delete()
    {
        return $this->can_delete;
    }

    public function setCan_delete($can_delete)
    {
        $this->can_delete = $can_delete;
    }

    public function getA_products()
    {
        return $this->a_products;
    }

    public function setA_products($a_products)
    {
        $this->a_products = $a_products;
    }

    public function getProduct_accType()
    {
        return $this->product_accType;
    }

    public function setProduct_accType($product_accType)
    {
        $this->product_accType = $product_accType;
    }

    public function getProduct_put_type()
    {
        return $this->product_put_type;
    }

    public function setProduct_put_type($product_put_type)
    {
        $this->product_put_type = $product_put_type;
    }

    public function getProduct_up_have()
    {
        return $this->product_up_have;
    }

    public function setProduct_up_have($product_up_have)
    {
        $this->product_up_have = $product_up_have;
    }

    public function getProduct_down_have()
    {
        return $this->product_down_have;
    }

    public function setProduct_down_have($product_down_have)
    {
        $this->product_down_have = $product_down_have;
    }

    public function getProduct_is_score()
    {
        return $this->product_is_score;
    }

    public function setProduct_is_score($product_is_score)
    {
        $this->product_is_score = $product_is_score;
    }

    public function getProduct_count()
    {
        return $this->product_count;
    }

    public function setProduct_count($product_count)
    {
        $this->product_count = $product_count;
    }

    public function getProduct_area()
    {
        if (empty($this->product_area)) {
            $this->product_area = 0;
        }
        return $this->product_area;
    }

    public function setProduct_area($product_area)
    {
        $this->product_area = $product_area;
    }

    public function getProduct_price()
    {
        return $this->product_price;
    }

    public function setProduct_price($product_price)
    {
        $this->product_price = $product_price;
    }

    public function getProduct_total_price()
    {
        return $this->product_total_price;
    }

    public function setProduct_total_price($product_total_price)
    {
        $this->product_total_price = $product_total_price;
    }

    public function getProduct_unit()
    {
        return $this->product_unit;
    }

    public function setProduct_unit($product_unit)
    {
        $this->product_unit = $product_unit;
    }

    public function getProduct_spec()
    {
        return $this->product_spec;
    }

    public function setProduct_spec($product_spec)
    {
        $this->product_spec = $product_spec;
    }

    public function getProduct_dis_price()
    {
        return $this->product_dis_price;
    }

    public function setProduct_dis_price($product_dis_price)
    {
        $this->product_dis_price = $product_dis_price;
    }

    public function getProduct_discount()
    {
        return $this->product_discount;
    }

    public function setProduct_discount($product_discount)
    {
        $this->product_discount = $product_discount;
    }

    public function getProduct_dis_total_price()
    {
        return $this->product_dis_total_price;
    }

    public function setProduct_dis_total_price($product_dis_total_price)
    {
        $this->product_dis_total_price = $product_dis_total_price;
    }

    public function getR_left()
    {
        return $this->r_left;
    }

    public function setR_left($r_left)
    {
        $this->r_left = $r_left;
    }

    public function getR_top()
    {
        return $this->r_top;
    }

    public function setR_top($r_top)
    {
        $this->r_top = $r_top;
    }

    public function getProduct_is_move()
    {
        return $this->product_is_move;
    }

    public function setProduct_is_move($product_is_move)
    {
        $this->product_is_move = $product_is_move;
    }

    public function getProduct_key()
    {
        return $this->product_key;
    }

    public function setProduct_key($product_key)
    {
        $this->product_key = $product_key;
    }

    public function getAcc_products()
    {
        return $this->acc_products;
    }

    public function setAcc_products($acc_products)
    {
        $this->acc_products = $acc_products;
    }

    public function getAcc_products_map()
    {
        return $this->acc_products_map;
    }

    public function setAcc_products_map($acc_products_map)
    {
        $this->acc_products_map = $acc_products_map;
    }

    public function getCom_products()
    {
        return $this->com_products;
    }

    public function setCom_products($com_products)
    {
        $this->com_products = $com_products;
    }

    public function getCom_products_map()
    {
        return $this->com_products_map;
    }

    public function setCom_products_map($com_products_map)
    {
        $this->com_products_map = $com_products_map;
    }

    public function getProduct_category()
    {
        return $this->product_category;
    }

    public function setProduct_category($product_category)
    {
        $this->product_category = $product_category;
    }

    public function getScheme_no()
    {
        return $this->scheme_no;
    }

    public function setScheme_no($scheme_no)
    {
        $this->scheme_no = $scheme_no;
    }

    public function getScheme_index()
    {
        return $this->scheme_index;
    }

    public function setScheme_index($scheme_index)
    {
        $this->scheme_index = $scheme_index;
    }

    public function getProduct_pic_list()
    {
        return $this->product_pic_list;
    }

    public function setProduct_pic_list($product_pic_list)
    {
        $this->product_pic_list = $product_pic_list;
    }

    public function getProduct_sm_color_name()
    {
        return $this->product_sm_color_name;
    }

    public function setProduct_sm_color_name($product_sm_color_name)
    {
        $this->product_sm_color_name = $product_sm_color_name;
    }

    public function getProduct_sm_color_no()
    {
        return $this->product_sm_color_no;
    }

    public function setProduct_sm_color_no($product_sm_color_no)
    {
        $this->product_sm_color_no = $product_sm_color_no;
    }

    public function getProduct_direction()
    {
        return $this->product_direction;
    }

    public function setProduct_direction($product_direction)
    {
        $this->product_direction = $product_direction;
    }

    public function getProduct_pic1()
    {
        return $this->product_pic1;
    }

    public function setProduct_pic1($product_pic1)
    {
        $this->product_pic1 = $product_pic1;
    }

    public function isIs_error()
    {
        return $this->is_error;
    }

    public function setIs_error($is_error)
    {
        $this->is_error = $is_error;
    }

    public function getError_no()
    {
        return $this->error_no;
    }

    public function setError_no($error_no)
    {
        $this->error_no = $error_no;
    }

    public function getError_text()
    {
        return $this->error_text;
    }

    public function setError_text($error_text)
    {
        $this->error_text = $error_text;
    }

    public function getProduct_sm_count()
    {
        return $this->product_sm_count;
    }

    public function setProduct_sm_count($product_sm_count)
    {
        $this->product_sm_count = $product_sm_count;
    }

    public function getOrder_no()
    {
        return $this->order_no;
    }

    public function setOrder_no($order_no)
    {
        $this->order_no = $order_no;
    }

    public function getScheme_col_index()
    {
        return $this->scheme_col_index;
    }

    public function setScheme_col_index($scheme_col_index)
    {
        $this->scheme_col_index = $scheme_col_index;
    }

    public function getProduct_index()
    {
        return $this->product_index;
    }

    public function setProduct_index($product_index)
    {
        $this->product_index = $product_index;
    }

    public function getZ_axis()
    {
        return $this->z_axis;
    }

    public function setZ_axis($z_axis)
    {
        $this->z_axis = $z_axis;
    }

    public function getProductContent()
    {
        return $this->product_content;
    }

    public function setProductContent($product_content)
    {
        $this->product_content = $product_content;
    }


}
