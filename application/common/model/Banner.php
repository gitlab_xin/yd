<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Db;
use think\Model;

class Banner extends Model
{
    protected $table = "yd_banner";
    protected $pk = 'banner_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    public $redirectType = null;
    public $redirect = null;

    /**
     * @return Banner
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function miniField()
    {
        return [
            'banner_id',
            'img_src',
            'related',
        ];
    }

    /**
     * @author: Airon
     * @time: 2017年7月14日
     * description:获取轮播图列表
     * @param array $where
     * @param array $userField
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function bannerList(array $where, array $userField = [])
    {
        $userField = $userField ?: self::miniField();
        $modelList = self::build()->where($where)->field($userField)->select();
        return $modelList;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:查看轮播图是否有超出最大限制
     * @param $max
     * @param $where
     * @return bool true:超过,false:没超过
     */
    public static function checkMaxLimit($max,$where,$id = 0)
    {
        if($max < 1){
            return true;
        }

        if($id > 0){//更新时
            $where['banner_id'] = ['neq',$id];
        }
        if(self::where($where)->count() >= $max){
            //如果查出来的数量比最大值还则代表无法添加或更新
            return true;
        }
        return false;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:添加或修改banner
     * @param $postData
     * @param int $id
     * @return $this
     */
    public static function saveBanner($postData,$id = 0)
    {
        return $id == 0?self::create($postData,true):self::update($postData,['banner_id'=>$id],true);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:设置model的redirectType属性
     * @param $value
     * @param $data
     * @return null
     */
    public function getRedirectTypeAttr($value,$data)
    {
        if($this->redirectType == null){
            $this->setRedirectProp($data['related']);
        }
        return $this->redirectType;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:设置model的redirect属性
     * @param $value
     * @param $data
     * @return null
     */
    public function getRedirectAttr($value,$data)
    {
        if($this->redirectType == null){
            $this->setRedirectProp($data['related']);
        }
        return $this->redirect;
    }

    private function setRedirectProp($related)
    {
        if($related == ''){
            $this->redirectType = $this->redirect = '';
        }else{
            $tem = spiltRelated($related);
            $this->redirectType = $tem['redirect_type'];
            $this->redirect = $tem['related'];
        }

    }
}
