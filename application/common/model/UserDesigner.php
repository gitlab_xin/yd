<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/24
 * Time: 14:27
 */

namespace app\common\model;

use think\Model;
use app\common\tools\JPush;

class UserDesigner extends Model
{
    protected $table = "yd_user_designer";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    public static function init()
    {
        parent::init();
        self::beforeInsert(function ($model) {
            if ($model->certificate_src === null) {
                $model->certificate_src = '';
            }
            if ($model->desc === null) {
                $model->desc = '';
            }
        });
        self::afterUpdate(function ($model) {
            if ($model->user->device_type != null && $model->user->push_id != null) {
                $config = config('jpush');
                $push = new JPush($config['AppKey'], $config['MasterSecret']);
                switch ($model->status) {
                    case 1:
                        $push->push($model->user->push_id, $model->user->device_type, '您的设计师申请已通过！', '');
                        break;
                    case 2:
                        $push->push($model->user->push_id, $model->user->device_type, '您的设计师申请未通过，请重新提交审核', '');
                        break;
                    default:
                        break;
                }


            }
        });
    }

    /**
     * @return UserDesigner
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function user()
    {
        return $this->hasOne('user', 'user_id', 'user_id', [], 'left');
    }
}
