<?php
namespace app\common\model;

use think\Model;

class OrderCustomizedScheme extends Model
{
    protected $table = "yd_order_customized_scheme";

    public static function build()
    {
        return new self();
    }
}
