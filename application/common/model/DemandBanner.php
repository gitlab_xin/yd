<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:48
 */

namespace app\common\model;

use think\Model;

class DemandBanner extends Model
{
    protected $table = "yd_demand_banner";
    protected $pk = 'banner_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return DemandBanner
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
