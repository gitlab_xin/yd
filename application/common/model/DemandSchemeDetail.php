<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/5
 * Time: 14:35
 */

namespace app\common\model;

use think\Model;

class DemandSchemeDetail extends Model
{
    protected $table = "yd_demand_scheme_detail";
    protected $pk = 'scheme_detail_id';
    protected $createTime = false;
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return DemandSchemeDetail
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
