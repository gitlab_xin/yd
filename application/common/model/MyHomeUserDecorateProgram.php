<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */
namespace app\common\model;

use think\Model;

class MyHomeUserDecorateProgram extends Model
{
    protected $table = "yd_my_home_user_decorate_program";

    /**
     * @return MyHomeUserDecorateProgram
     */
    public static function build()
    {
        return new self();
    }
}
