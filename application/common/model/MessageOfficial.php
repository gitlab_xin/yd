<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 11:12
 */

namespace app\common\model;

use think\Model;
use app\common\tools\JPush;

class MessageOfficial extends Model
{
    protected $table = "yd_message_official";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:极光推送
     */
    protected static function init()
    {
        self::afterInsert(function ($model) {
            $config = config('jpush');
            $push = new JPush($config['AppKey'], $config['MasterSecret']);
            $msg = $model->activity_id == 0?['protocol'=>'offical://id='.$model->id]:['protocol'=>'activity://id='.$model->activity_id];
            $push->allPush('易道官方发布了新的消息',$msg);
        });
    }

    /**
     * @return MessageOfficial
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveMessage($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['id'=>$id],true);
    }

    public function activity()
    {
        return $this->hasOne('activity','activity_id','activity_id');
    }
}
