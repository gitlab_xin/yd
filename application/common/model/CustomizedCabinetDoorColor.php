<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedCabinetDoorColor extends Model
{
    protected $table = "yd_customized_cabinet_door_color";
    protected $pk = 'cabinet_door_color_id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedCabinetDoorColor
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
