<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/4
 * Time: 15:54
 */
namespace app\common\model;

use think\Model;

class ShopOrderArea extends Model
{
    protected $table = "yd_shop_order_area";

    /**
     * @return ShopOrderArea
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
