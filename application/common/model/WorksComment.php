<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 12:01
 */

namespace app\common\model;

use think\Model;

class WorksComment extends Model
{
    protected $table = "yd_works_comment";
    protected $pk = 'comment_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return WorksComment
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:回复主贴者
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('user','user_id','user_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:回复回复者
     * @return \think\model\relation\HasOne
     *
     */
    public function toUser()
    {
        return $this->hasOne('user','user_id','by_user_id');
    }
}
