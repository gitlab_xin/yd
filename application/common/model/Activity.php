<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class Activity extends Model
{
    protected $table = "yd_activity";
    protected $pk = 'activity_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return Activity
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:保存或更新活动
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveActivity($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['activity_id'=>$id],true);
    }
}
