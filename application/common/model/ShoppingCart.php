<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 11:35
 */

namespace app\common\model;

use app\api\controller\Customized;
use app\common\model\CustomizedColor as ColorModel;
use think\Model;

class ShoppingCart extends Model
{
    protected $table = "yd_shop_shopping_cart";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShoppingCart
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:添加商品到购物车
     * @param int $user_id 用户ID
     * @param int $product_id 商品ID
     * @param int $standard_id 规格ID
     * @param int $count 数量
     * @return int|string|true
     */
    public static function add($user_id, $product_id, $standard_id, $count = 1)
    {
        $where['user_id'] = $user_id;
        $where['product_id'] = $product_id;
        $where['standard_id'] = $standard_id;
        $bool = self::build()->where($where)->find();
        if (empty($bool)) {
            $where['count'] = $count;
            $where['create_time'] = time();
            return self::build()->insert($where);
        } else {
            return self::build()->where($where)->setInc('count', $count);
        }
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:修改购物车商品数量
     * @param $cart_id
     * @param $user_id
     * @param $count
     * @return bool|$this
     */
    public static function setCount($cart_id, $user_id, $count)
    {
        $where['cart_id'] = $cart_id;
        $where['user_id'] = $user_id;
        $where['from'] = '1';
        $info = self::build()->where($where)->find();
        if (empty($info)) {
            return false;
        }
        return self::build()->update(['count' => $count], $where);
    }

    public static function getColorNameAttr($value, $data){
        if ($data['type'] == '3'){
            return '';
        }

        return ColorModel::build()
            ->where(['color_id' => $value])
            ->value('color_name');
    }

    public function getCartProductInfoAttr($value, $data){
        //商城 || 易家整配商城
        if ($data['from'] == '1' || $data['type'] == '3'){
            $info = ShopProductStandard::build()
                ->alias('s')
                ->join('yd_shop_product p', 'p.product_id = s.product_id')
                ->where(['s.standard_id' => $data['standard_id'], 's.product_id' => $data['product_id']])
                ->field([
                    's.img', 'p.name', 's.name standard_name', 's.price', 's.is_deleted', 's.stocks'
                ])
                ->find();

            return [
                'image' => $info['img'],
                'name' => $info['name'],
                'standard_name' => $info['standard_name'],
                'discount_price' => $info['price'],
                'is_delete' => $info['is_deleted'],
                'stocks' => $info['stocks']
            ];
        }

        //定制 || 易家整配用户自定义
        if ($data['from'] == '3' || $data['type'] == '1'){
            $info = CustomizedScheme::build()
                ->alias('s')
                ->join('yd_customized_color c', 'c.color_no = s.scheme_color_no')
                ->where(['s.id' => $data['product_id'], 'c.is_deleted' => '0', ])
                ->field([
                    'c.color_name', 's.scheme_pic', 's.scheme_name', 's.scheme_price', 's.scheme_type',
                    's.scheme_b_type'
                ])
                ->find();
            return [
                'image' => $info['scheme_pic'],
                'name' => $info['scheme_name'],
                'discount_price' => $info['scheme_price'],
                'is_delete' => '0',
                'standard_name' => $info['color_name'],
                'scheme_type' => $info['scheme_type'],
                'scheme_b_type' => $info['scheme_b_type'],
            ];
        }


        //易家整配
        if ($data['from'] == '2'){
            $info = MyDecorateProductStandard::build()
                ->alias('s')
                ->join('yd_customized_color c', 'c.color_id = s.color_id')
                ->join('yd_my_decorate_product p', 'p.id = s.decorate_product_id')
                ->join('yd_customized_scheme p_s','p_s.id = p.product_id')
                ->where(['s.color_id' => $data['color_id'], 's.decorate_product_id' => $data['decorate_product_id']])
                ->field([
                    'c.color_name', 's.product_img', 'p.name', 'p.discount_price', 'p.is_delete',
                    'p_s.scheme_type', 'p_s.scheme_b_type'
                ])
                ->find();
            return [
                'image' => $info['product_img'],
                'name' => $info['name'],
                'discount_price' => $info['discount_price'],
                'is_delete' => $info['is_delete'],
                'standard_name' => $info['color_name'],
                'scheme_type' => $info['scheme_type'],
                'scheme_b_type' => $info['scheme_b_type'],
            ];

        }
    }
}
