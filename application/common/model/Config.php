<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */
namespace app\common\model;

use think\Model;

class Config extends Model
{
    protected $table = "yd_config";
    protected $pk = 'id';

    /**
     * @return Config
     */
    public static function build()
    {
        return new self();
    }
}
