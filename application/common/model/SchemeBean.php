<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\model;

use app\common\tools\CommonMethod;

class SchemeBean implements \JsonSerializable
{

    public function jsonSerialize()
    {
        $maininfo = $this->mainInfo();
        unset($maininfo['scheme_pic']);
        return $maininfo;
    }

    public static function build($data)
    {
        $bean = new self();
        $bean->setScheme_no(isset($data['scheme_no']) && !empty($data['scheme_no']) ? $data['scheme_no'] : '');
        $bean->setScheme_name(isset($data['scheme_name']) && !empty($data['scheme_name']) ? $data['scheme_name'] : '');
        $bean->setScheme_type(isset($data['scheme_type']) && !empty($data['scheme_type']) ? $data['scheme_type'] : '');
        $bean->setScheme_pic(isset($data['scheme_pic']) && !empty($data['scheme_pic']) ? $data['scheme_pic'] : '');
        $bean->setScheme_width(isset($data['scheme_width']) && !empty($data['scheme_width']) ? $data['scheme_width'] : 0);
        $bean->setScheme_height(isset($data['scheme_height']) && !empty($data['scheme_height']) ? $data['scheme_height'] : 0);
        $bean->setScheme_hole_type(isset($data['scheme_hole_type']) && !empty($data['scheme_hole_type']) ? $data['scheme_hole_type'] : '');
        $bean->setScheme_hole_width(isset($data['scheme_hole_width']) && !empty($data['scheme_hole_width']) ? $data['scheme_hole_width'] : 0);
        $bean->setScheme_hole_height(isset($data['scheme_hole_height']) && !empty($data['scheme_hole_height']) ? $data['scheme_hole_height'] : 0);
        $bean->setScheme_hole_sl_height(isset($data['scheme_hole_sl_height']) && !empty($data['scheme_hole_sl_height']) ? $data['scheme_hole_sl_height'] : 0);
        $bean->setM_left(isset($data['m_left']) && !empty($data['m_left']) ? $data['m_left'] : 0);
        $bean->setM_top(isset($data['m_top']) && !empty($data['m_top']) ? $data['m_top'] : 0);
        $bean->setM_left_mm(isset($data['m_left_mm']) && !empty($data['m_left_mm']) ? $data['m_left_mm'] : 0);
        $bean->setM_top_mm(isset($data['m_top_mm']) && !empty($data['m_top_mm']) ? $data['m_top_mm'] : 0);
        $bean->setScheme_color_no(isset($data['scheme_color_no']) && !empty($data['scheme_color_no']) ? $data['scheme_color_no'] : '');
        $bean->setScheme_color_name(isset($data['scheme_color_name']) && !empty($data['scheme_color_name']) ? $data['scheme_color_name'] : '');
        $bean->setScheme_sk_color_no(isset($data['scheme_sk_color_no']) && !empty($data['scheme_sk_color_no']) ? $data['scheme_sk_color_no'] : '');
        $bean->setScheme_door_count(isset($data['scheme_door_count']) && !empty($data['scheme_door_count']) ? $data['scheme_door_count'] : 0);
        $bean->setScheme_door_color_no(isset($data['scheme_door_color_no']) ? $data['scheme_door_color_no'] : '');
        $bean->setScheme_door_haveHCQ(isset($data['scheme_door_have_hcq']) ? $data['scheme_door_have_hcq'] : '');
        $bean->setScheme_door_direction(isset($data['scheme_door_direction']) ? $data['scheme_door_direction'] : '');

        // scheme_schemes
        $schemeSchemesList = [];
        if (isset($data['scheme_schemes']) && is_array($data['scheme_schemes'])) {
            foreach ($data['scheme_schemes'] as $item) {
                $colScheme = new self();
                $schemeProductsList = $schemedoorsList = [];
                foreach ($item['scheme_products'] as $productData) {
                    $schemeProductsList[] = ProductBean::build($productData);
                }
                if (isset($item['scheme_door_schemes'])) {
                    foreach ($item['scheme_door_schemes'] as $doorData) {
                        $schemedoorsList[] = self::build($doorData);
                    }
                }
                $colScheme->setScheme_products($schemeProductsList);
                $colScheme->setScheme_door_schemes($schemedoorsList);
                $colScheme->setScheme_no(isset($item['scheme_no']) ? $item['scheme_no'] : '');
                $colScheme->setScheme_name(isset($item['scheme_name']) ? $item['scheme_name'] : '');
                $colScheme->setScheme_b_type(isset($item['scheme_b_type']) ? $item['scheme_b_type'] : '');
                $colScheme->setM_top_mm(isset($item['m_top_mm']) ? $item['m_top_mm'] : '');
                $colScheme->setM_left_mm(isset($item['m_left_mm']) ? $item['m_left_mm'] : '');
                $colScheme->setIs_optimize(
                    (isset($item['is_optimize']) && $item['is_optimize'])
                        ? '1'
                        : '0'
                );
                $schemeSchemesList[] = $colScheme;
            }
        }
        $bean->setScheme_schemes($schemeSchemesList);
        // wcb
        $wcbList = [];
        if (isset($data['scheme_schemes']) && is_array($data['scheme_wcb_products'])) {
            foreach ($data['scheme_wcb_products'] as $wcbData) {
                $wcbList[] = ProductBean::build($wcbData);
            }
        }
        $bean->setScheme_wcb_products($wcbList);
        // ncb
        $ncbList = [];
        if (isset($data['scheme_ncb_products']) && is_array($data['scheme_ncb_products'])) {
            foreach ($data['scheme_ncb_products'] as $ncbData) {
                $ncbList[] = ProductBean::build($ncbData);
            }
        }
        $bean->setScheme_ncb_products($ncbList);
        // sk
        $skList = [];
        if (isset($data['scheme_sk_products']) && is_array($data['scheme_sk_products'])) {
            foreach ($data['scheme_sk_products'] as $skData) {
                $skList[] = ProductBean::build($skData);
            }
        }
        $bean->setScheme_sk_products($skList);
        // g 盖板
        $gList = [];
        if (isset($data['scheme_g_products']) && is_array($data['scheme_g_products'])) {
            foreach ($data['scheme_g_products'] as $gData) {
                $gList[] = ProductBean::build($gData);
            }
        }
        $bean->setScheme_g_products($gList);

        $dgList = [];
        if (isset($data['scheme_dg_products']) && is_array($data['scheme_dg_products'])) {
            foreach ($data['scheme_dg_products'] as $dgData) {
                $dgList[] = ProductBean::build($dgData);
            }
        }
        $bean->setScheme_dg_products($dgList);

        $dtList = [];
        if (isset($data['scheme_dt_products']) && is_array($data['scheme_dt_products'])) {
            foreach ($data['scheme_dt_products'] as $dtData) {
                $dtList[] = ProductBean::build($dtData);
            }
        }
        $bean->setScheme_dt_products($dtList);

        return $bean;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function toArray()
    {
        $reflect = new \ReflectionClass($this);
        $pros = $reflect->getProperties();

        $res = [];
        foreach ($pros as $ref) {
            $key = $ref->getName();
            $value = $this->__get($key);
            if (!is_array($value)) {
                $res[$key] = $value;
            } else {
                $res2 = [];
                foreach ($value as $obj) {
                    $res2[] = $obj->toArray();
                }
                $res[$ref->getName()] = $res2;
            }
        }
        return $res;
    }

    public function mainInfo()
    {
        $result = [
            'scheme_no' => $this->getScheme_no(),
            'scheme_name' => $this->getScheme_name(),
            'scheme_pic' => $this->getScheme_pic(),

            'scheme_width' => $this->getScheme_width(),
            'scheme_height' => $this->getScheme_height(),

            'scheme_hole_type' => $this->getScheme_hole_type(),
            'scheme_hole_width' => $this->getScheme_hole_width(),
            'scheme_hole_height' => $this->getScheme_hole_height(),

            'm_left' => $this->getM_left(),
            'm_top' => $this->getM_top(),
            'm_left_mm' => $this->getM_left_mm(),
            'm_top_mm' => $this->getM_top_mm(),

            'scheme_type' => $this->getScheme_type(),
            'scheme_b_type' => $this->getScheme_b_type(),
            'scheme_s_type' => $this->getScheme_s_type(),
            'scheme_color_no' => $this->getScheme_color_no(),
            'scheme_color_name' => $this->getScheme_color_name(),
            'scheme_sk_color_no' => $this->getScheme_sk_color_no(),
            'scheme_door_count' => $this->getScheme_door_count(),
            'scheme_door_count_init' => $this->getScheme_door_count_init(),
            'scheme_error_range' => $this->getScheme_error_range(),

            'scheme_door_color_no' => $this->getScheme_door_color_no(),
            'scheme_door_have_hcq' => $this->getScheme_door_haveHCQ(),

            'scheme_schemes' => $this->getScheme_schemes_array(),
            'scheme_g_products' => $this->getScheme_g_products_array(),
            'scheme_wcb_products' => $this->getScheme_wcb_products_array(),
            'scheme_ncb_products' => $this->getScheme_ncb_products_array(),
            'scheme_sk_products' => $this->getScheme_sk_products_array(),
            'scheme_dg_products' => $this->getScheme_dg_products_array(),
            'scheme_dt_products' => $this->getScheme_dt_products_array(),
        ];

        return $result;
    }

    public function getScheme_schemes_array($schemes = null)
    {
        $result = [];
        $schemeSchemes = $schemes == null ? $this->getScheme_schemes() : $schemes;
        if (empty($schemeSchemes)) {
            return [];
        }
        foreach ($schemeSchemes as $schemeBean) {
            $schemeBean = change2SchemeBean($schemeBean);
            $result[] = [
                'scheme_products' => $schemeBean->getScheme_products_array(),
                'is_optimize' => $schemeBean->getIs_optimize(),
                'scheme_no' => $schemeBean->getScheme_no(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_color_no' => $schemeBean->getScheme_color_no(),
                'scheme_b_type' => $schemeBean->getScheme_b_type(),
                'scheme_name' => $schemeBean->getScheme_name(),
                'm_top_mm' => $schemeBean->getM_top_mm() === null ? 0 : $schemeBean->getM_top_mm(),
                'm_left_mm' => $schemeBean->getM_left_mm() === null ? 0 : $schemeBean->getM_left_mm(),
            ];
        }
        return $result;
    }

    public function getScheme_door_schemes_array()
    {
        $result = [];
        foreach ($this->getScheme_door_schemes() as $schemeBean) {
            $schemeBean = change2SchemeBean($schemeBean);
            $result[] = [
                'scheme_name' => $schemeBean->getScheme_name(),
                'scheme_no' => $schemeBean->getScheme_no(),
                'scheme_type' => $schemeBean->getScheme_type(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_s_height' => $schemeBean->getScheme_s_height(),
                'scheme_s_width' => $schemeBean->getScheme_s_width(),
                'scheme_color_no' => $schemeBean->getScheme_color_no(),
                'scheme_color_name' => $schemeBean->getScheme_color_name(),
                'scheme_door_direction' => $schemeBean->getScheme_door_direction(),
                'm_top_mm' => $schemeBean->getM_top_mm(),
                'm_left_mm' => $schemeBean->getM_left_mm(),
            ];
        }
        return $result;
    }

    public function getScheme_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getScheme_g_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_g_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

//    public static function buildScheme_products($data)
//    {
//        if (!is_array($data)) {
//            return [];
//        }
//        foreach ($data as $item) {
//            $productBean = ProductBean::build($item);
//        }
//    }

    public function getScheme_wcb_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_wcb_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getScheme_ncb_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_ncb_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getScheme_sk_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_sk_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getScheme_dt_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_dt_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getScheme_dg_products_array()
    {
        $result = [];
        $schemeProducts = $this->getScheme_dg_products();
        if (empty($schemeProducts)) {
            return [];
        }
        foreach ($schemeProducts as $productBean) {
            $productBean = change2ProductBean($productBean);
            $result[] = $productBean->mainInfo();
        }
        return $result;
    }

    public function getHashCode()
    {
        return $this->scheme_type . ($this->scheme_b_type != null ? '_' . $this->scheme_b_type : '') . '_' . md5(json_encode($this));
    }

    // 方案订单号
    private $order_no = "";

    // 方案名称
    private $scheme_name = "";

    // 方案编号
    private $scheme_no = "";

    // 方案购买数量
    private $scheme_order_count = 1;

    //是否被统计 0无1已统计
    private $scheme_is_score = 0;

    // 子方案列数
    private $scheme_count = 0;

    // 方案索引
    private $scheme_index = "";

    // 方案列索引
    private $scheme_col_index;

    // 洞口类型
    private $scheme_hole_type = 0;

    // 方案洞口宽度
    private $scheme_hole_width = 0;

    // 方案洞口高度
    private $scheme_hole_height = 0;

    // 方案洞口深度
    private $scheme_hole_deep = 0;

    // 方案洞口左边空间
    private $scheme_hole_left = 0;

    // 方案洞口右边空间
    private $scheme_hole_right = 0;

    // 洞口上梁高度
    private $scheme_hole_sl_height = 0;

    // 方案宽度
    private $scheme_width = 0;

    // 方案高度
    private $scheme_height = 0;

    // 方案深度
    private $scheme_deep = 0;

    // 方案显示宽度
    private $scheme_s_width = 0;

    // 方案显示高度
    private $scheme_s_height = 0;

    // 方案规格
    private $scheme_spec = "";

    // 误差
    private $scheme_error_range = 0;

    // 电视摆放方式
    private $scheme_tv_type = "";

    // 电视尺寸
    private $scheme_tv_size;

    // 花色编码
    private $scheme_color_no = "";

    // 花色名称
    private $scheme_color_name = "";

    // 图片地址
    private $scheme_pic = "";

    // 门框宽度
    private $scheme_mk_width = "";

    // 收口花色
    private $scheme_sk_color_no = "";

    // 方案门类型
    private $scheme_door_type = "";

    /**
     * 方案可安装的门数量
     */
    private $scheme_door_count = 0;

    /**
     * 方案可安装的门数量
     */
    private $scheme_door_count_init = 0;

    // 方案门花色代码
    private $scheme_door_color_no = "";

    // 单扇门宽度
    private $scheme_door_width = 0;

    // 单扇门宽度
    private $scheme_door_width_one = 0;

    // 单扇门高度
    private $scheme_door_height = 0;

    // 饰面花色代码
    private $scheme_sm_color_no = "";

    // 饰面花色名称
    private $scheme_sm_color_name = "";

    // 饰面数量
    private $scheme_sm_count;

    // 收口条 0无 1白色 2灰色
    private $scheme_sk;

    // 方案 List<Scheme>
    private $scheme_schemes;

    // 订单详情方案显示 List<Scheme>
    private $scheme_order_scheme;

    // 方案的门方案 List<Scheme>
    private $scheme_door_schemes;

    // 方案顶帽列表 List<Scheme>
    private $scheme_dm_schemes;

    // 方案无法安装门 0能按 1不能按
    private $scheme_no_door;

    // 方案是否有门
    private $scheme_have_door;

    // 方案是否有背板 0有 1没有
    private $scheme_have_bb;

    // 方案是否有桌子 0没有1有
    private $scheme_have_zz;

    // 方案门有无缓冲器
    private $scheme_door_have_hcq = 0;

    // 方案大类别
    private $scheme_b_type = "";

    // 方案类别
    private $scheme_type = "";

    // 方案小类别
    private $scheme_s_type = "";

    // 方案是否是其他杂项不予统计 boolean
    private $scheme_is_other = false;

    // 外侧板产品列表 List<Product>
    private $scheme_wcb_products;

    // 内侧板产品列表 List<Product>
    private $scheme_ncb_products;

    // 挡条产品列表 List<Product>
    private $scheme_dt_products;

    // 收口信息 List<Product>
    private $scheme_sk_products;

    // 方案顶柜信息 List<Product>
    private $scheme_dg_products;

    // 方案盖板 List<Product>
    private $scheme_g_products;

    // 方案产品Map Map<String, Product>
    private $scheme_products_map;

    // 方案产品信息 List<Product>
    private $scheme_products;

    // 方案饰品信息 List<Product>
    private $scheme_acc_products;

    // 方案抽屉组件列表 List<Product>
    private $scheme_ct_products;

    // 方案产品统计信息 List<Product>
    private $scheme_score_products;

    // 方案组件产品信息 List<Product>
    private $scheme_score_zj_products;

    // 方案门信息 List<Product>
    private $scheme_score_door_products;

    // 方案收口信息 List<Product>
    private $scheme_score_sk_products;

    // 阴影效果 List<Product>
    private $scheme_yy_products;

    // 单扇门规格
    private $scheme_door_spec = "";

    // 门数量
    private $scheme_door_has_count;

    // 门方向
    private $scheme_door_direction;

    // 门 girdcount
    private $scheme_gird_count;

    // 单扇门面积
    private $scheme_s_door_area;

    // 方案原价
    private $scheme_order_price = 0;

    // 方案折后价
    private $scheme_order_dis_price = 0;

    // 方案原价
    private $scheme_price = 0;

    // 方案折后价
    private $scheme_dis_price = 0;

    // 方案组件原件
    private $scheme_zj_price = 0;

    // 方案组件折扣价
    private $scheme_dis_zj_price = 0;

    // 柜体原价
    private $scheme_gt_price = 0;

    // 柜体折后价
    private $scheme_dis_gt_price = 0;

    // 折扣 默认68折
    private $scheme_discount = 0.68;

    // 门面积
    private $scheme_door_area;

    // 门材料价格
    private $scheme_door_mat_price = 0;

    // 门材料折扣价格
    private $scheme_dis_door_mat_price = 0;

    // 方案显示折扣
    private $scheme_show_discount = "";

    // 板材价格
    private $scheme_plate_price = 0;

    // 板材折扣价
    private $scheme_dis_plate_price = 0;

    // 五金配件价格
    private $scheme_wujin_price = 0;

    // 五金配件折扣价格
    private $scheme_dis_wujin_price = 0;

    // 门单价
    private $scheme_s_door_price = 0;

    //门单价（原价）
    private $scheme_o_door_price = 0;

    // 方案门价格
    private $scheme_door_price = 0;

    // 方案门折扣价格
    private $scheme_dis_door_price = 0;

    // 收口原价
    private $scheme_sk_price = 0;

    // 收口折扣价
    private $scheme_dis_sk_price = 0;

    // 门和收口合计原价
    private $scheme_door_and_sk_price = 0;

    // 门和收口合计折扣价
    private $scheme_dis_door_and_sk_price = 0;

    // 方案板材信息 List<Product>
    private $scheme_score_plates;

    // 五金配件产品信息 List<Product>
    private $scheme_score_wujin_products;

    // 方案是否已经计算完毕 boolean
    private $scheme_is_stats;

    /**
     * 0两边25 1左16右25 2两边16 (zhg0左侧16 1左侧25)
     */
    private $scheme_wcb_type;

    // 方案方向
    private $scheme_dir;

//    private Date add_cart_date;
    // 方案用户信息
//    private Customer scheme_customer;

    /*
     * 方案显示位置
     */
    private $m_left;

    private $m_top;

    private $m_left_mm;
    private $m_top_mm;

    /*
     * 方案效果展示相关属性
     */

    private $show_bizhi = "16";

    private $show_diban = "13";

    private $show_tianhuaban = "1";

    private $show_qianggao = "1";

    /*
     * 错误码
     */
    private $error_no = "";

    private $error_text = "";

    private $is_error;

    /**
     * 是否优化过
     */
    private $is_optimize = 0;

    /**
     * 右直墙、不带柱体
     */
    private $is_right_wall = false;

    /**
     * 左直墙、不带柱体
     */
    private $is_left_wall = false;

    /**
     * 右直墙、带柱体
     */
    private $is_right_wall_zt = false;

    /**
     * 左直墙、带柱体
     */
    private $is_left_wall_zt = false;

    /**
     * @var mongodb索引
     */
    private $search_index;

    /**
     * @var 门边框颜色
     */
    private $border_color;

    /**
     * @var 门把手颜色
     */
    private $handle_color;

    /**
     * @var 金属颜色
     */
    private $alloy_color;

    /**
     * @var 材质颜色
     */
    private $material_color;

    /**
     * @var 移门边框颜色
     */
    private $door_border_color;

    /**
     * @var 移门金属条
     */
    private $door_alloy_color;

    /**
     * @var 颜色等级
     */
    private $color_level;

    /**
     * @var 是否单色
     */
    private $is_monochrome;

    # function here

    /**
     * 右直墙、不带柱体
     */
    public function getIs_right_wall()
    {
        return $this->is_right_wall;
    }

    /**
     * 左直墙、不带柱体
     */
    public function getIs_left_wall()
    {
        return $this->is_left_wall;
    }

    /**
     * 右直墙、带柱体
     */
    public function getIs_right_wall_zt()
    {
        return $this->is_right_wall_zt;
    }

    /**
     * 左直墙、带柱体
     */
    public function getIs_left_wall_zt()
    {
        return $this->is_left_wall_zt;
    }

    public function getScheme_hole_width()
    {
        return $this->scheme_hole_width;
    }

    public function setScheme_hole_width($scheme_hole_width)
    {
        $this->scheme_hole_width = $scheme_hole_width;
    }

    public function getScheme_hole_height()
    {
        return $this->scheme_hole_height;
    }

    public function setScheme_hole_height($scheme_hole_height)
    {
        $this->scheme_hole_height = $scheme_hole_height;
    }

    public function getScheme_hole_deep()
    {
        return $this->scheme_hole_deep;
    }

    public function setScheme_hole_deep($scheme_hole_deep)
    {
        $this->scheme_hole_deep = $scheme_hole_deep;
    }

    public function getScheme_width()
    {
        return $this->scheme_width;
    }

    public function setScheme_width($scheme_width)
    {
        $this->scheme_width = $scheme_width;
    }

    public function getScheme_height()
    {
        return $this->scheme_height;
    }

    public function setScheme_height($scheme_height)
    {
        $this->scheme_height = $scheme_height;
    }

    public function getScheme_deep()
    {
        return $this->scheme_deep;
    }

    public function setScheme_deep($scheme_deep)
    {
        $this->scheme_deep = $scheme_deep;
    }

    public function getScheme_color_no()
    {
        if (is_null($this->scheme_color_no)) {
            $this->scheme_color_no = "";
        }
        return $this->scheme_color_no;
    }

    public function setScheme_color_no($scheme_color_no)
    {
        $this->scheme_color_no = $scheme_color_no;
    }

    public function getScheme_color_name()
    {
        if (is_null($this->scheme_color_name)) {
            $this->scheme_color_name = "";
        }
        return $this->scheme_color_name;
    }

    public function setScheme_color_name($scheme_color_name)
    {
        $this->scheme_color_name = $scheme_color_name;
    }

    public function getScheme_mk_width()
    {
        if (is_null($this->scheme_mk_width)) {
            $this->scheme_mk_width = "";
        }
        return $this->scheme_mk_width;
    }

    public function setScheme_mk_width($scheme_mk_width)
    {
        $this->scheme_mk_width = $scheme_mk_width;
    }

    public function getScheme_schemes()
    {
        return empty($this->scheme_schemes) ? [] : $this->scheme_schemes;
    }

    public function setScheme_schemes($scheme_schemes)
    {
        $this->scheme_schemes = $scheme_schemes;
    }

    public function getScheme_no()
    {
        if (is_null($this->scheme_no)) {
            $this->scheme_no = "";
        }
        return $this->scheme_no;
    }

    public function setScheme_no($scheme_no)
    {
        $this->scheme_no = $scheme_no;
    }

    public function getScheme_b_type()
    {
        if (is_null($this->scheme_b_type)) {
            $this->scheme_b_type = "";
        }
        return $this->scheme_b_type;
    }

    public function setScheme_b_type($scheme_b_type)
    {
        $this->scheme_b_type = $scheme_b_type;
    }

    public function getScheme_type()
    {
        if (is_null($this->scheme_type)) {
            $this->scheme_type = "";
        }
        return $this->scheme_type;
    }

    public function setScheme_type($scheme_type)
    {
        $this->scheme_type = $scheme_type;
    }

    public function getScheme_wcb_products()
    {
        return $this->scheme_wcb_products;
    }

    public function setScheme_wcb_products($scheme_wcb_products)
    {
        return $this->scheme_wcb_products = $scheme_wcb_products;
    }

    public function getScheme_ncb_products()
    {
        return $this->scheme_ncb_products;
    }

    public function setScheme_ncb_products($scheme_ncb_products)
    {
        $this->scheme_ncb_products = $scheme_ncb_products;
    }

    public function getScheme_dt_products()
    {
        return $this->scheme_dt_products;
    }

    public function setScheme_dt_products($scheme_dt_products)
    {
        $this->scheme_dt_products = $scheme_dt_products;
    }

    public function getScheme_products()
    {
        return empty($this->scheme_products) ? [] : $this->scheme_products;
    }

    public function setScheme_products($scheme_products)
    {
        $this->scheme_products = $scheme_products;
    }

    public function getScheme_sk()
    {
        return $this->scheme_sk;
    }

    public function setScheme_sk($scheme_sk)
    {
        $this->scheme_sk = $scheme_sk;
    }

    public function getScheme_s_width()
    {
        return $this->scheme_s_width;
    }

    public function setScheme_s_width($scheme_s_width)
    {
        $this->scheme_s_width = $scheme_s_width;
    }

    public function getScheme_s_height()
    {
        return $this->scheme_s_height;
    }

    public function setScheme_s_height($scheme_s_height)
    {
        $this->scheme_s_height = $scheme_s_height;
    }

    public function getScheme_pic()
    {
        if (is_null($this->scheme_pic)) {
            $this->scheme_pic = "";
        }
        return $this->scheme_pic;
    }

    public function setScheme_pic($scheme_pic)
    {
        $this->scheme_pic = $scheme_pic;
    }

    /**
     * 方案可安装的门数量
     *
     * @return
     */
    public function getScheme_door_count()
    {
        return $this->scheme_door_count;
    }

    /**
     * 方案可安装的门数量
     *
     * @param scheme_door_count
     */
    public function setScheme_door_count($scheme_door_count)
    {
        $this->scheme_door_count = $scheme_door_count;
    }

    /**
     * 方案可安装的门数量
     *
     * @return
     */
    public function getScheme_door_count_init()
    {
        return $this->scheme_door_count_init;
    }

    /**
     * 方案可安装的门数量
     *
     * @param scheme_door_count
     */
    public function setScheme_door_count_init($scheme_door_count)
    {
        $this->scheme_door_count_init = $scheme_door_count;
    }

    public function getScheme_wcb_type()
    {
        return $this->scheme_wcb_type;
    }

    public function setScheme_wcb_type($scheme_wcb_type)
    {
        $this->scheme_wcb_type = $scheme_wcb_type;
    }

    public function getScheme_sk_products()
    {
        return $this->scheme_sk_products;
    }

    public function setScheme_sk_products($scheme_sk_products)
    {
        $this->scheme_sk_products = $scheme_sk_products;
    }

    public function getScheme_sk_color_no()
    {
        if (is_null($this->scheme_sk_color_no)) {
            $this->scheme_sk_color_no = "";
        }
        return $this->scheme_sk_color_no;
    }

    public function setScheme_sk_color_no($scheme_sk_color_no)
    {
        $this->scheme_sk_color_no = $scheme_sk_color_no;
    }

    public function getScheme_score_products()
    {
        return $this->scheme_score_products;
    }

    public function setScheme_score_products($scheme_score_products)
    {
        $this->scheme_score_products = $scheme_score_products;
    }

    public function getScheme_plate_price()
    {
        return $this->scheme_plate_price;
    }

    public function setScheme_plate_price($scheme_plate_price)
    {
        $this->scheme_plate_price = floatval($scheme_plate_price);
    }

    public function getScheme_wujin_price()
    {
        return $this->scheme_wujin_price;
    }

    public function setScheme_wujin_price($scheme_wujin_price)
    {
        $this->scheme_wujin_price = $scheme_wujin_price;
    }

    public function getScheme_door_schemes()
    {
        return $this->scheme_door_schemes;
    }

    public function setScheme_door_schemes($scheme_door_schemes)
    {
        $this->scheme_door_schemes = $scheme_door_schemes;
    }

    public function getScheme_door_color_no()
    {
        if (null == $this->scheme_door_color_no) {
            $this->scheme_door_color_no = "";
        }
        return $this->scheme_door_color_no;
    }

    public function setScheme_door_color_no($scheme_door_color_no)
    {
        $this->scheme_door_color_no = $scheme_door_color_no;
    }

    public function getScheme_door_price()
    {
        return $this->scheme_door_price;
    }

    public function setScheme_door_price($scheme_door_price)
    {
        $this->scheme_door_price = $scheme_door_price;
    }

    public function getScheme_have_door()
    {
        return $this->scheme_have_door;
    }

    public function setScheme_have_door($scheme_haveDoor)
    {
        $this->scheme_have_door = $scheme_haveDoor;
    }

    public function getScheme_door_haveHCQ()
    {
        return $this->scheme_door_have_hcq;
    }

    public function setScheme_door_haveHCQ($scheme_door_haveHCQ)
    {
        $this->scheme_door_have_hcq = $scheme_door_haveHCQ;
    }

    public function getScheme_discount()
    {
        return $this->scheme_discount;
    }

    public function setScheme_discount($scheme_discount)
    {
        $this->scheme_discount = $scheme_discount;
    }

    public function getScheme_dis_plate_price()
    {
        return $this->scheme_dis_plate_price;
    }

    public function setScheme_dis_plate_price($scheme_dis_plate_price)
    {
        $this->scheme_dis_plate_price = floatval($scheme_dis_plate_price);
    }

    public function getScheme_dis_wujin_price()
    {
        return $this->scheme_dis_wujin_price;
    }

    public function setScheme_dis_wujin_price($scheme_dis_wujin_price)
    {
        $this->scheme_dis_wujin_price = $scheme_dis_wujin_price;
    }

    public function getScheme_dis_door_price()
    {
        return $this->scheme_dis_door_price;
    }

    public function setScheme_dis_door_price($scheme_dis_door_price)
    {
        $this->scheme_dis_door_price = $scheme_dis_door_price;
    }

    public function getScheme_score_door_products()
    {
        return $this->scheme_score_door_products;
    }

    public function setScheme_score_door_products(
        $scheme_score_door_products
    )
    {
        $this->scheme_score_door_products = $scheme_score_door_products;
    }

    public function getScheme_score_sk_products()
    {
        return $this->scheme_score_sk_products;
    }

    public function setScheme_score_sk_products(
        $scheme_score_sk_products
    )
    {
        $this->scheme_score_sk_products = $scheme_score_sk_products;
    }

    public function getScheme_acc_products()
    {
        return $this->scheme_acc_products;
    }

    public function setScheme_acc_products($scheme_acc_products)
    {
        $this->scheme_acc_products = $scheme_acc_products;
    }

    public function getScheme_score_plates()
    {
        return $this->scheme_score_plates;
    }

    public function setScheme_score_plates($scheme_score_plates)
    {
        $this->scheme_score_plates = $scheme_score_plates;
    }

    public function getScheme_score_wujin_products()
    {
        return $this->scheme_score_wujin_products;
    }

    public function setScheme_score_wujin_products(
        $scheme_score_wujin_products
    )
    {
        $this->scheme_score_wujin_products = $scheme_score_wujin_products;
    }

    public function getShow_bizhi()
    {
        if (is_null($this->show_bizhi)) {
            $this->show_bizhi = "";
        }
        return $this->show_bizhi;
    }

    public function setShow_bizhi($show_bizhi)
    {
        $this->show_bizhi = $show_bizhi;
    }

    public function getShow_diban()
    {
        if (null == $this->show_diban) {
            $this->show_diban = "";
        }
        return $this->show_diban;
    }

    public function setShow_diban($show_diban)
    {
        $this->show_diban = $show_diban;
    }

    public function getShow_tianhuaban()
    {
        if (null == $this->show_tianhuaban) {
            $this->show_tianhuaban = "";
        }
        return $this->show_tianhuaban;
    }

    public function setShow_tianhuaban($show_tianhuaban)
    {
        $this->show_tianhuaban = $show_tianhuaban;
    }

    public function getShow_qianggao()
    {
        if (null == $this->show_qianggao) {
            $this->show_qianggao = "";
        }
        return $this->show_qianggao;
    }

    public function setShow_qianggao($show_qianggao)
    {
        $this->show_qianggao = $show_qianggao;
    }

    public function getScheme_price()
    {
        return $this->scheme_price;
    }

    public function setScheme_price($scheme_price)
    {
        $this->scheme_price = $scheme_price;
    }

    public function getScheme_dis_price()
    {
        return $this->scheme_dis_price;
    }

    public function setScheme_dis_price($scheme_dis_price)
    {
        $this->scheme_dis_price = $scheme_dis_price;
    }

    public function getScheme_gt_price()
    {
        return $this->scheme_gt_price;
    }

    public function setScheme_gt_price($scheme_gt_price)
    {
        $this->scheme_gt_price = $scheme_gt_price;
    }

    public function getScheme_dis_gt_price()
    {
        return $this->scheme_dis_gt_price;
    }

    public function setScheme_dis_gt_price($scheme_dis_gt_price)
    {
        $this->scheme_dis_gt_price = $scheme_dis_gt_price;
    }

    public function getScheme_show_discount()
    {
        if (null == $this->scheme_show_discount) {
            $this->scheme_show_discount = "";
        }
        return $this->scheme_show_discount;
    }

    public function setScheme_show_discount($scheme_show_discount)
    {
        $this->scheme_show_discount = $scheme_show_discount;
    }

    public function getScheme_door_area()
    {
        return $this->scheme_door_area;
    }

    public function setScheme_door_area($scheme_door_area)
    {
        $this->scheme_door_area = $scheme_door_area;
    }

    public function getScheme_sk_price()
    {
        return $this->scheme_sk_price;
    }

    public function setScheme_sk_price($scheme_sk_price)
    {
        $this->scheme_sk_price = $scheme_sk_price;
    }

    public function getScheme_dis_sk_price()
    {
        return $this->scheme_dis_sk_price;
    }

    public function setScheme_dis_sk_price($scheme_dis_sk_price)
    {
        $this->scheme_dis_sk_price = $scheme_dis_sk_price;
    }

    public function getScheme_s_door_price()
    {
        return $this->scheme_s_door_price;
    }

    public function setScheme_s_door_price($scheme_s_door_price)
    {
        $this->scheme_s_door_price = $scheme_s_door_price;
    }

    public function getScheme_o_door_price()
    {
        return $this->scheme_o_door_price;
    }

    public function setScheme_o_door_price($scheme_o_door_price)
    {
        $this->scheme_o_door_price = $scheme_o_door_price;
    }

    public function getScheme_door_spec()
    {
        if (null == $this->scheme_door_spec) {
            $this->scheme_door_spec = "";
        }
        return $this->scheme_door_spec;
    }

    public function setScheme_door_spec($scheme_door_spec)
    {
        $this->scheme_door_spec = $scheme_door_spec;
    }

    public function getScheme_door_has_count()
    {
        return $this->scheme_door_has_count;
    }

    public function setScheme_door_has_count($scheme_door_has_count)
    {
        $this->scheme_door_has_count = $scheme_door_has_count;
    }

    public function getScheme_s_door_area()
    {
        return $this->scheme_s_door_area;
    }

    public function setScheme_s_door_area($scheme_s_door_area)
    {
        $this->scheme_s_door_area = $scheme_s_door_area;
    }

    public function getScheme_door_mat_price()
    {
        return $this->scheme_door_mat_price;
    }

    public function setScheme_door_mat_price($scheme_door_mat_price)
    {
        $this->scheme_door_mat_price = $scheme_door_mat_price;
    }

    public function getScheme_dis_door_mat_price()
    {
        return $this->scheme_dis_door_mat_price;
    }

    public function setScheme_dis_door_mat_price($scheme_dis_door_mat_price)
    {
        $this->scheme_dis_door_mat_price = $scheme_dis_door_mat_price;
    }

    public function getScheme_door_and_sk_price()
    {
        return $this->scheme_door_and_sk_price;
    }

    public function setScheme_door_and_sk_price($scheme_door_and_sk_price)
    {
        $this->scheme_door_and_sk_price = $scheme_door_and_sk_price;
    }

    public function getScheme_dis_door_and_sk_price()
    {
        return $this->scheme_dis_door_and_sk_price;
    }

    public function setScheme_dis_door_and_sk_price($scheme_dis_door_and_sk_price)
    {
        $this->scheme_dis_door_and_sk_price = $scheme_dis_door_and_sk_price;
    }

    public function getScheme_score_zj_products()
    {
        return $this->scheme_score_zj_products;
    }

    public function setScheme_score_zj_products(
        $scheme_score_zj_products
    )
    {
        $this->scheme_score_zj_products = $scheme_score_zj_products;
    }

    public function getScheme_zj_price()
    {
        return $this->scheme_zj_price;
    }

    public function setScheme_zj_price($scheme_zj_price)
    {
        $this->scheme_zj_price = $scheme_zj_price;
    }

    public function getScheme_dis_zj_price()
    {
        return $this->scheme_dis_zj_price;
    }

    public function setScheme_dis_zj_price($scheme_dis_zj_price)
    {
        $this->scheme_dis_zj_price = $scheme_dis_zj_price;
    }

    public function getScheme_yy_products()
    {
        return $this->scheme_yy_products;
    }

    public function setScheme_yy_products($scheme_yy_products)
    {
        $this->scheme_yy_products = $scheme_yy_products;
    }

    public function getScheme_products_map()
    {
        return $this->scheme_products_map;
    }

    public function setScheme_products_map($scheme_products_Map)
    {
        $this->scheme_products_map = $scheme_products_Map;
    }

    public function getScheme_noDoor()
    {
        return $this->scheme_no_door;
    }

    public function setScheme_no_door($scheme_noDoor)
    {
        $this->scheme_no_door = $scheme_noDoor;
    }

    public function getScheme_hole_left()
    {
        return $this->scheme_hole_left;
    }

    public function setScheme_hole_left($scheme_hole_left)
    {
        $this->scheme_hole_left = $scheme_hole_left;
    }

    public function getScheme_hole_right()
    {
        return $this->scheme_hole_right;
    }

    public function setScheme_hole_right($scheme_hole_right)
    {
        $this->scheme_hole_right = $scheme_hole_right;
    }

    public function getM_left()
    {
        return $this->m_left;
    }

    public function setM_left($m_left)
    {
        $this->m_left = $m_left;
    }

    public function getM_top()
    {
        return $this->m_top;
    }

    public function setM_top($m_top)
    {
        $this->m_top = $m_top;
    }

    public function getM_left_mm()
    {
        return $this->m_left_mm;
    }

    public function setM_left_mm($m_left_mm)
    {
        $this->m_left_mm = $m_left_mm;
    }

    public function getM_top_mm()
    {
        return $this->m_top_mm;
    }

    public function setM_top_mm($m_top_mm)
    {
        $this->m_top_mm = $m_top_mm;
    }

    public function getScheme_door_width()
    {
        return $this->scheme_door_width;
    }

    public function setScheme_door_width($scheme_door_width)
    {
        $this->scheme_door_width = $scheme_door_width;
    }

    public function getScheme_door_height()
    {
        return $this->scheme_door_height;
    }

    public function setScheme_door_height($scheme_door_height)
    {
        $this->scheme_door_height = $scheme_door_height;
    }

    public function isScheme_is_stats()
    {
        return $this->scheme_is_stats;
    }

    public function setScheme_is_stats($scheme_is_stats)
    {
        $this->scheme_is_stats = $scheme_is_stats;
    }

    public function getScheme_dg_products()
    {
        return $this->scheme_dg_products;
    }

    public function setScheme_dg_products($scheme_dg_products)
    {
        $this->scheme_dg_products = $scheme_dg_products;
    }

    public function getScheme_count()
    {
        return $this->scheme_count;
    }

    public function setScheme_count($scheme_count)
    {
        $this->scheme_count = $scheme_count;
    }

    public function getScheme_is_score()
    {
        return $this->scheme_is_score;
    }

    public function setScheme_is_score($scheme_is_score)
    {
        $this->scheme_is_score = $scheme_is_score;
    }

    public function getScheme_index()
    {
        if (null == $this->scheme_index) {
            $this->scheme_index = "";
        }
        return $this->scheme_index;
    }

    public function setScheme_index($scheme_index)
    {
        $this->scheme_index = $scheme_index;
    }

    public function getScheme_haveBB()
    {
        return $this->scheme_have_bb;
    }

    public function setScheme_haveBB($scheme_haveBB)
    {
        $this->scheme_have_bb = $scheme_haveBB;
    }

    public function getError_no()
    {
        if (null == $this->error_no) {
            $this->error_no = "";
        }
        return $this->error_no;
    }

    public function setError_no($error_no)
    {
        $this->error_no = $error_no;
    }

    public function isIs_error()
    {
        return $this->is_error;
    }

    public function setIs_error($is_error)
    {
        $this->is_error = $is_error;
    }

    public function getError_text()
    {
        if (null == $this->error_text) {
            $this->error_text = "";
        }
        return $this->error_text;
    }

    public function setError_text($error_text)
    {
        $this->error_text = $error_text;
    }

    public function getScheme_name()
    {
        if (null == $this->scheme_name) {
            $this->scheme_name = "";
        }
        return $this->scheme_name;
    }

    public function setScheme_name($scheme_name)
    {
        $this->scheme_name = $scheme_name;
    }

    public function getScheme_error_range()
    {
        return $this->scheme_error_range;
    }

    public function setScheme_error_range($scheme_error_range)
    {
        $this->scheme_error_range = $scheme_error_range;
    }

    public function getScheme_s_type()
    {
        if (null == $this->scheme_s_type) {
            $this->scheme_s_type = "";
        }
        return $this->scheme_s_type;
    }

    public function setScheme_s_type($scheme_s_type)
    {
        $this->scheme_s_type = $scheme_s_type;
    }

    public function getScheme_tv_size()
    {
        return $this->scheme_tv_size;
    }

    public function setScheme_tv_size($scheme_tv_size)
    {
        $this->scheme_tv_size = $scheme_tv_size;
    }

    public function getScheme_dm_schemes()
    {
        return $this->scheme_dm_schemes;
    }

    public function setScheme_dm_schemes($scheme_dm_schemes)
    {
        $this->scheme_dm_schemes = $scheme_dm_schemes;
    }

    public function getScheme_ct_products()
    {
        return $this->scheme_ct_products;
    }

    public function setScheme_ct_products($scheme_ct_products)
    {
        $this->scheme_ct_products = $scheme_ct_products;
    }

    public function getScheme_tv_type()
    {
        if (null == $this->scheme_tv_type) {
            $this->scheme_tv_type = "";
        }
        return $this->scheme_tv_type;
    }

    public function setScheme_tv_type($scheme_tv_type)
    {
        $this->scheme_tv_type = $scheme_tv_type;
    }

    public function getScheme_g_products()
    {
        return $this->scheme_g_products;
    }

    public function setScheme_g_products($scheme_g_products)
    {
        $this->scheme_g_products = $scheme_g_products;
    }

    public function getScheme_door_direction()
    {
        return $this->scheme_door_direction;
    }

    public function setScheme_door_direction($scheme_doorDirection)
    {
        $this->scheme_door_direction = $scheme_doorDirection;
    }

    public function getScheme_gird_count()
    {
        return $this->scheme_gird_count;
    }

    public function setScheme_gird_count($scheme_gird_count)
    {
        $this->scheme_gird_count = $scheme_gird_count;
    }

    // public function isScheme_doorColor() {
    // return $this->scheme_doorColor;
    // }

    // public function setScheme_doorColor($scheme_doorColor) {
    // $this->scheme_doorColor = $scheme_doorColor;
    // }

    public function getScheme_haveZZ()
    {
        return $this->scheme_have_zz;
    }

    public function setScheme_haveZZ($scheme_haveZZ)
    {
        $this->scheme_have_zz = $scheme_haveZZ;
    }

    public function getScheme_dir()
    {
        return $this->scheme_dir;
    }

    public function setScheme_dir($scheme_dir)
    {
        $this->scheme_dir = $scheme_dir;
    }

    public function getScheme_door_width_one()
    {
        return $this->scheme_door_width_one;
    }

    public function setScheme_door_width_one($scheme_door_width_one)
    {
        $this->scheme_door_width_one = $scheme_door_width_one;
    }

    public function getScheme_order_count()
    {
        return $this->scheme_order_count;
    }

    public function setScheme_order_count($scheme_order_count)
    {
        $this->scheme_order_count = $scheme_order_count;
    }

    public function getScheme_spec()
    {
        if (null == $this->scheme_spec) {
            $this->scheme_spec = "";
        }
        return $this->scheme_spec;
    }

    public function setScheme_spec($scheme_spec)
    {
        $this->scheme_spec = $scheme_spec;
    }

    public function getScheme_order_price()
    {
        return $this->scheme_order_price;
    }

    public function setScheme_order_price($scheme_order_price)
    {
        $this->scheme_order_price = $scheme_order_price;
    }

    public function getScheme_order_dis_price()
    {
        return $this->scheme_order_dis_price;
    }

    public function setScheme_order_dis_price($scheme_order_dis_price)
    {
        $this->scheme_order_dis_price = $scheme_order_dis_price;
    }

    public function isScheme_is_other()
    {
        return $this->scheme_is_other;
    }

    public function setScheme_is_other($scheme_is_other)
    {
        $this->scheme_is_other = $scheme_is_other;
    }

    public function getOrder_no()
    {
        if (null == $this->order_no) {
            $this->order_no = "";
        }
        return $this->order_no;
    }

    public function setOrder_no($order_no)
    {
        $this->order_no = $order_no;
    }

    public function getScheme_col_index()
    {
        return $this->scheme_col_index;
    }

    public function setScheme_col_index($scheme_col_index)
    {
        $this->scheme_col_index = $scheme_col_index;
    }

    public function getScheme_order_scheme()
    {
        return $this->scheme_order_scheme;
    }

    public function setScheme_order_scheme($scheme_order_scheme)
    {
        $this->scheme_order_scheme = $scheme_order_scheme;
    }

    public function getScheme_sm_color_no()
    {
        if (null == $this->scheme_sm_color_no) {
            $this->scheme_sm_color_no = "";
        }
        return $this->scheme_sm_color_no;
    }

    public function setScheme_sm_color_no($scheme_sm_color_no)
    {
        $this->scheme_sm_color_no = $scheme_sm_color_no;
    }

    public function getScheme_sm_color_name()
    {
        if (null == $this->scheme_sm_color_name) {
            $this->scheme_sm_color_name = "";
        }
        return $this->scheme_sm_color_name;
    }

    public function setScheme_sm_color_name($scheme_sm_color_name)
    {
        $this->scheme_sm_color_name = $scheme_sm_color_name;
    }

    public function getScheme_sm_count()
    {
        return $this->scheme_sm_count;
    }

    public function setScheme_sm_count($scheme_sm_count)
    {
        $this->scheme_sm_count = $scheme_sm_count;
    }

    public function getScheme_hole_sl_height()
    {
        return $this->scheme_hole_sl_height;
    }

    public function setScheme_hole_sl_height($scheme_hole_sl_height)
    {
        $this->scheme_hole_sl_height = $scheme_hole_sl_height;
    }

    public function getScheme_hole_type()
    {
        return $this->scheme_hole_type;
    }

    public function setScheme_hole_type($scheme_hole_type)
    {
        $this->scheme_hole_type = $scheme_hole_type;
        $holetype = $this->scheme_hole_type % 4;

        // 长墙有柱子
        if ($this->scheme_hole_type == 7 || $this->scheme_hole_type == 8
            || $this->scheme_hole_type == 15 || $this->scheme_hole_type == 16
        ) {
            $this->is_left_wall_zt = true;
        }
        if ($this->scheme_hole_type == 10 || $this->scheme_hole_type == 12
            || $this->scheme_hole_type == 14 || $this->scheme_hole_type == 16
        ) {
            $this->is_right_wall_zt = true;
        }

        if (0 == $holetype) {
            if (!$this->is_left_wall_zt) {
                $this->is_left_wall = true;
            }
            if (!$this->is_right_wall_zt) {
                $this->is_right_wall = true;
            }
        } else {
            if (1 == $holetype) {

            } else {
                if (2 == $holetype) {
                    if (!$this->is_right_wall_zt) {
                        $this->is_right_wall = true;
                    }
                } else {
                    if (3 == $holetype) {
                        if (!$this->is_left_wall_zt) {
                            $this->is_left_wall = true;
                        }
                    }
                }
            }
        }
    }

    public function getSearch_index()
    {
        if (null == $this->search_index) {
            $this->search_index = "";
        }
        return $this->search_index;
    }

    public function setSearch_index($search_index)
    {
        $this->search_index = $search_index;
    }

    public function getScheme_door_type()
    {
        if (null == $this->scheme_door_type) {
            $this->scheme_door_type = "";
        }
        return $this->scheme_door_type;
    }

    public function setScheme_door_type($scheme_door_type)
    {
        $this->scheme_door_type = $scheme_door_type;
    }

    public function getIs_optimize()
    {
        return $this->is_optimize;
    }

    public function setIs_optimize($isOptimize)
    {
        $this->is_optimize = $isOptimize;
    }

    public function setBorder_color($border_color = '')
    {
        $this->border_color = $border_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['border_color'] : $border_color;
    }

    public function setHandle_color($handle_color = '')
    {
        $this->handle_color = $handle_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['handle_color'] : $handle_color;
    }

    public function setAlloy_color($alloy_color = '')
    {
        $this->alloy_color = $alloy_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['alloy_color'] : $alloy_color;
    }

    public function setMaterial_color($material_color = '')
    {
        $this->material_color = $material_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['material_color'] : $material_color;
    }

    public function setDoor_border_color($door_border_color = '')
    {
        $this->door_border_color = $door_border_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['door_border_color'] : $door_border_color;
    }

    public function setDoor_alloy_color($door_alloy_color = '')
    {
        $this->door_alloy_color = $door_alloy_color == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['door_alloy_color'] : $door_alloy_color;
    }

    public function setColor_level($color_level = '')
    {
        $this->color_level = $color_level == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['color_level'] : $color_level;
    }

    public function setIs_monochrome($is_monochrome = '')
    {
        $this->is_monochrome = $is_monochrome == '' ? CommonMethod::getColorDetail($this->getScheme_color_no())['is_monochrome'] : $is_monochrome;
    }

    public function getBorder_color()
    {
        if ($this->border_color == '') {
            $this->setBorder_color();
        }
        return $this->border_color;
    }

    public function getHandle_color()
    {
        if ($this->handle_color == '') {
            $this->setHandle_color();
        }
        return $this->handle_color;
    }

    public function getAlloy_color()
    {
        if ($this->alloy_color == '') {
            $this->setAlloy_color();
        }
        return $this->alloy_color;
    }

    public function getMaterial_color()
    {
        if ($this->material_color == '') {
            $this->setMaterial_color();
        }
        return $this->material_color;
    }

    public function getDoor_border_color()
    {
        if ($this->door_border_color == '') {
            $this->setDoor_border_color();
        }
        return $this->door_border_color;
    }

    public function getDoor_alloy_color()
    {
        if ($this->door_alloy_color == '') {
            $this->setDoor_alloy_color();
        }
        return $this->door_alloy_color;
    }

    public function getColor_level()
    {
        if ($this->color_level == '') {
            $this->setColor_level();
        }
        return $this->color_level;
    }

    public function getIs_monochrome()
    {
        if ($this->is_monochrome == '') {
            $this->setIs_monochrome();
        }
        return $this->is_monochrome;
    }

    public function setIs_left_wall($is_left_wall)
    {
        $this->is_left_wall = $is_left_wall;
    }

    public function setIs_right_wall($is_right_wall)
    {
        $this->is_right_wall = $is_right_wall;
    }
}
