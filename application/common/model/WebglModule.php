<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class WebglModule extends Model
{
    protected $table = "yd_webgl_module";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return WebglModule
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function saveModules($data)
    {
        $arr = [];
        foreach ($data['url'] as $url) {
            $arr[] = ['url' => $url, 'scene_id' => $data['scene_id'],
                'name' => explode('.', explode('/', $url)[1])[0],
                'is_obj' => strpos($url, 'obj', strlen($url) - 3) !== false ? 1 : 0];
        }
        return static::build()->saveAll($arr);
    }

    public static function getObjs()
    {
        return static::build()->field(['name', 'url', 'create_time'])->where(['is_obj' => 1])->order(['id' => 'DESC'])->group('scene_id')->select();
    }

}
