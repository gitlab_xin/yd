<?php
/**
 * Created by PhpStorm.
<<<<<<< HEAD
 * User: Rudy
 * Date: 2017/7/21
 * Time: 14:35
=======
 * User: Airon
 * Date: 2017/7/21
 * Time: 14:46
>>>>>>> dev
 */

namespace app\common\model;

use think\Model;

class ShopChangePriceRecord extends Model
{
    protected $table = "yd_shop_change_price_record";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopChangePriceRecord
     */
    public static function build()
    {
        return new self();
    }

    public static function getList($product_id, $standard_id)
    {
        return self::build()
            ->where(['product_id' => $product_id, 'standard_id' => $standard_id])
            ->field(['id', 'change_price', 'reason', 'create_time'])
            ->order(['create_time' => 'ASC'])
            ->select();
    }

}