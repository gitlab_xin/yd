<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 16:12
 */

namespace app\common\model;

use think\Model;

class WorksCollect extends Model
{
    protected $table = "yd_works_collect";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return WorksCollect
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 验证是否已经收藏
     * @param int $user_id 用户ID
     * @param int $works_id 案例ID
     * @return array|false|\PDOStatement|string|Model
     */
    public static function isExists($user_id, $works_id)
    {
        return self::build()->where([
            'user_id' => $user_id,
            'works_id' => $works_id
        ])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 收藏
     * @param array $data 需要插入到数据库的信息
     * @return int|string
     */
    public static function addCollect($data)
    {
        Works::build()->where(['works_id'=>$data['works_id']])->setInc('collect_count');
        return self::build()->insert($data);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 取消收藏
     * @param int $user_id 用户ID
     * @param int $works_id 案例ID
     * @return int
     */
    public static function delCollect($user_id, $works_id)
    {
        Works::build()->where(['works_id'=>$works_id])->setDec('collect_count');
        return self::build()->where([
            'user_id' => $user_id,
            'works_id' => $works_id
        ])->delete();
    }
}
