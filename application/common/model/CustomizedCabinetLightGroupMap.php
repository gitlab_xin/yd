<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetLightGroupMap extends Model
{
    protected $autoWriteTimestamp = false;

    public static function build()
    {
        return new self();
    }
}
