<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedItemClassify extends Model
{
    protected $table = "yd_customized_item_classify";
    protected $pk = 'classify_id';
    protected $createTime = false;
    protected $updateTime = false;

    /**
     * @return CustomizedItemClassify
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getList($where, $withItems = false)
    {
        $result = self::where(array_merge(['is_deleted' => 0], $where))->field('is_deleted', true)->select()->toArray();
        if ($withItems && !empty($result)) {
            $items = CustomizedItem::build()->whereIn('classify_id', array_column($result, 'classify_id'))->select()->toArray();
            foreach ($items as &$item){
                $item['position'] = $item['position'] != '' ? explode(',', $item['position']):[];
            }
            unset($item);
            array_walk($result, function (&$v) use ($items) {
                $classify_id = $v['classify_id'];
                $v['items'] = array_values(array_filter($items, function ($v) use ($classify_id) {
                    return $v['classify_id'] == $classify_id;
                }));
            });
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveClassify($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['classify_id' => $id], true);
    }
}
