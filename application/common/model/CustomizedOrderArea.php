<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/4
 * Time: 15:54
 */
namespace app\common\model;

use think\Model;

class CustomizedOrderArea extends Model
{
    protected $table = "yd_customized_order_area";

    /**
     * @return CustomizedOrderArea
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
