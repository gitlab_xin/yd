<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * 我家-楼盘-层-单元
 */

namespace app\common\model;

use think\Model;

class MyHomeBuildingsUnit extends Model
{
    protected $table = "yd_my_home_buildings_unit";

    /**
     * @return MyHomeBuildingsUnit
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:新增或修改
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function addOrEdit($data, $id = 0)
    {
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            return $userModel = self::where('unit_id', $id)->update($data);
        }
    }
}
