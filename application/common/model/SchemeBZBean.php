<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 */

namespace app\common\model;

use app\common\tools\CommonMethod;

class SchemeBZBean extends SchemeBean
{
    public function mainInfo()
    {
        $result = [
            'search_index' => $this->getSearch_index(),
            'scheme_name' => $this->getScheme_name(),
            'scheme_pic' => $this->getScheme_pic() . '?' . getRandomInt(),

            'scheme_width' => $this->getScheme_width(),
            'scheme_height' => $this->getScheme_height(),
            'scheme_deep' => $this->getScheme_deep(),

            'border_color' => $this->getBorder_color(),
            'handle_color' => $this->getHandle_color(),
            'alloy_color' => $this->getAlloy_color(),
            'material_color' => $this->getMaterial_color(),
            'door_border_color' => $this->getDoor_border_color(),
            'door_alloy_color' => $this->getDoor_alloy_color(),
            'color_level' => $this->getColor_level(),
            'is_monochrome' => $this->getIs_monochrome(),

//            'm_left_mm' => $this->getM_left_mm(),
//            'm_top_mm' => $this->getM_top_mm(),
            'scheme_type' => $this->getScheme_type(),
            'scheme_s_type' => $this->getScheme_s_type(),
            'scheme_color_no' => $this->getScheme_color_no(),
            'scheme_color_name' => $this->getScheme_color_name(),
            'scheme_door_count' => $this->getScheme_door_count(),
            'scheme_door_type' => $this->getScheme_door_type(),
            'scheme_door_color_no' => $this->getScheme_door_color_no(),
//            'scheme_error_range' => $this->getScheme_error_range(),
            'scheme_schemes' => $this->getBzScheme_schemes_array(),
        ];

        return $result;
    }

    public function getBzScheme_schemes_array()
    {
        $result = [];
        $schemeSchemes = $this->getScheme_schemes();
        foreach ($schemeSchemes as $schemeBean) {
            $result[] = [
                'scheme_schemes' => $schemeBean->getScheme_schemes_array($schemeBean->getScheme_schemes()),
                'scheme_wcb_products' => $schemeBean->getScheme_wcb_products_array(),
                'scheme_ncb_products' => $schemeBean->getScheme_ncb_products_array(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_color_no' => $schemeBean->getScheme_color_no()
            ];
        }
        return $result;
    }

    public function getScheme_schemes_array($schemes = null)
    {
        $result = [];
        $schemeSchemes = $schemes == null ? $this->getScheme_schemes() : $schemes;
        foreach ($schemeSchemes as $schemeBean) {
            $result[] = [
                'scheme_products' => $schemeBean->getScheme_products_array(),
                'scheme_no' => $schemeBean->getScheme_no(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_color_no' => $schemeBean->getScheme_color_no(),
                'scheme_b_type' => $schemeBean->getScheme_b_type(),
                'scheme_name' => $schemeBean->getScheme_name(),
//                'm_top_mm' => $schemeBean->getM_top_mm() === null ? 0 : $schemeBean->getM_top_mm(),
//                'm_left_mm' => $schemeBean->getM_left_mm() === null ? 0 : $schemeBean->getM_left_mm(),
            ];
        }
        return $result;
    }

    public function getScheme_dm_schemes_array()
    {
        $result = [];
        foreach ($this->getScheme_dm_schemes() as $schemeBean) {
            $result[] = [
                'scheme_no' => $schemeBean->getScheme_no(),
                'scheme_type' => $schemeBean->getScheme_type(),
                'scheme_width' => $schemeBean->getScheme_width(),
                'scheme_height' => $schemeBean->getScheme_height(),
                'scheme_s_height' => $schemeBean->getScheme_s_height(),
                'scheme_s_width' => $schemeBean->getScheme_s_width(),
            ];
        }
        return $result;
    }

    public static function build($data)
    {
        $bean = new self();
        $bean->setScheme_no(isset($data['scheme_no']) ? $data['scheme_no'] : '');
        $bean->setScheme_name(isset($data['scheme_name']) ? $data['scheme_name'] : '');
        $bean->setScheme_pic(isset($data['scheme_pic']) ? $data['scheme_pic'] : '');

        $bean->setScheme_type(isset($data['scheme_type']) ? $data['scheme_type'] : 'BZ');
        $bean->setScheme_b_type(isset($data['scheme_b_type']) ? $data['scheme_b_type'] : '');
        $bean->setScheme_width($data['scheme_width']);
        $bean->setScheme_height($data['scheme_height']);

        $bean->setM_left_mm(isset($data['m_left_mm']) ? $data['m_left_mm'] : 0);
        $bean->setM_top_mm(isset($data['m_top_mm']) ? $data['m_top_mm'] : 0);
        if (isset($data['scheme_color_no'])) {
            $bean->setScheme_color_no($data['scheme_color_no']);
            $bean->setScheme_color_name(CommonMethod::getSchemeColorName($data['scheme_color_no']));
        }
        $bean->setScheme_door_type(isset($data['scheme_door_type']) ? $data['scheme_door_type'] : 1);
        $bean->setScheme_door_count(isset($data['scheme_door_count']) ? $data['scheme_door_count'] : 0);
        $bean->setScheme_door_color_no(isset($data['scheme_door_color_no']) ? $data['scheme_door_color_no'] : '');

        // wcb
        if (!empty($data['scheme_wcb_products'])) {
            $wcbList = [];

            foreach ($data['scheme_wcb_products'] as $wcbData) {
                $wcbList[] = ProductBean::build($wcbData);
            }
            $bean->setScheme_wcb_products($wcbList);
        }

        // ncb
        if (!empty($data['scheme_ncb_products'])) {
            $ncbList = [];

            foreach ($data['scheme_ncb_products'] as $ncbData) {
                $ncbList[] = ProductBean::build($ncbData);
            }
            $bean->setScheme_ncb_products($ncbList);

        }

        // door
        if (!empty($data['scheme_door_schemes'])) {
            $doorList = [];

            foreach ($data['scheme_door_schemes'] as $doorData) {
                $doorList[] = self::build($doorData);
            }
            $bean->setScheme_door_schemes($doorList);
        }

        // dm
        if (!empty($data['scheme_dm_schemes'])) {
            $dmList = [];

            foreach ($data['scheme_dm_schemes'] as $dmData) {
                $dmList[] = self::build($dmData);
            }
            $bean->setScheme_dm_schemes($dmList);
        }

        //product
        if (!empty($data['scheme_products'])) {
            $schemeProductsList = [];
            foreach ($data['scheme_products'] as $productData) {
                $schemeProductsList[] = ProductBean::build($productData);
            }
            $bean->setScheme_products($schemeProductsList);
        }

        // scheme_schemes
        if (!empty($data['scheme_schemes'])) {
            $schemeSchemesList = [];
            foreach ($data['scheme_schemes'] as $item) {
                $schemeSchemesList[] = self::build($item);

            }
            $bean->setScheme_schemes($schemeSchemesList);
        }

        return $bean;
    }
}
