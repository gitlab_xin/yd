<?php
namespace app\common\model;

use think\Model;

class Order extends Model
{
    protected $table = "yd_order";

    public static function build()
    {
        return new self();
    }

    public function getOrderProductInfoAttr($value){
        return OrderDetail::build()
            ->alias('d')
            ->where(['d.order_id' => $value])
            ->field([
                'd.product_id', 'd.decorate_product_id', 'd.decorate_id', 'd.name', 'd.img', 'd.price',
                'd.from', 'd.type', 'd.count', 'd.standard_name'
            ])
            ->select();
    }
}
