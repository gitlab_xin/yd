<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:48
 */

namespace app\common\model;

use think\Model;
use app\common\model\DemandNotify as NotifyModel;
use app\common\model\Demand as DemandModel;
use app\common\model\User as UserModel;

class DemandScheme extends Model
{
    protected $table = "yd_demand_scheme";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:插入通知表数据
     */
    protected static function init()
    {
        self::afterInsert(function ($model) {
            $where = ['type'=>'push','demand_id'=>$model->demand_id];
            //如果没有该推送消息,则新建一个
            if(($notify = NotifyModel::build()->where($where)->find()) == null){
                $user_id = DemandModel::build()->where(['demand_id'=>$model->demand_id])->value('user_id');
                $username = UserModel::build()->where(['user_id'=>$model->user_id])->value('username');
                $desc = "承接方{$username}为需求投稿";
                NotifyModel::create(['demand_id'=>$model->demand_id,'user_id'=>$user_id,'type'=>'push','desc'=>$desc]);
            }else{//如果有则更新
                $username = UserModel::build()->where(['user_id'=>$model->user_id])->value('username');
                $notify->desc = "承接方{$username}为需求投稿";
                $notify->update_time = time();
                $notify->isUpdate()->save();
            }
        });

        self::afterUpdate(function ($model){
            if($model != null && isset($model->is_accepted) && ($changedData = $model->getChangedData()) && isset($changedData['is_accepted'])){
                switch ($changedData['is_accepted']){
                    case 1://稿件被采纳了
                        /*
                         * 通知被采纳者,你的稿件被采纳
                         * 通知投稿者,你的稿件未被采纳
                         */

                        $data[]= ['demand_id'=>$model->demand_id,'user_id'=>$model->user_id,'type'=>'accepted'
                            ,'desc'=>'恭喜您，需求方已采纳您的稿件'];

                        $user_ids = static::build()->where(['demand_id'=>$model->demand_id,'user_id'=>['neq',$model->user_id]])
                            ->column('distinct user_id');
                        if(!empty($user_ids)){
                            $msg = '需求方已采纳其它稿件';
                            foreach ($user_ids as $user_id){
                                $data[] = ['demand_id'=>$model->demand_id,'user_id'=>$user_id,'type'=>'unaccepted','desc'=>$msg];
                            }
                        }
                        DemandNotify::build()->saveAll($data);
                        break;
                    default:
                        break;
                }
            }
        });
    }


    /**
     * @return DemandScheme
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
