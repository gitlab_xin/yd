<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */
namespace app\common\model;

use think\Model;

class MyHomeFurniture extends Model
{
    protected $table = "yd_my_home_furniture";
    protected $pk = 'furniture_id';

    /**
     * @return MyHomeFurniture
     */
    public static function build()
    {
        return new self();
    }
}
