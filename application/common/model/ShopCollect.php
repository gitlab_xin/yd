<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/20
 * Time: 11:48
 */

namespace app\common\model;

use think\Db;
use think\Model;

class ShopCollect extends Model
{
    protected $table = "yd_shop_collect";
    protected $pk = 'product_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopCollect
     */
    public static function build()
    {
        return new self();
    }

    public static function collect($user_id, $product_id)
    {
        $data['user_id'] = $user_id;
        $data['product_id'] = $product_id;
        $bool = self::build()->where($data)->count();
        if (empty($bool)) {
            $data['create_time'] = time();
            return self::build()->insert($data);
        } else {
            return true;
        }
    }

    public static function cancel($user_id, $product_id)
    {
        $data['user_id'] = $user_id;
        $data['product_id'] = $product_id;
        return self::build()->where($data)->delete();
    }

    public static function check($user_id, $product_id)
    {
        $data['user_id'] = $user_id;
        $data['product_id'] = $product_id;
        return self::build()->where($data)->find();
    }
}