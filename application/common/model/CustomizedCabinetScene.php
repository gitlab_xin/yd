<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetScene extends Model
{
    public static function build()
    {
        return new self();
    }

    public static function saveScene($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['scene_id' => $id], true);
    }
}
