<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedOrder extends Model
{
    protected $table = "yd_customized_order";
    protected $pk = 'order_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    /**
     * @return CustomizedOrder
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
