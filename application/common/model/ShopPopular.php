<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/17
 * Time: 15:29
 */

namespace app\common\model;

use think\Db;
use think\Model;

class ShopPopular extends Model
{
    protected $table = "yd_shop_popular";
    protected $pk = 'popular_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopPopular
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Airon
     * @time: 2017年7月18日
     * description:api
     * @param $limit
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getList($limit)
    {
        $field = [
            'img_src',
            'product_id',
        ];
        return static::build()->field($field)->order('create_time DESC')->limit($limit)->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:建立一对一关系
     * @return \think\model\relation\HasOne
     */
    public function product()
    {
        return $this->hasOne('ShopProduct','product_id','product_id',[],'LEFT');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:添加或更新热门商品
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function savePopular($data,$id = 0)
    {
        if($id == 0){
            return self::create($data,true);
        }else{
            return self::update($data,['popular_id'=>$id],true);
        }
    }
}
