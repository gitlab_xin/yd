<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:18
 */

namespace app\common\model;

use think\Model;

class ActivityApply extends Model
{
    protected $table = "yd_activity_apply";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ActivityApply
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:建立一对一关系(对应活动)
     * @return \think\model\relation\HasOne
     */
    public function activity()
    {
        return $this->hasOne('activity','activity_id','activity_id');
    }
}
