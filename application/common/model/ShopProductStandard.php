<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/21
 * Time: 9:52
 */

namespace app\common\model;


use app\common\model\ShopProductStandard as StandardModel;
use think\Model;

class ShopProductStandard extends Model
{
    protected $table = "yd_shop_product_standard";
    protected $pk = 'standard_id';
    protected $createTime = false;
    protected $updateTime = false;

    /**
     * @return ShopProductStandard
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:建立与商品的一对一关系(一个规格对应一个商品)
     * @return \think\model\relation\HasOne
     */
    public function product()
    {
        return $this->hasOne('shop_product', 'product_id', 'product_id', [], 'LEFT');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:保存或更新规格
     * @param $data
     * @param int $id
     * @return $this
     *
     */
    public static function saveStandard($data, $id = 0)
    {
        if ($data['status'] == '0'){
            $info = self::build()
                ->where(['product_id' => $data['product_id'],'is_deleted' => '0','status' => '1'])
                ->find();
            if (empty($info)){
                $data['status'] = '1';
            }
        }

        if ($data['status'] == '1'){
            StandardModel::build()
                ->where(['product_id' => $data['product_id']])
                ->update([
                    'status' => '0',
                    'update_time' => time()
                ]);
        }

        if ($id == 0){
            return  self::create($data, true);
        } else {
            //同时更新易家整配数据
            MyDecorateProduct::build()
                ->where(['standard_id' => $id])
                ->update([
                    'original_price' => $data['price'],
                    'image' => $data['img'],
                    'update_time' => time(),
                ]);

            return self::update($data, ['standard_id' => $id], true);
        }

    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:获取商品所有规格
     * @param $product_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getProductStandard($product_id)
    {
        return self::build()
            ->where(['product_id' => $product_id, 'is_deleted' => 0])
            ->field(['standard_id', 'name', 'price', 'stocks'])
            ->select();
    }

    public static function check($product_id, $standard_id)
    {
        return self::build()
            ->where(['product_id' => $product_id, 'standard_id' => $standard_id, 'is_deleted' => '0'])
            ->count();
    }
}
