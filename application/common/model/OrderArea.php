<?php
namespace app\common\model;

use think\Model;

class OrderArea extends Model
{
    protected $table = "yd_order_area";

    public static function build()
    {
        return new self();
    }
}