<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */
namespace app\common\model;

use think\Model;

class MyHomeDecorateProgram extends Model
{
    protected $table = "yd_my_home_decorate_program";

    /**
     * @return MyHomeDecorateProgram
     */
    public static function build()
    {
        return new self();
    }
}
