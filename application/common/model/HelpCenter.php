<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 16:54
 */

namespace app\common\model;

use think\Model;

class HelpCenter extends Model
{
    protected $table = "yd_help_center";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return HelpCenter
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:保存或更新
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveHelp($data,$id = 0)
    {
        return $id == 0?self::create($data,true):self::update($data,['id'=>$id],true);
    }
}
