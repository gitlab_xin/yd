<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetCamera extends Model
{
    public static function build()
    {
        return new self();
    }

    public static function saveCamera($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['camera_id' => $id], true);
    }
}
