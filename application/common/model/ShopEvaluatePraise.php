<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/20
 * Time: 16:32
 */

namespace app\common\model;

use think\Model;

class ShopEvaluatePraise extends Model
{
    protected $table = "yd_shop_evaluate_praise";
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopEvaluatePraise
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * 点赞
     * @author: airon
     * @time: 2017年8月7日
     * description: 点赞
     * @param array $data 需要插入到数据库的信息
     * @return int|string
     */
    public static function addPraise($data)
    {
        $data['create_time'] = time();
        ShopEvaluate::build()->where(['evaluate_id'=>$data['evaluate_id']])->setInc('praise_count');
        return self::build()->insert($data);
    }

    /**
     * 取消点赞
     * @author: airon
     * @time: 2017年8月7日
     * description: 取消点赞
     * @param int $user_id 用户ID
     * @param int $evaluate_id 评价ID
     * @return int
     */
    public static function delPraise($user_id, $evaluate_id)
    {
        ShopEvaluate::build()->where(['evaluate_id'=>$evaluate_id])->setDec('praise_count');
        return self::build()->where([
            'user_id' => $user_id,
            'evaluate_id' => $evaluate_id
        ])->delete();
    }

    /**
     * 点赞验证
     * @author: Airon
     * @time: 2017年8月7日
     * description: 验证是否已经点赞
     * @param int $user_id 用户ID
     * @param int $evaluate_id 评价ID
     * @return array|false|\PDOStatement|string|Model
     */
    public static function isExists($user_id, $evaluate_id)
    {
        return self::build()->where([
            'user_id' => $user_id,
            'evaluate_id' => $evaluate_id
        ])->find();
    }
}
