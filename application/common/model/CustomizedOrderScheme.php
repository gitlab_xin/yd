<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/16
 * Time: 14:19
 */

namespace app\common\model;

use think\Model;

class CustomizedOrderScheme extends Model
{
    protected $table = "yd_customized_order_scheme";
    protected $pk = 'id';

    /**
     * @return CustomizedOrderScheme
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
