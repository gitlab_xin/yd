<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Model;

class Demand extends Model
{
    protected $table = "yd_demand";
    protected $pk = 'demand_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    public $closed_type = 'closed';
    public static $sendNotify = [];

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:插入通知表数据
     */
    protected static function init()
    {
        self::afterUpdate(function ($model) {
            if($model != null && isset($model->status) && ($changedData = $model->getChangedData()) && isset($changedData['status'])){
                switch ($changedData['status']){
                    case 2://关闭需求
                        /*
                         * 通知所有投稿者,需求已经关闭了
                         */
                        $username = User::build()->where(['user_id'=>$model->user_id])->value('username');
                        $msg = $model->closed_type == 'closed'?"需求方{$username}关闭了需求":'需求已超时';
                        $user_ids = DemandScheme::build()->where(['demand_id'=>$model->demand_id,'user_id'=>['neq',$model->user_id]])->column('distinct user_id');
                        $user_ids[] = $model->user_id;
                        $data = [];
                        foreach ($user_ids as $user_id){
                            $data[] = ['user_id'=>$user_id,'type'=>$model->closed_type,'demand_id'=>$model->demand_id,'desc'=>$msg];
                        }
                        static::$sendNotify = $data;
                        break;
                    default:
                        break;
                }
            }
        });
    }

    /**
     * @return Demand
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月11日
     * description:批量插入通知
     */
    public static function sendNotify()
    {
        if(static::$sendNotify == []){
            return;
        }
        DemandNotify::build()->saveAll(static::$sendNotify);
        static::$sendNotify = [];
    }

    public static function clearNotify()
    {
        static::$sendNotify = [];
    }
}
