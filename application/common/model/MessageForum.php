<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 17:28
 */

namespace app\common\model;

use think\Model;
use app\common\tools\JPush;

class MessageForum extends Model
{
    protected $table = "yd_message_forum";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:极光推送
     */
    protected static function init()
    {
        self::afterInsert(function ($model) {
            if ($model->user->device_type != null && $model->user->push_id != null) {
                $type = $model['type'] == 1 ? '评论' : '帖子';
                $alert = $model->byUser->username . '回复了你的' . $type . '，快点查看吧！';
                $msg = ['protocol' => 'forum://id=' . $model->article_id];

                $config = config('jpush');
                $push = new JPush($config['AppKey'], $config['MasterSecret']);
                $push->push($model->user->push_id, $model->user->device_type, $alert, $msg);
            }
        });
    }

    /**
     * @return MessageForum
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public function user()
    {
        return $this->hasOne('user', 'user_id', 'user_id', [], 'left');
    }

    public function byUser()
    {
        return $this->hasOne('user', 'user_id', 'by_user_id', [], 'left');
    }
}
