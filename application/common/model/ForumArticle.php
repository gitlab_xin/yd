<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 15:05
 */

namespace app\common\model;

use think\Model;

class ForumArticle extends Model
{
    protected $table = "yd_forum_article";
    protected $pk = 'article_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return ForumArticle
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:建立一对一关系(user表)
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('user', 'user_id', 'user_id');
    }

    /**
     * 发布帖子
     * @author: Jun
     * @time: 2017年7月26日
     * description:用户发布帖子插入到数据库
     * @param $data
     * @return int|string
     */
    public static function addArticle($data)
    {
        return self::build()->insert($data);
    }

    /**
     * 获取帖子详情
     * @author: Jun
     * @time: 2017年7月26日
     * description:入参帖子ID获取帖子详情
     * @param int $article_id 帖子ID
     * @param int $at_user_id
     * @return mixed
     */
    public static function getArticleDetail($article_id, $at_user_id)
    {
        return self::build()->alias('art')->field([
            'art.article_id',
            'art.title',
            'art.classify',
            'art.content',
            'art.user_id',
            'art.scheme_id',
            'art.product_id',
            'art.is_essence',
            'art.is_top',
            'art.is_official',
            'art.praise_count',
            'art.comment_count',
            'art.collect_count',
            'art.create_time',
            'user.username',
            'user.avatar',
            'user.is_designer',
            'IF(ISNULL(praise.id),"no","yes") as is_praise',
            'IF(ISNULL(collect.id),"no","yes") as is_collect',
            'IF(ISNULL(comment.comment_id),"no","yes") as is_comment',
        ])
            ->join('yd_user user', 'user.user_id=art.user_id')// 关联用户表查询用户名和用户头像
            ->join('yd_forum_praise praise',
                'praise.article_id = art.article_id and praise.user_id =' . $at_user_id,
                'left')// 关联用户表查询用户名和用户头像
            ->join('yd_forum_collect collect',
                'collect.article_id = art.article_id and collect.user_id =' . $at_user_id,
                'left')// 关联用户表查询用户名和用户头像
            ->join('yd_forum_comment comment',
                'comment.article_id = art.article_id and comment.parent_comment_id = 0 and comment.user_id =' . $at_user_id,
                'left')// 关联用户表查询用户名和用户头像
            ->where(['art.article_id' => $article_id])
            ->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:保存或编辑
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveArticle($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['article_id' => $id], true);
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:检查文章是否存在
     * @param array $where
     * @return bool
     */
    public function check($where)
    {
        $where['is_deleted'] = 0;
        $data = self::where($where)->find();
        return empty($data) ? false : true;
    }

}
