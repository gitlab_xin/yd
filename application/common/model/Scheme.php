<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\model;

use think\Model;

class Scheme extends Model
{
    protected $table = "yd_scheme";

    /**
     * @return Scheme
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
