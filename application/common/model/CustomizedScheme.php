<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 15:55
 */

namespace app\common\model;

use think\Config;
use think\Model;

class CustomizedScheme extends Model
{
    protected $table = "yd_customized_scheme";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return CustomizedScheme
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getInfo($where, $field)
    {
        return self::build()->where($where)->field($field)->find();
    }

    public function getProductImgAttr($value){
        $img_head = Config::get('qiniu.BucketDomain');

        return $img_head.$value.'?imageView2/0/w/50/h/50';
    }
}
