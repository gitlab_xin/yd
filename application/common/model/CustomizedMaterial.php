<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:01
 */

namespace app\common\model;

use think\Model;

class CustomizedMaterial extends Model
{
    protected $pk = 'material_id';
    protected $table = "yd_customized_material";

    /**
     * @return CustomizedMaterial
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function getName($id)
    {
        $data = self::where(['material_id' => $id])->field(['name'])->find();
        return empty($data) ? false : $data['name'];
    }

    public static function getList(){
        return self::select();
    }

    public static function getIdByName($name)
    {
        return self::where(['name'=>$name])->value('material_id');
    }
}
