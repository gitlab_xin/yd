<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 15:53
 */

namespace app\common\model;

use think\Model;

class UserArea extends Model
{
    protected $table = "yd_user_area";

    /**
     * @return UserArea
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }
}
