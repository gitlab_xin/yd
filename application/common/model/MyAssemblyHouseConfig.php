<?php
namespace app\common\model;

use think\Model;

class MyAssemblyHouseConfig extends Model
{
    protected $table = "yd_my_assembly_house_config";

    public static function build()
    {
        return new self();
    }
}