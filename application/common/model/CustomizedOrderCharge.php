<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedOrderCharge extends Model
{
    protected $table = "yd_customized_order_charge";
    protected $pk = 'id';
    protected $createTime = false;
    protected $updateTime = false;

    /**
     * @return CustomizedOrderCharge
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

}
