<?php
namespace app\common\model;

use think\Model;

class MyAssemblyBuildings extends Model
{
    protected $table = "yd_my_assembly_buildings";

    public static function build()
    {
        return new self();
    }

    public function getHouseCountAttr($value){
        return MyAssemblyHouseType::build()
            ->where(['buildings_id' => $value,'is_deleted' => '0'])
            ->count();
    }

    public function getDecorateCountAttr($value){
        return MyAssemblyHouseDecorate::build()
            ->where(['buildings_id' => $value,'is_deleted' => '0'])
            ->count();
    }

    public static function addOrEdit($data, $id = 0)
    {
        $data['begin_time'] = strtotime($data['begin_time']);
        $data['others_time'] = strtotime($data['others_time']);
        if ($id == 0) {
            //新增
            $data['create_time'] = time();
            return $userModel = self::create($data);
        } else {
            //修改
            unset($data['id']);
            $data['update_time'] = time();
            return $userModel = self::where('buildings_id', $id)->update($data);
        }
    }
}