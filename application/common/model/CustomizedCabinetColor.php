<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\common\model;

use think\Model;

class CustomizedCabinetColor extends Model
{
    protected $table = "yd_customized_cabinet_color";
    protected $pk = 'cabinet_color_id';
    protected $autoWriteTimestamp = false;

    /**
     * @return CustomizedCabinetColor
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function init()
    {
        parent::init();
        self::afterDelete(function ($model){
            CustomizedCabinetDoorColor::build()->where(['cabinet_color_id' => $model->cabinet_color_id])->delete();
        });
    }
}
