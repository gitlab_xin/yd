<?php

namespace app\common\model;

use think\Model;

class CustomizedCabinetControl extends Model
{
    public static function build()
    {
        return new self();
    }

    public static function saveControl($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['control_id' => $id], true);
    }
}
