<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/18
 * Time: 14:21
 */
namespace app\common\model;
use think\Db;
use think\Model;
class ShopAdvertisement extends Model
{
    protected $table = "yd_shop_advertisement";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;
    /**
     * @return ShopAdvertisement
     */
    public static function build()
    {
        return new self();
    }
    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:建立与分类的一对一关系
     * @return \think\model\relation\HasOne
     */
    public function classify()
    {
        return $this->hasOne('shop_classify','classify_id','classify_id',[],'LEFT');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:保存或更新广告
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveAd($data,$id = 0)
    {
        if($id == 0){
            return self::create($data,true);
        }else{
            return self::update($data,['id'=>$id],true);
        }
    }
}