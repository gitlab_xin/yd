<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/27
 * Time: 9:27
 */

namespace app\common\model;

use think\Model;

class News extends Model
{
    protected $table = "yd_news";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return News
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:保存或更新资讯
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveNews($data,$id = 0)
    {
        return $id ==0?self::create($data,true):self::update($data,['id'=>$id],true);
    }
}
