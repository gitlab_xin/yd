<?php
/**
 * Created by PhpStorm.
 * User: rudy
 */

namespace app\common\model;

use think\Model;

class UserBlackList extends Model
{
    protected $table = "yd_user_black_list";
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    /**
     * @return User
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:保存或更新黑名单
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveBlackList($data,$id = 0)
    {
        return $id ==  0?self::create($data,true):self::update($data,['id'=>$id],true);
    }
}
