<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 16:04
 */

namespace app\common\model;

use think\Model;

class ShopOrderLogistics extends Model
{
    protected $table = "yd_shop_order_logistics";
    protected $pk = 'logistics_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @return ShopOrderLogistics
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    public static function checkExists($data)
    {
        return static::build()->field(['logistics_id','order_id','order_type','contact_name','contact_number','logistics_type','logistics_number'])->where($data)->find();
    }

    public function shopOrder()
    {
        return $this->hasOne('shop_order','order_id','order_id',[],'LEFT');
    }
}
