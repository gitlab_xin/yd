<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 17:16
 */

namespace app\common\model;

use think\Model;

class ForumComment extends Model
{
    protected $table = "yd_forum_comment";
    protected $pk = 'comment_id';
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $autoWriteTimestamp = true;

    protected static function init()
    {
        self::afterInsert(function ($model) {
            /*
             * 两种情况下需要进行论坛消息推送
             * 1.有人作为一级评论回复了用户主贴
             * 2.有人在任何帖子下回复了被回复的用户
             * (自己回复自己不算入)
             */
            if(0 == $model->parent_comment_id && 0 == $model->by_user_id && 0 == $model->by_comment_id){
                $article = ForumArticle::build()->field(['user_id'])->where(['article_id'=>$model->article_id])->find();
                //第一种情况,
                if($article->user_id != $model->user_id){//不能自己回复自己
                    $data = ['type'=>0,'article_id' => $model->article_id,'comment_id' => $model->comment_id,
                        'by_user_id' => $model->user_id,'user_id' => $article->user_id];
                    MessageForum::create($data);
                }
            }elseif(0 < $model->by_user_id && $model->by_user_id != $model->user_id){
                //第二种情况
                $data = ['type'=>1,'article_id' => $model->article_id,'comment_id' => $model->comment_id,
                    'by_user_id' => $model->user_id,'user_id' => $model->by_user_id];
                MessageForum::create($data);
            }
        });
    }

    /**
     * @return ForumComment
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:回复主贴者
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('user','user_id','user_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:回复回复者
     * @return \think\model\relation\HasOne
     *
     */
    public function toUser()
    {
        return $this->hasOne('user','user_id','by_user_id');
    }


}
