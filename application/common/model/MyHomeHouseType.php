<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:25
 */

namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class MyHomeHouseType extends Model
{
    protected $table = "yd_my_home_house_type";
    protected $pk = 'house_id';

    /**
     * @return MyHomeHouseType
     */
    public static function build()
    {
        return new self();
    }

    public static function saveHouse($data, $id)
    {
        if ($id == 0) {
            //新增
            $decorate_id_array = $data['decorate_id'];

            unset($data['decorate_id']);
            $data['create_time'] = time();
            Db::startTrans();
            $house_id = self::build()->insertGetId($data);
            if ($house_id === false) {
                Db::rollback();
                return false;
            }
            if (!empty($decorate_id_array) && $house_id !== false) {
                foreach ($decorate_id_array as $key => $value) {
                    $decorate[] = ['decorate_id' => $value, 'house_id' => $house_id];
                }
                $bool = !empty($decorate) ? MyHomeHouseDecorate::build()->insertAll($decorate) : true;
                if ($bool === false) {
                    Db::rollback();
                    return false;
                }
            }
            Db::commit();
            return true;

        } else {
            //修改
            unset($data['id']);
            $decorate_id_array = $data['decorate_id'];
            Db::startTrans();
            try {
                $bool = MyHomeHouseDecorate::build()->where(['house_id' => $id, 'decorate_id' => ['notin', $decorate_id_array]])->delete();
                if ($bool === false) {
                    Db::rollback();
                    return false;
                }
                $array = MyHomeHouseDecorate::build()->where(['house_id' => $id, 'decorate_id' => ['in', $decorate_id_array]])->select()->toArray();
                $hd_id_array = array_column($array, 'decorate_id');
                $decorate_id_array = array_diff($decorate_id_array, $hd_id_array);//得到新增部分的decorate_id

                if (!empty($decorate_id_array) && $id !== false) {
                    foreach ($decorate_id_array as $key => $value) {
                        $decorate[] = ['decorate_id' => $value, 'house_id' => $id];
                    }
                    $bool = !empty($decorate) ? MyHomeHouseDecorate::build()->insertAll($decorate) : true;
                    if (empty($bool)) {
                        Db::rollback();
                        return false;
                    }
                }

                unset($data['decorate_id']);
                $data['update_time'] = time();
                $bool = self::where('house_id', $id)->update($data);
                if (empty($bool)) {
                    Db::rollback();echo 3;
                    return false;
                }
                Db::commit();
                return true;
            } catch (Exception $e) {echo 4;
                Db::rollback();
            }
        }
    }
}
