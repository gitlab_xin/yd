<?php
namespace app\common\model;

use app\common\model\CustomizedColor as ColorModel;
use think\Model;

class MyDecorateProductCart extends Model
{
    protected $table = "yd_my_decorate_product_cart";

    public static function build()
    {
        return new self();
    }

    public static function getColorNameAttr($value, $data){
        if ($data['type'] == '3'){
            return '';
        }

        return ColorModel::build()
            ->where(['color_id' => $value])
            ->value('color_name');
    }

}