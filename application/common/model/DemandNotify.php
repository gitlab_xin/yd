<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/9
 * Time: 17:06
 */

namespace app\common\model;

use think\Model;
use app\common\model\User as UserModel;
use app\common\tools\JPush;

class DemandNotify extends Model
{
    protected $table = "yd_demand_notify";
    protected $pk = 'notify_id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $autoWriteTimestamp = true;

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:极光推送
     */
    protected static function init()
    {
        $func = function ($model) {
            $user = UserModel::build()->field(['device_type', 'push_id'])->where(['user_id' => $model->user_id])->find();
            if ($user != null && $user->device_type != null && $user->push_id != null) {
                $alert = '你有新的通知，快快查看吧！';
                $msg = ['protocol' => 'custom://id=' . $model->demand_id];
                $config = config('jpush');
                $push = new JPush($config['AppKey'], $config['MasterSecret']);
                $push->push($user->push_id, $user->device_type, $alert, $msg);
            }
        };
        self::afterWrite($func);
        self::afterUpdate($func);
    }

    /**
     * @return DemandNotify
     */
    public static function build()
    {
        return new self();
    }

    public static function whereCount($dataMap = [])
    {
        return static::build()->where($dataMap)->count();
    }


}
