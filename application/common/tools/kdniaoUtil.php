<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/31
 * Time: 9:42
 */

namespace app\common\tools;

use think\Exception;

class kdniaoUtil
{
    private $EBusinessID;
    private $AppKey;
    private $ReqURL;

    public function __construct()
    {
        $this->EBusinessID = config('kdniao')['EBusinessID'];
        $this->AppKey = config('kdniao')['AppKey'];
        $this->ReqURL = "http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx";
    }

    /**
     * Json方式 查询订单物流轨迹
     * @param string $express_num
     * @param string $logistic_code
     * @return string|bool|array
     */
    public function getOrderTracesByJson($express_num, $logistic_code)
    {
        try {
            $requestData = "{'OrderCode':'','ShipperCode':'{$express_num}','LogisticCode':'{$logistic_code}'}";

            $data = array(
                'EBusinessID' => $this->EBusinessID,
                'RequestType' => '1002',
                'RequestData' => urlencode($requestData),
                'DataType' => '2',
            );
            $data['DataSign'] = $this->encrypt($requestData, $this->AppKey);
            $result = $this->sendPost($this->ReqURL, $data);

            //根据公司业务处理返回的信息......
            $result = json_decode($result, true);
            if ($result['Success'] === true) {
                return ['State' => $result['State'], 'Traces' => $result['Traces']];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Json方式 单号识别
     * @param string $logistic_code
     * @return string|array|bool
     */
    public function getLogisticCompany($logistic_code)
    {
        try {
            $requestData = "{'LogisticCode':'{$logistic_code}'}";

            $data = array(
                'EBusinessID' => $this->EBusinessID,
                'RequestType' => '2002',
                'RequestData' => urlencode($requestData),
                'DataType' => '2',
            );
            $data['DataSign'] = $this->encrypt($requestData, $this->AppKey);
            $result = $this->sendPost($this->ReqURL, $data);

            //根据公司业务处理返回的信息......

            $result = json_decode($result, true);
            if ($result['Success'] === true) {
                return $result['Shippers'];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     *  post提交数据
     * @author: Airon
     * @time: 2017年月日
     * description:
     * @param  string $url 请求Url
     * @param array $datas 提交的数据
     * @return string url响应返回的html
     */
    public function sendPost($url, $data)
    {
        $temps = array();
        foreach ($data as $key => $value) {
            $temps[] = sprintf('%s=%s', $key, $value);
        }
        $post_data = implode('&', $temps);
        $url_info = parse_url($url);
        if (empty($url_info['port'])) {
            $url_info['port'] = 80;
        }
        $httpheader = "POST " . $url_info['path'] . " HTTP/1.0\r\n";
        $httpheader .= "Host:" . $url_info['host'] . "\r\n";
        $httpheader .= "Content-Type:application/x-www-form-urlencoded;charset=utf-8\r\n";
        $httpheader .= "Content-Length:" . strlen($post_data) . "\r\n";
        $httpheader .= "Connection:close\r\n\r\n";
        $httpheader .= $post_data;
        $fd = fsockopen($url_info['host'], $url_info['port']);
        fwrite($fd, $httpheader);
        $gets = "";
        $headerFlag = true;
        while (!feof($fd)) {
            if (($header = @fgets($fd)) && ($header == "\r\n" || $header == "\n")) {
                break;
            }
        }
        while (!feof($fd)) {
            $gets .= fread($fd, 128);
        }
        fclose($fd);

        return $gets;
    }

    /**
     * 电商Sign签名生成
     * @param string $data 内容
     * @param string $appkey Appkey
     * @return string DataSign签名
     */
    public function encrypt($data, $appkey)
    {
        return urlencode(base64_encode(md5($data . $appkey)));
    }


}