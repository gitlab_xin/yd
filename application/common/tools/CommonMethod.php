<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/25
 * Time: 10:49
 */

namespace app\common\tools;

use app\common\model\CustomizedColor;

class CommonMethod
{
    private static $color_border_handle = [];

    /**
     * @author: Rudy
     * @time: 2017年9月25日
     * description:计算门宽,翻译java代码的,我还没细看,具体逻辑不用管,总之好犀利!
     * @param $holeWidth
     * @param $funitureWidth
     * @param $doorCount
     * @param $doorFrameWidth
     * @param $hasHole
     * @param $hasShouKou
     * @param $isLeftWall
     * @param $isRightWall
     * @return int
     */
    public static function getDoorWidth($holeWidth, $funitureWidth, $doorCount, $doorFrameWidth, $hasHole, $hasShouKou, $isLeftWall, $isRightWall)
    {
        $doorWidth = 0;

        if ($hasHole) {
            if ($hasShouKou) {
                $temp = 0;
                if ($isLeftWall) {
                    $temp += CUSTOM_PADDING;
                }
                $temp += CUSTOM_SHOUKOU_IN_OUT;
                if ($isRightWall) {
                    $temp += CUSTOM_PADDING;
                }
                $temp += CUSTOM_SHOUKOU_IN_OUT;
                $doorWidth = intval(($holeWidth - $temp - ($doorCount + 1) * $doorFrameWidth) / $doorCount) + 2 * $doorFrameWidth;
            } else {
                $doorWidth = intval(($holeWidth - intval(($doorCount + 1) * $doorFrameWidth)) / $doorCount) + 2 * $doorFrameWidth;
            }
        } else {
            $doorWidth = intval(($funitureWidth - 2 * INNER_WIDTH - ($doorCount + 1) * $doorFrameWidth) / $doorCount) + 2 * $doorFrameWidth;
        }

        return $doorWidth;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月25日
     * description:计算门高,翻译java代码的,我还没细看,具体逻辑不用管,总之好犀利!
     * @param $holeHeight
     * @param $furnitureHeight
     * @param $hasHole
     * @param $hasShouKou
     * @return int
     */
    public static function getDoorHeight($holeHeight, $furnitureHeight, $hasHole, $hasShouKou)
    {
        $doorHeight = 0;
        if ($hasHole) {
            $doorHeight = $hasShouKou ? $holeHeight - CUSTOM_SHOUKOU_IN - 40 : $holeHeight - 40;
        } else {
            $doorHeight = $furnitureHeight - 66 - 25 - 40;
        }

        return $doorHeight;
    }

    /**
     * @author: Rudy
     * @time: 2017年12月6日
     * description:获取颜色名称，先从缓存拿 scheme_color_no
     * @param $color_no
     * @return mixed|string
     */
    public static function getSchemeColorName($color_no)
    {
        if (($result = RedisUtil::getInstance()->hGet('scheme_color_no', $color_no)) === false) {

            $result = CustomizedColor::build()->where(['color_no' => $color_no, 'is_deleted' => 0])->value('color_name');
        }

        return $result;
        //    $array = [
//        "灰布纹",
//        "梨木",
//        "浅胡桃",
//        "深胡桃",
//        "暖白",
//        "5",
//        "6",
//        "7",
//        "8",
//        "9",
//        "10",
//        "11",
//        "12",
//        "13",
//        "14",
//        "15",
//        "16",
//        "17",
//        "18",
//        "19",
//        "花色20",
//        "花色21",
//        "花色22",
//        "花色23",
//        "花色24",
//        "花色25",
//        "花色26",
//        "27",
//        "28",
//        "29",
//        "30",
//        "31",
//        "32",
//        "33",
//        "34",
//        "35",
//        "36",
//        "37",
//        "38",
//        "39",
//        "40",
//        "41",
//        "42",
//        "43",
//        "操作色"
//    ];
    }

    public static function getColorDetail($color_no)
    {
        if (!isset(static::$color_border_handle[$color_no])) {

            $result = CustomizedColor::build()->field(['create_time', 'update_time'], true)
                ->where(['color_no' => $color_no, 'is_deleted' => 0])->find();
            if ($result != null) {
                static::$color_border_handle[$color_no] = $result->toArray();
            }
        }

        return static::$color_border_handle[$color_no];
    }

    public static function getPicDir($type, $with_rand = 1)
    {
        $result = "";
        $rd = mt_rand() % 5;
        if ("ZH" == $type) {
            $result = "zh_";
        } elseif ("BZ" == $type) {
            $result = "bz_";
        } else {
            $result = "r_";
        }
        return $result . ($with_rand ? $rd : 0);
    }
}