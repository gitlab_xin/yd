<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/3/20
 * Time: 15:43
 */

namespace app\common\tools;

use JPush\Client;
use JPush\Config;
use JPush\Exceptions\APIConnectionException;
use JPush\Exceptions\APIRequestException;

class JPush
{
    public $appKey = "49e040d2de9bcc20382bb659";
    public $masterSecret = "29b9dd1e326614c29a45468e";
    public $JPush;

    public function __construct($appKey, $masterSecret)
    {
        $this->appKey = !empty($appKey) ? $appKey : $this->appKey;
        $this->masterSecret = !empty($masterSecret) ? $masterSecret : $this->masterSecret;
        $this->JPush = new Client($this->appKey, $this->masterSecret,LOG_PATH.'jpush.log');
    }

    /**
     * @author: Airon
     * @time: 2017年8月21日
     * description:推送方法
     * @param string $push_id 设备ID
     * @param string $device_type 设备类型
     * @param string $alert
     * @param array $message
     * @return array|bool|\Exception|Exception\
     */
    public function push($push_id, $device_type, $alert = "", $message)
    {
        if (empty($push_id) || empty($device_type)) {
            return true;//不存在push_id 则不继续推送。
        }
        try {
            if ($device_type == 'ios') {
                $response = $this->JPush->push()
                    ->setPlatform(array('ios', 'android'))
                    ->addRegistrationId($push_id)
                    ->iosNotification($alert, array(
                        'sound' => 'sound.caf',
                        'badge' => '+1',
                        'content-available' => true,
                        'extras' => $message
                    ))
                    ->options(array(
                        'sendno' => 100,
                        'time_to_live' => 1,
                        'apns_production' => false,
                    ))
                    ->send();
            } else {
                $response = $this->JPush->push()
                    ->setPlatform(array('ios', 'android'))
                    ->addRegistrationId($push_id)
                    ->androidNotification($alert, array(
                        'title' => 'hello',
                        'extras' => $message,
                    ))->options(array(
                        'sendno' => 100,
                        'time_to_live' => 1,
                        'apns_production' => false,
                    ))
                    ->send();
            }
            if ($response['http_code'] == '200') {
                return true;
                //return $response;
            }
            return false;
        } catch (APIConnectionException $e) {
            log_file($e, 'LOG', 'jpush');
            return false;
        } catch (APIRequestException $e) {
            log_file($e, 'LOG', 'jpush');
            return false;
        }
    }

    public function allPush($alert, $message)
    {
        try {
            $response = $this->JPush->push()
                ->setPlatform('all')
                ->addAllAudience()
                ->setNotificationAlert($alert)
                ->message('这里是消息内容', array(
                    'title' => $alert,
                    'extras' => $message,
                ))
                ->options(array(
                    'sendno' => 100,
                    'time_to_live' => 1,
                    'apns_production' => false,
                ))
                ->send();
            if ($response['http_code'] == '200') {
                return true;
                //return $response;
            }
            return false;
        } catch (APIConnectionException $e) {
            log_file($e, 'LOG', 'jpush');
            return false;
        } catch (APIRequestException $e) {
            log_file($e, 'LOG', 'jpush');
            return false;
        }
    }
}