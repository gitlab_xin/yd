<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\tools;

use think\Request;
use app\common\tools\wechatUtil;

class wechatPay
{
    private $appId = '';
    private $mchId = '';
    private $appPayKey = '';
    public $notifyUrl = '';
    public $unifiedOrderUrl = '';
    public $refundUrl = '';
    public $errorMessage = '';
    public $errorDetail;
    public $logPath = '';

    public function __construct($appId, $mchId, $payKey)
    {
        $this->setAppId($appId);
        $this->setMchId($mchId);
        $this->setAppPayKey($payKey);
        $this->notifyUrl = PUBLIC_PATH . '/index.php/api/Callback/wxPay';
        $this->unifiedOrderUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $this->refundUrl = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        $this->logPath = LOG_PATH;
    }

    public function order($outTradeNo, $fee, $prepay_id, $body, $attach = '', $tradeType,$notifyUrl = "")
    {
        if(!empty($notifyUrl)){
            $this->notifyUrl = $notifyUrl;
        }
        $xmlData = $this->getWeChatOrderData($outTradeNo, $fee * 100, $prepay_id, $body, $attach, $tradeType);
        $result = wechatUtil::postCurl($this->unifiedOrderUrl, $xmlData);
        if (empty($result)) {
            $this->setErrorInfo('下单失败', '请求订单失败');
            return false;
        }
        $result = wechatUtil::xmlParser($result);
        if (empty($result)) {
            $this->setErrorInfo('下单失败', '订单解析失败');
            return false;
        }

        if ($result['return_code'] != 'SUCCESS' || $result['result_code'] != 'SUCCESS') {
            $this->setErrorInfo('下单失败', $result['return_msg']);
            return false;
        }

        // build request json, 返回给前端调用支付页面
        $requestData = [
            'appid' => $this->appId,
            'partnerid' => $this->mchId,
            'prepayid' => $result['prepay_id'],
            'package' => 'Sign=WXPay',
            'noncestr' => wechatUtil::getRandomString(32),
            'timestamp' => time(),
            'sign' => "",
        ];
        if($tradeType == 'NATIVE'){
            $requestData['out_trade_no'] = $outTradeNo;
            $requestData['code_url'] = $result['code_url'];
        }
        $requestData['sign'] = wechatUtil::makeSign($requestData, $this->appPayKey);
        return $requestData;
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:退款
     * @param string $outRefundNo 退款单号
     * @param string $outTradeNo 支付单号
     * @param float $total_fee 支付订单总金额
     * @param float $fee 本次退款金额
     * @return bool
     */
    public function refund($outRefundNo, $outTradeNo, $total_fee, $fee)
    {
        $xmlData = $this->getWeChatRefundData($outRefundNo, $outTradeNo, $total_fee * 100, $fee * 100);
        $result = wechatUtil::curl_post_ssl($this->refundUrl, $xmlData);
        if (empty($result)) {
            $this->setErrorInfo('退款失败', '请求退款失败');
            return false;
        }
        $result = wechatUtil::xmlParser($result);
        if (empty($result)) {
            $this->setErrorInfo('退款失败', '退款单解析失败');
            return false;
        }
        if ($result['return_code'] != 'SUCCESS') {
            $this->setErrorInfo('退款失败', $result['return_msg']);
            return false;
        }
        if ($result['return_code'] != 'SUCCESS' || $result['result_code'] != 'SUCCESS') {
            $this->setErrorInfo('退款失败', $result['return_msg']);
            return false;
        }
        return true;
    }

    public function handleNotify($requestStr)
    {
        $requestArray = wechatUtil::xmlParser($requestStr);

        if (empty($requestArray) || !isset($requestArray['return_code'])) {
            wechatUtil::log_file($requestArray, 'parse error', 'wxpay', $this->logPath);
            return false;
        }
        if ($requestArray['return_code'] != 'SUCCESS') {
            wechatUtil::log_file($requestArray, 'return code error', 'wxpay', $this->logPath);
            return false;
        }

        $realSign = wechatUtil::makeSign($requestArray, $this->appPayKey);

        if ($realSign != $requestArray['sign']) {
            // sign fail
            wechatUtil::log_file($requestArray, 'sign fail:' . $realSign, 'wxpay', $this->logPath);
            return false;
        }

        if ($requestArray['result_code'] != 'SUCCESS') {
            // pay fail
            wechatUtil::log_file($requestArray, 'result is fail', 'wxpay', $this->logPath);
            return false;
        }

        return $requestArray;
    }

    private function getWeChatOrderData($outTradeNo, $fee, $prepay_id, $body, $attach, $tradeType)
    {
        $orderData = [
            'appid' => $this->appId, // 公众账号ID
            'mch_id' => $this->mchId, // 商户号
            'device_info' => 'WEB',
            'nonce_str' => wechatUtil::getRandomString(32), // 随机字符串
            'sign' => '',
            'sign_type' => 'MD5',
            'body' => $body,
            'attach' => '' . $attach, // 自定义参数
            'out_trade_no' => $outTradeNo, // 商户系统内部订单号
            'fee_type' => 'CNY',
            'total_fee' => $fee,
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'notify_url' => $this->notifyUrl,
            'trade_type' => $tradeType,
            'prepay_id' => $prepay_id,
        ];
        if ($orderData['spbill_create_ip'] == '::1') {
            $orderData['spbill_create_ip'] = '127.0.0.1';
        }
        $orderData['sign'] = wechatUtil::makeSign($orderData, $this->appPayKey);
        $result = wechatUtil::toXml($orderData);
        return $result;
    }

    private function getWeChatRefundData($out_refund_no, $outTradeNo, $total_fee, $fee)
    {
        $orderData = [
            'appid' => $this->appId, // 公众账号ID
            'mch_id' => $this->mchId, // 商户号
            'op_user_id' => $this->mchId,// 商户号
            'nonce_str' => wechatUtil::getRandomString(32), // 随机字符串
            'sign' => '',
            'sign_type' => 'MD5',
            'out_trade_no' => $outTradeNo, // 商户系统内部订单号
            'out_refund_no' => $out_refund_no, // 商户系统内部退款单号
            'fee_type' => 'CNY',
            'total_fee' => $total_fee,//订单金额
            'refund_fee' => $fee,//退款金额
            'refund_account' => "REFUND_SOURCE_RECHARGE_FUNDS",//退款资金来源 默认为未结算资金 当前设置为余额资金
        ];
        $orderData['sign'] = wechatUtil::makeSign($orderData, $this->appPayKey);
        $result = wechatUtil::toXml($orderData);
        return $result;
    }

    public function setErrorInfo($message, $detail)
    {
        $this->errorMessage = $message;
        $this->errorDetail = $detail;
    }

    /**
     * @param string $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @param string $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @param string $mchId
     */
    public function setMchId($mchId)
    {
        $this->mchId = $mchId;
    }

    /**
     * @param string $appPayKey
     */
    public function setAppPayKey($appPayKey)
    {
        $this->appPayKey = $appPayKey;
    }

}
