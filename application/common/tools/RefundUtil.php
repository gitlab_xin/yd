<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/26
 * Time: 15:30
 */

namespace app\common\tools;

use think\Config;

class RefundUtil
{
    private $payType = '';//支付类型
    private $randomNum = '';//随机码
    private $billNum = '';//订单号
    private $fee = '';//单笔费用
    private $sum = '';//订单总额

    private function __construct()
    {
        if($this->randomNum === ''){
            $this->randomNum = wechatUtil::getRandomString(32);//获取随机的号码
        }
    }

    public static function getInstance()
    {
        return new self();
    }

    public function setRandomNum($str)
    {
        $this->randomNum = $str;
        return $this;
    }

    public function setPayType($str)
    {
        $this->payType = $str;
        return $this;
    }

    public function setBillNum($str)
    {
        $this->billNum = $str;
        return $this;
    }

    public function setFee($str)
    {
        $this->fee = $str;
        return $this;
    }

    public function setSum($str)
    {
        $this->sum = $str;
        return $this;
    }

    public function setProps(array $arr)
    {
        foreach ($arr as $name => $value){
            if(property_exists($this,$name)){
                $this->$name = $value;
            }
        }
        return $this;
    }

    public function getRandomNum()
    {
        return $this->randomNum;
    }

    public function exec()
    {
        return $this->{'refundBy'.ucfirst($this->payType)}();
    }

    private function refundByWechat()
    {
        $wechatConfig = Config::get('wechat');
        $appId = $wechatConfig['AppID'];
        $mchId = $wechatConfig['Pay']['MchId'];
        $appKey = $wechatConfig['Pay']['Key'];
        $wechatPay = new wechatPay($appId, $mchId, $appKey);
        //todo $total_fee 为总金额 测试阶段 为 0.01*订单内商品数
        $res = $wechatPay->refund($this->randomNum,$this->billNum,$this->sum,$this->fee);

        return $res;
    }

    private function refundByAlipay()
    {
        $alipayConfig = Config::get('alipay');
        $appId = $alipayConfig['AppID'];
        $partner_public_key = $alipayConfig['partnerPublicKey'];
        $partner_private_key = $alipayConfig['rsaPrivateKey'];
        $alipay_public_key = $alipayConfig['alipayPublicKey'];
        $alipayClient = new alipayClient($appId,$partner_private_key,$partner_public_key,$alipay_public_key);
        $requestData =  $alipayClient->refund($this->billNum,$this->fee,$this->randomNum,"订单已取消");

        return $requestData;
    }

    private function refundByBank($order)
    {
        return false;
    }
}