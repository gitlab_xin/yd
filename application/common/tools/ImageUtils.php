<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\common\tools;

use app\common\model\SchemeBean;
use Qiniu\Auth;
use function Qiniu\base64_urlSafeEncode;
use Qiniu\Storage\UploadManager;

class ImageUtils
{
    public function createRQSSchemeBasic(SchemeBean $scheme, $path, $hashcode)
    {

        $d_width = intval($scheme->getScheme_width() / 8);
        $d_height = intval($scheme->getScheme_height() / 8);

        $image = imagecreatetruecolor($d_width, $d_height);
        $c = imagecolorallocatealpha($image, 0, 0, 0, 127); //拾取一个完全透明的颜色
        imagealphablending($image, false); //关闭混合模式，以便透明颜色能覆盖原画布
        imagefill($image, 0, 0, $c); //填充
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息

        $fileName = '';
        $s_left = 0;

        foreach ($scheme->getScheme_wcb_products() as $p) {
            if (empty($p->getM_left())) {
                $s_left = $p->getProduct_s_width();
            }
            $fileName = $path . PRO_PIC . $p->getProduct_pic();
            $img = $this->getImageResource($fileName);
            imagecopy($image, $img, $p->getM_left(), $p->getM_top(), 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
        }

        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left(), $p->getM_top(), 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }

        for ($i = 0; $i < count($scheme->getScheme_schemes()); $i++) {
            $t_s = $scheme->getScheme_schemes()[$i];
            if ($i > 0) {
                $s_left += intval(($scheme->getScheme_schemes()[$i - 1]->getScheme_width() + 25) / 8);
            }
            foreach ($t_s->getScheme_products() as $p) {
                if ($p->getProduct_type() != 'ZB') {
                    continue;
                }
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left() + $s_left, $p->getM_top(), 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
            foreach ($t_s->getScheme_products() as $p) {
                if ($p->getProduct_type() == 'ZB') {
                    continue;
                }
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);

                // todo: png图片缩放后会糊掉
                imagecopy($image, $img, $p->getM_left() + $s_left, $p->getM_top(), 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息
        $srcImageFile = SCHEME_PIC . $hashcode . '.png';
        imagepng($image, $srcImageFile);
        $this->saveToQiniu($srcImageFile, $hashcode);
    }

    public function createQWSchemeBasic(SchemeBean $scheme, $path, $hashcode)
    {
        $d_width = intval($scheme->getScheme_width() / 8);
        $d_height = intval($scheme->getScheme_height() / 8);

        $t = 0;
        if (!empty($scheme->getScheme_dg_products())) {
            $t = intval(484 / 8);
            $d_height += $t;
        }

        $image = imagecreatetruecolor($d_width, $d_height);
        $c = imagecolorallocatealpha($image, 0, 0, 0, 127); //拾取一个完全透明的颜色
        imagealphablending($image, false); //关闭混合模式，以便透明颜色能覆盖原画布
        imagefill($image, 0, 0, $c); //填充
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息

        $fileName = '';
        $s_left = 0;

        foreach ($scheme->getScheme_wcb_products() as $p) {
            if (empty($p->getM_left())) {
                $s_left = $p->getProduct_s_width();
            }
            $fileName = $path . PRO_PIC . $p->getProduct_pic();
            $img = $this->getImageResource($fileName);
            imagecopy($image, $img, $p->getM_left(), $p->getM_top() + $t, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
        }

        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                if (empty($p->getM_left())) {
                    $s_left = $p->getProduct_s_width();
                }
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left(), $p->getM_top(), 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }
        if ($scheme->getScheme_have_door() == 1) {
            if (!empty($scheme->getScheme_door_schemes())) {
                for ($i = 0; $i < count($scheme->getScheme_door_schemes()); $i++) {
                    $s = $scheme->getScheme_door_schemes()[$i];
                    $d_left = 2;
                    $d_left += ($i * $s->getScheme_s_width()) - $i * 2;
                    $fileName = $path . PRO_PIC . $s->getScheme_pic() . "";
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $d_left, $s->getM_top() + $t + 8, 0, 0, $s->getProduct_s_width(), $s->getProduct_s_height());
                }
            }
        } else {

            if (!empty($scheme->getScheme_ncb_products())) {
                foreach ($scheme->getScheme_ncb_products() as $p) {
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left(), $p->getM_top() + $t, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
                }
            }
            for ($i = 0; $i < count($scheme->getScheme_schemes()); $i++) {
                $t_s = $scheme->getScheme_schemes()[$i];
                if ($i > 0) {
                    $s_left += intval(($scheme->getScheme_schemes()[$i - 1]->getScheme_width() + 25) / 8);
                }
                foreach ($t_s->getScheme_products() as $p) {
                    if ($p->getProduct_type() != 'ZB') {
                        continue;
                    }
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left() + $s_left, $p->getM_top() + $t, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
                }
                foreach ($t_s->getScheme_products() as $p) {
                    if ($p->getProduct_type() == 'ZB') {
                        continue;
                    }
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    // todo: png图片缩放后会糊掉
                    imagecopy($image, $img, $p->getM_left() + $s_left, $p->getM_top() + $t, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());

                }
            }
        }


//        header("Content-type: image/png");
//        imagepng($image);exit;
        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left(), $p->getM_top() + $t, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息
        $srcImageFile = SCHEME_PIC . $hashcode . '.png';
        imagepng($image, $srcImageFile);
        $this->saveToQiniu($srcImageFile, $hashcode);
    }

    public function createZHGSchemeBasic(SchemeBean $scheme, $path, $showDoor = false, $showAcc = false, $hashcode = null)
    {
        $hdiff = intval(25 / 8);
        $d_width = intval($scheme->getScheme_width() / 8);
        $d_height = intval($scheme->getScheme_height() / 8) + $hdiff;

        $image = imagecreatetruecolor($d_width, $d_height);
        $c = imagecolorallocatealpha($image, 0, 0, 0, 127); //拾取一个完全透明的颜色
        imagealphablending($image, false); //关闭混合模式，以便透明颜色能覆盖原画布
        imagefill($image, 0, 0, $c); //填充
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息

        $fileName = '';
        $start = 0;
        $s_left = 0;
        //外侧板
        $w = $b = $n = 0;
        $s_left_1 = $s_left_2 = $s_left_3 = 0;//每8快内测版会多1的距离
        foreach ($scheme->getScheme_wcb_products() as $p) {
            if ($p->getM_left() == 0) {
                $s_left = $p->getProduct_s_width();
                $start = $p->getProduct_s_width();
            }
            if($w == 1) {
                $s_left_1 = 1;
            }
            $w = $w + 1;
            $fileName = $path . PRO_PIC . $p->getProduct_pic();
            $img = $this->getImageResource($fileName);
            imagecopy($image, $img, $p->getM_left() - $s_left_1, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
        }

        //内侧板
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                if($n >= 7) {
                    $s_left_2 = 1;
                }
                $n = $n + 1;
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left() - $s_left_2, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }


        if (!empty($scheme->getScheme_g_products())) {
            foreach ($scheme->getScheme_g_products() as $p) {
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left(), $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
        }

        for ($i = 0; $i < count($scheme->getScheme_schemes()); $i++) {
            $t_s = $scheme->getScheme_schemes()[$i];
            if ($i > 0) {
                $s_left += floatval(($scheme->getScheme_schemes()[$i - 1]->getScheme_width() + 25) / 8);
            }
            if($i >= 7) {
                $s_left_3 = 1;
            }
            //画背板
            foreach ($t_s->getScheme_products() as $p) {
                if ($p->getProduct_type() != 'DB') {
                    continue;
                }
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $p->getM_left() + $s_left - $s_left_3, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
            // 画除了门和抽屉、桌子以外的组件
            foreach ($t_s->getScheme_products() as $p) {
                if ($p->getProduct_type() == 'DB') {
                    continue;
                }
                if (strpos($p->getProduct_type(), 'CT') !== false) {
                    continue;
                }
                if ($p->getProduct_type() == 'ZZ') {
                    continue;
                }
                if ($p->getProduct_type() == 'DD' && $p->getProduct_pic() == '') {
                    continue;
                }
                $fileName = $path . PRO_PIC . $p->getProduct_pic();
                $img = $this->getImageResource($fileName);

                // todo: png图片缩放后会糊掉
                imagecopy($image, $img, $p->getM_left() + $s_left - $s_left_3, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
            }
            if ($showAcc) {
                foreach ($t_s->getScheme_products() as $p) {
                    if ($p->getProduct_type() == "DB") {
                        continue;
                    }
                    if (strpos($p->getProduct_type(), 'CT') !== false) {
                        continue;
                    }
//                    if ($p->getProduct_type() == "ZZ") {
//                        continue;
//                    }
                    if ($p->getAcc_products() != null && count($p->getAcc_products()) > 0) {
                        foreach ($p->getAcc_products() as $a_p) {
                            $a_p = change2ProductBean($a_p);
                            $fileName = $path . PRO_PIC . $a_p->getProduct_pic();
                            $img = $this->getImageResource($fileName);
                            list($srcWidth, $srcHeight) = getimagesize($fileName);

                            // 缩放 创建新图
                            $newWidth = $a_p->getProduct_s_width();
                            $newHeight = $a_p->getProduct_s_height();
                            $newImg = imagecreatetruecolor($newWidth, $newHeight);

                            imagesavealpha($newImg, true);
                            $alpha = imagecolorallocatealpha($newImg, 0, 0, 0, 127);
                            imagefill($newImg, 0, 0, $alpha);

                            // 将源图拷贝到新图上，并设置在保存 PNG 图像时保存完整的 alpha 通道信息
                            imagecopyresampled($newImg, $img, 0, 0, 0, 0, $newWidth, $newHeight, $srcWidth, $srcHeight);
                            imagealphablending($image, true);

                            imagecopy($image, $newImg, $a_p->getM_left() + $s_left - $s_left_3, $a_p->getM_top() + $hdiff, 0, 0, $a_p->getProduct_s_width(), $a_p->getProduct_s_height());
                            $newImg = null;
                            $img = null;
                        }
                    }
                    if ($p->getProduct_type() == "ZZ") {
                        $fileName = $path . PRO_PIC . $p->getProduct_pic();
                        $img = $this->getImageResource($fileName);
                        imagecopy($image, $img, $p->getM_left() + $s_left - $s_left_3,
                            $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                            $p->getProduct_s_height());
                    }

                }
            }
            if ($showDoor) {
                // 画抽屉和门
                foreach ($t_s->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    if (strpos($p->getProduct_type(), 'CT') === false) {
                        continue;
                    }
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left() + $s_left, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
                }
                if ($t_s->getScheme_door_schemes() != null) {
                    foreach ($t_s->getScheme_door_schemes() as $d_scheme) {
                        $d_scheme = new SchemeBean();
                        $fileName = $path . PRO_PIC . $d_scheme->getScheme_pic();
                        $img = $this->getImageResource($fileName);
                        imagecopy($image, $img, $p->getM_left() + $s_left - $s_left_3, $p->getM_top() + $hdiff, 0, 0, $p->getProduct_s_width(), $p->getProduct_s_height());
                    }
                }
            }

        }

//        header("Content-type: image/png");
        imagesavealpha($image, true);
//        imagepng($image);exit;
        $srcImageFile = SCHEME_PIC . $hashcode . '.png';

        imagepng($image, $srcImageFile);
        $this->saveToQiniu($srcImageFile, $hashcode);
    }

    public function createRQSSchemeWithData($schemeData, $srcImageFile, $path)
    {
        $d_width = intval($schemeData['scheme_width'] / 8);
        $d_height = intval($schemeData['scheme_height'] / 8);

        $image = imagecreatetruecolor($d_width, $d_height);
        $c = imagecolorallocatealpha($image, 0, 0, 0, 127); //拾取一个完全透明的颜色
        imagealphablending($image, false); //关闭混合模式，以便透明颜色能覆盖原画布
        imagefill($image, 0, 0, $c); //填充
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息

        $fileName = '';

        $productList = $schemeData['product_list'];
        foreach ($productList as $key => $value) {
            foreach ($value as $productData) {
                $fileName = $path . PRO_PIC . $productData['product_pic'];
                $img = $this->getImageResource($fileName);
                imagecopy($image, $img, $productData['x_px'], $productData['y_px'], 0, 0, $productData['s_width_px'], $productData['s_height_px']);
            }
        }

        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息
        imagepng($image, $srcImageFile);
        $this->saveToQiniu($srcImageFile);
    }

    public function createBZSchemeAll(SchemeBean $scheme, $path, $hashcode)
    {
        $d_width = intval($scheme->getScheme_width() / 8);
        $d_height = intval($scheme->getScheme_height() / 8);

        $image = imagecreatetruecolor($d_width, $d_height);
        $c = imagecolorallocatealpha($image, 0, 0, 0, 127); //拾取一个完全透明的颜色
        imagealphablending($image, false); //关闭混合模式，以便透明颜色能覆盖原画布
        imagefill($image, 0, 0, $c); //填充
        imagesavealpha($image, true); //设置保存PNG时保留透明通道信息

        $fileName = '';
        $total_left = 0;

        foreach ($scheme->getScheme_schemes() as $scheme_i) {
            $scheme_i = change2SchemeBean($scheme_i);
            $s_left = $total_left;
            $total_left += intval($scheme_i->getScheme_width() / 8);
            //外侧板
            if ($scheme_i->getScheme_wcb_products() != null) {
                foreach ($scheme_i->getScheme_wcb_products() as $p) {
                    $p = change2ProductBean($p);
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left() + $s_left,
                        $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                        $p->getProduct_s_height());
                }
            }

            //内侧版
            if ($scheme_i->getScheme_ncb_products() != null) {
                foreach ($scheme_i->getScheme_ncb_products() as $p) {
                    $p = change2ProductBean($p);
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left() + $s_left,
                        $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                        $p->getProduct_s_height());
                }
            }

            $s_left += $scheme_i->getScheme_wcb_products()[0]->getProduct_s_width();
            foreach ($scheme_i->getScheme_schemes() as $col) {
                $col = change2SchemeBean($col);
                //背板
                foreach ($col->getScheme_products() as $p) {
                    $p = change2ProductBean($p);

                    if ($p->getProduct_type() != 'MB') {
                        continue;
                    }
                    $fileName = $path . PRO_PIC . $p->getProduct_pic();
                    $img = $this->getImageResource($fileName);
                    imagecopy($image, $img, $p->getM_left() + $s_left,
                        $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                        $p->getProduct_s_height());


                }


                //其他
                foreach ($col->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    if ($p->getProduct_type() == 'MB') {
                        continue;
                    }
                    if ($p->getProduct_type() == 'DD' && $p->getProduct_pic() == '') {
                        continue;
                    }
                    if ($p->getProduct_type() == 'ZZ') {

                        if ($p->getAcc_products() != null) {
                            foreach ($p->getAcc_products() as $a_p) {
                                $a_p = change2ProductBean($a_p);

                                $fileName = $path . PRO_PIC . $a_p->getProduct_pic();
                                $img = $this->getImageResource($fileName);
                                imagecopy($image, $img, $a_p->getM_left() + $s_left,
                                    $a_p->getM_top(), 0, 0, $a_p->getProduct_s_width(),
                                    $a_p->getProduct_s_height());
                            }
                        }
                        $fileName = $path . PRO_PIC . $p->getProduct_pic();
                        $img = $this->getImageResource($fileName);
                        imagecopy($image, $img, $p->getM_left() + $s_left,
                            $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                            $p->getProduct_s_height());
                    } else {
                        $fileName = $path . PRO_PIC . $p->getProduct_pic();
                        $img = $this->getImageResource($fileName);
                        imagecopy($image, $img, $p->getM_left() + $s_left,
                            $p->getM_top(), 0, 0, $p->getProduct_s_width(),
                            $p->getProduct_s_height());

//                        if ($p->getCom_products() != null) {
//                            foreach ($p->getCom_products() as $c) {
//                                $c = change2ProductBean($c);
//
//                                $fileName = $path . PRO_PIC . $c->getProduct_pic();
//                                $img = $this->getImageResource($fileName);
//                                imagecopy($image, $img, $c->getM_left() + $s_left,
//                                    $c->getM_top(), 0, 0, $c->getProduct_s_width(),
//                                    $c->getProduct_s_height());
//                            }
//                        }
                        if ($p->getAcc_products() != null) {
                            foreach ($p->getAcc_products() as $a_p) {
                                $a_p = change2ProductBean($a_p);

                                $fileName = $path . PRO_PIC . $a_p->getProduct_pic();
                                $img = $this->getImageResource($fileName);
                                imagecopy($image, $img, $a_p->getM_left() + $s_left,
                                    $a_p->getM_top(), 0, 0, $a_p->getProduct_s_width(),
                                    $a_p->getProduct_s_height());
                            }
                        }
                    }


                }

                $s_left += intval(($col->getScheme_width() + 25) / 8);
            }
        }


//        header("Content-type: image/png");
        imagesavealpha($image, true);
        $srcImageFile = SCHEME_PIC . $hashcode . '.png';
        imagepng($image, $srcImageFile);
//        exit;

        $this->saveToQiniu($srcImageFile, $hashcode);
    }

    private function saveToQiniu($srcImageFile, $hashcode = null)
    {
        $fileName = pathinfo($srcImageFile, PATHINFO_BASENAME);
        $upManager = new UploadManager();
        $qnConfig = config('qiniu');
        $auth = new Auth($qnConfig['AccessKey'], $qnConfig['SecretKey']);
        $token = $auth->uploadToken($qnConfig['BucketName'], $fileName);
        $uploadResult = $upManager->putFile($token, $fileName, $srcImageFile);
        $key = $uploadResult[0]['key'];
        if (empty($key)) {
            # 上传失败
            $response = $uploadResult[1]->getResponse();
            if ($response->statusCode == 614 && $hashcode != null) {//重复
                RedisUtil::getInstance()->sAdd('scheme_pic', $hashcode);
            }
            return false;
        }
        if ($hashcode != null) {
            RedisUtil::getInstance()->sAdd('scheme_pic', $hashcode);
        }
//        $url = $qnConfig['BucketDomain'] . $key;
    }

    private function getImageResource($fileName)
    {
        list(, , $type) = getimagesize($fileName);

        switch ($type) {
            case 2://JPG
                return imagecreatefromjpeg($fileName);
            case 3://PNG
                return imagecreatefrompng($fileName);
            default:
                return null;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月29日
     * description:
     * @param $srcImageFile 原图地址s
     * @param $result 目标地址
     * @param $x 目标切片起点坐标X
     * @param $y 目标切片起点坐标Y
     * @param $width 目标切片宽度
     * @param $height 目标切片高度
     */
    public function cut($srcImageFile, $result, $x, $y, $width, $height)
    {
//        dump($result);exit;
        if (file_exists($result)) {
            return;
        }
        $dts_image = imagecreatetruecolor($width, $height);
        $src_image = $this->getImageResource($srcImageFile);

        imagecopyresampled($dts_image, $src_image, 0, 0, $x, $y, $width, $height, $width, $height);
//        header("Content-type: image/jpeg");
        imagejpeg($dts_image, $result);
//        exit;
    }
}
