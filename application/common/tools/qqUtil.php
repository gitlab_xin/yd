<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/9/8
 * Time: 10:30
 */

namespace app\common\tools;

class qqUtil
{
    public static function getWebAccessToken($appId, $secret, $code, $redirect_uri)
    {
        if (empty($appId) || empty($secret) || empty($code)) {
            return false;
        }
        $url = "https://graph.qq.com/oauth2.0/token?"
            . "grant_type=authorization_code&client_id={$appId}&client_secret={$secret}&code={$code}&redirect_uri={$redirect_uri}";
        $result = self::getCurl($url);
        return $result;

    }

    public static function getWebUserInfo($accessToken, $openId)
    {
        if (empty($accessToken) || empty($openId)) {
            return false;
        }
        $appId = config('qq')['WebAppID'];
        $url = "https://graph.qq.com/user/get_user_info?"
            . "access_token={$accessToken}&oauth_consumer_key={$appId}&openid={$openId}&format=json";
        $result = self::getCurl($url);
        return $result;
    }

    public static function getOpenId($access_token)
    {
        if (empty($access_token)) {
            return false;
        }
        $url = "https://graph.qq.com/oauth2.0/me?"
            . "access_token={$access_token}&unionid=1";
        $result = self::getCurl($url);
        return $result;
    }

    public static function getAccessToken($appId, $appSecret)
    {
        if (empty($appId) || empty($appSecret)) {
            return false;
        }
        $url = "https://api.weixin.qq.com/cgi-bin/token"
            . "?grant_type=client_credential&appid={$appId}&secret={$appSecret}";
        $result = self::getCurl($url);
        return $result;
    }

    public static function validateResult($result)
    {
        $validate_result = str_replace('callback( ', "", $result);
        $validate_result = str_replace(' );', "", $validate_result);
        $validate_result = str_replace("\n", "", $validate_result);
        if (!empty(json_decode($validate_result, true)) && !empty(json_decode($validate_result, true)['error'])) {
            return false;
        }
        return $validate_result;
    }

    public static function convertUrlQuery($query)
    {
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1];
        }
        return $params;
    }

    // get方法请求
    public static function getCurl($url)
    {
        try {
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($curlHandle);
            curl_close($curlHandle);
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    // post方法请求
    public static function postCurl($url, $postData)
    {
        try {
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postData);
            $result = curl_exec($curlHandle);
            curl_close($curlHandle);
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

}
