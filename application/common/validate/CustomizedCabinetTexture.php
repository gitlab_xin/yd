<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetTexture extends FormView
{
    protected $rule = [
        'type' => 'require|max:32',
        'name' => 'require|max:32',
        'belong' => 'require|max:32',
        'url' => 'require|max:255',
        'material' => 'require|max:32',
    ];

    protected $field = [
        'type' => '类型',
        'name' => '名称',
        'belong' => '从属',
        'url' => '图片地址',
        'material' => '材质类型',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['type', 'name', 'belong', 'url', 'material']
    ];

    protected $input_type = [
        'type' => 'text',
        'name' => 'text',
        'belong' => 'text',
        'url' => 'image',
        'material' => 'text',
    ];
}