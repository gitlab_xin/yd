<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;

use think\Validate;

class OfficialLogistics extends Validate
{
    protected $rule = [
        'content' => 'require|max:30',
    ];

    protected $field = [
        'content' => '物流信息',
    ];

    protected $message = [
    ];

    protected $scene = [
        'add' => ['content'],
    ];
}