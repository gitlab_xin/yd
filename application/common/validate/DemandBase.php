<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;

use think\Validate;
use app\common\model\Demand as DemandModel;

class DemandBase extends Validate
{
    public function __construct(array $rules = [], array $message = [], array $field = [])
    {
        $this->rule = array_merge($this->getBaseRules(), $this->rule, $rules);
        $this->message = array_merge($this->getBaseMessages(), $this->message, $message);
        $this->field = array_merge($this->getBaseFields(), $this->field, $field);
    }

    public function getBaseRules()
    {
        return [
            'title' => 'require|max:40',
            'content' => 'require|max:200',
            'cost_min' => 'require|float',
            'cost_max' => 'require|float|gt:cost_min',
            'img_src_list' => 'max:255',
            'user_id' => 'require|integer|checkTodayCount',
            'env_width' => 'require|integer|between:0,9999',
            'env_height' => 'require|integer|between:0,3000',
        ];
    }

    public function getBaseFields()
    {
        return [
            'title' => '需求标题',
            'content' => '需求说明',
            'cost_min' => '成本区间最小值',
            'cost_max' => '成本区间最大值',
            'img_src_list' => '环境图片',
            'user_id' => '发布者',
            'env_width' => '环境宽度',
            'env_height' => '环境高度',
        ];
    }

    public function getBaseMessages()
    {
        return [
            'cost_max.gt' => ':attribute必须大于最小值',
            'user_id.checkTodayCount' => '今日发布需求已达上限',
        ];
    }

    public function checkTodayCount($value)
    {
        $current = time();
        $begin_time = strtotime(date('Y-m-d', $current));//当天0点时间戳
        $where['create_time'] = ['between', [$begin_time, $current]];
        $where['user_id'] = $value;
        $where['status'] = ['neq', 0];
        $count = DemandModel::build()->where($where)->count();
        //todo 发布限制,目前未999,正式环境为10
        return $count <= config('demand.daily_max_limit') ? true : false;
    }
}