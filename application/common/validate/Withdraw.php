<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 17:53
 */

namespace app\common\validate;

use think\Validate;

class Withdraw extends Validate
{
    protected $rule = [
        'money' => 'require|integer|egt:100|elt:9999',
        'account_name' => 'require|max:10',
        'bank' => 'require|max:20',
        'card_num' => 'require|number|regex:\d{16,19}',
        'type' => 'require|in:withdraw_increase,withdraw_reduce,money_reward',
        'status' => 'in:1,2',
        'remark' => 'max:180',
        'withdraw_id'=>'require|number|regex:\d{1,10}'
    ];

    protected $field = [
        'money' => '提现金额',
        'account_name' => '持卡人',
        'bank' => '银行',
        'card_num' => '银行卡号',
        'type' => '流水类型',
        'status' => '审核状态'
    ];

    protected $message = [
        'type' => '没有符合:attribute的类型'
    ];

    protected $scene = [
        'withdraw' => ['money','account_name','bank','card_num'],
        'flow' => ['type'],
        'changeStatus' => ['status','remark'],
        'detail' => ['withdraw_id']
    ];

}