<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetRender extends FormView
{
    protected $rule = [
        'name' => 'require|max:20',
        'shadowMapType' => 'require|max:32',
        'toneMapping' => 'require|max:32',
        'physicallyCorrectLights' => 'require|in:0,1',
        'gammaInput' => 'require|in:0,1',
        'gammaOutput' => 'require|in:0,1',
        'shadowMapSoft' => 'require|in:0,1',
        'shadowMapEnabled' => 'require|in:0,1',
        'envMap' => 'require|in:0,1',
        'size' => 'require|max:1024|checkJson',
        'clearColor' => 'require|max:1024|checkJson',
    ];

    protected $field = [
        'name' => '名称',
        'shadowMapType' => '阴影类型',
        'toneMapping' => '色调映射',
        'physicallyCorrectLights' => '物理正确光',
        'gammaInput' => 'gamma输入',
        'gammaOutput' => 'gamma输出',
        'shadowMapSoft' => '阴影贴图柔软度',
        'shadowMapEnabled' => '阴影启用',
        'envMap' => '环境贴图',
        'size' => '尺寸',
        'clearColor' => '重置色',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'shadowMapType', 'toneMapping', 'physicallyCorrectLights', 'gammaInput', 'gammaOutput',
            'shadowMapSoft', 'shadowMapEnabled', 'envMap', 'size', 'clearColor'
        ]
    ];

    protected $input_type = [
        'name' => 'text',
        'shadowMapType' => 'text',
        'toneMapping' => 'text',
        'physicallyCorrectLights' => 'radio|0=否&1=是|0',
        'gammaInput' => 'radio|0=否&1=是|0',
        'gammaOutput' => 'radio|0=否&1=是|0',
        'shadowMapSoft' => 'radio|0=否&1=是|0',
        'shadowMapEnabled' => 'radio|0=否&1=是|0',
        'envMap' => 'radio|0=否&1=是|0',
        'size' => 'textArea',
        'clearColor' => 'textArea',
    ];
}