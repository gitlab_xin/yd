<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetLight extends FormView
{
    protected $rule = [
        'name' => 'require|max:20',
        'type' => 'require|in:AmbientLight,DirectionalLight,PointLight,HemisphereLight,SpotLight,RectAreaLight',
    ];

    protected $field = [
        'name' => '名称',
        'type' => '类型',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'type']
    ];

    protected $input_type = [
        'name' => 'text',
//        'type_bat' => 'radio|AmbientLight=环境光&DirectionalLight=平行光&PointLight=点光|AmbientLight',
        'type' => 'radio|AmbientLight=环境光&DirectionalLight=平行光&PointLight=点光&HemisphereLight=半球光&SpotLight=聚光灯&RectAreaLight=矩形面光|AmbientLight',
    ];
}