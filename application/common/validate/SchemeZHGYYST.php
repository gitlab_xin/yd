<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/28
 * Time: 20:27
 */

namespace app\common\validate;

class SchemeZHGYYST extends SchemeBase
{
    protected $scheme_type = 'ZHYYST';

    protected $rule = [
        'width' => 'require|integer|checkWidth:s_type',
        's_type' => 'in:0,1',//0:侧搭式 1:嵌入式
        'error_range' => 'in:50,100,150',
        'tv_size' => 'require|in:32,36,38,40,42,46,50,52,54,56',
        'tv_type' => 'in:GS,TS'
    ];

    protected $field = [
        'width' => '宽度',
        's_type' => '组合形式',
        'error_range' => '误差值',
        'tv_size' => '电视规格',
        'tv_type' => '摆放形式'
    ];

    protected $message = [
    ];

    protected $scene = [
        'math' => ['width', 's_type', 'tv_size'],
        'create' => ['width', 'tv_type', 'tv_size', 's_type', 'error_range'],
        'store' => ['scheme_name', 'scheme_pic', 'width', 'tv_type'],
        'update' => ['scheme_name', 'scheme_pic', 'width', 'tv_type', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];

    public function checkWidth($value, $rule, $data)
    {
        $val = $this->getDataValue($data, $rule);
        if ($val == 0) {
            self::$typeMsg['checkWidth'] = ':attribute只能在 1140 - 2900 之间';
            return $value <= 2900 && $value >= 1140;
        } else {
            self::$typeMsg['checkWidth'] = ':attribute只能在 1643 - 6000 之间';
            return $value <= 6000 && $value >= 1643;
        }


    }
}