<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;
use app\admin\widget\FormView as FormWidget;

class FormView extends Validate
{

    public function getFieldsByScene($scene)
    {
        $fields = $this->scene[$scene];
        $result = $this->field;
        $input_type = $this->input_type;

        $arr = array_filter($result, function ($k) use ($fields) {
            return in_array($k, $fields);
        }, ARRAY_FILTER_USE_KEY);

        return empty($arr) ? $arr : array_map(function ($v) use ($input_type, $result) {
            $arr = explode('|', $input_type[$v]);
            $ret = new FormWidget($v, $result[$v], $arr[0]);
            if (isset($arr[1])) {
                parse_str($arr[1], $params);
                $ret->input_params = $params;
                $ret->default_value = $arr[2];
            }
            return $ret;
        }, array_flip($arr));
    }

    public function getAllInput($scene)
    {
        $arr = $this->getFieldsByScene($scene);
        return !empty($arr) ? implode('', array_map(function ($model) {
            return $model->defaultInput();
        }, $arr)) : '';
    }

    protected function checkJson($v)
    {
        json_decode($v);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}