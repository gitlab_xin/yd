<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/24
 * Time: 15:22
 */

namespace app\common\validate;

use think\Validate;
use app\common\model\CustomizedScheme;

class SchemeBase extends Validate
{

    protected $scheme_type_map = ['RQS' => '入墙式移门衣柜', 'QW' => '墙外独立移门衣柜', 'BZ' => '平开门标准衣柜', 'YM' => '移门定制',
        'ZHCWSN' => '书柜和综合柜类', 'ZHSJSC' => '综合柜书桌', 'ZHYYST' => '电视柜类','ZHBDGD' => '壁挂吊柜','YMJ'=>'衣帽间'];

    public function __construct(array $rules = [], array $message = [], array $field = [])
    {
        $this->rule = array_merge($this->getBaseRules(), $this->rule, $rules);
        $this->message = array_merge($this->getBaseMessages(), $this->message, $message);
        $this->field = array_merge($this->getBaseFields(), $this->field, $field);
    }

    public function getBaseRules()
    {
        return [
            'scheme_id' => 'require|integer',
            'user_id' => 'require|integer|checkScheme:scheme_id',
            'scheme_name' => 'require|max:64',
            'scheme_pic' => 'require|max:100',
            'scheme_color_no' => 'require|in:000,001,002,003,004,005,006,007,008,009,010,011,012,013,014,015,016,017,018,019,020,021,022,023,024,025,026',
        ];
    }

    public function getBaseFields()
    {
        return [
            'scheme_id' => '方案id',
            'user_id' => '用户id',
            'scheme_name' => '方案名称',
            'scheme_pic' => '方案图片',
            'scheme_color_no' => '花色',
        ];
    }

    public function getBaseMessages()
    {
        return [
        ];
    }

    public function checkScheme($value, $rule, $data)
    {
        if (($scheme_type = CustomizedScheme::build()->where(['id' => $this->getDataValue($data, $rule), 'user_id' => $value])->value('CONCAT(scheme_type,scheme_b_type)')) === null) {
            $this->message('user_id.checkScheme', '该方案不属于您');
            return false;
        } elseif ($scheme_type != $this->scheme_type) {
            $this->message('user_id.checkScheme', '该方案类型不属于' . $this->scheme_type_map[$this->scheme_type]);
            return false;
        } else {
            return true;
        }
    }
}