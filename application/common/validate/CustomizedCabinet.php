<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedCabinet extends Validate
{
    protected $rule = [
        'cabinet_id' => 'require|integer|gt:0',
        'name' => 'require|max:20',
        'type' => 'require|max:20|unique:customized_cabinet',
        'scheme_type' => 'require|max:20',
        'color_no' => 'require|max:5'
    ];

    protected $field = [
        'cabinet_id' => 'id',
        'name' => '柜体名称',
        'type' => '柜体类型',
        'scheme_type' => '柜体类型',
        'color_no' => '颜色编号',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'type'],
        'getCabinetColorList' => ['scheme_type'],
        'getDoorColorList' => ['scheme_type','color_no'],
        'getColorDetail' => ['color_no'],
    ];

}