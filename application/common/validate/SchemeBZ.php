<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/24
 * Time: 15:22
 */

namespace app\common\validate;

use app\common\model\CustomizedCabinet;

class SchemeBZ extends SchemeBase
{
    protected $scheme_type = 'BZ';
    /*
     * 成人衣柜只有一个单元的柜体 16 + 424/880/944 + 16
     * 学生衣柜有一种两单元柜体 16 + 368 + 25 + 752 + 16
     *          一种一单元柜体 16 + 752 + 16
     *
     * 写字桌只能现代风格和高度2019有值
     */
    protected $rule = [
        'width' => 'require|integer|between:456,5016',
        'height' => 'require|integer|in:2019,2403',//柜体高度:2019:学生 2403:成人
        'color_no' => 'require',
//        'color_no' => 'require|checkColor:BZ',
        'type' => 'require|in:0,1|checkType',//风格:0简欧 1现代
        'table_type' => 'require|in:-1,0,1|checkHeight|checkWidth',//写字桌:-1 没有桌子 0 垂直 1 平行
        'doorType' => 'require|in:0,1,2|checkDoorType',//门的类型 0平板门 1简欧门 2合金门
        'doorDirection' => 'require|in:-1,0,1|checkDoorDirection',//门把手方向 0右开 1左开
    ];

    protected $field = [
        'width' => '宽度',
        'height' => '高度',
        'color_no' => '花色风格',
        'type' => '外观风格',
        'table_type' => '写字桌',
        'doorType' => '门类型',
        'doorDirection' => '门把手方向'
    ];

    protected $message = [
        'table_type.checkHeight' => '请选择现代风格和学生衣柜才能选择写字桌',
        'table_type.checkWidth' => '有桌子的情况下，宽度必须是1698,1954,2100,2356',
        'type.checkType' => '欧式风格只能选择成人柜子',
        'doorType.checkDoorType' => '简欧门要求：白色花色，简欧风格;合金门要求：非白色花色，现代风格',
        'doorDirection.checkDoorDirection' => '学生柜时，门把手方向请指定为-1',
    ];

    protected $scene = [
        'create' => ['width', 'height', 'type', 'table_type'],
        'store' => ['width', 'height', 'color_no', 'type', 'table_type', 'scheme_name', 'doorType', 'doorDirection', 'scheme_pic'],
        'update' => ['width', 'height', 'color_no', 'type', 'table_type', 'scheme_name', 'doorType', 'doorDirection', 'scheme_pic', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:检测桌子是否选定了现代风格和学生衣柜
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkHeight($value, $rule, $data)
    {
        if ($value >= 0 && !($data['height'] == 2019 && $data['type'] == 1)) {//如果选择了写字桌,则代表风格一定是现代,以及高度是2019
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月19日
     * description:检查宽度是多少
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkWidth($value, $rule, $data)
    {
        if ($value >= 0 && !in_array($data['width'], [1698, 1954, 2100, 2356])) {//如果选择了写字桌,宽度必须在这几个里面
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月20日
     * description:检查风格是多少
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkType($value, $rule, $data)
    {
        return $value == 0 && $data['height'] == 2019 ? false : true;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月20日
     * description:检测门
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkDoorType($value, $rule, $data)
    {
        if ($value == 1 && ($data['height'] != 2403 || $data['color_no'] != 4 || $data['type'] == 1)) {//简欧门要求：白色，简欧，成人柜
            return false;
        } elseif ($value == 2 && ($data['height'] != 2403 || $data['color_no'] == 4 || $data['type'] == 0)) {//合金门要求：非白色，成人柜
            return false;
        } elseif ($value == 0 && $data['type'] == 0) {
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月8日
     * description:检测门把手
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkDoorDirection($value, $rule, $data)
    {
        if ($value != -1 && $data['height'] == 2019) {//学生柜
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月8日
     * description:检测柜体颜色
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkColor($value, $rule, $data)
    {
        $color_col = CustomizedCabinet::build()->alias('ca')->where(['ca.type' => $rule, 'co.is_deleted' => 0])
            ->join('customized_cabinet_color cc', 'ca.cabinet_id = cc.cabinet_id', 'left')
            ->join('customized_color co', 'cc.color_id = co.color_id', 'left')
            ->column('co.color_no');
        if (!in_array($value, $color_col)) {
            $this->message('color_no.checkColor', '柜体颜色必须在' . implode(',', $color_col) . '内');
            return false;
        } else {
            return true;
        }
    }
}