<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

//class CustomizedCabinetScene extends Validate
//修复不能创建场景==========>Neear
class CustomizedCabinetScene extends FormView
{
    protected $rule = [
        'scene_id' => 'require|integer|gt:0',
        'data' => 'require',
        'type' => 'require|in:camera,lightGroup,render,orbitCtrl',
        'name' => 'require|max:10',
        'size_x' => 'require|number|length:0,5',
        'size_y' => 'require|number|length:0,5',
        'size_z' => 'require|number|length:0,5',
    ];

    protected $field = [
        'scene_id' => 'id',
        'data' => '数据',
        'type' => '柜体类型',
        'scheme_type' => '柜体类型',
        'color_no' => '颜色编号',
        'name' => '场景名称',
        'size_x' => '房间x轴',
        'size_y' => '房间y轴',
        'size_z' => '房间z轴',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'size_x', 'size_y', 'size_z'],
        'update' => ['scene_id', 'data', 'type'],
        'get' => ['scene_id', 'type']
    ];

    protected $input_type = [
        'name' => 'text',
        'size_x' => 'text',
        'size_y' => 'text',
        'size_z' => 'text',
    ];
}