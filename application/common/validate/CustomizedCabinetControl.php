<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetControl extends FormView
{
    protected $rule = [
        'name' => 'require|max:20',
        'type' => 'require|max:20',
        'camera' => 'require|max:20',
        'render' => 'require|max:20',
        'enable' => 'require|in:0,1',
        'open' => 'require|max:1024|checkJson',
    ];

    protected $field = [
        'name' => '名称',
        'type' => '类型',
        'camera' => '相机',
        'render' => '渲染器',
        'open' => '范围限制',
        'enable' => '是否启用',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'type', 'camera', 'render', 'enable', 'open']
    ];

    protected $input_type = [
        'name' => 'text',
        'type' => 'text',
        'camera' => 'text',
        'render' => 'text',
        'enable' => 'radio|0=否&1=是|1',
        'open' => 'textArea',
    ];
}