<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/25
 * Time: 15:47
 */

namespace app\common\validate;

use think\Validate;

class DemandPrivateMessage extends Validate
{
    protected $rule = [
        'demand_id' => 'require|number|regex:\d{1,10}',
        'user_id' => 'number|regex:\d{1,10}',
        'status' => 'require|in:master,guest',
        'type' => 'require|in:image,text',
        'content' => 'require|max:200',
        'at_user_id' => 'number|regex:\d{1,10}',
        'last_message_id' => 'number|regex:\d{1,10}',
    ];

    protected $field = [
        'demand_id' => '需求id',
        'user_id' => '用户id',
        'status' => '发送者类型',
        'type' => '私信类型',
        'content' => '内容',
    ];

    protected $message = [
    ];

    protected $scene = [
        'send' => ['demand_id', 'user_id', 'content','type','content'],
        'get' => ['demand_id'],
        'detail' => ['demand_id','at_user_id','last_message_id']
    ];

}