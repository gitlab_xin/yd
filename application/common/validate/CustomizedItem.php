<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;
use think\Config;

class CustomizedItem extends Validate
{
    protected $rule = [
        'id' => 'require|integer|notIn:0',
        'name' => 'require|max:255',
        'thumb_img_src' => 'require|max:255',
        'item_img_src' => 'require|max:255',
        'width' => 'require|integer|between:0,9999',
        'deep' => 'require|integer|between:0,9999',
        'height' => 'require|integer|between:0,9999',
        'classify_id' => 'require|integer|gt:0',
        'type' => 'require|checkType',
        'position' => 'array|checkPosition',
        'is_stack' => 'require|in:yes,no',
    ];

    protected $field = [
        'name' => '物品名称',
        'thumb_img_src' => '物品缩略图',
        'item_img_src' => '物品图片(背景透明)',
        'width' => '物品实际宽度(mm)',
        'deep' => '物品实际深度(mm)',
        'height' => '物品实际高度(mm)',
        'classify_id' => '物品类型ID',
        'type' => '堆放类型',
        'position' => '物品位置',
        'is_stack' => '是否层叠',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'thumb_img_src', 'item_img_src', 'width', 'deep', 'height', 'classify_id', 'type', 'is_stack','position']
    ];

    protected function checkType($value)
    {
        return in_array($value, array_keys(Config::get('customized.item_type')));
    }

    protected function checkPosition($array)
    {
        foreach ($array as $v) {
            if (in_array($v, array_keys(Config::get('customized.item_position'))) == false) {
                return false;
            }
        }
        return true;
    }
}