<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/27
 * Time: 11:27
 */

namespace app\common\validate;

use think\Validate;

class News extends Validate
{
    protected $rule = [
        'news_id' => 'require|number|gt:0',
        'title' => 'require|max:20',
        'image' => 'require|max:64',
        'synopsis' => 'max:60',
        'content' => 'require|max:500000'
    ];

    protected $field = [
        'title' => '资讯标题',
        'synopsis' => '资讯副标题',
        'image' => '资讯图片',
        'content' => '资讯详情',
    ];

    protected $message = [
        'image' => '请上传:attribute',
        'news_id' => 'ID参数不符合'
    ];

    protected $scene = [
        'getInfo' => ['news_id'],
        'addOrEdit' => ['title', 'synopsis', 'image', 'content'],
    ];
}