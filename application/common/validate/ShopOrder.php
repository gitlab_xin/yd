<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 11:33
 */

namespace app\common\validate;

use think\Validate;

class ShopOrder extends Validate
{
    protected $rule = [
        'status' => 'require|in:all,unpaid,preparing,delivering,toevaluate,refunded',
        'order_id' => 'require|number|regex:\d{1,10}',
        'reason' => 'require|max:20',
        'keyword' => 'require|max:20',
        'content' => 'max:255'
    ];

    protected $field = [
        'status' => '订单状态',
        'order_id' => '订单id',
        'keyword' => '关键字'
    ];

    protected $message = [
    ];

    protected $scene = [
        'list' => ['status'],
        'search' => ['status','keyword'],
        'detail' => ['order_id'],
        'refund' => ['order_id','content','reason']
    ];
}