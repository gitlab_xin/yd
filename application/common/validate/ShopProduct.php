<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/17
 * Time: 11:34
 */

namespace app\common\validate;

use think\Validate;

class ShopProduct extends Validate
{
    protected $rule = [
        'product_id' => 'require|number',
        'standard_id' => 'require|number',
        'name' => 'require|max:30',
        'classify_one_id' => 'require|number|notin:0',
        'classify_two_id' => 'require|number',
        'classify_three_id' => 'require|number',
        'supplier_id' => 'number',
        'original_price' => 'require|float',
        'real_price' => 'require|float',
        'sales' => '',
        'stocks' => 'require|number',
        'product_classify' => '',
        'freight_charge' => 'require|number|regex:\d{1,4}(\.\d{1})?',
        'processing_charge' => 'float',
        'service_charge' => 'float',
        'home_charge' => 'float',
        'is_hot' => 'require|in:0,1',
        'is_new' => 'require|in:0,1',
        'classify_level' => 'require|in:one,two,three',
        'classify_id' => 'require|number',
        'order_type' => 'require|in:composite,sales,price,hot',
        'sort' => 'require|in:DESC,ASC',
        'collect_type' => 'require|in:collect,cancel',
        'user_id' => 'require|number',
        'province' => 'require|notin:0',
        'city' => 'require|notin:0',
        'count' => 'number',
        'evaluate_type' => 'require|in:all,well,mediocre,bad,image',
        'praise_type' => 'require|in:praise,cancel',
        'evaluate_id' => 'require|number',
        'min_price' => 'number',
        'max_price' => 'number',
        'img_src_list' => 'require',
        'product_ids' => 'require|regex:(\d+(,?))+',
//        'content' => ''
    ];

    protected $field = [
        'name' => '商品名称',
        'classify_one_id' => '一级分类',
        'classify_two_id' => '二级分类',
        'classify_three_id' => '三级分类',
        'brand_id' => '品牌id',
        'original_price' => '原价格',
        'real_price' => '真实价格',
        'stocks' => '库存量',
        'freight_charge' => '运费',
        'processing_charge' => '加工费',
        'service_charge' => '服务费',
        'home_charge' => '上门费',
        'is_hot' => '是否热门',
        'is_new' => '是否新品',
        'supplier_id' => '商家',
        'province' => '省份',
        'city' => '城市',
        'img_src_list' => '轮播图片',
        'product_ids' => '商品ids',
        'content' => '商品详情'
    ];

    protected $message = [
        'product_id' => '商品ID参数不符合',
        'standard_id' => 'ID参数不符合',
        'collect_type' => '操作类型有误',
        'classify_one_id.number' => '请选择:attribute',
        'classify_one_id.notin' => '请选择:attribute',
        'classify_two_id.number' => '请选择:attribute',
        'classify_three_id.number' => '请选择:attribute',
        'supplier_id.number' => ':attribute必须为数字',
        'classify_id' => '分类ID参数不符合',
        'classify_level' => '分类等级参数不符合',
        'order_type' => '排序类型不正确',
        'sort' => '排序必须为正序或倒序',
        'user_id' => '用户ID参数不符合',
        'is_hot.require' => '请选择:attribute',
        'is_hot.in' => '请选择:attribute',
        'province.require' => '请选择:attribute',
        'province.notin' => '请选择:attribute',
        'city.require' => '请选择:attribute',
        'city.notin' => '请选择:attribute',
        'evaluate_type' => '类型有误',
        'praise_type' => '类型有误',
        'evaluate_id' => '评价ID不能为空',
        'product_ids.regex' => ':attribute请用,(小写逗号)进行字符串拼接',
        'freight_charge.regex' => ':attribute必须是千位数,最小单位为毫'
    ];

    protected $scene = [
        'addOrEdit' => [
            'name',
            'classify_one_id',
            'classify_two_id',
            'classify_three_id',
            'is_hot',
            'is_new',
            'freight_charge',
            'processing_charge',
            'service_charge',
            'home_charge',
            'supplier_id',
            'province',
            'city',
            'img_src_list',
            'content'
        ],
        'classifyProduct' => ['classify_id', 'classify_level', 'order_type', 'sort'],
        'searchByKeyword' => ['order_type', 'sort','supplier_id','min_price','max_price'],
        'searchWebByKeyword' => ['order_type', 'sort', 'supplier_id', 'classify_id', 'classify_level'],
        'collect' => ['product_id', 'collect_type'],
        'product_id' => ['product_id'],
        'product_info' => ['product_id', 'user_id'],
        'changePriceRecord' => ['product_id', 'standard_id'],
        'count' => ['count'],
        'evaluateType' => ['product_id', 'evaluate_type', 'user_id'],
        'evaluatePraise' => ['evaluate_id', 'praise_type'],
        'batchCancel' => ['product_ids']
    ];
}