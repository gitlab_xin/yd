<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 16:35
 */

namespace app\common\validate;


use think\Validate;

class ShopAdvertisement extends Validate
{
    protected $rule = [
        'classify_id' => 'require|number|notIn:0',
        'related' => 'require|number|regex:\d{1,10}',
        'img_src' => 'require|max:64',
    ];

    protected $field = [
        'classify_id' => '分类',
        'related' => '相关链接',
        'img_src' => '图片',
    ];

    protected $message = [
        'classify_id.notIn'=>'请选择:attribute',
        'img_src.require'=>'请上传:attribute',
    ];

    protected $scene = [
        'addOrEdit' => [ 'classify_id','related','img_src'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false){
        if(isset($this->rule[$key]) && is_string($value)){
            $append ? $this->rule[$key] = $this->rule[$key].$value:$this->rule[$key] = $value;
        }
        return $this;
    }
}