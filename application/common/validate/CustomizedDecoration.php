<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

class CustomizedDecoration extends FormView
{
    protected $rule = [
        'decoration_name' => 'require|max:20',
    ];

    protected $field = [
        'decoration_name' => '装饰名称',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['decoration_name']
    ];

    protected $input_type = [
        'decoration_name' => 'text',
    ];
}