<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 */

namespace app\common\validate;

use think\Validate;

class UserBlackList extends Validate
{
    protected $rule = [
        'ban_begin_time' => 'require|date',
        'ban_end_time' => 'require|date|gt:ban_begin_time'
    ];

    protected $field = [
        'ban_begin_time' => '封号开始时间',
        'ban_end_time' => '封号结束时间'
    ];

    protected $message = [
        'ban_end_time.gt' => ':attribute必须晚于封号结束时间',
        'ban_end_time.after' => ':attribute必须晚于当前时间',
    ];

    protected $scene = [
        'ban_time' => ['ban_begin_time','ban_end_time']
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false){
        if(isset($this->rule[$key]) && is_string($value)){
            $append ? $this->rule[$key] = $this->rule[$key].$value:$this->rule[$key] = $value;
        }
        return $this;
    }
}
