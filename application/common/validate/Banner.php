<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:25
 */

namespace app\common\validate;

use think\Validate;

class Banner extends Validate
{
    protected $rule = [
        'device_type' => 'in:web,app',
        'module_type' => 'in:store,home,forum,case,activity,demand',
        'img_src' => 'require|max:64',
        'related' => 'require|url|number'
    ];

    protected $field = [
        'device_type' => '设备类型',
        'module_type' => '模块类型',
        'img_src' => '轮播图',
        'related' => '跳转类型',
    ];

    protected $message = [
        'device_type' => ':attribute不正确',
        'device_type.require' => ':attribute不能为空',
        'module_type' => ':attribute不正确',
        'module_type.require' => ':attribute不能为空',
        'img_src.require' => '请上传:attribute',
        'related.regex' => ':attribute长度不能超过11'
    ];

    protected $scene = [
        'list' => ['device_type', 'module_type'],
        'addOrEdit' => ['device_type', 'module_type','img_src','related'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false)
    {
        if (isset($this->rule[$key]) && is_string($value)) {
            $append ? $this->rule[$key] = $this->rule[$key] . $value : $this->rule[$key] = $value;
        }
        return $this;
    }
}
