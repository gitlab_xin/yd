<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class WebglScene extends Validate
{
    protected $rule = [
        'id' => 'require|number|notIn:0',
        'name' => 'require|max:10'
    ];

    protected $field = [
        'name' => '场景名称'
    ];

    protected $message = [
    ];

    protected $scene = [
        'item' => ['classify_id'],
        'addOrEdit' => ['name']
    ];

}