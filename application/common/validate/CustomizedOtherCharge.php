<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/15
 * Time: 16:57
 */

namespace app\common\validate;

use think\Validate;

class CustomizedOtherCharge extends Validate
{
    protected $rule = [
        'freight_charge' => 'require|number|regex:\d{1,4}(\.\d{1})?',
        'processing_charge' => 'require|number|regex:\d{1,4}(\.\d{1})?',
        'home_charge' => 'require|number|regex:\d{1,4}(\.\d{1})?',
        'service_charge' => 'require|number|regex:\d{1,4}(\.\d{1})?',
    ];

    protected $field = [
        'freight_charge' => '运费',
        'processing_charge' => '安装费用',
        'home_charge' => '上楼费用',
        'service_charge' => '其他费用',
    ];

    protected $message = [
        'freight_charge.regex' => ':attribute必须是千位数,最小单位为毫',
        'processing_charge.regex' => ':attribute必须是千位数,最小单位为毫',
        'home_charge.regex' => ':attribute必须是千位数,最小单位为毫',
        'service_charge.regex' => ':attribute必须是千位数,最小单位为毫',
    ];

    protected $scene = [
        'edit' => ['freight_charge', 'processing_charge', 'home_charge','service_charge'],
    ];

}