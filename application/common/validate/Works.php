<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 11:23
 */

namespace app\common\validate;

use think\Validate;
use app\common\model\CustomizedMaterial as MaterialModel;

class Works extends Validate
{
    protected $rule = [
        'audit_remarks' => 'max:255',
        'type' => 'in:0,1,2',
        'works_id' => 'require|number|regex:\d{1,10}',
        'at_user_id' => 'number|regex:\d{1,10}',
        'praise_type' => 'require|in:praise,cancel',
        'collect_type' => 'require|in:collect,cancel',
        'sort' => 'in:recommended,time',
        'title' => 'require|max:10',
        'content' => 'require|max:500000',
        'scene_src' => 'require|max:255|checkLimit',
        'origin_scheme_id' => 'require|number|regex:\d{1,10}',
        'keyword' => 'max:20',
        'scheme_type' => 'in:ALL,RQS,QW,BZ,YYST',
        'material_name' => 'checkMaterial',
        'scheme_hole_width_min' => 'number|between:1182,5000|requireLt:scheme_hole_width_max',
        'scheme_hole_width_max' => 'number|between:1182,5000',
        'scheme_hole_sl_height_min' => 'number|between:2036,2900|requireLt:scheme_hole_sl_height_max',
        'scheme_hole_sl_height_max' => 'number|between:2036,2900',
    ];

    protected $field = [
        'audit_remarks' => '备注',
        'type' => '审核状态',
        'works_id' => '案例ID',
        'at_user_id' => '登录用户ID',
        'praise_type' => '点赞类型',
        'collect_type' => '收藏类型',
        'sort' => '排序类型',
        'title' => '标题',
        'content' => '说明',
        'scene_src' => '实景图',
        'origin_scheme_id' => '方案id',
        'keyword' => '关键字',
        'scheme_type' => '分类',
        'material_name' => '花色',
        'scheme_hole_width_min' => '洞口宽度最小值',
        'scheme_hole_width_max' => '洞口宽度最大值',
        'scheme_hole_sl_height_min' => '移门上梁高度最小值',
        'scheme_hole_sl_height_max' => '移门上梁高度最大值',
    ];

    protected $message = [
        'material_name' => ':attribute不存在'
    ];

    protected $scene = [
        'changeType' => ['audit_remarks', 'type'],
        'detail' => ['works_id', 'at_user_id'],
        'praise' => ['works_id', 'praise_type'],
        'collect' => ['works_id', 'collect_type'],
        'list' => ['sort', 'at_user_id', 'keyword', 'scheme_type', 'material_name',
            'scheme_hole_width_min', 'scheme_hole_width_max', 'scheme_hole_sl_height_min', 'scheme_hole_sl_height_max'],
        'add' => ['title', 'content', 'scene_src', 'origin_scheme_id']
    ];

    /**
     * @author: Rudy
     * @time: 2017年9月22日
     * description:检测花色是否存在
     * @param $value
     * @return bool
     */
    public function checkMaterial($value)
    {
        return MaterialModel::getIdByName($value) === null ? false : true;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月23日
     * description:不传值时验证小于
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    public function requireLt($value, $rule, $data)
    {
        $val = $this->getDataValue($data, $rule);
        if ($val === '' || $val === null) {
            return true;
        }
        self::$typeMsg['requireLt'] = ':attribute必须小于 :rule';
        return $value < $val;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月23日
     * description:验证图片的上传数量
     * @param $value
     * @return bool
     */
    public function checkLimit($value)
    {
        self::$typeMsg['checkLimit'] = ':attribute最多只能上传三张';
        return count(explode('|', $value)) > 3 ? false : true;
    }
}