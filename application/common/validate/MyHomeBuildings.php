<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeBuildings extends Validate
{
    protected $rule = [
        'buildings_id' => 'require|number|regex:\d{0,10}',
        'id' => 'require|number|regex:\d{0,10}',
        'house_id' => 'require|number|regex:\d{0,10}',
        'program_id' => 'require|number|regex:\d{0,10}',
        'name' => 'require|max:255',
        'province' => 'require|max:50',
        'city' => 'require|max:50',
        'address' => 'require|max:255',
        'house_type' => 'require|max:255',
        'begin_time' => 'require|max:255',
        'others_time' => 'require|max:255',
        'decorate_criterion' => 'require|max:255',
        'developer' => 'require|max:255',
        'property_company' => 'require|max:255',
        'floor_space' => 'require|max:255',
        'greenest' => 'require|max:255',
        'property_fee' => 'require|max:255',
        'property_type' => 'require|max:255',
        'plot_ratio' => 'require|max:255',
        'planning_count' => 'require|max:255',
        'house_type_src' => 'require|max:255',
        'poster_src' => 'require|max:255',
        'carport_scale' => 'require|max:255',
        'intro' => 'require|max:255',
        'keyword' => 'require|max:50',
        'collect_type' => 'require|in:collect,cancel',
        'ridgepole_id' => 'require|number|regex:\d{0,10}',
        'floor_id' => 'require|number|regex:\d{0,10}',
        'unit_id' => 'require|number|regex:\d{0,10}',
    ];

    protected $field = [
        'type' => '家具类型',
        'id' => '分类id',
        'name' => '名称',
        'province' => '省',
        'city' => '市',
        'address' => '地址',
        'house_type' => '户型',
        'begin_time' => '开盘时间',
        'others_time' => '交房时间',
        'decorate_criterion' => '装修标准',
        'developer' => '开发商',
        'property_company' => '物业公司',
        'floor_space' => '占地面积',
        'greenest' => '绿化率',
        'property_fee' => '物业费',
        'property_type' => '物业类型',
        'plot_ratio' => '容积率',
        'planning_count' => '规划户数',
        'house_type_src' => '户型图',
        'poster_src' => '海报图',
        'carport_scale' => '车位比',
        'program_id' => '案例id',
        'intro' => '简介'
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => [
            'name',
            'province',
            'city',
            'address',
            'house_type',
            'begin_time',
            'others_time',
            'decorate_criterion',
            'developer',
            'property_company',
            'floor_space',
            'greenest',
            'property_fee',
            'property_type',
            'plot_ratio',
            'planning_count',
            'house_type_src',
            'poster_src',
            'carport_scale',
            'intro'
        ],
        'addOrEdit2' => [
            'name',
            'province',
            'city',
            'address',
            'house_type',
            'begin_time',
            'others_time',
            'house_type_src',
            'intro',
        ],
        'getInfo' => ['buildings_id'],
        'getInfo2' => ['house_id'],
        'getInfo3' => ['program_id'],
        'getList' => ['province'],
        'getList2' => ['city'],
        'search' => ['keyword'],
        'collect' => ['buildings_id','collect_type'],
        'getRidgepole' => ['buildings_id'],
        'getFloor' => ['buildings_id','ridgepole_id'],
        'getUnit' => ['buildings_id','floor_id'],
        'getHouseType' => ['buildings_id'],
        'getUnitDecorates' => ['id'],
        'addOrEdit2' => [
            'name',
            'province',
            'city',
            'address',
            'house_type',
            'begin_time',
            'others_time',
            'house_type_src',
            'intro'
        ],
    ];

}