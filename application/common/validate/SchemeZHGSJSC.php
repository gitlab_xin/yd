<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/28
 * Time: 20:27
 */

namespace app\common\validate;

class SchemeZHGSJSC extends SchemeBase
{
    protected $scheme_type = 'ZHSJSC';

    protected $rule = [
        'width' => 'require|integer|between:1387,4861',
        'scheme_s_type' => 'require|in:0,1',
        'errorrange' => 'in:50,100,150'
    ];

    protected $field = [
        'width' => '宽度',
        'scheme_s_type' => '桌子类型',
        'errorrange' => '误差值'
    ];

    protected $message = [
    ];

    protected $scene = [
        'create' => ['width', 'scheme_s_type', 'errorrange'],
        'math' => ['width', 'scheme_s_type'],
        'store' => ['scheme_name', 'scheme_pic', 'scheme_s_type'],
        'update' => ['scheme_name', 'scheme_pic', 'scheme_s_type', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];

}