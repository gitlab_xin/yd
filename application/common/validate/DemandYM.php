<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;


class DemandYM extends DemandBase
{
    protected $rule = [
        'scheme_hole_type' => 'require|integer|between:1,16',
        'scheme_hole_width' => 'require|integer|between:432,5000',
        'scheme_hole_sl_height' => 'require|integer|between:2036,2900',
        'scheme_door_count' => 'require|integer|in:2,3,4,5',
        'scheme_sk' => 'require|in:-1,000,004',
    ];

    protected $field = [
        'scheme_hole_type' => '洞口类型',
        'scheme_hole_width' => '洞口宽度',
        'scheme_hole_sl_height' => '上梁高度',
        'scheme_door_count' => '移门数量',
        'scheme_sk' => '收口颜色',
    ];

    protected $message = [
    ];

    protected $scene = [
        'info' => [
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_hole_type', 'scheme_hole_width', 'scheme_hole_sl_height', 'scheme_door_count', 'scheme_sk'
        ],
    ];
}