<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeDecoratePoint extends Validate
{
    protected $rule = [
        'name' => 'require|max:8',
        'decorate_id' => 'require|number',
        'img_src' => 'max:64',
        'sub_title'=>'max:255'
    ];

    protected $field = [
        'name' => '分类名称',
        'img_src' => '图片',
        'sub_title'=>'副标题'
    ];

    protected $message = [
        'decorate_id' => '装修亮点ID参数不符合',
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'logo_src','sub_title'],
        'decorate_id' => ['decorate_id'],
    ];
}