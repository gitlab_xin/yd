<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 16:35
 */

namespace app\common\validate;


use think\Validate;

class ShopProductStandard extends Validate
{
    protected $rule = [
        'name'=>'require|max:8',
        'price' => 'require|number|regex:\d{1,7}(\.\d{1,2})?',
        'stocks' => 'require|number|regex:\d{1,6}',
        'product_id' => 'require|notIn:0',
        'reason' => 'max:20',
        'img' => 'require',
    ];

    protected $field = [
        'name' => '规格名称',
        'price' => '价格',
        'stocks' => '库存',
        'product_id' => '所属商品',
        'reason' => '修改原因',
        'img' => '规格图片'
    ];

    protected $message = [
        'product_id.require'=>'请选择:attribute',
        'product_id.notIn'=>'请选择:attribute',
        'price.regex' => ':attribute最大为百万级的数字,且最小单位为分'
    ];

    protected $scene = [
        'addOrEdit' => [ 'name','price','stocks','product_id','reason','img'],
    ];
}