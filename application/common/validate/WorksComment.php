<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 16:26
 */

namespace app\common\validate;

use think\Validate;

class WorksComment extends Validate
{
    protected $rule = [
        'works_id' => 'require|number|regex:\d{1,10}',
        'user_id' => 'require|number|regex:\d{1,10}',
        'by_user_id' => 'number|requireWith:by_comment_id',
        'parent_comment_id' => 'number|regex:\d{1,10}',
        'by_comment_id' => 'number|requireWith:by_user_id|regex:\d{1,10}',
        'content' => 'require|max:200',
        'comment_id' => 'require|number|regex:\d{1,10}'
    ];

    protected $field = [
        'works_id' => '案例id',
        'user_id' => '用户id',
        'by_user_id' => '被回复user_id',
        'parent_comment_id' => '上级评论id',
        'by_comment_id' => '被回复comment_id',
        'content' => '回复内容',
        'comment_id' => '回复id'
    ];

    protected $message = [
        'by_comment_id.requireWith' => ':attribute和被回复user_id必须同时有值',
        'by_user_id.requireWith' => ':attribute和被回复by_comment_id必须同时有值',
    ];

    protected $scene = [
        'commentList' => ['works_id'],
        'reply' => ['works_id','by_user_id','parent_comment_id','by_comment_id','content'],
        'commentDetail' => ['comment_id']
    ];

}