<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 17:02
 */

namespace app\common\validate;

use think\Validate;

class ForumArticle extends Validate
{
    protected $rule = [
        'article_type' => 'require|in:choiceness,strategy,customization,all',
        'pageIndex' => 'number',
        'pageSize' => 'number',
        'title' => 'require|max:20',
        'classify' => 'require|in:choiceness,strategy,customization',
        'content' => 'require|max:500000',
        'is_essence' => 'in:0,1',
        'is_top' => 'in:0,1',
        'article_title' => 'require|max:20',
        'article_classify' => 'require|in:choiceness,strategy,customization',
        'article_content' => 'require|max:500000',
        'article_id' => 'require|number|regex:\d{0,10}',
        'at_user_id' => 'number|regex:\d{0,10}',
        'collect_type' => 'require|in:collect,cancel',
        'sort' => 'in:hot,time',
        'keyword' => 'max:32',
        'praise_type' => 'require|in:praise,cancel',
        'scheme_id' => 'number|regex:\d{0,10}|compare:product_id',
        'product_id' => 'number|regex:\d{0,10}|compare:scheme_id',
    ];

    protected $field = [
        'article_type' => '帖子列表类型',
        'title' => '帖子标题',
        'classify' => '所属模块',
        'content' => '帖子详情',
        'is_essence' => '是否精华',
        'is_top' => '是否置顶',
        'article_title' => '帖子标题',
        'article_classify' => '帖子所在模块',
        'article_content' => '帖子内容',
        'article_id' => '帖子ID',
        'sort' => '排序条件',
        'keyword' => '搜索关键字',
        'praise_type' => '点赞类型',
        'scheme_id' => '关联方案',
        'product_id' => '关联商品'
    ];

    protected $message = [
        'article_type.in' => ':attribute参数不正确',
        'article_type.require' => ':attribute参数必须',
        'pageIndex.number' => '当前页数参数错误',
        'pageSize.number' => '获取条数参数错误',
        'is_essence.in' => '请选择:attribute',
        'is_top.in' => '请选择:attribute',
        'article_title.require' => ':attribute为必填',
        'article_title.length' => ':attribute最大长度为20个字符',
        'article_classify.require' => ':attribute为必填',
        'article_classify.in' => ':attribute选择错误',
        'article_content.require' => ':attribute为必填',
        'article_content.max' => ':attribute超出最大长度',
        'article_id.require' => ':attribute为必须',
        'article_id.number' => ':attribute格式错误',
        'at_user_id.number' => '用户ID格式错误',
    ];

    protected $scene = [
        'info' => ['article_id', 'at_user_id'],
        'list' => ['article_type', 'pageIndex', 'pageSize', 'at_user_id','sort'],
        'search' => ['article_type','keyword', 'pageIndex', 'pageSize', 'at_user_id','sort'],
        'addOrEdit' => ['title','classify','content','is_essence','is_top'],
        'add' => ['article_title', 'article_classify', 'article_content','scheme_id','product_id'],
        'praise' => ['article_id','praise_type'],
        'collect' =>['article_id','collect_type'],
        'delete' => ['article_id']
    ];

    /**
     * 对比方案id和商品id,两个字段不能同时大于0
     * @access protected
     * @param mixed     $value  字段值
     * @param mixed     $rule  验证规则
     * @param array     $data  数据
     * @return bool
     */
    protected function compare($value, $rule, $data)
    {
        $compare_val = $this->getDataValue($data, $rule);
        if($value > 0 && $compare_val > 0){
            self::setTypeMsg('compare','仅能关联一个元素');
            return false;
        }else{
            return true;
        }

    }
}