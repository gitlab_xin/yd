<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetLightGroup extends FormView
{
    protected $rule = [
        'name' => 'require|max:20',
    ];

    protected $field = [
        'name' => '名称',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name']
    ];

    protected $input_type = [
        'name' => 'text',
    ];
}