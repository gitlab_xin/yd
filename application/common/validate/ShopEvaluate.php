<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 15:01
 */

namespace app\common\validate;

use think\Validate;

class ShopEvaluate extends Validate
{
    protected $rule = [
        'order_id' =>'require|number|regex:\d{1,10}',
        'star' =>'require|in:1,2,3,4,5',
        'content' => 'max:200',
        'img_src_list' => 'max:600',
        'is_anonymous' => 'require|in:0,1'
    ];

    protected $field = [
        'order_id' => '订单id',
        'star' => '星级',
        'content' => '内容',
        'img_src_list' => '图片',
        'is_anonymous' => '是否匿名',
    ];

    protected $message = [
    ];

    protected $scene = [
        'evaluate' => [ 'order_id','star','content','img_src_list','is_anonymous'],
        'myEvaluate' => ['order_id']
    ];

}