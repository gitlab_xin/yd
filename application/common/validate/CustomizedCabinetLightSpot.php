<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetLightSpot extends FormView
{
    protected $rule = [
        'name' => 'require|max:20',
        'type' => 'require|in:DirectionalLight',
        'isHelper' => 'require|in:0,1',
        'param' => 'require|max:1024|checkJson',
        'position' => 'require|max:1024|checkJson',
        'shadow' => 'require|max:1024|checkJson'
    ];

    protected $field = [
        'name' => '名称',
        'type' => '类型',
        'isHelper' => '灯光助手',
        'param' => '参数',
        'position' => '坐标',
        'shadow' => '阴影'
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['isHelper', 'param', 'position', 'shadow'],
    ];

    protected $input_type = [
        'name' => 'hidden',
        'type' => 'hidden',
        'isHelper' => 'radio|0=否&1=是|0',
        'param' => 'textArea',
        'position' => 'textArea',
        'shadow' => 'textArea'
    ];
}