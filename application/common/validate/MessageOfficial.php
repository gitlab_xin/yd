<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MessageOfficial extends Validate
{
    protected $rule = [
        'activity_id' => 'require|number|regex:\d{0,10}',
        'title' => 'require|max:20',
        'img_src' => 'require|max:50',
        'content' => 'max:500000',
        'message_id' => 'require|number|regex:\d{0,10}'
    ];

    protected $field = [
        'activity_id' => '活动ID',
        'title' => '标题',
        'img_src' => '封面图',
        'content' => '详情',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['activity_id','title', 'img_src', 'content'],
        'detail' => ['message_id']
    ];

}