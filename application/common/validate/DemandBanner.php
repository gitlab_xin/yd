<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:53
 */

namespace app\common\validate;

use think\Validate;

class DemandBanner extends Validate
{
    protected $rule = [
        'demand_id' => 'require|number|gt:0',
        'scheme_pic' => 'require|max:300'

    ];

    protected $field = [
        'demand_id' => '需求id',
        'scheme_pic' => '采取方案图片'
    ];

    protected $message = [

    ];

    protected $scene = [
        'addOrEdit' => ['demand_id','scheme_pic'],
        'detail' => ['banner_id']
    ];
}