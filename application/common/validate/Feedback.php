<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/21
 * Time: 17:38
 */

namespace app\common\validate;

use think\Validate;

class Feedback extends Validate
{
    protected $rule = [
        'content' => 'require|max:200',
    ];

    protected $field = [
        'content' => '反馈内容',
    ];

    protected $message = [
    ];

    protected $scene = [
        'feedback' => ['content'],
    ];
}