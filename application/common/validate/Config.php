<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/4
 * Time: 9:48
 */

namespace app\common\validate;

use think\Validate;

class Config extends Validate
{
    protected $rule = [
        'video_src' => 'require',
        'img_src' => 'require',
        'product_id' => 'require|number|gt:0|regex:\d{1,11}',
        'discount' => 'require|number|between:0,1|gt:0|length:5'
    ];

    protected $field = [
        'video_src' => '推荐视频',
        'img_src' => '推荐图片',
        'product_id' => '推荐商品',
        'discount' => '定制间折扣'
    ];

    protected $message = [
    ];

    protected $scene = [
        'tutorial_recommend' => ['video_src', 'img_src'],
        'store_recommend' => ['img_src','product_id'],
        'customeized_discount' => ['discount'],
    ];
}