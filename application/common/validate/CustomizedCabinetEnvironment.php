<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetEnvironment extends FormView
{
    protected $rule = [
        'top' => 'require|max:255',
        'bottom' => 'require|max:255',
        'left' => 'require|max:255',
        'right' => 'require|max:255',
        'front' => 'require|max:255',
        'back' => 'require|max:255',
        'scene_id' => 'require|integer'
    ];

    protected $field = [
        'top' => '顶部图片',
        'bottom' => '底部图片',
        'left' => '左边图片',
        'right' => '右边图片',
        'front' => '前方图片',
        'back' => '背部图片',
        'scene_id' => '柜体id',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['top', 'bottom', 'left', 'right', 'front', 'back']
    ];

    protected $input_type = [
        'top' => 'image',
        'bottom' => 'image',
        'left' => 'image',
        'right' => 'image',
        'front' => 'image',
        'back' => 'image',
        'scene_id' => 'hidden',
    ];

}