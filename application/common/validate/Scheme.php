<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 17:10
 */

namespace app\common\validate;

use think\Validate;

class Scheme extends Validate
{
    protected $rule = [
        'id' => 'require|integer|gt:0',
        'scheme_id' => 'require|integer|gt:0',
        'classify' => 'max:20',
        'scheme_type' => 'require|in:all,RQS,QW,BZ,YM,ZH-CWSN,ZH-SJSC,ZH-YYST,ZH-BGDG,YMJ,ZH-BGDG',
    ];

    protected $field = [
    ];

    protected $message = [
    ];

    protected $scene = [
        'detail' => ['scheme_id'],
        'list' => ['scheme_type']
    ];
}