<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/3
 * Time: 11:15
 */

namespace app\common\validate;

use think\Validate;

class Tutorial extends Validate
{
    protected $rule = [
        'title' => 'require|max:50',
        'synopsis'=>'require|max:255',
        'img_src'=>'require|max:255',
        'video_src'=>'require|max:255',
    ];

    protected $field = [
        'title' => '标题',
        'synopsis' => '副标题',
        'img_src'=> '封面图',
        'video_src'=> '教程视频'
    ];

    protected $message = [
    ];

    protected $scene = [
        'addAndEdit' => [ 'title','synopsis','img_src','video_src'],
    ];
}