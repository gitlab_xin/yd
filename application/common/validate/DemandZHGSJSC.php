<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;


class DemandZHGSJSC extends DemandBase
{
    protected $rule = [
        'scheme_width' => 'require|integer|between:464,4861',
        'scheme_height' => 'require|integer|in:435,755,1075,1395,1715,2019',
        'scheme_s_type' => 'require|in:0,1',
    ];

    protected $field = [
        'scheme_width' => '柜体宽度',
        'scheme_height' => '柜体高度',
        'scheme_s_type' => '写字桌布局'
    ];

    protected $message = [

    ];

    protected $scene = [
        'info' => [
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width', 'scheme_height','scheme_s_type','env_width', 'env_height'
        ],
    ];
}