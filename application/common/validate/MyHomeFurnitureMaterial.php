<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeFurnitureMaterial extends Validate
{
    protected $rule = [
        'furniture_id' => 'require|number|regex:\d{0,10}',
        'material_id' => 'require|number|regex:\d{0,10}',
        'name' => 'require|max:255',
        'color' => 'require|max:50',
        'render_img' => 'require|max:255',
    ];

    protected $field = [
        'furniture_id' => '家具模型ID',
        'name' => '款式名称',
        'color' => '颜色',
        'render_img' => '图片'
    ];

    protected $message = [
    ];

    protected $scene = [
        'furniture_insert' => ['name','color', 'render_img'],
        'insert' => ['furniture_id','name','color', 'render_img'],
        'edit' => ['material_id','name','color', 'render_img'],
        'getInfo' => ['material_id'],
        'getList' => ['furniture_id']
    ];

}