<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 15:53
 */

namespace app\common\validate;


use think\Validate;

class UserArea extends Validate
{
    protected $regex = [
        'mobile' => '1\d{10}'
    ];

    protected $rule = [
        'area_id' => 'require|number|regex:\d{1,10}',
        'username' => 'chsAlphaNum',
        'phone' => 'require|number|length:11|regex:mobile',
        'province' => 'chs|max:10',
        'city' => 'chs|max:10',
        'county' => 'chs|max:10',
        'town' => 'chs|max:10',
        'detail' => 'require|max:50',
        'is_default' => 'require|in:1,0',
        'code' => 'require|max:8',
    ];

    protected $field = [
        'phone' => '手机号码'
    ];
    protected $message = [
        'area_id' => 'ID参数不符合',
        'username' => '姓名格式不符合',
        'phone.regex' => ':attribute格式不符合',
        'province' => '省份参数不符合',
        'city' => '市级参数不符合',
        'county' => '县/区级参数不符合',
        'town' => '街道参数不符合',
        'detail' => '详细地址参数不符合',
        'is_default' => '默认选项参数不符合',
    ];

    protected $scene = [
        'add' => [
            'username',
            'phone',
            'province',
            'city',
            'county',
            'town',
            'detail',
            'is_default',
            'code',
        ],
        'edit' =>
        [
            'area_id',
            'username',
            'phone',
            'province',
            'city',
            'county',
            'town',
            'detail',
            'is_default',
            'code',
        ],
        'delete' => ['area_id']
    ];

}