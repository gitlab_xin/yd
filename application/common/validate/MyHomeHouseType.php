<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeHouseType extends Validate
{
    protected $rule = [
        'house_id' => 'require|number|regex:\d{0,10}',
        'classify_id' => 'require|number|regex:\d{0,10}',
        'type' => 'require|max:255',
        'name' => 'require|max:255',
        'obj_url' => 'require|max:255',
        'mtl_url' => 'require|max:255',
        'size' => 'require',
        'position' => 'require',
        'scale' => 'require',
        'rotation' => 'require',
        'material' => 'require',
        'intro' => 'require',
        'render_img' => 'require',
        'bedroom' => 'require|max:255',
        'data' => 'require',
        'environment_type' => 'require|in:camera,lightGroup,render,orbitCtrl'
    ];

    protected $field = [
        'type' => '户型类型',
        'name' => '模型名称',
        'obj_url' => 'obj',
        'mtl_url' => 'mtl',
        'size' => '尺寸',
        'position' => '坐标',
        'scale' => '缩放比',
        'rotation' => '旋转',
        'material' => '材质',
        'intro' => '简介',
        'render_img' => '渲染图',
        'bedroom' => '居室'
    ];

    protected $message = [
    ];

    protected $scene = [
        'insertMode' => ['house_id', 'name', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro', 'render_img'],
        'getInfo' => ['house_id'],
        'insertOrEdit' => ['name', 'proportion', 'elevator_count', 'orientation', 'img_src', 'bedroom'],
        'insertOrEdit2' => ['buildings_id', 'proportion', 'name', 'img_src', 'bedroom'],
        'getEnvironment' => ['house_id', 'environment_type'],
        'updateEnvironment' => ['house_id', 'environment_type', 'data']
    ];

}