<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:37
 */

namespace app\common\validate;


use think\Validate;

class DemandOrder extends Validate
{
    protected $rule = [
        'demand_id' => 'require|number|gt:0',
        'reward' => 'require|number|gt:0|elt:999',
        'pay_type' => 'require|in:wechat,alipay,balance',
        'trade_type' => 'require|in:APP,JSAPI,NATIVE',
    ];

    protected $field = [
        'demand_id' => '需求ID',
        'reward' => '赏金',
        'pay_type' => '支付类型',
        'trade_type' => '终端类型',
    ];

    protected $message = [
        'pay_type.require' => '支付类型不能为空',
        'pay_type.in' => '支付类型错误',
        'trade_type' => '终端类型不正确'
    ];

    protected $scene = [
        'pay'=>['demand_id','reward','pay_type','trade_type'],
        'detail' => ['demand_id']
    ];

}