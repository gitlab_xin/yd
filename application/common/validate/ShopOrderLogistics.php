<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 16:06
 */

namespace app\common\validate;

use think\Validate;

class ShopOrderLogistics extends Validate
{
    protected $regex = [
        'mobile' => '1\d{10}'
    ];

    protected $rule = [
        'logistics_id' => 'require|number|notIn:0',
        'order_id' => 'require|number|notIn:0',
        'user_id' => 'require|number|notIn:0',
        'logistics_type' => 'require|max:10',
        'logistics_number' => 'max:20|requireIfNot:logistics_type,official',
        'contact_name' => 'requireWith:contact_number|max:8',
        'contact_number' => 'requireWith:contact_name|number|regex:mobile',
    ];

    protected $field = [
        'logistics_type' => '物流方式',
        'logistics_number' => '物流单号',
        'contact_name' => '联系人',
        'contact_number' => '联系电话'
    ];

    protected $message = [
        'contact_name.requireWith' => ':attribute不能为空',
        'contact_number.requireWith' => ':attribute不能为空',
        'logistics_number.requireIfNot' => '非官方物流请务必填写快递单号'
    ];

    protected $scene = [
        'add' => ['order_id','logistics_type','logistics_number'],
        'update' => ['contact_name','contact_number'],
        'detail' => ['logistics_id'],
    ];

    /**
     * 验证某个字段等于某个值的时候必须
     * @access protected
     * @param mixed     $value  字段值
     * @param mixed     $rule  验证规则
     * @param array     $data  数据
     * @return bool
     */
    protected function requireIfNot($value, $rule, $data)
    {
        list($field, $val) = explode(',', $rule);
        if ($this->getDataValue($data, $field) != $val) {
            return !empty($value);
        } else {
            return true;
        }
    }
}