<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/24
 * Time: 14:27
 */

namespace app\common\validate;

use think\Validate;

class UserDesigner extends Validate
{
    protected $regex = [
        'id_num' => '\d{17}[0-9Xx]'
    ];

    protected $rule = [
        'name' => 'require|max:8',
        'identity_num' => 'require|max:18|regex:id_num',
        'identity_front_src' => 'require|max:64',
        'identity_back_src' => 'require|max:64',
        'certificate_src' => 'max:255|checkLimit',
        'desc' => 'max:255',
        'remark' => 'max:100',
        'status' => 'in:1,2'
    ];

    protected $field = [
        'name' => '姓名',
        'identity_num' => '身份证号',
        'identity_front_src' => '身份证正面照',
        'identity_back_src' => '身份证背面照',
        'certificate_src' => '资质证书',
        'desc' => '个人描述',
        'remark' => '审核备注',
        'status' => '审核状态'
    ];

    protected $message = [
    ];

    protected $scene = [
        'add' => [ 'name','identity_num','identity_front_src','identity_back_src','certificate_src','desc'],
        'changeStatus' => ['status','remark']
    ];

    /**
     * @author: Rudy
     * @time: 2017年9月26日
     * description:验证图片的上传数量
     * @param $value
     * @return bool
     */
    public function checkLimit($value)
    {
        self::$typeMsg['checkLimit'] = ':attribute最多只能上传三张';
        return count(explode('|', $value)) > 3 ? false : true;
    }
}
