<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;

use think\Validate;

class Demand extends Validate
{
    protected $rule = [
        'demand_id' => 'require|integer|gt:0',
        'order_type'  => 'require|in:new,reward_desc,reward_asc',
        'reward_min' => 'number',
        'reward_max'  => 'number',
        'scheme_type' => 'in:RQS,QW,BZ,YM,ZH-CWSN,ZH-SJSC,ZH-YYST,ZH-BGDG',
        'style_name' => 'max:20',
        'material_name' => 'max:20',
        'scheme_hole_width_min' => 'integer',
        'scheme_hole_width_max' => 'integer',
        'scheme_hole_height_min'  => 'integer',
        'scheme_hole_height_max'  => 'integer',
        'show_status' => 'number|in:1,2,3'//1.发布完成2.已关闭3已完成
    ];

    protected $field = [
        'demand_id' => '需求ID',
        'order_type'  => '排序类型',
        'reward_min' => '赏金最小值',
        'reward_max'  => '赏金最大值',
        'scheme_type' => '柜类',
        'style_name' => '设计风格',
        'material_name' => '花色',
        'scheme_hole_width_min' => '洞口宽度最小值',
        'scheme_hole_width_max' => '洞口宽度最大值',
        'scheme_hole_height_min'  => '洞内顶部高度最小值',
        'scheme_hole_height_max'  => '洞内顶部高度最大值',
    ];

    protected $message = [
        'classify.in' => ':attribute不正确',
        'cost_max.gt' => ':attribute必须大于最小值',
        'hole_top_height' => ':attribute必须大于移门上梁高度',
    ];

    protected $scene = [
        'info' => ['demand_id'],
        'edit' => ['title', 'content'],
        'RQS' => [
            'order_type',
            'reward_min',
            'reward_max',
            'scheme_type',
            'style_name',
            'material_name',
            'scheme_hole_width_min',
            'scheme_hole_width_max',
            'scheme_hole_height_min',
            'scheme_hole_height_max',
        ],
        'list' => ['show_status']
    ];
}