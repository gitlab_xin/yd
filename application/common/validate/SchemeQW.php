<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 17:10
 */

namespace app\common\validate;

class SchemeQW extends SchemeBase
{
    protected $scheme_type = 'QW';

    protected $rule = [
        'width' => 'require|integer|between:1451,2475',
        'height' => 'require|integer|between:2025,2892',
        'scheme_height' => 'require|integer|between:2019,2886',
    ];

    protected $message = [
    ];

    protected $scene = [
        'create' => ['width', 'height'],
        'store' => ['scheme_name', 'scheme_pic', 'width', 'scheme_height'],
        'update' => ['scheme_name', 'scheme_pic', 'width', 'scheme_height', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];
}
