<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/24
 * Time: 15:22
 */

namespace app\common\validate;

use app\common\model\CustomizedColor;

class SchemeYM extends SchemeBase
{
    protected $scheme_type = 'YM';

    protected $rule = [
        'hole_type' => 'require|integer|between:1,16',
        'hole_width' => 'require|integer|between:432,6150',
        'hole_height' => 'require|integer|between:2036,2900',
        'door_count' => 'require|in:0,2,3,4,5|checkWidth:hole_width',
        'door_color_no' => 'require|checkDoorColor',
        'door_hcq' => 'require|in:0,1',
        'sk_color_no' => 'require|in:-1,000,004'
    ];

    protected $field = [
        'hole_type' => '洞口类型',
        'hole_width' => '洞口宽度',
        'hole_height' => '洞口高度',
        'door_count' => '移门数量',
        'door_color_no' => '移门颜色',
        'sk_color_no' => '收口条',
    ];

    protected $message = [
        'door_color_no.checkDoorColor' => '不存在的门颜色'
    ];

    protected $scene = [
        'create' => ['hole_type', 'hole_width', 'hole_height', 'door_count', 'sk_color_no'],
        'store' => ['hole_type', 'hole_width', 'hole_height', 'door_count', 'sk_color_no', 'door_color_no', 'scheme_name', 'scheme_pic', 'door_hcq'],
        'update' => ['scheme_id', 'door_color_no', 'scheme_name', 'scheme_pic', 'door_hcq', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年9月24日
     * description:校验门的数量是否符合洞口宽度
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    public function checkWidth($value, $rule, $data)
    {
        $width = $this->getDataValue($data, $rule);
        if ($value > 0) {
            $num = intval($width / $value) + intval(30 / 2);
            if ($num >= 1500) {
                $this->message('door_count.checkWidth', '您选择的门数目过小,生成的门单扇宽度过宽,请另行选择门数量!');
                return false;
            } elseif ($num <= 720) {
                $this->message('door_count.checkWidth', '您选择的门数目过大,生成的门单扇宽度过窄,请另行选择门数量!');
                return false;
            }
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月8日
     * description:检测柜体颜色
     * @param $value
     * @return bool
     */
    protected function checkDoorColor($value)
    {
        return CustomizedColor::build()->where(['color_no' => $value, 'is_door' => 1, 'is_deleted' => 0])->value('color_id') === null ? false : true;
    }

}