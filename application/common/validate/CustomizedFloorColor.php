<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedFloorColor extends Validate
{
    protected $rule = [
        'id' => 'require|number|notIn:0',
        'name' => 'require|max:20',
        'thumb_img_src' => 'require|max:255',
        'floor_img_src' => 'require|max:255',
    ];

    protected $field = [
        'name' => '颜色名称',
        'thumb_img_src' => '缩略图',
        'floor_img_src' => '地板图',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'thumb_img_src', 'floor_img_src'],
    ];

}