<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


use think\Validate;

class CustomizedColor extends Validate
{
    protected $rule = [
        'color_id' => 'require|integer|gt:0',
        'color_no' => 'require|unique:customized_color,is_deleted^color_no|length:3',
        'color_name' => 'require|max:10',
        'border_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'handle_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'alloy_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'material_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'door_border_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'door_alloy_color' => 'max:10|regex:#[a-zA-Z0-9]{6}',
        'color_level' => 'require|in:1,2,3',
        'img_src_row_light' => 'require|max:64',
        'img_src_row_dark' => 'require|max:64',
        'door_img_src' => 'max:64|requireIf:is_door,1',
        'origin_img_src' => 'require|max:64',
        'is_door' => 'require|in:0,1',
        'is_monochrome' => 'require|in:0,1',
    ];

    protected $field = [
        'color_id' => 'id',
        'color_no' => '颜色编号',
        'color_name' => '颜色名称',
        'border_color' => '门框颜色',
        'handle_color' => '把手颜色',
        'alloy_color' => '铝合金颜色',
        'material_color' => '材料颜色',
        'img_src_row_light' => '图片地址(横浅)',
        'img_src_row_dark' => '图片地址(横深)',
        'door_img_src' => '图片地址(移门)',
        'origin_img_src' => '原图地址',
        'is_door' => '是否移门',
        'is_monochrome' => '是否单色',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['color_no', 'color_name', 'border_color', 'handle_color', 'alloy_color', 'material_color', 'door_border_color',
            'door_alloy_color', 'color_level', 'is_monochrome',
            'img_src_row_light', 'img_src_row_dark', 'door_img_src', 'is_door', 'origin_img_src']
    ];

}