<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:37
 */

namespace app\common\validate;


use think\Validate;
use app\common\tools\RedisUtil;
use app\common\model\UserArea;
use app\common\model\CustomizedScheme;

class CustomizedPay extends Validate
{
    protected $rule = [
        'scheme_id' => 'require|integer|gt:0|checkOwner|checkCache',
        'count' => 'require|integer|gt:0',
        'delivery_time' => 'require|integer|gt:0|checkTime',
        'pay_type' => 'require|in:wechat,alipay,balance',
        'prepay_id' => 'requireIf:pay_type,wechat',
        'user_remarks' => 'max:200',
        'area_id' => 'require|integer|gt:0|checkAddress',
        'trade_type' => 'require|in:APP,JSAPI,NATIVE',
        'user_id' => 'require|integer|gt:0'
    ];

    protected $field = [
        'scheme_id' => '方案id',
        'count' => '购买数量',
        'delivery_time' => '送达时间',
        'pay_type' => '支付类型',
        'prepay_id' => '预付id',
        'user_remarks' => '买家留言',
        'area_id' => '地址id',
        'trade_type' => '终端类型',
        'user_id' => '用户id'
    ];

    protected $message = [
        'scheme_id.checkOwner' => '您不是该方案的拥有者',
        'scheme_id.checkCache' => '结算缓存已失效，请重新结算',
        'delivery_time.checkTime' => ':attribute必须大于当前日期三天以上',
    ];

    protected $scene = [
        'buildOrder' => ['scheme_id', 'count', 'delivery_time', 'pay_type', 'prepay_id', 'user_remarks', 'area_id', 'trade_type', 'user_id'],
        'confirmOrder' => ['scheme_id', 'count'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年11月28日
     * description:检查是不是方案的拥有者
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkOwner($value, $rule, $data)
    {
        if (CustomizedScheme::build()->where(['id' => $value, 'user_id' => $data['user_id']])->value('id') === null) {
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月28日
     * description:查看缓存中是否存在结算方案
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkCache($value, $rule, $data)
    {
        if (RedisUtil::getInstance()->hExists('scheme_complete', $value) == false) {
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月28日
     * description:检查日期不能少于三天
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkTime($value, $rule, $data)
    {
        if ($value < strtotime('+3 day')) {
            return false;
        }
        return true;
    }

    /**
     * @author: Rudy
     * @time: 2017年11月28日
     * description:查看收货地址是否属于该用户
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    protected function checkAddress($value, $rule, $data)
    {
        if (UserArea::build()->where(['area_id' => $value, 'user_id' => $data['user_id']])->value('username') === null) {
            return false;
        }
        return true;
    }
}