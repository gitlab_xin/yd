<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:21
 */

namespace app\common\validate;

use think\Validate;

class HelpCenterClassify extends Validate
{
    protected $rule = [
        'id' => 'require|integer|gt:0',
        'name' => 'require|max:20',
    ];

    protected $field = [
        'id' => '分类id',
        'name' => '分类名称',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name'],
        'getPosts' => ['id']
    ];

}