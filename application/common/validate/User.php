<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\validate;

use think\Validate;

class User extends Validate
{
    protected $regex = [
        'mobile' => '1\d{10}'
    ];

    protected $rule = [
        'mobile'    => 'require|number|length:11|regex:mobile',
        'username' => 'require|max:30',
        'password'  => 'require|min:6|max:64',
        'avatar' => 'require|max:500',
        'gender' => 'in:男,女,male,female,,',
        'province' => 'chsAlpha',
        'city' => 'chsAlpha',
        'oauth_type' => 'require|in:wechat,qq',
        'oauth_id' => 'require',
        'user_name' => 'require|max:30',
        'user_mobile' => 'number|length:11|regex:mobile',
        'bank_name' => 'require|max:200',
        'bank_account' => 'require|number',
        'money' => 'require|number|gt:0',
        'email' => 'email|unique:user,status=1&email=',//添加用户时规则.修改用户时后面需要加上",$id"
        'add_money' => 'number|egt:0',
        'experience' => 'regex:\d{0,6}',
        'add_mobile' => 'require|number|length:11|regex:mobile|unique:user,status=1&mobile=',//添加用户时规则.修改用户时后面需要加上",$id"
        'edit_password' => 'min:6|max:64',
        'ban' => 'require|in:0,1',
        'ban_begin_time' => 'date',
        'ban_end_time' => 'date',
        'user_id' => 'require|number',
        'push_id' => 'require|max:64',
        'device_type' => 'require|in:ios,android'
    ];

    protected $field = [
        'experience' => '经验值',
        'add_mobile' => '手机号码',
        'email' => '邮箱地址',
        'ban' => '封号',
        'ban_begin_time' => '封号开始时间',
        'ban_end_time' => '封号结束时间'
    ];

    protected $message = [
        'mobile'            => '手机号码格式错误',
        'mobile.require'    => '手机号不能为空',
        'username.require' => '昵称不能为空',
        'username.max'     => '昵称长度不能超过20',
        'password.require'  => '密码不能为空',
        'password.min'      => '密码过短',
        'avatar' => '头像不能为空',
        'gender' => '性别设置错误',
        'province' => '省份设置错误',
        'city' => '城市设置错误',
        'oauth_type' => '第三方类型必须',
        'oauth_id' => '第三方id必须',
        'username' => '请填写真实姓名',
        'user_mobile' => '请填写联系手机号',
        'bank_name' => '请填写银行名称',
        'bank_account' => '请填写银行账号',
        'money' => '请填写提现金额',
        'email.unique' => '邮箱已存在',
        'ban_end_time.after' => ':attribute必须晚于封号结束时间',
        'ban.require' => '请选择是否进行:attribute',
        'ban.in' => '请选择是否进行:attribute',
        'experience.regex' => ':attribute必须是长度为6的整数型数字',
    ];

    protected $scene = [
        'login' => [ 'mobile','password' ],
        'username' => [ 'username' ],
        'avatar' => [ 'avatar' ],
        'gender' => [ 'gender' ],
        'age' => [ 'age' ],
        'province' => [ 'province' ],
        'city' => [ 'city' ],
        'oauth' => ['oauth_type', 'oauth_id', 'nick_name', 'avatar'],
        'withdraw' => [ 'user_name', 'user_mobile', 'bank_name', 'bank_account', 'money' ],
        'add'=>['username','password','add_mobile','gender','add_money','email','experience'],
        'edit'=>['username','edit_password','add_mobile','gender','add_money','email','experience'],
        'user' => ['user_id'],
        'ban_time' => ['ban','ban_begin_time','ban_end_time'],
        'password' => ['password'],
        'push' => ['push_id','device_type']
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false){
        if(isset($this->rule[$key]) && is_string($value)){
            $append ? $this->rule[$key] = $this->rule[$key].$value:$this->rule[$key] = $value;
        }
        return $this;
    }

}
