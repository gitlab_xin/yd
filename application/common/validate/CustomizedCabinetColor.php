<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedCabinetColor extends Validate
{
    protected $rule = [
        'cabinet_color_id' => 'require|integer|gt:0',
        'cabinet_id' => 'require|integer|gt:0',
        'color_id' => 'require|integer|gt:0',
    ];

    protected $field = [
        'cabinet_color_id' => 'id',
        'cabinet_id' => '柜体id',
        'color_id' => '颜色id',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['cabinet_id', 'color_id'],
    ];

}