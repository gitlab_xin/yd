<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 11:58
 */

namespace app\common\validate;


use think\Validate;

class ShoppingCart extends Validate
{
    protected $rule = [
        'cart_id' => 'require|number|regex:\d{1,10}',
        'product_id' => 'require|number|regex:\d{1,10}',
        'standard_id' => 'require|number|regex:\d{1,10}',
        'count' => 'require|number|regex:\d{1,10}',
        //'type' => 'require|in:INC,DEC',
        'cart_id_list' => 'require'
    ];

    protected $message = [
        'product_id' => '商品ID参数不符合',
        'standard_id' => '规格ID参数不符合',
        'count' => '商品数量参数不符合',
        'type' => '类型参数不符合',
        'cart_id' => '购物车ID参数不符合',
        'cart_id_list.require'=> '购物车ID不能为空',
    ];

    protected $scene = [
        'add' => ['product_id', 'standard_id', 'count'],
        'setCount' => ['cart_id', 'count'],
        'delete' => ['cart_id_list'],
        'collect' => ['cart_id_list']
    ];

}