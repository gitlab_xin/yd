<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedWallColor extends FormView
{
    protected $rule = [
        'id' => 'require|number|notIn:0',
        'name' => 'require|max:20',
        'thumb_img_src' => 'require|max:255',
        'app_left_middle_wall_img_src' => 'require|max:255',
        'app_left_bottom_wall_img_src' => 'require|max:255',
        'app_right_middle_wall_img_src' => 'require|max:255',
        'app_right_bottom_wall_img_src' => 'require|max:255',
        'app_back_wall_img_src' => 'require|max:255',
        'web_left_wall_img_src' => 'require|max:255',
        'web_right_wall_img_src' => 'require|max:255',
        'web_back_wall_img_src' => 'require|max:255',
        'middle_cover_img_src' => 'require|max:255',
        'pad_left_middle_wall_img_src' => 'require|max:255',
        'pad_left_bottom_wall_img_src' => 'require|max:255',
        'pad_right_middle_wall_img_src' => 'require|max:255',
        'pad_right_bottom_wall_img_src' => 'require|max:255',
        'pad_back_wall_img_src' => 'require|max:255',
    ];

    protected $field = [
        'name' => '颜色名称',
        'thumb_img_src' => '缩略图',
        'app_left_middle_wall_img_src' => 'APP左墙体中图',
        'app_left_bottom_wall_img_src' => 'APP左墙体下图',
        'app_right_middle_wall_img_src' => 'APP右墙体中图',
        'app_right_bottom_wall_img_src' => 'APP右墙体下图',
        'app_back_wall_img_src' => 'APP背部墙体图',
        'web_left_wall_img_src' => 'web左墙体图',
        'web_right_wall_img_src' => 'web右墙体图',
        'web_back_wall_img_src' => 'web背部墙体图',
        'middle_cover_img_src' => '中间遮盖图',
        'pad_left_middle_wall_img_src' => 'PAD左墙体中图',
        'pad_left_bottom_wall_img_src' => 'PAD左墙体下图',
        'pad_right_middle_wall_img_src' => 'PAD右墙体中图',
        'pad_right_bottom_wall_img_src' => 'PAD右墙体下图',
        'pad_back_wall_img_src' => 'PAD背部墙体图',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'thumb_img_src',
            'app_left_middle_wall_img_src',
            'app_left_bottom_wall_img_src',
            'app_right_middle_wall_img_src',
            'app_right_bottom_wall_img_src',
            'app_back_wall_img_src',
            'web_left_wall_img_src',
            'web_right_wall_img_src',
            'web_back_wall_img_src',
            'middle_cover_img_src',
            'pad_left_middle_wall_img_src',
            'pad_left_bottom_wall_img_src',
            'pad_right_middle_wall_img_src',
            'pad_right_bottom_wall_img_src',
            'pad_back_wall_img_src',
        ],
    ];

    protected $input_type = [
        'name' => 'text',
        'thumb_img_src' => 'image',
        'app_left_middle_wall_img_src' => 'image',
        'app_left_bottom_wall_img_src' => 'image',
        'app_right_middle_wall_img_src' => 'image',
        'app_right_bottom_wall_img_src' => 'image',
        'app_back_wall_img_src' => 'image',
        'web_left_wall_img_src' => 'image',
        'web_right_wall_img_src' => 'image',
        'web_back_wall_img_src' => 'image',
        'middle_cover_img_src' => 'image',
        'pad_left_middle_wall_img_src' => 'image',
        'pad_left_bottom_wall_img_src' => 'image',
        'pad_right_middle_wall_img_src' => 'image',
        'pad_right_bottom_wall_img_src' => 'image',
        'pad_back_wall_img_src' => 'image',
    ];
}