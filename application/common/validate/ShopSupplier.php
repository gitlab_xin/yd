<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/18
 * Time: 15:01
 */

namespace app\common\validate;


use think\Validate;

class ShopSupplier extends Validate
{
    protected $rule = [
        'supplier_id' =>'require|number',
        'br_id' =>'require|number',
        'name' => 'require|max:15',
        'intro' => 'require|max:255',
        'logo_src' => 'require|max:100',
        'bg_src' => 'require|max:100',
        'qualification' => 'max:255',
        'notice' => 'max:255'
    ];

    protected $field = [
        'name' => '商家名称',
        'intro' => '商家简介',
        'logo_src' => 'logo图片',
        'bg_src' => '背景图片',
        'qualification' => '资质',
        'content' => '图文详情',
        'notice'=>'公告'
    ];

    protected $message = [
        'supplier_id' => 'ID参数不符合',
        'br_id' =>'ID参数不符合',
        'logo_src' =>'请上传:attribute',
        'bg_src' =>'请上传:attribute',
    ];

    protected $scene = [
        'addOrEdit' => [ 'name','intro','logo_src','bg_src','qualification','notice'],
        'supplier_id' => ['supplier_id'],
        'br_id' => ['br_id']
    ];

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false){
        if(isset($this->rule[$key]) && is_string($value)){
            $append ? $this->rule[$key] = $this->rule[$key].$value:$this->rule[$key] = $value;
        }
        return $this;
    }
}