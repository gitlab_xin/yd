<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:25
 */

namespace app\common\validate;

use think\Validate;

class UserInviteCode extends Validate
{
    protected $rule = [
        'code' => 'require|min:4|max:8',
    ];

    protected $field = [
        'code' => '邀请码',
    ];

    protected $message = [
        'code' => ':attribute不正确'
    ];

    protected $scene = [
        'addOrEdit' => ['code'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false)
    {
        if (isset($this->rule[$key]) && is_string($value)) {
            $append ? $this->rule[$key] = $this->rule[$key] . $value : $this->rule[$key] = $value;
        }
        return $this;
    }
}
