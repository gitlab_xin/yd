<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetModel extends FormView
{
    protected $rule = [
        'type' => 'require|max:32',
        'name' => 'require|max:32',
        'uuid' => 'require|max:255',
        'objName' => 'require|max:255',
        'mtlName' => 'require|max:255',
        'bottom_to_top' => 'integer|length:0,999',
        'scale' => 'require|max:255|checkJson',
        'rotation' => 'require|max:255|checkJson',
        'position' => 'require|max:255|checkJson',
        'size' => 'require|max:255|checkJson',
        'scene_id' => 'require|integer',
        'model_id' => 'require|integer'
    ];

    protected $field = [
        'type' => '类型',
        'name' => '模型名称',
        'bottom_to_top' => '底部到顶部距离',
        'uuid' => '唯一标示码',
        'scale' => '缩放比',
        'rotation' => '旋转',
        'position' => '坐标',
        'size' => '尺寸',
        'objName' => 'obj地址',
        'mtlName' => 'mtl地址',
        'pics' => '图片',
        'scene_id' => '柜类id'
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['type', 'name', 'bottom_to_top', 'uuid', 'scale', 'rotation', 'position', 'size', 'objName', 'mtlName', 'pics',],
        'set' => ['model_id','type', 'name', 'bottom_to_top', 'uuid', 'scale', 'rotation', 'position', 'size', 'objName', 'mtlName', 'pics',]
    ];

    protected $input_type = [
        'type' => 'text',
        'name' => 'text',
        'bottom_to_top' => 'text',
        'uuid' => 'hidden',
        'scale' => 'textArea',
        'rotation' => 'textArea',
        'position' => 'textArea',
        'size' => 'textArea',
        'objName' => 'file',
        'mtlName' => 'file',
        'pics' => 'files',
    ];
}