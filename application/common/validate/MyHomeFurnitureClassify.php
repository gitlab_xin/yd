<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeFurnitureClassify extends Validate
{
    protected $rule = [
        'name' => 'require|max:8',
        'classify_id' => 'require|number',
        'img_src' => 'max:64'
    ];

    protected $field = [
        'name' => '分类名称',
        'img_src' => '图片',
    ];

    protected $message = [
        'classify_id' => '分类ID参数不符合',
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'logo_src'],
        'classify_id' => ['classify_id'],
    ];
}