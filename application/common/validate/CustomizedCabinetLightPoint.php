<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetLightPoint extends FormView
{
    protected $rule = [
        'isHelper' => 'require|in:0,1',
        'param' => 'require|max:1024|checkJson',
        'position' => 'require|max:1024|checkJson',
    ];

    protected $field = [
        'isHelper' => '灯光助手',
        'param' => '参数',
        'position' => '坐标',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['isHelper', 'param', 'position']
    ];

    protected $input_type = [
        'isHelper' => 'radio|0=否&1=是|0',
        'param' => 'textArea',
        'position' => 'textArea',
    ];
}