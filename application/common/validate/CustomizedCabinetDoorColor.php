<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedCabinetDoorColor extends Validate
{
    protected $rule = [
        'cabinet_door_color_id' => 'require|integer|gt:0',
        'cabinet_color_id' => 'require|integer|gt:0',
        'color_id' => 'require|integer|gt:0',
    ];

    protected $field = [
        'cabinet_door_color_id' => 'id',
        'cabinet_color_id' => '柜体颜色id',
        'color_id' => '颜色id',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['cabinet_color_id', 'color_id'],
    ];

}