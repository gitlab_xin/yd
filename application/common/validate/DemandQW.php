<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;


class DemandQW extends DemandBase
{
    protected $rule = [
        'scheme_width' => 'require|integer|between:1451,2475',
        'scheme_height' => 'require|integer|between:2025,2892',
    ];

    protected $field = [
        'scheme_width' => '柜体宽度',
        'scheme_height' => '柜体高度',
    ];

    protected $message = [
    ];

    protected $scene = [
        'info' => [
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width', 'scheme_height','env_width', 'env_height'
        ],
    ];
}