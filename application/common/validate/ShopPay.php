<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:37
 */

namespace app\common\validate;


use think\Validate;
use app\common\model\UserArea;
use app\common\model\ConfigArea;

class ShopPay extends Validate
{
    protected $rule = [
        'product_id' => 'require|number|gt:0',
        'standard_id' => 'require|number|gt:0',
        'count' => 'require|number|gt:0',
        'pay_type' => 'require|in:wechat,alipay,balance',
        'cart_id_list' => 'require',
        'prepay_id' => 'requireIf:pay_type,wechat',
        'user_remarks' => 'max:200',
        'area_id' => 'require|number|gt:0|checkArea',
        'cart_array' => 'require|validateCartArray',
        'trade_type' => 'require|in:APP,JSAPI,NATIVE',
        'out_trade_no' => 'require|length:32',

    ];

    protected $message = [
        'cart_id_list.require' => '购物车ID不能为空',
        'pay_type.require' => '支付类型不能为空',
        'pay_type.in' => '支付类型错误',
        'standard_id' => '规格参数不能为空',
        'count' => '购买数量不能为空',
        'area_id.checkArea' => '无法送达该地址',
        'user_remarks' => '买家留言长度不能超过200',
        'cart_array' => '购物车数据不合法',
        'trade_type' => '终端类型不正确'
    ];

    protected $scene = [
        'shoppingCart' => ['cart_array', 'pay_type', 'area_id', 'prepay_id', 'trade_type'],
        'product' => ['product_id', 'standard_id', 'count', 'pay_type', 'prepay_id', 'area_id', 'user_remarks', 'trade_type'],
        'settlement' => ['product_id', 'standard_id', 'count'],
        'settlementCart' => ['cart_id_list'],
        'outTradeNo' => ['out_trade_no']
    ];

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:
     * @param string $array 内容
     * @return bool
     */
    public function validateCartArray($array)
    {
        if (empty($array)) {
            return false;
        }
        if (!is_array($array)) {
            return false;
        }
        foreach ($array as $key => $value) {
            if (!is_int($value['cart_id']) || !isset($value['user_remarks'])) {
                return false;
            }
        }
        return true;
    }

    public function checkArea($val)
    {
        $code = UserArea::build()->where(['area_id' => $val])->value('code');
        if ($code === null || $code == '' || strlen($code) < 4) {
            return false;
        }
        return ConfigArea::build()->where(['code' => substr($code, 0,4)])->value('is_distribution') ? true : false;
    }
}