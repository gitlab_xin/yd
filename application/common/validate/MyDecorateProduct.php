<?php
namespace app\common\validate;

use think\Validate;

class MyDecorateProduct extends Validate
{
    protected $rule = [
        'product_id' => 'require|number',
        'room_id' => 'require|number',
        'program_id' => 'require|number',
        'name'=>'require',
//        'code'=>'require',
        'original_price'=>'require',
        'discount_price'=>'require|elt:original_price',
        'intro'=>'require',
        'image'=>'require',
        'cart_list'=>'require',
        'img_src' => 'require',
        'standard' => 'require',
        'color' => 'require',
        'type'=>'require|number',
        'color_id'=>'requireIf:type,2|number',
        'standard_id'=>'requireIf:type,3|number',
    ];

    protected $field = [
        'product_id' => '产品ID',
        'room_id' => '房间ID',
        'program_id' => '装修ID',
        'name'=>'产品名称',
        'code'=>'产品编码',
        'original_price'=>'原价',
        'discount_price'=>'优惠价',
        'intro'=>'简介',
        'image'=>'图片',
        'cart_list'=>'购物车商品',
        'type' => '产品类型',
        'color_id' => '花色',
        'standard_id' => '规格',
        'img_src' => '花色图片',
        'color' => '花色id',
        'standard' => '产品',
    ];


    protected $scene = [
        'add' => ['product_id', 'room_id','decorate_id','name','code','original_price','image'],
        'add_2' => ['product_id', 'room_id','decorate_id','name','code','original_price','discount_price','intro','image', 'standard'],
        'standard' => ['img_src','color','is_edit'],
        'cart_list' => ['cart_list','program_id'],
        'cart_info' => ['product_id', 'type', 'color_id','standard_id'],
    ];
}