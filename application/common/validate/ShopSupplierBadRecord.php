<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/18
 * Time: 15:01
 */

namespace app\common\validate;


use think\Validate;

class ShopSupplierBadRecord extends Validate
{
    protected $rule = [
        'supplier_id' => 'require|notin:0',
        'title' => 'require|max:100',
        'intro' => 'require|max:255',
        'solution' => 'require|max:255',
        'reason' => 'require|max:255',
        'content' => 'require|max:255',
    ];

    protected $field = [
        'supplier_id' => '商家',
        'title' => '标题',
        'intro' => '简介',
        'solution' => '解决方案',
        'reason' => '具体原因',
        'content' => '内容',
    ];

    protected $message = [
        'supplier_id.notin' => '请选择商家'
    ];

    protected $scene = [
        'addOrEdit' => ['supplier_id', 'title','intro','solution','reason','content'],
    ];

}