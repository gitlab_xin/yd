<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/31
 * Time: 11:49
 */

namespace app\common\validate;

use think\Validate;

class ShopSupplierParams extends Validate
{
    protected $rule = [
        'supplier_id' =>'number',
        'title' =>'require|max:12',
        'content' => 'require|max:64',
        'logo_src' => 'require|max:255'
    ];

    protected $field = [
        'supplier_id' => '商家id',
        'title' => '标题',
        'content' => '内容',
        'logo_src' => '图标'
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['supplier_id', 'title','content','logo_src'],
    ];
}