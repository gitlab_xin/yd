<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeFurniture extends Validate
{
    protected $rule = [
        'furniture_id' => 'require|number|regex:\d{0,10}',
        'classify_id' => 'require|number|regex:\d{0,10}',
        'type' => 'require|number|regex:\d{0,10}',
        'name' => 'require|max:255',
        'obj_url' => 'require|max:255',
        'mtl_url' => 'require|max:255',
        'size' => 'require',
        'position' => 'require',
        'scale' => 'require',
        'rotation' => 'require',
        'material' => 'require',
        'intro' => 'require',
        'render_img' => 'require'
    ];

    protected $field = [
        'type' => '家具类型',
        'name' => '模型名称',
        'obj_url' => 'obj',
        'mtl_url' => 'mtl',
        'size' => '尺寸',
        'position' => '坐标',
        'scale' => '缩放比',
        'rotation' => '旋转',
        'material' => '款式',
        'intro' => '简介',
        'render_img' => '渲染图'
    ];

    protected $message = [
    ];

    protected $scene = [
        'insert' => ['name', 'type', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro','render_img'],
        'edit' => ['furniture_id', 'name', 'type', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro','render_img'],
        'getInfo' => ['furniture_id'],
        'getList' => ['classify_id']
    ];

}