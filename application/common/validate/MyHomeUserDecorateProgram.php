<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 11:54
 */

namespace app\common\validate;

use think\Validate;

class MyHomeUserDecorateProgram extends Validate
{
    protected $rule = [
        'house_id' => 'require|number|regex:\d{0,10}',
        'user_id' => 'require|number|regex:\d{0,10}',
        'name' => 'require|max:255',
        'img_src' => 'require|max:255',
        'serial_array' => 'require',
        'program_id' => 'require|number|regex:\d{0,10}'
    ];

    protected $field = [
        'name' => '模型名称',
        'img_src' => '缩略图',
        'serial_array' => '模型数据'
    ];

    protected $message = [
    ];

    protected $scene = [
        'insert' => ['house_id', 'name', 'img_src', 'serial_array'],
        'getInfo' => ['program_id'],
        'save' => ['program_id', 'name', 'img_src', 'serial_array']
    ];

}