<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:21
 */

namespace app\common\validate;

use think\Validate;

class Activity extends Validate
{
    protected $rule = [
        'activity_id' => 'require|number|gt:0',
        'title' => 'require|max:40',
        'bg_src' => 'require|max:64',
        'content' => 'require',
        'end_time' => 'require|date|lt:activity_begin_time',
        'activity_begin_time' => 'require|date',
        'activity_end_time' => 'require|date|gt:activity_begin_time',
        'type' => 'require|in:ongoing,end,all'
    ];

    protected $field = [
        'activity_id' => '活动ID',
        'title' => '活动标题',
        'bg_src' => '活动封面图',
        'content' => '图文详情',
        'end_time' => '截止时间',
        'activity_begin_time' => '活动开始时间',
        'activity_end_time' => '活动结束时间',
    ];

    protected $message = [
        'activity_id' => 'ID参数不符合',
        'end_time.lt' => ':attribute必须小于活动开始时间',
        'end_time.after' => ':attribute必须大于当前时间',
        'activity_end_time.gt' => ':attribute必须大于活动开始时间',
    ];

    protected $scene = [
        'getInfo' => ['activity_id'],
        'addOrEdit' => ['title', 'bg_src', 'content', 'end_time','activity_begin_time','activity_end_time'],
        'myList' => ['type']
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false)
    {
        if (isset($this->rule[$key]) && is_string($value)) {
            $append ? $this->rule[$key] = $this->rule[$key] . $value : $this->rule[$key] = $value;
        }
        return $this;
    }
}