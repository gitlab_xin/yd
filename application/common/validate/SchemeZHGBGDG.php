<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/28
 * Time: 20:27
 */

namespace app\common\validate;

class SchemeZHGBGDG extends SchemeBase
{
    protected $scheme_type = 'ZHBGDG';

    protected $rule = [
        'width' => 'require|integer|checkWidth:s_type',
        'error_range' => 'in:50,100,150',
        'scheme_name' => 'require',
        'scheme_pic'  => 'require',
        'scheme_width'=> 'require',
        'scheme_height'=> 'require',
        'scheme_deep'=> 'require',
        'product_list'=> 'require',
    ];

    protected $field = [
        'width' => '宽度',
        's_type' => '组合形式',
        'error_range' => '误差值',
        'product_list' => '吊柜参数',
    ];

    protected $message = [
    ];

    protected $scene = [
        'math' => ['width', 's_type', 'tv_size'],
        'create' => ['width', 'tv_type', 'tv_size', 's_type', 'error_range'],
        'store' => ['scheme_name', 'scheme_pic', 'scheme_width', 'scheme_height','scheme_deep'],
        'update' => ['scheme_name', 'scheme_pic', 'scheme_width', 'scheme_height','scheme_deep', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
        'price' => ['product_list']
    ];

    public function checkWidth($value, $rule, $data)
    {
        $val = $this->getDataValue($data, $rule);
        if ($val == 0) {
            self::$typeMsg['checkWidth'] = ':attribute只能在 1140 - 2900 之间';
            return $value <= 2900 && $value >= 1140;
        } else {
            self::$typeMsg['checkWidth'] = ':attribute只能在 1643 - 6000 之间';
            return $value <= 6000 && $value >= 1643;
        }


    }
}