<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/9/16
 * Time: 15:17
 */

namespace app\common\validate;

use think\Validate;

class CustomizedEvaluate extends Validate
{
    protected $rule = [
        'order_id' => 'require|number',
        'materials_star' => 'require|in:1,2,3,4,5',
        'service_star' => 'require|in:1,2,3,4,5',
        'process_star' => 'require|in:1,2,3,4,5',
        'install_star' => 'require|in:1,2,3,4,5',
        'delivery_star' => 'require|in:1,2,3,4,5',
        'content' => 'max:200',
    ];

    protected $field = [
        'order_id' => '订单id',
        'materials_star' => '材料评星',
        'service_star' => '服务评星',
        'process_star' => '加工评星',
        'install_star' => '安装评星',
        'delivery_star' => '配送评星',
        'content' => '内容',
    ];

    protected $message = [
    ];

    protected $scene = [
        'evaluate' => [
            'order_id',
            'materials_star',
            'service_star',
            'process_star',
            'install_star',
            'delivery_star',
            'content'
        ],
        'getOrderEvaluate' => ['order_id']
    ];

}