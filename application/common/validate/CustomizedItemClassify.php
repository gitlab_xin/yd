<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

class CustomizedItemClassify extends FormView
{
    protected $rule = [
        'classify_id' => 'require|number|notIn:0',
        'name' => 'require|max:120',
        'cabinet_type' => 'require|max:20',
        'img_src' => 'require|max:128',
    ];

    protected $field = [
        'classify_id' => '分类',
        'name' => '物品类型名称',
        'cabinet_type' => '所属柜类',
        'img_src' => '分类缩略图',
    ];

    protected $message = [
    ];

    protected $scene = [
        'item' => ['classify_id'],
        'addOrEdit' => ['name', 'cabinet_type', 'img_src']
    ];

    protected $input_type = [
        'name' => 'text',
        'cabinet_type' => 'radio|YG=衣柜&ZH=综合|YG',
        'img_src' => 'image',
    ];
}