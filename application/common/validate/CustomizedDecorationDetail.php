<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;
use think\Config;

class CustomizedDecorationDetail extends Validate
{
    protected $rule = [
        'id' => 'require|integer|notIn:0',
        'name' => 'require|max:255',
        'thumb_img_src' => 'require|max:255',
        'item_img_src' => 'require|max:255',
        'width' => 'require|integer|between:0,9999',
        'deep' => 'require|integer|between:0,9999',
        'height' => 'require|integer|between:0,9999',
        'decoration_id' => 'require|integer|gt:0',
        'position' => 'array|checkPosition',
    ];

    protected $field = [
        'name' => '装饰名称',
        'thumb_img_src' => '装饰缩略图',
        'item_img_src' => '装饰图片(背景透明)',
        'width' => '装饰实际宽度(mm)',
        'deep' => '装饰实际深度(mm)',
        'height' => '装饰实际高度(mm)',
        'decoration' => '装饰类型ID',
        'position' => '装饰位置',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'thumb_img_src', 'item_img_src', 'width', 'deep', 'height', 'decoration_id']
    ];

    protected function checkPosition($array)
    {
        foreach ($array as $v) {
            if (in_array($v, array_keys(Config::get('customized.item_position'))) == false) {
                return false;
            }
        }
        return true;
    }
}