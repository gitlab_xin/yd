<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetCamera extends FormView
{
    protected $rule = [
        'type' => 'require|max:20',
        'name' => 'require|max:10',
        'is_default' => 'require|in:0,1',
        'param' => 'require|max:1024|checkJson',
        'position' => 'require|max:1024|checkJson',
        'lookAt' => 'require|max:1024|checkJson',
    ];

    protected $field = [
        'type' => '类型',
        'name' => '名称',
        'is_default' => '是否默认',
        'param' => '参数',
        'position' => '坐标',
        'lookAt' => '焦点',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['type', 'name', 'is_default', 'param', 'position', 'lookAt']
    ];

    protected $input_type = [
        'type' => 'text',
        'name' => 'text',
        'is_default' => 'radio|0=否&1=是|0',
        'param' => 'textArea',
        'position' => 'textArea',
        'lookAt' => 'textArea',
    ];
}