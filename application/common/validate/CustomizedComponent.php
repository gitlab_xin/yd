<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedComponent extends Validate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'img_src' => 'require|max:64',
        'min_width' => 'require|integer|between:0,9999',
        'max_width' => 'require|integer|between:0,9999|egt:min_width',
        'min_height' => 'require|integer|between:2019,2403',
        'max_height' => 'require|integer|between:2019,2403|egt:min_height',
        'height' => 'require|integer|between:2019,2403|egt:min_height|elt:max_height',
        'width' => 'require|integer|in:424,752,784,880,944,1200',
        'scheme_type' => 'in:RQS,QW,BZ',
        'adjust_type' => 'in:0,1,2,3',
    ];

    protected $field = [
        'img_src' => '缩略图',
        'min_width' => '最小宽度',
        'max_width' => '最大宽度',
        'height' => '高度',
        'width' => '宽度',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['img_src','img_src_original'],
        'module' => ['width', 'height', 'scheme_type', 'adjust_type'],
    ];

}