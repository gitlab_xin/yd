<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


class CustomizedCabinetLightAmbinet extends FormView
{
    protected $rule = [
        'param' => 'require|max:1024|checkJson',
    ];

    protected $field = [
        'param' => '参数',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['param']
    ];

    protected $input_type = [
        'param' => 'textArea',
    ];
}