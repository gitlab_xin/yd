<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedDoorColor extends Validate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'index' => 'number',
        'name' => 'require|max:20',
        'thumb_img_src' => 'require|max:255',
        'door_img_src' => 'require|max:255',
    ];

    protected $field = [
        'name' => '颜色名称',
        'number' => '索引',
        'thumb_img_src' => '缩略图',
        'door_img_src' => '移门图',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'thumb_img_src', 'door_img_src'],
    ];

}