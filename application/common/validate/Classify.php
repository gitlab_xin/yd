<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 11:34
 */

namespace app\common\validate;

use think\Validate;

class Classify extends Validate
{
    protected $rule = [
        'name' => 'require|max:8',
        'classify_id' => 'require|number',
        'product_id' => 'require|number',
        'img_src' => 'max:64'
    ];

    protected $field = [
        'name' => '分类名称',
        'img_src' => '图片',
        'product_id' => '商品ID'
    ];

    protected $message = [
        'classify_id' => '分类ID参数不符合',
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'img_src'],
        'classify_id' => ['classify_id'],
        'classifyNames' => ['product_id']
    ];
}