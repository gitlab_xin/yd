<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 11:33
 */

namespace app\common\validate;

use think\Validate;

class CustomizedOrder extends Validate
{
    protected $rule = [
        'status' => 'require|in:all,unpaid,preparing,delivering,toevaluate,refunded',
        'order_id' => 'require|number',
        'keyword' => 'require|max:20',
        'refund_number' => 'require',
        'refuse_content' => 'require'
    ];

    protected $field = [
        'status' => '订单状态',
        'order_id' => '订单id',
        'keyword' => '关键字',
        'refund_number' => '退款金额',
        'refuse_content' => '拒绝理由'
    ];

    protected $message = [
    ];

    protected $scene = [
        'list' => ['status'],
        'search' => ['status','keyword'],
        'detail' => ['order_id'],
        'refund' => ['order_id','refund_number'],
        'refuse' => ['order_id','refuse_content']
    ];
}