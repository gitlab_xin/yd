<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:21
 */

namespace app\common\validate;

use think\Validate;

class ActivityApply extends Validate
{
    protected $regex = [
        'mobile' => '1\d{10}'
    ];

    protected $rule = [
        'activity_id' => 'require|number|gt:0',
        'name' => 'require|max:20',
        'age' => 'require|number|gt:0|lt:128|regex:\d{1,3}',
        'phone' => 'require|number|length:11|regex:mobile',
        'gender' => 'require|in:男,女,',
        'email' => 'max:40|email',
        'organize_type' => 'in:empty,school,company',
        'school_name' => 'requireIf:organize_type,school|max:20',
        'company_name' => 'requireIf:organize_type,company|max:20',
        'company_position' => 'requireIf:organize_type,company|max:20',
        'remarks' => 'max:200',
        'status' => 'in:no,wait,yes',
    ];

    protected $field = [
        'activity_id' => '所属活动',
        'name' => '性名',
        'age' => '年龄',
        'phone' => '手机号码',
        'gender' => '性别',
        'email' => '邮箱地址',
        'organize_type' => '组织类型',
        'school_name' => '学校名称',
        'company_name' => '公司名称',
        'company_position' => '公司职位',
        'remarks' => '额外说明',
        'status' => '审核状态',
    ];

    protected $message = [
        'name.require' => '请输入姓名',
        'name.max' => '姓名长度应小于20',
        'activity_id.activity_id' =>'活动ID非法',
        'age' => '年龄参数非法',
        'phone.regex' => '电话号码参数非法',
        'gender' => '性别参数非法',
        'email' => '邮箱参数非法',
        'organize_type' =>'单位类型参数非法',
        'school_name' => '学校名称非法',
        'company_name' => '公司名称非法',
        'company_position' => '公司职位非法',
        'remarks' => '附加说明非法',
    ];

    protected $scene = [
        'apply' => [
            'activity_id',
            'name',
            'phone',
            'age',
            'gender',
            'email',
            'organize_type',
            'school_name',
            'company_name',
            'company_position',
            'remarks',
        ],
        'applicationInfo' => ['activity_id']
    ];
}