<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class WebglModule extends Validate
{
    protected $rule = [
        'id' => 'require|number|notIn:0',
        'name' => 'require|max:32',
        'url' => 'require'
    ];

    protected $field = [
        'name' => '模型名称'
    ];

    protected $message = [
        'url.checkObj' => '请至少上传一个obj文件',
        'url.require' => '请上传模型图片'
    ];

    protected $scene = [
        'addOrEdit' => ['url']
    ];

    protected function checkObj($arr)
    {
        $flag = false;
        foreach ($arr as $val) {
            if (strpos($val, 'obj', strlen($val) - 3) !== false) {
                $flag = true;
            }
        }
        return $flag;
    }
}