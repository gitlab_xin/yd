<?php
/**
 * Created by PhpStorm.
 * User: airon
 */

namespace app\common\validate;

use think\Validate;

class Search extends Validate
{
    protected $rule = [
        //用户
        'mobile'    => 'number',
        'username' => 'max:30',
        'gender' => 'in:男,女,ALL',
        //商品
        'classify_one_id'=>'number',
        'classify_two_id'=>'number',
        'classify_three_id'=>'number',
        'name'=>'max:30',
        'supplier_id'=>'number',
        'left_price'=>'requireWith:right_price|float',
        'right_price'=>'requireWith:left_price|float|gt:left_price',
        //帖子
        'title' => 'max:30',
        'classify'=>'in:ALL,choiceness,strategy,customization',
        'is_essence'=>'in:99,0,1',
        'is_top'=>'in:99,0,1',
        'is_official'=>'in:99,0,1',
        //资讯
        'content'=>'max:40',
        //公用
        'begin_time' => 'requireWith:end_time',
        'end_time' => 'requireWith:begin_time|gt:begin_time',
    ];

    protected $field = [
        //用户
        'mobile' => '手机号码',
        'username' => '用户名',
        'gender' => '性别',
        //商品
        'classify_one_id' => '一级分类',
        'classify_two_id' => '二级分类',
        'classify_three_id' => '三级分类',
        'name' => '名称',
        'supplier_id' => '商家',
        'left_price' => '左价格区间',
        'right_price' => '右价格区间',
        //帖子
        'title'=>'标题',
        'classify'=>'所属模块',
        'is_essence'=>'是否精华',
        'is_top'=>'是否置顶',
        'is_official'=>'是否官方',
        //公用
        'begin_time' => '开始时间',
        'end_time' => '结束时间',
    ];

    protected $message = [
        'begin_time.requireWith' => '请选择:attribute',
        'end_time.requireWith' => '请选择:attribute',
        'end_time.gt' => ':attribute必须大于开始时间',
        'left_price.requireWith' => '请选择:attribute',
        'right_price.requireWith' => '请选择:attribute',
        'right_price.gt' => ':attribute必须大于左价格区间',
    ];

    protected $scene = [
        //用户
        'userSearch' => [ 'mobile','username','gender','begin_time','end_time'],
        //商品
        'productSearch' => [ 'classify_one_id','classify_two_id','classify_three_id','begin_time','end_time',
            'name','supplier_id','left_price','right_price'],
        //帖子
        'articleSearch' => [  'title','classify','is_essence','is_top','is_official','begin_time','end_time'],
        //资讯
        'newsSearch' => [  'title','content','begin_time','end_time'],
    ];

}
