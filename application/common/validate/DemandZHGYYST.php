<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 14:28
 */

namespace app\common\validate;


class DemandZHGYYST extends DemandBase
{
    protected $rule = [
        'scheme_width' => 'require|integer|between:464,4861',
        'scheme_s_type' => 'require|in:0,1',//0:侧搭式 1:嵌入式
        'tv_size' => 'in:32,36,38,40,42,46,50,52,54,56',
        'tv_type' => 'in:GS,TS'
    ];

    protected $field = [
        'scheme_width' => '柜体宽度',
        'scheme_height' => '柜体高度',
        'scheme_s_type' => '电视柜布局',
        'tv_size' => '电视柜规格',
        'tv_type' => '摆放方式',
    ];

    protected $message = [

    ];

    protected $scene = [
        'info' => [
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width','scheme_s_type','tv_size','tv_type','env_width', 'env_height'
        ],
    ];
}