<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:53
 */

namespace app\common\validate;

use think\Validate;

class DemandCloseReason extends Validate
{
    protected $rule = [
        'demand_id' => 'require|number|gt:0',
        'reason' => 'require|max:20|in:change_mind,mistake,unsuitable,others',
        'desc' => 'max:200',
    ];

    protected $field = [
        'demand_id' => '需求id',
        'reason' => '原因',
        'desc' => '说明'
    ];

    protected $message = [

    ];

    protected $scene = [
        'add' => ['demand_id','reason','desc']
    ];
}