<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;


use think\Validate;
use app\common\model\CustomizedCabinetSceneLightGroup as SceneLightGroupModel;

class CustomizedCabinetSceneLightGroup extends Validate
{
    protected $rule = [
        'type' => 'require|in:fullShow,floadShow,explosive|checkType',
        'scene_id' => 'require|integer',
        'light_group_id' => 'require|integer',
    ];

    protected $field = [
        'type' => '灯光组类型',
        'scene_id' => '场景id',
        'light_group_id' => '灯光组id',
    ];

    protected $message = [
        'type.checkType' => '灯光类型不能重复'
    ];

    protected $scene = [
        'addOrEdit' => ['type', 'scene_id', 'light_group_id']
    ];

    protected function checkType($value, $rule, $data)
    {
        $types = SceneLightGroupModel::build()->where(['scene_id' => $data['scene_id']])->column('type');
        return in_array($value, $types) ? false : true;
    }
}