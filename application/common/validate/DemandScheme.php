<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/4
 * Time: 17:53
 */

namespace app\common\validate;

use think\Validate;

class DemandScheme extends Validate
{
    protected $rule = [
        'scheme_id' => 'require|number|regex:\d{1,10}',
        'demand_id' => 'require|number|regex:\d{1,10}',
        'is_accepted' => 'in:0,1',
        'is_praise' => 'in:0,1',
        'is_good' => 'in:0,1',
        'message' => 'require|max:200',
        'title' => 'require|max:30',
        'desc' => 'require|max:200',
        'origin_scheme_id' => 'require|number|regex:\d{1,10}'
    ];

    protected $field = [
        'demand_id' => '需求id',
        'is_accepted' => '采纳',
        'is_praise' => '点赞',
        'is_good' => '优秀',
        'message' => '评价',
        'title' => '标题',
        'desc' => '说明',
        'origin_scheme_id' => '原始的方案id'
    ];

    protected $message = [

    ];

    protected $scene = [
        'detail'=> ['demand_id'],
        'add' => ['title','desc','origin_scheme_id','demand_id'],
        'comment' => ['scheme_id','message'],
        'praise' => ['scheme_id']
    ];
}