<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/28
 * Time: 20:27
 */

namespace app\common\validate;

class SchemeZHG extends SchemeBase
{
    protected $scheme_type = 'ZHCWSN';

    protected $rule = [
        'width' => 'require|integer|between:464,4861',
        'height' => 'require|integer|in:435,755,1075,1395,1715,2019',
        'errorrange' => 'in:50,100,150'
    ];

    protected $field = [
        'width' => '宽度',
        'height' => '高度',
        'errorrange' => '误差值'
    ];

    protected $message = [
    ];

    protected $scene = [
        'create' => ['width', 'height', 'color_no', 'errorrange'],
        'math' => ['width', 'height'],
        'store' => ['scheme_name', 'scheme_pic'],
        'update' => ['scheme_name', 'scheme_pic', 'scheme_id', 'user_id'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id'],
    ];

}