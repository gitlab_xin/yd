<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 17:10
 */

namespace app\common\validate;

use app\common\model\CustomizedColor;

class SchemeRQS extends SchemeBase
{
    protected $scheme_type = 'RQS';

    protected $rule = [
        'hole_type' => 'require|integer|between:0,16',
        'hole_width' => 'require|integer|between:432,5000',
        'hole_height' => 'require|integer|between:2036,2900',
        'sl_height' => 'require|integer|between:2036,2900|elt:hole_height',
        'b_width' => 'require|integer|between:0,999',
        'c_width' => 'require|integer|between:0,999',
        'door_count' => 'require|integer|in:-1,0,2,3,4,5|checkDoorCount:hole_width',
        'sk_color_no' => 'require|in:-1,000,004',
        'door_color_id' => 'require|integer|checkDoorColor',
        'scheme_door_have_hcq' => 'require|integer|in:0,1',
    ];

    protected $field = [
        'hole_type' => '洞口类型',
        'hole_width' => '洞口宽度',
        'hole_height' => '洞口高度',
        'hole_deep' => '洞口深度',
        'sl_height' => '上梁高度',
        'b_width' => '柱体宽度',
        'c_width' => '柱体宽度',
        'door_count' => '移门数量',
        'sk_color_no' => '收口颜色',
        'door_color_id' => '门颜色id',
        'scheme_door_have_hcq' => '缓冲器',
    ];

    protected $message = [
        'door_color_id.checkDoorColor' => '不存在的门颜色'
    ];

    protected $scene = [
        'create' => ['hole_type', 'hole_width', 'b_width', 'c_width', 'hole_height', 'sl_height', 'door_count', 'sk_color_no'],
        'store' => ['scheme_name', 'scheme_pic', 'door_color_id', 'scheme_door_have_hcq',
            'hole_type', 'hole_width', 'hole_height', 'sl_height', 'door_count', 'sk_color_no'],
        'update' => ['scheme_name', 'scheme_pic', 'door_color_id', 'scheme_door_have_hcq', 'user_id', 'scheme_id',
            'hole_type', 'hole_width', 'hole_height', 'sl_height', 'door_count', 'sk_color_no'],
        'complete' => ['scheme_id', 'user_id'],
        'complete2' => ['scheme_id']
    ];

    protected function checkDoorCount($value, $rule, $data)
    {
        $width = $this->getDataValue($data, $rule);
        if ($value > 0) {
            $num = intval($width / $value) + intval(30 / 2);
            if ($num >= 1500) {
                $this->message('door_count.checkDoorCount', '您选择的门数目过小,生成的门单扇宽度过宽,请另行选择门数量!');
                return false;
            } elseif ($num <= 591) {
                $this->message('door_count.checkDoorCount', '您选择的门数目过大,生成的门单扇宽度过窄,请另行选择门数量!');
                return false;
            }
        }
        return true;
    }

    protected function checkDoorColor($value)
    {
        if ($value == 0) {
            return true;
        }
        return CustomizedColor::build()->where(['color_id' => $value, 'is_door' => 1, 'is_deleted' => 0])->value('color_id') === null ? false : true;
    }

}
