<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 16:35
 */

namespace app\common\validate;


use think\Validate;

class ShopPopular extends Validate
{
    protected $rule = [
        'img_src' => 'require',
        'remarks' => 'max:100',
        'product_id' => 'require|notin:0|unique:shop_popular',
    ];

    protected $field = [
        'img_src' => '图片',
        'remarks' => '备注',
        'product_id' => '所属商品',
    ];

    protected $message = [
        'product_id.notin'=>'请选择:attribute',
        'product_id.unique'=>'一件商品只能对应一个热门商品'
    ];

    protected $scene = [
        'addOrEdit' => [ 'img_src','remarks','product_id'],
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false){
        if(isset($this->rule[$key]) && is_string($value)){
            $append ? $this->rule[$key] = $this->rule[$key].$value:$this->rule[$key] = $value;
        }
        return $this;
    }
}