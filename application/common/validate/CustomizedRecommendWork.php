<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 15:08
 */

namespace app\common\validate;

use think\Validate;

class CustomizedRecommendWork extends Validate
{
    protected $rule = [
        'width' => 'require|integer|between:0,9999',
        'height' => 'require|integer|between:0,9999',
        'scheme_type' => 'require|in:RQS,QW,BZ,ZH',
        'scheme_s_type' => 'checkSType:scheme_type,scheme_b_type',
        'scheme_b_type' => 'checkBType:scheme_type',
        'sl_height' => "requireIf:scheme_type,RQS|number",
        'hole_height' => "requireIf:scheme_type,RQS|number",
        'scheme_detail_id' => "require|integer",
        'range' => "integer|between:0,300",
    ];

    protected $field = [
        'width' => '柜体宽度',
        'height' => '柜体高度',
        'scheme_type' => '柜类类别',
        'scheme_s_type' => '柜类小类别',
        'scheme_b_type' => '柜类大类别',
        'sl_height' => '上梁高度',
        'hole_height' => '洞口高度',
        'scheme_detail_id' => "方案ID",
    ];

    protected $message = [
    ];

    protected $scene = [
        'get' => ['width', 'height', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'sl_height', 'hole_height'],
        'info' => ['scheme_detail_id']
    ];

    /**
     * @author: Airon
     * @time: 2017年11月7日
     * description:校验方案小类别
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    public function checkSType($value, $rule, $data)
    {
        $paramData = explode(",", $rule);
        $schemeType = $this->getDataValue($data, $paramData[0]);
        $schemeBType = $this->getDataValue($data, $paramData[1]);
        self::setTypeMsg('checkSType', '柜类小类别错误!');
        switch ($schemeType) {
            case "RQS":
                return true;
            case "QW":
                return true;
            case "BZ":
                return $value == -1 || $value == 0 || $value == 1;
            case "ZH":
                switch ($schemeBType) {
                    case "YYST":
                        return $value == 0 || $value == 1;
                        break;
                    case "CWSN":
                        return true;
                        break;
                    case "SJSC":
                        return $value == 0 || $value == 1;
                        break;
                    case "BGDG":
                        return $value == 0 || $value == 1;
                        break;
                    default:
                        self::setTypeMsg('checkSType', '综合柜类型错误!');
                        return false;
                }

        }
        return true;
    }

    /**
     * @author: Airon
     * @time: 2017年11月7日
     * description:校验方案大类别
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    public function checkBType($value, $rule, $data)
    {
        $schemeType = $this->getDataValue($data, $rule);
        self::setTypeMsg('checkBType', '柜类大类别错误!');
        switch ($schemeType) {
            case "zh":
                return $value == "CWSN" || $value == "YYST" || $value == "SJSC" || $value == "BGDG";
            default:
                return true;
        }
    }
}