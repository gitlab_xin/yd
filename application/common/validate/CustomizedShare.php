<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:21
 */

namespace app\common\validate;

use think\Validate;

class CustomizedShare extends Validate
{
    protected $rule = [
        'share_id' => 'require|integer|gt:0',
        'scheme_id' => 'require|integer|gt:0',
        'scheme_pic_two' => 'require|max:128',
        'scheme_pic_three' => 'require|max:128',
        'expired_time' => 'require|integer|checkTime',
    ];

    protected $field = [
        'share_id' => '分享id',
        'scheme_id' => '方案id',
        'scheme_pic_two' => '二维方案图片',
        'scheme_pic_three' => '三维方案图片',
        'expired_time' => '过期时间',
    ];

    protected $message = [
        'expired_time.checkTime' => '过期时间必须大于当前时间'
    ];

    protected $scene = [
        'save' => ['scheme_id', 'scheme_pic_two', 'expired_time'],
    ];

    protected function checkTime($value)
    {
        return $value > time() ? true : false;
    }

}