<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/23
 * Time: 17:46
 */

namespace app\common\validate;

use think\Validate;

class CooperativePartner extends Validate
{
    protected $rule = [
        'name' => 'require|max:16',
        'logo_src' => 'require|max:64',
        'link' => 'max:64|url',
    ];

    protected $field = [
        'name' => '合作伙伴',
        'logo_src' => '图片',
        'link' => '跳转链接',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['name', 'logo_src', 'link'],
    ];

}