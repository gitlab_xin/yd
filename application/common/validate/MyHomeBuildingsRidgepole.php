<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * 我家-楼盘-栋
 */

namespace app\common\validate;

use think\Validate;

class MyHomeBuildingsRidgepole extends Validate
{
    protected $rule = [
        'ridgepole_id' => 'require|number|regex:\d{0,10}',
        'buildings_id' => 'require|number|regex:\d{0,10}',
        'name' => 'require|max:20',
    ];

    protected $field = [
        'ridgepole_id' => '栋ID',
        'name' => '标题',
        'buildings_id' => '楼盘ID',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['buildings_id','name'],
    ];

}
