<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:21
 */

namespace app\common\validate;

use think\Validate;

class HelpCenter extends Validate
{
    protected $rule = [
        'id' => 'require|integer|gt:0',
        'title' => 'require|max:18',
        'content' => 'require|max:490',
        'classify_id' => 'require|integer|gt:0',
    ];

    protected $field = [
        'title' => '标题',
        'content' => '内容',
        'classify_id' => '所属分类',
    ];

    protected $message = [
    ];

    protected $scene = [
        'addOrEdit' => ['title', 'content', 'classify_id'],
        'detail' => ['id']
    ];

}