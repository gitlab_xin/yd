<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
define('UPLOAD_PATH', ROOT_PATH . 'public' . DS . 'uploads');
define('HTTP_GET', '0');
define('HTTP_POST', '1');
define('G_HTTP', isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? "https://" : "http://");
define('G_HTTP_HOST', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "");
// http://localhost/project_name/public
defined('PUBLIC_PATH') or define('PUBLIC_PATH', dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']));

define('PRIVATE_KEY', 'DANKAL_CUBE');
define('API_PATH', dirname(G_HTTP . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']));
define('PRO_PIC', DS . 'proPic' . DS);
define('DCOLOR', DS . 'dcolor' . DS);
define('SCHEME_PIC', EXTEND_PATH . 'scheme_pic' . DS);
/**
 * 收口基料厚度
 */
define('CUSTOM_SHOUKOU_IN', 20);
/**
 * 收口基料+盖板厚度
 */
define('CUSTOM_SHOUKOU_IN_OUT', 23);
/**
 * 收口垫料(垫方宽度) 35
 */
define('CUSTOM_PADDING', 35);
/**
 * 自定义衣柜列最小宽度400
 */
define('CUSTOM_COLUMN_MIN_WIDTH', 400);
/**
 * 内侧板宽度 25mm
 */
define('INNER_WIDTH', 25);
/**
 * 自定义衣柜列宽度步进值 32mm
 */
define('CUSTOM_COLUMN_WIDTH_STEP', 32);
/**
 * 自定义衣柜列高度步进值 32mm
 */
define('CUSTOM_COLUMN_HEIGHT_STEP', 32);
/**
 * 自定义衣柜列最大宽度1200
 */
define('CUSTOM_COLUMN_MAX_WIDTH', 1200);
/**
 * 抽拉组件两侧各有10mm的空间
 */
define('DRAW_SPACE', 10);
/**
 * 外侧板宽度1 16mm
 */
define('SIDE_WIDTH_1', 16);
/**
 * 外侧板宽度2 25mm
 */
define('SIDE_WIDTH_2', 25);
/**
 * 标准宽度 400mm
 */
define('CUSTOM_KEY_WIDTH_400', 400);
/**
 * 标准宽度 784mm
 */
define('CUSTOM_KEY_WIDTH_784', 784);
/**
 * 自定义衣柜洞口最大高度 2900mm
 */
define('CUSTOM_MAX_HEIGHT', 2900);
/**
 * 自定义衣柜最低高度 2019mm
 */
define('CUSTOM_MIN_HEIGHT', 2019);
/**
 * 自定义衣柜板材最大高度 2403mm
 */
define('BOARD_MAX_HEIGHT', 2403);
/*
 * 自定义衣柜门框宽度 30mm
 */
define('DOOR_FRAME_WIDTH', 30);

function get_value($name, $defaultValue = '', $type = 'POST')
{
    $data = $type == 'POST' ? \think\Request::instance()->post($name) : \think\Request::instance()->get($name);
    return ((isset($data)) && ($data !== '')) ? $data : $defaultValue;
}

function is_post()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST' ? true : false;
}

function get_json_input($key = null)
{
    $inputs = \think\Request::instance()->param();
    if ($key) {
        return isset($inputs[$key]) ? $inputs[$key] : null;
    } else {
        return \think\Request::instance()->param();
    }
}

function randString($len = 4)
{
    $chars = str_repeat('0123456789', $len);
    $chars = str_shuffle($chars);
    $str = substr($chars, 0, $len);
    return $str;
}

//封装HTTP请求
function sendRequest($method, $url, $requestBody)
{
    $ch = curl_init($url);
    if ($method == HTTP_GET) {
        curl_setopt_array($ch, array(
            CURLOPT_HTTPGET => true,
            CURLOPT_RETURNTRANSFER => true,
        ));
    } elseif ($method == HTTP_POST) {
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $requestBody
        ));
    }
    // Send the request
    $response = curl_exec($ch);
    // Check for errors
    if ($response === false) {
        die(curl_error($ch));
    }

    return $response;
}

/**
 * @author: Airon
 * @time: 2017年2月25日 9:47
 * description:获取唯一名称
 * @param string $path_name
 * @return string
 */
function get_only($path_name = "")
{
    $charId = strtoupper(md5(uniqid(mt_rand(), true)));

    $hyphen = chr(45);// "-"

    $uuid = chr(123)

        . substr($charId, 0, 8) . $hyphen

        . substr($charId, 8, 4) . $hyphen

        . substr($charId, 12, 4) . $hyphen

        . substr($charId, 16, 4) . $hyphen

        . substr($charId, 20, 12)

        . chr(125);

    return strtoupper(md5($uuid)) . '.' . fileSuffix($path_name);

}

/**
 * @author: Airon
 * @time: 2017年3月9日
 * description:获取文件后缀名
 * @param $filename
 * @return string
 */
function fileSuffix($filename)
{

    return strtolower(trim(substr(strrchr($filename, '.'), 1)));

}

function get_token()
{
    return \think\Request::instance()->header('token');
}

function log_file($content, $title = 'LOG', $filename = 'log_file')
{
    try {
        $titleShow = (strlen($title) > 30) ? substr($title, 0, 27) . '...' : $title;
        $spaceNum = (66 - strlen($titleShow)) / 2;
        $titleShow = '=' . str_repeat(' ', intval($spaceNum)) . $titleShow . str_repeat(' ', ceil($spaceNum)) . '=';

        $time = date('Y-m-d H:i:s');
        $content = var_export($content, true);

        $logContent = <<<EOT
====================================================================
{$titleShow}
====================================================================
time:     {$time}
title:    {$title}
--------------------------------------------------------------------
content:  \n{$content}\n\n\n
EOT;

        $logPath = LOG_PATH;
        $logName = $filename . date('Ymd') . '.log';
        if (!is_dir($logPath)) {
            mkdir($logPath);
        }
        $logFile = fopen($logPath . $logName, "a");
        fwrite($logFile, $logContent);
        fclose($logFile);
    } catch (\Exception $e) {
        // do nothing
    }
}

function get_random_num_str($length = 4, $withZero = false, $canBeRepeated = true)
{
    $baseChars = "123456789";
    if ($withZero) {
        $baseChars .= "0";
    }
    if ($canBeRepeated || $length > 10) {
        $baseChars = str_repeat($baseChars, $length);
    }
    $chars = str_shuffle($baseChars);
    $output = substr($chars, 0, $length);
    return $output;
}

function getCurl($url)
{
    try {
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($curlHandle);
        curl_close($curlHandle);
        return $result;
    } catch (\Exception $e) {
        return null;
    }
}

/**
 * @author: Rudy
 * @time: 2017年月
 * description:将结果集对象返回一个数组
 * @param $obj
 * @return mixed
 */
function objToArray($obj)
{
    return collection($obj)->toArray();
}

/**
 * 自定义结构序列化, 把字典转换为 key:value|key:value 形式
 * @param array $data
 *
 * @return string
 */
function DIYSerialize(Array $data)
{
    if (empty($data)) {
        return "";
    }
    $temp = [];
    foreach ($data as $key => $value) {
        $temp[] = $key . ":" . $value;
    }
    $result = implode("|", $temp);
    return $result;
}

/**
 * 自定义结构反序列化, 把 key:value|key:value 形式的字符串转换为字典
 * @param $string
 *
 * @return array
 */
function DIYUnSerialize($string)
{
    if (empty($string)) {
        return [];
    }
    $dataArr = explode('|', $string);
    $result = [];
    foreach ($dataArr as $item) {
        $temp = explode(':', $item);
        $result[$temp[0]] = $temp[1];
    }

    return $result;
}

function uuid($prefix = '')
{
    $chars = md5(uniqid(mt_rand(), true));
    $uuid = substr($chars, 0, 8);
    $uuid .= substr($chars, 8, 4);
    $uuid .= substr($chars, 12, 4);
    $uuid .= substr($chars, 16, 4);
    $uuid .= substr($chars, 20, 12);
    return $prefix . $uuid;
}

function getSchemeColorArrH($color)
{
    $array = [
        "XiaTeHuiBuWenh.jpg",
        "XiaTeLiMuh.jpg",
        "XiaTeQianHuTaoh.jpg",
        "XiaTeShenHuTaoh.jpg",
        "XiaWangNuanBaih.jpg",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "hs20h.jpg",
        "hs21h.jpg",
        "hs22h.jpg",
        "hs23h.jpg",
        "hs24h.jpg",
        "hs25h.jpg",
        "hs26h.jpg",
        "27",
        "28",
        "29",
        "30",
        "31",
        "32",
        "33",
        "34",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42",
        "43",
        "hs44h.jpg"
    ];
    return $array[intval($color)];
}

function getPlatePrice($index)
{
    $platePriceArr = [
        350,
        272,
        190,
        160
    ];

    return $platePriceArr[intval($index)];
}

function getRandomInt()
{
    return mt_rand() % 876;
}

function GetBZDoorPicDir()
{
    return 'bz_' . (getRandomInt() % 20);
}

function change2SchemeBean($scheme)
{
    return false ? new \app\common\model\SchemeBean() : $scheme;
}

function change2ProductBean($product)
{
    return false ? new \app\common\model\ProductBean() : $product;
}

function is_starts_with($str, $pattern)
{
    return strpos($str, $pattern) === 0;
}

function str_has($str, $pattern)
{
    return strpos($str, $pattern) !== false;
}

function formatMoney($money, $decimals = 2)
{
    return number_format(floatval($money), $decimals, '.', '');
}

/**
 * 进位
 */
function getLength($l)
{
    return ceil($l / 10) * 10;
}

/**
 * 进位
 */
function getMoney($l)
{
    return ceil($l * 10) / 10;
}

/**
 * 计算16:9电视长边
 *
 * @param x
 * @return float
 */
function GetLongSide16Bi9($x)
{
    $temp = (16 / 9) * (16 / 9);
    $longerSide = sqrt((($x * 25.4) * ($x * 25.4) * $temp) / (1 + $temp));
    return $longerSide;
}