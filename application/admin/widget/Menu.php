<?php

/**
 *  布居
 * @file   menu.php  
 * @date   2016-9-2 16:18:45 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\widget;

use app\admin\model\AdminMenu;

class Menu {

    public $user_id;

    public function __construct() {
        if (!session('user_id')) {
            return false;
        }
        $this->user_id = session('user_id');
    }

    public function index() {

        $model = new AdminMenu();
        $menu = $model->getMyMenu($this->user_id, 1);
        $menuTree = list_to_tree($menu);
        
        $html = '<ul class="nav nav-list">';
        $html .=$this->menu_tree($menuTree);
        $html .= "
                </ul>";
        return $html;
    }

    public function menu_tree($tree) {
        $html = '';

        if (is_array($tree)) {
            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    $url = !empty($val["action"])?request()->root().'/admin/' . $val['controller'] . '/' . $val['action']:'#';
                    $id = empty($val["id"])?$val["name"]:$val["id"];
                    $icon = empty($val['icon'])?"fa-caret-right":$val['icon'];
                    $active = (lineToLow($val['controller']) == strtolower(request()->controller()) && strtolower($val['action'] == request()->action()))?'active':'';

                    if (isset($val['_child'])) {

                        $html.=' 
                            <li class="">
                            <a href="' . $url . '" class="dropdown-toggle">
                                <i class="menu-icon fa ' . $icon . '"></i>
                                <span class="menu-text"> ' . $title . ' </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                            ';

                        $html.=$this->menu_tree($val['_child']);

                        $html.='              
                            </ul>
                        </li>
                        ';
                    } else {

                        $html.='
                    <li class = "' . $active . '">
                    <a href = "' . $url . '">
                    <i class = "menu-icon fa ' . $icon . '"></i>
                    <span class = "menu-text"> ' . $title . ' </span>
                    </a>
                    <b class = "arrow"></b>
                    </li>
                    ';
                    }
                }
            }
        }

        return $html;
    }

}
