<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 10:15
 */

namespace app\admin\widget;

use think\Config;
use think\Request;
use think\Loader;


class FormView
{
    public $field;//字段名
    public $alias;//字段别名
    public $input_type = '';//输入框类型
    public $input_params = [];//参数
    public $default_value = null;//默认值
    public static $replace = [];

    public function __construct($field, $alias, $input_type)
    {
        $this->field = $field;
        $this->alias = $alias;
        $this->input_type = $input_type;

        if (static::$replace == []) {
            $request = Request::instance();
            $base = $request->root();
            $root = strpos($base, '.') ? ltrim(dirname($base), DS) : $base;
            if ('' != $root) {
                $root = '/' . ltrim($root, '/');
            }
            static::$replace = [
                'root' => $root,
                'url' => $base . '/' . $request->module() . '/' . Loader::parseName($request->controller()),
                'static' => $root . '/static',
                'css' => $root . '/static/css',
                'js' => $root . '/static/js',
            ];
        }
    }

    public function defaultInput($default = '')
    {
        return $this->{$this->input_type}($default);
    }

    public function display($default = '')
    {
        return $this->{$this->input_type . 'Display'}($default);
    }

    public function hidden($default = '')
    {
        return '<input type="hidden" class="col-xs-10 col-sm-5" name="' . $this->field . '" value="' . $default . '"/>';
    }

    public function hiddenDisplay($default = '')
    {
        return $this->textDisplay($default);
    }

    public function text($default = '')
    {
        return '<div class="form-group"><label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label><div class="col-sm-9"><input type="text" class="col-xs-10 col-sm-5" name="' . $this->field . '" value="' . $default . '"/></div></div><div class="space-4"></div>';
    }

    public function textDisplay($default = '')
    {
        return '<div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">' . $this->alias . '</label>
                    <div class="col-sm-9">
                        <input type="text" class="col-xs-10 col-sm-5" name="' . $this->field . '" value="' . $default . '" disabled/>
                    </div>
                </div>
                <div class="space-4"></div>';
    }

    public function image($default = '')
    {
        $imgPre = Config::get('qiniu.BucketDomain');
        return '<div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label>
                    <div class="fileinput-button col-sm-2">
                        <div class="thumbnail" id="' . $this->field . '" style="border: 0" >
                            <div class="cover">' .
            ($default != null ?
                '<img src="' . $imgPre . $default . '?imageView2/0/w/360/h/360" />' :
                '<img src="__STATIC__/img/add_img.png" />')
            . '</div>
                        </div>
                        <input class="fileupload" type="file" name="file" data-type="' . $this->field . '" data-input="' . $this->field . '_input"/>
                    </div>
                </div>
                <input type="hidden" name="' . $this->field . '" id="' . $this->field . '_input" value="' . $default . '"/>
                <div class="space-4"></div>';
    }

    public function imageDisplay($default = '')
    {
        $imgPre = Config::get('qiniu.BucketDomain');
        return '<div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label>
                    <div class="col-sm-9">
                        <img class="img-thumbnail" src="' . $imgPre . $default . '"/>
                    </div>
                </div>
                <div class="space-4"></div>';
    }

    public function file($default = '', $hidden = 1)
    {
        return '<div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label>
                    <div class="fileinput-button col-sm-2" id="' . $this->field . '_container">
                        <div class="thumbnail" style="border: 0" >
                            <div class="cover" id="' . $this->field . '_pickfile">
                                <img src="__STATIC__/img/add_img.png" />
                            </div>
                        </div>
                    </div>
                </div>' . ($hidden == 1 ?
                '<input type="hidden" id="' . $this->field . '_input" name="' . $this->field . '" value="' . $default . '"/>' : ''
            ) . '<div class="space-4"></div>';
    }

    public function fileDisplay($default = '')
    {
        return $this->textDisplay($default);
    }

    public function filesDisplay($default = '')
    {
        return $this->textDisplay($default);
    }

    public function radio($default = '')
    {
        $radios = '';
        foreach ($this->input_params as $k => $v) {
            $check = $default === '' ?
                ($k == $this->default_value ? 'checked' : '') :
                ($k == $default ? 'checked' : '');
            $radios .= '<label class="checkbox-inline"><input type="radio" class="__'.$this->field.'" name="'.$this->field.'" value="' . $k . '" '. $check .'>' . $v . '</label>';
        }
        return '<div class="form-group"><label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label><div class="col-sm-9">' . $radios . '</div></div><div class="space-4"></div>';
    }

    public function files($default = '')
    {
        return $this->file($default, 0);
    }

    public function textArea($default = '')
    {
        return '<div class="form-group"><label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label><div class="col-sm-9"><textarea class="col-xs-10 col-sm-5" name="' . $this->field . '" rows="6">' . $default . '</textarea></div></div><div class="space-4"></div>';
    }

    public function textAreaDisplay($default = '')
    {
        return '<div class="form-group"><label class="col-sm-3 control-label no-padding-right">' . $this->alias . ':</label><div class="col-sm-9"><textarea class="col-xs-10 col-sm-5" name="' . $this->field . '" rows="6">' . $default . '</textarea></div></div><div class="space-4"></div>';
    }
}
