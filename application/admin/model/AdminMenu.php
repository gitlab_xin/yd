<?php

/**
 *
 * @file   Menu.php
 * @date   2016-9-1 15:48:53
 * @author Zhenxun Du<5552123@qq.com>
 * @version    SVN:$Id:$
 */

namespace app\admin\model;

use think\Db;
use think\Request;
use think\Model;
use think\Config;

class AdminMenu extends Model
{

    public $display = array('1' => '显示', '2' => '不显示');

    protected $autoWriteTimestamp = true;

    public static function build()
    {
        return new self();
    }

    /**
     * 获取当前方法名
     * @return type
     */
    public function getName()
    {
        $where = array();
        $request = Request::instance();
        $where['controller'] = upToLine($request->controller());
        $where['action'] = $request->action();
        $res = $this->where($where)->field(['id', 'name', 'parent_id'])->find();
        return $res['name'];
    }

    public function getInfo()
    {
        $where = array();
        $request = Request::instance();
        $where['controller'] = $request->controller();
        $where['action'] = $request->action();
        $res = $this->where($where)->field('id,name,parent_id')->find();
        return $res;
    }

    /**
     * 获取上级方法名
     * @return boolean
     */
    public function getParentNname()
    {

        $info = $this->getInfo();
        if ($info != null && $info->parent_id) {
            return $this->where('id', $info->parent_id)->value('name');
        } else {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:我的菜单
     * @param $user_id
     * @param null $display
     * @return array|bool|false|\PDOStatement|string|\think\Collection|Model
     */
    public function getMyMenu($user_id, $display = null)
    {
        $where = array();
        if ($user_id != 1) {
            $res = DB::name('admin_group_access')
                ->alias('t1')
                ->field('GROUP_CONCAT(DISTINCT `menu_id`) as `rules`')
                ->join(Config::get('database.prefix') . 'admin_group_menu t2', 't1.admin_group_id = t2.group_id', 'LEFT')
                ->where(['t1.admin_id' => $user_id])
                ->find();
            if (!$res['rules']) {
                return false;
            }
//            $tmp = '';
//            foreach ($res as $k => $v) {
//                $tmp .=$v['rules'] . ',';
//            }
//
//            $menu_ids = trim($tmp, ',');
            $where['id'] = ['in', $res['rules']];
        }


        if ($display) {
            $where['display'] = $display;
        }
        $res = objToArray(self::build()
            ->field(['create_time', 'update_time'], true)
            ->where($where)->order('list_order asc')->select());

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:保存或更新菜单
     * @param $data
     * @param int $id
     * @return $this
     */
    public static function saveMenu($data, $id = 0)
    {
        return $id == 0 ? self::create($data, true) : self::update($data, ['id' => $id], true);
    }

}
