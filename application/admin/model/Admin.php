<?php

/**
 *
 * @file   admin.php
 * @date   2016-8-30 15:22:57
 * @author Zhenxun Du<5552123@qq.com>
 * @version    SVN:$Id:$
 */

namespace app\admin\model;

use think\Model;
use think\Request;

class Admin extends Model
{

    protected $autoWriteTimestamp = true;
    public $status = array(1 => '无效', 2 => '有效');
    public $display = array(1 => '选', 2 => '不选');

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:实例化自身
     * @return Admin
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:初始化时添加插入事件,为每个刚添加的管理员密码加密md5
     */
    protected static function init()
    {
        self::beforeInsert(function ($admin) {
            $admin->password = md5($admin->password);
        });
        self::beforeUpdate(function ($admin) {
            if (property_exists($admin, 'password') && $admin->password != null) {
                $admin->password = md5($admin->password);
            }
        });
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:登录更新
     * @param $id
     * @param array $data
     * @return Admin
     */
    public static function editInfo($id, $data = array())
    {
        $data['last_login_time'] = time();
        $data['last_login_ip'] = ip2long(Request::instance()->ip());
        return self::update($data, ['id' => $id], true);
    }

}
