<?php

/**
 *  
 * @file   AdminGroup.php  
 * @date   2016-8-30 18:22:31 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\model;

use think\Model;
use think\Db;

class AdminGroupMenu extends Model {
    
    /**
     * @return AdminGroup
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:获取menuids
     * @param $groupId
     * @return mixed
     */
    public static function getMenuIdsByGroupId($groupId)
    {
        return Db::name('admin_group_menu')->field(['GROUP_CONCAT(menu_id)'=>'ids'])->where(['group_id'=>$groupId])->find();
    }

}
