<?php

namespace app\admin\model;

use think\Model;

class AdminGroupAccess extends Model
{

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:实例化自身
     * @return Admin
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:获取管理员所在的组
     * @param $admin_id
     * @return mixed
     *
     */
    public static function getUserGroups($admin_id)
    {
        $res = self::build()
            ->field('GROUP_CONCAT(admin_group_id) as group_ids')
            ->where(['admin_id'=>$admin_id])
            ->find();
        return $res->group_ids;
    }
}
