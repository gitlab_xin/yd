<?php

/**
 *  
 * @file   AdminLog.php  
 * @date   2016-10-9 17:29:09 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */
namespace app\admin\model;

use think\Model;

class AdminLog extends Model
{
    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:实例化自身
     * @return Admin
     */
    public static function build()
    {
        return new self();
    }
}
