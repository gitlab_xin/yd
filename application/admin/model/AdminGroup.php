<?php

/**
 *  
 * @file   AdminGroup.php  
 * @date   2016-8-30 18:22:31 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\model;

use think\Model;

class AdminGroup extends Model {
    
    protected $autoWriteTimestamp = true;


    /**
     * @return AdminGroup
     */
    public static function build()
    {
        return new self();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:获取所有分组并以key-value形式整合
     * @return array
     */
    public static function getGroups()
    {

        $res = objToArray(self::build()->field(['id','name'])->select());
        if($res){
            $res = array_column($res,'name','id');
        }
        return $res;
    }

    public function getGroupName($group_id) {
        return self::build()->where(['id' => $group_id])->value('name');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:保存或修改管理组
     * @param $data
     * @param int $id
     * @return $this|false|int
     */
    public static function saveGroup($data,$id = 0)
    {
        if($id == 0){
            return self::build()->allowField(true)->save($data);
        }else{
            return self::build()->allowField(true)->where(['id'=>$id])->update($data);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:删除管理组时把中间表信息也删除
     * @param int $admin_group_id
     */
    public static function delGroupAdmins($admin_group_id = 0)
    {
        if($admin_group_id != 0){
            AdminGroupAccess::build()->where(['admin_group_id'=>$admin_group_id])->delete();
        }
    }
}
