<?php

/**
 * @author: Rudy
 * @time: 2017年7月11日 15:13
 * @description:定义行为
 */
return [
    //用户是否已经登录和权限操作验证
    'module_init' => [
        'app\\admin\\behavior\\CheckAuth',
    ],
    //response结束后进行日志写入操作
    'response_end' => [
        'app\\admin\\behavior\\Log'
    ]
];
