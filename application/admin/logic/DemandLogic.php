<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/26
 * Time: 16:44
 */

namespace app\admin\logic;

use think\Db;
use think\Url;
use app\common\tools\wechatUtil;
use app\common\tools\RefundUtil;
use app\common\model\Demand as DemandModel;
use app\common\model\DemandOrder as DemandOrderModel;
use app\common\validate\Demand as DemandValidate;

class DemandLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月5日
     * description:获取全部
     * @param $data
     * @return \think\Paginator
     *
     */
    public function getAll($data)
    {
        $where = ['d.is_deleted'=>0];
        $alias = 'd';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }

        return DemandModel::build()->alias($alias)->join('user u','d.user_id = u.user_id','LEFT')
            ->join('demand_banner b','b.demand_id = d.demand_id','LEFT')
            ->field(['d.demand_id','d.title','d.classify','d.style_name','d.material_name','d.reward','d.create_time','d.status','u.username','b.banner_id'])
            ->where($where)->order(['demand_id'=>'DESC'])->paginate(20,false,['query'=>$data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:获取单个
     * @param $id
     * @return null|static
     *
     */
    public function getDetail($id)
    {
        return DemandModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:编辑需求
     * @param $data
     * @param $id
     * @return array
     *
     */
    public function edit($data,$id)
    {
        $validate = new DemandValidate();

        $res = ['method'=>'error','msg'=> '编辑失败'];

        if(!$validate->scene('edit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(DemandModel::update($data,['demand_id'=>$id],true)){
                $res['method'] = 'success';
                $res['msg'] = '编辑成功';
                $res['redirect'] = Url::build('/admin/demand/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:删除需求
     * @param $id
     * @return array
     */
    public function del($id)
    {
        $res = ['method'=>'error','msg'=> '删除失败'];

        Db::startTrans();
        try{
            /*
             * 1.更改需求状态 is_deleted = 1
             * 2.更改订单状态 refund_no
             * 3.退款
             */
            $where = ['demand_id'=>$id];

            $model = DemandModel::get($id);
            $model->is_deleted = 1;
            $model->status = 2;

            if(!$model->isUpdate()->save()){
                throw new \Exception('需求状态更新失败');
            }

            $randomNum = wechatUtil::getRandomString(32);
            $order = DemandOrderModel::build()->where($where)->find();
            if($order != null && $order->refund_no == ''){
                if(!$order->isUpdate()->save(['refund_no'=>$randomNum,'refund_time'=>time()],$where)){
                    throw new \Exception('需求订单状态更新失败');
                }
                //todo 测试金额
                if(!RefundUtil::getInstance()->setProps(['payType'=>$order->pay_type,'sum'=>0.01,'fee'=>0.01,'billNum'=>$order->bill_no])->exec()){
                    throw new \Exception('退款失败');
                }
            }

            Db::commit();
            $res['method'] = 'success';
            $res['msg'] = '删除成功';
        }catch (\Exception $e){
            Db::rollback();
//            dump($e->getTrace());exit;
            $res['msg'] = $e->getMessage();
        }

        return $res;
    }
}