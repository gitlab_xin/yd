<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:53
 */

namespace app\admin\logic;


use think\Url;
use app\common\model\CustomizedCabinetRender as RenderModel;
use app\common\model\CustomizedCabinetSceneRender as SceneRenderModel;
use app\common\validate\CustomizedCabinetRender as RenderValidate;

class CustomizedCabinetRenderLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll()
    {
        return RenderModel::build()->paginate(20);
    }

    public function getList()
    {
        return RenderModel::build()->field(['render_id', 'name', 'shadowMapType', 'toneMapping'])->select()->toArray();
    }

    public function getSeleted($id)
    {
        return SceneRenderModel::build()->where(['scene_id' => $id])->column('render_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return RenderModel::get($id);
    }


    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new RenderValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (RenderModel::saveRender($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_render/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return RenderModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function addScene($data)
    {
        $model = new SceneRenderModel();
        $scene_id = $data['scene_id'];
        $model->where(['scene_id' => $data['scene_id']])->delete();
        if (isset($data['render_id'])) {
            $model->saveAll(array_map(function ($v) use ($scene_id) {
                return ['render_id' => $v, 'scene_id' => $scene_id];
            }, $data['render_id']));
        }
        return ['method' => 'success', 'msg' => '修改成功'];
    }
}