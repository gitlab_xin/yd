<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/21
 * Time: 9:54
 */

namespace app\admin\logic;

use app\common\validate\ShopProductStandard as StandardValidate;
use app\common\model\ShopProductStandard as StandardModel;
use app\common\model\ShopChangePriceRecord as PriceRecordModel;
use think\Db;
use think\Url;
use think\Config;

class ShopProductStandardLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:获取单个规则信息
     * @param $id
     * @return null|static
     */
    public function getStandard($id)
    {
        return StandardModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:获取所有规格
     * @return \think\Paginator
     */
    public function getStandards()
    {
        return StandardModel::build()->where(['is_deleted' => '0'])->order('product_id DESC')->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:根据商品id获取规则
     * @param $product_id
     * @return \think\Paginator
     */
    public function getStandardsByProductId($product_id)
    {
        return StandardModel::build()
            ->alias('ps')
            ->join('shop_product p', 'p.product_id = ps.product_id', 'LEFT')
            ->where(['p.is_deleted' => '0', 'ps.is_deleted' => '0', 'ps.product_id' => $product_id])
            ->field([
                'ps.name standard_name', 'p.name product_name', 'ps.standard_id','ps.product_id','ps.status',
                'ps.stocks','ps.price'
            ])
            ->order('ps.standard_id DESC')
            ->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:添加或编辑规格
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditStandard($data, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $validate = new StandardValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } else {
            if ($id != 0 && $data['price'] != $data['original_price'] && $data['reason'] == '') {
                //如果是更新状态并且价格有变动而每天填写变动原因
                $res['msg'] = '价格有变动请填写变动原因';
                $flag = false;
            }
            Db::startTrans();
            try{
                if (!isset($flag) && StandardModel::saveStandard($data, $id)) {
                    $res['method'] = 'success';
                    $res['msg'] = $handle . '成功';
                    $res['redirect'] = Url::build('/admin/shop_product_standard/index',['product_id'=>$data['product_id']]);
                    if ($id != 0 && $data['price'] != $data['original_price']) {
                        //如果价格有变动
                        $data['standard_id'] = $id;
                        $data['change_price'] = $data['price'];
                        unset($data['id']);
                        if(!PriceRecordModel::create($data, true)){
                            throw new \Exception('数据存储失败');
                        }
                    }
                    Db::commit();
                }
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }

        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:删除规格
     * @param $id
     * @return array
     */
    public function delStandard($id)
    {
        return StandardModel::update(['is_deleted' => 1], ['standard_id' => $id,'status' => '0']) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败，请先选择其他默认规格再进行删除'];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月21日
     * description:查看是否有超过限制
     * @param $product_id
     * @return int|string
     */
    public function checkLimit($product_id)
    {
        $max = Config::get('mall.product')['max_standard'];
        if(is_numeric($max) && $max > 0){
            $count = StandardModel::build()->where(['product_id'=>$product_id,'is_deleted'=>'0'])->count();
            return $count >= $max ? $max:0;
        }else{
            return 0;
        }
    }

    public function change($data){
        if ($data['status'] == '1'){
            StandardModel::build()
                ->where(['product_id' => $data['product_id']])
                ->update([
                    'status' => '0',
                    'update_time' => time()
                ]);
        }

        if ($data['status'] == '0'){
            $info = StandardModel::build()
                ->where(['product_id' => $data['product_id'],'standard_id' => ['<>',$data['standard_id']],'status' => '1','is_deleted' => '0'])
                ->find();
            if (empty($info)) {
                return '至少有一个默认规格';
            }
        }

        StandardModel::build()
            ->where(['product_id' => $data['product_id'], 'standard_id' => $data['standard_id']])
            ->update([
                'status' => '1',
                'update_time' => time()
            ]);

        return true;
    }
}