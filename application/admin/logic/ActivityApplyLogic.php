<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 9:48
 */

namespace app\admin\logic;

use app\common\model\ActivityApply as ApplyModel;
use app\common\model\Activity as ActivityModel;

class ActivityApplyLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取所有活动信息,只取id,tite
     * @return mixed
     */
    public function getActivities()
    {
        return objToArray(ActivityModel::build()->field(['activity_id','title'])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取所有申请人
     * @param $data
     * @return \think\Paginator
     */
    public function getApplications($data)
    {

        $alias = 'app';
        $fields = [
            'app.id','app.name','app.phone','app.gender','app.status','a.title'
        ];
        $where = [];
        $this->handleSearch($where,$data,$alias.'.');

        $res = ApplyModel::build()
            ->alias($alias)
            ->where($where)
            ->field($fields)
            ->join('yd_activity a','app.activity_id = a.activity_id')
            ->paginate(20,false,['query'=>$data]);

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取单个申请信息
     * @param $id
     * @return null|static
     */
    public function getApplication($id)
    {
        return ApplyModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:修改审核状态
     * @param $id
     * @param $status yes|no
     * @return array
     */
    public function changeStatus($id,$status)
    {
        $handle = $status == 'yes'?'通过审核':'拒绝审核';
        return ApplyModel::update(['status' => $status],['id'=>$id])?['method' => 'success' ,'msg' => $handle.'成功']:['method' => 'error','msg' => $handle.'失败'];
    }
}