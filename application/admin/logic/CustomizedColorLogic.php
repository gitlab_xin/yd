<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:53
 */

namespace app\admin\logic;

use app\api\logic\SchemeBaseLogic;
use think\Db;
use think\Url;
use app\common\tools\RedisUtil;
use app\common\model\CustomizedColor as ColorModel;
use app\common\model\CustomizedCabinetColor as CabinetColorModel;
use app\common\model\CustomizedCabinetDoorColor as CabinetDoorColorModel;
use app\common\validate\CustomizedColor as ColorValidate;

class CustomizedColorLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取所有数据
     */
    public function getAll()
    {
        $result = ColorModel::build()->where(['is_deleted' => 0])->order(['is_default' => 'DESC'])->select()->toArray();
        array_walk($result, function (&$v) {
            if (isset($v['door_img_src']) && $v['door_img_src'] != '') {
                $v['door_img_src'] = explode(',', $v['door_img_src'])[0];
            }
        });
        //$this->updateCache($result);
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加或编辑
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new ColorValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (ColorModel::saveColor($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/customized_color/index');

            $logic = new SchemeBaseLogic();
            $logic->flushMongoCache();
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除帮助中心
     * @param $id
     * @return array
     */
    public function del($id)
    {
        $where = ['color_id' => $id];
        $res = ['method' => 'success', 'msg' => '删除成功'];
        Db::startTrans();
        try {
            CabinetColorModel::destroy($where);
            CabinetDoorColorModel::destroy($where);
            ColorModel::update(['is_deleted' => 1], ['color_id' => $id]);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            $res['msg'] = '删除失败，' . $e->getMessage();
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return ColorModel::get($id);
    }

    private function updateCache($result)
    {
        $instance = RedisUtil::getInstance();
        $instance->delete('scheme_color_no');
        foreach ($result as $row) {
            $instance->hSet('scheme_color_no', $row['color_no'], $row['color_name']);
        }
    }
}