<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:52
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\WebglScene as WebglSceneModel;
use app\common\model\WebglModule as WebglModuleModel;
use app\common\validate\WebglScene as WebglSceneValidate;

class WebglLogic
{

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll()
    {
        return WebglSceneModel::build()->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return WebglSceneModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new WebglSceneValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (WebglSceneModel::saveScene($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/webgl/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        WebglModuleModel::build()->where(['scene_id' => $id])->delete();
        return WebglSceneModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }
}