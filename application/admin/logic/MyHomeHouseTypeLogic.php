<?php

namespace app\admin\logic;

use app\common\model\MyHomeDecoratePoint;
use app\common\model\MyHomeHouseType as HouseTypeModel;
use app\common\validate\MyHomeHouseType as HouseTypeValidate;

/**
 * Class MyHomeHouseTypeLogic
 * @package app\admin\logic
 */
class MyHomeHouseTypeLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型
     * @param null $id
     * @return null|static
     */
    public function getHouse($id = null)
    {
        return $id != null > 0 ? HouseTypeModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @return false|static[]
     */
    public function getAllHouse()
    {
        $lists = HouseTypeModel::build()->where(['is_deleted'=>0])->paginate(20);
        return $lists;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:添加或编辑户型
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEditHouse($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new HouseTypeValidate();
        if (!$validate->scene('insertOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (HouseTypeModel::saveHouse($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:删除户型
     * @param $id
     * @return array
     */
    public function delHouse($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = HouseTypeModel::get($id);
        if ($model != null) {
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (HouseTypeModel::build()->update(['is_deleted'=>1],['house_id'=>$id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取某户型所有装修亮点
     * @param $house_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getDecorate($house_id)
    {
        return MyHomeDecoratePoint::build()->alias('d')
            ->join('yd_my_home_house_decorate hd', 'hd.decorate_id = d.decorate_id')
            ->join('yd_my_home_house_type h', 'h.house_id = hd.house_id')
            ->where(['hd.house_id' => $house_id])
            ->field(['d.decorate_id', 'd.name', 'd.sub_title'])
            ->select();
    }
}
