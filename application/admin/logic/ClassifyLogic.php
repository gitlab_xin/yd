<?php

namespace app\admin\logic;

use app\common\model\ShopClassify as ClassifyModel;
use app\common\validate\Classify as ClassifyValidate;

/**
 * Class ClassifyLogic
 * @package app\admin\logic
 */
class ClassifyLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:获取分类
     * @param null $id
     * @return null|static
     */
    public function getClassify($id = null)
    {
        return $id != null > 0 ? ClassifyModel::get($id) : null;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:
     * @return array|null
     */
    public function getAllClassify()
    {
        $lists = ClassifyModel::all();
        return $lists == null ? null : nodeTree(objToArray($lists), 0, 0, 'classify_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:获取下拉单分类
     * @return array
     */
    public function getSelectClassify()
    {

        return ClassifyModel::getSelectClassify();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:添加或编辑分类
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEditClassify($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new ClassifyValidate();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (ClassifyModel::saveClassify($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:删除分类**该分类下无子分类,该分类下无商品**
     * @param $id
     * @return array
     */
    public function delClassify($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = ClassifyModel::get($id);
        if ($model != null) {
            //1.检查下面是否有子分类,如果有的话,提示不能删除
            if (ClassifyModel::checkChildExist($id)) {
                $res['msg'] = '请先删除子分类';
                return $res;
            }
            //2.检查分类下面是否存在产品,如果有的话,提示不能删除
            if (ClassifyModel::checkProductsExist($id)) {
                $res['msg'] = '请先删除分类下的商品';
                return $res;
            }
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (ClassifyModel::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
