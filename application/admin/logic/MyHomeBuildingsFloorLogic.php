<?php

namespace app\admin\logic;

use app\common\model\MyHomeBuildingsFloor as FloorModel;
use app\common\validate\MyHomeBuildingsRidgepole;
use think\Db;

/**
 * Class MyHomeBuildingsFloorLogic
 * @package app\admin\logic
 */
class MyHomeBuildingsFloorLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取层
     * @param null $id
     * @return null|static
     */
    public function getInfo($id = null)
    {
        return $id != null > 0 ? FloorModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取层列表
     * @param $buildings_id
     * @param $ridgepole_id
     * @return array
     */
    public function getList($buildings_id, $ridgepole_id)
    {
        return FloorModel::build()->where(['is_deleted' => 0, 'buildings_id' => $buildings_id, 'ridgepole_id' => $ridgepole_id])->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:添加或编辑层
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEdit($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new MyHomeBuildingsRidgepole();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (FloorModel::addOrEdit($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @param $id
     * @return array
     */
    public function del($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该层'];

        $model = FloorModel::get($id);
        if ($model != null) {

            if (FloorModel::build()->update(['is_deleted' => 1], ['floor_id' => $id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
