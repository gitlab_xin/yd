<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:52
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\WebglScene as WebglSceneModel;
use app\common\model\WebglModule as WebglModuleModel;
use app\common\validate\WebglModule as WebglModuleValidate;

class WebglModuleLogic
{

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll($scene_id)
    {
        return WebglModuleModel::build()->where(['scene_id' => $scene_id])->paginate(20,false,['query' => ['scene_id' => $scene_id]]);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return WebglModuleModel::get($id);
    }

    public function getSceneName($id)
    {
        return WebglSceneModel::build()->where(['id' => $id])->value('name');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new WebglModuleValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (WebglModuleModel::saveModules($data)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/webgl_module/index', ['scene_id' => $data['scene_id']]);
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return WebglModuleModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }
}