<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 14:38
 */

namespace app\admin\logic;

use app\common\model\ForumArticle as ArticleModel;
use app\common\model\ForumComment as CommentModel;
use app\common\model\ForumPraise as LikeModel;
use app\common\validate\ForumArticle as ArticleValidate;
use think\Db;
use think\Url;

class ForumArticleLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:获取所有帖子(区分已删除和未删除)
     * @param $data
     * @param int $is_deleted
     * @return \think\Paginator
     */
    public function getArticles($data, $is_deleted = 0)
    {
        $where = ['a.is_deleted' => $is_deleted];
        //查询条件整合
        $alias = 'a';
        if (count($data) > 0) {
            $this->handleSearch($where, $data, $alias . '.');
        }
        //此处判断是否需要关联用户表
        $model = ArticleModel::build()->alias($alias);

        //所需字段
        $field = [
            $alias . '.article_id',
            $alias . '.title',
            $alias . '.classify',
            $alias . '.is_essence',
            $alias . '.is_top',
            $alias . '.is_official',
            $alias . '.praise_count',
            $alias . '.comment_count',
            $alias . '.collect_count',
            $alias . '.user_id',
            'u.username'
        ];
        return $model
            ->field($field)
            ->where($where)
            ->join('yd_user u', 'a.user_id = u.user_id', 'LEFT')
            ->order(['a.is_essence' => 'DESC', 'a.is_top' => 'DESC', 'a.create_time' => 'DESC'])
            ->paginate(20, false, [
                'query' => $data
            ]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:根据id和获取帖子详情
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getArticle($id)
    {
        return ArticleModel::build()->field(['create_time'], true)->where(['article_id' => $id])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:添加或修改帖子内容
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditArticle($data, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];


        $validate = new ArticleValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } else {
            if ($id != 0) {
                //如果是编辑帖子时,重新统计点赞数和评论数
                $commentCount = CommentModel::whereCount(['article_id' => $id]);
                $likeCount = LikeModel::whereCount(['article_id' => $id]);
                $data['praise'] = $likeCount;
                $data['comment'] = $commentCount;
            } else {
                $data['is_official'] = 1;
                $data['user_id'] = 0;
            }

            if (ArticleModel::saveArticle($data, $id)) {
                $res['method'] = 'success';
                $res['msg'] = $handle . '成功';
                $res['redirect'] = Url::build('/admin/forum_article/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:删除帖子(评论与点赞都要删掉)**注意开启事务**
     * @param $id
     * @return array
     */
    public function delOrRevertArticle($id, $is_deleted)
    {
        $handle = $is_deleted == 1 ? '删除' : '恢复';
        Db::startTrans();
        try {
            ArticleModel::update(['is_deleted' => $is_deleted], ['article_id' => $id]);
//            ArticleModel::destroy($id);
//            CommentModel::build()->where(['article_id'=>$id])->delete();
//            LikeModel::build()->where(['article_id'=>$id])->delete();
            Db::commit();
            $res = ['method' => 'success', 'msg' => $handle . '成功'];
        } catch (\Exception $e) {
            Db::rollback();
            $res = ['method' => 'error', 'msg' => $handle . '失败'];
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:获取所有回复
     * @param $data
     * @return false|\PDOStatement|string|\think\Collection
     *
     */
    public function getReplys($data)
    {
        $where = [];
        $this->handleSearch($where, $data);
        return CommentModel::build()->field(['create_time'], true)->where($where)->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:删除回复
     * @param $id
     * @return array
     */
    public function delReply($id)
    {
        Db::startTrans();

        try {
            //如果该回复是一级回复则把一级回复下所有的回复也删除
            $reply = CommentModel::build()->get($id);
            if ($reply->parent_comment_id == 0) {
                //此处判断说明属于一级回复,则需要删除下面其余的回复
                CommentModel::build()->where(['parent_comment_id' => $id])->delete();
            }
            $article_id = $reply->article_id;
            $reply->delete();

            //删除完回复后要重新进行回复数量统计
            $count = CommentModel::whereCount(['article_id' => $article_id, 'parent_comment_id' => 0]);
            ArticleModel::update(['comment_count' => $count], ['article_id' => $article_id], true);

            Db::commit();
            $res = ['method' => 'success', 'msg' => '删除成功'];
        } catch (\Exception $e) {
            Db::rollback();
            $res = ['method' => 'error', 'msg' => '删除失败'];
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:设置字段状态,如精华/置顶/官方   ajax
     * @param $id
     * @param $field
     * @param $success
     * @return array
     */
    public function changeStatus($id, $field, $success)
    {
        $res = ['code' => 0, 'msg' => '未知错误'];

        if ($field == null || $success == null) {
            $res['msg'] = '参数缺失';
        } elseif ($id == null) {
            $res['msg'] = '不存在该id的帖子';
        } elseif ($model = ArticleModel::get($id)) {
            $status = $model->$field;
            $handle = $status == 1 ? '取消' : $success;
            $model->$field = $status == 1 ? 0 : 1;
            if (!$model->save()) {
                $res['msg'] = $handle . '失败';
            } else {
                $res['code'] = 1;
                $res['msg'] = $handle . '成功';
            }
        } else {
            $res['msg'] = '该帖子不存在';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:复写handleSearch
     * @param array $where
     * @param $data
     * @param string $alias
     *
     */
    protected function handleSearch(&$where = array(), $data, $alias = '', $time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias);
        //如果有username这个key,则换成关联表的用户
        if (isset($where[$alias . 'username'])) {
            $where['u.username'] = $where[$alias . 'username'];
            unset($where[$alias . 'username']);
        }
    }
}