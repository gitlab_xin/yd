<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/20
 * Time: 18:11
 */

namespace app\admin\logic;

use app\admin\model\AdminMenu as MenuModel;
use app\admin\model\AdminMenu;
use think\Url;

class MenuLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:获取所有菜单并格式化
     * @return array
     *
     */
    public function getAllMenu()
    {
        if ($res = MenuModel::build()->order(['list_order' => 'ASC'])->select()) {
            $res = nodeTree(objToArray($res));
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:修改菜单排序
     * @param $data
     */
    public function setListOrder($data)
    {
        foreach ($data['list_order'] as $k => $v) {
            $temp = array();
            $temp['list_order'] = $v;
            MenuModel::update($temp, ['id' => $k], true);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:获取菜单的下拉菜单,格式化
     * @return array
     */
    public function selectMenu()
    {
        $tmpArr = nodeTree(objToArray(MenuModel::build()->field(['id', 'name', 'parent_id'])->order(['list_order' => 'ASC'])->select()));
        $data = array();
        foreach ($tmpArr as $k => $v) {
            $name = $v['level'] == 0 ? '<b>' . $v['name'] . '</b>' : '├─' . $v['name'];
            $name = str_repeat("│        ", $v['level']) . $name;
            $data[$v['id']] = $name;
        }
        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:获取单个菜单信息
     * @param $id
     * @return null|static
     */
    public function getMenu($id)
    {
        return MenuModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:删除菜单
     * @param $id
     * @return array
     */
    public function delMenu($id)
    {
        $res = ['method' => 'error', 'msg' => '删除失败'];
        if (MenuModel::build()->where(['parent_id' => $id])->count() > 0) {
            $res['msg'] = '请先删除子菜单再删除菜单';
        } else {
            if (MenuModel::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:保存或更新菜单
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditMenu($data, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $data['level'] = $data['parent_id'] == 0 ? 1 : AdminMenu::build()->where(['id' => $data['parent_id']])->value('level') + 1;
        if (MenuModel::saveMenu($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
            $res['redirect'] = Url::build('/admin/menu/index');
        }
        return $res;
    }
}