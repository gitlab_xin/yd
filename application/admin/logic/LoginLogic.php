<?php

namespace app\admin\logic;

use app\admin\model\Admin;
use app\admin\validate\Admin as AdminValidate;
use think\Session;
use think\Cookie;
use think\Url;

class LoginLogic
{

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:验证登录状态
     * @return array|null
     */
    public function loginAuth()
    {
        $result = null;
        if (Session::get('user_id')) {//如果已经有session
            $result = ['method' => 'success', 'msg' => '您已登入','redirect' => Url::build('/admin/index/index')];
        }
        if ($admin_id = Cookie::get('user_id')) {//如果存在cookie
            $admin_id = encry_code($admin_id, 'DECODE');
            if($info = Admin::build()->field(['id','username'])->where(['id'=>$admin_id])->find()){
                //记录
                Session::set('user_name', $info->username);
                Session::set('user_id', $info->id);
                Admin::editInfo($info['id']);
                $result =  ['method' => 'success','msg' => '登入成功','redirect' => Url::build('/admin/index/index')];
            }
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:清除cookie和session
     */
    public function cleanUserCache()
    {
//        Session::set('user_name', null);
        Session::set('user_id', null);
        Cookie::set('user_name', null);
        Cookie::set('user_id', null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:验证登录信息
     * @param $data
     * @return array
     */
    public function loginPostAuth($data)
    {
        $result = ['method' => 'error','msg' => '登录失败'];
        $validate = new AdminValidate();

        if(!captcha_check($data['captcha'])){
            $result['msg'] = '验证码错误';
        }elseif (!$validate->changeRule('username','require|max:20')->scene('login')->check($data)) {
            //验证器验证规则
            $result['msg'] = $validate->getError();
        }elseif($info = Admin::build()->field(['id','username','password'])->where(['username'=>$data['username']])->find()){
            //验证是否存在用户和密码
            if (md5($data['password']) != $info['password']) {
                $result['msg'] = '密码不正确';
            }else{
                Session::set('user_name', $info['username']);
                Session::set('user_id', $info['id']);
                if (isset($data['islogin'])) {
                    Cookie::set('user_name', encry_code($info['username']));
                    Cookie::set('user_id', encry_code($info['id']));
                }
                //记录登录信息
                Admin::editInfo($info['id']);
                $result['method'] = 'success';
                $result['msg'] = '登入成功';
                $result['redirect'] = Url::build('/admin/index/index');
            }
        }else{
            $result['msg'] = '用户不存在';
        }


        return $result;
    }

}
