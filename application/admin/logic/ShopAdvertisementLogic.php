<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/20
 * Time: 9:37
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\ShopClassify as ClassifyModel;
use app\common\model\ShopAdvertisement as AdModel;
use app\common\validate\ShopAdvertisement as AdValidate;

class ShopAdvertisementLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取一级分类
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getTopClassify()
    {
        return ClassifyModel::selectLevelOne();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取所有广告
     * @return false|static[]
     */
    public function getAds()
    {
        return AdModel::build()->field(['create_time', 'update_time'], true)->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取单个广告
     * @param $id
     * @return null|static
     */
    public function getAd($id)
    {
        return AdModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:添加或编辑广告
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditAd($data, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new AdValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (AdModel::saveAd($data, $id)) {
            //通过验证
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
            $res['redirect'] = Url::build('/admin/shop_advertisement/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:删除广告
     * @param $id
     * @return array
     */
    public function delAd($id)
    {
        $model = AdModel::build()->field(['img_src'])->find(['id' => $id]);
        $src = $model->img_src;
        if ($model && $model->delete()) {
            $this->delCloudFile($src);
            return ['method' => 'success', 'msg' => '删除成功'];
        } else {
            return ['method' => 'error', 'msg' => '删除失败'];
        }
    }
}