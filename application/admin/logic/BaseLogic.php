<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/13
 * Time: 14:27
 */
namespace app\admin\logic;

use think\Request;
use app\common\model\Qiniu;
use Qiniu\Storage\UploadManager;

class BaseLogic
{
    //公用token
    private $_qiniuToken;
    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:
     * @param $inputName
     * @return bool
     *
     */
    public function saveFileToCloud($inputName)
    {
        if(Request::instance()->file($inputName) != null){
            $token = $this->getQiniuToken();
            //上传文件的本地路径
            $filePath = $_FILES[$inputName];
            $uploadMgr = new UploadManager();
            list($ret, $err) = $uploadMgr->putFile($token, get_only($filePath['name']), $filePath['tmp_name']);
            return $err !== null? null: $ret['key'];
        }else{
            return false;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:保存多个图片
     * @param $inputName
     * @return bool|string
     */
    protected function saveFilesToCloud($inputName)
    {
        if(Request::instance()->file($inputName) != null){
            //上传文件的本地路径
            $token = $this->getQiniuToken();
            $uploadMgr = new UploadManager();

            $result = [];
            $len = count($_FILES[$inputName]['name']);
            for($i = 0;$i < $len ;$i++){
                list($ret, $err) = $uploadMgr->putFile($token, get_only($_FILES[$inputName]['name'][$i]), $_FILES[$inputName]['tmp_name'][$i]);
                $result[] = $ret['key'];
            }
            return implode('|',$result);
        }else{
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:删除云图片
     * @param $key
     * @return bool|mixed
     */
    protected function delCloudFile($key)
    {
        $qiniu = new Qiniu();
        return $qiniu->delPic($key);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:获取七牛token
     * @return string
     */
    public function getQiniuToken()
    {
        if($this->_qiniuToken == null){
            $qiniu = new Qiniu();
            return $qiniu->getToken();
        }else{
            return $this->_qiniuToken;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:处理序列化数据
     * @param $data 数据数组
     * @param $dataKey 对应db字段
     * @param $postKey post的key
     * @param $postVal post的value
     * @param $dbKey 存入db时的key
     * @param $dbVal 存入db时的value
     */
    protected function handlerData(&$data,$dataKey,$postKey,$postVal,$dbKey,$dbVal)
    {
        if(isset($data[$postKey]) && count($data[$postKey]>0) ){
            $tem = [];
            $count = count($data[$postKey]);
            for($i = 0;$i<$count;$i++){
                $tem[$i][$dbKey] = $data[$postKey][$i];
                $tem[$i][$dbVal] = $data[$postVal][$i];
            }
            $data[$dataKey] = serialize($tem);
            unset($data[$postKey],$data[$postVal]);
        }else{
            $data[$dataKey] = '';
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:临时总结的一个搜索方法,不用看了,早晚会改的
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    protected function handleSearch(&$where = array(),$data,$alias = '',$time = 'create_time')
    {
        foreach ($data as $key => $value){
            if(strpos($key,'_id')!== false){//如果带有id的字段,则直接进行 等于= 的条件
                if($value != 0){
                    $where[$alias.$key] = $value;
                }
            }elseif(strpos($key,'is_')!== false){//如果带有是否的字段
                if($value != 99){
                    $where[$alias.$key] = $value;
                }
            }elseif(strpos($key,'_time')!== false){//如果是时间区间的
                if(!isset($where[$alias.$time][1])){
                    $where[$alias.$time][0] = 'between';
                    $where[$alias.$time][1] = [];
                }
                if($key == 'begin_time'){//begin_time插入队列头
                    $value = $value == ''?0:strtotime($value);
                    array_unshift($where[$alias.$time][1],$value);
                }else{//end_time插入队列尾
                    $value = $value == null?0:strtotime($value);
                    array_push($where[$alias.$time][1],$value);
                }
            }elseif(strpos($key,'_price')!== false){
                if(!isset($where[$alias.'price'][1])){
                    $where[$alias.'price'][0] = 'between';
                    $where[$alias.'price'][1] = [];
                }
                if($key == 'left_price'){
                    $value = $value == ''?0:$value;
                    array_unshift($where[$alias.'price'][1],$value);
                }else{
                    $value = $value == null?0:$value;
                    array_push($where[$alias.'price'][1],$value);
                }

            }elseif($key == 'field' || $key == 'page'){

                continue;

            }else{//其它则进行like文本搜索
                if($value !== 'ALL' && $value != '') {
                    $where[$alias.$key] = ['like', "%{$value}%"];
                }
            }
        }
        if(isset($where[$alias.$time][1]) && $where[$alias.$time][1][0] == $where[$alias.$time][1][1]){
            unset($where[$alias.$time]);
        }
        if(isset($where[$alias.'price'][1]) && $where[$alias.'price'][1][0] == $where[$alias.'price'][1][1]){
            unset($where[$alias.'price']);
        }
    }
}