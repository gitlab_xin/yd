<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 11:46
 */

namespace app\admin\logic;

use app\common\model\User;
use app\common\model\UserMoneyRecord;
use app\common\model\Withdraw as WithdrawModel;
use app\common\model\Withdraw;
use app\common\validate\Withdraw as WithdrawValidate;
use think\Db;
use think\Session;

class WithdrawLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取列表
     * @param $data
     * @return \think\Paginator
     *
     */
    public function getWithdrawList($data)
    {
        $where = [];
        $this->handleSearch($where,$data,'w.');

        return WithdrawModel::build()->alias('w')
            ->field(['u.username','w.id','w.money','w.status','w.remark','w.account_name','w.bank','w.card_num','w.create_time'])
            ->where($where)
            ->join('user u','w.user_id = u.user_id','left')
            ->paginate(20,false, [ 'query' => $data,]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:通过或拒绝审核
     * @param $data
     * @param $id
     * @param $status
     * @return array
     */
    public function changeStatus($data,$id,$status)
    {
        $res = ['method'=>'error','msg'=>'操作失败'];
        $data['status'] = $status;
        $validate = new WithdrawValidate();

        if(!$validate->scene('changeStatus')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            Db::startTrans();
            try{
                $data['admin_id'] = Session::get('user_id');
                if(! Withdraw::update($data,$id,true)){
                    throw new \Exception('审核失败');
                }
                /*
                 * 如果是拒绝,则需要添加对应的金额回用户账户里
                 * 并且添加用户流水
                 */
                if($status == 2){
                    $model = Withdraw::get($id);
                    $user = User::get($model->user_id);
                    $before_money = $user->money;
                    $money = $model->money;
                    $after_money = ($user->money *100 + $model->money*100)/100;
                    $user->money = $after_money;
                    if(!$user->save()){
                        throw new \Exception('账户金额回滚失败');
                    }
                    if(!UserMoneyRecord::create(['user_id'=>$model->user_id,'before_change'=>$before_money,'after_change'=>$after_money,
                        'change_var'=>$money,'change_type'=>1,'type'=>'withdraw_increase'])){
                        throw new \Exception('账户流水记录失败');
                    }
                }
                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '操作成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }

        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取单个提现记录
     * @param $id
     * @return null|static
     *
     */
    public function getDetail($id)
    {
        return Withdraw::get($id);
    }
}