<?php


namespace app\admin\logic;

use app\admin\controller\MyDecorateProduct;
use app\common\model\CustomizedCabinet as CabinetModel;
use app\common\model\CustomizedColor;
use app\common\model\CustomizedColor as ColorModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\MyDecorateProductStandard;
use app\common\model\MyDecorateRoom;
use app\common\model\ShopProduct;
use think\Config;
use think\Db;

class MyDecorateProductLogic extends BaseLogic
{
    public static function index($data){
        $where = ['is_delete' => 0, 'decorate_id' => $data['program_id'], 'type' => ['<>', '1']];

        if (!empty($data['room_id'])){
            $where['room_id'] = $data['room_id'];
        }

        return \app\common\model\MyDecorateProduct::build()
            ->field([
                'id','name','room_id room_name','color_id','standard_id','type','original_price',
                'discount_price','color_id','product_id product_standard_info','product_id'
            ])
            ->where($where)
            ->order(['create_time' => 'DESC'])
            ->paginate(20,false,['query'=>$data]);
    }

    public static function roomIndex($data){

        return MyDecorateRoom::build()
            ->where(['decorate_id' => $data['program_id'],'is_delete' => '0'])
            ->select();
    }

    public static function roomAdd($data){
        $count = MyDecorateRoom::build()
            ->where(['name' => $data['name'],'decorate_id' => $data['id'],'is_delete' => '0'])
            ->count();
        if ($count > 0){
            return true;
        }
        return MyDecorateRoom::build()
            ->insert([
                'decorate_id' => $data['id'],
                'name' => $data['name'],
                'create_time' => time()
            ]);
    }

    public static function roomEdit($data){

        return MyDecorateRoom::build()
            ->where(['id' => $data['id']])
            ->Update([
                'name' => $data['name'],
                'update_time' => time()
            ]);
    }

    public static function roomDel($data){

        return MyDecorateRoom::build()
            ->where(['id' => $data['id']])
            ->Update([
                'is_delete' => '1',
                'update_time' => time()
            ]);
    }
    public static function del($data){

        return \app\common\model\MyDecorateProduct::build()
            ->where(['id' => $data['id']])
            ->Update([
                'is_delete' => '1',
                'update_time' => time()
            ]);
    }

    public static function colorIndex($info = ''){
        if (empty($info['product_info']['scheme_b_type'])){
            $type =  $info['product_info']['scheme_type'];
        }else{
            $type = $info['product_info']['scheme_type'].'-'.$info['product_info']['scheme_b_type'];
        }

        //只有移门的柜体
        if (in_array($type, ['RQS','YM'])){
            $list = CustomizedColor::build()
                ->field([
                    'color_id', 'color_name', 'origin_img_src'
                ])
                ->where(['is_deleted' => 0, 'is_door' => 1, 'is_show' => 1])
                ->order('is_default ASC,color_no ASC')
                ->select()->toArray();
        } else {
            $list = CabinetModel::build()
                ->alias('ca')
                ->join('customized_cabinet_color cc', 'cc.cabinet_id = ca.cabinet_id', 'LEFT')
                ->join('customized_color co', 'cc.color_id = co.color_id', 'LEFT')
                ->where(['ca.type' => $type, 'co.is_deleted' => 0, 'co.is_show' => 1])
                ->field(['co.color_id', 'co.color_name', 'co.origin_img_src'])
                ->order('co.is_default ASC,co.color_no ASC')
                ->select()
                ->toArray();
        }

        return $list;

    }

    public static function colorIndex2($data){

        //只有移门的柜体
        if (in_array($data['type'], ['RQS','YM'])){
            $list = CustomizedColor::build()
                ->field([
                    'color_id', 'color_name', 'origin_img_src'
                ])
                ->where(['is_deleted' => 0, 'is_door' => 1, 'is_show' => 1])
                ->order('is_default ASC,color_no ASC')
                ->select()->toArray();
        } else {
            $list = CabinetModel::build()
                ->alias('ca')
                ->join('customized_cabinet_color cc', 'cc.cabinet_id = ca.cabinet_id', 'LEFT')
                ->join('customized_color co', 'cc.color_id = co.color_id', 'LEFT')
                ->where(['ca.type' => $data['type'], 'co.is_deleted' => 0,'co.is_show' => 1])
                ->field(['co.color_id', 'co.color_name', 'co.origin_img_src'])
                ->order('co.is_default ASC,co.color_no ASC')
                ->select()
                ->toArray();
        }


        $html = '';
        $img_head = Config::get('qiniu.BucketDomain');
        foreach ($list as $k => $v){
            $img = $img_head.$v['origin_img_src'].'?imageView2/0/w/50/h/50';
            $color_img = ($v['color_id'] == $data['color_id']) ? $data['img'] : '/static/img/add_img.png';
            $color_img2 = ($v['color_id'] == $data['color_id']) ? $data['scheme_pic'] : '';
            $html .= '<div class="col-sm-4 fileinput-button ">
                            <div style="border: 0" >
                                <div class="cover">
                                    <span class="thumbnail no-padding-left col-sm-3" id="thumbnail_poster_src'.$v['color_id'].'">
                                        <img src="'.$color_img.'" width="50" height="50"/>
                                    </span>
                                    <input class="fileupload col-sm-3" style="width: 100px;height: 50px;left: 0px" type="file" name="file" data-type="thumbnail_poster_src'.$v['color_id'].'" data-input="house_type_src'.$v['color_id'].'" data-delete="delete_src'.$v['color_id'].'" />

                                    <div class="col-sm-8" style="margin-top:10px">
                                        <span>'.$v['color_name'].'</span>
                                        <span class="help-button" data-rel="popover" data-trigger="hover" data-placement="left" data-html="true" data-content="<img src='.$img.'>" title="" data-original-title="'.$v['color_name'].'">?</span>
                                        <span class="delete_src'.$v['color_id'].'">
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" class="customize" name="standard['.$k.'][img_src]" id="house_type_src'.$v['color_id'].'" value="'.$color_img2.'"/>
                            <input type="hidden" class="customize" name="standard['.$k.'][color_name]"  value="'.$v['color_name'].'" />
                            <input type="hidden" class="customize" name="standard['.$k.'][color]"  value="'.$v['color_id'].'" />
                            <input type="hidden" class="customize" name="standard['.$k.'][is_edit]"  value="2" />
                            <input type="hidden" class="customize" name="standard['.$k.'][id]"  value="" />
                        </div>';
        }

        return $html;
    }

    public static function info($id){
        $info = \app\common\model\MyDecorateProduct::build()
            ->where(['id' => $id])
            ->field([
                'id','product_id','room_id','decorate_id','type','name','code','original_price','discount_price','intro','image',
                'product_id product_info','color_id','id standard_info','supplier_id', 'supplier_id supplier_name','standard_id'
            ])
            ->find();
        return $info;
    }

    public static function productindex($data){
        $product_html = $product_html_head = '';
        $where = ['p.is_sale' => '1','p.is_deleted' => '0','p_s.is_deleted' => '0','p_s.status' => '1'];
        $where['p.name|p.product_id'] = ['like',"%{$data['keyword']}%"];

        if (empty($data['keyword'])){
            return '';
        }

        $list = ShopProduct::build()
            ->alias('p')
            ->join('yd_shop_supplier s', 's.supplier_id = p.supplier_id')
            ->join('yd_shop_product_standard p_s','p_s.product_id = p.product_id')
            ->field([
                'p.product_id','p.name','p_s.img product_img', 'p_s.img', 's.supplier_id','s.name supplier_name','p_s.price',
                'p_s.standard_id'
            ])
            ->where($where)
            ->order('p.create_time DESC')
            ->select();
        if (count($list) <= 0){
            return '';
        }


        foreach (collection($list)->toArray() as $k => $v){
            $k = $k+1;
            $url = url("/admin/shop_product/detail",'id='.$v['product_id'].'');
            if ($v['product_id'] == $data['product_id']){
                $product_html_head = '    <tr>
                                        <td class="center col-sm-1 width-5">
                                            <label class="pos-rel">
                                                <input type="checkbox" class="ace"  checked/>
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td class="center col-sm-1 width-5">
                                           '.$v['product_id'].'
                                        </td>
                                        <td>
                                            <a title="查看" style="color: #393939"  target="_blank" href='.$url.'>
                                            '.$v['name'].'
                                            </a>
                                        </td>
                                        <td class="center col-sm-1 width-5"><img src='.$v['product_img'].'?imageView2/0/w/50/h/50'.'></td>
                                        <td>/</td>
                                        <td>/</td>
                                        <td class="center">
                                            <a class="blue" onclick="select_product(this)" data-standard-id='.$v['standard_id'].' data-img='.$v['img'].' data-price='.$v['price'].' data-supplier-id='.$v['supplier_id'].' data-supplier-name='.$v['supplier_name'].'  data-id='.$v['product_id'].'   data-product-img-300='.$v['product_img'].'?imageView2/0/w/350/h/200'.' data-product-img='.$v['product_img'].'?imageView2/0/w/50/h/50'.'  data-standard-name="/" data-color-name="/" data-name='.$v['name'].' data-dismiss="modal" data-product-type="no" href="#">
                                                <i class="ace-icon fa fa-plus bigger-130"></i>
                                            </a>
                                        </td>
                                    </tr>';
                continue;
            }
            $product_html  .= '    <tr>
                                        <td class="center col-sm-1 width-5">
                                            <label class="pos-rel">
                                                <input type="checkbox" class="ace" />
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td class="center col-sm-1 width-5">
                                           '.$v['product_id'].'
                                        </td>
                                        <td>
                                             <a title="查看" style="color: #393939"  target="_blank" href='.$url.'>
                                            '.$v['name'].'
                                            </a>
                                        </td>
                                        <td class="center col-sm-1 width-5"><img src='.$v['product_img'].'?imageView2/0/w/50/h/50'.'></td>
                                        <td>/</td>
                                        <td>/</td>
                                        <td class="center">
                                            <a class="blue" onclick="select_product(this)" data-standard-id='.$v['standard_id'].' data-img='.$v['img'].' data-price='.$v['price'].' data-supplier-id='.$v['supplier_id'].' data-supplier-name='.$v['supplier_name'].' data-id='.$v['product_id'].' data-product-img-300='.$v['product_img'].'?imageView2/0/w/350/h/200'.'  data-product-img='.$v['product_img'].'?imageView2/0/w/50/h/50'.'  data-standard-name="/" data-color-name="/" data-name='.$v['name'].' data-product-type="no" data-dismiss="modal" href="#">
                                                <i class="ace-icon fa fa-plus bigger-130"></i>
                                            </a>
                                        </td>
                                    </tr>';
        }


        return $product_html_head.$product_html;
    }

    public static function product2index($data){
        $product_html = $product_html_head = '';
        //todo 暂时定46为官方账号
        $where = ['p.user_id' => '46','p.parent_id' => '0','c.is_deleted' => '0'];
        $where['p.scheme_name|p.id'] = ['like',"%{$data['keyword']}%"];

        if (empty($data['keyword'])){
            return '';
        }

        //todo 暂时写死8为整配类的商家
        $list = SchemeModel::build()
            ->alias('p')
            ->join('yd_customized_color c','c.color_no = p.scheme_color_no')
            ->join('yd_shop_supplier s', 's.supplier_id = 8')
            ->field([
                'p.id product_id','p.scheme_type', 'p.scheme_b_type' ,'p.scheme_name name',
                'p.scheme_pic product_img',"CONCAT_WS('*',p.scheme_width,p.scheme_height,p.scheme_deep) as 'standard_name'",
                'c.color_name','c.color_id','p.scheme_pic', 's.supplier_id','s.name supplier_name'
            ])
            ->order('p.create_time DESC')
            ->where($where)
            ->select();
        if (count($list) <= 0){
            return '';
        }

        foreach (collection($list)->toArray() as $k => $v){
            $product_type = (empty($v['scheme_b_type'])) ? $v['scheme_type'] : $v['scheme_type'].'-'.$v['scheme_b_type'];
            //todo 方案绝对路径，不同环境需要修改域名
            switch (trim($v['scheme_type'].$v['scheme_b_type'])){
                case 'RQS':
                    $type = 1;
                    break;
                case 'QW':
                    $type = 2;
                    break;
                case 'BZ':
                    $type = 3;
                    break;
                case 'YM':
                    $type = 4;
                    break;
                case 'ZHCWSN':
                    $type = 5;
                    break;
                case 'ZHSJSC':
                    $type = 6;
                    break;
                case 'ZHYYST':
                    $type = 7;
                    break;
                case 'ZHBGDG':
                    $type = 'hangingCabinet';
                    break;
                case 'YMJ':
                    $type = 'cloakroom';
                    break;
            }
            $url = 'https://pctest.inffur.com'.'/custom-cabinet-info?isAdminPage=true&'.'schemeId='.$v['product_id'].'&cabinetType='.$type.'&installPic='.$v['scheme_pic'].'';
            if ($v['product_id'] == $data['product_id']){
                $product_html_head = '    <tr>
                                        <td class="center col-sm-1 width-5">
                                            <label class="pos-rel">
                                                <input type="checkbox" class="ace"  checked/>
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td class="center col-sm-1 width-5">
                                           '.$v['product_id'].'
                                        </td>
                                       <td>
                                             <a title="查看" style="color: #393939"  target="_blank" href='.$url.'>
                                            '.$v['name'].'
                                            </a>
                                        </td>
                                        <td class="center col-sm-1 width-5"><img src='.$v['product_img'].'></td>
                                        <td>'.$v['color_name'].'</td>
                                        <td>'.$v['standard_name'].'</td>
                                        <td class="center">
                                            <a class="blue" onclick="select_product(this)" data-supplier-id='.$v['supplier_id'].' data-supplier-name='.$v['supplier_name'].' data-id='.$v['product_id'].'  data-product-img='.$v['product_img'].' data-scheme-pic='.$v['scheme_pic'].'  data-standard-name='.$v['standard_name'].' data-color-name='.$v['color_name'].' data-color-id='.$v['color_id'].' data-name='.$v['name'].' data-product-type='.$product_type.' data-dismiss="modal" href="#">
                                                <i class="ace-icon fa fa-plus bigger-130"></i>
                                            </a>
                                        </td>
                                    </tr>';
                continue;
            }
            $product_html  .= '    <tr>
                                        <td class="center col-sm-1 width-5">
                                            <label class="pos-rel">
                                                <input type="checkbox" class="ace" />
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td class="center col-sm-1 width-5">
                                           '.$v['product_id'].'
                                        </td>
                                        <td>
                                             <a title="查看" style="color: #393939"  target="_blank" href='.$url.'>
                                            '.$v['name'].'
                                            </a>
                                        </td>
                                        <td class="center col-sm-1 width-5"><img src='.$v['product_img'].'></td>
                                        <td>'.$v['color_name'].'</td>
                                        <td>'.$v['standard_name'].'</td>
                                        <td class="center">
                                            <a class="blue" onclick="select_product(this)" data-supplier-id='.$v['supplier_id'].' data-supplier-name='.$v['supplier_name'].' data-id='.$v['product_id'].'  data-product-img='.$v['product_img'].' data-scheme-pic='.$v['scheme_pic'].'  data-standard-name='.$v['standard_name'].' data-color-name='.$v['color_name'].' data-color-id='.$v['color_id'].' data-name='.$v['name'].' data-product-type='.$product_type.' data-dismiss="modal" href="#">
                                                <i class="ace-icon fa fa-plus bigger-130"></i>
                                            </a>
                                        </td>
                                    </tr>';
        }


        return $product_html_head.$product_html;
    }

    public static function add($data){
        $res = ['method' => 'error', 'msg' => '添加失败'];

        Db::startTrans();
        $insert = [
            'product_id' => $data['product_id'],
            'room_id' => $data['room_id'],
            'decorate_id' => $data['program_id'],
            'type' => $data['type'],
            'name' => $data['name'],
            'code' => $data['code'],
            'original_price' => $data['original_price'],
            'discount_price' => $data['discount_price'],
            'image' => $data['image'],
            'intro' => $data['intro'],
            'create_time' => time()
        ];
        if ($data['type'] == '2') {
            $insert['color_id'] = $data['color_id'];
        } elseif ($data['type'] == '3'){
            $insert['standard_id'] = $data['standard_id'];
        }
        $bool = \app\common\model\MyDecorateProduct::build()->insert($insert);
        $id =  \app\common\model\MyDecorateProduct::build()->getLastInsID();

        //整配
        if ($data['type'] == '2'){
            $insertColor = [];
            foreach ($data['standard'] as $k => $v){
                if (empty($v['img_src'])){
                    continue;
                }

                $insertColor[] = [
                    'decorate_product_id' => $id,
                    'color_id' => $v['color'],
                    'product_img' => $v['img_src'],
                    'create_time' => time(),
                ];
            }
            if (count($insertColor) > 0){
                $bool = MyDecorateProductStandard::build()
                    ->insertAll($insertColor);
            }
        }


        Db::commit();

        if ($bool){
            $res['method'] = 'success';
            $res['msg'] = '添加成功';
        }

        return $res;
    }

    public static function edit($data){
        $res = ['method' => 'error', 'msg' => '编辑失败'];
        Db::startTrans();
        $update = [
            'product_id' => $data['product_id'],
            'supplier_id' => $data['supplier_id'],
            'room_id' => $data['room_id'],
            'decorate_id' => $data['program_id'],
            'type' => $data['type'],
            'name' => $data['name'],
            'code' => $data['code'],
            'original_price' => $data['original_price'],
            'discount_price' => $data['discount_price'],
            'image' => $data['image'],
            'intro' => $data['intro'],
            'update_time' => time()
        ];
        if ($data['type'] == '2') {
            $update['color_id'] = $data['color_id'];
        } elseif ($data['type'] == '3'){
            $update['standard_id'] = $data['standard_id'];
        }


        $bool = \app\common\model\MyDecorateProduct::build()
            ->where(['id' => $data['id']])
            ->update($update);

        //整配
        if ($data['type'] == '2'){
            $insertColor = [];
            foreach ($data['standard'] as $k => $v){

                if (empty($v['img_src']) && $v['is_edit'] == '2'){
                    continue;
                }
                if (empty($v['img_src']) && $v['is_edit'] == '1'){
                    MyDecorateProductStandard::build()
                        ->where(['id' => $v['id']])
                        ->delete();
                }

                //新增花色
                if ($v['is_edit'] == '2'){
                    $insertColor[] = [
                        'decorate_product_id' => $data['id'],
                        'color_id' => $v['color'],
                        'product_img' => $v['img_src'],
                        'create_time' => time(),
                    ];
                } else {
                    MyDecorateProductStandard::build()
                        ->where(['id' => $v['id']])
                        ->update([
                            'color_id' => $v['color'],
                            'product_img' => $v['img_src'],
                            'update_time' => time(),
                        ]);
                }
            }

            if (count($insertColor) > 0){
                $bool = MyDecorateProductStandard::build()
                    ->insertAll($insertColor);
            }
        }


        Db::commit();
        if ($bool){
            $res['method'] = 'success';
            $res['msg'] = '编辑成功';
        }

        return $res;
    }



}
