<?php

namespace app\admin\logic;

use app\common\model\UserBlackList;
use think\Url;
use think\Request;
use app\common\model\User as UserModel;
use app\common\validate\User as UserValidate;
/**
 * Class UserLogic
 * @package app\admin\logic
 */
class UserLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:获取多个用户
     * @param $data
     * @return \think\Paginator
     *
     */
    public function getUsers($data){

        $where = ['status' => ['gt',0],'user_id' => ['gt',0]];
        if(count($data)>0){
            $this->handleSearch($where,$data,'','reg_time');
        }
        $fields = [
            'user_id',
            'username',
            'mobile',
            'email',
            'status',
            'level',
            'last_login_time',
            'reg_time'
        ];

        return UserModel::build()->field($fields)
            ->where($where)->order('status asc')->paginate(20,false, [
                'query' => $data,
            ]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取单个用户信息
     * @param $id
     * @param array $field
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getUserInfo($id,$field = []){

        return UserModel::build()->field($field)->find($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:添加用户
     * @param $data
     * @return array
     */
    public function addUser($data){
        $result = ['method' => 'error',
            'msg' => '操作失败'];
        $validate = new UserValidate;
        //验证器验证规则
        if (!$validate->changeRule('add_mobile',$data['add_mobile'],true)
            ->changeRule('email',$data['email'],true)->scene('add')->check($data)) {
            $result['msg'] = $validate->getError();
        }else{
            //若验证通过
            if(UserModel::saveUser($data)){
                $result['msg'] = '添加成功';
                $result['method'] = 'success';
            }else{
                $result['msg'] = '添加失败';
            }

        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:删除用户
     * @param $id
     * @return array
     */
    public function delUser($id){
        $res = ['method'=>'error','msg'=>'删除失败'];
        if(UserModel::where('user_id',$id)->update(['status'=>0])){
            $res['method'] = 'success';
            $res['msg'] = '删除成功';
        }
        return $res;
    }

    /***
     * @author: Rudy
     * @time: 2017年9月1日
     * description:
     * @param $data
     * @return array
     */
    public function editUser($data){
        $result = ['method' => 'error',
            'msg' => '操作失败'];
        $validate = new UserValidate;
        //验证器验证规则
        if (!$validate->changeRule('add_mobile',$data['add_mobile'].",{$data['id']},user_id",true)
            ->changeRule('email',$data['email'].",{$data['id']},user_id",true)
            ->scene('edit')->check($data)) {
            $result['msg'] = $validate->getError();
        }else{
            //若验证通过
            if(UserModel::saveUser($data, $data['id'])){
                $result['msg'] = '修改成功';
                $result['method'] = 'success';
            }else{
                $result['msg'] = '修改失败';
            }

        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:封号以及解封
     * @param $data
     * @param int $id
     * @return array
     */
    public function banUser($data,$id = 0)
    {

        $res = ['method'=>'error','msg'=>'操作失败'];

        if($data['ban'] == 1){
            //要封号
            $validate = new UserValidate();
            if(!$validate
                ->changeRule('ban_end_time','|after:'.$data['ban_begin_time'],true)
                ->scene('ban_time')->check($data)){
                $res['msg'] = $validate->getError();
            }else{
                $data['ban_begin_time'] = strtotime($data['ban_begin_time']);
                $data['ban_end_time'] = strtotime($data['ban_end_time']);
                if(UserBlackList::saveBlackList($data,$id)){
                    $res['method'] = 'success';
                    $res['msg'] = '封号成功';
                    $res['redirect'] = Url::build('/admin/user/index');
                }
            }
        }else{
            //取消封号,即删除记录
            $blackList = UserBlackList::get(['user_id'=>$data['user_id']]);
            if($blackList){
                if($blackList->delete()){
                    $res['method'] = 'success';
                    $res['msg'] = '解封成功';
                }else{
                    $res['msg'] = '解封失败';
                }
            }else{
                $res['msg'] = '该用户不在黑名单内';
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:根据user_id获取黑名单用户
     * @param $user_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getBlackListByUserId($user_id)
    {
        return UserBlackList::build()->where(['user_id'=>$user_id])->find();
    }

}
