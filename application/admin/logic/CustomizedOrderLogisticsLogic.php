<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 11:23
 */

namespace app\admin\logic;

use think\Url;
use app\common\tools\kdniaoUtil;
use app\common\model\ShopOrderLogistics as LogisticsModel;
use app\common\model\ShopOrderOfficialLogistics as OfficialLogisticsModel;
use app\common\validate\CustomizedOrderLogistics as LogisticsValidate;
use app\common\validate\OfficialLogistics as OfficialLogisticsValidate;

class CustomizedOrderLogisticsLogic
{

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:获取物流信息(官方)
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getOfficialLogistics($id)
    {
        return OfficialLogisticsModel::build()->where(['logistics_id'=>$id])->order(['create_time'=>'DESC'])->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:获取物流(官方和非官方)
     * @param $id
     * @param $type
     * @param string $num
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getLogisticsMessages($id,$type,$num = '')
    {
        if($type == 'official'){
            return $this->getOfficialLogistics($id);
        }else{
            $util = new kdniaoUtil;
            $return = [];
            $lists = $util->getOrderTracesByJson($type,$num);
            if($lists !== false){
                foreach ($lists['Traces'] as &$v){
                    $v['content'] = $v['AcceptStation'];
                    $v['create_time'] = strtotime($v['AcceptTime']);
                    $v['user_id'] = $v['id'] = 0;
                    $v['logistics_id'] = $id;
                    unset($v['AcceptStation'],$v['AcceptTime']);
                }
                $return = $lists['Traces'];
                krsort($return);
                unset($lists);
            }
            return $return;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:获取物流详情
     * @param $id
     * @return null|static
     */
    public function getLogistics($id)
    {
        return LogisticsModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:添加物流信息
     * @param $data
     * @return array
     */
    public function addLogistics($data)
    {
        $res = ['method'=>'error','msg'=>'添加失败'];

        $validate = new OfficialLogisticsValidate();
        if(!$validate->scene('add')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            $logistics = LogisticsModel::get($data['logistics_id']);
            $data['user_id'] = $logistics->user_id;
            if(OfficialLogisticsModel::create($data,true)){
                $res['method'] = 'success';
                $res['msg'] = '添加成功';
                $res['redirect'] = Url::build('/admin/customized_order_logistics/index',['id'=>$data['logistics_id']]);
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:删除物流信息
     * @param $id
     * @return array
     */
    public function delLogistics($id)
    {
        return OfficialLogisticsModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:更新物流状态
     * @param $data
     * @param $id
     * @return array
     */
    public function updateLogistics($data,$id)
    {

        $res = ['method'=>'error','msg'=>'更新失败'];

        $validate = new LogisticsValidate();
        if(!$validate->scene('update')->check($data)){
            $res['msg'] = $validate->getError();
        }elseif(LogisticsModel::update($data,['id'=>$id],true)){
            $res['method'] = 'success';
            $res['msg'] = '更新成功';
        }

        return $res;
    }
}