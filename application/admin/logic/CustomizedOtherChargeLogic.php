<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/15
 * Time: 16:49
 */

namespace app\admin\logic;

use app\common\model\CustomizedOtherCharge as ChargeModel;
use app\common\validate\CustomizedOtherCharge as ChargeValidate;


class CustomizedOtherChargeLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取全部
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getAll()
    {
        return ChargeModel::getList();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return ChargeModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:编辑
     * @param $data
     * @param $id
     * @return array
     */
    public function edit($data,$id)
    {
        $res = ['method'=>'error','msg'=> '编辑失败'];

        $validate = new ChargeValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }elseif(ChargeModel::update($data,['id'=>$id])){
            $res['method'] = 'success';
            $res['msg'] = '编辑成功';
        }
        return $res;
    }
}