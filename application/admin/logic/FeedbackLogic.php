<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/21
 * Time: 17:52
 */

namespace app\admin\logic;

use app\common\model\Feedback as FeedbackModel;

class FeedbackLogic extends BaseLogic
{
    public function getAll($data)
    {
        $where = [];
        $this->handleSearch($where,$data,'f.');
        return FeedbackModel::build()->field(['f.id','f.create_time','case f.is_read when 0 then "未读" when 1 then "已读" end read_status','f.is_read','u.username'])
            ->alias('f')->join('user u','f.user_id = u.user_id','left')->where($where)->order(['f.is_read'=>'ASC','f.create_time'=>'DESC'])
            ->paginate(20,false,['query'=>$data]);
    }

    public function getDetail($id)
    {
        return FeedbackModel::get($id);
    }

    public function del($id)
    {
        return FeedbackModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }

    public function setNotYetReadStatus($id)
    {
        return $this->changeStatus($id,0)?['method'=>'success','msg'=>'设置成功']:['method'=>'error','msg'=>'设置失败'];
    }

    public function changeStatus($id,$status)
    {
        return FeedbackModel::update(['is_read'=>$status],['id'=>$id],true);
    }
}