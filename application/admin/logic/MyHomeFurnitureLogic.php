<?php

namespace app\admin\logic;

use app\common\model\MyHomeFurniture as FurnitureModel;
use app\common\validate\MyHomeFurniture as FurnitureValidate;
use think\Db;

/**
 * Class MyHomeFurnitureLogic
 * @package app\admin\logic
 */
class MyHomeFurnitureLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取家具列表
     * @return \think\Paginator
     */
    public function getFurnitureList()
    {
        return Db::name('my_home_furniture')->alias('f')
            ->join('my_home_furniture_classify fc','fc.classify_id = f.classify_id')
            ->where(['f.is_deleted' => 0])
            ->field(["f.*","`fc`.`name` as `classify_name`"])
            ->order(['f.create_time' => 'DESC'])->paginate(20);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @param $id
     * @return array
     */
    public function delFurniture($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该家具'];

        $model = FurnitureModel::get($id);
        if ($model != null) {

            if (FurnitureModel::build()->update(['is_deleted' => 1], ['furniture_id' => $id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
