<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 15:46
 */

namespace app\admin\logic;

use app\common\model\UserInviteCode as CodeModel;
use app\common\validate\UserInviteCode as CodeValidate;
use think\Url;

class UserInviteCodeLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param $data
     * @param array $orderby
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getCodeGroup($data, $orderby = ['id'=>"DESC"])
    {
        $where = [];
        $this->handleSearch($where, $data);

        return CodeModel::build()
            ->field('update_time', true)
            ->where($where)
            ->order($orderby)
            ->paginate(20, false, ['query' => $data]);
    }

    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param $id
     * @return null|static
     * @throws \think\exception\DbException|null
     */
    public function getInfo($id)
    {
        $info = CodeModel::get($id);
        return $info;
    }

    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param $postData
     * @param int $id
     * @return array
     */
    public function addOrEdit($postData, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';

        $res = ['method' => 'error', 'msg' => $handler . '失败'];
        //1.验证post的数据
        $validate = new CodeValidate();


        if (!$validate->scene('addOrEdit')->check($postData)) {
            $res['msg'] = $validate->getError();
        }  else {
            //4.判断是否成功添加数据库
            if (CodeModel::saveCode($postData, $id)) {
                $res['method'] = 'success';
                $res['msg'] = $handler . '成功';
                $res['redirect'] = Url::build('/admin/user_invite_code/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:删除banner
     * @param $id
     * @return array
     */
    public function delCode($id)
    {
        return CodeModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }
}