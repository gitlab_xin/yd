<?php

namespace app\admin\logic;

use app\admin\model\Admin;
use app\admin\model\AdminGroup;
use app\admin\model\AdminGroupAccess;
use app\admin\validate\Admin as AdminValidate;
use think\Db;
use think\Url;

class AdminLogic
{

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:获取所有管理员信息,包括其所在组
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getAllAdmin()
    {
         $res = Admin::build()->alias('a')
            ->field(['a.*','GROUP_CONCAT(`ag`.name) as group_name'])
            ->join('yd_admin_group_access `aga`','`aga`.admin_id = `a`.id','left')
            ->join('yd_admin_group `ag`','`ag`.id = `aga`.admin_group_id','left')
            ->group('a.id')
            ->order(['a.id'=>'DESC'])
            ->select();
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:获取后台用户信息(单个)
     * @param $id
     * @return null|static
     */
    public function getAdminInfo($id)
    {
        $info = Admin::get($id);
        $info['userGroups'] = AdminGroupAccess::getUserGroups($id);
        return $info;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:获取所有管理组
     * @return array
     */
    public function getGroups()
    {
        return AdminGroup::getGroups();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:添加管理员
     * @param $data
     * @return array
     */
    public function addAdmin($data)
    {
        $result = ['method' => 'error','msg' => '添加失败'];
        $validate = new AdminValidate();
        //验证器验证规则
        if (!$validate->scene('register')->check($data)) {
            $result['msg'] = $validate->getError();
        }else{
            Db::startTrans();
            try{
                //保存管理员
                $data['password'] = md5($data['password']);
                $res = Admin::create($data,true);
                if($res == false){
                    throw new \Exception('插入数据失败');
                }
                //如果需要保存管理员所在组
                if (isset($data['groups']) && count($data['groups']) > 0) {
                    $uid = $res->id;
                    $items = [];
                    foreach ($data['groups'] as $v) {
                        $items[] = ['admin_id' => $uid, 'admin_group_id' => $v];
                    }
                    $model = new AdminGroupAccess();
                    if(!$model->saveAll($items)){
                        throw new \Exception('插入管理组失败');
                    }
                }
                Db::commit();
                $result['method'] = 'success';
                $result['msg'] = '添加成功';
                $result['redirect'] = url('/admin/admin/index');
            }catch (\Exception $e){
                Db::rollback();
                $result['msg'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:修改管理员
     * @param $data
     * @param $id
     * @return array
     */
    public function editAdmin($data,$id)
    {
        $result = ['method' => 'error','msg' => '编辑失败'];
        $validate = new AdminValidate();

        if($data['password'] == ''){
            $validate = $validate->scene('editWithoutPassword');
        }else{
            $validate = $validate->changeRule('username',','.$id,true)->scene('register');
        }
        //验证器验证规则
        if (!$validate->check($data)) {
            $result['msg'] = $validate->getError();
        }else{
            Db::startTrans();
            try{
                //保存管理员
                $res = Admin::update($data,['id'=>$id],true);
                if($res == false){
                    throw new \Exception('更新数据失败');
                }
                //如果需要保存管理员所在组
                if (isset($data['groups']) && count($data['groups']) > 0) {
                    $uid = $res->id;
                    $items = [];
                    foreach ($data['groups'] as $v) {
                        $items[] = ['admin_id' => $uid, 'admin_group_id' => $v];
                    }
                    AdminGroupAccess::build()->where(['admin_id'=>$uid])->delete();
//                        throw new \Exception('更新管理组失败');

                    if(!AdminGroupAccess::build()->saveAll($items)){
                        throw new \Exception('更新管理组失败');
                    }
                }
                Db::commit();
                $result['method'] = 'success';
                $result['msg'] = '编辑成功';
                $result['redirect'] = url('/admin/admin/index');
            }catch (\Exception $e){
                Db::rollback();
                $result['msg'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:删除后台用户信息(单个)**注意添加事务**
     * @param $id
     * @return array
     */
    public function delAdmin($id)
    {
        Db::startTrans();
        try {
            if($id == 1){
                throw new \Exception('无法删除超级管理员');
            }
            Admin::destroy($id);
            AdminGroupAccess::build()->where(['admin_id' => $id])->delete();
            // 提交事务
            Db::commit();
            $res = ['method' => 'success', 'msg' => '删除成功', Url::build('/admin/admin/index')];
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $res = ['method' => 'error', 'msg' => $e->getMessage()?$e->getMessage():'删除失败'];
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:编辑管理员个人信息
     * @param $data
     * @param $id
     * @return array
     */
    public function editPersonalInfo($data,$id)
    {
        $result = ['method' => 'error','msg' => '编辑失败'];
        $validate = new AdminValidate();
        $bool = $validate->scene('editPersonalInfo')->check($data);
        $data['password'] = md5($data['password']);
        $data['rpassword'] = md5($data['rpassword']);
        if (!$bool) {
            $result['msg'] = $validate->getError();
        }elseif(Admin::update($data,['id'=>$id],true)){
            $result['method'] = 'success';
            $result['msg'] = '编辑成功';
        }

        return $result;
    }

}
