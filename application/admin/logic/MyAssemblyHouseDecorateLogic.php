<?php
namespace app\admin\logic;

use app\common\model\MyAssemblyHouseDecorate;
use app\common\model\MyAssemblyHouseType;
use app\common\validate\MyHomeDecorateProgram as HouseTypeValidate;

use think\Db;
use think\Url;


class MyAssemblyHouseDecorateLogic extends BaseLogic
{
    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:38 上午
     * description
     * @param $data
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getAllProgram($data)
    {
        $where = ['d.is_deleted'=>0];
        $alias = 'd';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }

        $lists = MyAssemblyHouseDecorate::build()
            ->alias('d')
            ->field([
                'd.program_id','d.house_id','d.buildings_id','d.label label_text','d.name',
                'd.img_src','d.video','d.shape_spark_url'
            ])
            ->where($where)
            ->paginate(20,false,['query'=>$data]);
        return $lists;
    }

    public function delProgram($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = MyAssemblyHouseDecorate::get($id);
        if ($model != null) {
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (MyAssemblyHouseDecorate::build()->update(['is_deleted'=>1],['program_id'=>$id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    public function addOrEdit($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $postData['buildings_id'] = MyAssemblyHouseType::build()
            ->where(['house_id' => $postData['house_id']])
            ->value('buildings_id');
        $validate = new HouseTypeValidate();
        $postData['label'] = json_encode($postData['label']);
        if (!$validate->scene('insertOrEdit2')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (MyAssemblyHouseDecorate::addOrEdit($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    public function getDecorate($id = null)
    {
        return $id != null > 0 ? MyAssemblyHouseDecorate::get($id) : null;
    }
}