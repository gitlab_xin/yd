<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/31
 * Time: 11:16
 */

namespace app\admin\logic;

use app\common\model\ShopSupplierParams as ParamsModel;
use app\common\validate\ShopSupplierParams as ParamsValidate;
use think\Url;

class ShopSupplierParamsLogic extends BaseLogic
{
    /***
     * @author: Rudy
     * @time: 2017年7月31日
     * description:获取商家配置参数
     * @param array $field
     * @param int $supplier_id
     * @return mixed
     */
    public function getParams($field = ['id','title','content'],$supplier_id = 0)
    {
        return objToArray(ParamsModel::build()->field($field)->where(['supplier_id'=>$supplier_id])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:获取单个商家配置参数
     * @param $id
     * @return null|static
     */
    public function getParam($id)
    {
        return ParamsModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:编辑配置参数
     * @param $data
     * @return array
     *
     */
    public function editParam($data,$id)
    {
        $res = ['method'=>'error','msg'=>'编辑失败'];

        $validate = new ParamsValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(ParamsModel::update($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = '编辑成功';
                $res['redirect'] = Url::build('/admin/shop_supplier_params');
            }
        }
        return $res;
    }
}