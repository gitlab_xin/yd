<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/3
 * Time: 17:16
 */

namespace app\admin\logic;


use app\common\model\Config;
use app\common\validate\Config as ConfigValidate;
use think\Url;

class ConfigLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:获取全部参数
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getConfigs()
    {
        return Config::build()->field(['id','key'])->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:获取单个config
     * @param $id
     * @return null|static
     */
    public function getConfig($id)
    {
        return Config::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:编辑推荐配置
     * @param $data
     * @param $id
     * @return array
     */
    public function editConfig($data,$id)
    {
        $res = ['method'=>'error','msg'=>'编辑失败'];
        $validate = new ConfigValidate();
        if(!isset($data['key'])){
            $res['msg'] = '缺少推荐模块参数';
        }elseif(!$validate->scene($data['key'])->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            unset($data['id'],$data['key']);
            $data['create_time'] = time();
            $data['value'] = json_encode($data);
            if(Config::update($data,['id'=>$id],true)){
                $res['method'] = 'success';
                $res['msg'] = '编辑成功';
                $res['redirect'] = Url::build('/admin/config/index');
            }
        }
        return $res;
    }
}