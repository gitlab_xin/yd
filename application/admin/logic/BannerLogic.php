<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 15:46
 */

namespace app\admin\logic;

use app\common\model\Banner as BannerModel;
use app\common\validate\Banner as BannerValidate;
use think\Url;

class BannerLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:获取banner,排序获取,没有逻辑直接调用model
     * @return mixed
     */
    public function getBannersGroup($data, $orderby = 'device_type,module_type,banner_id desc')
    {
        $where = [];
        $this->handleSearch($where, $data);

        return BannerModel::build()
            ->field('create_time,update_time', true)
            ->where($where)
            ->order($orderby)
            ->paginate(20, false, ['query' => $data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:获取单个banner信息
     * @param $id
     * @return null|static
     */
    public function getBannerInfo($id)
    {
        $info = BannerModel::get($id);
//        $tem = spiltRelated($info->related);
//        $info->redirect_type = $tem['redirect_type'];
//        $info->related = $tem['related'];
        return $info;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:添加或编辑banner
     * @param $postData
     * @param $maxPic
     * @param int $id
     * @return array
     */
    public function addOrEditBanner($postData, $maxPic, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';

        $res = ['method' => 'error', 'msg' => $handler . '失败'];
        //1.验证post的数据
        $validate = new BannerValidate();
        //修改跳转链接验证规则
        if ($postData['redirect_type'] == 'url') {
            $validate->changeRule('related', 'url|max:255');
        } elseif ($postData['redirect_type'] == 'customroom') {
            $validate->changeRule('related', 'integer');
        } else {
            $validate->changeRule('related', 'require|integer');
        }

        if (!$validate->scene('addOrEdit')->check($postData)) {
            $res['msg'] = $validate->getError();
        } elseif (BannerModel::checkMaxLimit($maxPic, ['module_type' => $postData['module_type'], 'device_type' => $postData['device_type']], $id)) {
            //2.验证是否超过最大轮播图限制
            $res['msg'] = '超过轮播图最大限制' . $maxPic;
        } else {
            //3.上传图片
            if ($postData['redirect_type'] != 'url') {
                //跳转规则
                if($postData['redirect_type'] == 'store'){
                    $postData['related'] = '/mall-goods-detail?goodsId='.$postData['related'];
                }else{
                    $postData['related'] = $postData['redirect_type'] . '://' . ($postData['redirect_type'] == 'customroom' ? '' : ('id=' . $postData['related']));

                }
            }
            //4.判断是否成功添加数据库
            if (BannerModel::saveBanner($postData, $id)) {
                $res['method'] = 'success';
                $res['msg'] = $handler . '成功';
                $res['redirect'] = Url::build('/admin/banner/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:删除banner
     * @param $id
     * @return array
     */
    public function delBanner($id)
    {
        return BannerModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }
}