<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/15
 * Time: 11:02
 */

namespace app\admin\logic;

use app\admin\model\AdminMenu as MenuModel;
use app\admin\model\AdminGroup as AdminGroupModel;
use app\admin\model\AdminGroupMenu as GroupMenuModel;
use think\Db;
use think\Url;

class GroupLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:获取所有管理组信息
     * @return mixed
     */
    public function getGroupInfos()
    {
        return objToArray(AdminGroupModel::build()->field(['id','name','description'])->order(['list_order'=>'ASC'])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:获取单个管理组信息
     * @param $id
     * @return null|static
     */
    public function getGroupInfo($id)
    {
        return AdminGroupModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:添加或编辑管理组信息
     * @param $postData
     * @param int $id
     * @return array
     */
    public function addOrEditGroup($postData,$id = 0)
    {
        $res = [];
        $handler = $id == 0 ?'添加':'编辑';

        if(AdminGroupModel::saveGroup($postData,$id)){
            //成功
            $res['method'] = 'success';
            $res['msg'] = $handler.'成功';
            $res['redirect'] = Url::build('/admin/group/index');
        }else{
            //失败
            $res['method'] = 'error';
            $res['msg'] = $handler.'失败';
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:删除管理组(同时删除中间表,注意加事务)
     * @param int $id
     * @return array
     */
    public function delGroup($id = 0)
    {
        $res = [];
        if($id != 0) {
            Db::startTrans();
            try {
                AdminGroupModel::destroy($id);
                AdminGroupModel::delGroupAdmins($id);
                // 提交事务
                Db::commit();
                $res = ['method'=>'success','msg'=>'删除成功'];
            } catch (\Exception $e) {
                // 回滚事务
                $res = ['method'=>'error','msg'=>'删除失败'];
                Db::rollback();

            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:保存或修改权限**注意开启事务**
     * @param $post
     * @return array
     */
    public function saveRules($post)
    {
        $res = [];
        Db::startTrans();
        try{
            //删除group_id为$id的中间表信息
            GroupMenuModel::build()->where(['group_id'=>$post['id']])->delete();
            //批量插入
            $data = [];
            foreach ($post['rules'] as $key => $menu_id){
                $data[$key]['group_id'] = $post['id'];
                $data[$key]['menu_id'] = $menu_id;
            }
            GroupMenuModel::build()->saveAll($data);
            Db::commit();
            $res = ['method'=>'success','msg'=>'操作成功'];
        }catch (\Exception $e){
            Db::rollback();
            $res = ['method'=>'error','msg'=>'操作失败'];
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:获取权限
     * @param $groupId
     * @return mixed
     */
    public function getRules($groupId)
    {
        return GroupMenuModel::getMenuIdsByGroupId($groupId);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月24日
     * description:获取所有menu
     * @return mixed
     */
    public function getMenus()
    {
        return objToArray(MenuModel::build()->field(['id','name','parent_id','list_order'])->order(['list_order'=>'ASC'])->select());
    }
}