<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:53
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\CooperativePartner as PartnerModel;
use app\common\validate\CooperativePartner as PartnerValidate;

class CooperativePartnerLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取所有数据
     * @return \think\Paginator
     */
    public function getAll()
    {
        return PartnerModel::build()->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加或编辑
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data,$id = 0)
    {
        $handler = $id == 0 ?'添加':'编辑';
        $res = ['method'=>'error','msg'=> $handler.'失败'];

        $validate = new PartnerValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }elseif(PartnerModel::savePartner($data,$id)){
            $res['method'] = 'success';
            $res['msg'] = $handler.'成功';
            $res['redirect'] = Url::build('/admin/cooperative_partner/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return PartnerModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return PartnerModel::get($id);
    }
}