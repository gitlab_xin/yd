<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 14:32
 */

namespace app\admin\logic;

use app\common\tools\wechatUtil;
use think\Db;
use app\common\tools\RefundUtil;
use app\common\model\ShopOrder as OrderModel;
use app\common\model\ShopOrderArea as AreaModel;
use app\common\model\ShopProductStandard as StandardModel;
use app\common\model\ShopOrderLogistics as LogisticsModel;
use app\common\validate\ShopOrderLogistics as LogisticsValidate;
use app\common\model\ShopOrderOfficialLogistics as OfficialLogisticsModel;

class ShopOrderLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:获取所有订单
     * @param $data
     * @return \think\Paginator
     */
    public function getOrders($data)
    {
        $where = [];
        $alias = 'o';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }

        return OrderModel::build()->alias($alias)
            ->join('shop_order_product op','o.order_id = op.order_id','left')
            ->join('user u','o.user_id = u.user_id','left')
            ->field(['o.order_id','o.order_num','o.price','o.pay_type','o.status','o.count','o.pay_type','o.logistics_id',
                'op.product_name','op.standard_name','u.username'])
            ->where($where)
            ->order(['o.order_id'=>'DESC'])
            ->paginate(20,false,['query'=>$data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:获取单个订单详情
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getOrderDetail($id)
    {
        return OrderModel::build()->alias('o')
            ->join('shop_order_product op','o.order_id = op.order_id','left')
            ->join('shop_order_charge oc','o.order_id = oc.order_id','left')
            ->join('shop_supplier s','o.supplier_id = s.supplier_id','left')
            ->join('user u','o.user_id = u.user_id','left')
            ->field(['o.order_id','o.order_num','o.price','o.pay_type','o.status','o.count','o.pay_type',
                'oc.product_price','oc.freight_charge','oc.processing_charge','oc.home_charge','oc.service_charge',
                'op.product_name','op.standard_name','s.name as supplier_name','u.username'])
            ->where(['o.order_id'=>$id])
            ->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    public function handleSearch(&$where = array(), $data, $alias = '',$time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias);
        //如果有username这个key
        if(isset($where[$alias.'username'])){
            $where['u.username'] = $where[$alias.'username'];
            unset($where[$alias.'username']);
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月12日
     * description:退款操作
     * @param $id
     * @return array
     */
    public function refund($id)
    {
        $res = ['method'=>'error','msg'=>'退款失败'];
        if(($order = OrderModel::get($id)) == null){//若该订单不存在
            $res['msg'].=',该订单不存在';
        }elseif($order->status != ('待发货'||'退款中') ){///非待发货下不准退款
            $res['msg'].=',该订单状态处于['.$order->status.']下无法退款';
        }else{
            /*
             * 开启事务,先进行本地服务器数据库操作,再对外进行curl操作
             * 记得按顺序来,否则可能会导致退款成功,但是由于数据库操作异常而导致退款后数据错误
             */
            Db::startTrans();
            try{
                $ramdomNum = wechatUtil::getRandomString(32);//获取随机的号码

                $wholeOrder = OrderModel::build()->field(['sum(price) as sum','count(*) as count'])->where(['bill_no'=>$order->bill_no])->find()->toArray();
//                正式用
//                $sum = $wholeOrder['count'] == 1?$order->price:$wholeOrder['sum'];
//                $fee = $order->price;

                //测试用
                $sum = $wholeOrder['count'] == 1?0.01:0.01*$wholeOrder['count'];
                $fee = 0.01;

                if(!StandardModel::build()->where(['standard_id'=>$order->standard_id])->setInc('stocks',$order->count)){
                    throw new \Exception('库存恢复失败');
                }
                $order->refund_no = $ramdomNum;
                $order->is_refund = 'yes';
                $order->status = '已退款';
                if(!$order->save()){
                    throw new \Exception('订单状态修改失败');
                }

                if(!RefundUtil::getInstance()->setProps(['payType'=>$order->pay_type,'sum'=>$sum,'fee'=>$fee,'billNum'=>$order->bill_no])->exec()){
                    throw new \Exception('退款失败');
                }

                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '退款成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:修改订单状态到已发货
     * @param $data
     * @return array
     */
    public function delivery($data)
    {
        $res = ['method'=>'error','msg'=>'发货失败'];

        $validate = new LogisticsValidate();
        if(!$validate->scene('add')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            $data['order_type'] = 'shop';
            Db::startTrans();
            try{
                $order = OrderModel::get($data['order_id']);
                $data['user_id'] = $order->user_id;
                //插入物流表
                if(!($model = LogisticsModel::create($data,true))){
                    throw new \Exception('物流状态添加失败');
                }
                //插入官方物流详情表
                if($data['logistics_type'] == 'official'){
                    $data['logistics_id'] = $model->logistics_id;
                    $data['content'] = '你的包裹正在揽收中';
                    if(!OfficialLogisticsModel::create($data,true)){
                        throw new \Exception('物流状态添加失败');
                    }
                }
                //更改订单状态
                if(!$order->isUpdate()->save(['status'=>'待收货','logistics_id'=>$model->logistics_id],['order_id'=>$data['order_id']])){
                    throw new \Exception('订单状态更新失败');
                }

                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '发货成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取收获地址
     * @param $order_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getAddress($order_id)
    {
        return AreaModel::build()->field(['province','city','county','town','detail','username','phone'])
            ->where(['order_id'=>$order_id])->find();
    }
}