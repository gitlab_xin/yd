<?php
namespace app\admin\logic;

use app\common\model\MyAssembleBuildingsLabel;
use think\Url;
use think\Db;

/**
 * Class MyHomeBuildingsFloorLogic
 * @package app\admin\logic
 */
class MyAssemblyBuildingsLabelLogic extends BaseLogic
{

    public function getList($data){
        return MyAssembleBuildingsLabel::build()
            ->alias('l')
            ->join("(select parent_id, count(id) as l_count from yd_my_assembly_buildings_label where is_deleted = 0 group by parent_id) l_c",'l_c.parent_id = l.id','left')
            ->where([
                'l.is_deleted' => '0','l.level' => '1','l.parent_id'=> '0'
            ])
            ->field([
                'l.id','l.name','l.id answer','l_c.l_count'
            ])
            ->order([
                'l_c.l_count' => 'DESC','l.id' => 'DESC'
            ])
            ->select();
    }

    public function del($id){
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = MyAssembleBuildingsLabel::get($id);
        if ($model != null) {
            if (MyAssembleBuildingsLabel::build()->update(['is_deleted'=>1, 'update_time' => time()],['id'=>$id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    public function change($data){
        return MyAssembleBuildingsLabel::build()
            ->where(['id' => $data['id']])
            ->update([
               'update_time' => time(),
               'name' => $data['name'],
            ]);
    }

    public function add($data){
        return MyAssembleBuildingsLabel::build()
            ->insert([
                'parent_id' => $data['id'],'name' => $data['name'],'create_time' => time(),'level' => $data['level']
            ]);
    }
}
