<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/15
 * Time: 14:28
 */

namespace app\admin\logic;

use app\common\model\MyDecorateProduct;
use app\common\model\ShopProduct as ProductModel;
use app\common\model\ShopPopular as PopularModel;
use app\common\model\ShopProductStandard as StandardModel;
use app\common\model\ShopClassify as ClassifyModel;
use app\common\model\ShopSupplier as SupplierModel;
use app\common\model\ConfigArea as AreaModel;
use app\common\validate\ShopProduct as ProductValidate;
use think\Url;
use think\Db;

class ShopProductLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取所有供应商
     * @return mixed
     */
    public function getSuppliers()
    {
        return objToArray(SupplierModel::build()->field(['supplier_id','name'])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:获取所有商品
     * @param $data
     * @param string $is_sale
     * @return \think\Paginator
     */
    public function getProducts($data,$is_sale = '1')
    {
        //有搜索条件的情况下
        $alias = 'p';
        $where = [$alias.'.is_deleted'=>'0',$alias.'.is_sale'=>$is_sale];
        //有参数时进行搜索条件这整理
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }
        //创建model,如果有价格则需要关联表
        $model =  ProductModel::build()->alias($alias);
        if(isset($where['ps.price'])){
            $model->join('yd_shop_product_standard ps','p.product_id = ps.product_id','LEFT')
            ->group('p.product_id');
            $where['ps.is_deleted'] = '0';
        }
        //字段需要
        $fields = [
            $alias.'.product_id',
            $alias.'.name',
            $alias.'.sales',
            $alias.'.like_count',
            $alias.'.look_count',
            $alias.'.collect_count',
            $alias.'.supplier_id',
            $alias.'.is_sale',
        ];
        return $model->field($fields)->where($where)->paginate(20,false, [
            'query' => $data,
        ]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:获取单个商品(pk获取)
     * @param int $id
     * @return mixed
     */
    public function getProduct($id = 0)
    {
        return ProductModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:获取分类
     * @param int $parentId
     * @return mixed
     */
    public function getClassify($parentId = 0)
    {
        return objToArray(ClassifyModel::build()->field('create_time',true)->where(['parent_id' => $parentId])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:获取商品分类一二三级
     * @param $ids
     * @param bool $line(默认连线返回)
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getClassifysForProduct($ids,$line = true)
    {
        return ClassifyModel::getClassifysForProduct($ids,$line);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:获取商品所属分类的同组分类
     * @param $ids
     * @return array
     */
    public function getAllClassifys($ids)
    {
        $arr = objToArray(ClassifyModel::build()->field(['img_src','create_time'],true)->where(['parent_id'=>['in',array_keys($ids)]])->select());

        $result = [];
        foreach ($arr as $v){
            $result[$v['parent_id']][] = $v;
        }
        ksort($ids);
        ksort($result);
        $result = array_combine(array_values($ids),$result);
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:添加或编辑商品
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditProduct($data,$id = 0)
    {
        $handler = $id == 0 ? '添加':'修改';
        $res = ['method'=>'error','msg'=> $handler.'失败'];
        $validate = new ProductValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            //上传图片
            $data['img_src_list'] = implode('|',$data['img_src_list']);
            $this->handlerData($data,'product_param','key','value','key','value');
            //验证成功后保存到数据库
            if(ProductModel::saveProduct($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = $handler.'成功';
                $res['redirect'] = Url::build('/admin/shop_product/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:删除商品(修改商品is_deleted字段)
     * @param $product_id
     * @return $this
     */
    public function delProduct($product_id)
    {
        Db::startTrans();
        try{
            ProductModel::update(['is_deleted'=>1],['product_id'=>$product_id]);
            StandardModel::update(['is_deleted'=>1],['product_id'=>$product_id]);
            Db::commit();
            $res = ['method'=>'success','msg'=>'删除成功'];
        }catch (\Exception $e){
            Db::rollback();
            $res = ['method'=>'error','msg'=>'删除失败'];
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:上下架商品
     * @param $id
     * @param $is_sale
     * @return array
     */
    public function changeProductSale($id,$is_sale)
    {
        $temp = $is_sale?'上架':'下架';
        return ProductModel::update(['is_sale'=>$is_sale],['product_id'=>$id])?['method'=>'success','msg'=>$temp.'成功']:['method'=>'error','msg'=>$temp.'失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:根据父id获取地区
     * @param int $parent_id
     * @return mixed
     */
    public function getArea($parent_id = 0)
    {
        return objToArray(AreaModel::build()->field(['id','name'])->where(['parent_id'=>$parent_id])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    public function handleSearch(&$where = array(), $data, $alias = '',$time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias);
        //如果有price这个key,则换成关联的规格表
        if(isset($where[$alias.'price'])){
            $where['ps.price'] = $where[$alias.'price'];
            unset($where[$alias.'price']);
        }
    }
}