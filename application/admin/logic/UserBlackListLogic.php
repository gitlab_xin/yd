<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/7
 * Time: 10:25
 */

namespace app\admin\logic;

use app\common\model\UserBlackList as BlackListModel;
use app\common\validate\UserBlackList as BlackListValidate;
use think\Url;

class UserBlackListLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:获取所有黑名单
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getBlackList()
    {
        $res = BlackListModel::build()->alias('b')
            ->field(['b.*','u.username','u.mobile','u.avatar'])
            ->join('user u','b.user_id = u.user_id')
            ->select();
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:获取单个黑名单信息
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getDetail($id)
    {
        return BlackListModel::build()->where(['id'=>$id])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:修改黑名单
     * @param $data
     * @param $id
     * @return array
     */
    public function editBlackList($data,$id)
    {
        $res = ['method'=>'error','msg'=>'编辑失败'];

        $validate = new BlackListValidate();

        if(!$validate->scene('ban_time')->changeRule('ban_end_time','|after:'.date('Y-m-d H:i:s'),true)->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            $data['ban_begin_time'] = strtotime($data['ban_begin_time']);
            $data['ban_end_time'] = strtotime($data['ban_end_time']);
            if(BlackListModel::update($data,['id'=>$id],true)){
                $res['method'] = 'success';
                $res['msg'] = '编辑成功';
                $res['redirect'] = Url::build('/admin/user_black_list/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:解封黑名单
     * @param $id
     * @return array
     *
     */
    public function delBlackList($id)
    {
        return BlackListModel::destroy($id)?['method'=>'success','msg'=>'解封成功']:['method'=>'error','msg'=>'解封失败'];
    }
}