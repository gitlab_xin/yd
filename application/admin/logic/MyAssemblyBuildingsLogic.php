<?php
namespace app\admin\logic;

use app\common\model\MyAssemblyBuildings;
use app\common\model\MyAssemblyBuildingsConfig;
use app\common\model\MyAssemblyHouseConfig;
use app\common\validate\MyHomeBuildings;
use think\Url;
use think\Db;

/**
 * Class MyHomeBuildingsFloorLogic
 * @package app\admin\logic
 */
class MyAssemblyBuildingsLogic extends BaseLogic
{

    /**
     * User: zhaoxin
     * Date: 2020/1/17
     * Time: 4:42 下午
     * description
     * @param $data
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($data)
    {
        $where = ['b.is_deleted'=>0];
        $alias = 'b';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
            }

        return MyAssemblyBuildings::build()
            ->alias('b')
            ->field([
                'b.name','b.province','b.city','b.begin_time','b.others_time','b.buildings_id'
            ])
            ->where($where)
            ->paginate(20,false,['query'=>$data]);
    }

    public function getConfigList($data){
        return  MyAssemblyBuildingsConfig::build()
            ->where(['buildings_id' => $data['buildings_id'],'level' => '1'])
            ->field([
                'id',
                'name',
                'id list'
            ])
            ->select();
    }

    public function getInfo($id = null)
    {
        return $id != null > 0 ? MyAssemblyBuildings::get($id) : null;
    }

    public function addOrEdit($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $validate = new MyHomeBuildings();
        if (!$validate->scene('addOrEdit2')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (MyAssemblyBuildings::addOrEdit($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    public function del($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该楼盘'];

        $model = MyAssemblyBuildings::get($id);
        if ($model != null) {

            if (MyAssemblyBuildings::build()->update(['is_deleted' => 1], ['buildings_id' => $id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:47 上午
     * description
     * @return array|false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAllMenu($data)
    {
        if ($res = MyAssemblyBuildingsConfig::build()
            ->where(['buildings_id' => $data['buildings_id']])
            ->order(['id' => 'ASC'])->select()) {
            $res = nodeTree(objToArray($res));
        }
        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/3
     * Time: 11:10 上午
     * description
     * @param $lists
     * @param $data
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function menuStatus($lists, $data){
        //所有该楼盘的配置数据
        foreach ($lists as &$v) {
            $info = MyAssemblyHouseConfig::build()
                ->where(['config_id' => $v['id']])
                ->field(['id','house_id'])
                ->find();
            $v['status'] = '-1';//不可选
            $v['config_id'] = $info['id'];

            $count = MyAssemblyBuildingsConfig::build()
                ->where(['parent_id' => $v['id']])
                ->count();

            //最后一级
            if ($count <= 0) {
                $v['status'] = '0';//可选
                //没有被其他户型选择
                if (empty($info)) {
                    $v['status'] = '0';//可选
                } elseif ($info['house_id'] == $data['house_id']) {
                    $v['status'] = '1';//已选
                } elseif ($info['house_id'] != $data['house_id']) {
                    $v['status'] = '2';//其他户型已选
                }
            }


        }
        return $lists;
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/3
     * Time: 2:39 下午
     * description
     * @param $data
     */
    public function setListOrder($data)
    {
        foreach ($data['list_order'] as $k => $v) {
            $temp = array();
            $temp['list_order'] = $v;
            MenuModel::update($temp, ['id' => $k], true);
        }
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:49 上午
     * description
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function selectMenu($buildings_id)
    {
        $tmpArr = nodeTree(objToArray(MyAssemblyBuildingsConfig::build()->where(['buildings_id' => $buildings_id])->field(['id', 'name', 'parent_id'])->select()));
        $data = array();
        foreach ($tmpArr as $k => $v) {
            $name = $v['level'] == 0 ? '<b>' . $v['name'] . '</b>' : '├─' . $v['name'];
            $name = str_repeat("│        ", $v['level']) . $name;
            $data[$v['id']] = $name;
        }
        return $data;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:49 上午
     * description
     * @param $id
     * @return MyAssemblyBuildingsConfig|null
     * @throws \think\exception\DbException
     */
    public function getMenu($id)
    {
        return MyAssemblyBuildingsConfig::get($id);
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:49 上午
     * description
     * @param $id
     * @return array
     * @throws \think\Exception
     */
    public function delMenu($id)
    {
        $res = ['method' => 'error', 'msg' => '删除失败'];
        if (MyAssemblyBuildingsConfig::build()->where(['parent_id' => $id])->count() > 0) {
            $res['msg'] = '请先删除子菜单再删除菜单';
        } else {
            //删除被选的户型
            MyAssemblyHouseConfig::build()
                ->where(['config_id' => $id])
                ->delete();
            if (MyAssemblyBuildingsConfig::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            }
        }
        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/3
     * Time: 2:39 下午
     * description
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditMenu($data, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $data['level'] = $data['parent_id'] == 0 ? 1 : MyAssemblyBuildingsConfig::build()->where(['id' => $data['parent_id']])->value('level') + 1;
        if (MyAssemblyBuildingsConfig::saveMenu($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
            $res['redirect'] = Url::build('/admin/my_assembly_buildings_config/index',['buildings_id' => $data['buildings_id']]);
        }
        return $res;
    }

    public function change($data){
        //选
        if ($data['is_checked'] == '1'){
            MyAssemblyHouseConfig::build()
                ->insert([
                    'house_id' => $data['house_id'],
                    'buildings_id' => $data['buildings_id'],
                    'config_id' => $data['id'],
                    'create_time' => time(),
                ]);
        } else {
            MyAssemblyHouseConfig::build()
                ->where(['config_id' => $data['id'],'house_id' => $data['house_id'],'buildings_id' => $data['buildings_id']])
                ->delete();
        }
        return true;

    }
}
