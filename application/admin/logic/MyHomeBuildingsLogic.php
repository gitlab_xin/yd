<?php

namespace app\admin\logic;

use app\common\model\MyHomeBuildings as BuildingsModel;
use app\common\validate\MyHomeBuildings;
use app\common\validate\MyHomeFurniture as FurnitureValidate;
use think\Db;

/**
 * Class MyHomeBuildingsLogic
 * @package app\admin\logic
 */
class MyHomeBuildingsLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取楼盘
     * @param null $id
     * @return null|static
     */
    public function getInfo($id = null)
    {
        return $id != null > 0 ? BuildingsModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取楼盘列表
     * @return \think\Paginator
     */
    public function getList()
    {
        return BuildingsModel::build()->where(['is_deleted'=>0])->paginate(20);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:添加或编辑楼盘
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEdit($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new MyHomeBuildings();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (BuildingsModel::addOrEdit($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @param $id
     * @return array
     */
    public function del($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该楼盘'];

        $model = BuildingsModel::get($id);
        if ($model != null) {

            if (BuildingsModel::build()->update(['is_deleted' => 1], ['buildings_id' => $id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
