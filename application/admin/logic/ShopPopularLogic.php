<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 15:31
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\ShopPopular as PopularModel;
use app\common\model\ShopProduct as ProductModel;
use app\common\validate\ShopPopular as PopularValidate;

class ShopPopularLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:根据三级分类获取商品
     * @param $level_three
     * @return mixed
     */
    public function getProductByClassify($level_three)
    {
        return objToArray(ProductModel::build()->field(['product_id','name'])->where(['classify_three_id'=>$level_three,'is_deleted'=>0])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取所有热门商品
     * @return mixed
     */
    public function getPopulars()
    {
        return PopularModel::build()->field(['create_time','update_time'],true)->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取单个热门商品信息
     * @param $id
     * @return null|static
     */
    public function getPopular($id)
    {
        return PopularModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:添加或修改热门商品
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditPopular($data,$id=0)
    {
        $handle = $id == 0?'添加':'编辑';
        $res = ['method'=>'error','msg'=>$handle.'失败'];

        $validate = new PopularValidate();
        if($id != 0){
            $validate->changeRule('product_id',',product_id,'.$id,true);
        }
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(PopularModel::savePopular($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = $handle.'成功';
                $res['redirect'] = Url::build('admin/shop_popular/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:删除热门商品
     * @param $id
     * @return array
     */
    public function delPopular($id)
    {
        $model = PopularModel::build()->field(['img_src'])->where(['popular_id'=>$id])->find();
        $img_src = $model->img_src;
        if($model->delete()){
            $this->delCloudFile($img_src);
            return ['method'=>'success','msg'=>'删除成功'];
        }else{
            return ['method'=>'error','msg'=>'删除失败'];
        }
    }
}