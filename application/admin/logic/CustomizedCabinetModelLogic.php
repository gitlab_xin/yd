<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:53
 */

namespace app\admin\logic;


use think\Url;
use app\common\model\CustomizedCabinetModel as ModelModel;
use app\common\validate\CustomizedCabinetModel as ModelValidate;

class CustomizedCabinetModelLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll($scene_id)
    {
        $data = ['scene_id' => $scene_id];
        return ModelModel::build()->where($data)->paginate(20, false, ['query' => $data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return ModelModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new ModelValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (ModelModel::saveModel($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_model/index', ['scene_id' => $data['scene_id']]);
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return ModelModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }
}