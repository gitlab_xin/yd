<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 9:48
 */

namespace app\admin\logic;

use app\common\model\MessageOfficial;
use app\common\validate\Activity as ActivityValidate;
use app\common\model\Activity as ActivityModel;
use think\Db;
use think\Url;

class ActivityLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:添加或编辑活动
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditActivity($data, $id = 0)
    {
        $validate = new ActivityValidate();

        if ($id != 0) {
            $validate->changeRule('end_time', '|after:' . date('Y-m-d H:i:s', time()), true);
            $handle = '编辑';
        } else {
            $handle = '添加';
        }

        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } else {
            $data['end_time'] = strtotime($data['end_time']);
            $data['activity_begin_time'] = strtotime($data['activity_begin_time']);
            $data['activity_end_time'] = strtotime($data['activity_end_time']);
            if (ActivityModel::saveActivity($data, $id)) {
                $res['method'] = 'success';
                $res['msg'] = $handle . '成功';
                $res['redirect'] = Url::build('/admin/activity/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取所有活动
     * @return \think\Paginator
     *
     */
    public function getActivities($data)
    {
        $where = ['is_deleted' => 0];
        $this->handleSearch($where, $data);
        return ActivityModel::build()->where($where)->paginate(20, false, ['query' => $data,]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取单个活动信息
     * @param $id
     * @return null|static
     *
     */
    public function getActivity($id)
    {
        return ActivityModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:删除活动
     * @param $id
     * @return array
     *
     */
    public function delActivity($id)
    {
        Db::startTrans();
        try {
            ActivityModel::update(['is_deleted' => 1], ['activity_id' => $id]);
            MessageOfficial::build()->where(['activity_id' => $id])->delete();
            Db::commit();
            return ['method' => 'success', 'msg' => '删除成功'];
        } catch (\Exception $e) {
            Db::rollback();
            return ['method' => 'error', 'msg' => '删除失败'];
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:复写搜索条件
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    public function handleSearch(&$where = array(), $data, $alias = '', $time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias, $time);
        if (isset($where['status'])) {
            unset($where['status']);
            if (isset($data['status'])) {
                switch ($data['status']) {
                    case 1:
                        $where['end_time'] = ['>', time()];
                        break;
                    case 2:
                        $where['end_time'] = ['<', time()];
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:提前结束活动
     * @return array
     */
    public function endActivity($id)
    {
        return ActivityModel::update(['end_time' => time()], ['activity_id' => $id]) ? ['method' => 'success', 'msg' => '提前结束成功'] : ['method' => 'error', 'msg' => '提前结束失败'];
    }
}