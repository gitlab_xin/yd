<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:53
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\CustomizedCabinetDoorColor as CabinetDoorColorModel;
use app\common\model\CustomizedColor as ColorModel;

class CustomizedCabinetDoorColorLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取所有数据
     * @return array
     */
    public function getAll($cabinet_color_id)
    {
        return CabinetDoorColorModel::build()->alias('cdc')->where(['cdc.cabinet_color_id' => $cabinet_color_id])
            ->join('customized_color c', 'c.color_id = cdc.color_id', 'left')->select()->toArray();
    }

    public function getAllColor()
    {
        return ColorModel::build()->field(['color_id', 'color_name', 'img_src_row_light as img_src'])->where(['is_deleted' => 0])->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加或编辑
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id)
    {
        $res = ['method' => 'error', 'msg' => '编辑失败'];

        $model = CabinetDoorColorModel::build();

        $model->where(['cabinet_color_id' => $id])->delete();
        $list = [];
        foreach ($data['color_id'] as $color_id) {
            $list[] = ['color_id' => $color_id, 'cabinet_color_id' => $id];
        }

        if ($model->saveAll($list)) {
            $res['method'] = 'success';
            $res['msg'] = '编辑成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_door_color/index', ['id' => $id]);
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return CabinetDoorColorModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function quickSet($cabinet_id)
    {
        $all = CabinetDoorColorModel::build()->where(['cabinet_color_id' => $cabinet_id])->column('color_id');
        $color = ColorModel::build()->where(['is_deleted' => 0])->column('color_id');
        $set = array_diff($color, array_intersect($all, $color));
        if (!empty($set)) {
            $data = [];
            foreach ($set as $color_id) {
                $data[] = ['cabinet_color_id' => $cabinet_id, 'color_id' => $color_id];
            }
            CabinetDoorColorModel::build()->saveAll($data);
            return ['method' => 'success', 'msg' => '编辑成功', 'redirect' => Url::build('/admin/customized_cabinet_door_color/index', ['id' => $cabinet_id])];

        } else {
            return ['method' => 'error', 'msg' => '已拥有所有花色'];
        }
    }

    public function quickDel($cabinet_id)
    {
        CabinetDoorColorModel::build()->where(['cabinet_color_id' => $cabinet_id])->delete();
        return ['method' => 'success', 'msg' => '编辑成功', 'redirect' => Url::build('/admin/customized_cabinet_door_color/index', ['id' => $cabinet_id])];
    }
}