<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 16:57
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\HelpCenterClassify as ClassifyModel;
use app\common\validate\HelpCenterClassify as ClassifyValidate;

class HelpCenterClassifyLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取所有帮助中心
     * @return \think\Paginator
     */
    public function getList()
    {
        return ClassifyModel::build()->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:添加或编辑
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new ClassifyValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (ClassifyModel::saveHelp($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/help_center_classify/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:删除帮助中心
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return ClassifyModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return ClassifyModel::get($id);
    }
}