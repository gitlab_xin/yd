<?php

namespace app\admin\logic;

use app\common\model\MyHomeDecoratePoint;
use app\common\model\MyHomeDecorateProgram as ProgramModel;
use app\common\validate\MyHomeHouseType as HouseTypeValidate;

/**
 * Class MyHomeDecorateProgramLogic
 * @package app\admin\logic
 */
class MyHomeDecorateProgramLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型
     * @param null $id
     * @return null|static
     */
    public function getProgram($id = null)
    {
        return $id != null > 0 ? ProgramModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @param int $house_id 户型ID
     * @return false|static[]
     */
    public function getAllProgram($house_id)
    {
        $lists = ProgramModel::build()->where(['house_id'=>$house_id])->select();
        return $lists;
    }


    /**
     * @author: Airon
     * @time: 2018年4月
     * description:删除模型
     * @param $id
     * @return array
     */
    public function delProgram($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该模型'];

        $model = ProgramModel::get($id);
        if ($model != null) {
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (ProgramModel::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    public function updateProgram($data){
        $res = ['method' => 'error', 'msg' => '编辑失败'];
        $model = ProgramModel::get($data['program_id']);
        if ($model != null) {

            $model->program_id = $data['program_id'];
            $model->shapespar_url = $data['shapespar_url'];

            if(!$model->save()){
                return $res;
            }

            $res['method'] = 'success';
            $res['msg'] = '编辑成功';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取某户型所有装修亮点
     * @param $house_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getDecorate($house_id)
    {
        return MyHomeDecoratePoint::build()->alias('d')
            ->join('yd_my_home_house_decorate hd', 'hd.decorate_id = d.decorate_id')
            ->join('yd_my_home_house_type h', 'h.house_id = hd.house_id')
            ->where(['hd.house_id' => $house_id])
            ->field(['d.decorate_id', 'd.name', 'd.sub_title'])
            ->select();
    }
}
