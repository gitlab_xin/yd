<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 17-11-2
 * Time: 上午11:37
 */

namespace app\admin\logic;

use app\admin\behavior\CheckAuth;
use app\common\tools\RedisUtil;
use app\common\validate\CustomizedComponent;
use think\Request;
use think\Url;
use app\common\model\CustomizedComponent as ComponentModel;
use app\common\validate\CustomizedComponent as ComponentValidate;
use app\api\logic\ProductLogic;

class CustomizedComponentLogic extends BaseLogic
{
    public function getDetail($id)
    {
        return ComponentModel::get($id);
    }

    public function getAll($data)
    {
        $where = [];
        $this->handleSearch($where, $data);
        return ComponentModel::build()->where($where)->paginate(20, false, ['query' => $data]);
    }

    public function del($id)
    {
        return ComponentModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function buildCombineData($data)
    {
        $data['component_id'] = 0;
        $data['min_width'] = 0;
        $data['max_width'] = 0;
        $data['width'] = 1200;
        $data['img_src'] = '';
        $data['img_src_without_bg'] = '';
        $data['scheme_type'] = 'RQS';

        $productLogic = new ProductLogic();
        $data['products'] = [
//            $productLogic->createZDYYG_ZLB(400, 0, 400, 0, 16, $data['height'], 490, '000')->mainInfo()
        ];

        RedisUtil::getInstance()->set('component_temp', serialize($data));

        return $this->buildUrl();
    }

    public function buildModuleData($data)
    {
        $data['component_id'] = 0;
        $data['min_width'] = 0;
        $data['max_width'] = 0;
        $data['img_src'] = '';
        $data['img_src_without_bg'] = '';

        $col_pros = [];
        $productLogic = new ProductLogic();
        switch ($data['scheme_type']) {
            case 'RQS':
                $col_pros[] = $productLogic->createZDYYG_BB(0, 0, 0, 1201, 12, $data['height'] - 1201, $data['width'], '000', 0)->mainInfo();
                $col_pros[] = $productLogic->createZDYYG_BB(0, 0, 0, 0, 12, 1201, $data['width'], '000', 0)->mainInfo();
                $col_pros[] = $productLogic->createZDYYG_HGB(0, 0, 0, $data['height'] - 25, 25, 490, $data['width'], '000', 2)->mainInfo();
                break;
            case 'QW':
                $col_pros[] = $productLogic->createZDYYG_BB(0, 0, 0, 1201, 12, $data['height'] - 1201, $data['width'], '000', 0)->mainInfo();
                $col_pros[] = $productLogic->createZDYYG_BB(0, 0, 0, 0, 12, 1201, $data['width'], '000', 0)->mainInfo();
                $col_pros[] = $productLogic->createZDYYG_HGB_GD_DD(0, $data['height'], 0, 0, 25, 574, $data['width'], '000', 0, 0, 1)->mainInfo();
                $col_pros[] = $productLogic->createZDYYG_HGB_GD_DD(0, 66, 0, ($data['height'] - 66), 25, 574, $data['width'], '000', 3, 0, 1)->mainInfo();
                break;
            case 'BZ':
                $type = strval($data['height'] / 100);
                $col_pros[] = $productLogic->createBZYG_BB(0, 0, 0, 1201, 12, $data['height'] - 1201, $data['width'], '000', 0, $type)->mainInfo();
                $col_pros[] = $productLogic->createBZYG_BB(0, 0, 0, 0, 12, 1201, $data['width'], '000', 0, $type)->mainInfo();
                $col_pros[] = $productLogic->createBZYG_DD(0, 72, 0, ($data['height'] - 72), 25, 72, $data['width'], '001', 0, $type)->mainInfo();
                $col_pros[] = $productLogic->createBZYG_HGB_GD(0, 0, 0, 0, 25, 554, $data['width'], '001', 1, 0, $type)->mainInfo();
                $col_pros[] = $productLogic->createBZYG_HGB_GD(0, 25 + 74, 0, $data['height'] - 25 - 74, 25, 554, $data['width'], '001', 2, 0, $type)->mainInfo();
                break;
        }
        $data['products'] = $col_pros;

        RedisUtil::getInstance()->set('component_temp', serialize($data));

        return $this->buildUrl();
    }

    public function buildUrl($component_id = 0)
    {
        $auth = new CheckAuth();
        $token = $auth->buildToken();
        $url_param = [
            'component_id' => $component_id,
            'token' => $token,
            'redirect' => Url::build('/admin/' . upToLine(Request::instance()->controller()) . '/index', '', true, true)
        ];
        return COMPONENT_URL . '?' . http_build_query($url_param);
    }

    public function addOrEdit($data,$component_id){
        $handler = $component_id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        if (ComponentModel::setImgSrcOriginal($data, $component_id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/customized_component_combine/index');
        }
        return $res;
    }

    public function buildProducts($id)
    {
        $productLogic = new ProductLogic();

        $model = $this->getDetail($id);
        $width = $model->min_width;
        $height = $model->height;

        $arr = $com_products = [];

        $arr[] = $productLogic->createZDYYG_ZLB(400, 0, 400, 0, 16, $height, 490, '000')->mainInfo();
        $arr[] = $productLogic->createZDYYG_HGB(416, 0, 416, $height - 288, 25, 490, 400, '000', 2)->mainInfo();
        $s_p = $productLogic->createZDYYG_HGB(0, 0, 0, 288, 25, 490, 400, '000', 2);
//        $com_products[] = $productLogic->createZDYYG_LL(0, 0, 0, $s_p->getM_top_mm() + 58, $s_p->getProduct_width());
        $com_products[] = $productLogic->createZDYYG_KC(0, 0, 0, $s_p->getM_top_mm() + 58, $s_p->getProduct_width());
//        $com_products[] = $productLogic->createZDYYG_LL(0, 0, 0, $s_p->getM_top_mm() + 58 + 448, $s_p->getProduct_width());
//        $com_products[] = $productLogic->createZDYYG_LL(0, 0, 0, $s_p->getM_top_mm() + 58 + 668, $s_p->getProduct_width());

        $s_p->setCom_products($com_products);

        $arr[] = $s_p->mainInfo();

//        $model->products = json_encode($arr, JSON_UNESCAPED_UNICODE);
//        $model->isUpdate()->save();
        exit;
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function buildProductsModule($id)
    {
        $productLogic = new ProductLogic();

        $model = $this->getDetail($id);
        $width = $model->min_width - 32;
        $height = $model->height;
        $col_pros = [];

        $col_pros[] = $productLogic->createBZYG_BB(0, 0, 0, 1201, 12, 744, $width, '000', 0, '20')->mainInfo();
        $col_pros[] = $productLogic->createBZYG_BB(0, 0, 0, 0, 12, 1201, $width, '000', 0, '20')->mainInfo();

        $col_pros[] = $productLogic->createBZYG_HGB_YTG_GD(0, $height, 0, 0, 25, 554, $width, '000', 1, 0, '20')->mainInfo();
//        $col_pros[] = $productLogic->createBZYG_HGB_YTG(0, $height - 352, 0, 352, 25, 554, $width, '000', 1, 0, '20')->mainInfo();
        $hgb = $productLogic->createBZYG_HGB(0, $height - 1088, 0, 1088, 25, 554, $width, '000', 1, 0, '20');
        $com = [];
        $com[] = $productLogic->createBZYG_LL(0, $height - (1088 + 58), 0, 1088 + 58, $width, '00', 0);
        $com[] = $productLogic->createBZYG_KC(0, $height - (1088 + 224 + 58), 0, 1088 + 224 + 58, $width, 0);
        $hgb->setCom_products($com);
        $col_pros[] = $hgb->mainInfo();


        $col_pros[] = $productLogic->createBZYG_HGB_GD(0, 25 + 74, 0, ($height - 25 - 74), 25, 554, $width, '000', 2, 0, '20')->mainInfo();
        $col_pros[] = $productLogic->createBZYG_DD(0, 72, 0, ($height - 72), 25, 72, $width, '000', 0, '20')->mainInfo();

//        dump($col_pros);
//        exit;

        $model->products = json_encode($col_pros, JSON_UNESCAPED_UNICODE);
        $model->isUpdate()->save();
        exit;
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        exit;
    }
}