<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/5
 * Time: 14:43
 */

namespace app\admin\logic;

use think\Db;
use think\Url;
use app\common\model\DemandBanner as BannerModel;
use app\common\model\DemandScheme as SchemeModel;

class DemandBannerLogic
{
    public function getDetail($id)
    {
        return BannerModel::get($id);
    }

    public function getAll()
    {
        return BannerModel::build()->alias('b')->field(['d.title','b.demand_id','b.banner_id','b.scheme_pic'])
            ->join('yd_demand d','d.demand_id = b.demand_id','LEFT')
            ->paginate(20);
    }

    public function getSchemesByDemand($demand_id)
    {
        return objToArray(SchemeModel::build()->alias('s')->field(['s.id','sd.scheme_pic','s.is_good'])
            ->where(['s.demand_id'=> $demand_id,'s.is_accepted'=> 0])
            ->join('yd_demand_scheme_detail sd','s.scheme_detail_id = sd.scheme_detail_id','LEFT')
            ->select());
    }

    public function add($data)
    {
        $res = ['method'=>'error','msg'=>'添加失败'];
        $count = BannerModel::build()->where(['demand_id'=>$data['demand_id']])->count();

        if($count > 0){
            $res['msg'] = '该需求已经在banner中';
        }else{
            Db::startTrans();
            try{
                //插入banner表中
                $data['scheme_pic'] = SchemeModel::build()->alias('s')
                    ->where(['s.demand_id'=> $data['demand_id'],'s.is_accepted'=> 1])
                    ->join('yd_demand_scheme_detail sd','s.scheme_detail_id = sd.scheme_detail_id','LEFT')
                    ->value('sd.scheme_pic');
                if(!BannerModel::create($data,true)){
                    throw new \Exception('添加banner失败');
                }
                //把对应案例修改成优秀案例
                if(isset($data['scheme_id']) && count($data['scheme_id'])>0 && !SchemeModel::update(['is_good'=>1],['id'=>['in',$data['scheme_id']]],true)){
                    throw new \Exception('设置优秀案例失败');
                }
                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '添加成功';
                $res['redirect'] = Url::build('/admin/demand_banner/index');
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }
        return $res;
    }

    public function edit($data)
    {
        $res = ['method'=>'error','msg'=>'修改失败'];


        Db::startTrans();
        try{
            //把所有案例修改成is_good = 0
            if(!SchemeModel::update(['is_good'=>0],['demand_id'=>$data['demand_id'],'is_good'=>1],true)){
                throw new \Exception('案例恢复失败');
            }
            //把对应案例修改成优秀案例
            if(isset($data['scheme_id']) && count($data['scheme_id'])>0 && !SchemeModel::update(['is_good'=>1],['id'=>['in',$data['scheme_id']]],true)){
                throw new \Exception('设置优秀案例失败');
            }
            Db::commit();
            $res['method'] = 'success';
            $res['msg'] = '修改成功';
            $res['redirect'] = Url::build('/admin/demand_banner/index');
        }catch (\Exception $e){
            Db::rollback();
            $res['msg'] = $e->getMessage();
        }

        return $res;
    }

    public function del($demand_id)
    {
        $res = ['method'=>'error','msg'=>'删除失败'];
        Db::startTrans();
        try{
            $where = ['demand_id'=>$demand_id];
            //删除banner
            if(!BannerModel::destroy($where)){
                throw new \Exception('banner表删除失败');
            }
            //把所有案例的is_good都设置成0
            if(!SchemeModel::update(['is_good'=>0],$where)){
                throw new \Exception('scheme修改失败');
            }
            Db::commit();
            $res['msg'] = '删除成功';
            $res['method'] = 'success';
        }catch (\Exception $e){
            Db::rollback();
            $res['msg'] = $e->getMessage();
        }
        return $res;
    }
}