<?php

namespace app\admin\logic;

use app\common\model\MyHomeBuildingsUnit as UnitModel;
use app\common\model\MyHomeHouseType;
use app\common\validate\MyHomeBuildingsRidgepole;
use think\Db;

/**
 * Class MyHomeBuildingsUnitLogic
 * @package app\admin\logic
 */
class MyHomeBuildingsUnitLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取单元
     * @param null $id
     * @return null|static
     */
    public function getInfo($id = null)
    {
        return $id != null > 0 ? UnitModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取单元列表
     * @param $buildings_id
     * @param $floor_id
     * @return array
     */
    public function getList($buildings_id, $floor_id)
    {
       return  UnitModel::build()->alias('u')->join('yd_my_home_house_type house','house.house_id = u.house_id','left')
           ->field(['u.*','house.name as house_type_name','house.img_src'])
           ->where(['u.is_deleted' => 0, 'u.buildings_id' => $buildings_id, 'u.floor_id' => $floor_id])->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取所有户型
     * @return false|static[]
     */
    public function getAllHouse()
    {
        $lists = MyHomeHouseType::build()->where(['is_deleted'=>0])->select()->toArray();
        return $lists;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:添加或编辑单元
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEdit($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new MyHomeBuildingsRidgepole();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (UnitModel::addOrEdit($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @param $id
     * @return array
     */
    public function del($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该单元'];

        $model = UnitModel::get($id);
        if ($model != null) {

            if (UnitModel::build()->update(['is_deleted' => 1], ['unit_id' => $id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
