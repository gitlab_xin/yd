<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 11:46
 */

namespace app\admin\logic;

use app\common\model\User;
use app\common\model\UserDesigner as DesignerModel;
use app\common\model\User as UserModel;
use app\common\validate\UserDesigner as DesignerValidate;
use think\Db;

class UserDesignerLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:获取列表
     * @param $data
     * @return \think\Paginator
     *
     */
    public function getDesignerList($data)
    {
        $where = [];
        $this->handleSearch($where,$data,'d.');

        return DesignerModel::build()->alias('d')
            ->field(['u.username','d.id','d.name','d.identity_num','d.identity_front_src','d.identity_back_src','d.status','d.remark'])
            ->where($where)
            ->order(['d.id'=>'DESC'])
            ->join('user u','d.user_id = u.user_id','left')
            ->paginate(20,false, [ 'query' => $data,]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:通过或拒绝审核
     * @param $data
     * @param $id
     * @param $status
     * @return array
     */
    public function changeStatus($data,$id,$status)
    {
        $res = ['method'=>'error','msg'=>'操作失败'];
        $data['status'] = $status;
        $validate = new DesignerValidate();

        if(!$validate->scene('changeStatus')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            Db::startTrans();
            try{
                $model = DesignerModel::get($id);
                if(!$model->isUpdate()->save($data)){
                    throw new \Exception('审核失败');
                }
                if($status === 1){
                    UserModel::update(['is_designer'=>1],['user_id'=>$model->user_id]);
                }
                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '操作成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }

        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:获取单个记录
     * @param $id
     * @return null|static
     *
     */
    public function getDetail($id)
    {
        return DesignerModel::get($id);
    }
}