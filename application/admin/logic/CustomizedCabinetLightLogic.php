<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:53
 */

namespace app\admin\logic;


use think\Url;
use app\common\model\CustomizedCabinetLight as LightModel;
use app\common\model\CustomizedCabinetLightGroupMap as LightGroupMapModel;
use app\common\model\CustomizedCabinetSceneLight as SceneLightModel;
use app\common\model\CustomizedCabinetLightAmbinet as AmbinetModel;
use app\common\model\CustomizedCabinetLightDirection as DirectionModel;
use app\common\model\CustomizedCabinetLightPoint as PointModel;
use app\common\model\CustomizedCabinetLightHemisphere as HemisphereModel;
use app\common\model\CustomizedCabinetLightSpot as SpotModel;
use app\common\model\CustomizedCabinetLightRectArea as RectAreaModel;
use app\common\validate\CustomizedCabinetLight as LightValidate;
use app\common\validate\CustomizedCabinetLightAmbinet as AmbinetValidate;
use app\common\validate\CustomizedCabinetLightDirection as DirectionValidate;
use app\common\validate\CustomizedCabinetLightPoint as PointValidate;
use app\common\validate\CustomizedCabinetLightHemisphere as HemisphereValidate;
use app\common\validate\CustomizedCabinetLightSpot as SpotValidate;
use app\common\validate\CustomizedCabinetLightRectArea as RectAreaValidate;

class CustomizedCabinetLightLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll()
    {
        return LightModel::build()->paginate(20);
    }

    public function getList()
    {
        return LightModel::build()->field(['light_id', 'name', 'type'])->select()->toArray();
    }

    public function getSeleted($id)
    {
        return SceneLightModel::build()->where(['scene_id' => $id])->column('light_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return LightModel::get($id);
    }

    public function getDetailByType($type, $light_id)
    {
        switch ($type) {
            case 'AmbientLight':
                $model = AmbinetModel::get($light_id);
                $val = new AmbinetValidate();
                break;
            case 'DirectionalLight':
                $model = DirectionModel::get($light_id);
                $val = new DirectionValidate();
                break;
            case 'PointLight':
                $model = PointModel::get($light_id);
                $val = new PointValidate();
                break;
            case 'HemisphereLight':
                $model = HemisphereModel::get($light_id);
                $val = new HemisphereValidate();
                break;
            case 'SpotLight':
                $model = SpotModel::get($light_id);
                $val = new SpotValidate();
                break;
            case 'RectAreaLight':
                $model = RectAreaModel::get($light_id);
                $val = new RectAreaValidate();
                break;
            default:
                $model = '';
                break;
        }
        return [$model, $val];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @return array
     */
    public function add($data)
    {
        $res = ['method' => 'error', 'msg' => '添加失败'];

        $validate = new LightValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (LightModel::saveLight($data)) {
            $res['method'] = 'success';
            $res['msg'] = '添加成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_light/index');
        }
        return $res;
    }

    public function edit($data)
    {
        $res = ['method' => 'error', 'msg' => '编辑失败'];

        if (LightModel::updateLight($data)) {
            $res['method'] = 'success';
            $res['msg'] = '编辑成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_light/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        LightModel::destroy($id);
        LightGroupMapModel::destroy(['light_id' => $id]);
        return ['method' => 'success', 'msg' => '删除成功'];
    }

    public function addLight($data)
    {
        $model = new SceneLightModel();
        $scene_id = $data['scene_id'];
        $model->where(['scene_id' => $data['scene_id']])->delete();
        if (isset($data['light_id'])) {
            $model->saveAll(array_map(function ($v) use ($scene_id) {
                return ['light_id' => $v, 'scene_id' => $scene_id];
            }, $data['light_id']));
        }
        return ['method' => 'success', 'msg' => '修改成功'];
    }
}