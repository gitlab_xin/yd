<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/16
 * Time: 14:50
 */

namespace app\admin\logic;

use app\common\model\CustomizedEvaluate as EvaluateModel;

class CustomizedEvaluateLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:获取全部评价
     * @return \think\Paginator
     */
    public function getAll()
    {
        return EvaluateModel::build()->alias('e')->field(['e.*','u.username'])
            ->join('user u','e.user_id = u.user_id','LEFT')
            ->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:获取详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return EvaluateModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:删除评论
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return EvaluateModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }
}