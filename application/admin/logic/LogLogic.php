<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/7
 * Time: 10:04
 */

namespace app\admin\logic;

use app\admin\model\AdminLog;

class LogLogic extends BaseLogic
{
    public function getLogs($data)
    {
        $where = [];
        if(count($data)>0){
            $this->handleSearch($where,$data);
        }
        return AdminLog::build()->where($where)->order(['id'=>'DESC'])->paginate(30,false,['query'=>$data]);
    }
}