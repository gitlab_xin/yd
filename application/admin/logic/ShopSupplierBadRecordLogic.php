<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 10:37
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\ShopSupplier as SupplierModel;
use app\common\model\ShopSupplierBadRecord as RecordModel;
use app\common\validate\ShopSupplierBadRecord as RecordValidate;

class ShopSupplierBadRecordLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:返回所有黑历史
     * @return false|static[]
     */
    public function getRecords()
    {
        return RecordModel::build()->field(['br_id','title','create_time','supplier_id'])->order('supplier_id')->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取单个黑历史
     * @param $id
     * @return null|static
     */
    public function getRecord($id)
    {
        return RecordModel::get($id);
    }
    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取商家
     * @return mixed
     */
    public function getSuppliers()
    {
        return objToArray(SupplierModel::build()->field(['supplier_id','name'])->select());
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:添加或修改黑历史
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditRecord($data,$id = 0)
    {
        $handle = $id == 0 ? '添加':'编辑';
        $res = ['method'=>'error','msg'=> $handle.'失败'];
        $validate = new RecordValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(RecordModel::saveRecord($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = $handle.'成功';
                $res['redirect'] = Url::build('/admin/shop_supplier_bad_record/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:删除黑历史
     * @param $id
     * @return array
     */
    public function delRecord($id)
    {
        return RecordModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除成功'];
    }
}