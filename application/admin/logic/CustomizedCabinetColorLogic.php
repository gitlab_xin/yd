<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:53
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\CustomizedCabinetColor as CabinetColorModel;
use app\common\model\CustomizedCabinetDoorColor as CabinetDoorColorModel;
use app\common\model\CustomizedColor as ColorModel;

class CustomizedCabinetColorLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:获取所有数据
     * @return array
     */
    public function getAll($cabinet_id)
    {
        return CabinetColorModel::build()->alias('cc')
                ->join('customized_color c', 'c.color_id = cc.color_id')
                ->where(['cc.cabinet_id' => $cabinet_id,'c.is_deleted' => '0'])
                ->select()
                ->toArray();
    }

    public function getAllColor()
    {
        return ColorModel::build()->field(['color_id', 'color_name', 'origin_img_src as img_src'])->where(['is_deleted' => 0])->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加或编辑
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id)
    {
        $res = ['method' => 'error', 'msg' => '编辑失败'];

        $model = CabinetColorModel::build();


        //获取选中的颜色
        $selected_color = isset($data['color_id']) ? $data['color_id'] : [];

        if (empty($selected_color)) {//如果没有选中颜色，则直接删除所有颜色
            $cabinet_color_ids = $model->where(['cabinet_id' => $id])->column('cabinet_color_id');
            if (!empty($cabinet_color_ids)) {
                CabinetDoorColorModel::build()->whereIn('cabinet_color_id', $cabinet_color_ids)->delete();
            }
            $model->where(['cabinet_id' => $id])->delete();
        } else {
            //获取原有的颜色
            $origin_color = $model->where(['cabinet_id' => $id])->column('color_id');
            $origin_diff_color = array_diff($origin_color, $selected_color);//以原有的颜色作为比较基础，删除差集

            if (!empty($origin_diff_color)) {
                $cabinet_color_ids = $model->where(['cabinet_id' => $id])->whereIn('color_id', $origin_diff_color)->column('cabinet_color_id');
                if (!empty($cabinet_color_ids)) {
                    CabinetDoorColorModel::build()->whereIn('cabinet_color_id', $cabinet_color_ids)->delete();
                }
                $model->where(['cabinet_id' => $id])->whereIn('color_id', $origin_diff_color)->delete();
            }
            $selected_diff_color = array_diff($selected_color, $origin_color);//以数据库颜色作为比较基础，添加差集

            if (!empty($selected_diff_color)) {
                $list = [];
                foreach ($selected_diff_color as $color_id) {
                    $list[] = ['color_id' => $color_id, 'cabinet_id' => $id];
                }
                $model->saveAll($list);
            }
        }


        $res['method'] = 'success';
        $res['msg'] = '编辑成功';
        $res['redirect'] = Url::build('/admin/customized_cabinet_color/index', ['id' => $id]);

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除帮助中心
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return CabinetColorModel::destroy($id)? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function quickSet($cabinet_id)
    {
        $all = CabinetColorModel::build()->where(['cabinet_id' => $cabinet_id])->column('color_id');
        $color = ColorModel::build()->where(['is_deleted' => 0])->column('color_id');
        $set = array_diff($color, array_intersect($all, $color));
        if (!empty($set)) {
            $data = [];
            foreach ($set as $color_id) {
                $data[] = ['cabinet_id' => $cabinet_id, 'color_id' => $color_id];
            }
            CabinetColorModel::build()->saveAll($data);
            return ['method' => 'success', 'msg' => '编辑成功', 'redirect' => Url::build('/admin/customized_cabinet_color/index', ['id' => $cabinet_id])];

        } else {
            return ['method' => 'error', 'msg' => '已拥有所有花色'];
        }
    }

    public function quickDel($cabinet_id)
    {
        $cabinet_color_model = CabinetColorModel::build();
        $all = $cabinet_color_model->where(['cabinet_id' => $cabinet_id])->column('cabinet_color_id');
        if (!empty($all)) {
            $cabinet_color_model->whereIn('cabinet_color_id', $all)->delete();
            CabinetDoorColorModel::build()->whereIn('cabinet_color_id', $all)->delete();
        }
        return ['method' => 'success', 'msg' => '编辑成功', 'redirect' => Url::build('/admin/customized_cabinet_color/index', ['id' => $cabinet_id])];
    }
}