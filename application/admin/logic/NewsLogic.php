<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/27
 * Time: 9:33
 */

namespace app\admin\logic;

use app\common\model\News as NewsModel;
use app\common\validate\News as NewsValidate;
use think\Url;

class NewsLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:获取全部资讯,分页形式,20个一页
     * @return \think\Paginator
     */
    public function getAllNews($data)
    {
        $where = [];
        $this->handleSearch($where,$data);
        return NewsModel::build()->field(['content','create_time'],true)->where($where)->paginate(20,false,['query'=>$data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:根据资讯id返回具体内容
     * @param $id
     * @return null|static
     */
    public function getNews($id)
    {
        return NewsModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:保存或者更新资讯
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditNews($data,$id = 0)
    {
        $handle = $id == 0? '添加':'编辑';
        $res = ['method'=>'error','msg'=>$handle.'失败'];

        $validate = new NewsValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(NewsModel::saveNews($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = $handle.'成功';
                $res['redirect'] = Url::build('/admin/news/index');
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:根据资讯id删除具体资讯
     * @param $id
     * @return array
     */
    public function delNews($id)
    {
        return NewsModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }
}