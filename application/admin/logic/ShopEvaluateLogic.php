<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 9:47
 */

namespace app\admin\logic;

use app\common\model\ShopEvaluate as EvaluateModel;
use app\common\model\ShopOrder as OrderModel;
use think\Db;

class ShopEvaluateLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:获取所有评价
     * @param $data
     * @return \think\Paginator
     */
    public function getAllEvaluate($data)
    {
        $where = [];
        $alias = 'e';
        if (count($data) > 0) {
            $this->handleSearch($where, $data, $alias . '.');
        }

        return EvaluateModel::build()->alias($alias)
            ->join('shop_product p', 'e.product_id = p.product_id', 'left')
            ->join('user u', 'e.user_id = u.user_id', 'left')
            ->field(['e.evaluate_id', 'e.star', 'e.content', 'e.praise_count', 'p.name', 'u.username'])
            ->where($where)
            ->order(['e.create_time' => 'DESC'])
            ->paginate(20, false, ['query' => $data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:获取单个评论
     * @param $id
     * @return null|static
     */
    public function getEvaluate($id)
    {
        return EvaluateModel::build()->alias('e')
            ->join('shop_product p', 'e.product_id = p.product_id', 'left')
            ->join('shop_product_standard s', 'e.standard_id = s.standard_id', 'left')
            ->join('shop_order o', 'e.order_id = o.order_id', 'left')
            ->join('user u', 'e.user_id = u.user_id', 'left')
            ->field(['e.*', 'p.name', 'u.username', 's.name as standard_name', 'o.order_num'])
            ->where(['e.evaluate_id' => $id])
            ->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:删除单个评论
     * @param $id
     * @return array
     */
    public function delEvaluate($id)
    {
        Db::startTrans();
        try {
            $evaluate = EvaluateModel::get($id);
            if (!OrderModel::update(['status' => '待评价'], ['order_id' => $evaluate->order_id])) {
                throw new \Exception('订单状态修改失败');
            }
            if (!$evaluate->delete()) {
                throw new \Exception('删除失败');
            }
            Db::commit();
            return ['method' => 'success', 'msg' => '删除成功'];
        } catch (\Exception $e) {
            Db::rollback();
            return ['method' => 'error', 'msg' => $e->getMessage()];
        }
//        return EvaluateModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    public function handleSearch(&$where = array(), $data, $alias = '', $time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias);
        //如果有username这个key
        if (isset($where[$alias . 'username'])) {
            $where['u.username'] = $where[$alias . 'username'];
            unset($where[$alias . 'username']);
        }
        //如果有product_name这个key
        if (isset($where[$alias . 'product_name'])) {
            $where['p.name'] = $where[$alias . 'product_name'];
            unset($where[$alias . 'product_name']);
        }
    }

}