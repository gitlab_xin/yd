<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/3
 * Time: 10:02
 */

namespace app\admin\logic;

use app\common\model\Tutorial as TutorialModel;
use app\common\validate\Tutorial as TutorailValidate;
use think\Url;

class TutorialLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:获取单个教程信息
     * @param $id
     * @return null|static
     */
    public function getTutorial($id)
    {
        return TutorialModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:
     * @return \think\Paginator
     */
    public function getTutorials()
    {
        return TutorialModel::build()->order(['create_time'=>'DESC'])->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:保存或更新教程
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEditTutorial($data,$id=0)
    {
        $handle = $id == 0?'添加':'编辑';
        $res = ['method'=>'error','msg'=>$handle.'失败'];

        $validate = new TutorailValidate();

        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            if(TutorialModel::saveTutorial($data,$id)){
                $res['method'] = 'success';
                $res['msg'] = $handle.'成功';
                $res['redirect'] = Url::build('/admin/tutorial/index');
            }
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:删除教程
     * @param $id
     * @return int
     */
    public function delTutorial($id)
    {
        return TutorialModel::destroy($id)?['method'=>'success','msg'=>'删除成功']:['method'=>'error','msg'=>'删除失败'];
    }
}