<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/18
 * Time: 11:53
 */

namespace app\admin\logic;

use think\Db;
use app\common\model\ShopProduct as ProductModel;
use app\common\model\ShopProductStandard as StandardModel;
use app\common\model\ShopSupplier as SupplierModel;
use app\common\model\ShopSupplierParams as ParamsModel;
use app\common\validate\ShopSupplier as SupplierValidate;
use think\Url;

class ShopSupplierLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:获取商家信息(全部)
     * @return mixed
     */
    public function getSuppliers()
    {
        return SupplierModel::build()->field(['supplier_id','name','intro','logo_src'])->where(['is_deleted'=>0])->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:获取商家信息(单个)
     * @param $id
     * @return mixed
     */
    public function getSupplier($id){
        return SupplierModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:添加商家
     * @param $data
     * @return array
     */
    public function addSupplier($data)
    {

        $res = ['method'=>'error','msg'=> '添加失败'];

        $validate = new SupplierValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            //若验证通过了
            //如果有资质图片就保存
            $data['qualification'] = isset($data['qualification']) && count($data['qualification']) > 0?implode('|',$data['qualification']):'';
            Db::startTrans();
            try{
                SupplierModel::create($data,true);
                ParamsModel::build()->saveAll($this->handleData($data,'title','desc','title','content',Db::getLastInsID()),true);
                Db::commit();
                $res['method'] ='success';
                $res['msg'] = '添加成功';
                $res['redirect'] = Url::build('/admin/shop_supplier/index');
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }


        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:编辑商家
     * @param $data
     * @return array
     */
    public function editSupplier($data)
    {
        $res = ['method'=>'error','msg'=> '编辑失败'];
        $id = $data['id'];
        $validate = new SupplierValidate();
        if(!$validate->scene('addOrEdit')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            $data['qualification'] = isset($data['qualification'])?implode('|',$data['qualification']):'';
            //售后服务
            $arr = $this->handleData($data,'title','desc','title','content',$id);
            //开启事务
            Db::startTrans();
            try{
                if(!SupplierModel::update($data,['supplier_id'=>$id],true)){
                    throw new \Exception('编辑失败');
                }
                if(count($arr)>0){
                    ParamsModel::build()->where(['supplier_id'=>$id])->delete();
                    ParamsModel::build()->saveAll($arr,false);
                }
                Db::commit();
                $res['method'] ='success';
                $res['msg'] = '编辑成功';
                $res['redirect'] = Url::build('/admin/shop_supplier/index');
            }catch (\Exception $e){
                $res['msg'] = $e->getMessage();
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:伪删除
     * @param $id
     * @return array
     */
    public function delSupplier($id)
    {
        Db::startTrans();
        try{
            $data = ['is_deleted'=>1];
            $where = ['supplier_id'=>$id];
            SupplierModel::update($data,$where);
            if(($ids = ProductModel::build()->where($where)->value('GROUP_CONCAT(`product_id`) as ids'))!==null){
                $where = ['product_id'=>['in',$ids]];
                ProductModel::update($data,$where);
                StandardModel::update($data,$where);
            }
            Db::commit();
            $res['method'] = 'success';
            $res['msg'] = '删除成功';
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $res['method'] = 'error';
            $res['msg'] = '删除失败';
        }
        return $res;
    }

    public function handleData($data,$postKey, $postVal, $dbKey, $dbVal,$supplier_id)
    {
        $tem = [];
        if(isset($data[$postKey]) && count($data[$postKey]>0) ){
            $count = count($data[$postKey]);
            for($i = 0;$i<$count;$i++){
                $tem[$i][$dbKey] = $data[$postKey][$i];
                $tem[$i][$dbVal] = $data[$postVal][$i];
                $tem[$i]['logo_src'] = $data['logo_src_list'][$i];
                $tem[$i]['supplier_id'] = $supplier_id;
            }
        }
        return $tem;
    }
}