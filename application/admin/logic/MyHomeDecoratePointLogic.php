<?php

namespace app\admin\logic;

use app\common\model\MyHomeDecoratePoint as DecorateModel;
use app\common\validate\MyHomeDecoratePoint as DecorateValidate;

/**
 * Class DecorateLogic
 * @package app\admin\logic
 */
class MyHomeDecoratePointLogic extends BaseLogic
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修亮点
     * @param null $id
     * @return null|static
     */
    public function getDecorate($id = null)
    {
        return $id != null > 0 ? DecorateModel::get($id) : null;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:
     * @return false|static[]
     */
    public function getAllDecorate()
    {
        $lists = DecorateModel::all();
        return $lists;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:添加或编辑分类
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEditDecorate($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new DecorateValidate();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (DecorateModel::saveDecorate($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:删除分类**该分类下无子分类,该分类下无商品**
     * @param $id
     * @return array
     */
    public function delDecorate($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = DecorateModel::get($id);
        if ($model != null) {
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (DecorateModel::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
