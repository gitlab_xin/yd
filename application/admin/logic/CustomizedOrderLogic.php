<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/16
 * Time: 9:57
 */

namespace app\admin\logic;

use app\api\logic\SchemeBZLogic;
use app\api\logic\SchemeBZOrderLogic;
use app\api\logic\SchemeQWLogic;
use app\api\logic\SchemeQWOrderLogic;
use app\api\logic\SchemeRQSLogic;
use app\api\logic\SchemeRQSOrderLogic;
use app\api\logic\SchemeZHGBGDGLogic;
use app\api\logic\SchemeZHGLogic;
use app\api\logic\SchemeZHGOrderLogic;
use app\api\logic\SchemeZHGSJSCLogic;
use app\common\model\ShopOrderRefund;
use app\common\tools\RedisUtil;
use think\Db;
use app\common\tools\wechatUtil;
use app\common\tools\RefundUtil;
use app\common\model\CustomizedOrder as OrderModel;
use app\common\model\CustomizedOrderArea as AreaModel;
use app\common\model\CustomizedOrderScheme as SchemeModel;
use app\common\model\ShopOrderLogistics as LogisticsModel;
use app\common\validate\CustomizedOrderLogistics as LogisticsValidate;
use app\common\model\ShopOrderOfficialLogistics as OfficialLogisticsModel;
use think\Session;

class CustomizedOrderLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:获取订单详情
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getOrderDetail($id)
    {
        return OrderModel::build()->alias('o')
            ->join('customized_order_charge oc','o.order_id = oc.order_id','left')
            ->join('user u','o.user_id = u.user_id','left')
            ->join('order_refund or', 'o.order_id = or.order_id', 'left')
            ->join('admin a','a.id = or.admin_id', 'left')
            ->field([
                'o.order_id','o.order_num','o.price','o.pay_type','o.status','o.count','o.pay_type','o.delivery_time',
                'o.user_remarks', 'oc.scheme_price','oc.discount','oc.scheme_dis_price','oc.freight_charge','oc.processing_charge',
                'oc.home_charge','oc.service_charge','u.username','or.content', 'or.reason','or.refuse_time','or.refuse_content',
                'a.username'
            ])
            ->where(['o.order_id'=>$id])
            ->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:获取订单列表
     * @param $data
     * @return \think\Paginator
     */
    public function getOrders($data)
    {
        $where = [];
        $alias = 'o';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }

        return OrderModel::build()->alias($alias)
            ->join('user u','o.user_id = u.user_id','left')
            ->join('customized_order_area c_o_a','c_o_a.order_id = o.order_id','left')
            ->field(['o.order_id','o.order_num','o.price','o.pay_type','o.status','o.count','o.pay_type','u.username','o.logistics_id'])
            ->where($where)
            ->order(['o.order_id'=>'DESC'])
            ->paginate(20,false,['query'=>$data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:获取送货地址
     * @param $order_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getAddress($order_id)
    {
        return AreaModel::build()->field(['province','city','county','town','detail','username','phone'])
            ->where(['order_id'=>$order_id])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:退货
     * @param $id
     * @return array
     */
    public function refund($data)
    {
        $id = $data['order_id'];
        $res = ['method'=>'error','msg'=>'退款失败'];
        if(($order = OrderModel::get($id)) == null){//若该订单不存在
            $res['msg'].=',该订单不存在';
        }elseif($order['status'] != ('待发货'||'退款中') ){///非待发货下不准退款
            $res['msg'].=',该订单状态处于['.$order['status'].']下无法退款';
        }else{
            /*
             * 开启事务,先进行本地服务器数据库操作,再对外进行curl操作
             * 记得按顺序来,否则可能会导致退款成功,但是由于数据库操作异常而导致退款后数据错误
             */
            Db::startTrans();
            try{
                $ramdomNum = wechatUtil::getRandomString(32);//获取随机的号码

                $wholeOrder = OrderModel::build()->field(['sum(price) as sum','count(*) as count'])->where(['bill_no'=>$order->bill_no])->find()->toArray();
//                正式用
//                $sum = $wholeOrder['count'] == 1?$data['refund_number']:$data['refund_number']*$wholeOrder['sum'];
//                $fee = $data['refund_number'];

                //测试用
                $sum = $wholeOrder['count'] == 1?0.01:0.01*$wholeOrder['count'];//支付宝没用到
                $fee = 0.01;

                $order->refund_no = $ramdomNum;
                $order->is_refund = 'yes';
                $order->status = '已退款';
                if(!$order->save()){
                    throw new \Exception('订单状态修改失败');
                }

                if(!RefundUtil::getInstance()->setProps(['payType'=>$order['pay_type'],'sum'=>$sum,'fee'=>$fee,'billNum'=>$order['bill_no']])->exec()){
                    throw new \Exception('退款失败');
                }

                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '退款成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }
        return $res;
    }

    public function refuse($data){

        $id = $data['order_id'];
        $res = ['method'=>'error','msg'=>'拒绝退款失败'];
        if(($order = OrderModel::get($id)) == null){//若该订单不存在
            $res['msg'].=',该订单不存在';
        }elseif($order['status'] != ('退款中') ){///退款中下不准退款
            $res['msg'].=',该订单状态处于['.$order['status'].']下无法退款';
        } else {

            Db::startTrans();
            try{

                $order->status = '已拒绝';
                if(!$order->save()){
                    throw new \Exception('订单状态修改失败');
                }

                $order_refund = ShopOrderRefund::where(['order_id' => $id])->find();

                $order_refund->admin_id = Session::get('user_id');
                $order_refund->refuse_time = time();
                $order_refund->refuse_content = $data['refuse_content'];

                if(!$order_refund->save()){
                    throw new \Exception('订单状态修改失败');
                }

                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '拒绝退款成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }

        return $res;

    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:发货
     * @param $data
     * @return array
     */
    public function delivery($data)
    {
        $res = ['method'=>'error','msg'=>'发货失败'];

        $validate = new LogisticsValidate();
        if(!$validate->scene('add')->check($data)){
            $res['msg'] = $validate->getError();
        }else{
            $data['order_type'] = 'customized';
            Db::startTrans();
            try{
                $order = OrderModel::get($data['order_id']);
                $data['user_id'] = $order['user_id'];
                //插入物流表
                if(!($model = LogisticsModel::create($data,true))){
                    throw new \Exception('物流状态添加失败');
                }
                //插入官方物流详情表
                if($data['logistics_type'] == 'official'){
                    $data['logistics_id'] = $model['logistics_id'];
                    $data['content'] = '你的包裹正在揽收中';
                    if(!OfficialLogisticsModel::create($data,true)){
                        throw new \Exception('物流状态添加失败');
                    }
                }
                //更改订单状态
                if(!$order->isUpdate()->save(['status'=>'待收货','logistics_id'=>$model['logistics_id']],['order_id'=>$data['order_id']])){
                    throw new \Exception('订单状态更新失败');
                }

                Db::commit();
                $res['method'] = 'success';
                $res['msg'] = '发货成功';
            }catch (\Exception $e){
                Db::rollback();
                $res['msg'] = $e->getMessage();
            }
        }
        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2019/12/23
     * Time: 10:45 上午
     * description 生成方案详情
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getScheme($id)
    {
        $info = SchemeModel::build()->where(['order_id'=>$id])->find();
        $method = 'get'.$info['scheme_type'].'info';
        if ($info['scheme_type'] == 'ZH') {
            $method = 'get'.$info['scheme_type'].$info['scheme_b_type'].'info';
        }
        $scheme_info = self::$method($info['serial_array']);
        $info['serial_array'] = $scheme_info;
        return  $info;
    }

    /**
     * User: zhaoxin
     * Date: 2019/12/23
     * Time: 10:46 上午
     * description 墙外
     * @param $serial_array
     * @return mixed
     */
    public static function getQWinfo($serial_array){
        $schemeData = json_decode($serial_array, true);

        $schemeLogic = new SchemeQWLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeQWOrderLogic();
        $schemeBean = $orderLogic->scoreQWSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreQWSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }
        $doorProducts = [];
        foreach ($schemeBean->getScheme_score_door_products() as $item) {
            $item = change2ProductBean($item);
            $doorProducts[] = $item->scoreInfo();
        }

        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['scheme_name'] = $schemeBean->getScheme_name();
        $scheme_info['zj_products'] = $zjProducts;
        $scheme_info['score_products'] = $scoreProducts;
        $scheme_info['plate_products'] = $plateProducts;
        $scheme_info['wujin_products'] = $wujinProducts;
        $scheme_info['door_products'] = $doorProducts;
        $scheme_info['door_price'] = formatMoney($schemeBean->getScheme_door_price(), 2);
        $scheme_info['door_area'] = $schemeBean->getScheme_door_area();
        $scheme_info['door'] = $doorInfo;
        $scheme_info['s_door_area'] = $schemeBean->getScheme_s_door_area();
        $scheme_info['gt_price'] = formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['dis_gt_price'] = formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['dis_door_price'] = formatMoney($schemeBean->getScheme_dis_door_price(), 2);
        $scheme_info['zj_price'] = formatMoney($schemeBean->getScheme_zj_price(), 2);
        $scheme_info['dis_zj_price'] = formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] = formatMoney($schemeBean->getScheme_wujin_price(), 2);
        $scheme_info['dis_wujin_price'] = formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
        $scheme_info['plate_price'] = formatMoney($schemeBean->getScheme_plate_price(), 2);
        $scheme_info['dis_plate_price'] = formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
        $scheme_info['gt_price'] = formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['gt_dis_price'] = formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['scheme_price'] = formatMoney($schemeBean->getScheme_price(), 2);
        $scheme_info['scheme_dis_price'] = formatMoney($schemeBean->getScheme_dis_price(), 2);
        $scheme_info['discount'] = formatMoney($schemeBean->getScheme_discount(), 2);

        return $scheme_info;
    }

    public static function getBZinfo($serial_array){
        $schemeData = json_decode($serial_array, true);
        $schemeLogic = new SchemeBZLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeBZOrderLogic();


        //统计标准衣柜功能件
        $orderLogic->scoreBZYGSchemeProducts($schemeBean);
        //统计标准衣柜组合件
        $orderLogic->scoreBZYGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $orderLogic->scoreBZYGSchemeplates($schemeBean);
        //统计综合柜五金件
        $orderLogic->scoreBZYGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['zj_products'] = $zjProducts;
        $scheme_info['score_products'] = $scoreProducts;
        $scheme_info['plate_products'] = $plateProducts;
        $scheme_info['wujin_products'] = $wujinProducts;
        $scheme_info['gt_price'] = formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['dis_gt_price'] = formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['zj_price'] = formatMoney($schemeBean->getScheme_zj_price(), 2);
        $scheme_info['dis_zj_price'] = formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] = formatMoney($schemeBean->getScheme_wujin_price(), 2);
        $scheme_info['dis_wujin_price'] = formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
        $scheme_info['plate_price'] = formatMoney($schemeBean->getScheme_plate_price(), 2);
        $scheme_info['dis_plate_price'] = formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
        $scheme_info['scheme_price'] = formatMoney($schemeBean->getScheme_price(), 2);
        $scheme_info['scheme_dis_price'] = formatMoney($schemeBean->getScheme_dis_price(), 2);
        $scheme_info['discount'] = formatMoney($schemeBean->getScheme_discount(), 2);

        return $scheme_info;
    }

    public static function getRQSinfo($serial_array){
        $schemeData = json_decode($serial_array, true);

        $schemeLogic = new SchemeRQSLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeRQSOrderLogic();
        $schemeBean = $orderLogic->scoreRQSSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreRQSSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }

        $schemeBean = $orderLogic->scoreRQSSchemeShouKou($schemeBean);


        //柜体构件明细 -> 构件清单
        $panelProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $panelProducts[] = $item->scoreInfo();
        }
        //柜体构件明细 -> 用料明细
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }

        //功能组件明细
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }

        //五金附件明细
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        //滑动门明细 -> 材料清单
        $doorProducts = [];
        if (is_array($schemeBean->getScheme_score_door_products())) {
            foreach ($schemeBean->getScheme_score_door_products() as $item) {
                $item = change2ProductBean($item);
                $doorProducts[] = $item->scoreInfo();
            }
        }
        //滑动门明细 -> 用料明细
        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        //收口明细
        $shouKouProducts = [];
        if (is_array($schemeBean->getScheme_score_sk_products())) {
            foreach ($schemeBean->getScheme_score_sk_products() as $item) {
                $item = change2ProductBean($item);
                $shouKouProducts[] = $item->scoreInfo();
            }
        }

        $scheme_info['scheme_name'] = $schemeBean->getScheme_name();
        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['zj_products'] = $zjProducts;
        $scheme_info['score_products'] = $panelProducts;
        $scheme_info['plate_products'] = $plateProducts;
        $scheme_info['wujin_products'] = $wujinProducts;
        $scheme_info['door_products'] = $doorProducts;
        $scheme_info['shoukou_products'] = $shouKouProducts;
        $scheme_info['door'] = $doorInfo;
        $scheme_info['door_price'] = formatMoney($schemeBean->getScheme_door_price(), 2);
        $scheme_info['dis_door_price'] = formatMoney($schemeBean->getScheme_dis_door_price(), 2);
        $scheme_info['door_mat_price'] = formatMoney($schemeBean->getScheme_door_mat_price(), 2);
        $scheme_info['dis_door_mat_price'] = formatMoney($schemeBean->getScheme_dis_door_mat_price(),2);
        $scheme_info['zj_price'] = formatMoney($schemeBean->getScheme_zj_price() ,2);
        $scheme_info['dis_zj_price'] = formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] = formatMoney($schemeBean->getScheme_wujin_price() ,2);
        $scheme_info['dis_wujin_price'] = formatMoney($schemeBean->getScheme_dis_wujin_price() ,2);
        $scheme_info['shoukou_price'] = formatMoney($schemeBean->getScheme_sk_price() ,2);
        $scheme_info['dis_shoukou_price'] = formatMoney($schemeBean->getScheme_dis_sk_price() ,2);
        $scheme_info['plate_price'] = formatMoney($schemeBean->getScheme_plate_price() ,2);
        $scheme_info['plate_dis_price'] = formatMoney($schemeBean->getScheme_dis_plate_price() ,2);
        $scheme_info['gt_price'] = formatMoney($schemeBean->getScheme_gt_price() ,2);
        $scheme_info['gt_dis_price'] = formatMoney($schemeBean->getScheme_dis_gt_price() ,2);
        $scheme_info['scheme_price'] = formatMoney($schemeBean->getScheme_price() ,2);
        $scheme_info['scheme_dis_price'] = formatMoney($schemeBean->getScheme_dis_price() ,2);
        $scheme_info['discount'] = formatMoney($schemeBean->getScheme_discount() ,2);

        return $scheme_info;
    }

    public static function getZHCWSNinfo($serial_array){
        $schemeData = json_decode($serial_array, true);
        $schemeLogic = new SchemeZHGLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['zj_products'] =$zjProducts;
        $scheme_info['score_products'] =$scoreProducts;
        $scheme_info['plate_products'] =$plateProducts;
        $scheme_info['wujin_products'] =$wujinProducts;
        $scheme_info['gt_price'] =formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['dis_gt_price'] =formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['zj_price'] =formatMoney($schemeBean->getScheme_zj_price(), 2);
        $scheme_info['dis_zj_price'] =formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] =formatMoney($schemeBean->getScheme_wujin_price(), 2);
        $scheme_info['dis_wujin_price'] =formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
        $scheme_info['plate_price'] =formatMoney($schemeBean->getScheme_plate_price(), 2);
        $scheme_info['dis_plate_price'] =formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
        $scheme_info['gt_price'] =formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['gt_dis_price'] =formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['scheme_dis_price'] =formatMoney($schemeBean->getScheme_dis_price(), 2);
        $scheme_info['scheme_price'] =formatMoney($schemeBean->getScheme_price(), 2);
        $scheme_info['discount'] =formatMoney($schemeBean->getScheme_discount(), 2);

        return $scheme_info;
    }

    public static function getZHSJSCinfo($serial_array){
        $schemeData = json_decode($serial_array, true);
        $schemeLogic = new SchemeZHGSJSCLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['zj_products'] =$zjProducts;
        $scheme_info['score_products'] =$scoreProducts;
        $scheme_info['plate_products'] =$plateProducts;
        $scheme_info['wujin_products'] =$wujinProducts;
        $scheme_info['gt_price'] =formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['dis_gt_price'] =formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['zj_price'] =formatMoney($schemeBean->getScheme_zj_price(), 2);
        $scheme_info['dis_zj_price'] =formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] =formatMoney($schemeBean->getScheme_wujin_price(), 2);
        $scheme_info['dis_wujin_price'] =formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
        $scheme_info['plate_price'] =formatMoney($schemeBean->getScheme_plate_price(), 2);
        $scheme_info['dis_plate_price'] =formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
        $scheme_info['discount'] =formatMoney($schemeBean->getScheme_discount(), 2);
        $scheme_info['scheme_price'] =formatMoney($schemeBean->getScheme_price(), 2);
        $scheme_info['scheme_dis_price'] =formatMoney($schemeBean->getScheme_dis_price(), 2);

        return $scheme_info;
    }

    public static function getZHYYSTinfo($serial_array){
        $schemeData = json_decode($serial_array, true);
        $schemeLogic = new SchemeZHGLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $scheme_info['scheme_pic'] = $schemeBean->getScheme_pic();
        $scheme_info['zj_products'] =$zjProducts;
        $scheme_info['score_products'] =$scoreProducts;
        $scheme_info['plate_products'] =$plateProducts;
        $scheme_info['wujin_products'] =$wujinProducts;
        $scheme_info['gt_price'] =formatMoney($schemeBean->getScheme_gt_price(), 2);
        $scheme_info['dis_gt_price'] =formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
        $scheme_info['zj_price'] =formatMoney($schemeBean->getScheme_zj_price(), 2);
        $scheme_info['dis_zj_price'] =formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
        $scheme_info['wujin_price'] =formatMoney($schemeBean->getScheme_wujin_price(), 2);
        $scheme_info['dis_wujin_price'] =formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
        $scheme_info['plate_price'] =formatMoney($schemeBean->getScheme_plate_price(), 2);
        $scheme_info['dis_plate_price'] =formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
        $scheme_info['scheme_dis_price'] =formatMoney($schemeBean->getScheme_dis_price(), 2);
        $scheme_info['scheme_price'] =formatMoney($schemeBean->getScheme_price(), 2);
        $scheme_info['discount'] =formatMoney($schemeBean->getScheme_discount(), 2);

        return $scheme_info;
    }

    public static function getZHBGDGinfo($serial_array){
        $schemeData = json_decode($serial_array, true);
        $schemeLogic = new SchemeZHGBGDGLogic();

        $data = $schemeLogic->score($schemeData);//结算
        $zjProducts = $data['zj_products'];
        $plateProducts = $data['plate_products'];
        $wujinProducts = $data['wujin_products'];
        $scoreProducts = $data['score_products'];

        $scheme_info['zj_products'] = $zjProducts;
        $scheme_info['score_products'] = $scoreProducts;
        $scheme_info['plate_products'] = $plateProducts;
        $scheme_info['wujin_products'] = $wujinProducts;
        $scheme_info['gt_price'] = formatMoney($data['gt_price']);
        $scheme_info['dis_gt_price'] = formatMoney($data['dis_gt_price']);
        $scheme_info['zj_price'] = formatMoney($data['zj_price']);
        $scheme_info['dis_zj_price'] = formatMoney($data['dis_zj_price']);
        $scheme_info['wujin_price'] = formatMoney($data['wujin_price']);
        $scheme_info['dis_wujin_price'] = formatMoney($data['dis_wujin_price']);
        $scheme_info['plate_price'] = formatMoney($data['plate_price']);
        $scheme_info['dis_plate_price'] = formatMoney($data['dis_plate_price']);
        $scheme_info['scheme_price'] = formatMoney($data['gt_price']);
        $scheme_info['scheme_dis_price'] = formatMoney($data['dis_gt_price']);
        $scheme_info['discount'] = 0.68;

        return $scheme_info;
    }


    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:搜索规则
     * @param array $where
     * @param $data
     * @param string $alias
     * @param string $time
     */
    public function handleSearch(&$where = array(), $data, $alias = '',$time = 'create_time')
    {
        parent::handleSearch($where, $data, $alias);
        //如果有username这个key
        if(isset($where[$alias.'username'])){
            $where['u.username'] = $where[$alias.'username'];
            unset($where[$alias.'username']);
        }
        if(isset($where[$alias.'phone'])){
            $where['c_o_a.phone'] = $where[$alias.'phone'];
            unset($where[$alias.'phone']);
        }
        if(isset($where[$alias.'mobile'])){
            $where['u.mobile'] = $where[$alias.'mobile'];
            unset($where[$alias.'mobile']);
        }
    }
}