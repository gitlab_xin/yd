<?php

namespace app\admin\logic;

use app\common\model\MyAssemblyHouseType as HouseTypeModel;
use app\common\validate\MyHomeHouseType as HouseTypeValidate;

class MyAssemblyHouseTypeLogic extends BaseLogic
{

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:16 上午
     * description
     * @param null $id
     * @return HouseTypeModel|null
     * @throws \think\exception\DbException
     */
    public function getHouse($id = null)
    {
        return $id != null > 0 ? HouseTypeModel::get($id) : null;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:16 上午
     * description
     * @param $data
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getAllHouse($data)
    {
        $where = ['t.is_deleted'=>0];
        $alias = 't';
        if(count($data)>0){
            $this->handleSearch($where,$data,$alias.'.');
        }

        $lists = HouseTypeModel::build()
            ->alias('t')
            ->where($where)
            ->paginate(20,false,['query'=>$data]);
        return $lists;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:16 上午
     * description
     * @param $postData
     * @param int $id
     * @return array
     */
    public function addOrEditHouse($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];
        $validate = new HouseTypeValidate();
        if (!$validate->scene('insertOrEdit2')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (HouseTypeModel::saveHouse($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:16 上午
     * description
     * @param $id
     * @return array
     * @throws \think\exception\DbException
     */
    public function delHouse($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = HouseTypeModel::get($id);
        if ($model != null) {
            //3.如果图片,记得删除分类图片
            if ($model->img_src != null) {
                $this->delCloudFile($model->img_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (HouseTypeModel::build()->update(['is_deleted'=>1],['house_id'=>$id])) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:16 上午
     * description
     * @param $house_id
     * @return mixed
     */
    public function getDecorate($house_id)
    {
        return MyHomeDecoratePoint::build()->alias('d')
            ->join('yd_my_home_house_decorate hd', 'hd.decorate_id = d.decorate_id')
            ->join('yd_my_home_house_type h', 'h.house_id = hd.house_id')
            ->where(['hd.house_id' => $house_id])
            ->field(['d.decorate_id', 'd.name', 'd.sub_title'])
            ->select();
    }
}
