<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 17:37
 */

namespace app\admin\logic;

use think\Db;
use app\common\model\Works as WorksModel;
use app\common\model\WorksComment as CommentModel;
use app\common\validate\Works as WorksValidate;

class WorksLogic extends BaseLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取单个案例
     * @param $id
     * @return null|static
     */
    public function getWorks($id)
    {
        return WorksModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取所有案例
     * @param $data
     * @return \think\Paginator
     */
    public function getAllWorks($data)
    {
        $where = [];
        if(count($data)){
            $this->handleSearch($where,$data,'w.');
        }
        return WorksModel::build()->alias('w')
            ->join('user u','w.user_id = u.user_id','left')
            ->field(['w.works_id','w.type','u.username','w.audit_remarks','w.title','w.is_recommended'])
            ->where($where)
            ->order(['w.create_time'=>'DESC'])
            ->paginate(20,false,['query'=>$data]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:更改审核状态
     * @param $data
     * @param $id
     * @param $type
     * @return array
     */
    public function changeType($data,$id,$type)
    {
        $res = ['method'=>'error','msg'=>'操作失败'];
        $data['type'] = $type;
        $validate = new WorksValidate();

        if(!$validate->scene('changeType')->check($data)){
            $res['msg'] = $validate->getError();
        }elseif(WorksModel::update($data,['works_id'=>$id],true)){
            $res['method'] = 'success';
            $res['msg'] = '操作成功';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取所有回复
     * @param $data
     * @return false|\PDOStatement|string|\think\Collection
     *
     */
    public function getReplys($data)
    {
        $where = [];
        $this->handleSearch($where,$data);
        return CommentModel::build()->field(['create_time'],true)->where($where)->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:删除回复
     * @param $id
     * @return array
     */
    public function delReply($id)
    {
        Db::startTrans();

        try{
            //如果该回复是一级回复则把一级回复下所有的回复也删除
            $reply = CommentModel::build()->get($id);
            if($reply->parent_comment_id == 0){
                //此处判断说明属于一级回复,则需要删除下面其余的回复
                CommentModel::build()->where(['parent_comment_id'=>$id])->delete();
            }
            $works_id = $reply->works_id;
            $reply->delete();

            //删除完回复后要重新进行回复数量统计
            $count = CommentModel::whereCount(['works_id'=>$works_id]);
            WorksModel::update(['comment_count'=>$count],['works_id'=>$works_id],true);

            Db::commit();
            $res = ['method'=>'success','msg'=>'删除成功'];
        }catch (\Exception $e){
            Db::rollback();
            $res = ['method'=>'error','msg'=>'删除失败'];
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:设置字段状态,如推荐   ajax
     * @param $id
     * @param $field
     * @param $success
     * @return array
     */
    public function changeStatus($id,$field,$success)
    {
        $res = ['code'=>0,'msg'=>'未知错误'];

        if($field == null || $success == null){
            $res['msg'] = '参数缺失';
        }elseif($id == null){
            $res['msg'] = '不存在该id的帖子';
        }elseif($model = WorksModel::get($id)){
            $status = $model->$field;
            $handle = $status == 1 ? '取消':$success;
            $model->$field = $status==1 ? 0:1;
            if(!$model->save()){
                $res['msg'] = $handle.'失败';
            }else{
                $res['code'] = 1;
                $res['msg'] = $handle.'成功';
            }
        }else{
            $res['msg'] = '该帖子不存在';
        }
        return $res;
    }

}