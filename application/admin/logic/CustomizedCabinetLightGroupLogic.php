<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:53
 */

namespace app\admin\logic;


use think\Url;
use app\common\model\CustomizedCabinetLight as LightModel;
use app\common\model\CustomizedCabinetLightGroup as LightGroupModel;
use app\common\model\CustomizedCabinetLightGroupMap as LightGroupMapModel;
use app\common\model\CustomizedCabinetSceneLightGroup as SceneLightGroupModel;
use app\common\validate\CustomizedCabinetLightGroup as LightGroupValidate;

class CustomizedCabinetLightGroupLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll($page = true)
    {
        return $page ? LightGroupModel::build()->paginate(20) : LightGroupModel::build()->select()->toArray();
    }

    public function getList()
    {
        return LightModel::build()->field(['light_id', 'name', 'type'])->select()->toArray();
    }

    public function getSeleted($id)
    {
        return LightGroupMapModel::build()->where(['light_group_id' => $id])->column('light_id');
    }

    public function getSceneGroup($scene_id)
    {
        return SceneLightGroupModel::build()->alias('slg')->field(['slg.type', 'clg.name', 'clg.light_group_id', 'slg.is_default', 'slg.scene_light_group_id'])
            ->join('customized_cabinet_light_group clg', 'slg.light_group_id=clg.light_group_id')
            ->where(['slg.scene_id' => $scene_id])->select();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return LightGroupModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @return array
     */
    public function add($data)
    {
        $res = ['method' => 'error', 'msg' => '添加失败'];

        $validate = new LightGroupValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (LightGroupModel::saveGroup($data)) {
            $res['method'] = 'success';
            $res['msg'] = '添加成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_light_group/index');
        }
        return $res;
    }

    public function edit($data)
    {
        $res = ['method' => 'error', 'msg' => '编辑失败'];

//        if (LightGroupModel::updateLight($data)) {
//            $res['method'] = 'success';
//            $res['msg'] = '编辑成功';
//            $res['redirect'] = Url::build('/admin/customized_cabinet_light/index');
//        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        LightGroupModel::destroy($id);
        SceneLightGroupModel::destroy(['light_group_id' => $id]);
        return ['method' => 'success', 'msg' => '删除成功'];
    }

    public function delScene($scene_light_group_id)
    {
        return SceneLightGroupModel::destroy($scene_light_group_id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function addLight($data)
    {
        $model = new LightGroupMapModel();
        $light_group_id = $data['light_group_id'];
        $model->where(['light_group_id' => $data['light_group_id']])->delete();
        if (isset($data['light_id'])) {
            $model->saveAll(array_map(function ($v) use ($light_group_id) {
                return ['light_id' => $v, 'light_group_id' => $light_group_id];
            }, $data['light_id']));
        }
        return ['method' => 'success', 'msg' => '修改成功'];
    }

    public function addLightGroup($data)
    {
        $res['redirect'] = Url::build('/admin/customized_cabinet_scene_light_group/index', ['scene_id' => $data['scene_id']]);
        if (SceneLightGroupModel::create($data)) {
            $res['method'] = 'success';
            $res['msg'] = '添加成功';
        } else {
            $res['method'] = 'error';
            $res['msg'] = '添加失败';
        }
        return $res;
    }

    public function setDefault($data)
    {
        $model = new SceneLightGroupModel();
        $model->isUpdate(true, ['scene_id' => $data['scene_id']])->save(['is_default' => 0]);
        $model->isUpdate(true, $data)->save(['is_default' => 1]);
        return ['method' => 'success', 'msg' => '修改成功'];
    }
}