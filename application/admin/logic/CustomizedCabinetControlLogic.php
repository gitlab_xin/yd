<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:53
 */

namespace app\admin\logic;


use think\Url;
use app\common\model\CustomizedCabinetControl as ControlModel;
use app\common\model\CustomizedCabinetSceneControl as SceneControlModel;
use app\common\validate\CustomizedCabinetControl as ControlValidate;

class CustomizedCabinetControlLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取全部
     * @return \think\Paginator
     */
    public function getAll()
    {
        return ControlModel::build()->paginate(20);
    }

    public function getList()
    {
        return ControlModel::build()->field(['control_id', 'name', 'type', 'camera', 'render'])->select()->toArray();
    }

    public function getSeleted($id)
    {
        return SceneControlModel::build()->where(['scene_id' => $id])->column('control_id');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取单个详情
     * @param $id
     * @return null|static
     */
    public function getDetail($id)
    {
        return ControlModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new ControlValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (ControlModel::saveControl($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/customized_cabinet_control/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return ControlModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    public function addControl($data)
    {
        $model = new SceneControlModel();
        $scene_id = $data['scene_id'];
        $model->where(['scene_id' => $data['scene_id']])->delete();
        if (isset($data['control_id'])) {
            $model->saveAll(array_map(function ($v) use ($scene_id) {
                return ['control_id' => $v, 'scene_id' => $scene_id];
            }, $data['control_id']));
        }
        return ['method' => 'success', 'msg' => '修改成功'];
    }
}