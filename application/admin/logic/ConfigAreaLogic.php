<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/24
 * Time: 18:04
 */

namespace app\admin\logic;

use app\common\model\ConfigArea as AreaModel;
use think\Db;
use think\Url;

class ConfigAreaLogic
{

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:获取所有非配送城市,由于表数据量过大,所以不使用join而分两次sql执行
     * @return mixed
     */
    public function getAreas()
    {
        $prov = objToArray(AreaModel::build()
            ->field(['id','name'])
            ->where(['level'=>1])
            ->select());
        $prov = array_column($prov,'name','id');

        $citys = objToArray(AreaModel::build()
            ->field(['GROUP_CONCAT(name) AS citys','parent_id'])
            ->where(['is_distribution' => '0','level'=>2])
            ->group('parent_id')
            ->select());
        $citys = array_column($citys,'citys','parent_id');

        $res['prov'] = $prov;
        $res['citys'] = $citys;

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:快速设置省份下所有城市为非配送
     * @param $id
     * @return array
     */
    public function quickSet($id,$is_distribution)
    {
        return AreaModel::update(['is_distribution'=>$is_distribution],['parent_id'=>$id])?['method'=>'success','msg'=>'快速设置成功']:['method'=>'success','快速设置失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:返回所有下级区域
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getChildArea($id)
    {
        return objToArray(AreaModel::build()->where(['parent_id'=>$id])->select());
    }

    public function getCity($name)
    {
        $parent_id = AreaModel::build()->where(['name' => $name])->value('id');
        return AreaModel::build()->field(['id', 'code', 'name', 'level'])->where(['parent_id' => $parent_id])->select()->toArray();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:设置非配送
     * @param $data
     * @param $parent_id
     * @return array
     */
    public function setDistribution($data,$parent_id)
    {
        $res = ['method'=>'error','msg'=>'设置失败'];

        Db::startTrans();
        try{
            AreaModel::update(['is_distribution'=>'1'],['parent_id'=>$parent_id]);
            if(isset($data['city'])){
                $ids = implode(',',$data['city']);
                AreaModel::update(['is_distribution'=>'0'],['id'=>['in',$ids]]);
            }
            Db::commit();
            $res['method'] = 'success';
            $res['msg'] = '设置成功';
            $res['redirect'] = Url::build('/admin/config_area/index');
        }catch (\Exception $e){
            Db::rollback();
            $res['msg'] = $e->getMessage();
        }

        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:导入文件保存地区信息
     * @param $area 地区sql文件 json_decode转数组
     * @param int $parent_id
     * @param int $level
     * @param string $precode
     *
     */
    private function insertAreas($area,$parent_id = 0,$level = 1,$precode = '00'){
        foreach ($area as $index => $p){
            $index = ++$index;
            if($index<10){
                $index = '0'.$index;
            }
            $code = $precode == '00'?$index:$precode.$index;
            $data = ['code'=>$code,'parent_id'=>$parent_id,'name' =>$p['text'],'level'=>$level,'is_distribution'=>1];
            AreaModel::create($data,true);
            $id = Db::getLastInsID();
            if(isset($p['children'])){
                $level = ++$level;
                $this->insert($p['children'],$id,$level,$code);
                $level = --$level;
            }
        }
    }
}