<?php

namespace app\admin\logic;

use app\common\model\MyHomeFurnitureClassify as ClassifyModel;
use app\common\validate\MyHomeFurnitureClassify as ClassifyValidate;

/**
 * Class ClassifyLogic
 * @package app\admin\logic
 */
class MyHomeClassifyLogic extends BaseLogic
{

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:获取分类
     * @param null $id
     * @return null|static
     */
    public function getClassify($id = null)
    {
        return $id != null > 0 ? ClassifyModel::get($id) : null;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:
     * @return array|null
     */
    public function getAllClassify()
    {
        $lists = ClassifyModel::all();
        return $lists;
    }

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:获取下拉单分类
     * @return array
     */
    public function getSelectClassify()
    {

        return ClassifyModel::getSelectClassify();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:添加或编辑分类
     * @param $postData
     * @param $id
     * @return array
     */
    public function addOrEditClassify($postData, $id = 0)
    {
        $handle = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handle . '失败'];

        $validate = new ClassifyValidate();
        if (!$validate->scene('addOrEdit')->check($postData)) {
            //加入验证器没通过,则返回错误信息
            $res['msg'] = $validate->getError();
        } elseif (ClassifyModel::saveClassify($postData, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handle . '成功';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:删除分类**该分类下无子分类,该分类下无商品**
     * @param $id
     * @return array
     */
    public function delClassify($id)
    {
        $res = ['method' => 'error', 'msg' => '不存在该分类'];

        $model = ClassifyModel::get($id);
        if ($model != null) {

            //2.检查分类下面是否存在产品,如果有的话,提示不能删除
            if (ClassifyModel::checkFurnitureExist($id)) {
                $res['msg'] = '请先删除分类下的家具';
                return $res;
            }
            //3.如果图片,记得删除分类图片
            if ($model->logo_src != null) {
                $this->delCloudFile($model->logo_src);
                unset($model);
            }
            //4.如果分类下没有子分类和子产品,则进行分类删除
            if (ClassifyModel::destroy($id)) {
                $res['method'] = 'success';
                $res['msg'] = '删除成功';
            } else {
                $res['msg'] = '删除失败';
            }
        }

        return $res;
    }
}
