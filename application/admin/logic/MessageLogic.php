<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 10:58
 */

namespace app\admin\logic;

use think\Url;
use app\common\model\MessageOfficial as MessageModel;
use app\common\validate\MessageOfficial as MessageValidate;
use app\common\model\Activity as ActivityModel;

class MessageLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取所有消息
     * @return \think\Paginator
     */
    public function getMessages()
    {
        return MessageModel::build()->alias('m')->field(['m.*', 'a.title as activity_title'])
            ->join('activity a', 'm.activity_id=a.activity_id', 'left')->order(['m.id' => 'DESC'])->paginate(20);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取单个消息
     * @param $id
     * @return null|static
     *
     */
    public function getMessage($id)
    {
        return MessageModel::get($id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:添加或修改
     * @param $data
     * @param int $id
     * @return array
     */
    public function addOrEdit($data, $id = 0)
    {
        $handler = $id == 0 ? '添加' : '编辑';
        $res = ['method' => 'error', 'msg' => $handler . '失败'];

        $validate = new MessageValidate();
        if (!$validate->scene('addOrEdit')->check($data)) {
            $res['msg'] = $validate->getError();
        } elseif (MessageModel::saveMessage($data, $id)) {
            $res['method'] = 'success';
            $res['msg'] = $handler . '成功';
            $res['redirect'] = Url::build('/admin/message/index');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:删除信息
     * @param $id
     * @return array
     */
    public function del($id)
    {
        return MessageModel::destroy($id) ? ['method' => 'success', 'msg' => '删除成功'] : ['method' => 'error', 'msg' => '删除失败'];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取所有活动
     * @return mixed
     */
    public function getActivities()
    {
        return objToArray(ActivityModel::build()->field(['activity_id', 'title'])->where(['is_deleted' => 0])->select());
    }
}