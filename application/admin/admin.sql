# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.29-log)
# Database: tp5admin
# Generation Time: 2017-05-17 09:21:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yd_admin`;

CREATE TABLE `yd_admin` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `last_login_ip` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录ip',
  `last_login_time` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后登录时间',
--   `email` varchar(40) NOT NULL DEFAULT '' COMMENT '邮箱',
--   `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
--   `realname` varchar(50) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `yd_admin` WRITE;
/*!40000 ALTER TABLE `yd_admin` DISABLE KEYS */;

INSERT INTO `yd_admin` (`id`, `username`, `password`, `last_login_ip`, `last_login_time`, `status`, `create_time`, `update_time`)
VALUES
	(1,'admin','e10adc3949ba59abbe56e057f20f883e',2130706433,1479969414,1,1477623198,1477623198),
	(2,'zhenxun','e10adc3949ba59abbe56e057f20f883e',2130706433,1476067533,1,1477624790,1477624790),
	(3,'zhangsan','e10adc3949ba59abbe56e057f20f883e',0,0,1,1477625400,1477625400),
	(4,'test','e10adc3949ba59abbe56e057f20f883e',2130706433,1495012830,1,1479969550,1479969550);

/*!40000 ALTER TABLE `yd_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yd_admin_group`;

CREATE TABLE `yd_admin_group` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '组名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  `list_order` smallint(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `list_order` (`list_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `yd_admin_group` WRITE;
/*!40000 ALTER TABLE `yd_admin_group` DISABLE KEYS */;

INSERT INTO `yd_admin_group` (`id`, `name`, `description`, `rules`, `list_order`, `create_time`, `update_time`)
VALUES
	(1,'普通管理员','密码加密只是MD5','',0,1477622552,1477622552),
	(2,'工作人员','仅拥有日志管理权限','18,23,27',0,1476067479,1476067479),
	(3,'后台演示','只能看，不能增删改','31,1,2,3,7,8,23,18,12,13,27',0,1479969527,1479969527);

/*!40000 ALTER TABLE `yd_admin_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table yd_admin_group_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yd_admin_group_access`;

CREATE TABLE `yd_admin_group_access` (
  `admin_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `admin_group_id` int(11) UNSIGNED NOT NULL COMMENT '用户组id',
  UNIQUE KEY `adminid_group_id` (`admin_id`,`admin_group_id`),
  KEY `admin_id` (`admin_id`),
  KEY `admin_group_id` (`admin_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `yd_admin_group_access` WRITE;
/*!40000 ALTER TABLE `yd_admin_group_access` DISABLE KEYS */;

INSERT INTO `yd_admin_group_access` (`admin_id`, `admin_group_id`)
VALUES
	(2,1),
	(3,1),
	(3,2),
	(4,3);

/*!40000 ALTER TABLE `yd_admin_group_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table yd_admin_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yd_admin_log`;

CREATE TABLE `yd_admin_log` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户id',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `module` varchar(20) NOT NULL COMMENT '模块',
  `controller` varchar(20) NOT NULL COMMENT '控制器',
  `action` varchar(20) NOT NULL COMMENT '动作',
  `params` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  `data` varchar(255) NOT NULL DEFAULT '' COMMENT '详细参数',
  `method` enum('GET','POST') DEFAULT 'GET',
  `ip` int(11) NOT NULL COMMENT 'ip地址',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- LOCK TABLES `admin_log` WRITE;
/*!40000 ALTER TABLE `yd_admin_log` DISABLE KEYS */;

-- INSERT INTO `admin_log` (`id`, `m`, `c`, `a`, `querystring`, `userid`, `username`, `ip`, `time`)
-- VALUES
-- 	(1,'admin','Group','index','',4,'test',2130706433,0);

/*!40000 ALTER TABLE `yd_admin_log` ENABLE KEYS */;
-- UNLOCK TABLES;

# Dump of table yd_admin_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `yd_admin_menu`;

CREATE TABLE `yd_admin_menu` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '' COMMENT '菜单名',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单',
  `icon` varchar(20) NOT NULL DEFAULT '' COMMENT '显示icon',
  `controller` varchar(20) NOT NULL DEFAULT '' COMMENT '控制器',
  `action` varchar(20) NOT NULL DEFAULT '' COMMENT '动作',
--   `data` varchar(50) NOT NULL DEFAULT '',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
--   `group` varchar(50) DEFAULT '' COMMENT '分组',
  `list_order` smallint(6) UNSIGNED NOT NULL DEFAULT '0',
  `display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示(1:显示,0:不显示)',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `list_order` (`list_order`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `yd_admin_menu` WRITE;
/*!40000 ALTER TABLE `yd_admin_menu` DISABLE KEYS */;

INSERT INTO `yd_admin_menu` (`id`, `name`, `parent_id`, `icon`, `controller`, `action`,  `tip`, `list_order`, `display`, `create_time`, `update_time`)
VALUES
	(1,'管理设置',0,'fa-users','admin','admin','',1,1,1476175413,1476175413),
	(2,'管理员管理',1,'','admin','index','',0,1,1476175413,1476175413),
	(3,'详情',2,'','admin','detail','',0,2,1476175413,1476175413),
	(4,'添加',2,'','admin','add','',0,2,1476175413,1476175413),
	(5,'修改',2,'','admin','edit','',0,2,1476175413,1476175413),
	(6,'删除',2,'','admin','del','',0,2,1476175413,1476175413),
	(7,'分组管理',1,'','group','index','',0,1,1476175413,1476175413),
	(8,'详情',7,'','group','info','',0,2,1476175413,1476175413),
	(9,'添加',7,'','group','add','',0,2,1476175413,1476175413),
	(10,'修改',7,'','group','edit','',0,2,1476175413,1476175413),
	(11,'删除',7,'','group','del','',0,2,1476175413,1476175413),
	(12,'菜单管理',18,'','menu','index','',0,1,1476175413,1476175413),
	(13,'查看',12,'','menu','info','',0,2,1476175413,1476175413),
	(14,'添加',12,'','menu','add','',0,2,1476175413,1476175413),
	(15,'修改',12,'','menu','edit','',0,2,1476175413,1476175413),
	(16,'删除',12,'','menu','del','',0,2,1476175413,1476175413),
	(18,'系统设置',0,'fa-cogs','menu','index','',2,1,1476175413,1476175413),
	(22,'权限设置',7,'','group','editrule','',999,2,1476175413,1476175413),
	(23,'个人设置',1,'','admin','public_edit_info','',999,1,1476175413,1476175413),
	(27,'日志管理',18,'','log','index','',999,1,1476175413,1476175413),
	(31,'系统首页',0,'fa-bank','index','index','',0,1,1476175413,1476175413);

/*!40000 ALTER TABLE `yd_admin_menu` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
