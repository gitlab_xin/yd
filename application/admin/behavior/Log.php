<?php

/**
 * 行为：日志写入
 * @file   Log.php  
 * @date   2017/7/11 15:19
 * @author Rudy
 */

namespace app\admin\behavior;

use app\admin\model\AdminLog;
use think\Config;
use think\Request;
use think\Session;
use think\Log as TpLog;

class Log
{

    /**
     * @author: Rudy
     * @time: 2017年8月11日
     * description:日志写入
     */
    public function run()
    {
        $request = Request::instance();
        $data = array();
        $data['controller'] = upToLine($request->controller());
        $data['action'] = strtolower($request->action());
        $data['method'] = $request->isPost()?'POST':'GET';//还有很多种,不过一般web用get和post,目前只存这两种
        $data['params'] = $request->query() ? '?' . $request->query() : '';
        $data['data'] = $request->param();

        if($this->filter($data) && $data['controller'] != 'validate'){
            $data['module'] = strtolower($request->module());
            $data['admin_id'] = Session::get('user_id') ? Session::get('user_id') : 0;
            $data['username'] = Session::get('user_name') ? Session::get('user_name') : 'vistor';
            $data['ip'] = ip2long($request->ip());
            $data['create_time'] = time();
            $data['data'] = serialize($data['data']);

            try{
                AdminLog::create($data);
            }catch (\Exception $e){
                TpLog::record($e->getMessage());
            }
        }

    }

    /***
     * @author: Rudy
     * @time: 2017年8月11日
     * description:检测是否需要写入日志
     * @param $data 引用参数,如果返回true,则$data['desc']会有值
     * @return bool 返回false的话则表示无需写入日志,该操作不属于写入日志的范围,true的话表示要写的
     */
    private function filter(&$data)
    {
        try{
            $res = true;

            $log = Config::get('adminlog');

            $allowable_action = $log['allowable_action'];
            $controller_mapping = $log['controller_mapping'];
            $mapping = $log['mapping'];

            if((isset($allowable_action[$data['action']][$data['method']]) && $arr = $allowable_action[$data['action']][$data['method']]) || //如果是增删改查之类的常规操作
                (isset($mapping[$data['controller']][$data['action']][$data['method']]) && $arr = $mapping[$data['controller']][$data['action']][$data['method']])){//如果是专有操作
                $controller = $controller_mapping[$data['controller']];
                $behaviour = $arr['desc'];
                $data['desc'] = "管理员在［{$controller}］进行了［{$behaviour}］操作";
                if(isset($arr['params']) && isset($data['data'][$arr['params']])){
                    $data['desc'] .= "，对应参数{$arr['params']}为{$data['data'][$arr['params']]}";
                }
            }else{
                $res = false;
            }
        }catch (\Exception $e){
            $res = false;
        }


        return $res;
    }
}
