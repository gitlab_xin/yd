<?php

/**
 * 行为：用户登录验证及权限验证
 * @file   CheckAuth.php
 * @date   2017/7/11 15:22
 * @author Rudy
 * @description 登录验证以及权限验证沿用开源着代码，但安全度低及代码未达到规范，日后会修改
 */

namespace app\admin\behavior;

use think\Response;
use think\Session;
use think\Request;
use think\Cookie;
use think\Url;
use app\admin\model\AdminMenu;
use app\admin\model\Admin;
use app\admin\traits\MyJump;
use app\common\tools\RedisUtil;
use think\exception\HttpResponseException;


class CheckAuth
{

    use MyJump;

    protected $user_id;
    protected $username;
    /*
     * 白名单：保存无需验证的controller
     */
    private $_controllerWhiteList = [
        'login'
    ];

    public function run()
    {
        //1.先验证是否在白名单，若在白名单内则直接return
        if (in_array(strtolower(Request::instance()->controller()), $this->_controllerWhiteList)) {
            return;
        }
        //2.查看是否已经登录,没有登录直接跳转到登录页
        if (!Session::get('user_id')) {
            //3.查看是否存在cookie,有则代表已经登录
            if (Cookie::get('user_id')) {
                $username = encry_code(cookie('user_name'), 'DECODE');
                if ($info = Admin::build()->field(['id', 'username', 'password'])->where(['username' => $username])->find()) {
                    //记录
                    Session::set('user_name', $info['username']);
                    Session::set('user_id', $info['id']);
                }
            } else {
                $this->error('请登陆', Url::build('/admin/login/index/'));
            }
        }
        //3.验证否是否有权限
        $this->user_id = Session::get('user_id');
        $this->username = Session::get('user_name');

        if (!$this->checkAuthor($this->user_id)) {
//            MyHook::remove('response_end','app\admin\behavior\Log');
            $this->error('你无权限操作');
        }
    }

    public function checkAuthor($user_id)
    {
        if (!$user_id) {
            return false;
        }
        if ($user_id == 1) {
            return true;
        }
        $request = Request::instance();
        $c = upToLine($request->controller());
        $a = strtolower($request->action());


        if (preg_match('/^public_/', $a)) {
            return true;
        }
        if ($c == 'index' && $a == 'index') {
            return true;
        }
        $model = new AdminMenu();
        $menu = $model->getMyMenu($user_id);
        foreach ($menu as $k => $v) {
            $v['controller'] = strtolower($v['controller']);
            $v['action'] = strtolower($v['action']);
            if (strtolower($v['controller']) == $c && strtolower($v['action']) == 'index' && $v['level'] == 3) {
                return true;
            } elseif (strtolower($v['controller']) == $c && strtolower($v['action']) == $a) {
                return true;
            }
        }
        return false;
    }

    public function buildToken()
    {
        $user_id = Session::get('user_id');
        $username = Session::get('user_name');

        $session_info = $user_id . '-' . $username;
        $token = md5($session_info);

        RedisUtil::getInstance()->hSet('admin_token', $token, $session_info . '-' . time());
        return $token;
    }

    public function checkToken()
    {
        $token = Request::instance()->header('token');

        if ($token === null) {
            throw new HttpResponseException($this->buildRes(['code' => 401, 'message' => '请重新登录']));
        }
        $info = RedisUtil::getInstance()->hget('admin_token', $token);
        list($user_id, $username, $time) = explode('-', $info);
        if (time() - $time > 60 * 60 * 2) {
            throw new HttpResponseException($this->buildRes(['code' => 401, 'message' => '登录超时，请重新登录']));
        }
        if (!$this->checkAuthor($user_id)) {
            throw new HttpResponseException($this->buildRes(['code' => 470, 'message' => '该管理员无权限操作']));
        }
    }

    private function buildRes($arr)
    {
//        header('Access-Control-Allow-Headers:token', false);
        header('Access-Control-Allow-Headers:content-type', false);
        return Response::create($arr, 'json', 200);
    }
}
