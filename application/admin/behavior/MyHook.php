<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/11
 * Time: 15:58
 */

namespace app\admin\behavior;


use think\Hook;

class MyHook extends Hook
{
    /**
     * @author: Rudy
     * @time: 2017年8月11日
     * description:删除tp5中的tags
     * @param $tag tag名称
     * @param null $behavior 具体的行为,为null时删除清空所有tags
     *
     */
//    public static function remove($tag, $behavior = null)
//    {
//        if(isset(self::$tags[$tag])){
//            if($behavior != null){
//                if(($key = array_search($behavior,self::$tags[$tag])) !== false){
//                    unset(self::$tags[$tag][$key]);
//                }
//            }else{
//                self::$tags[$tag] = [];
//            }
//        }
//    }

}