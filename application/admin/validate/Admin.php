<?php

namespace app\admin\validate;

use think\Validate;

/**
 * Description of Admin
 *
 * @author Rudy
 * @date 2017-7-11 23:40:39
 * @description     
 */
class Admin extends Validate
{

    //规则
    protected $rule = [
        'username' => 'require|max:20|unique:admin,username',
        'password' => 'require|max:32|min:6',
        'rpassword' => 'require|max:32|min:6|confirm:password',
    ];
    //字段
    protected $field = [
        'username' => '用户名',
        'password' => '密码',
        'rpassword' => '密码',
    ];
    //错误信息
    protected $message = [
        'username.require' => ':attribute不能为空',
        'username.max' => ':attribute长度不能超过20',
        'rpassword.confirm' => ':attribute不一致',
    ];
    //场景
    protected $scene = [
        'login' => ['username', 'password'],
        'register' => ['username', 'password', 'rpassword'],
        'editWithoutPassword' => ['username'],
        'editPersonalInfo' => ['password', 'rpassword']
    ];

    /**
     * @author: Rudy
     * @time: 2017年月
     * description:修改规则
     * @param string $key rule的key
     * @param string $value 修改的内容
     * @param bool $append 是否追加,true为追加,false为重置
     * @return $this
     */
    public function changeRule($key, $value, $append = false)
    {
        if (isset($this->rule[$key]) && is_string($value)) {
            $append ? $this->rule[$key] = $this->rule[$key] . $value : $this->rule[$key] = $value;
        }
        return $this;
    }

}
