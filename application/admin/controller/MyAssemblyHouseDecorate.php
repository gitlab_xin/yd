<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeDecoratePointLogic;
use app\admin\logic\MyAssemblyHouseDecorateLogic as ProgramLogic;
use app\admin\logic\MyAssemblyBuildingsLabelLogic as LabelLogic;
use think\Request;
use think\Url;
use think\Config;


class MyAssemblyHouseDecorate extends Base
{
    private $_programLogic;
    private $_labelLogic;

    /**
     * @time: 2018年4月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_programLogic = new ProgramLogic();
        $this->_labelLogic = new LabelLogic();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:31 上午
     * description
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $buildings_id = \app\common\model\MyAssemblyHouseType::build()
            ->where(['house_id' => $data['house_id']])
            ->value('buildings_id');
        $lists = $this->_programLogic->getAllProgram($data);
        $data['buildings_id'] = $buildings_id;
        $this->assign('data', $data);
        $this->assign('lists', $lists);
        return $this->fetch();
    }


    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:31 上午
     * description
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_programLogic->delProgram($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:49 上午
     * description
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $this->assign('action',Url::build('/admin/my_assembly_house_decorate/add'));
                $data = $request->get();
                $label = $this->_labelLogic->getList('');
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('house_id', $data['house_id']);
                $this->assign('label', $label);
                $this->assign('info', ['label'=>[],'tag' => '']);
                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $data = $request->post();
                $res = $this->_programLogic->addOrEdit($data,0);
                $res['redirect'] = Url::build('/admin/my_assembly_house_decorate/index',['house_id' => $data['house_id']]);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * User: zhaoxin
     * Date: 2020/1/18
     * Time: 10:49 上午
     * description
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $info = $this->_programLogic->getDecorate($request->get('id'));
                if($info == null){
                    return $this->error('不存在该分类');
                }
                $label = $this->_labelLogic->getList('');
                $info['label'] = json_decode($info['label'], true);
                $this->assign('info',$info);
                $this->assign('house_id', $info['house_id']);
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('action',Url::build('/admin/my_assembly_house_decorate/edit'));
                $this->assign('label', $label);

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->_programLogic->addOrEdit($data,$data['id']);
                $res['redirect'] = Url::build('/admin/my_assembly_house_decorate/edit',['id' => $data['id']]);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }
}
