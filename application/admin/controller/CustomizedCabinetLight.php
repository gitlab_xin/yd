<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedCabinetLightLogic as LightLogic;
use app\common\validate\CustomizedCabinetLight as LightValidate;
use app\common\validate\CustomizedCabinetLightAmbinet as AmbinetValidate;
use app\common\validate\CustomizedCabinetLightDirection as DirectionValidate;
use app\common\validate\CustomizedCabinetLightPoint as PointValidate;
use app\common\validate\CustomizedCabinetLightHemisphere as HemisphereValidate;
use app\common\validate\CustomizedCabinetLightSpot as SpotValidate;
use app\common\validate\CustomizedCabinetLightRectArea as RectAreaValidate;

class CustomizedCabinetLight extends Base implements ControllerInterface
{

    private $_lightLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_lightLogic = new LightLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $lists = $this->_lightLogic->getAll();

        $this->assign('lists', $lists);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new LightValidate();
        $this->assign('model', $model->getFieldsByScene('addOrEdit'));
        $this->assign('info', $this->_lightLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new LightValidate();

                $this->assign('model', $model->getFieldsByScene('addOrEdit'));
                $this->assign('ambinet', (new AmbinetValidate())->getAllInput('addOrEdit'));
                $this->assign('direction', (new DirectionValidate())->getAllInput('addOrEdit'));
                $this->assign('point', (new PointValidate())->getAllInput('addOrEdit'));
                $this->assign('hemisphere', (new HemisphereValidate())->getAllInput('addOrEdit'));
                $this->assign('spot', (new SpotValidate())->getAllInput('addOrEdit'));
                $this->assign('rect_area', (new RectAreaValidate())->getAllInput('addOrEdit'));

                $this->assign('action', Url::build('/admin/customized_cabinet_light/add'));
                return $this->fetch('add');
            case 'POST':
                $data = $request->post();

                $res = $this->_lightLogic->add($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_lightLogic->getDetail($request->get('id'));

                list($info, $validate) = $this->_lightLogic->getDetailByType($info->type, $request->get('id'));

                $this->assign('info', $info);
                $this->assign('model', $validate->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_light/edit'));
                return $this->fetch('edit');
            case 'POST':
                $data = $request->post();

                $res = $this->_lightLogic->edit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_lightLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}