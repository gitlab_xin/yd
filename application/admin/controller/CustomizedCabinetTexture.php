<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedCabinetTextureLogic as TextureLogic;
use app\common\validate\CustomizedCabinetTexture as TextureValidate;

class CustomizedCabinetTexture extends Base implements ControllerInterface
{

    private $_textureLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_textureLogic = new TextureLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['scene_id']
            ],
            'add'=>[
                'GET'=>['scene_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $scene_id = $request->get('scene_id');

        $lists = $this->_textureLogic->getAll($scene_id);

        $this->assign('lists', $lists);
        $this->assign('scene_id',$scene_id);
        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new TextureValidate();
        $this->assign('model',$model->getFieldsByScene('addOrEdit'));

        $this->assign('scene_id',$request->get('scene_id'));
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info', $this->_textureLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new TextureValidate();
                $this->assign('scene_id',$request->get('scene_id'));
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_texture/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_textureLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_textureLogic->getDetail($request->get('id'));
                $model = new TextureValidate();

                $this->assign('scene_id',$info->scene_id);
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_cabinet_texture/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_textureLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_textureLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}