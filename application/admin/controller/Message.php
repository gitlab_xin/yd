<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 10:57
 */

namespace app\admin\controller;

use app\admin\logic\MessageLogic;
use think\Request;
use think\Url;
use think\Config;

class Message extends Base implements ControllerInterface
{
    private $_messagesLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_messagesLogic = new MessageLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_messagesLogic->getMessages();

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取消息详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_messagesLogic->getMessage($request->get('id'));

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('action',Url::build('/admin/message/add'));
                $this->assign('activities',$this->_messagesLogic->getActivities());
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_messagesLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('activities',$this->_messagesLogic->getActivities());
                $this->assign('info',$this->_messagesLogic->getMessage($request->get('id')));
                $this->assign('action',Url::build('/admin/message/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_messagesLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_messagesLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }


}