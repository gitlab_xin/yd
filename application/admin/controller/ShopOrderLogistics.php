<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/23
 * Time: 11:22
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\ShopOrderLogisticsLogic as LogisticsLogic;

class ShopOrderLogistics extends Base
{

    private $_logisticsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_logisticsLogic = new LogisticsLogic();
    }

    public function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['id']
            ],
            'add'=>[
                'GET'=>['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:显示物流信息
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $logistics_id = $request->get('id');
        $detail = $this->_logisticsLogic->getLogistics($logistics_id);
        $lists = $this->_logisticsLogic->getLogisticsMessages($logistics_id,$detail->logistics_type,$detail->logistics_number);
        $config = Config::get('mall.logistics');

        $this->assign('detail',$detail);
        $this->assign('status',$config['status']);
        $this->assign('logistics_id',$logistics_id);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:添加物流信息
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('logistics_id',$request->get('id'));
                $this->assign('action',Url::build('/admin/shop_order_logistics/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_logisticsLogic->addLogistics($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:删除物流信息
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_logisticsLogic->delLogistics($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function updateLogistics(Request $request)
    {
        $data = $request->post();
        $res = $this->_logisticsLogic->updateLogistics($data,$data['logistics_id']);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}