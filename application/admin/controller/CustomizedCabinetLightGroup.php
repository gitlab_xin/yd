<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedCabinetLightGroupLogic as LightGroupLogic;
use app\common\validate\CustomizedCabinetLightGroup as LightGroupValidate;

class CustomizedCabinetLightGroup extends Base
{

    private $_lightGroupLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_lightGroupLogic = new LightGroupLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $lists = $this->_lightGroupLogic->getAll();

        $this->assign('lists', $lists);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new LightGroupValidate();
        $this->assign('model', $model->getFieldsByScene('addOrEdit'));
        $this->assign('info', $this->_lightGroupLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new LightGroupValidate();

                $this->assign('model', $model->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_light_group/add'));
                return $this->fetch('add');
            case 'POST':
                $data = $request->post();

                $res = $this->_lightGroupLogic->add($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $light_group_id = $request->get('id');

                $lists = $this->_lightGroupLogic->getList();
                $seleted = $this->_lightGroupLogic->getSeleted($light_group_id);

                $this->assign('action', Url::build('/admin/customized_cabinet_light_group/update'));
                $this->assign('lists', $lists);
                $this->assign('seleted', $seleted);
                $this->assign('light_group_id', $light_group_id);
                return $this->fetch('update');
            case 'POST':
                $data = $request->post();

                $res = $this->_lightGroupLogic->addLight($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_lightGroupLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}