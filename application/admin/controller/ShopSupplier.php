<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/18
 * Time: 11:52
 */

namespace app\admin\controller;


use think\Config;
use think\Request;
use think\Url;
use app\admin\logic\ShopSupplierLogic as SupplierLogic;
use app\admin\logic\ShopSupplierParamsLogic as ParamsLogic;

class ShopSupplier extends Base
{
    private $_supplierLogic;
    private $_paramsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_supplierLogic = new SupplierLogic();
        $this->_paramsLogic = new ParamsLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_supplierLogic->getSuppliers();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:获取商家信息(单个)
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {

        $id = $request->get('id');
        $info = $this->_supplierLogic->getSupplier($id);
        $qualification = $info['qualification'] != null ?explode('|',$info['qualification']):[];

        $this->assign('info',$info);
        $this->assign('qualification',$qualification);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('params',$this->_paramsLogic->getParams(['title','content','logo_src'],0));
        $this->assign('after_sale_service',$this->_paramsLogic->getParams(['title','content','logo_src'],$id));
        return $this->fetch();

    }
    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:添加商家
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('params',$this->_paramsLogic->getParams(['title','content','logo_src']));
                $this->assign('action',Url::build('/admin/shop_supplier/add'));
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));

                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_supplierLogic->addSupplier($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:编辑商家
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $id = $request->get('id');
                $info = $this->_supplierLogic->getSupplier($id);
                if($info['qualification'] != null){
                    $this->assign('qualification',explode('|',$info['qualification']));
                }
                unset($info['qualification']);

                $this->assign('params',$this->_paramsLogic->getParams(['title','content','logo_src'],0));
                $this->assign('after_sale_service',$this->_paramsLogic->getParams(['title','content','logo_src'],$id));
                $this->assign('action',Url::build('/admin/shop_supplier/edit'));
                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));

                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_supplierLogic->editSupplier($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:删除商家
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_supplierLogic->delSupplier($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}