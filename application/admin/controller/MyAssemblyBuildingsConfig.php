<?php

/**
 *
 * @file   Menu.php
 * @date   2016-8-30 11:46:22
 * @author Zhenxun Du<5552123@qq.com>
 * @version    SVN:$Id:$
 */

namespace app\admin\controller;

use app\admin\logic\MenuLogic;
use app\admin\logic\MyAssemblyBuildingsLogic as BuildingsLogic;
use think\Request;
use think\Url;

class MyAssemblyBuildingsConfig extends Base {

    private $_buildingsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_buildingsLogic = new BuildingsLogic();

    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:50 上午
     * description
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(Request $request) {
        $data = $request->get();
        $lists = $this->_buildingsLogic->getAllMenu($data);
        $this->assign('selectMenu', $this->_buildingsLogic->selectMenu($data['buildings_id']));
        $this->assign('lists', $lists);
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:50 上午
     * description
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add(Request $request) {

        switch ($request->method()){
            case 'GET';
                $data = $request->get();
                $this->assign('selectMenu', $this->_buildingsLogic->selectMenu($data['buildings_id']));
                $this->assign('action',Url::build('/admin/my_assembly_buildings_config/add'));
                $this->assign('buildings_id', $data['buildings_id']);
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();
                if ($data['parent_id'] == null) {
                    $data['parent_id'] = 0;
                }
                $res = $this->_buildingsLogic->addOrEditMenu($data);
                $this->redirect($res['redirect']);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:51 上午
     * description
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit(Request $request) {

        switch ($request->method()){
            case 'GET';
                $info = $this->_buildingsLogic->getMenu($request->get('id'));
                $this->assign('buildings_id', $info['buildings_id']);
                $this->assign('info', $info);
                $this->assign('action',Url::build('/admin/my_assembly_buildings_config/edit'));
                $this->assign('selectMenu', $this->_buildingsLogic->selectMenu($info['buildings_id']));
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();
                if ($data['parent_id'] == null) {
                    $data['parent_id'] = 0;
                }
                $res = $this->_buildingsLogic->addOrEditMenu($data,$data['id']);
                $this->redirect($res['redirect']);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:51 上午
     * description
     * @param Request $request
     * @throws \think\Exception
     */
    public function del(Request $request) {
        $res = $this->_buildingsLogic->delMenu($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:51 上午
     * description
     * @param Request $request
     */
    public function setListorder(Request $request) {
        switch ($request->method()){
            case 'POST':
                $data = $request->post();
                $this->_buildingsLogic->setListOrder($data);
                return $this->redirect('/admin/my_assembly_buildings_config/index');
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}
