<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 15:45
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\UserInviteCodeLogic;
use think\Url;
use think\Config;

class UserInviteCode extends Base
{
    private $_codeLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_codeLogic = new UserInviteCodeLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:首页,获取所有轮播图(按照device_type,module_type排序)
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_codeLogic->getCodeGroup($data);
        $this->assign('data',$data);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:添加轮播图
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //get请求::直接展示表单

                $this->assign('action',Url::build('/admin/user_invite_code/add'));
                return $this->fetch('info');
            case 'POST':
                //post请求::处理数据
                $res = $this->_codeLogic->addOrEdit($request->post());
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default;
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param Request $request
     * @return mixed
     * @throws \think\exception\DbException
     * @throws null
     */
    public function edit(Request $request)
    {

        switch ($request->method()){
            case 'GET':
                //get请求::直接展示表单
                $info = $this->_codeLogic->getInfo($request->get('id'));
                $this->assign('info',$info);
                $this->assign('action',Url::build('/admin/user_invite_code/edit'));

                return $this->fetch('info');
            case 'POST':
                //post请求::处理数据
                $res = $this->_codeLogic->addOrEdit($request->post() ,$request->post('id'));
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default;
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:删除banner
     * @param Request $request
     */
    public function del(Request $request)
    {
        //get请求::直接展示表单
        $res = $this->_codeLogic->delCode($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}