<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedItemLogic as ItemLogic;

class CustomizedItem extends Base implements ControllerInterface
{

    private $_itemLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_itemLogic = new ItemLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['classify_id']
            ],
            'add'=>[
                'GET'=>['classify_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $classify_id = $request->get('classify_id');

        $lists = $this->_itemLogic->getAll($classify_id);

        $this->assign('lists', $lists);
        $this->assign('classify_id',$classify_id);
        $this->assign('item_type',Config::get('customized.item_type'));
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $this->assign('classify_id',$request->get('classify_id'));
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('item_type',Config::get('customized.item_type'));
        $this->assign('info', $this->_itemLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('item_type',Config::get('customized.item_type'));
                $this->assign('item_position',Config::get('customized.item_position'));
                $this->assign('classify_id',$request->get('classify_id'));
                $this->assign('action', Url::build('/admin/customized_item/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_itemLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_itemLogic->getDetail($request->get('id'));

                $this->assign('classify_id',$info->classify_id);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('item_type',Config::get('customized.item_type'));
                $this->assign('item_position',Config::get('customized.item_position'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_item/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_itemLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_itemLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}