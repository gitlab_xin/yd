<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeFurnitureLogic as FurnitureLogic;
use think\Request;

/**
 * Class MyHomeFurniture 我的家-家具
 * @package app\admin\controller
 */
class MyHomeFurniture extends Base
{
    private $_FurnitureLogic;

    /**
     * 初始化函数
     * MyHomeFurniture constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_FurnitureLogic = new FurnitureLogic();
    }

    /**
     * @time: 2018年4月
     * description:家具列表
     * User: Airon
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_FurnitureLogic->getFurnitureList();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @time: 2018年4月
     * description:删除家具
     * User: Airon
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_FurnitureLogic->delFurniture($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
