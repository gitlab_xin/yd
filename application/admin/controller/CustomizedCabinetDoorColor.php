<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:50
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedCabinetDoorColorLogic as CabinetDoorColorLogic;

class CustomizedCabinetDoorColor extends Base
{

    private $_cabinetDoorColorLogic;

    protected function addFilter()
    {
        return [
            'index' => [
                'GET' => ['id']
            ]
        ];
    }

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_cabinetDoorColorLogic = new CabinetDoorColorLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $cabinet_color_id = $request->get('id');
        $lists = $this->_cabinetDoorColorLogic->getAll($cabinet_color_id);

        $this->assign('lists', $lists);
        $this->assign('cabinet_color_id', $cabinet_color_id);
        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $cabinet_color_id = $request->get('id');
                $this->assign('cabinet_color_id', $cabinet_color_id);
                $this->assign('lists', array_column($this->_cabinetDoorColorLogic->getAll($cabinet_color_id), 'color_id'));
                $this->assign('colors', $this->_cabinetDoorColorLogic->getAllColor());
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('action', Url::build('/admin/customized_cabinet_door_color/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_cabinetDoorColorLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_cabinetDoorColorLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function quickSet(Request $request)
    {
        $cabinet_id = $request->get('id');
        $res = $this->_cabinetDoorColorLogic->quickSet($cabinet_id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function quickDel(Request $request)
    {
        $cabinet_id = $request->get('id');
        $res = $this->_cabinetDoorColorLogic->quickDel($cabinet_id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}