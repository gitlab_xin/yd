<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/20
 * Time: 9:36
 */

namespace app\admin\controller;

use think\Config;
use think\Request;
use think\Url;
use app\admin\logic\ShopAdvertisementLogic as AdLogic;

class ShopAdvertisement extends Base
{
    private $_AdLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_AdLogic = new AdLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_AdLogic->getAds();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:显示单个广告
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        $info = $this->_AdLogic->getAd($request->get('id'));
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:添加分类广告
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $topClassify = $this->_AdLogic->getTopClassify();

                $this->assign('topClassify',$topClassify);
                $this->assign('action',Url::build('/admin/shop_advertisement/add'));
                return $this->fetch('info');
            case 'POST':
                $res = $this->_AdLogic->addOrEditAd($request->post());
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:编辑广告
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $topClassify = $this->_AdLogic->getTopClassify();
                $info = $this->_AdLogic->getAd($request->get('id'));

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('topClassify',$topClassify);
                $this->assign('action',Url::build('/admin/shop_advertisement/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_AdLogic->addOrEditAd($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:删除广告
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $res  = $this->_AdLogic->delAd($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}