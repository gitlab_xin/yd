<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 16:56
 */

namespace app\admin\controller;

use app\admin\logic\HelpCenterLogic;
use think\Request;
use think\Url;

class HelpCenter extends Base
{
    private $_helpLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_helpLogic = new HelpCenterLogic();
    }


    protected function addFilter()
    {
        return [
            'index' => [
                'GET' => ['classify_id']
            ],
            'add' => [
                'GET' => ['classify_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $classify_id = $this->request->get('classify_id');
        $lists = $this->_helpLogic->getList(['classify_id' => $classify_id]);
        $this->assign('lists', $lists);
        $this->assign('classify_id', $classify_id);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:添加帮助中心
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $classify_id = $this->request->get('classify_id');
                $this->assign('action', Url::build('/admin/help_center/add'));
                $this->assign('classify_id', $classify_id);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_helpLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('info', ($info = $this->_helpLogic->getDetail($request->get('id'))));
                $this->assign('action', Url::build('/admin/help_center/edit'));
                $this->assign('classify_id', $info->classify_id);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_helpLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:查看详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('info', $this->_helpLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_helpLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}