<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/24
 * Time: 18:02
 */

namespace app\admin\controller;


use app\admin\logic\ConfigAreaLogic as AreaLogic;
use think\Controller;
use think\Request;
use think\Url;


class ConfigArea extends Base
{
    private $_areaLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_areaLogic = new AreaLogic();
    }

    public function addFilter()
    {
        return [
            'quickSet' => [
                'GET' => ['id']
            ],
            'quickReset' => [
                'GET' => ['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:展示所有省份以及其非配送区域
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_areaLogic->getAreas();
        $prov = $lists['prov'];
        $citys = $lists['citys'];

        $this->assign('prov',$prov);
        $this->assign('citys',$citys);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:快速设置非配送
     * @param Request $request
     * @return mixed
     */
    public function quickSet(Request $request)
    {
        $res = $this->_areaLogic->quickSet($request->get('id'),'0');
        return $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:快速设置非配送
     * @param Request $request
     * @return mixed
     */
    public function quickReset(Request $request)
    {
        $res = $this->_areaLogic->quickSet($request->get('id'),'1');
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $citys = $this->_areaLogic->getChildArea($request->get('id'));
                $this->assign('citys',$citys);
                $this->assign('action',Url::build('/admin/config_area/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_areaLogic->setDistribution($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }
}