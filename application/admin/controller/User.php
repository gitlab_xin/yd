<?php

/**
 *  后台继承类
 * @file   AdminLogic.php
 * @date   2016-8-23 19:45:21 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\controller;

use app\admin\logic\UserLogic;
use think\Request;
use think\Url;
use think\Config;

class User extends Base
{

    protected $_userLogic;

    /**
     * @author: Rudy
     * @time: 2017年7月11日 16:59
     * @description:初始化userLogic
     */
    public function __construct()
    {
        parent::__construct();
        $this->_userLogic = new UserLogic;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:添加filter
     * @return array
     */
    protected function addFilter()
    {
        return [
            'ban' => [
                'GET'=>['id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:获取用户信息(多个)
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        //添加验证
        if(count($data)>0){
            $res = $this->validateData($request);
            if($res['code'] == 0){
                $this->error($res['msg']);
            }
        }

        $lists = $this->_userLogic->getUsers($data);

        $this->assign('data',$data);
        $this->assign('statusMap',Config::get('mall.user')['status']);
        $this->assign(['lists'=>$lists]);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:获取单个用户信息
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        $info = $this->_userLogic->getUserInfo($request->get('id'));

        $this->assign('avatar',strpos($info['avatar'],'http') !== false?$info['avatar']:Config::get('qiniu.BucketDomain').$info['avatar']);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:添加用户(get则展示页面,post则进行增加操作)
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request){
        switch ($request->method()){
            case 'GET':
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_userLogic->addUser($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:编辑用户(get则展示页面,post则进行增加操作)
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request){
        switch ($request->method()){
            case 'GET':
                $info = $this->_userLogic->getUserInfo($request->get('id'));
                if($info['avatar'] != ''){
                    $this->assign('avatar',strpos($info['avatar'],'http') !== false?$info['avatar']:Config::get('qiniu.BucketDomain').$info['avatar']);
                }
                $this->assign('info',$info);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_userLogic->editUser($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:删除用户
     * @param Request $request
     *
     */
    public function del(Request $request)
    {
        $res = $this->_userLogic->delUser($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:封号处理
     * @param Request $request
     * @return mixed|void
     */
    public function ban(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $id = $request->get('id');
                $info = $this->_userLogic->getBlackListByUserId($id);

                $this->assign('id',$id);
                $this->assign('info',$info);
                $this->assign('action',Url::build('/admin/user/ban'));
                return $this->fetch();
            case 'POST':
                $data = $request->post();
                $res = $this->_userLogic->banUser($data,isset($data['id'])?$data['id']:0);
                return $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
            default:
                return $this->error('无效的请求方式');
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:验证搜索条件
     * @param Request $request
     * @return array
     */
    public function validateData(Request $request)
    {
        $retArr = [];

        $data = $request->get();
        $res = $this->validate($data,'search.userSearch');
        if($res === true){
            $retArr['msg'] = 'pass';
            $retArr['code'] = 1;
        }else{
            $retArr['msg'] = $res;
            $retArr['code'] = 0;
        }
        return $retArr;
    }
}
