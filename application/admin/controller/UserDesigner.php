<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/24
 * Time: 15:27
 */

namespace app\admin\controller;

use app\admin\logic\UserDesignerLogic;
use think\Request;
use think\Config;

class UserDesigner extends Base
{
    private $_designerLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_designerLogic = new UserDesignerLogic();
    }

    public function addFilter()
    {
        return [
            'reject' => [
                'POST' => ['id']
            ],
            'pass' => [
                'POST' => ['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_designerLogic->getDesignerList($data);

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('statusMap',Config::get('mall.works')['type']);
        $this->assign('lists',$lists);
        $this->assign('data',$data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:通过审核
     * @param Request $request
     */
    public function pass(Request $request)
    {
        $data = $request->post();
        $res = $this->_designerLogic->changeStatus($data,$data['id'],1);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:拒绝审核
     * @param Request $request
     */
    public function reject(Request $request)
    {
        $data = $request->post();
        $res = $this->_designerLogic->changeStatus($data,$data['id'],2);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:查看单个明细
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_designerLogic->getDetail($request->get('id'));
        $this->assign('imgPre',Config('qiniu.BucketDomain'));
        $this->assign('info',$info);
        return $this->fetch();
    }
}