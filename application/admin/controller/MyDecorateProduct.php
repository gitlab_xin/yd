<?php
namespace app\admin\controller;

use app\common\model\CustomizedColor as ColorModel;
use app\common\model\MyDecorateRoom;
use think\Config;
use think\Request;
use think\Url;
use app\admin\logic\MyDecorateProductLogic;

class MyDecorateProduct extends Base
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $data = $request->get();

        $house_id = \app\common\model\MyAssemblyHouseDecorate::build()
            ->where(['program_id' => $data['program_id']])
            ->value('house_id');
        $data['house_id'] = $house_id;
        if (!isset($data['room_id'])){
            $data['room_id'] = '';
        }
        $lists = MyDecorateProductLogic::index($data);
        $rooms = MyDecorateProductLogic::roomIndex($data);
        $this->assign('lists', $lists);
        $this->assign('rooms', $rooms);
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function room_add(Request $request){
        $data = $request->post();
        if (!empty($data['name'])){

            MyDecorateProductLogic::roomAdd($data);
        }

        $this->redirect(Url::build('/admin/my_decorate_product/index?program_id='.$data['id']));

    }

    public function room_edit(Request $request){
        $data = $request->post();
        if (!empty($data['name'])){

            MyDecorateProductLogic::roomEdit($data);
        }
        $id = MyDecorateRoom::build()->where(['id' => $data['id']])->value('decorate_id');

        $this->redirect(Url::build('/admin/my_decorate_product/index?program_id='.$id));

    }

    public function room_del(Request $request){
        $data = $request->get();
        MyDecorateProductLogic::roomDel($data);
        $id = MyDecorateRoom::build()->where(['id' => $data['id']])->value('decorate_id');

        $this->redirect(Url::build('/admin/my_decorate_product/index?program_id='.$id));

    }

    public function del(Request $request){
        $data = $request->get();
        MyDecorateProductLogic::del($data);
        $id = \app\common\model\MyDecorateProduct::build()->where(['id' => $data['id']])->value('decorate_id');

        $this->redirect(Url::build('/admin/my_decorate_product/index?program_id='.$id.'&room_id='.$data['room_id']));

    }

    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('action', Url::build('/admin/my_decorate_product/add'));
                $data = $request->get();
                $rooms = MyDecorateProductLogic::roomIndex($data);
//                $color = MyDecorateProductLogic::colorIndex();
                $this->assign('rooms', $rooms);
                $this->assign('colors', []);
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('program_id', $data['program_id']);
                $this->assign('room_id', $data['room_id']);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = MyDecorateProductLogic::add($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('action', Url::build('/admin/my_decorate_product/edit'));
                $data = $request->get();
                $rooms = MyDecorateProductLogic::roomIndex($data);
                $info = MyDecorateProductLogic::info($data['id']);
                $color = MyDecorateProductLogic::colorIndex($info);
                $this->assign('rooms', $rooms);
                $this->assign('info', $info);
                $this->assign('colors', $color);
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('program_id', $data['program_id']);
                $this->assign('room_id', $info['room_id']);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = MyDecorateProductLogic::edit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    public function getProduct(Request $request){
        $data = $request->post();
        if ($data['type'] == '3'){
            $product_html = MyDecorateProductLogic::productindex($data);
        } elseif ($data['type'] == '2'){
            $product_html = MyDecorateProductLogic::product2index($data);
        }

        return $product_html;
    }

    public function getColor(Request $request){
        $data = $request->post();
        $color = MyDecorateProductLogic::colorIndex2($data);

        return $color;

    }
}