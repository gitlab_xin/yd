<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/26
 * Time: 16:44
 */

namespace app\admin\controller;

use app\admin\logic\DemandLogic;
use think\Request;
use think\Config;
use think\Url;

class Demand extends Base
{
    private $_demandLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_demandLogic = new DemandLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:首页
     * @param Request $request
     * @return mixed
     *
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_demandLogic->getAll($data);
        $config = Config::get('demand');

        $this->assign('statusMap',$config['status']);
        $this->assign('lists',$lists);
        $this->assign('data',$data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:查看需求详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_demandLogic->getDetail($request->get('id'));
        $imgList = $info->img_src_list == ''?[]:explode(",",$info->img_src_list);
        foreach ($imgList as $key => $value){
            $imgList[$key] = explode(':',$value)[0];
        }
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('imgList',$imgList);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_demandLogic->getDetail($request->get('id'));

                $this->assign('info',$info);
                $this->assign('action',Url::build('/admin/demand/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_demandLogic->edit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:删除需求
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_demandLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}