<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedCabinetControlLogic as ControlLogic;
use app\common\validate\CustomizedCabinetControl as ControlValidate;

class CustomizedCabinetControl extends Base implements ControllerInterface
{

    private $_controlLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_controlLogic = new ControlLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $lists = $this->_controlLogic->getAll();

        $this->assign('lists', $lists);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new ControlValidate();
        $this->assign('model',$model->getFieldsByScene('addOrEdit'));
        $this->assign('info', $this->_controlLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new ControlValidate();
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_control/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_controlLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_controlLogic->getDetail($request->get('id'));
                $model = new ControlValidate();

                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_cabinet_control/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_controlLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_controlLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}