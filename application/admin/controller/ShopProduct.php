<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/15
 * Time: 14:28
 */

namespace app\admin\controller;

use think\Request;
use think\Url;
use think\Config;
use app\admin\logic\ShopProductLogic;

class ShopProduct extends Base
{

    private $_productLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_productLogic = new ShopProductLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:添加filter
     * @return array
     *
     */
    public function addFilter(){
        return [
            'changeSale'=>[
                'GET' => ['id','is_sale']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:商品首页,默认上架商品,未删除的
     * @param Request $request
     * @param string $view
     * @param string $is_sale
     * @return mixed
     */
    public function index(Request $request,$view = 'index',$is_sale = '1')
    {
        $data = $request->get();

        //验证搜索条件
        if(count($data)>0){
            $res = $this->validateData($request);
            if($res['code'] == 0){
                $this->error($res['msg']);
            }
        }
        $lists = $this->_productLogic->getProducts($data,$is_sale);

        $this->assign('data',$data);
        $this->assign('is_sale',$is_sale);
        $this->assign('classify_one_ids',$this->_productLogic->getClassify());
        if(isset($data['classify_two_id']) && $data['classify_one_id']!=0){
            $this->assign('classify_two_ids',$this->_productLogic->getClassify($data['classify_one_id']));
        }
        if(isset($data['classify_three_id']) && $data['classify_two_id']!=0){
            $this->assign('classify_three_ids',$this->_productLogic->getClassify($data['classify_two_id']));
        }
        $this->assign('suppliers',$this->_productLogic->getSuppliers());
        $this->assign('lists',$lists);
        return $this->fetch($view);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:获取单个商品信息
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        //商品信息
        $info = $this->_productLogic->getProduct($request->get('id'));
        //商品图片
        $img_src_list = $info['img_src_list']!= null ?explode('|',$info['img_src_list']):[];
        unset($info['img_src_list']);
        //商品分类
        $classify = $this->_productLogic->getClassifysForProduct([$info['classify_one_id'],$info['classify_two_id'],$info['classify_three_id']]);
        //商品参数
        if($info['product_param'] != null){
            $this->assign('product_param',unserialize($info['product_param']));
        }
        unset($info['product_param']);

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('shop_classify',isset($classify['line'])?$classify['line']:'');
        $this->assign('img_src_list',$img_src_list);
        $this->assign('info',$info);
        return $this->fetch();

    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:添加商品
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('suppliers',$this->_productLogic->getSuppliers());
                $this->assign('classify_one_ids',$this->_productLogic->getClassify());
                $this->assign('provs',$this->_productLogic->getArea());
                $this->assign('action',Url::build('/admin/shop_product/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_productLogic->addOrEditProduct($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:编辑商品
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //商品信息
                $info = $this->_productLogic->getProduct($request->get('id'));
                //商品图片
                $img_src_list = $info['img_src_list']!= null ?explode('|',$info['img_src_list']):[];
                unset($info['img_src_list']);
                //商品分类
                $classify = $this->_productLogic->getAllClassifys([0=>'classify_one',
                    $info['classify_one_id'] => 'classify_two',
                    $info['classify_two_id'] => 'classify_three']);
                //商品参数
                if($info['product_param'] != null){
                    $this->assign('product_param',unserialize($info['product_param']));
                }
                unset($info['product_param']);
                //城市
                if($info['province'] != null && $info['city'] != null){
                    $this->assign('citys',$this->_productLogic->getArea($info['province']));
                }

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('img_src_list',$img_src_list);
                $this->assign('suppliers',$this->_productLogic->getSuppliers());
                $this->assign('provs',$this->_productLogic->getArea());
                $this->assign('classify_one_ids',$classify['classify_one']);
                $this->assign('classify_two_ids',$classify['classify_two']);
                $this->assign('classify_three_ids',$classify['classify_three']);
                $this->assign('action',Url::build('/admin/shop_product/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_productLogic->addOrEditProduct($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:删除商品
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_productLogic->delProduct($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:上下架商品
     * @param Request $request
     * @return mixed
     */
    public function changeSale(Request $request)
    {
        $res = $this->_productLogic->changeProductSale($request->get('id'),$request->get('is_sale'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:获取下架商品
     * @param Request $request
     * @return mixed
     */
    public function showNotSale(Request $request)
    {
        return $this->index($request,'index','0');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:验证商品搜索条件
     * @param Request $request
     * @return array
     */
    public function validateData(Request $request)
    {
        $retArr = [];

        $data = $request->get();
        $res = $this->validate($data,'search.productSearch');
        if($res === true){
            $retArr['msg'] = 'pass';
            $retArr['code'] = 1;
        }else{
            $retArr['msg'] = $res;
            $retArr['code'] = 0;
        }
        return $retArr;
    }
}