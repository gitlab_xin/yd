<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeDecoratePointLogic;
use app\admin\logic\MyHomeDecorateProgramLogic as ProgramLogic;
use think\Request;
use think\Url;
use think\Config;

/**
 * Class MyHomeClassify 我的家 户型管理
 * @package app\admin\controller
 */
class MyHomeDecorateProgram extends Base
{
    private $_programLogic;

    /**
     * @time: 2018年4月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_programLogic = new ProgramLogic();
    }

    /**
     * @time: 2018年4月
     * description:展示该户型下所有模型
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $id = $request->get('id');
        $lists = $this->_programLogic->getAllProgram($id);
        $this->assign('house_id', $id);
        $this->assign('lists', $lists);
        return $this->fetch();
    }


    /**
     * @time: 2018年4月
     * description:删除户型
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_programLogic->delProgram($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function update(Request $request){
        $data = $request->post();
        $res = $this->_programLogic->updateProgram($data);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
