<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedItemClassifyLogic as ItemClassifyLogic;
use app\common\validate\CustomizedItemClassify as ItemClassifyValidate;

class CustomizedItemClassify extends Base implements ControllerInterface
{

    private $_itemClassifyLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_itemClassifyLogic = new ItemClassifyLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_itemClassifyLogic->getAll();

        $this->assign('cabinet_type',Config::get('customized.cabinet_type'));
        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $this->error('尚未开放该功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $model = new ItemClassifyValidate();

                $this->assign('model', $model->getFieldsByScene('addOrEdit'));
                $this->assign('action',Url::build('/admin/customized_item_classify/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_itemClassifyLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $model = new ItemClassifyValidate();

                $this->assign('model', $model->getFieldsByScene('addOrEdit'));
                $this->assign('info',$this->_itemClassifyLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/customized_item_classify/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_itemClassifyLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_itemClassifyLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}