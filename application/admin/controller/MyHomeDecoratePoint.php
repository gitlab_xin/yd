<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeDecoratePointLogic as DecorateLogic;
use think\Request;
use think\Url;
use think\Config;

/**
 * Class MyHomeClassify 我的家 装修亮点
 * @package app\admin\controller
 */
class MyHomeDecoratePoint extends Base
{
    private $_decorateLogic;

    /**
     * @time: 2017年7月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_decorateLogic = new DecorateLogic();
    }

    /**
     * @time: 2017年7月
     * description:展示所有装修亮点
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_decorateLogic->getAllDecorate();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @time: 2017年7月
     * description:添加装修亮点
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //添加装修亮点时:get请求
                $this->assign('action', Url::build('/admin/my_home_decorate_point/add'));

                return $this->fetch('info');
            case 'POST':
                //添加装修亮点时:post请求
                $res = $this->_decorateLogic->addOrEditDecorate($request->post(), 0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2017年7月
     * description:修改装修亮点
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //修改分类时:get请求
                $info = $this->_decorateLogic->getDecorate($request->get('id'));
                if ($info == null) {
                    $this->error('不存在该分类');
                }
                $this->assign('info', $info);
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('action', Url::build('/admin/my_home_decorate_point/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->_decorateLogic->addOrEditDecorate($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2017年7月
     * description:删除装修亮点
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_decorateLogic->delDecorate($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
