<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\CustomizedCabinetRenderLogic as RenderLogic;

class CustomizedCabinetSceneRender extends Base
{

    private $_renderLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_renderLogic = new RenderLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['scene_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $scene_id = $request->get('scene_id');

        $lists = $this->_renderLogic->getList();
        $seleted = $this->_renderLogic->getSeleted($scene_id);

        $this->assign('lists', $lists);
        $this->assign('seleted', $seleted);
        $this->assign('scene_id',$scene_id);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        switch ($request->method()) {
            case 'POST':
                $data = $request->post();
                $res = $this->_renderLogic->addScene($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }


}