<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/16
 * Time: 14:49
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\CustomizedEvaluateLogic as EvaluateLogic;

class CustomizedEvaluate extends Base
{
    private $_evaluateLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_evaluateLogic = new EvaluateLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:列表
     * @return mixed
     */
    public function index()
    {
        $this->assign('lists',$this->_evaluateLogic->getAll());
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:删除评论
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_evaluateLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}