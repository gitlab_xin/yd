<?php

/**
 *
 * @file   Menu.php
 * @date   2016-8-30 11:46:22
 * @author Zhenxun Du<5552123@qq.com>
 * @version    SVN:$Id:$
 */

namespace app\admin\controller;

use app\admin\logic\MenuLogic;
use app\admin\logic\MyAssemblyBuildingsLogic as BuildingsLogic;
use think\Request;
use think\Url;

class MyAssemblyHouseConfig extends Base {

    private $_buildingsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_buildingsLogic = new BuildingsLogic();

    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 9:50 上午
     * description
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(Request $request) {
        $data = $request->get();
        $buildings_id = \app\common\model\MyAssemblyHouseType::build()
            ->where(['house_id' => $data['house_id']])
            ->value('buildings_id');
        $data['buildings_id'] = $buildings_id;
        $lists = $this->_buildingsLogic->getAllMenu($data);
        $lists = $this->_buildingsLogic->menuStatus($lists, $data);
        $this->assign('lists', $lists);
        $this->assign('house_id', $data['house_id']);
        $this->assign('buildings_id', $data['buildings_id']);
        $this->assign('data', $data);

        return $this->fetch();
    }

    public function change(Request $request){
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();

        $lists = $this->_buildingsLogic->change($data);
    }



}
