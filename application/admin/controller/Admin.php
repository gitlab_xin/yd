<?php

namespace app\admin\controller;

use think\Request;
use think\Session;
use app\admin\logic\AdminLogic;

class Admin extends Base
{

    protected $_adminLogic;

    /**
     * @author: Rudy
     * @time: 2017年7月11日 16:59
     * @description:初始化loginLogic
     */
    public function __construct()
    {
        parent::__construct();
        $this->_adminLogic = new AdminLogic;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月11日 17:14
     * @description:后台用户页
     */
    public function index()
    {
        $lists = $this->_adminLogic->getAllAdmin();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:修改管理员个人密码
     * @param Request $request
     * @return mixed
     */
    public function public_edit_info(Request $request)
    {
        $id = Session::get('user_id');
        switch ($request->method()) {
            case 'GET':
                $info = $this->_adminLogic->getAdminInfo($id);
                $this->assign('info',$info);
                return $this->fetch();
            case 'POST':
                $data = $request->post();

                $res = $this->_adminLogic->editPersonalInfo($data,$id);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:获取单个管理员信息
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_adminLogic->getAdminInfo($request->get('id'));
        $groups = $this->_adminLogic->getGroups();

        $this->assign('groups',$groups);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:添加管理员
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $groups = $this->_adminLogic->getGroups();

                $this->assign('groups',$groups);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_adminLogic->addAdmin($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:编辑用户
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request){

        switch ($request->method()){
            case 'GET':
                $info = $this->_adminLogic->getAdminInfo($request->get('id'));
                $groups = $this->_adminLogic->getGroups();

                $this->assign('groups',$groups);
                $this->assign('info',$info);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_adminLogic->editAdmin($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:删除管理员
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_adminLogic->delAdmin($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
