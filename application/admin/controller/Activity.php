<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 9:47
 */

namespace app\admin\controller;


use app\admin\logic\ActivityLogic;
use think\Config;
use think\Request;
use think\Url;

class Activity extends Base
{
    private $_activityLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_activityLogic = new ActivityLogic();
    }

    protected function addFilter()
    {
        return [
            'end'=>[
                'GET'=>['id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:活动列表
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_activityLogic->getActivities($data);

        $this->assign('data',$data);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取单个活动信息
     * @param Request $request
     * @return mixed
     *
     */
    public function detail(Request $request)
    {
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info',$this->_activityLogic->getActivity($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:添加活动信息
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('action',Url::build('/admin/activity/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_activityLogic->addOrEditActivity($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年77月29日
     * description:编辑活动内容
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_activityLogic->getActivity($request->get('id'));

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/activity/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_activityLogic->addOrEditActivity($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:删除活动(伪删除)
     * @param Request $request
     *
     */
    public function del(Request $request)
    {
        $res = $this->_activityLogic->delActivity($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:提前结束活动
     * @param Request $request
     */
    public function end(Request $request)
    {
        $res = $this->_activityLogic->endActivity($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}