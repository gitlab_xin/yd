<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/5
 * Time: 14:42
 */

namespace app\admin\controller;

use app\admin\logic\DemandBannerLogic as BannerLogic;
use think\Request;
use think\Url;
use think\Config;

class DemandBanner extends Base implements ControllerInterface
{
    private $_bannerLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_bannerLogic = new BannerLogic();
    }

    protected function addFilter()
    {
        return [
            'add'=>[
                'GET'=>['demand_id']
            ],
        ];
    }

    public function index(Request $request)
    {
        $lists = $this->_bannerLogic->getAll();

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    public function detail(Request $request)
    {
        $this->error('暂不开放此功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月5日
     * description:添加banner
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $demand_id = $request->get('demand_id');
                $schemes = $this->_bannerLogic->getSchemesByDemand($demand_id);

                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('demand_id',$demand_id);
                $this->assign('schemes',$schemes);
                $this->assign('action',Url::build('/admin/demand_banner/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_bannerLogic->add($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $banner_id = $request->get('id');
                $banner = $this->_bannerLogic->getDetail($banner_id);
                $schemes = $this->_bannerLogic->getSchemesByDemand($banner->demand_id);

                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('demand_id',$banner->demand_id);
                $this->assign('banner',$banner);
                $this->assign('schemes',$schemes);
                $this->assign('action',Url::build('/admin/demand_banner/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_bannerLogic->edit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月5日
     * description:删除banner
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_bannerLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}