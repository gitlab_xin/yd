<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/21
 * Time: 9:53
 */

namespace app\admin\controller;


use think\Request;
use think\Url;
use app\admin\logic\ShopProductStandardLogic as ProductStandardLogic;
use app\admin\logic\ShopProductLogic as ProductLogic;
use app\admin\logic\ShopPopularLogic as PopularLogic;

class ShopProductStandard extends Base
{
    private $_standardLogic;
    private $_productLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_standardLogic = new ProductStandardLogic();
        $this->_productLogic = new ProductLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['product_id']
            ],
            'add'=>[
                'GET'=>['product_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:查看单个规格
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        $info = $this->_standardLogic->getStandard($request->get('id'));
        $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:规格首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $product_id = $request->get('product_id');

        $lists = $this->_standardLogic->getStandardsByProductId($product_id);
        $this->assign('product_id',$product_id);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:添加规格
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $product_id = $request->get('product_id');
                $result = $this->_standardLogic->checkLimit($product_id);

                if($result > 0){
                    $this->error('一个商品最多只能添加'.$result .'个规格');
                }

                $this->assign('product_id',$product_id);
//                $this->assign('classify_one_ids',$this->_productLogic->getClassify());
                $this->assign('action',Url::build('/admin/shop_product_standard/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_standardLogic->addOrEditStandard($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:编辑规格
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_standardLogic->getStandard($request->get('id'));
                //商品分类
//                if(isset($info->product) && $info->product != null){
//                    $classify = $this->_productLogic->getAllClassifys([0=>'classify_one',
//                        $info->product->classify_one_id => 'classify_two',
//                        $info->product->classify_two_id => 'classify_three']);
//                    //获取三级分类下的所有商品
//                    $popularLogic = new PopularLogic();
//                    $products = $popularLogic->getProductByClassify($info->product->classify_three_id );
//
//                    $this->assign('classify_one_ids',$classify['classify_one']);
//                    $this->assign('classify_two_ids',$classify['classify_two']);
//                    $this->assign('classify_three_ids',$classify['classify_three']);
//                    $this->assign('products',$products);
//                }else{
//                    $this->assign('classify_one_ids',$this->_productLogic->getClassify());
//                }

                $this->assign('info',$info);
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('product_id',$info->product_id);
                $this->assign('action',Url::build('/admin/shop_product_standard/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_standardLogic->addOrEditStandard($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:删除规格
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_standardLogic->delStandard($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function change(Request $request)
    {
        $res = ['code' => 0, 'msg' => '失败'];
        $data = $request->param();
        $result = $this->_standardLogic->change($data);
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }
}