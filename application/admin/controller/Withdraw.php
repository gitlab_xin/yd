<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 11:45
 */

namespace app\admin\controller;

use app\admin\logic\WithdrawLogic;
use think\Request;
use think\Config;

class Withdraw extends Base
{
    private $_withdrawLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_withdrawLogic = new WithdrawLogic();
    }

    public function addFilter()
    {
        return [
            'reject' => [
                'POST' => ['id']
            ],
            'pass' => [
                'POST' => ['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_withdrawLogic->getWithdrawList($data);

        $this->assign('statusMap',array_merge(Config::get('withdraw.status')));
        $this->assign('lists',$lists);
        $this->assign('data',$data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:通过审核
     * @param Request $request
     */
    public function pass(Request $request)
    {
        $data = $request->post();
        $res = $this->_withdrawLogic->changeStatus($data,$data['id'],1);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:拒绝审核
     * @param Request $request
     */
    public function reject(Request $request)
    {
        $data = $request->post();
        $res = $this->_withdrawLogic->changeStatus($data,$data['id'],2);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:查看单个申请明细_
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_withdrawLogic->getDetail($request->get('id'));
        $this->assign('info',$info);
        return $this->fetch();
    }
}