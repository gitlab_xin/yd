<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedCabinetCameraLogic as CameraLogic;
use app\common\validate\CustomizedCabinetCamera as CameraValidate;

class CustomizedCabinetCamera extends Base implements ControllerInterface
{

    private $_cameraLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_cameraLogic = new CameraLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $lists = $this->_cameraLogic->getAll();

        $this->assign('lists', $lists);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new CameraValidate();
        $this->assign('model',$model->getFieldsByScene('addOrEdit'));
        $this->assign('info', $this->_cameraLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new CameraValidate();
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_camera/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_cameraLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_cameraLogic->getDetail($request->get('id'));
                $model = new CameraValidate();

                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_cabinet_camera/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_cameraLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_cameraLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}