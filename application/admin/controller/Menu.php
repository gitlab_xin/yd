<?php

/**
 *  
 * @file   Menu.php  
 * @date   2016-8-30 11:46:22 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\controller;

use app\admin\logic\MenuLogic;
use think\Request;
use think\Url;

class Menu extends Base {

    private $_menuLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_menuLogic = new MenuLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:菜单首页
     * @return mixed
     */
    public function index() {
        $lists = $this->_menuLogic->getAllMenu();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:添加菜单
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request) {

        switch ($request->method()){
            case 'GET';
                $this->assign('selectMenu', $this->_menuLogic->selectMenu());
                $this->assign('action',Url::build('/admin/menu/add'));
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();
                if ($data['parent_id'] == null) {
                    $data['parent_id'] = 0;
                }
                $res = $this->_menuLogic->addOrEditMenu($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:修改菜单
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request) {

        switch ($request->method()){
            case 'GET';
                $this->assign('info', $this->_menuLogic->getMenu($request->get('id')));
                $this->assign('action',Url::build('/admin/menu/edit'));
                $this->assign('selectMenu', $this->_menuLogic->selectMenu());
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();
                if ($data['parent_id'] == null) {
                    $data['parent_id'] = 0;
                }
                $res = $this->_menuLogic->addOrEditMenu($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:删除菜单
     * @param Request $request
     */
    public function del(Request $request) {
        $res = $this->_menuLogic->delMenu($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月20日
     * description:菜单排序
     * @param Request $request
     */
    public function setListorder(Request $request) {
        switch ($request->method()){
            case 'POST':
                $data = $request->post();
                $this->_menuLogic->setListOrder($data);
                return $this->redirect('/admin/menu/index');
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}
