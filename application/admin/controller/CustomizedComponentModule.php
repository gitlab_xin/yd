<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:50
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedComponentLogic as ComponentLogic;

class CustomizedComponentModule extends Base implements ControllerInterface
{

    private $_componentLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_componentLogic = new ComponentLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年11月2日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_componentLogic->getAll(array_merge($data, ['type' => 'module']));

        $this->assign('lists', $lists);
        $this->assign('data', $data);
        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
        $this->assign('scheme_map', Config::get('customized.scheme_type'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年11月2日
     * description:详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
        $this->assign('info', $this->_componentLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年11月2日
     * description:添加颜色
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('action', Url::build('/admin/customized_component_module/add'));
                return $this->fetch('add');
            case 'POST':
                $data = $request->post();
                $data['type'] = 'module';

                $res = $this->_componentLogic->buildModuleData($data);
//                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                $this->redirect($res);
                break;
            default:
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年11月2日
     * description:编辑
     * @param Request $request
     */
    public function edit(Request $request)
    {
//        switch ($request->method()) {
//            case 'GET':
//                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
//                $this->assign('info', $this->_componentLogic->getDetail($request->get('id')));
//                $this->assign('action', Url::build('/admin/customized_component_module/edit'));
//                return $this->fetch('info');
//            case 'POST':
//                $data = $request->post();
//                $data['type'] = 'module';
//
//                $res = $this->_componentLogic->addOrEdit($data, $data['id']);
//                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
//                break;
//            default:
//                $this->error('无效的请求方式');
//                break;
//        }
        switch ($request->method()) {
            case 'GET':
//                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
//                $this->assign('info', $this->_componentLogic->getDetail($request->get('id')));
//                $this->assign('action', Url::build('/admin/customized_component_module/edit'));
//                return $this->fetch('info');
                $res = $this->_componentLogic->buildUrl($request->get('id'));
                $this->redirect($res);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年11月2日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_componentLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function buildProducts(Request $request)
    {
        $this->_componentLogic->buildProducts($request->get('id'));
    }

    public function buildProductsM(Request $request)
    {
        $this->_componentLogic->buildProductsModule($request->get('id'));
    }
}