<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/4
 * Time: 9:46
 */

namespace app\admin\controller;


use think\Controller;
use think\Request;
use think\Response;
use think\Config;
use app\common\validate\User as UserValidate;

class Validate extends Controller
{

    public function _initialize()
    {
        parent::_initialize();
        Config::set('default_return_type', 'json');
        Config::set('default_ajax_return', 'json');

        if (!method_exists($this, ($action = $this->request->action()))) {
            $res = $this->validateSceneData($action);
            throw new \think\exception\HttpResponseException(Response::create($res, 'json', 200));
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:验证config
     * @param Request $request
     * @return array
     */
    public function config(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        if (!isset($data['id'])) {
            $res['msg'] = '需要主键参数';
        } elseif (!isset($data['key'])) {
            $res['msg'] = '需要推荐模块参数';
        } else {
            $result = $this->validate($data, 'config.' . $data['key']);
            if (true !== $result) {
                // 验证失败 输出错误信息
                $res['msg'] = $result;
            } else {
                $res['code'] = 1;
                $res['msg'] = '验证通过';
            }
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:验证用户信息
     * @param Request $request
     * @return array
     */
    public function user(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $validate = new UserValidate();
        if (isset($data['id'])) {//编辑
            $validate->changeRule('add_mobile', $data['add_mobile'] . ",{$data['id']},user_id", true)
                ->changeRule('email', $data['email'] . ",{$data['id']},user_id", true)
                ->scene('edit');
        } else {//'添加
            $validate->changeRule('add_mobile', $data['add_mobile'], true)
                ->changeRule('email', $data['email'], true)->scene('add');
        }
        if (!$validate->check($data)) {
            // 验证失败 输出错误信息
            $res['msg'] = $validate->getError();
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:验证物流信息
     * @param Request $request
     * @return array
     */
    public function logisticsInfo(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'ShopOrderLogistics.add');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    public function refuseInfo(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'CustomizedOrder.refuse');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    public function refundInfo(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'CustomizedOrder.refund');
        $price = \app\common\model\CustomizedOrder::build()->where(['order_id' => $data['order_id']])->value('price');

        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } elseif ($price < $data['refund_number'] || 0 >= $data['refund_number']) {
            $res['msg'] = '退款金额只能在0～'.$price;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:物品验证
     * @param Request $request
     * @return array
     */
    public function customizedItem(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'CustomizedItem.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }


    /**
     * @author: Rudy
     * @time: 2017年9月21日
     * description:分类验证
     * @param Request $request
     * @return array
     */
    public function shopClassify(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'Classify.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:分类验证
     * @param Request $request
     * @return array
     */
    public function myHomeClassify(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'MyHomeFurnitureClassify.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:我的家 装修亮点
     * @param Request $request
     * @return array
     */
    public function myHomeDecoratePoint(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'MyHomeDecoratePoint.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:我的家 户型管理
     * @param Request $request
     * @return array
     */
    public function myHomeHouseType(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'MyHomeHouseType.insertOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月21日
     * description:消息
     * @param Request $request
     * @return array
     */
    public function message(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'MessageOfficial.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    public function customizedCabinetLight(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'CustomizedCabinetLight.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            switch ($data['type']) {
                case 'AmbientLight':
                    $result = $this->validate($data, 'CustomizedCabinetLightAmbinet.addOrEdit');
                    break;
                case 'DirectionalLight':
                    $result = $this->validate($data, 'CustomizedCabinetLightDirection.addOrEdit');
                    break;
                case 'PointLight':
                    $result = $this->validate($data, 'CustomizedCabinetLightPoint.addOrEdit');
                    break;
            }
            if (true !== $result) {
                // 验证失败 输出错误信息
                $res['msg'] = $result;
            } else {
                $res['code'] = 1;
                $res['msg'] = '验证通过';
            }
        }
        return $res;
    }

    public function customizedCabinetLightEdit(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();

        $result = '验证不通过';
        switch ($data['type']) {
            case 'AmbientLight':
                $result = $this->validate($data, 'CustomizedCabinetLightAmbinet.addOrEdit');
                break;
            case 'DirectionalLight':
                $result = $this->validate($data, 'CustomizedCabinetLightDirection.addOrEdit');
                break;
            case 'PointLight':
                $result = $this->validate($data, 'CustomizedCabinetLightPoint.addOrEdit');
                break;
        }
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }

        return $res;
    }

    public function customizedComponentModule(Request $request)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();
        $result = $this->validate($data, 'CustomizedComponent.module');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    private function validateSceneData($validator)
    {
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $this->request->param();
        $result = $this->validate($data, $validator . '.addOrEdit');
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }
        return $res;
    }

    public function product(Request $request){
        $res = ['code' => 0, 'msg' => '验证失败'];
        $data = $request->param();

        $result = '验证不通过';
        switch ($data['type']) {
            case '3':
                $result = $this->validate($data, 'MyDecorateProduct.add');
                break;
            case '2':
                $result = $this->validate($data, 'MyDecorateProduct.add_2');
                if (true !== $result){
                    break;
                }
//                foreach ($data['standard'] as $k => $v) {
//                    $result = $this->validate($v, 'MyDecorateProduct.standard');
//                    if (true !== $result) {
//                        $result = $v['color_name'].'的'.$result;
//                        break;
//                    }
//                }
                break;
        }
        if (true !== $result) {
            // 验证失败 输出错误信息
            $res['msg'] = $result;
        } else {
            $res['code'] = 1;
            $res['msg'] = '验证通过';
        }

        return $res;
    }

}