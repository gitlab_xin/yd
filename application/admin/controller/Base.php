<?php

/**
 * 后台基类
 * @file   Base.php
 * @date   2017-7-11 15:56
 * @author Rudy
 * @description  开源者对于该类原本作用定义是实现AOP效果，目前用改用tp的行为解决，该类留作补充
 */

namespace app\admin\controller;

use app\admin\traits\MyJump;
use think\Controller;
use think\Request;

class Base extends Controller
{
    use MyJump;
    //基础filter
    protected $_baseFilter = [
        'detail' => [
            'GET' => ['id']
        ],
        'edit' => [
            'GET' => ['id'],
            'POST' => ['id']
        ],
        'del' => [
            'GET' => ['id'],
        ],
    ];

    /**
     * Base constructor.
     * @param Request|null $request
     *
     */
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:初始化
     */
    public function _initialize()
    {
        parent::_initialize();
        //进行filter过滤
        $this->routeFilter();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:用于给子类添加规则
     * @return array
     */
    protected function addFilter()
    {
        return [];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:进行参数过滤
     */
    protected function routeFilter()
    {
        //获取请求实例化对象
        $request = Request::instance();
        $action = $request->action();
        $method = $request->method();
        $param = $request->{strtolower($method)}();
        //添加过滤规则,本类和基类的,合并数组
        $localFilter = $this->addFilter();
        $filter = array_merge($localFilter, $this->_baseFilter);

        //如果需要进行这个action的判断
        if (isset($filter[$action])) {
            //如果action在filter里面,且有对应的请求方式参数判断,则要开始验证规则
            if (isset($filter[$action][$method])) {
                //查看是否存在该参数
                foreach ($filter[$action][$method] as $key) {
                    if (!isset($param[$key])) {
//                        MyHook::remove('response_end','app\admin\behavior\Log');
                        $this->error('缺少参数' . $key);
                        return;
                    }
                }
            }
        }
    }

}
