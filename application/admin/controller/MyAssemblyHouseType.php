<?php

namespace app\admin\controller;

use app\admin\logic\MyAssemblyHouseTypeLogic as HouseLogic;
use think\Request;
use think\Url;
use think\Config;

/**
 * Class MyHomeClassify 我的家 户型管理
 * @package app\admin\controller
 */
class MyAssemblyHouseType extends Base
{
    private $_houseLogic;

    /**
     * @time: 2018年4月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_houseLogic = new HouseLogic();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 10:00 上午
     * description
     * @param Request $request
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_houseLogic->getAllHouse($data);
        $this->assign('lists', $lists);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 10:00 上午
     * description
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //添加户型时:get请求
                $this->assign('action', Url::build('/admin/my_assembly_house_type/add'));
                $data = $request->get();
                $this->assign('buildings_id', $data['buildings_id']);
                return $this->fetch('info');
            case 'POST':
                //添加户型时:post请求
                $res = $this->_houseLogic->addOrEditHouse($request->post(), 0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 10:00 上午
     * description
     * @param Request $request
     * @return mixed
     * @throws \think\exception\DbException
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //修改户型时:get请求
                $info = $this->_houseLogic->getHouse($request->get('id'));
                if ($info == null) {
                    $this->error('不存在该户型');
                }

                $this->assign('info', $info);
                $this->assign('buildings_id', $info['buildings_id']);
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('action', Url::build('/admin/my_assembly_house_type/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改户型时:post请求
                $data = $request->post();
                $res = $this->_houseLogic->addOrEditHouse($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/19
     * Time: 10:00 上午
     * description
     * @param Request $request
     * @throws \think\exception\DbException
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_houseLogic->delHouse($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
