<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\WebglModuleLogic;

class WebglModule extends Base implements ControllerInterface
{

    private $_webglModuleLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_webglModuleLogic = new WebglModuleLogic();
    }


    protected function addFilter()
    {
        return [
            'index' => [
                'GET' => ['scene_id']
            ],
            'add' => [
                'GET' => ['scene_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $scene_id = $request->get('scene_id');

        $lists = $this->_webglModuleLogic->getAll($scene_id);

        $this->assign('lists', $lists);
        $this->assign('scene_id', $scene_id);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $this->error('尚未开放该功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('scene_id', ($scene_id = $request->get('scene_id')));
                $this->assign('scene_name', $this->_webglModuleLogic->getSceneName($scene_id));
                $this->assign('action', Url::build('/admin/webgl_module/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_webglModuleLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        $this->error('尚未开放该功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_webglModuleLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}