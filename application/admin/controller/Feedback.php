<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/21
 * Time: 17:51
 */

namespace app\admin\controller;

use app\admin\logic\FeedbackLogic;
use think\Request;

class Feedback extends Base
{
    private $_feedbackLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_feedbackLogic = new FeedbackLogic();
    }

    public function addFilter()
    {
        return [
            'setStatus'=>[
                'GET'=>['id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:意见反馈的首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_feedbackLogic->getAll($data);

        $this->assign('data',$data);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:查看反馈详情,并设置成已读状态
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $id = $request->get('id');
        $info = $this->_feedbackLogic->getDetail($id);
        $this->_feedbackLogic->changeStatus($id,1);

        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:删除反馈信息
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_feedbackLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月22日
     * description:设置成未读状态
     * @param Request $request
     */
    public function setStatus(Request $request)
    {
        $res = $this->_feedbackLogic->setNotYetReadStatus($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}