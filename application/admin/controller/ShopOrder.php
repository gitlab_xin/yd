<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 14:31
 */

namespace app\admin\controller;

use app\admin\logic\ShopOrderLogic as OrderLogic;
use think\Request;
use think\Config;

class ShopOrder extends Base
{
    private $_orderLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_orderLogic = new OrderLogic();
    }

    public function addFilter()
    {
        return [
            'refund'=>[
                'GET'=>['id']
            ],
            'delivery'=>[
                'POST'=>['order_id']
            ],
            'address'=>[
                'GET'=>['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:订单展示页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_orderLogic->getOrders($data);
        $orderConfig = Config::get('mall.order');
        $logisticsConfig = Config::get('mall.logistics');

        $this->assign('payType',$orderConfig['pay_type']);
        $this->assign('status',$orderConfig['status']);
        $this->assign('logisticsType',$logisticsConfig['type']);
        $this->assign('lists',$lists);
        $this->assign('data',$data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:获取单个订单详情
     * @param Request $request
     * @return mixed
     *
     */
    public function detail(Request $request)
    {
        $info = $this->_orderLogic->getOrderDetail($request->get('id'));
        $this->assign('info',$info);
        return  $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:退款
     * @param Request $request
     */
    public function refund(Request $request)
    {
        $res = $this->_orderLogic->refund($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:修改状态到已发货
     * @param Request $request
     */
    public function delivery(Request $request)
    {
        $res = $this->_orderLogic->delivery($request->post());
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取收货地址
     * @param Request $request
     * @return mixed
     */
    public function address(Request $request)
    {
        $info = $this->_orderLogic->getAddress($request->get('id'));
        $this->assign('info',$info);
        return  $this->fetch();
    }
}