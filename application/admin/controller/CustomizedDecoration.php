<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedDecorationLogic as DecorationLogic;
use app\common\validate\CustomizedDecoration as DecorationValidate;

class CustomizedDecoration extends Base implements ControllerInterface
{

    private $_decorationLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_decorationLogic = new DecorationLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_decorationLogic->getAll();

        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $this->error('尚未开放该功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        $this->error('尚未开放该功能');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $model = new DecorationValidate();

                $this->assign('model', $model->getFieldsByScene('addOrEdit'));
                $this->assign('info',$this->_decorationLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/customized_decoration/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_decorationLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $this->error('尚未开放该功能');
    }
}