<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/12
 * Time: 11:50
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedWallColorLogic as WallColorLogic;
use app\common\validate\CustomizedWallColor as WallColorValidate;

class CustomizedWallColor extends Base implements ControllerInterface
{

    private $_wallColorLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_wallColorLogic = new WallColorLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_wallColorLogic->getAll();

        $this->assign('lists',$lists);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info',$this->_wallColorLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加颜色
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('model',(new WallColorValidate())->getFieldsByScene('addOrEdit'));
                $this->assign('action',Url::build('/admin/customized_wall_color/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_wallColorLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('model',(new WallColorValidate())->getFieldsByScene('addOrEdit'));
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('info',$this->_wallColorLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/customized_wall_color/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_wallColorLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_wallColorLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2018年1月8日
     * description:设置成默认墙体
     * @param Request $request
     */
    public function setDefault(Request $request)
    {
        $res = $this->_wallColorLogic->setDefault($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}