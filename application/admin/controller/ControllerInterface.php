<?php

/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 11:16
 */
namespace app\admin\controller;

use think\Request;

interface ControllerInterface
{
    public function index(Request $request);

    public function detail(Request $request);

    public function add(Request $request);

    public function edit(Request $request);

    public function del(Request $request);
}