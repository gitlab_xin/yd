<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeBuildingsFloorLogic as FloorLogic;
use think\Request;
use think\Url;

/**
 * Class MyHomeBuildings 我的家-层
 * @package app\admin\controller
 */
class MyHomeBuildingsFloor extends Base
{
    private $FloorLogic;

    /**
     * 初始化函数
     * MyHome constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->FloorLogic = new FloorLogic();
    }

    /**
     * @time: 2018年4月
     * description:层列表
     * @param $request
     * User: Airon
     * @return mixed
     */
    public function index(Request $request)
    {
        $id = $request->get('id');
        $buildings_id = $request->get('buildings_id');
        $lists = $this->FloorLogic->getList($buildings_id,$id);
        $this->assign('lists', $lists);
        $this->assign('buildings_id',$buildings_id);
        $this->assign('ridgepole_id',$id);
        return $this->fetch();
    }

    /**
     * @time: 2017年7月
     * description:添加层
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $id = $request->get('ridgepole_id');
                $buildings_id = $request->get('buildings_id');
                $this->assign('action',Url::build('/admin/my_home_buildings_floor/add?buildings_id='.$buildings_id.'&ridgepole_id='.$id));
                $this->assign('buildings_id',$buildings_id);
                $this->assign('ridgepole_id',$id);
                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $buildings_id = $request->post('buildings_id');
                $id = $request->post('ridgepole_id');
                $res = $this->FloorLogic->addOrEdit($request->post(),0);
                if(!empty($res['method'])&&$res['method']=='success'){
                    $this->success($res['msg'],"my_home_buildings_floor/add?buildings_id={$buildings_id}&ridgepole_id={$id}");
                }
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2017年7月
     * description:修改层
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $info = $this->FloorLogic->getInfo($request->get('id'));
                if($info == null){
                    return $this->error('不存在该层');
                }
                $this->assign('info',$info);
                $this->assign('buildings_id',$request->get('buildings_id'));
                $this->assign('ridgepole_id',$request->get('ridgepole_id'));
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/my_home_buildings_floor/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->FloorLogic->addOrEdit($data,$data['id']);
                $buildings_id = $request->post('buildings_id');
                $id = $request->post('ridgepole_id');
                if(!empty($res['method'])&&$res['method']=='success'){
                    $this->success($res['msg'],"my_home_buildings_floor/index?buildings_id={$buildings_id}&id={$id}");
                }
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2018年4月
     * description:删除层
     * User: Airon
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->FloorLogic->del($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
