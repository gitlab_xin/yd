<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/16
 * Time: 9:55
 */

namespace app\admin\controller;

use think\Config;
use think\Request;
use app\admin\logic\CustomizedOrderLogic as OrderLogic;

class CustomizedOrder extends Base
{

    private $_orderLogic;

    public function addFilter()
    {
        return [
            'refund' => [
                'GET' => ['id']
            ],
            'delivery' => [
                'GET' => ['id']
            ],
            'address' => [
                'GET' => ['id']
            ],
            'scheme' => [
                'GET' => ['id']
            ],
        ];
    }

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_orderLogic = new OrderLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_orderLogic->getOrders($data);

        $orderConfig = Config::get('mall.order');
        $logisticsConfig = Config::get('mall.logistics');

        $this->assign('logisticsType',$logisticsConfig['type']);
        $this->assign('payType', $orderConfig['pay_type']);
        $this->assign('status', $orderConfig['status']);
        $this->assign('lists', $lists);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_orderLogic->getOrderDetail($request->get('id'));
        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:查看订单收货地址
     * @param Request $request
     * @return mixed
     */
    public function address(Request $request)
    {
        $info = $this->_orderLogic->getAddress($request->get('id'));
        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:退款
     * @param Request $request
     */
    public function refund(Request $request)
    {
        $res = $this->_orderLogic->refund($request->post());
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * User: zhaoxin
     * Date: 2019/12/19
     * Time: 4:36 下午
     * description 拒绝退款理由
     * @param Request $request
     */
    public function refuse(Request $request){
        $res = $this->_orderLogic->refuse($request->post());
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:发货
     * @param Request $request
     */
    public function delivery(Request $request)
    {
        $res = $this->_orderLogic->delivery($request->post());
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月16日
     * description:查看订单方案
     * @param Request $request
     * @return mixed
     */
    public function scheme(Request $request)
    {
        $info = $this->_orderLogic->getScheme($request->get('id'));
        $this->assign('info', $info);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }


}