<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 15:27
 */

namespace app\admin\controller;

use think\Config;
use think\Request;
use think\Url;
use app\admin\logic\ShopPopularLogic as PopularLogic;
use app\admin\logic\ShopProductLogic;

class ShopPopular extends Base
{
    private $_popularLogic;
    private $_productLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_popularLogic = new PopularLogic();
        $this->_productLogic = new ShopProductLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_popularLogic->getPopulars();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**\
     * @author: Rudy
     * @time: 2017年7月19日
     * description:查看单个热门商品
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        $info = $this->_popularLogic->getPopular($request->get('id'));
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:添加数据
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        return $this->error('功能未开放');
//        switch ($request->method()){
//            case 'GET':
//                $this->assign('classify_one_ids',$this->_productLogic->getClassify());
//                $this->assign('action',Url::build('/admin/shop_popular/add'));
//                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
//                return $this->fetch('info');
//            case 'POST':
//                $data = $request->post();
//                $res = $this->_popularLogic->addOrEditPopular($data);
//                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
//                break;
//            default:
//                $this->error('无效的请求方式');
//                break;
//        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月21日
     * description:编辑
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_popularLogic->getPopular($request->get('id'));
                //商品分类
                $classify = $this->_productLogic->getAllClassifys([0=>'classify_one',
                    $info->product->classify_one_id => 'classify_two',
                    $info->product->classify_two_id => 'classify_three']);
                //获取三级分类下的所有商品
                $products = $this->_popularLogic->getProductByClassify($info->product->classify_three_id );

                $this->assign('classify_one_ids',$classify['classify_one']);
                $this->assign('classify_two_ids',$classify['classify_two']);
                $this->assign('classify_three_ids',$classify['classify_three']);
                $this->assign('products',$products);
                $this->assign('action',Url::build('/admin/shop_popular/edit'));
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('info',$info);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_popularLogic->addOrEditPopular($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }
    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:删除热门商品
     * @param Request $request
     */
    public function del(Request $request)
    {
        return $this->error('功能未开放');
//        $res = $this->_popularLogic->delPopular($request->get('id'));
//        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}