<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/19
 * Time: 10:36
 */

namespace app\admin\controller;


use think\Request;
use think\Url;
use app\admin\logic\ShopSupplierBadRecordLogic as BadRecord;

class ShopSupplierBadRecord extends Base
{
    private $_recordLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_recordLogic = new BadRecord();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月18日
     * description:首页,获取全部记录
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_recordLogic->getRecords();
        $this->assign('lists',$lists);
        return $this->fetch();
    }


    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取黑历史
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        $info = $this->_recordLogic->getRecord($request->get('id'));
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:添加黑历史
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $suppliers = $this->_recordLogic->getSuppliers();

                $this->assign('suppliers',$suppliers != null?$suppliers:[]);
                $this->assign('action',Url::build('/admin/shop_supplier_bad_record/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_recordLogic->addOrEditRecord($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求类型');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:编辑黑历史
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_recordLogic->getRecord($request->get('id'));
                $suppliers = $this->_recordLogic->getSuppliers();

                $this->assign('info',$info);
                $this->assign('suppliers',$suppliers != null?$suppliers:[]);
                $this->assign('action',Url::build('/admin/shop_supplier_bad_record/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_recordLogic->addOrEditRecord($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求类型');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:删除黑历史
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_recordLogic->delRecord($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}