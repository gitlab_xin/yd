<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/31
 * Time: 11:09
 */

namespace app\admin\controller;

use think\Request;
use think\Url;
use think\Config;
use app\admin\logic\ShopSupplierParamsLogic as ParamsLogic;

class ShopSupplierParams extends Base
{
    private $_paramsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_paramsLogic = new ParamsLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_paramsLogic->getParams();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月31日
     * description:编辑商家默认配置参数
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_paramsLogic->getParam($request->get('id'));

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/shop_supplier_params/edit'));

                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_paramsLogic->editParam($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}