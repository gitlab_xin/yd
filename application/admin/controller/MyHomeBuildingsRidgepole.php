<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeBuildingsRidgepoleLogic as RidgepoleLogic;
use think\Request;
use think\Url;

/**
 * Class MyHomeBuildings 我的家-栋
 * @package app\admin\controller
 */
class MyHomeBuildingsRidgepole extends Base
{
    private $ridgepoleLogic;

    /**
     * 初始化函数
     * MyHome constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->ridgepoleLogic = new RidgepoleLogic();
    }

    /**
     * @time: 2018年4月
     * description:栋列表
     * @param $request
     * User: Airon
     * @return mixed
     */
    public function index(Request $request)
    {
        $id = $request->get('id');
        $lists = $this->ridgepoleLogic->getList($id);
        $this->assign('lists', $lists);
        $this->assign('buildings_id',$id);
        return $this->fetch();
    }

    /**
     * @time: 2017年7月
     * description:添加栋
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $id = $request->get('buildings_id');
                $this->assign('action',Url::build('/admin/my_home_buildings_ridgepole/add?buildings_id='.$id));
                $this->assign('buildings_id',$id);
                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $id = $request->post('buildings_id');
                $res = $this->ridgepoleLogic->addOrEdit($request->post(),0);
                if(!empty($res['method'])&&$res['method']=='success'){
                    $this->success($res['msg'],"my_home_buildings_ridgepole/add?buildings_id={$id}");
                }
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2017年7月
     * description:修改栋
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $info = $this->ridgepoleLogic->getInfo($request->get('id'));
                if($info == null){
                    return $this->error('不存在该栋');
                }
                $this->assign('info',$info);
                $this->assign('buildings_id',$request->get('buildings_id'));
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/my_home_buildings_ridgepole/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $id = $request->post('buildings_id');
                $res = $this->ridgepoleLogic->addOrEdit($data,$data['id']);
                if(!empty($res['method'])&&$res['method']=='success'){
                    $this->success($res['msg'],"my_home_buildings_ridgepole/index?id={$id}");
                }
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2018年4月
     * description:删除栋
     * User: Airon
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->ridgepoleLogic->del($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
