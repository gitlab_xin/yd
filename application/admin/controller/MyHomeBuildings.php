<?php

namespace app\admin\controller;

use app\admin\logic\ConfigAreaLogic;
use app\admin\logic\MyHomeBuildingsLogic as BuildingsLogic;
use think\Request;
use think\Url;

/**
 * Class MyHomeBuildings 我的家-楼盘
 * @package app\admin\controller
 */
class MyHomeBuildings extends Base
{
    private $_buildingsLogic;
    private $_configAreaLogic;

    /**
     * 初始化函数
     * MyHome constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_buildingsLogic = new BuildingsLogic();
        $this->_configAreaLogic = new ConfigAreaLogic();
    }

    /**
     * @time: 2018年4月
     * description:楼盘列表
     * User: Airon
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_buildingsLogic->getList();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @time: 2017年7月
     * description:添加楼盘
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $this->assign('action',Url::build('/admin/my_home_buildings/add'));
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('province',$this->_configAreaLogic->getChildArea(0));
                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $res = $this->_buildingsLogic->addOrEdit($request->post(),0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2017年7月
     * description:修改楼盘
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $info = $this->_buildingsLogic->getInfo($request->get('id'));
                if($info == null){
                    return $this->error('不存在该分类');
                }
                $this->assign('info',$info);
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('province',$this->_configAreaLogic->getChildArea(0));
                $this->assign('city',$this->_configAreaLogic->getCity($info['province']));
                $this->assign('action',Url::build('/admin/my_home_buildings/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->_buildingsLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2018年4月
     * description:删除楼盘
     * User: Airon
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_buildingsLogic->del($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
