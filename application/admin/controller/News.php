<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/27
 * Time: 9:32
 */

namespace app\admin\controller;

use app\admin\logic\NewsLogic;
use think\Request;
use think\Config;
use think\Url;

class News extends Base
{
    private $_NewsLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_NewsLogic = new NewsLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:显示所有资讯消息(分页)
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        //验证搜索条件
        if(count($data)>0){
            $res = $this->validateData($request);
            if($res['code'] == 0){
                $this->error($res['msg']);
            }
        }

        $lists = $this->_NewsLogic->getAllNews($data);

        $this->assign('data',$data);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:显示单个具体资讯
     * @param Request $request
     * @return mixed
     *
     */
    public function detail(Request $request)
    {
        $info = $this->_NewsLogic->getNews($request->get('id'));

        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:添加资讯
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':

                $this->assign('action',Url::build('/admin/news/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_NewsLogic->addOrEditNews($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:编辑资讯
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_NewsLogic->getNews($request->get('id'));

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/news/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_NewsLogic->addOrEditNews($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:删除资讯
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_NewsLogic->delNews($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:验证资讯搜索条件
     * @param Request $request
     * @return array
     */
    public function validateData(Request $request)
    {
        $retArr = [];

        $data = $request->get();
        $res = $this->validate($data,'search.newsSearch');
        if($res === true){
            $retArr['msg'] = 'pass';
            $retArr['code'] = 1;
        }else{
            $retArr['msg'] = $res;
            $retArr['code'] = 0;
        }
        return $retArr;
    }
}