<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/3
 * Time: 17:15
 */

namespace app\admin\controller;

use think\Config as TpConfig;
use app\admin\logic\ConfigLogic;
use think\Request;
use think\Url;

class Config extends Base
{
    private $_configLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_configLogic = new ConfigLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:显示所有配置
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_configLogic->getConfigs();

        $this->assign('config',TpConfig::get('config.key'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:查看具体配置
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_configLogic->getConfig($request->get('id'));
        $tem = json_decode($info->value,true);
        unset($tem['create_time']);
        $info->value = $tem;

        $this->assign('info',$info);
        $this->assign('action',Url::build('/admin/config/edit'));
        $this->assign('imgPre',TpConfig::get('qiniu.BucketDomain'));
        $this->assign('inputType',TpConfig::get('config.input_type'));
        $this->assign('inputName',TpConfig::get('config.input_name'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:编辑配置
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_configLogic->getConfig($request->get('id'));
                $tem = json_decode($info->value,true);
                unset($tem['create_time']);
                $info->value = $tem;

                $this->assign('info',$info);
                $this->assign('action',Url::build('/admin/config/edit'));
                $this->assign('imgPre',TpConfig::get('qiniu.BucketDomain'));
                $this->assign('inputType',TpConfig::get('config.input_type'));
                $this->assign('inputName',TpConfig::get('config.input_name'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_configLogic->editConfig($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}