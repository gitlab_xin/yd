<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/23
 * Time: 17:57
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CooperativePartnerLogic as PartnerLogic;

class CooperativePartner extends Base implements ControllerInterface
{

    private $_partnerLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_partnerLogic = new PartnerLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_partnerLogic->getAll();

        $this->assign('lists',$lists);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info',$this->_partnerLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:添加颜色
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('action',Url::build('/admin/cooperative_partner/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_partnerLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('info',$this->_partnerLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/cooperative_partner/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_partnerLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_partnerLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}