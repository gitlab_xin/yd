<?php

/**
 *  
 * @file   Log.php  
 * @date   2016-10-9 18:23:24 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\controller;

use app\admin\logic\LogLogic;
use think\Request;

class Log extends Base {

    private $_logLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_logLogic = new LogLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:首页
     * @return mixed
     *
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_logLogic->getLogs($data);

        $this->assign('data',$data);
        $this->assign('lists', $lists);
        return $this->fetch();
    }

}
