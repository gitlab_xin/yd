<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/15
 * Time: 16:50
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedOtherChargeLogic as ChargeLogic;

class CustomizedOtherCharge extends Base implements ControllerInterface
{

    private $_chargeLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_chargeLogic = new ChargeLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月12日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lists = $this->_chargeLogic->getAll();

        $this->assign('scheme_type_map',Config::get('customized.scheme_type'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('scheme_type_map',Config::get('customized.scheme_type'));
        $this->assign('info',$this->_chargeLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        $this->error('功能未开放');
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('scheme_type_map',Config::get('customized.scheme_type'));
                $this->assign('info',$this->_chargeLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/customized_other_charge/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_chargeLogic->edit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $this->error('功能未开放');
    }
}