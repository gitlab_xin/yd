<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 14:38
 */

namespace app\admin\controller;


use app\admin\logic\ForumArticleLogic as ArticleLogic;
use think\Config;
use think\Request;
use think\Url;

class ForumArticle extends Base
{
    private $_articleLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_articleLogic = new ArticleLogic();
    }

    protected function addFilter()
    {
        return [
            'showReplys' => [
                'GET' => ['article_id']
            ],
            'delReply' => [
                'GET' => ['id']
            ],
            'revert' => [
                'GET' => ['id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:获取帖子详情(单个)
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_articleLogic->getArticle($request->get('id'));
        $info->classify = Config::get('bbs.classify')[$info->classify];

        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:显示帖子
     * @param Request $request
     * @param string $view
     * @param int $is_deleted
     * @return mixed
     */
    public function index(Request $request,$view = 'index',$is_deleted = 0)
    {
        $data = $request->get();

        //验证搜索条件
        if(count($data)>0){
            $res = $this->validateData($request);
            if($res['code'] == 0){
                $this->error($res['msg']);
            }
        }

        $lists = $this->_articleLogic->getArticles($data,$is_deleted);
        $classify = Config::get('bbs.classify');

        $this->assign('is_deleted',$is_deleted);
        $this->assign('data',$data);
        $this->assign('lists',$lists);
        $this->assign('classify',$classify);
        return $this->fetch($view);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:添加官方帖子
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('action',Url::build('/admin/forum_article/add'));
                $this->assign('classify',Config::get('bbs.classify'));
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();

                $res = $this->_articleLogic->addOrEditArticle($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:编辑帖子
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_articleLogic->getArticle($request->get('id'));
                $classify =  Config::get('bbs.classify');

                $this->assign('classify',$classify);
                $this->assign('action',Url::build('/admin/forum_article/edit'));
                $this->assign('info',$info);
                return $this->fetch('info');
            case 'POST';
                $data = $request->post();

                $res = $this->_articleLogic->addOrEditArticle($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:删除帖子
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $res = $this->_articleLogic->delOrRevertArticle($request->get('id'),1);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:显示单个帖子的所有回复
     * @param Request $request
     * @return mixed
     *
     */
    public function showReplys(Request $request)
    {
        $data = $request->get();
        $lists = $this->_articleLogic->getReplys($data);

        $this->assign('data',$data);
        $this->assign('article_id',$request->get('article_id'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:删除回复
     * @param Request $request
     * @return mixed
     */
    public function delReply(Request $request)
    {
        $res = $this->_articleLogic->delReply($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:显示已删除的帖子
     * @param Request $request
     * @return mixed
     */
    public function showDeleted(Request $request)
    {
        return $this->index($request,'index',1);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月25日
     * description:恢复已删除的帖子
     * @return mixed
     */
    public function revert(Request $request)
    {
        $res = $this->_articleLogic->delOrRevertArticle($request->get('id'),0);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:设置精华(加精和取消)
     * @param Request $request
     * @return mixed
     */
    public function setEssence(Request $request)
    {
        $id = $request->post('id');
        $res = $this->_articleLogic->changeStatus($id,'is_essence','加精');
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月27日
     * description:设置置顶(置顶和取消)
     * @param Request $request
     * @return mixed
     */
    public function setTop(Request $request)
    {
        $id = $request->post('id');
        $res = $this->_articleLogic->changeStatus($id,'is_top','置顶');
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月28日
     * description:验证帖子搜索条件
     * @param Request $request
     * @return array
     */
    public function validateData(Request $request)
    {
        $retArr = [];

        $data = $request->get();
        $res = $this->validate($data,'search.articleSearch');
        if($res === true){
            $retArr['msg'] = 'pass';
            $retArr['code'] = 1;
        }else{
            $retArr['msg'] = $res;
            $retArr['code'] = 0;
        }
        return $retArr;
    }
}