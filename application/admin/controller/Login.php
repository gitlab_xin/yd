<?php

namespace app\admin\controller;

use app\admin\logic\LoginLogic;
use think\Request;
use think\Url;

class Login extends Base
{

    private $_loginLogic;

    /**
     * @author: Rudy
     * @time: 2017年7月11日 16:59
     * @description:初始化loginLogic
     */
    public function __construct()
    {
        parent::__construct();
        $this->_loginLogic = new LoginLogic;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月11日 17:00
     * @description:登录页
     */
    public function index()
    {
        $res = $this->_loginLogic->loginAuth();
        if ($res != null) {
            //如果已经登录了
            $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
        } else {
            $this->view->engine->layout(false);
            $this->assign('action',Url::build('/admin/login/login'));
            return $this->fetch('login');
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:退出登录
     */
    public function logout()
    {
        $this->_loginLogic->cleanUserCache();
        $this->success('退出成功', Url::build('/admin/login/index'));
    }

    /**
     * @author: Rudy
     * @time: 2017年8月4日
     * description:登入
     * @param Request $request
     */
    public function login(Request $request)
    {
        switch ($request->method()){
            case 'POST':
                $data = $request->post();
                $res = $this->_loginLogic->loginPostAuth($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}
