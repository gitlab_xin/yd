<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 17:37
 */

namespace app\admin\controller;

use app\admin\logic\WorksLogic;
use think\Request;
use think\Config;

class Works extends Base
{
    private $_worksLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_worksLogic = new WorksLogic();
    }

    public function addFilter()
    {
        return [
            'reject' => [
                'POST' => ['id']
            ],
            'pass' => [
                'POST' => ['id']
            ],
            'showReplys' => [
                'GET' => ['works_id']
            ],
            'delReply' => [
                'GET' => ['id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:首页
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_worksLogic->getAllWorks($data);
        $worksConfig = Config::get('mall.works');

        $this->assign('typeMap', $worksConfig['type']);
        $this->assign('lists', $lists);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取单个案例
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_worksLogic->getWorks($request->get('id'));
        $info->scene_src = explode(':', $info->scene_src)[0];
        $info->scheme_src = explode(':', $info->scheme_src)[0];

        $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:拒绝
     * @param Request $request
     */
    public function reject(Request $request)
    {
        $data = $request->post();
        $res = $this->_worksLogic->changeType($data, $data['id'], 2);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:通过
     * @param Request $request
     */
    public function pass(Request $request)
    {
        $data = $request->post();
        $res = $this->_worksLogic->changeType($data, $data['id'], 1);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:显示单个帖子的所有回复
     * @param Request $request
     * @return mixed
     *
     */
    public function showReplys(Request $request)
    {
        $data = $request->get();
        $lists = $this->_worksLogic->getReplys($data);

        $this->assign('data', $data);
        $this->assign('works_id', $request->get('works_id'));
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月26日
     * description:删除回复
     * @param Request $request
     * @return mixed
     */
    public function delReply(Request $request)
    {
        $res = $this->_worksLogic->delReply($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:设置推荐(置顶和取消)
     * @param Request $request
     * @return mixed
     */
    public function setRecommended(Request $request)
    {
        $id = $request->post('id');
        $res = $this->_worksLogic->changeStatus($id, 'is_recommended', '推荐');
        return $res;
    }
}