<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\CustomizedCabinetLightGroupLogic as LightGroupLogic;

class CustomizedCabinetSceneLightGroup extends Base
{

    private $_lightGroupLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_lightGroupLogic = new LightGroupLogic();
    }

    protected function addFilter()
    {
        return [
            'index' => [
                'GET' => ['scene_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $scene_id = $request->get('scene_id');

        $lists = $this->_lightGroupLogic->getSceneGroup($scene_id);

        $this->assign('lists', $lists);
        $this->assign('scene_id', $scene_id);

        return $this->fetch();
    }

    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $data = $request->get();
                $groups = $this->_lightGroupLogic->getAll(false);

                $this->assign('scene_id', $data['scene_id']);
                $this->assign('light_groups', empty($groups) ? $groups : array_column($groups, 'name', 'light_group_id'));

                return $this->fetch();
            case 'POST':
                $data = $request->post();
                $res = $this->_lightGroupLogic->addLightGroup($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $res = $this->_lightGroupLogic->delScene($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    public function setDefault(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $data = $request->get();
                $res = $this->_lightGroupLogic->setDefault($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }
}