<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\CustomizedCabinetCameraLogic as CameraLogic;

class CustomizedCabinetSceneCamera extends Base
{

    private $_cameraLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_cameraLogic = new CameraLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['scene_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $scene_id = $request->get('scene_id');

        $lists = $this->_cameraLogic->getList();
        $seleted = $this->_cameraLogic->getSeleted($scene_id);

        $this->assign('lists', $lists);
        $this->assign('seleted', $seleted);
        $this->assign('scene_id',$scene_id);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        switch ($request->method()) {
            case 'POST':
                $data = $request->post();
                $res = $this->_cameraLogic->addCamera($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }


}