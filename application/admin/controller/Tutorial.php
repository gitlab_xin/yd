<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/3
 * Time: 10:01
 */

namespace app\admin\controller;

use app\admin\logic\TutorialLogic;
use think\Config;
use think\Request;
use think\Url;

class Tutorial extends Base
{
    private $_tutorialLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_tutorialLogic = new TutorialLogic;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:视频教程首页
     * @return mixed
     *
     */
    public function index()
    {
        $lists = $this->_tutorialLogic->getTutorials();

        $this->assign('lists',$lists);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:查看单个教程详情
     * @param Request $request
     * @return mixed
     *
     */
    public function detail(Request $request)
    {
        $info = $this->_tutorialLogic->getTutorial($request->get('id'));
        $this->assign('info',$info);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:添加教程
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':

                $this->assign('action',Url::build('/admin/tutorial/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_tutorialLogic->addOrEditTutorial($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:编辑教程
     * @param Request $request
     * @return mixed
     *
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $info = $this->_tutorialLogic->getTutorial($request->get('id'));

                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/tutorial/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_tutorialLogic->addOrEditTutorial($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月3日
     * description:删除教程
     * @param Request $request
     *
     */
    public function del(Request $request)
    {
        $res = $this->_tutorialLogic->delTutorial($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}