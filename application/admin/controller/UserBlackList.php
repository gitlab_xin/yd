<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/7
 * Time: 10:24
 */

namespace app\admin\controller;

use app\admin\logic\UserBlackListLogic as BlackListLogic;
use think\Request;
use think\Url;

class UserBlackList extends Base
{
    public $_blackList;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_blackList = new BlackListLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:黑名单首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_blackList->getBlackList();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:编辑黑名单
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $id = $request->get('id');
                $info = $this->_blackList->getDetail($id);
                $this->assign('action',Url::build('/admin/user_black_list/edit'));
                $this->assign('info',$info);
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();
                $res = $this->_blackList->editBlackList($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:删除黑名单
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_blackList->delBlackList($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}