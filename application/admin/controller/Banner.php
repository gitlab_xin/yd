<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 15:45
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\BannerLogic;
use think\Url;
use think\Config;

class Banner extends Base
{
    private $_bannerLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_bannerLogic = new BannerLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:首页,获取所有轮播图(按照device_type,module_type排序)
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_bannerLogic->getBannersGroup($data);
        if($lists != null){
            $bannerConfig = Config::get('mall.banner');
            $this->assign('device_type',$bannerConfig != null && isset($bannerConfig['device_type'])?$bannerConfig['device_type']:[]);
            $this->assign('module_type',$bannerConfig != null && isset($bannerConfig['module_type'])?$bannerConfig['module_type']:[]);
        }
        $this->assign('data',$data);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:添加轮播图
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        $bannerConfig = Config::get('mall.banner');

        switch ($request->method()){
            case 'GET':
                //get请求::直接展示表单

                $this->assign('action',Url::build('/admin/banner/add'));
                $this->assign('device_type',$bannerConfig != null && isset($bannerConfig['device_type'])?$bannerConfig['device_type']:[]);
                $this->assign('module_type',$bannerConfig != null && isset($bannerConfig['module_type'])?$bannerConfig['module_type']:[]);

                return $this->fetch('info');
            case 'POST':
                //post请求::处理数据
                $res = $this->_bannerLogic->addOrEditBanner($request->post(),isset($bannerConfig['max_pic'])?(int)$bannerConfig['max_pic']:5);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default;
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月14日
     * description:修改banner
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request)
    {
        $bannerConfig = Config::get('mall.banner');

        switch ($request->method()){
            case 'GET':
                //get请求::直接展示表单
                $info = $this->_bannerLogic->getBannerInfo($request->get('id'));
                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('action',Url::build('/admin/banner/edit'));
                $this->assign('device_type',$bannerConfig != null && isset($bannerConfig['device_type'])?$bannerConfig['device_type']:[]);
                $this->assign('module_type',$bannerConfig != null && isset($bannerConfig['module_type'])?$bannerConfig['module_type']:[]);

                return $this->fetch('info');
            case 'POST':
                //post请求::处理数据
                $res = $this->_bannerLogic->addOrEditBanner($request->post(),isset($bannerConfig['max_pic'])?(int)$bannerConfig['max_pic']:5 ,$request->post('id'));
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default;
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:删除banner
     * @param Request $request
     */
    public function del(Request $request)
    {
        //get请求::直接展示表单
        $res = $this->_bannerLogic->delBanner($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}