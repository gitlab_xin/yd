<?php

namespace app\admin\controller;

use app\admin\logic\MyAssemblyBuildingsLabelLogic as BuildingsLogic;
use think\Request;
use think\Url;

/**
 * Class MyAssemblyBuildingsLabel
 * @package app\admin\controller
 */
class MyAssemblyBuildingsLabel extends Base
{
    private $_buildingsLogic;

    /**
     * 初始化函数
     * MyHome constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_buildingsLogic = new BuildingsLogic();
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/10
     * Time: 4:41 下午
     * description
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        $lists = $this->_buildingsLogic->getList($data);
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/10
     * Time: 4:41 下午
     * description
     * @param Request $request
     */
    public function del(Request $request) {
        $res = $this->_buildingsLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * User: zhaoxin
     * Date: 2020/2/11
     * Time: 11:41 上午
     * description 异步修改
     * @param Request $request
     * @return bool
     */
    public function change(Request $request){
        $data = $request->post();
        $this->_buildingsLogic->change($data);
        return true;
    }

    public function change2(Request $request){
        $data = $request->post();
        $this->_buildingsLogic->change($data);
        $this->redirect(Url::build('/admin/my_assembly_buildings_label/index'));
    }

    public function add(Request $request){
        $data = $request->post();
        $this->_buildingsLogic->add($data);
        $this->redirect(Url::build('/admin/my_assembly_buildings_label/index'));
    }

}
