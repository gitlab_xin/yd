<?php

namespace app\admin\controller;

use app\admin\logic\MyHomeDecoratePointLogic;
use app\admin\logic\MyHomeHouseTypeLogic as HouseLogic;
use think\Request;
use think\Url;
use think\Config;

/**
 * Class MyHomeClassify 我的家 户型管理
 * @package app\admin\controller
 */
class MyHomeHouseType extends Base
{
    private $_houseLogic;

    /**
     * @time: 2018年4月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_houseLogic = new HouseLogic();
    }

    /**
     * @time: 2018年4月
     * description:展示所有户型
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_houseLogic->getAllHouse();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @time: 2018年4月
     * description:添加户型
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //添加户型时:get请求
                $DecoratePointLogic = new MyHomeDecoratePointLogic();
                $decorate = $DecoratePointLogic->getAllDecorate();
                $this->assign('all_decorate', $decorate);
                $this->assign('action', Url::build('/admin/my_home_house_type/add'));

                return $this->fetch('info');
            case 'POST':
                //添加户型时:post请求
                $res = $this->_houseLogic->addOrEditHouse($request->post(), 0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2018年4月
     * description:修改户型
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                //修改户型时:get请求
                $info = $this->_houseLogic->getHouse($request->get('id'));
                if ($info == null) {
                    $this->error('不存在该户型');
                }

                $decorate_list = $this->_houseLogic->getDecorate($request->get('id'));
                $DecoratePointLogic = new MyHomeDecoratePointLogic();
                $decorate = $DecoratePointLogic->getAllDecorate();
                $this->assign('all_decorate', $decorate);//后台所有装修亮点
                $this->assign('decorate_list', $decorate_list);//户型现有装修亮点
                $this->assign('info', $info);
                $this->assign('imgPre', Config::get('qiniu.BucketDomain'));
                $this->assign('action', Url::build('/admin/my_home_house_type/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改户型时:post请求
                $data = $request->post();
                $res = $this->_houseLogic->addOrEditHouse($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2018年4月
     * description:删除户型
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_houseLogic->delHouse($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
