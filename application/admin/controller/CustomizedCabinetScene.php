<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Request;
use app\admin\logic\CustomizedCabinetSceneLogic as SceneLogic;
use app\common\validate\CustomizedCabinetScene as SceneValidate;

class CustomizedCabinetScene extends Base implements ControllerInterface
{

    private $_sceneLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_sceneLogic = new SceneLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['cabinet_id']
            ],
            'add'=>[
                'GET'=>['cabinet_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $cabinet_id = $request->get('cabinet_id');

        $lists = $this->_sceneLogic->getAll($cabinet_id);

        $this->assign('lists', $lists);
        $this->assign('cabinet_id',$cabinet_id);

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $model = new SceneValidate();
        $this->assign('model',$model->getFieldsByScene('addOrEdit'));

        $this->assign('cabinet_id',$request->get('cabinet_id'));
        $this->assign('info', $this->_sceneLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $model = new SceneValidate();
                $this->assign('cabinet_id',$request->get('cabinet_id'));
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('action', Url::build('/admin/customized_cabinet_scene/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_sceneLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_sceneLogic->getDetail($request->get('id'));
                $model = new SceneValidate();

                $this->assign('cabinet_id',$info->cabinet_id);
                $this->assign('model',$model->getFieldsByScene('addOrEdit'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_cabinet_scene/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_sceneLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_sceneLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}