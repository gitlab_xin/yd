<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/17
 * Time: 9:56
 */

namespace app\admin\controller;

use app\admin\model\AdminMenu;
use think\Controller;
use think\Request;
use think\Config;
use app\admin\logic\ShopProductLogic as ProductLogic;
use app\admin\logic\ShopPopularLogic as PopularLogic;
use app\admin\logic\BaseLogic;
use app\common\model\Qiniu as QiniuModel;
use app\common\tools\kdniaoUtil;


class Api extends Controller
{

    public function _initialize()
    {
        parent::_initialize();
        Config::set('default_return_type', 'json');
        Config::set('default_ajax_return', 'json');
    }

    /**
     * @author: Rudy
     * @time: 2017年7月17日
     * description:获取分类
     * @param Request $request
     * @return \think\Response
     */
    public function getClassifyByParentId(Request $request)
    {
        $parent_id = $request->get('parent_id');
        $productLogic = new ProductLogic();
        $data = $productLogic->getClassify($parent_id);
        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:保存富文本的图片到云端
     * @param Request $request
     * @return \think\Response
     */
    public function saveEditorPicToCloud(Request $request)
    {
        $file = $request->file('file');

        if (empty($file)) {
            $data["code"] = "1";
            $data["msg"] = "请选择图片";
            $data['data']["src"] = '';
        } else {
            //添加验证
            if (!$file->validate(['ext' => 'gif,jpg,jpeg,bmp,png'])->check()) {
                $data["code"] = "2";
                $data["msg"] = $file->getError();
            } else {
                $logic = new BaseLogic();
                if ($res = $logic->saveFileToCloud('file')) {
                    $domain = Config::get('qiniu.BucketDomain');
                    $data["code"] = "0";
                    $data["msg"] = "上传成功";
                    $data['data']["src"] = $domain . $res;
                    $data['data']["origin"] = $res;
                    $data['data']["small"] = $domain . $res . '?imageView2/0/w/120/h/120';
                } else {
                    $data["code"] = "2";
                    $data["msg"] = "上传失败";
                }
            }
        }

        return $data;
    }

    public function saveSimditorEditorPicToCloud(Request $request)
    {
        $file = $request->file('file');

        if (empty($file)) {
            $data["success"] = false;
            $data["msg"] = "不存在文件";
            $data["file_path"] = "";
        } else {
            //添加验证
            if (!$file->validate(['ext' => 'gif,jpg,jpeg,bmp,png'])->check()) {
                $data["success"] = false;
                $data["msg"] = $file->getError();
                $data["file_path"] = '';
            } else {
                $logic = new BaseLogic();
                if ($res = $logic->saveFileToCloud('file')) {
                    $domain = Config::get('qiniu.BucketDomain');
                    $data["success"] = true;
                    $data["msg"] = '上传七牛成功';
                    $data['file_path'] = $domain . $res;
                } else {
                    $data["success"] = false;
                    $data["msg"] = '上传七牛失败';
                    $data["file_path"] = "";
                }
            }
        }

        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年7月19日
     * description:获取三级分类的商品
     * @param Request $request
     * @return \think\Response
     */
    public function getProductByClassifyId(Request $request)
    {
        $level_three = $request->get('classify_id');
        $popularLogic = new PopularLogic();
        $data = $popularLogic->getProductByClassify($level_three);

        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:获取地区
     * @param Request $request
     * @return mixed
     *
     */
    public function getArea(Request $request)
    {
        $parent_id = $request->get('parent_id');
        $productLogic = new ProductLogic();
        $data = $productLogic->getArea($parent_id);

        return $data;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月7日
     * description:获取七牛token
     * @return array
     */
    public function getToken()
    {
        $QiniuModel = new QiniuModel();
        $key = $this->request->get('key');
        $bucket = $QiniuModel->getToken($key != null ? $key : null);
        return ['uptoken' => $bucket];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:根据快递单号查询快递公司
     * @param Request $request
     * @return array|bool|string
     */
    public function searchLogistics(Request $request)
    {
        $return = ['ShipperCode' => '', 'ShipperName' => ''];
        $data = $request->get();
        $util = new kdniaoUtil();
        //只取一个
        $result = $util->getLogisticCompany($data['logistics_number']);
        if ($result !== false) {
            $return = $result[0];
        }
//        $resultData = $util->getOrderTracesByJson('JD','23686933366');
//        dump($resultData);exit;
        return $return;

    }

    public function changeLevel()
    {
        $level1 = AdminMenu::build()->where(['parent_id' => 0])->column('id');
        AdminMenu::update(['level' => 1], ['id' => ['in', $level1]]);

        $level2 = AdminMenu::build()->whereIn('parent_id', $level1)->column('id');
        AdminMenu::update(['level' => 2], ['id' => ['in', $level2]]);

        $level3 = AdminMenu::build()->whereIn('parent_id', $level2)->column('id');
        AdminMenu::update(['level' => 3], ['id' => ['in', $level3]]);
    }

    public function delCloudPic()
    {
        return (new QiniuModel())->delPic($this->request->get('key'));
    }
}