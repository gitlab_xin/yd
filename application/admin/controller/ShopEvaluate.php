<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/8
 * Time: 9:47
 */

namespace app\admin\controller;

use app\admin\logic\ShopEvaluateLogic as EvaluateLogic;
use think\Request;
use think\Config;

class ShopEvaluate extends Base
{
    private $_evaluateLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_evaluateLogic = new EvaluateLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:评论首页
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_evaluateLogic->getAllEvaluate($data);

        $this->assign('data',$data);
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:获取单个评论详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $info = $this->_evaluateLogic->getEvaluate($request->get('id'));
        $img_src_list = $info->img_src_list != null ? explode('|',$info->img_src_list):[];
        $imgPre = Config::get('qiniu.BucketDomain');
        array_walk($img_src_list,function(&$value) use($imgPre){
            list($img) = explode(':',$value);
            $value = $imgPre.$img;
        });
        $this->assign('img_src_list',$img_src_list);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月8日
     * description:删除单条评论
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_evaluateLogic->delEvaluate($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}