<?php

namespace app\admin\controller;

use app\admin\logic\ConfigAreaLogic;
use app\admin\logic\MyAssemblyBuildingsLogic as BuildingsLogic;
use think\Request;
use think\Url;

/**
 * Class MyHomeBuildings 易家-楼盘
 * @package app\admin\controller
 */
class MyAssemblyBuildings extends Base
{
    private $_buildingsLogic;
    private $_configAreaLogic;

    /**
     * 初始化函数
     * MyHome constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_buildingsLogic = new BuildingsLogic();
        $this->_configAreaLogic = new ConfigAreaLogic();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/17
     * Time: 3:13 下午
     * description
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();
        if (!isset($data['province'])){
            $data['province'] = '';
        }
        if (!isset($data['city'])){
            $data['city'] = '';
        }
        $lists = $this->_buildingsLogic->getList($data);
        $this->assign('public_path',PUBLIC_PATH);
        $this->assign('province',$this->_configAreaLogic->getChildArea(0));
        if(isset($data['province']) && !empty($data['province'])){
            $this->assign('city',$this->_configAreaLogic->getCity($data['province']));
        }
        $this->assign('lists', $lists);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * @time: 2017年7月
     * description:添加楼盘
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $this->assign('action',Url::build('/admin/my_assembly_buildings/add'));
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('province',$this->_configAreaLogic->getChildArea(0));
                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $res = $this->_buildingsLogic->addOrEdit($request->post(),0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @time: 2017年7月
     * description:修改楼盘
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $info = $this->_buildingsLogic->getInfo($request->get('id'));
                if($info == null){
                    return $this->error('不存在该分类');
                }
                $this->assign('info',$info);
                $this->assign('imgPre',\think\Config::get('qiniu.BucketDomain'));
                $this->assign('public_path',PUBLIC_PATH);
                $this->assign('province',$this->_configAreaLogic->getChildArea(0));
                $this->assign('city',$this->_configAreaLogic->getCity($info['province']));
                $this->assign('action',Url::build('/admin/my_assembly_buildings/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->_buildingsLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @time: 2018年4月
     * description:删除楼盘
     * User: Airon
     * @param Request $request
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_buildingsLogic->del($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
