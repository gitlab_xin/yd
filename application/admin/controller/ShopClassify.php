<?php

namespace app\admin\controller;

use app\admin\logic\ClassifyLogic;
use think\Request;
use think\Url;
use think\Config;
/**
 * Class ShopClassify
 * @package app\admin\controller
 */
class ShopClassify extends Base
{
    private $_classifyLogic;

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:初始化函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_classifyLogic = new ClassifyLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:展示所有分类
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_classifyLogic->getALLClassify();
        $this->assign('lists', $lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:添加分类
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //添加分类时:get请求
                $selectClassify = $this->_classifyLogic->getSelectClassify();

                $this->assign('selectMenu', $selectClassify);
                $this->assign('action',Url::build('/admin/shop_classify/add'));

                return $this->fetch('info');
            case 'POST':
                //添加分类时:post请求
                $res = $this->_classifyLogic->addOrEditClassify($request->post(),0);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:修改分类
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                //修改分类时:get请求
                $selectClassify = $this->_classifyLogic->getSelectClassify();
                $info = $this->_classifyLogic->getClassify($request->get('id'));
                if($info == null){
                    return $this->error('不存在该分类');
                }
                $this->assign('info',$info);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('selectMenu', $selectClassify);
                $this->assign('action',Url::build('/admin/shop_classify/edit'));

                return $this->fetch('info');
            case 'POST':
                //修改分类时:post请求
                $data = $request->post();
                $res = $this->_classifyLogic->addOrEditClassify($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月
     * description:删除分类
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $id = $request->get('id');
        $res = $this->_classifyLogic->delClassify($id);
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}
