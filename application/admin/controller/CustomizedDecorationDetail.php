<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\admin\controller;

use think\Url;
use think\Config;
use think\Request;
use app\admin\logic\CustomizedDecorationDetailLogic as DecorationDetailLogic;

class CustomizedDecorationDetail extends Base implements ControllerInterface
{

    private $_decorationDetailLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_decorationDetailLogic = new DecorationDetailLogic();
    }

    protected function addFilter()
    {
        return [
            'index'=>[
                'GET'=>['decoration_id']
            ],
            'add'=>[
                'GET'=>['decoration_id']
            ]
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:列表
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        $decoration_id = $request->get('decoration_id');

        $lists = $this->_decorationDetailLogic->getAll($decoration_id);

        $this->assign('lists', $lists);
        $this->assign('decoration_id',$decoration_id);
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:详情
     * @param Request $request
     */
    public function detail(Request $request)
    {
        $this->assign('decoration_id',$request->get('decoration_id'));
        $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
        $this->assign('info', $this->_decorationDetailLogic->getDetail($request->get('id')));

        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:添加
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $this->assign('item_position',Config::get('customized.item_position'));
                $this->assign('decoration_id',$request->get('decoration_id'));
                $this->assign('action', Url::build('/admin/customized_decoration_detail/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_decorationDetailLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $info = $this->_decorationDetailLogic->getDetail($request->get('id'));

                $this->assign('decoration_id',$info->decoration_id);
                $this->assign('imgPre',Config::get('qiniu.BucketDomain'));
                $this->assign('item_position',Config::get('customized.item_position'));
                $this->assign('info', $info);
                $this->assign('action', Url::build('/admin/customized_decoration_detail/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_decorationDetailLogic->addOrEdit($data, $data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_decorationDetailLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

}