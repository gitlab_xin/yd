<?php

/**
 *  
 * @file   Group.php  
 * @date   2016-9-1 15:11:41 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */

namespace app\admin\controller;

use think\Request;
use app\admin\logic\GroupLogic;

class Group extends Base {

    private $_groupLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_groupLogic = new GroupLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:管理组首页
     * @return mixed
     */
    public function index() {
        $res = $this->_groupLogic->getGroupInfos();
        $this->assign('lists', $res);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:管理组详情
     * @param Request $request
     * @return mixed|void
     */
    public function detail(Request $request)
    {
        if($id = $request->get('id')){
            $res = $this->_groupLogic->getGroupInfo($id);
            $this->assign('info',$res);
            return $this->fetch();
        }else{
            return $this->error('缺少参数');
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:添加管理组
     * @param Request $request
     * @return mixed|void
     */
    public function add(Request $request) {

        switch ($request->method()){
            case 'GET':
                return $this->fetch('info');
            case 'POST':
                $postData = $request->post();
                $res = $this->_groupLogic->addOrEditGroup($postData);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:修改管理组
     * @param Request $request
     * @return mixed|void
     */
    public function edit(Request $request) {

        switch ($request->method()){
            case 'GET':
                $res = $this->_groupLogic->getGroupInfo($request->get('id'));
                $this->assign('info',$res);
                return $this->fetch('info');
            case 'POST':
                $postData = $request->post();
                $res = $this->_groupLogic->addOrEditGroup($postData,$postData['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年7月15日
     * description:删除管理组
     * @param Request $request
     */
    public function del(Request $request)
    {

        $res = $this->_groupLogic->delGroup($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:权限管理
     * @param Request $request
     * @return mixed|void
     */
    public function editRule(Request $request)
    {

        switch ($request->method()){
            case 'GET':
                $rules = $this->_groupLogic->getRules($request->get('gid'));
                $menu = $this->_groupLogic->getMenus();

                $this->assign('rules',$rules['ids']);
                $this->assign('menu', list_to_tree($menu));
                return $this->fetch();
            case 'POST':
                $post = $request->post();
                $res = $this->_groupLogic->saveRules($post);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

}
