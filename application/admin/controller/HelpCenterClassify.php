<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 16:56
 */

namespace app\admin\controller;

use app\admin\logic\HelpCenterClassifyLogic;
use think\Request;
use think\Url;

class HelpCenterClassify extends Base
{
    private $_classifyLogic;

    public function __construct($request = null)
    {
        parent::__construct($request);
        $this->_classifyLogic = new HelpCenterClassifyLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:首页
     * @return mixed
     */
    public function index()
    {
        $lists = $this->_classifyLogic->getList();
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:添加帮助中心
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('action',Url::build('/admin/help_center_classify/add'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_classifyLogic->addOrEdit($data);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:编辑
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        switch ($request->method()){
            case 'GET':
                $this->assign('info',$this->_classifyLogic->getDetail($request->get('id')));
                $this->assign('action',Url::build('/admin/help_center_classify/edit'));
                return $this->fetch('info');
            case 'POST':
                $data = $request->post();

                $res = $this->_classifyLogic->addOrEdit($data,$data['id']);
                $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
                break;
            default:
                $this->error('无效的请求方式');
                break;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:查看详情
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        $this->assign('info',$this->_classifyLogic->getDetail($request->get('id')));
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:删除
     * @param Request $request
     */
    public function del(Request $request)
    {
        $res = $this->_classifyLogic->del($request->get('id'));
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}