<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 9:47
 */

namespace app\admin\controller;


use app\admin\logic\ActivityApplyLogic as ApplyLogic;
use think\Config;
use think\Request;

class ActivityApply extends Base
{
    private $_applyLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_applyLogic = new ApplyLogic();
    }

    protected function addFilter()
    {
        return [
            'pass' => [
                  'GET' => ['id']
            ],
            'stop' => [
                'GET' => ['id']
            ],
        ];
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:活动列表
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = $request->get();

        $lists = $this->_applyLogic->getApplications($data);
        $activities = $this->_applyLogic->getActivities();

        $this->assign('data',$data);
        $this->assign('activities',$activities);
        $this->assign('statusMap',Config::get('activity.apply_status'));
        $this->assign('lists',$lists);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:获取单个申请信息
     * @param Request $request
     * @return mixed
     *
     */
    public function detail(Request $request)
    {
        $info = $this->_applyLogic->getApplication($request->get('id'));
        switch ($info->organize_type){
            case 'school':
                $info['message'] = $info->school_name;
                break;
            case 'company':
                $info['message'] = $info->company_name.'--'.$info->company_position;
                break;
            default:
                break;
        }
        $info->organize_type = Config::get('activity.organize_type')[$info->organize_type];

        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:通过审核
     * @param Request $request
     */
    public function pass(Request $request)
    {
        $res = $this->_applyLogic->changeStatus($request->get('id'),'yes');
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }

    /**
     * @author: Rudy
     * @time: 2017年7月29日
     * description:拒绝通过
     * @param Request $request
     */
    public function stop(Request $request)
    {
        $res = $this->_applyLogic->changeStatus($request->get('id'),'no');
        $this->{$res['method']}($res['msg'], isset($res['redirect']) ? $res['redirect'] : null);
    }
}