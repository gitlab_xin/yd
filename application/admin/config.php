<?php

return [
    // 应用调试模式
    'app_debug' => true,
    'template' => [
        'layout_on' => true,
        'layout_name' => 'layout',
    ],
    'cookie'=> [
        // cookie 保存时间
        'expire'    => 24 * 60 * 60 * 7
    ],
    'app_trace' =>  true,
];
