<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\CustomizedScheme;
use app\common\model\ProductBean;
use app\common\model\SchemeQWBean as SchemeBean;
use app\common\tools\CommonMethod;

class SchemeQWLogic extends SchemeBaseLogic
{
    public $productLogic = null;

    public function getOriginParams($scheme)
    {
        $params = [];

        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';
        $params['width'] = isset($scheme['scheme_width']) ? $scheme['scheme_width'] : 0;
        $params['scheme_height'] = isset($scheme['scheme_height']) ? $scheme['scheme_height'] : 0;

        return $params;
    }

    public function buildScheme(&$schemeData)
    {

        $scheme = SchemeBean::build($schemeData);

        $this->addZDYYGDoor($scheme);
        $schemeData['scheme_door_schemes'] = $scheme->getScheme_door_schemes_array();

        return $scheme;
    }

    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_type' => 'QW',
            'scheme_type_text' => '墙外独立移门衣柜',
            'scheme_s_type' => -1,
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 600,
            'scheme_hole_type' => 0,
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => 600,
            'scheme_door_count' => $schemeData['scheme_door_count'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'is_left_wall' => $schemeBean->getIs_left_wall() ? '1' : '0',
            'is_right_wall' => $schemeBean->getIs_right_wall() ? '1' : '0',
            'is_left_wall_zt' => $schemeBean->getIs_left_wall_zt() ? '1' : '0',
            'is_right_wall_zt' => $schemeBean->getIs_right_wall_zt() ? '1' : '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];

        $model = CustomizedScheme::create($schemeModelData);
        return $model['id'];
    }

    public function update(SchemeBean $schemeBean, $schemeData, $schemeId, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_door_count' => $schemeData['scheme_door_count'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'is_left_wall' => $schemeBean->getIs_left_wall() ? '1' : '0',
            'is_right_wall' => $schemeBean->getIs_right_wall() ? '1' : '0',
            'is_left_wall_zt' => $schemeBean->getIs_left_wall_zt() ? '1' : '0',
            'is_right_wall_zt' => $schemeBean->getIs_right_wall_zt() ? '1' : '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];
        $model = new CustomizedScheme();
        $model->where(['id' => $schemeId])->update($schemeModelData);
        return;
    }

    public function checkScheme(SchemeBean $schemeBean)
    {
        $colsList = $schemeBean->getScheme_schemes();
        $schemeHeight = $schemeBean->getScheme_height();

        $colsCount = count($colsList);
        $pointsCount = intval(($schemeHeight - 35) / 32) + 1;
        $checkPointData = $this->createCheckPointData($colsCount, $pointsCount);

        $currentCol = 0;
        $checkResult = true;

        foreach ($colsList as $colScheme) {
            $colScheme = change2SchemeBean($colScheme);
            $schemeProductsList = $colScheme->getScheme_products();
            foreach ($schemeProductsList as $productBean) {
                $productBean = change2ProductBean($productBean);
                // 检查横隔板
//                if ($productBean->getProduct_type() == 'ZH') {
//                    $checkResult = $this->getProductLogic()
//                        ->checkZDYYG_HGB($productBean, $schemeHeight, $currentCol, $checkPointData);
//                    if (!$checkResult) {
//                        halt(1);
//                        break;
//                    }
//                }
                if ($productBean->getProduct_type() == 'ZH-YTG') {
                    $checkResult = $this->getProductLogic()
                        ->checkZDYYG_HGB_YTG($productBean, $schemeHeight, $currentCol, $checkPointData);
                    if (!$checkResult) {
                        halt($productBean);
                        halt(2);
                        break;
                    }
                }

                if (!empty($productBean->getCom_products_array()) || count($productBean->getCom_products_array()) != 0) {
                    foreach ($productBean->getCom_products() as $comProduct) {
                        $comProduct = change2ProductBean($comProduct);
                        if ($comProduct->getProduct_type() == 'L00') {
                            $checkResult = $this->getProductLogic()
                                ->checkZDYYG_LL($productBean, $schemeHeight, $currentCol, $checkPointData);
                            if (!$checkResult) {
                                halt(3);
                                break;
                            }
                        }
                        if ($comProduct->getProduct_type() == 'K') {
                            $checkResult = $this->getProductLogic()
                                ->checkZDYYG_KC($productBean, $schemeHeight, $currentCol, $checkPointData);
                            if (!$checkResult) {
                                halt(4);
                                break;
                            }
                        }
                    }
                }
            }
            if (!$checkResult) {
                break;
            }
            $currentCol++;
        }

        return $checkResult;
    }

    public function addZDYYGDoor(SchemeBean $schemeBean)
    {
        $holeWidth = $schemeBean->getScheme_hole_width();
        $holeSlHeight = $schemeBean->getScheme_hole_sl_height();
        $furnitureWidth = $schemeBean->getScheme_width();
        $furnitureHeight = $schemeBean->getScheme_height();
        $doorCount = $schemeBean->getScheme_door_count();


        $doorWidth = CommonMethod::getDoorWidth($holeWidth, $furnitureWidth, $doorCount, 30, false, false,
            $schemeBean->getIs_left_wall() || $schemeBean->getIs_left_wall_zt(),
            $schemeBean->getIs_right_wall() || $schemeBean->getIs_right_wall_zt()
        );
        $doorHeight = CommonMethod::getDoorHeight($holeSlHeight, $furnitureHeight, false, false);

//        $unitCount = intval($doorHeight / 300);
//        $unitType = 0;
//        $mod = $doorHeight % 300;
//        if ($mod >= 100) {
//            $unitType = 1;
//        }

        $schemeBean->setScheme_door_width($doorHeight - 42);
        $schemeBean->setScheme_door_width_one($doorWidth);
        $schemeBean->setScheme_door_height($doorHeight);

        $s_schemes = [];
        $schemeBean->setScheme_door_has_count($doorCount);

        foreach (range(0, $doorCount - 1) as $i) {
            $s_scheme = new SchemeBean();
            $s_scheme->setScheme_width($doorWidth);
            $s_scheme->setScheme_height($doorHeight);
            $s_scheme->setScheme_s_width(intval($doorWidth / 8));
            $s_scheme->setScheme_s_height(intval($doorHeight / 8));
            $s_scheme->setM_top_mm(66);
            $s_scheme->setM_left_mm(0);
            $s_scheme->setScheme_color_no($schemeBean->getScheme_color_no());
            $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeBean->getScheme_door_color_no()));
//            $pNo = $doorWidth . "-" . $doorCount . "-" . $unitType . $s_scheme->getScheme_color_no() . $i . "-" . getRandomInt();
//            ImageUtils.createYiDoor1(
//                path
//                + SchemeConstant.dcolor
//                + SchemeConstant.schemeColorArrH[Integer
//                .parseInt(s_scheme.getScheme_color_no())],
//                path + SchemeConstant.proPic + pNo + ".jpg",
//                s_scheme.getScheme_s_width(),
//                s_scheme.getScheme_s_height(), unitCount, mod);
//            $s_scheme->setScheme_pic($pNo . ".jpg");
            $s_schemes[] = $s_scheme;
        }
        $schemeBean->setScheme_door_schemes($s_schemes);
        $schemeBean->setScheme_have_door(1);
        $schemeBean->setScheme_door_color_no($schemeBean->getScheme_color_no());
    }

    /**
     * @param $data
     * @return SchemeBean
     */
    public function createQWScheme($data)
    {
        $index = base64_encode(http_build_query($data));
        $productList = $this->beforeCreate('qw', $index);
        if (!empty($productList)) {
            return $productList;
        }

        $scheme = new SchemeBean();
        $scheme->setScheme_width($data['width']);
        $scheme->setScheme_hole_width($data['width']);
        $scheme->setScheme_type('QW');
        $scheme->setScheme_height($data['height']);
        $scheme->setScheme_hole_height($data['height']);
        $scheme->setScheme_deep(600);

        $scheme->setScheme_color_no('001');

        2502 > $scheme->getScheme_height() ? $this->createZDYYGOut($scheme) : $this->createZDYYGOutDg($scheme);

        $productList = $scheme->getScheme_schemes();

        if (!empty($productList)) {
            array_walk($productList, function (&$row) use ($index) {
                $row->setSearch_index($index);
                $row = $row->mainInfo();
            });
            $this->afterCreate('qw', $productList);
        }

        return $productList;
    }

    public function createZDYYGOut(SchemeBean $scheme)
    {
        $width = $scheme->getScheme_width();
        $height = $scheme->getScheme_height();
        $n = intval(($width - 816 * 2 - SIDE_WIDTH_2 * 3) / (CUSTOM_COLUMN_WIDTH_STEP * 2));
        $width = $n * CUSTOM_COLUMN_WIDTH_STEP * 2 + 816 * 2 + SIDE_WIDTH_2 * 3;//1451~2475
        $n = intval(($height - 2019) / CUSTOM_COLUMN_HEIGHT_STEP);
        $height = $n * CUSTOM_COLUMN_HEIGHT_STEP + 2019;//2019~2403 2502~2886
        if ($height < 2502 && $height > 2403) {
            $height = 2403;
        }
        $scheme->setScheme_width($width);
        $scheme->setScheme_height($height);
        $colorNo = $scheme->getScheme_color_no();

        $scheme->setScheme_s_width($width);
        $scheme->setScheme_s_height($height);
        // 准备生成方案
        // 生成外侧板
        $wcbProducts = $this->calWcbProducts($height, $width, $colorNo);
        $scheme->setScheme_wcb_products($wcbProducts);
        $scheme->setScheme_wcb_type(0);
        // 生成内侧板
        $ncbProducts[] = $this->calNcbProducts($height, $width, $colorNo);
        $scheme->setScheme_ncb_products($ncbProducts);

        // 生成挡条
        $dtProducts = $this->calDtProducts($height, $width, $colorNo);
        $scheme->setScheme_dt_products($dtProducts);

        // 初始化方案
        // 创建方案
        $sWidth = intval(($width - 75) / 2);
        $sHeight = $height;
        $s_scheme = new SchemeBean();
        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
        $s_scheme->setScheme_width($width);
        $s_scheme->setScheme_height($height);
        $s_scheme->setScheme_deep($scheme->getScheme_deep());
        $s_scheme->setScheme_hole_width($width);
        $s_scheme->setScheme_hole_height($height);
        $s_scheme->setScheme_door_count(2);
        $s_scheme->setScheme_s_width($width);
        $s_scheme->setScheme_s_height($height);
        $s_scheme->setScheme_wcb_products($wcbProducts);
        $s_scheme->setScheme_ncb_products($ncbProducts);
        $s_scheme->setScheme_dt_products($dtProducts);
        $s_scheme->setScheme_sk_color_no($colorNo);
        $s_scheme->setScheme_color_no($colorNo);
        $s_scheme->setScheme_type($scheme->getScheme_type());
        $s_scheme->setScheme_error_range(32);

        // 第一列
        $t_scheme = [];
        $t_scheme = $this->calSchemeColsAndNcbProducts($sWidth, $sHeight, $colorNo);
        $t_scheme = [$t_scheme[0], $t_scheme[0]];//两列一致不再重复生成.
        $s_scheme->setScheme_schemes($t_scheme);
        // 方案图
//        $s_pic = $s_scheme->getScheme_width()
//            . "_" . $height
//            . "_" . $colorNo
//            . "_" . "0"
//            . "_" . "2"
//            . "_" . getRandomInt();
        $this->createSchemePic($s_scheme);
        $s_scheme->setScheme_name('墙外独立移门衣柜-' . getRandomInt());


        $s_schemes[] = $s_scheme;
        //第二个方案
        if ($width < 2475) {
            $width = $width + 64;
        } else {
            $width = $width - 64;//等于2475时已经为最大宽度 第二的方案只能为小一尺寸
        }
        // 生成外侧板
        $wcbProducts = $this->calWcbProducts($height, $width, $colorNo);
        // 生成内侧板
        $ncbProducts = [];
        $ncbProducts[] = $this->calNcbProducts($height, $width, $colorNo);

        // 生成挡条
        $dtProducts = $this->calDtProducts($height, $width, $colorNo);

        // 初始化方案
        // 创建方案
        $sWidth = intval(($width - 75) / 2);
        $sHeight = $height;
        $s_scheme = new SchemeBean();
        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
        $s_scheme->setScheme_width($width);
        $s_scheme->setScheme_height($height);
        $s_scheme->setScheme_hole_width($width);
        $s_scheme->setScheme_hole_height($height);
        $s_scheme->setScheme_hole_deep($scheme->getScheme_hole_deep());
        $s_scheme->setScheme_door_count(2);
        $s_scheme->setScheme_s_width($width);
        $s_scheme->setScheme_s_height($height);
        $s_scheme->setScheme_wcb_products($wcbProducts);
        $s_scheme->setScheme_ncb_products($ncbProducts);
        $s_scheme->setScheme_dt_products($dtProducts);
        $s_scheme->setScheme_sk_color_no($colorNo);
        $s_scheme->setScheme_color_no($colorNo);
        $s_scheme->setScheme_type($scheme->getScheme_type());
        $s_scheme->setScheme_error_range(32);

        // 第一列
        $t_scheme = $this->calSchemeColsAndNcbProducts($sWidth, $sHeight, $colorNo);
        $t_scheme = [$t_scheme[0], $t_scheme[0]];//两列一致不再重复生成.
        $s_scheme->setScheme_schemes($t_scheme);
        // 方案图
//        $s_pic = $s_scheme->getScheme_width()
//            . "_" . $height
//            . "_" . $colorNo
//            . "_" . getRandomInt();
        $this->createSchemePic($s_scheme);
        $s_scheme->setScheme_name('墙外独立移门衣柜-' . getRandomInt());
        $s_schemes[] = $s_scheme;
        $scheme->setScheme_schemes($s_schemes);
    }

    public function createZDYYGOutDg(SchemeBean $scheme)
    {
        $d_height = 483;
        $width = $scheme->getScheme_width();
        $height = $scheme->getScheme_height();
        $height = $height - 483;
        $n = intval(($width - 816 * 2 - SIDE_WIDTH_2 * 3) / (CUSTOM_COLUMN_WIDTH_STEP * 2));
        $width = $n * CUSTOM_COLUMN_WIDTH_STEP * 2 + 816 * 2 + SIDE_WIDTH_2 * 3;//1451~2475
        $n = intval(($height - 2019) / CUSTOM_COLUMN_HEIGHT_STEP);
        $height = $n * CUSTOM_COLUMN_HEIGHT_STEP + 2019;//2019~2403 2502~2886
        if ($height < 2502 && $height > 2403) {
            $height = 2403;
        }
        $scheme->setScheme_width($width);
        $scheme->setScheme_height($height);
        $scheme->setM_top($d_height / 8);

        $colorNo = $scheme->getScheme_color_no();

        $scheme->setScheme_s_width($width);
        $scheme->setScheme_s_height($height);
        // 准备生成方案
        // 生成外侧板
        $wcbProducts = $this->calWcbProducts($height, $width, $colorNo);
        $scheme->setScheme_wcb_products($wcbProducts);
        $scheme->setScheme_wcb_type(0);
        // 生成内侧板
        $ncbProducts[] = $this->calNcbProducts($height, $width, $colorNo);
        $scheme->setScheme_ncb_products($ncbProducts);

        // 生成挡条
        $dtProducts = $this->calDtProducts($height, $width, $colorNo);
        $scheme->setScheme_dt_products($dtProducts);

        // 初始化方案
        // 创建方案
        $sWidth = intval(($width - 75) / 2);
        $sHeight = $height;

        $dg_products = $this->calDgProducts($d_height, $width, $colorNo);

        $scheme->setScheme_dg_products($dg_products);

        $s_scheme = new SchemeBean();
        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
        $s_scheme->setScheme_width($width);
        $s_scheme->setScheme_height($height);
        $s_scheme->setScheme_deep($scheme->getScheme_deep());
        $s_scheme->setScheme_hole_width($width);
        $s_scheme->setScheme_hole_height($height);
        $s_scheme->setScheme_door_count(2);
        $s_scheme->setScheme_s_width($width);
        $s_scheme->setScheme_s_height($height);
        $s_scheme->setScheme_wcb_products($wcbProducts);
        $s_scheme->setScheme_ncb_products($ncbProducts);
        $s_scheme->setScheme_dt_products($dtProducts);
        $s_scheme->setScheme_dg_products($dg_products);
        $s_scheme->setScheme_sk_color_no($colorNo);
        $s_scheme->setScheme_color_no($colorNo);
        $s_scheme->setM_top(intval($d_height / 8));
        $s_scheme->setScheme_type($scheme->getScheme_type());
        $s_scheme->setScheme_error_range(32);

        // 第一列
        $t_scheme = [];
        $t_scheme = $this->calSchemeColsAndNcbProducts($sWidth, $sHeight, $colorNo);
        $t_scheme = [$t_scheme[0], $t_scheme[0]];//两列一致不再重复生成.
        $s_scheme->setScheme_schemes($t_scheme);
        // 方案图
//        $s_pic = $s_scheme->getScheme_width()
//            . "_" . $height
//            . "_" . $colorNo
//            . "_" . "0"
//            . "_" . "2"
//            . "_" . getRandomInt();
        $this->createSchemePic($s_scheme);
        $s_scheme->setScheme_name('墙外独立移门衣柜-' . getRandomInt());

        $s_schemes[] = $s_scheme;
        //第二个方案
        if ($width < 2475) {
            $width = $width + 64;
        } else {
            $width = $width - 64;//等于2475时已经为最大宽度 第二的方案只能为小一尺寸
        }
        // 生成外侧板
        $wcbProducts = $this->calWcbProducts($height, $width, $colorNo);
        // 生成内侧板
        $ncbProducts = [];
        $ncbProducts[] = $this->calNcbProducts($height, $width, $colorNo);

        // 生成挡条
        $dtProducts = $this->calDtProducts($height, $width, $colorNo);
        $scheme->setScheme_dt_products($dtProducts);

        $dg_products = $this->calDgProducts($d_height, $width, $colorNo);
        $scheme->setScheme_dg_products($dg_products);
        // 初始化方案
        // 创建方案
        $sWidth = intval(($width - 75) / 2);
        $sHeight = $height;
        $s_scheme = new SchemeBean();
        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
        $s_scheme->setScheme_width($width);
        $s_scheme->setScheme_height($height);
        $s_scheme->setScheme_deep($scheme->getScheme_deep());
        $s_scheme->setScheme_hole_width($width);
        $s_scheme->setScheme_hole_height($height);
        $s_scheme->setScheme_door_count(2);
        $s_scheme->setScheme_s_width($width);
        $s_scheme->setScheme_s_height($height);
        $s_scheme->setScheme_wcb_products($wcbProducts);
        $s_scheme->setScheme_ncb_products($ncbProducts);
        $s_scheme->setScheme_dt_products($dtProducts);
        $s_scheme->setScheme_dg_products($dg_products);
        $s_scheme->setScheme_sk_color_no($colorNo);
        $s_scheme->setM_top(intval($d_height / 8));
        $s_scheme->setScheme_color_no($colorNo);
        $s_scheme->setScheme_type($scheme->getScheme_type());
        $s_scheme->setScheme_error_range(32);
        // 第一列
        $t_scheme = $this->calSchemeColsAndNcbProducts($sWidth, $sHeight, $colorNo);
        $t_scheme = [$t_scheme[0], $t_scheme[0]];//两列一致不再重复生成.
        $s_scheme->setScheme_schemes($t_scheme);
        // 方案图
//        $s_pic = $s_scheme->getScheme_width()
//            . "_" . $height
//            . "_" . $colorNo
//            . "_" . getRandomInt();
        $this->createSchemePic($s_scheme);
        $s_scheme->setScheme_name('墙外独立移门衣柜-' . getRandomInt());

        $s_schemes[] = $s_scheme;
        $scheme->setScheme_schemes($s_schemes);
    }

    /**
     * 计算列方案和内侧板
     * @param $width
     * @param $height
     * @param $schemeColorNo
     * @return array
     */
    private function calSchemeColsAndNcbProducts($width, $height, $schemeColorNo)
    {
        $colSchemes = [];
        $s_products = [];

        // 背板

        // 背板
        if ($height >= 2403) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1169, 12, 1169, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1169, $width + 24, '000', 0);
        } elseif ($height >= 2371) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1169, 12, 1137, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1169, $width + 24, '000', 0);
        } elseif ($height >= 2339) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1137, 12, 1137, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1137, $width + 24, '000', 0);
        } elseif ($height >= 2307) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1137, 12, 1105, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1137, $width + 24, '000', 0);
        } elseif ($height >= 2275) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1105, 12, 1105, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1105, $width + 24, '000', 0);
        } elseif ($height >= 2243) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1105, 12, 1073, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1105, $width + 24, '000', 0);
        } elseif ($height >= 2211) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1073, 12, 1073, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1073, $width + 24, '000', 0);
        } elseif ($height >= 2179) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1073, 12, 1041, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1073, $width + 24, '000', 0);
        } elseif ($height >= 2147) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1041, 12, 1041, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1041, $width + 24, '000', 0);
        } elseif ($height >= 2115) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0,1041, 12, 1009, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1041, $width + 24, '000', 0);
        } elseif ($height >= 2083) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1009, 12, 1009, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1009, $width + 24, '000', 0);
        } elseif ($height >= 2051) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 1009, 12, 977, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 1009, $width + 24, '000', 0);
        } elseif ($height >= 2019) {
            // 下背板 全板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 977, 12, 977, $width + 24, '000', 0);
            // 上背板  裁切
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, 977, $width + 24, '000', 0);
        } else {
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, ($height - 1202), 12, 1201, $width + 24, "000", 0);
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_BB(0, 0, 0, 0, 12, $height - 1201 - 1, $width + 24, "000", 0);
        }

        //衣柜顶底板1
        $s_products[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD(0, $height, 0, 0, 25, 574, $width, "000", 0, 0, 1);
        // 衣柜顶底板2
        $s_products[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD(0, 66, 0, ($height - 66), 25, 574, $width, "000", 3, 0, 1);

        //衣通杆
        $s_products[] = $this->getProductLogic()
            ->createZDYYG_HGB_YTG(0, $height - 352, 0, 352, 25, 490, $width, '000', 1, 0);

        // 初始化该列方案
        $colScheme = new SchemeBean();
        $colScheme->setScheme_width($width);
        $colScheme->setScheme_height($height);
        $colScheme->setScheme_color_no($schemeColorNo);
        $colScheme->setScheme_products($s_products);
        $colSchemes[] = $colScheme;


        return $colSchemes;
    }

    /**
     * 生成背板
     * @param $height string 柜体高度
     * @param $width string 柜体宽度
     * @return array
     */
    public function calBBProduct($height, $width)
    {
        $BBProduct = [];
        $BBProduct[] = $this->getProductLogic()
            ->createZDYYG_BB(0, 0, 0, ($height - 1202), 12, 1201, $width, "000", 0);
        $BBProduct[] = $this->getProductLogic()
            ->createZDYYG_BB(0, 0, 0, 0, 12, $height - 1201 - 1, $width, "000", 0);
        return $BBProduct;
    }

    /**
     * 生成外侧板
     * @param $height string 柜体高度
     * @param $width string 柜体宽度
     * @param $schemeColorNo string 柜体花色号码
     * @return array
     */
    private function calWcbProducts($height, $width, $schemeColorNo)
    {
        $wcbProducts = [];
        // 左外侧板
        $wcbProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB_YM(0, $height, 0, 0, SIDE_WIDTH_2, $height, 603, $schemeColorNo, 0);
        // 右外侧板
        $wcbProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB_YM(0, $height, ($width - SIDE_WIDTH_2), 0, SIDE_WIDTH_2, $height, 603, $schemeColorNo, 0);

        return $wcbProducts;
    }

//    /**
//     * 衣柜顶底板
//     * @param $height string 柜体高度
//     * @param $width string 柜体宽度
//     * @param $schemeColorNo string 柜体花色号码
//     * @return array
//     */
//    private function calHgbGdDdProducts($height, $width, $schemeColorNo)
//    {
//        $HgbGdDdProducts = [];
//        //衣柜顶底板1
//        $HgbGdDdProducts[] = $this->getProductLogic()
//            ->createZDYYG_HGB_GD_DD(0, $height, 0, 0, 25, 574, $width, $schemeColorNo, 0, 0, 1));
//        // 衣柜顶底板2
//        $HgbGdDdProducts[] = $this->getProductLogic()
//            ->createZDYYG_HGB_GD_DD(0, 66, 0, ($height - 66), 25, 574, $width, $schemeColorNo, 3, 0, 1));
//        return $HgbGdDdProducts;
//    }
//
    /**
     *   生成顶柜
     * @param $height string 柜体高度
     * @param $width string 顶柜宽度
     * @param $schemeColorNo string 柜体花色号码
     * @return array
     */
    private function calDgProducts($height, $width, $schemeColorNo)
    {
        $sWidth = intval(($width - 75) / 2);
        $cha = 0;
        if (0 != (($sWidth - 4) / 2 - 342) % 32) {
            $cha = 16;
        }
        $width1 = ($sWidth - 4) / 2 - $cha;
        $width2 = ($sWidth - 4) / 2 + $cha;

        $dgProducts = [];
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB_DG(0, $height, 0, 0, 25, $height, 603, $schemeColorNo, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB_DG(0, $height, ($width - 25), 0, 25, $height, 603, $schemeColorNo, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_NCB_DG(0, $height, intval(($width - 75) / 2) + 25, 0, 25, $height, 603, $schemeColorNo, 0);
        // 顶柜背板
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_BB_DG(0, 0, 25, 0, 5, 482, $sWidth, $schemeColorNo, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_BB_DG(0, 0, $sWidth + 50, 0, 5, 482, $sWidth, $schemeColorNo, 0);

        // 顶柜顶底板
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD(25, 0, 25, 0, 25, 574, $sWidth, '000', 0, 0, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD(25, $height - 25, 25, $height - 25, 25, 574, $sWidth, '000', 0, 0, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD($sWidth + 50, 0, $sWidth + 50, 0, 25, 574, $sWidth, '000', 0, 0, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_HGB_GD_DD($sWidth + 50, $height - 25, $sWidth + 50, $height - 25, 25, 574, $sWidth, '000', 0, 0, 0);

        // 顶柜门板
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_MB_DG(25, 0, 25 + 1, 3.5, 16, 476, $width1, $schemeColorNo, 0, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_MB_DG($width1 + 25, 0, $width1 + 25 + 2, 3.5, 16, 476, $width2, $schemeColorNo, 0, 1);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_MB_DG($sWidth + 50, 0, $sWidth + 50 + 1, 3.5, 16, 476, $width1, $schemeColorNo, 0, 0);
        $dgProducts[] = $this->getProductLogic()
            ->createZDYYG_MB_DG($sWidth + 50 + $width1, 0, $sWidth + 50 + $width1 + 2, 3.5, 16, 476, $width2, $schemeColorNo, 0, 1);
        return $dgProducts;
    }

    /**
     * 生成内侧板
     * @param $height
     * @param $width
     * @param $schemeColorNo
     * @return ProductBean
     */
    private function calNcbProducts($height, $width, $schemeColorNo)
    {
        $ncbProducts = $this->getProductLogic()
            ->createZDYYG_NCB(0, $height, (($width - 75) / 2 + 25), 0, 25, $height, 490, '000', 0);

        return $ncbProducts;
    }

    /**
     * 生成挡条
     * @param $height
     * @param $width
     * @param $schemeColorNo
     * @return array
     */
    private function calDtProducts($height, $width, $schemeColorNo)
    {
        $dtProducts = [];
        $dtProducts[] = $this->getProductLogic()
            ->createZDYYG_DT(25, 0, 25, 0, 16, $width - 50, 66, $schemeColorNo, 0);
        $dtProducts[] = $this->getProductLogic()
            ->createZDYYG_DT(25, 0, 25, ($height - 66), 16, $width - 50, 66, $schemeColorNo, 0);

        return $dtProducts;
    }

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    private function createCheckPointData($colsCount, $pointsCount)
    {
        $checkPointData = range(0, $colsCount - 1);
        foreach ($checkPointData as &$colPointData) {
            $colPointData = range(0, $pointsCount - 1);
            foreach ($colPointData as &$pointInfo) {
                $pointInfo = [
                    'is_use' => 0,
                    'can_use' => 1,
                ];
            }
        }

        return $checkPointData;
    }

}
