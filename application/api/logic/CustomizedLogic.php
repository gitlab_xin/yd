<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:19
 */

namespace app\api\logic;

use app\common\model\CustomizedCabinetModel;
use app\common\model\CustomizedColor;
use app\common\model\CustomizedComponent;
use app\common\model\MyHomeHouseType;
use app\common\model\WorksSchemeDetail;
use app\common\model\Works;
use app\common\model\CustomizedCabinet as CabinetModel;
use app\common\model\CustomizedCabinetDoorColor as CabinetDoorModel;
use app\common\model\CustomizedCabinetEnvironment as CabinetEnvironmentModel;
use app\common\model\CustomizedCabinetTexture as CabinetTextureModel;
use app\common\model\CustomizedCabinetModel as CabinetModelModel;
use app\common\model\CustomizedCabinetScene as CabinetSceneModel;
use app\common\model\CustomizedCabinetLightGroupMap as CabinetLightGroupMapModel;
use app\common\model\CustomizedCabinetSceneLightGroup as CabinetSceneLightGroupModel;
use app\common\model\CustomizedCabinetLightAmbinet as AmbinetLightModel;
use app\common\model\CustomizedCabinetLightDirection as DirectionLightModel;
use app\common\model\CustomizedCabinetLightPoint as PointLightModel;

use app\common\model\CustomizedCabinetLightHemisphere as HemisphereLightModel;
use app\common\model\CustomizedCabinetLightSpot as SpotLightModel;
use app\common\model\CustomizedCabinetLightRectArea as RectAreaLightModel;
use app\common\model\CustomizedCabinetSceneCamera as CabinetSceneCameraModel;
use app\common\model\CustomizedCabinetSceneControl as CabinetSceneControlModel;
use app\common\model\CustomizedCabinetSceneRender as CabinetSceneRenderModel;
use app\common\tools\RedisUtil;

class CustomizedLogic
{
    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:获取推荐方案
     * @param $data
     * @return array|bool
     */
    public static function getRecommend($data)
    {
        try {
            $where['work.type'] = 1;
            $where['detail.scheme_type'] = $data['scheme_type'];
            $order = ["order_key" => "DESC"];
            switch ($data['scheme_type']) {
                case "RQS":
                    $where['detail.scheme_hole_sl_height'] = ['between', [$data['sl_height'] - 96, $data['hole_height']]];
                    $where['detail.scheme_width'] = $data['width'];//宽度
                    $where['detail.scheme_height'] = $data['height'];//高度
                    break;
                case "BZ":
                    $where['detail.scheme_width'] = ['between', [$data['width'] - 150, $data['width'] + 150]];//宽度误差150
                    $where['detail.scheme_height'] = $data['height'];//高度相等
                    if ($data['height'] == 2019) {//如果是写字桌，则写字桌类型相等
                        $where['detail.scheme_s_type'] = $data['scheme_s_type'];
                    }
                    break;
                case "QW":
                    $where['detail.scheme_width'] = ['between', [$data['width'] - 150, $data['width'] + 150]];//宽度误差150
                    $n = intval(($data['width'] - 816 * 2 - SIDE_WIDTH_2 * 3) / (CUSTOM_COLUMN_WIDTH_STEP * 2));//高度误差
                    if ($data['height'] <= 2403) {
                        $where['detail.scheme_height'] = ['between', [$data['height'] - $n * CUSTOM_COLUMN_HEIGHT_STEP, $data['height']]];
                    } else {
                        $where['detail.scheme_height'] = ['between', $data['height'], [$data['height'] + $n * CUSTOM_COLUMN_HEIGHT_STEP]];
                    }

                    break;
                case "ZH":
                    $where['detail.scheme_width'] = ['between', [$data['width'] - $data['range'], $data['width'] + $data['range']]];//宽度误差150
                    $where['detail.scheme_b_type'] = $data['scheme_b_type'];//子类型相等
                    switch ($data['scheme_b_type']) {//电视柜和写字桌类型相等
                        case 'YYST':
                        case 'SJSC':
                            $where['detail.scheme_s_type'] = $data['scheme_s_type'];
                            break;
                        default:
                            break;
                    }
            }
            $result = Works::build()->alias("work")->join("yd_works_scheme_detail detail", "work.scheme_id = detail.scheme_detail_id")
                ->where($where)->field(['work.works_id', 'scheme_pic', '(work.view_count*0.2+work.praise_count*0.2+work.collect_count*0.3 + work.comment_count*0.3) as order_key'])->order($order)->page(1, 10)->select();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getRecommendInfo($scheme_detail_id)
    {
        return WorksSchemeDetail::build()->where(['scheme_detail_id' => $scheme_detail_id])->field('serial_array')->find();
    }

    public static function getColorByCabinetType($type)
    {
        try {
            return CabinetModel::build()->alias('ca')->where(['ca.type' => $type, 'co.is_deleted' => 0])
                ->field(['co.color_id', 'co.color_no', 'co.color_name', 'co.border_color', 'co.handle_color', 'co.alloy_color', 'co.material_color',
                    'co.door_border_color', 'co.door_alloy_color', 'co.color_level', 'co.is_monochrome', 'co.origin_img_src', 'co.img_src_row_light', 'co.img_src_row_dark', 'co.is_show','co.material'])
                ->join('customized_cabinet_color cc', 'cc.cabinet_id = ca.cabinet_id', 'LEFT')
                ->join('customized_color co', 'cc.color_id = co.color_id', 'LEFT')
                ->order('co.is_default ASC,co.color_no ASC')
                ->select()
                ->toArray();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getDoorColorByCabinetType($type, $color_no)
    {
        try {
            $cabinet_color_id = CabinetModel::build()->alias('ca')->where(['ca.type' => $type, 'co.color_no' => $color_no, 'co.is_deleted' => 0])
                ->join('customized_cabinet_color cc', 'cc.cabinet_id = ca.cabinet_id', 'LEFT')
                ->join('customized_color co', 'cc.color_id = co.color_id', 'LEFT')
                ->value('cc.cabinet_color_id');
            return $cabinet_color_id > 0 ? CabinetDoorModel::build()->alias('cd')
                ->field(['co.color_id', 'co.color_no', 'co.color_name', 'co.border_color', 'co.handle_color', 'co.alloy_color', 'co.material_color',
                    'co.door_border_color', 'co.door_alloy_color', 'co.color_level', 'co.is_monochrome', 'co.origin_img_src', 'co.img_src_row_light', 'co.img_src_row_dark', 'co.is_show','co.material'])
                ->where(['cd.cabinet_color_id' => $cabinet_color_id, 'co.is_deleted' => 0])
                ->join('customized_color co', 'cd.color_id = co.color_id', 'LEFT')
                ->order('co.is_default ASC,co.color_no ASC')
                ->select()->toArray() : [];
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getColorDetail($color_no)
    {
        try {
            return CustomizedColor::build()
                ->field(['color_id', 'color_no', 'color_name', 'border_color', 'handle_color', 'alloy_color', 'material_color', 'door_border_color',
                    'door_alloy_color', 'color_level', 'is_monochrome', 'origin_img_src', 'img_src_row_light', 'img_src_row_dark', 'is_show','material'])
                ->where(['color_no' => $color_no, 'is_deleted' => 0])
                ->find();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getColorDetailById($color_id)
    {
        try {
            return CustomizedColor::build()
                ->field(['color_id', 'color_no', 'color_name', 'border_color', 'handle_color', 'alloy_color', 'material_color', 'door_border_color',
                    'door_alloy_color', 'color_level', 'is_monochrome', 'origin_img_src', 'img_src_row_light', 'img_src_row_dark', 'is_show','material'])
                ->where(['color_id' => $color_id, 'is_deleted' => 0])
                ->find();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function setColorMaterial($color_id,$material)
    {
        try {
            return CustomizedColor::build()
                ->where(['color_id' => $color_id, 'is_deleted' => 0])
                ->update(['material'=>json_encode($material,JSON_UNESCAPED_UNICODE)]);
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function get3DByCabinet($scheme_type)
    {
        $cabinet_id = CabinetModel::build()->where(['type' => $scheme_type])->value('cabinet_id');

        if ($cabinet_id === null) {
            return $cabinet_id;
        }
        $scene = CabinetSceneModel::build()->field(['scene_id', 'size_x', 'size_y', 'size_z'])->where(['cabinet_id' => $cabinet_id])->find();
        if ($scene === null) {
            return $scene;
        }
        $scene = $scene->toArray();
        $scene_id = array_shift($scene);

        $env_pics = CabinetEnvironmentModel::build()->field(['environment_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $texture_pics = CabinetTextureModel::build()->field(['texture_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $models = CabinetModelModel::build()->field(['model_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $light_groups = CabinetSceneLightGroupModel::build()->alias('slg')->field(['clg.light_group_id', 'clg.name', 'slg.is_default', 'slg.type'])
            ->where(['slg.scene_id' => $scene_id])->join('customized_cabinet_light_group clg', 'clg.light_group_id = slg.light_group_id')->select()->toArray();
//        $light = CabinetSceneLightModel::build()->alias('csl')->field(['cl.light_id', 'cl.name', 'cl.type'])
//            ->where(['csl.scene_id' => $scene_id])->join('customized_cabinet_light cl', 'cl.light_id = csl.light_id')->select()->toArray();
        $camera = CabinetSceneCameraModel::build()->alias('csc')->field(['cc.name', 'cc.type', 'cc.is_default', 'cc.param', 'cc.position', 'cc.lookAt'])
            ->where(['csc.scene_id' => $scene_id])->join('customized_cabinet_camera cc', 'csc.camera_id = cc.camera_id')->select()->toArray();
        $control = CabinetSceneControlModel::build()->alias('csc')->field(['cc.name', 'cc.type', 'cc.camera', 'cc.render', 'cc.open', 'cc.enable'])
            ->where(['csc.scene_id' => $scene_id])->join('customized_cabinet_control cc', 'csc.control_id = cc.control_id')->select()->toArray();
        $render = CabinetSceneRenderModel::build()->alias('csc')
            ->field(['cc.name', 'cc.shadowMapType', 'cc.toneMapping', 'cc.physicallyCorrectLights', 'cc.gammaInput', 'cc.gammaOutput',
                'cc.shadowMapSoft', 'cc.shadowMapEnabled', 'cc.envMap', 'cc.size', 'cc.clearColor'])
            ->where(['csc.scene_id' => $scene_id])->join('customized_cabinet_render cc', 'csc.render_id = cc.render_id')->select()->toArray();
        if (!empty($models)) {//模型
            array_walk($models, function (&$v) {
                $v['scale'] = json_decode($v['scale']);
                $v['rotation'] = json_decode($v['rotation']);
                $v['position'] = json_decode($v['position']);
                $v['size'] = json_decode($v['size']);
                $v['objName'] = $v['objName'] . '?v=' . rand();
                $v['mtlName'] = $v['mtlName'] . '?v=' . rand();
                $v['pics'] = $v['pics'] == '' ? [] : explode(',', $v['pics']);
            });
            unset($v);
        }
        $groups = [];
        if (!empty($light_groups)) {
            foreach ($light_groups as $light_group) {
                $group['isDefault'] = $light_group['is_default'];
                $group['type'] = $light_group['type'];
                $light = CabinetLightGroupMapModel::build()->alias('m')->field(['cl.light_id', 'cl.name', 'cl.type'])
                    ->where(['m.light_group_id' => $light_group['light_group_id']])
                    ->join('customized_cabinet_light cl', 'cl.light_id = m.light_id')->select()->toArray();
                if (!empty($light)) {//灯光s
                    $arr = [];
                    foreach ($light as $v) {
                        $arr[$v['type']][] = $v['light_id'];
                    }
                    $light = [];
                    foreach ($arr as $k => $v) {
                        switch ($k) {
                            case 'AmbientLight':
                                $tem = AmbinetLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            case 'DirectionalLight':
                                $tem = DirectionLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $row['shadow'] = json_decode($row['shadow']);
                                    $row['position'] = json_decode($row['position']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            case 'PointLight':
                                $tem = PointLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $row['position'] = json_decode($row['position']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            case 'HemisphereLight':
                                $tem = HemisphereLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $row['position'] = json_decode($row['position']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            case 'SpotLight':
                                $tem = SpotLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $row['shadow'] = json_decode($row['shadow']);
                                    $row['position'] = json_decode($row['position']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            case 'RectAreaLight':
                                $tem = RectAreaLightModel::build()->whereIn('light_id', $v)->select()->toArray();
                                foreach ($tem as &$row) {
                                    $row['param'] = json_decode($row['param']);
                                    $row['shadow'] = json_decode($row['shadow']);
                                    $row['position'] = json_decode($row['position']);
                                    $light[$row['name']] = $row;
                                }
                                unset($row);
                                break;
                            default:
                                break;
                        }
                    }
                }
                $groups[] = array_merge($group, $light);
            }
        }

        if (!empty($camera)) {//相机
            array_walk($camera, function (&$v) {
                $v['param'] = json_decode($v['param']);
                $v['position'] = json_decode($v['position']);
                $v['lookAt'] = json_decode($v['lookAt']);
            });
            unset($v);
        }
        if (!empty($control)) {//控制器
            array_walk($control, function (&$v) {
                $v['open'] = json_decode($v['open']);
            });
            unset($v);
        }
        if (!empty($render)) {//渲染
            array_walk($render, function (&$v) {
                $v['size'] = json_decode($v['size']);
                $v['clearColor'] = json_decode($v['clearColor']);
            });
            unset($v);
        }

        return ['size' => $scene,
            'images' =>
                ['envMap' => $env_pics, 'texture' => $texture_pics],
            'models' => $models,
            'three' => [
                'camera' => $camera,
                'light' => $groups,
                'render' => $render,
                'control' => $control
            ]];
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 获取三维环境 新增接口
     * @param $scheme_type
     * @return array|mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function get3DByCabinetTemp($scheme_type)
    {
        $cabinet_id = CabinetModel::build()->where(['type' => $scheme_type])->value('cabinet_id');

        if ($cabinet_id === null) {
            return $cabinet_id;
        }
        $scene = CabinetSceneModel::build()->field(['scene_id', 'size_x', 'size_y', 'size_z','environment_array'])->where(['cabinet_id' => $cabinet_id])->find();
        if ($scene === null) {
            return $scene;
        }
        $scene = $scene->toArray();
        $scene_id = array_shift($scene);

        $env_pics = CabinetEnvironmentModel::build()->field(['environment_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $texture_pics = CabinetTextureModel::build()->field(['texture_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $models = CabinetModelModel::build()->field(['model_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
       if (!empty($models)) {//模型
            array_walk($models, function (&$v) {
                $v['scale'] = json_decode($v['scale']);
                $v['rotation'] = json_decode($v['rotation']);
                $v['position'] = json_decode($v['position']);
                $v['size'] = json_decode($v['size']);
                $v['objName'] = $v['objName'] . '?v=' . rand();
                $v['mtlName'] = $v['mtlName'] . '?v=' . rand();
                $v['pics'] = $v['pics'] == '' ? [] : explode(',', $v['pics']);
                $v['material'] = json_decode($v['material']);
            });
            unset($v);
        }
        $groups = [];
        if (!empty($light_groups)) {
            foreach ($light_groups as $light_group) {
                $group['isDefault'] = $light_group['is_default'];
                $group['type'] = $light_group['type'];
                $light = CabinetLightGroupMapModel::build()->alias('m')->field(['cl.light_id', 'cl.name', 'cl.type'])
                    ->where(['m.light_group_id' => $light_group['light_group_id']])
                    ->join('customized_cabinet_light cl', 'cl.light_id = m.light_id')->select()->toArray();
                $groups[] = array_merge($group, $light);
            }
        }

        $environment_array = json_decode($scene['environment_array'],true);

        return ['size' => $scene,
            'images' =>
                ['envMap' => $env_pics, 'texture' => $texture_pics],
            'models' => $models,
            'three' => [
                'camera' => &$environment_array['camera'],
                'light' => &$environment_array['lightGroup'],
                'render' => &$environment_array['render'],
                'control' => &$environment_array['orbitCtrl']
            ]];
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 获取三维环境 新增接口
     * @param $scene_id
     * @return array|mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function get3DByCabinetAdmin($scene_id)
    {

        $scene = CabinetSceneModel::build()->field(['scene_id', 'size_x', 'size_y', 'size_z','environment_array'])->where(['scene_id' => $scene_id])->find();
        if ($scene === null) {
            return $scene;
        }
        $scene = $scene->toArray();
        $scene_id = array_shift($scene);

        $env_pics = CabinetEnvironmentModel::build()->field(['environment_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $texture_pics = CabinetTextureModel::build()->field(['texture_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        $models = CabinetModelModel::build()->field(['model_id', 'scene_id'], true)->where(['scene_id' => $scene_id])->select()->toArray();
        if (!empty($models)) {//模型
            array_walk($models, function (&$v) {
                $v['scale'] = json_decode($v['scale']);
                $v['rotation'] = json_decode($v['rotation']);
                $v['position'] = json_decode($v['position']);
                $v['size'] = json_decode($v['size']);
                $v['objName'] = $v['objName'] . '?v=' . rand();
                $v['mtlName'] = $v['mtlName'] . '?v=' . rand();
                $v['pics'] = $v['pics'] == '' ? [] : explode(',', $v['pics']);
                $v['material'] = json_decode($v['material']);
            });
            unset($v);
        }
        $groups = [];
        if (!empty($light_groups)) {
            foreach ($light_groups as $light_group) {
                $group['isDefault'] = $light_group['is_default'];
                $group['type'] = $light_group['type'];
                $light = CabinetLightGroupMapModel::build()->alias('m')->field(['cl.light_id', 'cl.name', 'cl.type'])
                    ->where(['m.light_group_id' => $light_group['light_group_id']])
                    ->join('customized_cabinet_light cl', 'cl.light_id = m.light_id')->select()->toArray();
                $groups[] = array_merge($group, $light);
            }
        }

        $environment_array = json_decode($scene['environment_array'],true);

        return ['size' => $scene,
            'images' =>
                ['envMap' => $env_pics, 'texture' => $texture_pics],
            'models' => $models,
            'three' => [
                'camera' => &$environment_array['camera'],
                'light' => &$environment_array['lightGroup'],
                'render' => &$environment_array['render'],
                'control' => &$environment_array['orbitCtrl']
            ]];
    }


    public static function get3DByHouseType($house_id)
    {
        $data = MyHomeHouseType::build()->where(['house_id' => $house_id])->value('cabinet_array');
        return json_decode($data,true);
    }

    public static function set3DByHouseType($house_id,$data)
    {
        $data = MyHomeHouseType::build()->update(['update_time'=>time(),'cabinet_array'=>json_encode($data,JSON_UNESCAPED_UNICODE)],['house_id' => $house_id]);
        return $data;
    }

    public static function getComponentDetail($component_id)
    {
        if ($component_id > 0) {
            $result = CustomizedComponent::build()->field(['create_time', 'update_time'], true)->where(['component_id' => $component_id])->find();
        } else {
            $result = ($detail = RedisUtil::getInstance()->get('component_temp')) == null ? null : unserialize($detail);
        }
        return $result;
    }

    public static function saveComponent($data)
    {
        if (isset($data['component_id']) && $data['component_id'] > 0) {
            $component_id = $data['component_id'];
            unset($data['component_id']);
            CustomizedComponent::build()->setWidthRange($data)->isUpdate(true)->save($data, ['component_id' => $component_id]);
        } else {
            unset($data['component_id']);
            $data['create_time'] = $data['update_time'] = time();
            $component_id = CustomizedComponent::build()->setWidthRange($data)->insertGetId($data);
        }
        return ['component_id' => $component_id];
    }

    public static function saveModel($data,$model_id)
    {
        return CustomizedCabinetModel::build()->isUpdate(true)->save($data, ['model_id' => $model_id]);
    }

    public static function getModel($model_id)
    {
        $info = CustomizedCabinetModel::build()->where(['model_id' => $model_id])->field(['scene_id'], true)->find();
        if(empty($info)){
            return false;
        }
        $info['scale'] = json_decode($info['scale']);
        $info['rotation'] = json_decode($info['rotation']);
        $info['position'] = json_decode($info['position']);
        $info['size'] = json_decode($info['size']);
        $info['material'] = json_decode($info['material']);
        $info['objName'] = $info['objName'] . '?v=' . rand();
        $info['mtlName'] = $info['mtlName'] . '?v=' . rand();
        $info['pics'] = $info['pics'] == '' ? [] : explode(',', $info['pics']);
        return $info;
    }
}