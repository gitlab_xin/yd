<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/21
 * Time: 17:42
 */

namespace app\api\logic;

use app\common\model\Feedback;

class FeedbackLogic
{
    public static function feedback($data)
    {
        Feedback::create($data);
    }
}