<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 17-12-1
 * Time: 下午2:39
 */

namespace app\api\logic;

use app\common\model\SchemeBean;
use app\common\model\ProductBean;

class SchemeYMOrderLogic
{

    public function scoreYMSchemeDoor(SchemeBean $scheme)
    {
        $doorCount = $scheme->getScheme_door_count();
        $doorHeight = $scheme->getScheme_door_height();
        $doorWidth = $scheme->getScheme_door_width_one();

        $gridCount = 0;
        $gridHeight_300 = 0;

        $oneDoorArea = number_format(getLength($doorWidth) * getLength($doorHeight) / 1000000, 2, '.', '');
        $mod = ($doorHeight - 72) % 300;
        $gridHeight_300 = $gridCount = intval(($doorHeight - 72) / 300);

        if ($mod == 0) {

        } elseif ($mod >= 100) {
            $gridCount++;
        } else {
            $gridCount++;
            $gridHeight_300--;
        }

        $d_products = [];
        $temp = 0; // 门口宽

        if ($scheme->getScheme_sk_color_no() != '-1') {
            $temp = $scheme->getScheme_hole_width() - (2 * CUSTOM_SHOUKOU_IN_OUT);
            if ($scheme->getIs_left_wall() || $scheme->getIs_left_wall_zt()) {
                $temp -= CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall() || $scheme->getIs_right_wall_zt()) {
                $temp -= CUSTOM_PADDING;
            }
        } else {
            $temp = $scheme->getScheme_hole_width();
        }
        $dw = intval(($temp - ($scheme->getScheme_door_count() + 1) * DOOR_FRAME_WIDTH - (2 * $scheme->getScheme_door_count())) / $scheme->getScheme_door_count());

        $d_p = new ProductBean();
        $d_p->setProduct_name("移门边料");
        $d_p->setProduct_no("W06-15-" . $doorHeight . "-00");
        $d_p->setProduct_height(DOOR_FRAME_WIDTH);
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count(2 * $doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-01");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($dw));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-02");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($dw));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上滑轨");
        $d_p->setProduct_no("W06-18-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下滑轨");
        $d_p->setProduct_no("W06-19-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("工字铝");
        $d_p->setProduct_no("W06-17-" . strval($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-00");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount * ($gridCount - 1));
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($dw));
        $d_products[] = $d_p;

        if ($gridHeight_300 == $gridCount) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * $gridCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;
        } elseif ($gridHeight_300 == $gridCount - 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 1));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*{$mod}*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height($mod);
            $d_p->setProduct_count($doorCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($dw + 18) . "*{$mod}*9");
            $d_products[] = $d_p;
        } else {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 2));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                . "*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(intval(($mod + 300) / 2));
            $d_p->setProduct_count($doorCount * 2);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                . "*9");
            $d_products[] = $d_p;
        }

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("圆头不锈钢自攻钉");
//        $d_p->setProduct_no("W02-02-3*12-01");
//        $d_p->setProduct_count(2 * $doorCount * $gridCount);
//        $d_p->setProduct_unit("个");
//        $d_p->setProduct_spec("4*10");
//        $d_products[] = $d_p;
//
//        $d_p = new ProductBean();
//        $d_p->setProduct_name("移门上下滑轮");
//        $d_p->setProduct_no("W04-19-XX-00");
//        $d_p->setProduct_count($doorCount);
//        $d_p->setProduct_unit("套");
//        $d_products[] = $d_p;
//
//        $d_p = new ProductBean();
//        $d_p->setProduct_name("双面胶");
//        $d_p->setProduct_no("W04-38-XX-00");
//        $d_p->setProduct_count(1);
//        $d_p->setProduct_unit("条");
//        $d_p->setProduct_spec("适量");
//        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("缓冲条");
        $d_p->setProduct_no("W06-23-" . $doorHeight . "-00");
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count($doorCount * 2);
        $d_p->setProduct_unit("条");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $scheme->setScheme_door_spec($doorWidth . "*" . $doorHeight);
        $scheme->setScheme_s_door_area($oneDoorArea);
        $scheme->setScheme_door_has_count($doorCount);
        $scheme->setScheme_door_area($oneDoorArea * $doorCount);
        $scheme->setScheme_o_door_price(formatMoney(719));
        $scheme->setScheme_s_door_price(formatMoney(719 * $scheme->getScheme_discount()));
        $scheme->setScheme_dis_door_price(formatMoney($scheme->getScheme_s_door_price() * $scheme->getScheme_door_area()));
        $scheme->setScheme_door_price(formatMoney(719 * $scheme->getScheme_door_area()));

        if ($scheme->getScheme_door_haveHCQ() == 1 && $scheme->getScheme_have_door() == 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("移门缓冲器");
            $d_p->setProduct_no("W04-20-XX-00");
            $d_p->setProduct_count(intval($doorCount / 2));
            $d_p->setProduct_unit("套");
            $d_p->setProduct_price(formatMoney(199));
            $d_p->setProduct_dis_price(formatMoney($d_p->getProduct_price() * $scheme->getScheme_discount()));
            $d_p->setProduct_total_price(formatMoney($d_p->getProduct_price() * $d_p->getProduct_count()));
            $d_p->setProduct_dis_total_price(formatMoney($d_p->getProduct_dis_price() * $d_p->getProduct_count()));
            $scheme->setScheme_door_price(formatMoney($scheme->getScheme_door_price() + $d_p->getProduct_total_price()));
            $scheme->setScheme_dis_door_price(formatMoney($scheme->getScheme_dis_door_price() + $d_p->getProduct_dis_total_price()));
            $scheme->setScheme_door_mat_price(formatMoney($d_p->getProduct_total_price()));
            $d_products[] = $d_p;
        }

        $scheme->setScheme_score_door_products($d_products);
    }


    public function scoreYMSchemeShouKou(SchemeBean $scheme)
    {
        $skColorNo = "";
        $skColorNo = ($scheme->getScheme_sk_color_no() == '000') ? "H" : "B";

        $totalPrice = 0;
        $dis_totalPrice = 0;

        if (empty($scheme->getScheme_dt_products()) && ($scheme->getScheme_sk_color_no() != '-1')) {
            $ttt = 0;
            $count = 0;// 垫料数量
            if ($scheme->getIs_left_wall()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_left_wall_zt()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall_zt()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            $schemeDiscount = $scheme->getScheme_discount();
            $sk_products = [];
            $sk_p = null;

            // System.out.println(scheme.getScheme_sk_colorNo());
            // 洞口宽度-(左右基料+盖板厚度)-左右垫料厚度+ 20加工余量 "00/灰色01/白色"
            // W06-12-{尺寸}-{风格}-{花色}

            $shyjl = $scheme->getScheme_hole_width() - CUSTOM_SHOUKOU_IN_OUT * 2 - $ttt + 20;// 上缘基料长度

            // 洞口高度 + 20加工余量 W06-12-{尺寸}-{风格}-{花色}
            $zyjl = $scheme->getScheme_hole_sl_height() + 20;// 左右基料长度

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("洞口上缘基料");
            $sk_p->setProduct_no("W06-12-" . $shyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($shyjl);
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($shyjl . "mm");
            $sk_p->setProduct_price(formatMoney(59 * $shyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount, 2));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("洞口左右基料");
            $sk_p->setProduct_no("W06-12-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(59 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("左右基料盖板");
            // 洞口左右基料长度 W06-13-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-13-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(4);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(15 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("上缘外扣面板");
            // 洞口上缘基料长度 W06-14-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-14-" . $shyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($shyjl);
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($shyjl . "mm");
            $sk_p->setProduct_price(formatMoney(39 * $shyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("左右外扣面板");
            // 洞口左右基料长度 W06-14-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-14-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(39 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("侧衔接塑胶条");
            // 洞口左右基料长度 W06-22-{尺寸}-{风格}
            $sk_p->setProduct_no("W06-22-" . $zyjl . "-00");
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("条");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(15 * $scheme->getScheme_hole_sl_height() / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            if ($count != 0) {
                $sk_p = new ProductBean();
                $sk_p->setProduct_name("洞口侧向垫方");
                // W06-21-{尺寸}-{风格}
                $sk_p->setProduct_no("W06-21-35*78*" . $scheme->getScheme_hole_sl_height() . "-00");
                $sk_p->setProduct_width($scheme->getScheme_hole_sl_height());
                $sk_p->setProduct_count(4);
                $sk_p->setProduct_count($count);
                $sk_p->setProduct_unit("支");
                $sk_p->setProduct_spec("35*78*" . $scheme->getScheme_hole_sl_height());
                $sk_p->setProduct_price(formatMoney(26));
                $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
                $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
                $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
                $sk_products[] = $sk_p;
                $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
                $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();
            }

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("现场施工费");
            $sk_p->setProduct_no("现场施工费");
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("");
            $sk_p->setProduct_price(formatMoney(56 * $scheme->getScheme_door_area()));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $scheme->setScheme_score_sk_products($sk_products);
            $scheme->setScheme_sk_price($totalPrice);
            $scheme->setScheme_dis_sk_Price($dis_totalPrice);
        }
        $scheme->setScheme_door_and_sk_price(formatMoney($scheme->getScheme_door_price() + $scheme->getScheme_sk_price()));
        $scheme->setScheme_dis_door_and_sk_price(formatMoney($scheme->getScheme_dis_door_price() + $scheme->getScheme_dis_sk_price()));

        // 计算总价格
        $scheme->setScheme_price($scheme->getScheme_price() + $scheme->getScheme_gt_price());
        $scheme->setScheme_price($scheme->getScheme_price() + $scheme->getScheme_door_price());
        $scheme->setScheme_price(formatMoney($scheme->getScheme_price() + $scheme->getScheme_sk_price()));

        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_gt_price());
        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_door_price());
        $scheme->setScheme_dis_price(formatMoney($scheme->getScheme_dis_price() + $scheme->getScheme_dis_sk_price()));
    }

    public function scoreYMSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $wujinMap = [];
        $t_p = null;

        if($scheme->getScheme_door_count() != null && 0 < count($scheme->getScheme_door_count())) {
            for ($i = 1; $i <= $scheme->getScheme_door_count(); $i++) {
                if (!isset($wujinMap['WB-012']) || empty($wujinMap['WB-012'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('移动门五金包');
                    $t_p->setProduct_no('WB-012');
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit('包');
                    $t_p->setProduct_price(formatMoney(30));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $t_p->setSeq(0);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['WB-012'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            }
        }

        $wujinProducts = array_values($wujinMap);

        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($wujinProducts as $w_p) {
            $w_p = change2ProductBean($w_p);
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
        // Collections.sort(wujin_products, new ProductRptSort_Seq()); // todo:
        $scheme->setScheme_score_wujin_products($wujinProducts);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            $scheme->getScheme_dis_plate_price()
            + $scheme->getScheme_dis_wujin_price()
            + $scheme->getScheme_dis_zj_price()
        ));
        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));

        return $scheme;
    }
}