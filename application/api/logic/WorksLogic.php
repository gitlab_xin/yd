<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 14:42
 */

namespace app\api\logic;

use think\Db;
use app\common\model\User as UserModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\WorksSchemeDetail as SchemeDetailModel;
use app\common\model\Works as WorksModel;
use app\common\model\CustomizedMaterial as MaterialModel;
use app\common\model\WorksCollect;
use app\common\model\WorksPraise as PraiseModel;
use app\common\model\WorksCollect as CollectModel;

class WorksLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 获取帖子详情
     * @param array $requestData
     * @return mixed
     */
    public function getWorksDetail($requestData)
    {
        $fields = ['w.works_id', 'w.user_id', 'u.username', 'u.avatar', 'u.is_designer', 'w.title', 'w.scene_src', 'w.scheme_src', 'w.content', 'w.create_time'
            , 'w.praise_count', 'w.comment_count', 'w.collect_count', 'w.view_count', 'IF(scheme_b_type != "",concat_ws("-",scheme_type,scheme_b_type),scheme_type) as scheme_type'];
        $where = ['w.type' => 1, 'w.works_id' => $requestData['works_id']];
        $join[] = ['yd_user u', 'u.user_id=w.user_id', 'left'];
        $join[] = ['yd_works_scheme_detail dsd', 'dsd.scheme_detail_id=w.scheme_id', 'left'];
        if ($requestData['at_user_id']) {
            /*
             * 如果有登录用户,则需要连表查询是否点赞和收藏
             */
            $fields = array_merge($fields, ['IF(ISNULL(praise.id),"no","yes") as is_praise', 'IF(ISNULL(collect.id),"no","yes") as is_collect']);
            $join = array_merge($join, [['yd_works_praise praise', 'praise.works_id = w.works_id and praise.user_id =' . $requestData['at_user_id'], 'left'],
                ['yd_works_collect collect', 'collect.works_id = w.works_id and collect.user_id =' . $requestData['at_user_id'], 'left']]);
        }
        $model = WorksModel::build();
        foreach ($join as $v) {
            $model = $model->join($v[0], $v[1], $v[2]);
        }
        $res = $model->alias('w')->field($fields)->where($where)->find();
        if ($res) {
            /*
             * 如果有返回结果,则需要进行浏览数自增
             */
            if ($requestData['at_user_id'] == 0) {
                $res['is_praise'] = $res['is_collect'] = 'no';
            }
            $model->where(['works_id' => $requestData['works_id']])->setInc('view_count');
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 点赞和取消点赞
     * @param int $user_id 用户ID
     * @param array $data 点赞类型和案例id
     * @return int|string
     */
    public function worksPraise($user_id, $data)
    {
        // 验证是否已经点赞
        $isExists = PraiseModel::isExists($user_id, $data['works_id']);

        $result = false;
        switch ($data['praise_type']) {
            case 'praise':
                if (!$isExists) {//如果不存在的话才进行点赞
                    $result = PraiseModel::addPraise(['user_id' => $user_id, 'works_id' => $data['works_id']]);
                }
                break;
            case 'cancel':
                if ($isExists) {//如果存在的话才进行取消点赞
                    $result = PraiseModel::delPraise($user_id, $data['works_id']);
                }
                break;
            default:
                break;
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 收藏和取消收藏
     * @param int $user_id 用户ID
     * @param array $data 收藏类型和案例id
     * @return int|string
     */
    public function worksCollect($user_id, $data)
    {
        // 验证是否已经点赞
        $isExists = CollectModel::isExists($user_id, $data['works_id']);

        $result = false;
        switch ($data['collect_type']) {
            case 'collect':
                if (!$isExists) {//如果不存在的话才进行点赞
                    $result = CollectModel::addCollect(['user_id' => $user_id, 'works_id' => $data['works_id']]);
                }
                break;
            case 'cancel':
                if ($isExists) {//如果存在的话才进行取消点赞
                    $result = CollectModel::delCollect($user_id, $data['works_id']);
                }
                break;
            default:
                break;
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取案例列表
     * @param $data
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getWorksList($data)
    {
        /*创建model对象*/
        $model = WorksModel::build()->alias('w');

        /*声明查询条件*/
        $fields = [
            'w.works_id',
            'w.user_id',
            'w.scene_src',
            'w.scheme_src',
            'w.title',
            'w.praise_count',
            'w.comment_count',
            'w.view_count',
            'w.create_time',
            'user.is_designer',
            'user.username',
            'user.avatar',
            'user.is_designer'
        ];
        $where = ['w.type' => 1];//默认条件拿未删除的
        $order = ['w.create_time' => 'DESC'];//默认拿置顶的
        $join = [[//左连user表获取用户名和头像
            'yd_user user',
            'user.user_id=w.user_id',
            'left'
        ]];

        $this->handleSearchCondition($data, $where, $join);

        //用户id为0的情况下, 说明没有用户登录, $fields和$join不用加入连表判断, >0则要连表, 此处判断避免无用户登录时进行无用的连表
        if ($data['at_user_id'] != 0) {
            $fields[] = 'IF(ISNULL(praise.id),"no","yes") as is_praise';
            $join[] = ['yd_works_praise praise', 'praise.works_id=w.works_id and praise.user_id =' . $data['at_user_id'], 'left'];
            if (isset($data['collect']) && $data['collect'] == 1) {
                $join[] = ['yd_works_collect collect', 'collect.works_id=w.works_id and collect.user_id =' . $data['at_user_id'], 'right'];
            }
        }

        /*用model链式操作拼接sql*/
        foreach ($join as $row) {
            $model = $model->join($row[0], $row[1], $row[2]);
        }
        $result = $model->field($fields)->where($where)->order($order)->page($data['pageIndex'], $data['pageSize'])->select();

        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取案例总数
     * @param $data
     * @return int|string
     */
    public function getWorksCount($data)
    {
        $model = WorksModel::build()->alias('w');
        $where = ['w.type' => 1];//默认条件拿通过审核的
        $join = [];


        $this->handleSearchCondition($data, $where, $join);

        foreach ($join as $row) {
            $model = $model->join($row[0], $row[1], $row[2]);
        }


        return $model->where($where)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取收藏案例数量
     * @param $user_id
     * @return int|string
     */
    public function getWorksCollectionCount($user_id)
    {
        return WorksCollect::build()->where(['user_id' => $user_id])->count();
    }

    public function addWorks($data)
    {
        try {
            $res = ['code' => 1, 'msg' => '申请成功'];
            if (($scheme = SchemeModel::build()->where(['id' => $data['origin_scheme_id'], 'user_id' => $data['user_id']])->find()) == null) {
                $res['code'] = 2;
                $res['msg'] = '该方案不属于你';
            } elseif (UserModel::build()->where(['user_id' => $data['user_id']])->value('is_designer') == 0) {
                $res['code'] = 2;
                $res['msg'] = '请先申请成为设计师';
            } elseif (WorksModel::build()->where(['user_id' => $data['user_id'], 'type' => 0])->count() > 0) {
                $res['code'] = 3;
                $res['msg'] = '你有方案正在审核中';
            } else {
                $data['scheme_src'] = $scheme->scheme_pic;
                $scheme = $scheme->toArray();
                unset($scheme['id']);


                Db::startTrans();
                //copy 方案
                if (!($result = SchemeDetailModel::create($scheme, true))) {
                    Db::rollback();
                    $res['code'] = 5;
                    $res['msg'] = '方案复制失败';
                    return $res;
                }
                $data['scheme_id'] = $result->getLastInsID();
                if (!WorksModel::create($data, true)) {
                    Db::rollback();
                    $res['code'] = 6;
                    $res['msg'] = '数据插入失败';
                    return $res;
                }

                Db::commit();
            }

            return $res;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function getSchemeDetailByWorks($works_id)
    {
        $scheme_id = WorksModel::build()->where(['works_id' => $works_id])->value('scheme_id', false);
        if (!$scheme_id) {
            return $scheme_id;
        }
        $scheme = SchemeDetailModel::build()->where(['scheme_detail_id' => $scheme_id])
            ->field(['scheme_name', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'serial_array'])->find();
        return $scheme;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月22日
     * description:处理案例列表的筛选了条件
     * @param $data
     * @param $where
     * @param $join
     */
    private function handleSearchCondition($data, &$where, &$join)
    {
        /*过滤参数*/
        //排序筛选
        if (isset($data['sort']) && $data['sort'] == 'recommended') {
            $where['w.is_recommended'] = 1;
        }
        //关键字筛选
        if (!empty($data['keyword'])) {
            $keyword = "REGEXP '{$data['keyword']}'";
            $where['w.title'] = ['exp', $keyword];
        }
        //我的案例
        if (isset($data['user_id'])) {
            $where['w.user_id'] = $data['user_id'];
        }
        //分类筛选和颜色筛选
        if (isset($data['scheme_type']) && ($data['scheme_type'] != '' || $data['material_name'] != '')) {
            $join[] = ['yd_works_scheme_detail sd', 'w.scheme_id = sd.scheme_detail_id', 'left'];
            //花色
            if ($data['material_name'] != '') {
                $where['sd.scheme_color_no'] = MaterialModel::getIdByName($data['material_name']);
            }
            //分类
            if ($data['scheme_type'] != '') {
                $where['sd.scheme_type'] = $data['scheme_type'];
                switch ($data['scheme_type']) {
                    /*
                     * 入墙式移门壁柜
                     */
                    case 'RQS':
                        //洞口宽度
                        if (!empty($data['scheme_hole_width_min']) && !empty($data['scheme_hole_width_max'])) {
                            $where['sd.scheme_hole_width'] = [
                                'between',
                                [$data['scheme_hole_width_min'], $data['scheme_hole_width_max']]
                            ];
                        } elseif (!empty($data['scheme_hole_width_min'])) {
                            $where['sd.scheme_hole_width'] = ['egt', $data['scheme_hole_width_min']];
                        } elseif (!empty($data['scheme_hole_width_max'])) {
                            $where['sd.scheme_hole_width'] = ['elt', $data['scheme_hole_width_max']];
                        }
                        //移门上梁高度
                        if (!empty($data['scheme_hole_sl_height_min']) && !empty($data['scheme_hole_sl_height_max'])) {
                            $where['sd.scheme_hole_sl_height'] = [
                                'between',
                                [$data['scheme_hole_sl_height_min'], $data['scheme_hole_sl_height_max']]
                            ];
                        } elseif (!empty($data['scheme_hole_sl_height_min'])) {
                            $where['sd.scheme_hole_sl_height'] = ['egt', $data['scheme_hole_sl_height_min']];
                        } elseif (!empty($data['scheme_hole_sl_height_max'])) {
                            $where['sd.scheme_hole_sl_height'] = ['elt', $data['scheme_hole_sl_height_max']];
                        }
                        //花色默认是0
                        $where['sd.scheme_color_no'] = 0;
                        break;
                    default:
                        break;
                }
            }
        }

    }

}