<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/25
 * Time: 9:24
 */

namespace app\api\logic;

use app\common\model\Demand as DemandModel;
use app\common\model\DemandBanner as BannerModel;
use app\common\model\DemandParam as DemandParamModel;
use app\common\model\DemandScheme as DemandSchemeModel;
use think\Db;
use think\Config;

class DemandLogic
{
    /**
     * @author: Airon
     * @time: 2017年8月25日
     * description:入墙式移门壁柜 写入需求
     * @param $user_id
     * @param $param
     * @return bool|int|string
     */
    public static function buildDemand($user_id, $param, $classify, $scheme_type, $scheme_b_type = '')
    {
        $demand_data['user_id'] = $user_id;
        $demand_data['classify'] = $classify;
        $demand_data['title'] = $param['title'];
        $demand_data['content'] = $param['content'];
        $demand_data['material_name'] = $param['material_name'];
        $demand_data['img_src_list'] = $param['img_src_list'];
        $demand_data['cost_min'] = $param['cost_min'];
        $demand_data['cost_max'] = $param['cost_max'];
        $demand_data['create_time'] = time();

//        $demand_param['scheme_color_no'] = $param['scheme_color_no'];
//        $demand_param['scheme_hole_type'] = $param['hole_type'];
//        $demand_param['scheme_hole_width'] = $param['hole_width'];
//        $demand_param['scheme_hole_height'] = $param['hole_height'];
//        $demand_param['scheme_hole_sl_height'] = $param['sl_height'];
//        $demand_param['scheme_door_count'] = $param['door_count'];
//        $demand_param['scheme_hole_left'] = $param['b_width'];
//        $demand_param['scheme_hole_right'] = $param['c_width'];
//        $demand_param['scheme_sk'] = $param['sk_color_no'];
        Db::startTrans();
        $demand_id = DemandModel::build()->insertGetId($demand_data);
        if (empty($demand_id)) {
            Db::rollback();
            return false;
        }
        $param['demand_id'] = $demand_id;
        $param['scheme_type'] = $scheme_type;
        $param['scheme_b_type'] = $scheme_b_type;
        if (empty(DemandParamModel::create($param, true))) {
            Db::rollback();
            return false;
        }
        Db::commit();
        return intval($demand_id);
    }

    /**
     * @author: Airon
     * @time: 2017年8月31日
     * description:获取需求大厅列表(入墙式移门壁柜)
     * @param $requestData
     * @param $page
     * @param $size
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getListRQS($requestData, $page, $size)
    {
        $where['demand.is_deleted'] = 0;
        $where['demand.status'] = 1;
        if (!empty($requestData['scheme_type'])) {
            $where['param.scheme_type'] = $requestData['scheme_type'];
            //洞口宽度
            if (!empty($requestData['scheme_hole_width_min']) && !empty($requestData['scheme_hole_width_max'])) {
                $where['param.scheme_hole_width'] = [
                    'between',
                    [$requestData['scheme_hole_width_min'], $requestData['scheme_hole_width_max']]
                ];
            } elseif (!empty($requestData['scheme_hole_width_min'])) {
                $where['param.scheme_hole_width'] = ['egt', $requestData['scheme_hole_width_min']];
            } elseif (!empty($requestData['scheme_hole_width_max'])) {
                $where['param.scheme_hole_width'] = ['elt', $requestData['scheme_hole_width_max']];
            }
            //移门上梁高度
            if (!empty($requestData['scheme_hole_height_min']) && !empty($requestData['scheme_hole_height_max'])) {
                $where['param.scheme_hole_height'] = [
                    'between',
                    [$requestData['scheme_hole_height_min'], $requestData['scheme_hole_height_max']]
                ];
            } elseif (!empty($requestData['scheme_hole_height_min'])) {
                $where['param.scheme_hole_height'] = ['egt', $requestData['scheme_hole_height_min']];
            } elseif (!empty($requestData['scheme_hole_height_max'])) {
                $where['param.scheme_hole_height'] = ['elt', $requestData['scheme_hole_height_max']];
            }
        }

        //赏金
        if (!empty($requestData['reward_min']) && !empty($requestData['reward_max'])) {
            $where['demand.reward'] = [
                'between',
                [$requestData['reward_min'], $requestData['reward_max']]
            ];
        } elseif (!empty($requestData['reward_min'])) {
            $where['demand.reward'] = ['egt', $requestData['reward_min']];
        } elseif (!empty($requestData['reward_max'])) {
            $where['demand.reward'] = ['elt', $requestData['reward_max']];
        }

        if (!empty($requestData['keyword'])) {
            $keyword = "REGEXP '{$requestData['keyword']}'";
            $where['demand.title'] = ['exp', $keyword];
        }

        if (!empty($requestData['style_name'])) {
            $where['demand.style_name'] = $requestData['style_name'];
        }
        if (!empty($requestData['material_name'])) {
            $where['demand.material_name'] = $requestData['material_name'];
        }

        $field = [
            'demand.demand_id',
            'user.user_id',
            'user.username',
            'user.avatar',
            'demand.title',
            'demand.content',
            'demand.classify',
            'demand.style_name',
            'demand.material_name',
            'demand.reward',
            'demand.create_time',
            'demand.look_count',
            'user.is_designer'
        ];
        switch ($requestData['order_type']) {
            case 'new':
                $order = ['demand.create_time' => 'DESC'];
                break;
            case 'reward_desc':
                $order = ['demand.reward' => 'DESC'];
                break;
            case 'reward_asc':
                $order = ['demand.reward' => 'ASC'];
                break;
            default:
                $order = ['demand.create_time' => 'DESC'];
                break;
        }
        $data['demand_list'] = DemandModel::build()->alias('demand')
            ->join('yd_user user', 'user.user_id = demand.user_id')
            ->join('yd_demand_param param', 'param.demand_id = demand.demand_id')
            ->where($where)
            ->field($field)
            ->order($order)
            ->page($page, $size)
            ->select();
        $data['count'] = DemandModel::build()->alias('demand')
            ->join('yd_user user', 'user.user_id = demand.user_id')
            ->join('yd_demand_param param', 'param.demand_id = demand.demand_id')
            ->where($where)
            ->count();
        return $data;
    }

    /**
     * @author: Airon
     * @time: 2017年8月25日
     * description:检查需求内容是否存在
     * @param $user_id
     * @param $demand_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function checkDemand($user_id, $demand_id)
    {
        return DemandModel::build()->where(['user_id' => $user_id, 'demand_id' => $demand_id, 'status' => 0])->find();
    }

    /**
     * @author: Airon
     * @time: 2017年8月31日
     * description:需求详情
     * @param $demand_id
     * @param $status
     * @return array|bool|false|\PDOStatement|string|\think\Model
     */
    public static function getInfo($demand_id, $status = null)
    {
        $where = ['demand.demand_id' => $demand_id, 'demand.is_deleted' => 0, 'demand.status' => ['neq', 0]];
        if ($status != null) {
            $where['demand.status'] = $status;
        }

        DemandModel::build()->where(['demand_id' => $demand_id])->setInc('look_count');

        $field = [
            'demand.user_id',
            'user.username',
            'user.avatar',
            'user.is_designer',
            'demand.status',
            'demand.title',
            'demand.content',
            'demand.cost_min',
            'demand.cost_max',
            'demand.reward',
            'demand.look_count',
            'demand.classify',
            'demand.style_name',
            'demand.material_name',
            'demand.img_src_list',
            'demand.create_time',
            'param.scheme_type', 'param.scheme_s_type', 'param.scheme_b_type', 'param.scheme_width', 'param.scheme_height', 'param.scheme_door_count', 'param.scheme_color_no',
            'param.scheme_hole_type', 'param.scheme_hole_width', 'param.scheme_hole_height', 'param.scheme_hole_sl_height', 'param.scheme_hole_left', 'param.scheme_hole_right',
            'param.scheme_sk', 'tv_size', 'tv_type', 'env_width', 'env_height'
        ];

        $demand = DemandModel::build()->alias('demand')
            ->join('yd_demand_param param', 'demand.demand_id = param.demand_id')
            ->join('yd_user user', 'demand.user_id = user.user_id')
            ->where($where)
            ->field($field)
            ->find();
        if ($demand != null && $demand->status == 3) {
            $demand['accepted_scheme'] = DemandSchemeModel::build()->alias('s')
                ->field(['s.scheme_detail_id', 's.title', 's.desc', 'sd.scheme_pic', 's.create_time', 'u.username', 'u.avatar', 'u.is_designer'])
                ->join('yd_demand_scheme_detail sd', 'sd.scheme_detail_id = s.scheme_detail_id', 'LEFT')
                ->join('yd_user u', 's.user_id = u.user_id', 'LEFT')
                ->where(['demand_id' => $demand_id, 'is_accepted' => 1])->select()->toArray();
        }
        return $demand;
    }

    /**
     * @author: Airon
     * @time: 2017年8月25日
     * description:支付前设置需求的赏金
     * @param $demand_id
     * @param $data
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function setDemand($demand_id, $data)
    {
        return DemandModel::build()->update($data, ['demand_id' => $demand_id]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月30日
     * description:获取我的需求列表
     * @param $user_id
     * @param $status
     * @param $pageIndex
     * @param $pageSize
     * @return bool|false|\PDOStatement|string|\think\Collection
     */
    public static function getListByUser($user_id, $status, $pageIndex, $pageSize)
    {
        try {
            $result = DemandModel::build()->alias('d')
                ->field([
                    'd.demand_id',
                    'd.user_id',
                    'd.classify',
                    'd.title',
                    'd.content',
                    'd.style_name',
                    'd.material_name',
                    'd.reward',
                    'd.create_time',
                    'u.username',
                    'u.avatar',
                    'u.is_designer',
                    'd.look_count',
                    'count(ds.id) as scheme_count'
                ])
                ->join('user u', 'u.user_id = d.user_id', 'LEFT')
                ->join('demand_scheme ds', 'ds.demand_id = d.demand_id', 'LEFT')
                ->where(['d.user_id' => $user_id, 'd.status' => $status, 'd.is_deleted' => 0])
                ->group('d.demand_id')
                ->order(['d.demand_id' => 'DESC'])
                ->page($pageIndex, $pageSize)
                ->select();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取我的需求总数
     * @param $user_id
     * @param $status
     * @return int|string
     */
    public static function getDemandCount($user_id, $status)
    {
        return DemandModel::build()->where(['user_id' => $user_id, 'status' => $status, 'is_deleted' => 0])->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月5日
     * description:获取关闭原因
     * @return mixed
     */
    public static function getCloseReason()
    {
        $list = Config::get('demand')['close_reason'];
        return $list;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取banner中的需求详情
     * @param $banner_id
     * @return bool
     */
    public static function getBannerDetail($banner_id)
    {
        try {
            $demand_id = BannerModel::build()->where(['banner_id' => $banner_id])->value('demand_id');
            if ($demand_id != null && ($demand = self::getInfo($demand_id, 3))) {
                $return['detail'] = $demand;
                $return['good_schemes'] = DemandSchemeModel::build()->alias('s')
                    ->field(['s.scheme_detail_id', 's.title', 's.desc', 'sd.scheme_pic', 's.create_time', 'u.username', 'u.avatar', 'u.is_designer'])
                    ->join('yd_demand_scheme_detail sd', 'sd.scheme_detail_id = s.scheme_detail_id', 'LEFT')
                    ->join('yd_user u', 's.user_id = u.user_id', 'LEFT')
                    ->where(['demand_id' => $demand_id, 'is_good' => 1])->select()->toArray();
                return $return;
            }
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:获取我的承接列表
     * @param $user_id
     * @param $status 1进行中 2.承接失败 3.承接成功
     * @param $pageIndex
     * @param $pageSize
     * @return bool|false|\PDOStatement|string|\think\Collection
     */
    public static function getContributionsByUser($user_id, $status, $pageIndex, $pageSize)
    {
        try {
            $where = ['ds.user_id' => $user_id, 'd.status' => $status, 'd.is_deleted' => 0];
            switch ($status) {
                case 2://承接失败
                    $where['ds.is_accepted'] = 0;
//                    $where['d.status'] = ['in', [2, 3]];
                    break;
                case 3://承接成功
                    $where['ds.is_accepted'] = 1;
                    break;
                default:
                    break;
            }
            $result = DemandSchemeModel::build()->alias('ds')
                ->field([
                    'd.demand_id',
                    'd.user_id',
                    'd.classify',
                    'd.title',
                    'd.content',
                    'd.style_name',
                    'd.material_name',
                    'd.reward',
                    'd.create_time',
                    'u.username',
                    'u.avatar',
                    'u.is_designer',
                    'd.look_count',
                ])
                ->join('user u', 'u.user_id = ds.user_id', 'LEFT')
                ->join('demand d', 'ds.demand_id = d.demand_id', 'LEFT')
                ->where($where)
                ->group('ds.demand_id DESC')
                ->page($pageIndex, $pageSize)
                ->select();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getContributionsCount($user_id, $status)
    {
        $where = ['ds.user_id' => $user_id, 'd.status' => $status, 'd.is_deleted' => 0];
        switch ($status) {
            case 2://承接失败
                $where['ds.is_accepted'] = 0;
                $where['d.status'] = ['in', [2, 3]];
                break;
            case 3://承接成功
                $where['ds.is_accepted'] = 1;
                break;
            default:
                break;
        }
        return count(DemandSchemeModel::build()->alias('ds')->field('ds.demand_id')
            ->where($where)
            ->join('demand d', 'ds.demand_id = d.demand_id', 'LEFT')
            ->group('ds.demand_id')->select()->toArray());
    }

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:获取登陆者对该需求的状态
     * @param $user_id
     * @param $demand_id
     * @return array|bool
     *
     */
    public static function checkDemandStatus($user_id, $demand_id)
    {
        try {
            if (($demand = DemandModel::build()->where(['demand_id' => $demand_id, 'is_deleted' => 0, 'status' => ['neq', 0]])->find()) != null
                && $user_id == $demand->user_id) {//如果是需求方
                $return['status'] = $demand->status;
                $return['scheme_count'] = DemandSchemeModel::build()->where(['demand_id' => $demand_id])->count();
                return ['master' => $return, 'guest' => null];
            } elseif (($count = DemandSchemeModel::build()->where(['demand_id' => $demand_id, 'user_id' => $user_id])->count()) > 0) {//如果是承接方
                $return['status'] = $demand->status;
                $return['commit_count'] = $count;
                $return['commit_limit'] = Config::get('demand.contribute_limit');
                $return['is_accepted'] = DemandSchemeModel::build()->where(['demand_id' => $demand_id, 'user_id' => $user_id, 'is_accepted' => 1])->count();
                return ['guest' => $return, 'master' => null];
            } else {
                return ['guest' => null, 'master' => null];
            }
        } catch (\Exception $e) {
            return -1;
        }

    }
}