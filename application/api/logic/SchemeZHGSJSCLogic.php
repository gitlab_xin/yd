<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/27
 * Time: 9:57
 */

namespace app\api\logic;

use app\common\model\SchemeZHGBean as SchemeBean;
use app\common\model\CustomizedScheme;
use app\common\tools\CommonMethod;

class SchemeZHGSJSCLogic extends SchemeBaseLogic
{
    private $productLogic = null;


    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:重构成原来参数进行验证
     * @param $scheme
     * @return array
     */
    public function getOriginParams($scheme)
    {
        $params = [];

        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';
        $params['scheme_s_type'] = isset($scheme['scheme_s_type']) ? $scheme['scheme_s_type'] : -1;

        return $params;
    }

    public function getProductLogic()
    {
        if ($this->productLogic == null) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    public function buildScheme($schemeData)
    {
        return SchemeBean::build($schemeData);
    }

    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        $schemeModelData = [
            'user_id' => $userId,
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_type' => 'ZH',
            'scheme_type_text' => '书柜+写字桌组合',
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 314,
            'scheme_door_count' => 0,
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => '000',
            'scheme_sk' => -1,
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_b_type' => 'SJSC',
            'scheme_hole_type' => 0,
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => 314,
            'is_left_wall' => '0',
            'is_right_wall' => '0',
            'is_left_wall_zt' => '0',
            'is_right_wall_zt' => '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => ''
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model->getLastInsID();
    }

    public function update(SchemeBean $schemeBean, $schemeData, $userId, $scheme_id)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
        ];
        return CustomizedScheme::update($schemeModelData, ['user_id' => $userId, 'id' => $scheme_id]);
    }

    public function schemeMath($data)
    {
        $range_arr = [50 => 0, 100 => 0, 150 => 0];//误差值范围
        foreach ($range_arr as $range => &$value) {
            $scheme = new SchemeBean();
            $scheme->setScheme_height(2019); //高
            $scheme->setScheme_s_type($data['scheme_s_type']);
            $scheme->setScheme_error_range($range); //设置误差值
            $scheme->setScheme_width($data['width']); //宽
            $scheme->setScheme_type('ZH');
            $scheme->setScheme_b_type('SJSC');
            $scheme->setScheme_color_no('000');

            if ($data['scheme_s_type'] == 0) {
                $this->createZHGSJSCSchemeYZC($scheme, true);
            } else {
                $this->createZHGSJSCSchemeYZZ($scheme, true);
            }

            $value = count($scheme->getScheme_schemes());
        }
        return $range_arr;
    }

    public function schemeCreate($data)
    {
        $index = base64_encode(http_build_query($data));
        $productList = $this->beforeCreate('zhgsjsc', $index);
        if (!empty($productList)) {
            return $productList;
        }

        $scheme = new SchemeBean();
        $scheme->setScheme_height(2019); //高
        $scheme->setScheme_s_type($data['scheme_s_type']);
        $scheme->setScheme_error_range($data['error_range']); //设置误差值
        $scheme->setScheme_width($data['width']); //宽
        $scheme->setScheme_deep(314); //深度
        $scheme->setScheme_type('ZH');
        $scheme->setScheme_b_type('SJSC');
        $scheme->setScheme_color_no('test2');

        $data['scheme_s_type'] == 0 ? $this->createZHGSJSCSchemeYZC($scheme, false) : $this->createZHGSJSCSchemeYZZ($scheme, false);

        $productList = $scheme->getScheme_schemes();
        if (!empty($productList)) {
            array_walk($productList, function (&$row) use ($index) {
                $row->setSearch_index($index);
                $row = $row->mainInfo();
            });
            $this->afterCreate('zhgsjsc', $productList);
        }
        return $productList;
    }

    public function createZHGSJSCSchemeYZC(SchemeBean $scheme, $isMath)
    {
        $inputwidth = $scheme->getScheme_width();
        $height = $scheme->getScheme_height();
        if ($height < 2019) {
            $height = $height - 16;
        }
        $width = 0;// 家具宽度
        $errorrange = $scheme->getScheme_error_range();

        $zhWidthArr = [432, 560];
        $colWidthArr = [432, 560, 784];

        $s_scheme = new SchemeBean();
        $s_schemes = [];
        $t_scheme = new SchemeBean();
        $t_schemes = [];
        $s_products = [];

        $wcb_products = [];
        $ncb_products = [];

        $acc_products = [];
        $t_p = [];

        $w432 = 432;
        $w560 = 560;
        $w784 = 784;

        $colcount = 1;
        $gridcount = intval(($height - 74) / 320);

        $schemeColorNo = $scheme->getScheme_color_no();

        for ($i = 0; $i < count($zhWidthArr); $i++) {

            // 单列
            $width = $zhWidthArr[$i] * 2 + 25 + 50;

            if (abs($inputwidth - $width) < $errorrange) {
                $s_scheme = new SchemeBean();
                if (!$isMath) {
                    $t_schemes = [];
                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                    $s_scheme->setScheme_hole_width($inputwidth);
                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                    $s_scheme->setScheme_width($width);
                    $s_scheme->setScheme_height($scheme->getScheme_height());
                    $s_scheme->setScheme_wcb_type(1);
                    $s_scheme->setScheme_deep(314);
                    $s_scheme->setScheme_wcb_products($wcb_products);
                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                    $s_scheme->setScheme_type($scheme->getScheme_type());
                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                    $wcb_products = [];
                    $ncb_products = [];

                    // 外侧板
                    $wcb_products[] = $this->getProductLogic()->createZHG_WCB_ZJ(0, 0, 0, 0, 25, $height, 314,
                        $schemeColorNo, 0);
                    $s_scheme->setScheme_wcb_type(0);
                    $wcb_products[] = $this->getProductLogic()->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25, $height,
                        314, $schemeColorNo, 0);

                    // 固定功能体
                    $t_scheme = new SchemeBean();

                    $t_scheme = $this->getChuizhiXZZCol($schemeColorNo, $zhWidthArr[$i] * 2 + 25);

                    $t_schemes[] = $t_scheme;

                    $s_scheme->setScheme_wcb_products($wcb_products);
                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_schemes($t_schemes);

                    $s_scheme->setScheme_error_range($errorrange);
//                    $s_pic = "SJSC1" . $s_scheme->getScheme_width() . "_"
//                        . $s_scheme->getScheme_height() . "_"
//                        . $s_scheme->getScheme_color_no() . "_"
//                        . count($t_schemes) . "_"
//                        . getRandomInt();
                    $this->createSchemePic($s_scheme);
                    $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());
                }

                $s_schemes[] = $s_scheme;
            }

            // 两列
            for ($c = 0; $c < 3; $c++) {
                $width = $zhWidthArr[$i] * 2 + 25 + 25 + 25 + 16 + $colWidthArr[$c];
                if (abs($inputwidth - $width) < $errorrange) {
                    $s_scheme = new SchemeBean();
                    if (!$isMath) {
                        $t_schemes = [];
                        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                        $s_scheme->setScheme_hole_width($inputwidth);
                        $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                        $s_scheme->setScheme_width($width);
                        $s_scheme->setScheme_height($scheme->getScheme_height());
                        $s_scheme->setScheme_wcb_type(1);
                        $s_scheme->setScheme_deep(314);
                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $s_scheme->setScheme_type($scheme->getScheme_type());
                        $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                        $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                        $wcb_products = [];
                        $ncb_products = [];

                        // 外侧板
                        $wcb_products[] = ($this->getProductLogic()->createZHG_WCB(0, 0, 0, 0, 16, $height, 314,
                            $scheme->getScheme_color_no(), 0));
                        $s_scheme->setScheme_wcb_type(1);
                        $wcb_products[] = ($this->getProductLogic()->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25,
                            $height, 314, $schemeColorNo, 0));

                        // 普通列
                        $t_scheme = new SchemeBean();
                        $t_scheme->setScheme_width($colWidthArr[$c]);
                        $t_scheme->setScheme_height($height);
                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());

                        $s_products = [];

                        // 背板
//                        $s_products[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 5, $height - 74,
//                            $colWidthArr[$c], $schemeColorNo, 0));
                        if ($height <= 425) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 345, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 745) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1065) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1385) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1705) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 2025) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        }

                        // 横隔板顶
                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0, 25, 298,
                            $colWidthArr[$c], $schemeColorNo, 2, 0));

                        // 横隔板底
                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74),
                            25, 298, $colWidthArr[$c], $schemeColorNo, 2, 0));

//                         底担
                        $s_products[] = ($this->getProductLogic()->createZHG_DD(0, 72, 0, ($height - 72), 25, 72,
                            $colWidthArr[$c], $schemeColorNo, 0));

                        // 多层横隔板
                        for ($g = 1; $g < $gridcount; $g++) {
                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(0, $height - $g * (320), 0,
                                $g * (320), 25, 298, $colWidthArr[$c], $schemeColorNo, 2, 0));
                        }

                        $t_scheme->setScheme_products($s_products);
                        $t_schemes[] = ($t_scheme);

                        // 固定功能体
                        $t_scheme = new SchemeBean();
                        $t_scheme = $this->getChuizhiXZZCol($schemeColorNo, $zhWidthArr[$i] * 2 + 25);

                        $t_schemes[] = ($t_scheme);

                        // 内侧板
                        $x = 16;
                        for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                            if (0 == $n) {
                                $x = $x + $t_schemes[$n]->getScheme_width();
                            } else {
                                $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                            }

                            if ("" != $t_schemes[$n]->getScheme_no()
                                || "" != $t_schemes[$n + 1]->getScheme_no()
                            ) {
                                $ncb_products[] = ($this->getProductLogic()->createZHG_ZJXJCB($x, 0, $x, 0, 25,
                                    $height, 314, $schemeColorNo, 0));
                            } else {
                                $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB($x, 0, $x, 0, 25, $height,
                                    314, $schemeColorNo, 0));
                            }
                        }

                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_schemes($t_schemes);

                        $s_scheme->setScheme_error_range($errorrange);
//                        $s_pic = "SJSC1" . $s_scheme->getScheme_width() . "_" . $s_scheme->getScheme_height() . "_"
//                            . $s_scheme->getScheme_color_no() . "_" . count($t_schemes) . "_" . getRandomInt();
                        $this->createSchemePic($s_scheme);
                        $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());
                    }

                    $s_schemes[] = ($s_scheme);
                }
            }

            // 多于2列
            $max432colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w432);
            $max560colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w560);
            $max784colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w784);

            for ($i784 = 0; $i784 <= $max784colcount; $i784++) {
                for ($i560 = 0; $i560 <= $max560colcount; $i560++) {
                    for ($i432 = 0; $i432 <= $max432colcount; $i432++) {
                        $colcount = $i784 + $i560 + $i432 + 1;
                        if (3 <= $colcount) {
                            $width = 25 * ($colcount - 1) + $zhWidthArr[$i] * 2 + 25 + 2 * 16 + $i432 * $w432 + $i560 * $w560 + $i784 * $w784;
                            if (abs($inputwidth - $width) < $errorrange) {
                                $s_scheme = new SchemeBean();
                                if (!$isMath) {
                                    $t_schemes = [];
                                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                                    $s_scheme->setScheme_hole_width($inputwidth);
                                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_width($width);
                                    $s_scheme->setScheme_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_wcb_type(1);
                                    $s_scheme->setScheme_deep(314);
                                    $s_scheme->setScheme_wcb_products($wcb_products);
                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_color_no($schemeColorNo);
                                    $s_scheme->setScheme_type($scheme->getScheme_type());
                                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                                    $wcb_products = [];
                                    $ncb_products = [];

                                    // 外侧板
                                    $wcb_products[] = ($this->getProductLogic()->createZHG_WCB(0, 0, 0, 0, 16, $height,
                                        314, $schemeColorNo, 0));
                                    $s_scheme->setScheme_wcb_type(1);
                                    $wcb_products[] = ($this->getProductLogic()->createZHG_WCB(0, 0, ($width - 16), 0,
                                        16, $height, 314, $schemeColorNo, 0));

                                    // 组合功能单元

                                    for ($c = 0; $c < $i432; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w432);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);
                                        $s_products = [];

                                        // 背板
//                                        $s_products[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 5,
//                                            $height - 74, $w432, $schemeColorNo, 0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 653, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        }

                                        // 横隔板顶
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0,
                                            25, 298, $w432, $schemeColorNo, 2, 0));

                                        // 横隔板底
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, 74 + 25, 0,
                                            ($height - 25 - 74), 25, 298, $w432, $schemeColorNo, 2, 0));

                                        // 底担
                                        $s_products[] = ($this->getProductLogic()->createZHG_DD(0, 73, 0,
                                            ($height - 73), 25, 72, $w432, $schemeColorNo, 0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(0,
                                                $height - $g * (320), 0, $g * (320), 25, 298, $w432, $schemeColorNo, 2,
                                                0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }
                                    for ($c = 0; $c < $i560; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w560);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);

                                        $s_products = [];

                                        // 背板
//                                        $s_products[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 5,
//                                            $height - 74, $w560, $schemeColorNo, 0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653 ,
                                                5, 653, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973 ,
                                                5, 653, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973 ,
                                                5, 973, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        }

                                        // 横隔板顶
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0,
                                            25, 298, $w560, $schemeColorNo, 2, 0));

                                        // 横隔板底
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, 74 + 25, 0,
                                            ($height - 25 - 74), 25, 298, $w560, $schemeColorNo, 2, 0));

                                        // 底担
                                        $s_products[] = ($this->getProductLogic()->createZHG_DD(0, 73, 0,
                                            ($height - 73), 25, 72, $w560, $schemeColorNo, 0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(0,
                                                $height - $g * (320), 0, $g * (320), 25, 298, $w560, $schemeColorNo, 2,
                                                0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }
                                    for ($c = 0; $c < $i784; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w784);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);
                                        $s_products = [];

                                        // 背板
//                                        $s_products[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 5,
//                                            $height - 74, $w784, $schemeColorNo, 0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 653, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        }

                                        // 横隔板顶
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0,
                                            25, 298, $w784, $schemeColorNo, 2, 0));

                                        // 横隔板底
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, 74 + 25, 0,
                                            ($height - 25 - 74), 25, 298, $w784, $schemeColorNo, 2, 0));

                                        // 底担
                                        $s_products[] = ($this->getProductLogic()->createZHG_DD(0, 73, 0,
                                            ($height - 73), 25, 72, $w784, $schemeColorNo, 0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(0,
                                                $height - $g * (320), 0, $g * (320), 25, 298, $w784, $schemeColorNo, 2,
                                                0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }

                                    // 固定功能体
                                    $t_scheme = new SchemeBean();
                                    $t_scheme = $this->getChuizhiXZZCol($schemeColorNo, $zhWidthArr[$i] * 2 + 25);

                                    array_splice($t_schemes, intval(($colcount - 1) / 2), 0, [$t_scheme]);

                                    // 内侧板
                                    $x = 16;
                                    for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                                        if (0 == $n) {
                                            $x = $x + $t_schemes[$n]->getScheme_width();
                                        } else {
                                            $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                                        }

                                        if ("" != $t_schemes[$n]->getScheme_no() || "" != $t_schemes[$n + 1]->getScheme_no()) {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZJXJCB($x, 0, $x,
                                                0, 25, $height, 314, $schemeColorNo, 0));
                                        } else {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB($x, 0, $x, 0, 25,
                                                $height, 314, $schemeColorNo, 0));
                                        }
                                    }

                                    $s_scheme->setScheme_wcb_products($wcb_products);
                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_schemes($t_schemes);

                                    $s_scheme->setScheme_error_range($errorrange);
//                                    $s_pic = "SJSC1" . $s_scheme->getScheme_width() . "_"
//                                        . $s_scheme->getScheme_height() . "_"
//                                        . $s_scheme->getScheme_color_no()
//                                        . "_" . count($t_schemes) . "_"
//                                        . getRandomInt();
                                    $this->createSchemePic($s_scheme);
                                    $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());
                                }
                                $s_schemes[] = ($s_scheme);
                            }
                        }
                    }
                }
            }
        }
        $s_schemes = array_reverse($s_schemes);
        if (empty($scheme->getScheme_schemes())) {
            $scheme->setScheme_schemes($s_schemes);
        } else {
            $scheme->setScheme_schemes(array_merge($scheme->getScheme_schemes(), $s_schemes));
        }
    }

    //平行写字桌
    public function createZHGSJSCSchemeYZZ(SchemeBean $scheme, $isMath)
    {
        $inputwidth = $scheme->getScheme_width();
        $height = $scheme->getScheme_height();
        if ($height < 2019) {
            $height = $height - 16;
        }
        $width = 0;// 家具宽度
        $errorrange = $scheme->getScheme_error_range();

        $zhWidthArr = [432, 560];
        $colWidthArr = [432, 560, 784];

        $s_scheme = null;
        $s_schemes = [];
        $t_scheme = null;
        $t_schemes = null;
        $s_products = null;

        $wcb_products = null;
        $ncb_products = null;

        $acc_products = null;
        $t_p = null;

        $w432 = 432;
        $w560 = 560;
        $w784 = 784;

        $colcount = 1;
        $gridcount = intval(($height - 74) / 320);

        $schemeColorNo = $scheme->getScheme_color_no();

        for ($i = 0; $i < count($zhWidthArr); $i++) {

            // 单列
            $width = $zhWidthArr[$i] * 2 + 25 + 50;

            if (abs($inputwidth - $width) < $errorrange) {
                $s_scheme = new SchemeBean();
                if (!$isMath) {
                    $t_schemes = [];
                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                    $s_scheme->setScheme_hole_width($inputwidth);
                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                    $s_scheme->setScheme_width($width);
                    $s_scheme->setScheme_height($scheme->getScheme_height());
                    $s_scheme->setScheme_wcb_type(1);
                    $s_scheme->setScheme_deep(314);
                    $s_scheme->setScheme_wcb_products($wcb_products);
                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_color_no($schemeColorNo);
                    $s_scheme->setScheme_type($scheme->getScheme_type());
                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                    $wcb_products = [];
                    $ncb_products = [];

                    // 外侧板
                    $wcb_products[] = ($this->getProductLogic()->createZHG_WCB_ZJ(0, 0, 0, 0, 25, $height, 314,
                        $schemeColorNo, 0));
                    $s_scheme->setScheme_wcb_type(0);
                    $wcb_products[] = ($this->getProductLogic()->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25, $height,
                        314, $schemeColorNo, 0));

                    // 固定功能体
                    $t_scheme = new SchemeBean();

                    $t_scheme = $this->getPingxingXZZCol($schemeColorNo, $zhWidthArr[$i] * 2 + 25);

                    $t_schemes[] = ($t_scheme);

                    $s_scheme->setScheme_wcb_products($wcb_products);
                    // s_scheme.setScheme_ncb_products(ncb_products);
                    $s_scheme->setScheme_schemes($t_schemes);

                    $s_scheme->setScheme_error_range($errorrange);
//                    $s_pic = "SJSC0" . $s_scheme->getScheme_width() . "_"
//                        . $s_scheme->getScheme_height() . "_"
//                        . $s_scheme->getScheme_color_no() . "_"
//                        . count($t_schemes) . "_"
//                        . getRandomInt();
                    $this->createSchemePic($s_scheme);
                    $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());

                }

                $s_schemes[] = ($s_scheme);
            }

            // 两列
            for ($c = 0; $c < 3; $c++) {
                $width = $zhWidthArr[$i] * 2 + 25 + 25 + 25 + 16 + $colWidthArr[$c];
                if (abs($inputwidth - $width) < $errorrange) {
                    $s_scheme = new SchemeBean();
                    if (!$isMath) {
                        $t_schemes = [];
                        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                        $s_scheme->setScheme_hole_width($inputwidth);
                        $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                        $s_scheme->setScheme_width($width);
                        $s_scheme->setScheme_height($scheme->getScheme_height());
                        $s_scheme->setScheme_wcb_type(1);
                        $s_scheme->setScheme_deep(314);
                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_color_no($schemeColorNo);
                        $s_scheme->setScheme_type($scheme->getScheme_type());
                        $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                        $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                        $wcb_products = [];
                        $ncb_products = [];

                        // 外侧板
                        $wcb_products[] = ($this->getProductLogic()->createZHG_WCB(0, 0, 0, 0, 16, $height, 314,
                            $schemeColorNo, 0));
                        $s_scheme->setScheme_wcb_type(1);
                        $wcb_products[] = ($this->getProductLogic()->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25,
                            $height, 314, $schemeColorNo, 0));

                        // 标准列
                        $t_scheme = new SchemeBean();
                        $t_scheme->setScheme_width($colWidthArr[$c]);
                        $t_scheme->setScheme_height($height);
                        $t_scheme->setScheme_color_no($schemeColorNo);

                        $s_products = [];

                        // 背板
//                        $s_products[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 5, $height - 74,
//                            $colWidthArr[$c], $schemeColorNo, 0));
                        if ($height <= 425) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 345, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 745) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1065) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1385) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 1705) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 653, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        } elseif ($height <= 2025) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $schemeColorNo, 0);
                        }



                        // 横隔板顶
                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0, 25, 298,
                            $colWidthArr[$c], $schemeColorNo, 2, 0));

                        // 横隔板底
                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74),
                            25, 298, $colWidthArr[$c], $schemeColorNo, 2, 0));

//                        // 底担
                        $s_products[] = ($this->getProductLogic()->createZHG_DD(0, 72, 0, ($height - 72), 25, 72,
                            $colWidthArr[$c], $schemeColorNo, 0));

                        // 多层横隔板
                        for ($g = 1; $g < $gridcount; $g++) {
                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(0, $height - $g * (320), 0,
                                $g * (320), 25, 298, $colWidthArr[$c], $schemeColorNo, 2, 0));
                        }

                        $t_scheme->setScheme_products($s_products);
                        $t_schemes[] = ($t_scheme);

                        // 固定功能体
                        $t_scheme = new SchemeBean();
                        $t_scheme = $this->getPingxingXZZCol($schemeColorNo, $zhWidthArr[$i] * 2 + 25);

                        // t_scheme.setScheme_width(zhWidthArr[i] * 2 + 25);
                        // t_scheme.setScheme_height(height);
                        // t_scheme.setScheme_no("Z-DZX" + zhWidthArr[i] + "*2-"
                        // + scheme.getScheme_color_no());
                        // t_scheme.setScheme_name(zhWidthArr[i] +
                        // "*2写字桌组件");
                        // t_scheme.setScheme_b_type(scheme.getScheme_b_type());
                        // t_scheme.setScheme_s_type(scheme.getScheme_s_type());
                        // t_scheme.setScheme_color_no(scheme.getScheme_color_no());
                        // t_scheme.setScheme_color_name(SchemeConstant.schemeColorNameArr[Integer.parseInt(scheme.getScheme_color_no())]);
                        // s_products = new ArrayList<Product>();
                        //
                        // // 背板
                        // s_products.add(productGeneration.createZHG_BB(0,
                        // height, 0, 0, 5, height - 1379, zhWidthArr[i] * 2 +
                        // 25, scheme.getScheme_color_no(), 0));
                        //
                        // // 横隔板顶
                        // s_products.add(productGeneration.createZHG_HGB_GD(0,
                        // height, 0, 0, 25, 298, zhWidthArr[i],
                        // scheme.getScheme_color_no(), 2, 0));
                        //
                        // s_products.add(productGeneration.createZHG_HGB_GD(zhWidthArr[i]
                        // + 25, height, zhWidthArr[i] + 25, 0, 25, 298,
                        // zhWidthArr[i], scheme.getScheme_color_no(), 0, 0));
                        //
                        // s_products.add(productGeneration.createZHG_ZCB(zhWidthArr[i],
                        // height, zhWidthArr[i], 0, 25, height - 1379, 314,
                        // scheme.getScheme_color_no(), 0));
                        //
                        // // 多层横隔板
                        // for (int g = 1; g < gridcount - 4; g++) {
                        // s_products.add(productGeneration.createZHG_HGB_GD(0,
                        // height - g * (320), 0, g * (320), 25, 298,
                        // zhWidthArr[i], scheme.getScheme_color_no(), 2, 0));
                        //
                        // s_products.add(productGeneration.createZHG_HGB_GD(zhWidthArr[i]
                        // + 25, height - g * (320), zhWidthArr[i] + 25, g *
                        // (320), 25, 298, zhWidthArr[i],
                        // scheme.getScheme_color_no(), 2, 0));
                        // }
                        //
                        // s_products.add(productGeneration.createZHG_HGB_GD(0,
                        // 1379, 0, height - 1379, 25, 298, zhWidthArr[i] * 2 +
                        // 25, scheme.getScheme_color_no(), 0, 0));
                        //
                        // t_p = productGeneration.createZHG_ZZ_M(0, 745, 0,
                        // height - 745, 25, 550, zhWidthArr[i] * 2 + 25,
                        // scheme.getScheme_color_no(), 0, 0);
                        //
                        // acc_products = new ArrayList<Product>();
                        //
                        // acc_products.add(productGeneration.createZHG_Acc((zhWidthArr[i]
                        // * 2 + 25 - 59 * 8) / 2 / 8 + 10, (height - 745) / 8 -
                        // 40, 40, 59, "FunctionUnit/notebook3", "电脑", 0, 0));
                        // acc_products.add(productGeneration.createZHG_Acc((zhWidthArr[i]
                        // * 2 + 25 - 83 * 8) / 2 / 8, height / 8 - 126 + 8,
                        // 126, 83, "FunctionUnit/chair3", "椅子", 0, 0));
                        //
                        // t_p.setAcc_products(acc_products);
                        //
                        // s_products.add(t_p);
                        //
                        // // 底担
                        // s_products.add(productGeneration.createZHG_DD(0, 745
                        // - 25, 0, (height - 745 + 25), 25, 73, zhWidthArr[i] *
                        // 2 + 25, scheme.getScheme_color_no(), 0));
                        //
                        // t_scheme.setScheme_products(s_products);

                        $t_schemes[] = ($t_scheme);

                        // 内侧板
                        $x = 16;
                        for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                            if (0 == $n) {
                                $x = $x + $t_schemes[$n]->getScheme_width();
                            } else {
                                $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                            }

                            if (!empty($t_schemes[$n]->getScheme_no())
                                || !empty($t_schemes[$n + 1]->getScheme_no())
                            ) {
                                $ncb_products[] = $this->getProductLogic()->createZHG_ZJXJCB(
                                    $x,
                                    0,
                                    $x,
                                    0,
                                    25,
                                    $height,
                                    314,
                                    $schemeColorNo,
                                    0);
                            } else {
                                $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB($x, 0, $x, 0, 25, $height,
                                    314, $schemeColorNo, 0));
                            }
                        }

                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_error_range($errorrange);
                        $s_scheme->setScheme_schemes($t_schemes);

//                        $s_pic = "SJSC0" . $s_scheme->getScheme_width()
//                            . "_" . $s_scheme->getScheme_height() . "_"
//                            . $s_scheme->getScheme_color_no() . "_"
//                            . count($t_schemes) . "_"
//                            . getRandomInt();
                        $this->createSchemePic($s_scheme);
                        $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());

                    }

                    $s_schemes[] = ($s_scheme);
                }
            }

            // 多于2列
            $max432colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w432);
            $max560colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w560);
            $max784colcount = intval(($inputwidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w784);

            for ($i784 = 0; $i784 <= $max784colcount; $i784++) {
                for ($i560 = 0; $i560 <= $max560colcount; $i560++) {
                    for ($i432 = 0; $i432 <= $max432colcount; $i432++) {
                        $colcount = $i784 + $i560 + $i432 + 1;

                        if (3 <= $colcount) {
                            $width = 25 * ($colcount - 1) + $zhWidthArr[$i] * 2 + 25 + 2 * 16 + $i432 * $w432 + $i560 * $w560 + $i784 * $w784;
                            if (abs($inputwidth - $width) < $errorrange) {
                                $s_scheme = new SchemeBean();
                                if (!$isMath) {
                                    $t_schemes = [];
                                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeColorNo));
                                    $s_scheme->setScheme_hole_width($inputwidth);
                                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_width($width);
                                    $s_scheme->setScheme_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_wcb_type(1);
                                    $s_scheme->setScheme_deep(314);
                                    $s_scheme->setScheme_wcb_products($wcb_products);
                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_color_no($schemeColorNo);
                                    $s_scheme->setScheme_type($scheme->getScheme_type());
                                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                                    $wcb_products = [];
                                    $ncb_products = [];

                                    // 外侧板
                                    $wcb_products[] = $this->getProductLogic()->createZHG_WCB(
                                        0,
                                        0,
                                        0,
                                        0,
                                        16,
                                        $height,
                                        314,
                                        $schemeColorNo,
                                        0);
                                    $s_scheme->setScheme_wcb_type(1);
                                    $wcb_products[] = $this->getProductLogic()->createZHG_WCB(
                                        0,
                                        0,
                                        ($width - 16),
                                        0,
                                        16,
                                        $height,
                                        314,
                                        $schemeColorNo,
                                        0);

                                    // 组合功能单元

                                    for ($c = 0; $c < $i432; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w432);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);
                                        $s_products = [];

//                                        // 背板
//                                        $s_products[] = ($this->getProductLogic()->createZHG_BB(
//                                            0,
//                                            $height,
//                                            0,
//                                            0,
//                                            5,
//                                            $height - 74,
//                                            $w432,
//                                            $schemeColorNo,
//                                            0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653 ,
                                                5, 653, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 653, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w432, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $schemeColorNo, 0);
                                        }


                                        // 横隔板顶
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(
                                            0,
                                            $height,
                                            0,
                                            0,
                                            25,
                                            298,
                                            $w432,
                                            $schemeColorNo,
                                            2, 0));

                                        // 横隔板底
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(
                                            0,
                                            74 + 25,
                                            0,
                                            ($height - 25 - 74),
                                            25,
                                            298,
                                            $w432,
                                            $schemeColorNo,
                                            2, 0));

//                                        // 底担
                                        $s_products[] = ($this->getProductLogic()->createZHG_DD(
                                            0,
                                            73,
                                            0,
                                            ($height - 73),
                                            25,
                                            72,
                                            $w432,
                                            $schemeColorNo,
                                            0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(
                                                0,
                                                $height
                                                - $g
                                                * (320),
                                                0,
                                                $g * (320),
                                                25,
                                                298,
                                                $w432,
                                                $schemeColorNo,
                                                2, 0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }
                                    for ($c = 0; $c < $i560; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w560);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);

                                        $s_products = [];

                                        // 背板
//                                        $s_products[] =
//                                            ($this->getProductLogic()->createZHG_BB(
//                                                0,
//                                                $height,
//                                                0,
//                                                0,
//                                                5,
//                                                $height - 74,
//                                                $w560,
//                                                $schemeColorNo,
//                                                0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653 ,
                                                5, 653, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973 ,
                                                5, 653, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973 ,
                                                5, 973, $w560, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $schemeColorNo, 0);
                                        }

                                        // 横隔板顶
                                        $s_products[] =
                                            (
                                            $this->getProductLogic()->createZHG_HGB_GD(
                                                0,
                                                $height,
                                                0,
                                                0,
                                                25,
                                                298,
                                                $w560,
                                                $schemeColorNo,
                                                2, 0));

                                        // 横隔板底
                                        $s_products[] =
                                            ($this->getProductLogic()->createZHG_HGB_GD(
                                                0,
                                                74 + 25,
                                                0,
                                                ($height - 25 - 74),
                                                25,
                                                298,
                                                $w560,
                                                $schemeColorNo,
                                                2, 0));

                                        // 底担
                                        $s_products[] = (
                                        $this->getProductLogic()->createZHG_DD(
                                            0,
                                            73,
                                            0,
                                            ($height - 73),
                                            25,
                                            72,
                                            $w560,
                                            $schemeColorNo,
                                            0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] = ($this->getProductLogic()->createZHG_HGB(
                                                0,
                                                $height
                                                - $g
                                                * (320),
                                                0,
                                                $g * (320),
                                                25,
                                                298,
                                                $w560,
                                                $schemeColorNo,
                                                2, 0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }
                                    for ($c = 0; $c < $i784; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w784);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($schemeColorNo);
                                        $s_products = [];

                                        // 背板
//                                        $s_products[] =
//                                            ($this->getProductLogic()->createZHG_BB(
//                                                0,
//                                                $height,
//                                                0,
//                                                0,
//                                                5,
//                                                $height - 74,
//                                                $w784,
//                                                $schemeColorNo,
//                                                0));
                                        if ($height <= 425) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973 ,
                                                5, 653, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w784, $schemeColorNo, 0);
                                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $schemeColorNo, 0);
                                        }

                                        // 横隔板顶
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(
                                            0,
                                            $height,
                                            0,
                                            0,
                                            25,
                                            298,
                                            $w784,
                                            $schemeColorNo,
                                            2, 0));

                                        // 横隔板底
                                        $s_products[] = ($this->getProductLogic()->createZHG_HGB_GD(
                                            0,
                                            74 + 25,
                                            0,
                                            ($height - 25 - 74),
                                            25,
                                            298,
                                            $w784,
                                            $schemeColorNo,
                                            2, 0));

                                        // 底担
                                        $s_products[] = ($this->getProductLogic()->
                                        createZHG_DD(
                                            0,
                                            73,
                                            0,
                                            ($height - 73),
                                            25,
                                            72,
                                            $w784,
                                            $schemeColorNo,
                                            0));

                                        // 多层横隔板
                                        for ($g = 1; $g < $gridcount; $g++) {
                                            $s_products[] =
                                                ($this->getProductLogic()->createZHG_HGB(
                                                    0,
                                                    $height
                                                    - $g
                                                    * (320),
                                                    0,
                                                    $g * (320),
                                                    25,
                                                    298,
                                                    $w784,
                                                    $schemeColorNo,
                                                    2, 0));
                                        }

                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = ($t_scheme);
                                    }

                                    // 固定功能体
                                    $t_scheme = new SchemeBean();
                                    $t_scheme = $this->getPingxingXZZCol(
                                        $schemeColorNo,
                                        $zhWidthArr[$i] * 2 + 25);

                                    // t_scheme.setScheme_b_type("XZZ_H");
                                    // t_scheme.setScheme_width(zhWidthArr[i] *
                                    // 2 + 25);
                                    // t_scheme.setScheme_height(height);
                                    // t_scheme.setScheme_no("Z-DZX" +
                                    // zhWidthArr[i] + "*2-" +
                                    // scheme.getScheme_color_no());
                                    // t_scheme.setScheme_name(zhWidthArr[i] +
                                    // "*2写字桌组件");
                                    // t_scheme.setScheme_b_type(scheme.getScheme_b_type());
                                    // t_scheme.setScheme_s_type(scheme.getScheme_s_type());
                                    // t_scheme.setScheme_color_no(scheme.getScheme_color_no());
                                    // t_scheme.setScheme_color_name(SchemeConstant.schemeColorNameArr[Integer.parseInt(scheme.getScheme_color_no())]);
                                    //
                                    // s_products = new ArrayList<Product>();
                                    //
                                    // // 背板
                                    // s_products.add(productGeneration.createZHG_BB(0,
                                    // height, 0, 0, 5, height - 1379,
                                    // zhWidthArr[i] * 2 + 25,
                                    // scheme.getScheme_color_no(), 0));
                                    //
                                    // // 横隔板顶
                                    // s_products.add(productGeneration.createZHG_HGB_GD(0,
                                    // height, 0, 0, 25, 298, zhWidthArr[i],
                                    // scheme.getScheme_color_no(), 2, 0));
                                    //
                                    // s_products.add(productGeneration.createZHG_HGB_GD(zhWidthArr[i]
                                    // + 25, height, zhWidthArr[i] + 25, 0, 25,
                                    // 298, zhWidthArr[i],
                                    // scheme.getScheme_color_no(), 0, 0));
                                    //
                                    // s_products.add(productGeneration.createZHG_ZCB(zhWidthArr[i],
                                    // height, zhWidthArr[i], 0, 25, height -
                                    // 1379, 314, scheme.getScheme_color_no(),
                                    // 0));
                                    //
                                    // // 多层横隔板
                                    // for (int g = 1; g < gridcount - 4; g++) {
                                    // s_products.add(productGeneration.createZHG_HGB_GD(0,
                                    // height - g * (320), 0, g * (320), 25,
                                    // 298, zhWidthArr[i],
                                    // scheme.getScheme_color_no(), 2, 0));
                                    //
                                    // s_products.add(productGeneration.createZHG_HGB_GD(zhWidthArr[i]
                                    // + 25, height - g * (320), zhWidthArr[i] +
                                    // 25, g * (320), 25, 298, zhWidthArr[i],
                                    // scheme.getScheme_color_no(), 2, 0));
                                    // }
                                    //
                                    // s_products.add(productGeneration.createZHG_HGB_GD(0,
                                    // 1379, 0, height - 1379, 25, 298,
                                    // zhWidthArr[i] * 2 + 25,
                                    // scheme.getScheme_color_no(), 0, 0));
                                    // t_p = productGeneration.createZHG_ZZ_M(0,
                                    // 745, 0, height - 745, 25, 550,
                                    // zhWidthArr[i] * 2 + 25,
                                    // scheme.getScheme_color_no(), 0, 0);
                                    //
                                    // acc_products = new ArrayList<Product>();
                                    //
                                    // acc_products.add(productGeneration.createZHG_Acc((zhWidthArr[i]
                                    // * 2 + 25 - 59 * 8) / 2 / 8 + 10, (height
                                    // - 745) / 8 - 40, 40, 59,
                                    // "FunctionUnit/notebook3", "电脑", 0, 0));
                                    // acc_products.add(productGeneration.createZHG_Acc((zhWidthArr[i]
                                    // * 2 + 25 - 83 * 8) / 2 / 8, height / 8 -
                                    // 126 + 8, 126, 83, "FunctionUnit/chair3",
                                    // "椅子", 0, 0));
                                    //
                                    // t_p.setAcc_products(acc_products);
                                    //
                                    // s_products.add(t_p);
                                    //
                                    // // 底担
                                    // s_products.add(productGeneration.createZHG_DD(0,
                                    // 745 - 25, 0, (height - 745 + 25), 25, 73,
                                    // zhWidthArr[i] * 2 + 25,
                                    // scheme.getScheme_color_no(), 0));
                                    //
                                    // t_scheme.setScheme_products(s_products);

                                    array_splice($t_schemes, intval(($colcount - 1) / 2), 0, [$t_scheme]);


                                    // 内侧板
                                    $x = 16;
                                    for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                                        if (0 == $n) {
                                            $x = $x + $t_schemes[$n]->getScheme_width();
                                        } else {
                                            $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                                        }

                                        if (!empty($t_schemes[$n]->getScheme_no())
                                            || !empty($t_schemes[$n + 1]->getScheme_no())
                                        ) {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZJXJCB(
                                                $x,
                                                0,
                                                $x,
                                                0,
                                                25,
                                                $height,
                                                314,
                                                $schemeColorNo,
                                                0));
                                        } else {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB(
                                                $x,
                                                0,
                                                $x,
                                                0,
                                                25,
                                                $height,
                                                314,
                                                $schemeColorNo,
                                                0));
                                        }
                                    }

                                    $s_scheme->setScheme_wcb_products($wcb_products);
                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_error_range($errorrange);
                                    $s_scheme->setScheme_schemes($t_schemes);

//                                    $s_pic = "SJSC0" . $s_scheme->getScheme_width() . "_"
//                                        . $s_scheme->getScheme_height() . "_"
//                                        . $s_scheme->getScheme_color_no()
//                                        . "_" . count($t_schemes) . "_"
//                                        . getRandomInt();
                                    $this->createSchemePic($s_scheme);
                                    $s_scheme->setScheme_name('书柜+写字桌组合-' . getRandomInt());
                                }

                                $s_schemes[] = ($s_scheme);
                            }
                        }
                    }
                }
            }
        }
        $s_schemes = array_reverse($s_schemes);
        if (empty($scheme->getScheme_schemes())) {
            $scheme->setScheme_schemes($s_schemes);
        } else {
            $scheme->setScheme_schemes(array_merge($scheme->getScheme_schemes(), $s_schemes));
        }
    }

    private function getPingxingXZZCol($color_no, $colWidth)
    {
        $zhWidthArr = [432, 560];
        $i = -1;
        for ($idx = 0; $idx < count($zhWidthArr); $idx++) {
            if (intval($colWidth) == intval($zhWidthArr[$idx] * 2 + 25)) {
                $i = $idx;
                break;
            }
        }
        $b_type = "SJSC";
        $s_type = '1';
        $height = 2019;

        // 固定功能体
        $col = new SchemeBean();
        $col->setScheme_b_type("XZZ_H");
        $col->setScheme_width($colWidth);
        $col->setScheme_height($height);
        $col->setScheme_no("Z-DZX" . $zhWidthArr[$i] . "*2-" . $color_no);
        $col->setScheme_name($zhWidthArr[$i] . "*2写字桌组件");
        $col->setScheme_b_type($b_type);
        $col->setScheme_s_type($s_type);
        $col->setScheme_color_no($color_no);
        $col->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
        $col_pros = [];

        // 背板
        $col_pros[] = ($this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 12,
            320 * 2 + 25, $colWidth, $color_no, 0));

        // 横隔板顶
        $col_pros[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0, 25,
            298, $colWidth, $color_no, 2, 0));

        // 中侧板
        $col_pros[] = ($this->getProductLogic()->createZHG_ZCB($zhWidthArr[$i], 25,
            $zhWidthArr[$i], 25, 25, 320 * 2 - 25, 298, $color_no, 0));

        // 横隔板中
        $col_pros[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height - 320, 0,
            320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0));

        $col_pros[] = ($this->getProductLogic()->createZHG_HGB_GD($zhWidthArr[$i] + 25,
            $height - 320, $zhWidthArr[$i] + 25, 320, 25, 298, $zhWidthArr[$i],
            $color_no, 2, 0));

        // 横隔板底
        $col_pros[] = ($this->getProductLogic()->createZHG_HGB_GD(0, $height - 320 * 2, 0,
            320 * 2, 25, 298, $colWidth, $color_no, 0, 0));

        $pro_desk_h = $this->getProductLogic()->createZHG_ZZ_M(0, 745, 0, $height - 745, 25, 550, $colWidth, $color_no,
            0, 0);

        $desk_pros = [];

        $desk_pros[] = ($this->getProductLogic()->createZHG_Acc(($colWidth - 59 * 8) / 2 + 80,
            ($height - 745) - 40 * 8, 40 * 8, 59 * 8,
            "FunctionUnit/notebook3", "电脑", 0, 0));
        $desk_pros[] = ($this->getProductLogic()->createZHG_Acc(
            ($colWidth - 83 * 8) / 2, $height - 126 * 8 + 8 * 8, 126 * 8, 83 * 8,
            "FunctionUnit/chair3", "椅子", 0, 0));

        $pro_desk_h->setAcc_products($desk_pros);

        $col_pros[] = ($pro_desk_h);

        // 桌面底担
        $col_pros[] = ($this->getProductLogic()->createZHG_DD(0, 745 - 25, 0, $height
            - (745 - 25), 25, 381, $colWidth, $color_no, 0));

        $col_pros[] = ($this->getProductLogic()->createZHG_DD(0, 745 - 25, 0, $height
            - (745 - 25), 16, 93, $colWidth, $color_no, 1));

        $col->setScheme_products($col_pros);
        return $col;
    }

    private function getChuizhiXZZCol($color_no, $colWidth)
    {
        $zhWidthArr = [432, 560];
        $i = -1;
        for ($idx = 0; $idx < count($zhWidthArr); $idx++) {
            if ($colWidth == $zhWidthArr[$idx] * 2 + 25) {
                $i = $idx;
                break;
            }
        }
        $b_type = "SJSC";
        $s_type = '0';
        $height = 2019;

        // 固定功能体
        $t_scheme = new SchemeBean();
        $t_scheme->setScheme_b_type("XZZ_V");
        $t_scheme->setScheme_width($zhWidthArr[$i] * 2 + 25);
        $t_scheme->setScheme_height($height);
        $t_scheme->setScheme_no("Z-DZG" . $zhWidthArr[$i] . "*2-" . $color_no);
        $t_scheme->setScheme_name($zhWidthArr[$i] . "*2垂直桌柜体组件");
        $t_scheme->setScheme_b_type($b_type);
        $t_scheme->setScheme_s_type($s_type);
        $t_scheme->setScheme_color_no($color_no);
        $t_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
        $t_scheme->setScheme_haveZZ(1);

        $col_pros = [];

        // 背板
        $col_pros[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0, 12, $height - 1379,
            $zhWidthArr[$i] * 2 + 25, $color_no, 0);

        // 多层横隔板
        // 横隔板顶
        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $colWidth, $color_no, 2, 0);

        // 中侧板
        $col_pros[] = $this->getProductLogic()->createZHG_ZCB($zhWidthArr[$i], 25, $zhWidthArr[$i], 25, 25,
            320 * 2 - 25, 298, $color_no, 0);

        // 横隔板中
        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD(0, $height - 320, 0, 320, 25, 298, $zhWidthArr[$i],
            $color_no, 2, 0);

        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height - 320,
            $zhWidthArr[$i] + 25, 320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);

        // 横隔板底
        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD(0, $height - 320 * 2, 0, 320 * 2, 25, 298, $colWidth,
            $color_no, 0, 0);

        // 平托板1
        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD(0, 1155, 0, $height - 1155, 25, 298,
            $zhWidthArr[$i] * 2 + 25, $color_no, 0, 0);

        // 平托板2
        $col_pros[] = $this->getProductLogic()->createZHG_HGB_GD(0, 739, 0, $height - 739, 25, 298,
            $zhWidthArr[$i] * 2 + 25, $color_no, 0, 0);

        // 托撑
        $col_pros[] = $this->getProductLogic()->createZHG_DD(0, 739 - 25, 0, $height - (739 - 25), 25, 381,
            $zhWidthArr[$i] * 2 + 25, $color_no, 0);

        $t_p = $this->getProductLogic()->createZHG_ZZ(0, 739 + 25, 0, $height - (739 + 25), 739 + 25, 314, 682,
            $color_no, 0, 0);

        $acc_products = [];

        $acc_products[] = $this->getProductLogic()->createZHG_Acc(30 * 8, ($height - (739 + 25)) - 36 * 8, 36 * 8, 48 * 8,
            "FunctionUnit/notebook1", "电脑", 0, 0);
        $acc_products[] = $this->getProductLogic()->createZHG_Acc(682 - 35 * 8, $height - 106 * 8 + 2 * 8, 106 * 8, 77 * 8,
            "FunctionUnit/chair1", "椅子", 0, 0);

        $acc_products[] = $this->getProductLogic()->createZHG_Acc(10 * 8, ($height - (739 + 25)) - 57 * 8, 57 * 8, 44 * 8,
            "FunctionUnit/desklamp1", "台灯", 0, 0);

        $t_p->setAcc_products($acc_products);

        // 桌子放右侧
        $t_p->setM_left(intval(($colWidth - $t_p->getProduct_width()) / 8) + 1);
        $t_p->setM_left_mm($colWidth - $t_p->getProduct_width());
        if (!empty($t_p->getAcc_products())) {
            foreach ($t_p->getAcc_products() as $a_p) {
                $a_p = change2ProductBean($a_p);
                if ("电脑" == ($a_p->getProduct_name())) {
                    $a_p->setM_left(intval($colWidth / 8) - 30 - 48);
                    $a_p->setM_left_mm($colWidth - 30 * 8 - 48 * 8);
                    $a_p->setProduct_pic("../Adornment/Cabinetoffice/FunctionUnit/notebook2.png");
                } else {
                    if ("椅子" == ($a_p->getProduct_name())) {
                        $a_p->setM_left(intval($colWidth / 8) - intval(682 / 8) - 30);
                        $a_p->setM_left_mm($colWidth - 682 - 77 * 8 + 35 * 8);
                        $a_p->setProduct_pic("../Adornment/Cabinetoffice/FunctionUnit/chair2.png");
                    } else {
                        if ("台灯" == ($a_p->getProduct_name())) {
                            $a_p->setM_left(intval($colWidth / 8) - 10 - 44);
                            $a_p->setM_left_mm($colWidth - 10 * 8 - 44 * 8);
                            $a_p->setProduct_pic("../Adornment/Cabinetoffice/FunctionUnit/desklamp2.png");
                        }
                    }
                }
            }
        }
        $t_scheme->setScheme_dir(1);
        $col_pros[] = $t_p;
        $t_scheme->setScheme_products($col_pros);
        return $t_scheme;
    }
}
