<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 17-12-19
 * Time: 上午10:30
 */

namespace app\api\logic;


use app\common\model\SchemeBean;
use app\common\tools\ImageUtils;
use app\common\tools\RedisUtil;
use think\Config;
use think\Db;

class SchemeBaseLogic
{
    private $cache_enable = 1;
    private $mongo_config = [];

    protected $mapping = [
        'many' => ['rqs', 'qw', 'zhgcwsn', 'zhgsjsc', 'zhgyyst'],
        'one' => ['bz'],
    ];

    protected function beforeCreate($scheme_type, $condition)
    {
        $config = Config::get('mongo');
        $this->cache_enable = $config['enable'];
        $this->mongo_config = $config;

        if ($this->cache_enable == 0) {
            return [];
        }
        $handle = in_array($scheme_type, $this->mapping['many']) ? 'select' : 'find';
        $result = Db::connect($this->mongo_config)->name($scheme_type)->field(['_id'], true)->where(['search_index' => $condition])->$handle();
        return $result;
    }

    protected function afterCreate($scheme_type, $result)
    {
        if ($this->cache_enable == 1) {
            $handle = in_array($scheme_type, $this->mapping['many']) ? 'insertAll' : 'insert';
            Db::connect($this->mongo_config)->name($scheme_type)->$handle($result);
        }
    }

    public function flushMongoCache()
    {
        $con = Db::connect('mongo');
        $where = ['_id' => ['neq', 0]];
        $con->name('rqs')->where($where)->delete();
        $con->name('qw')->where($where)->delete();
        $con->name('bz')->where($where)->delete();
        $con->name('zhgcwsn')->where($where)->delete();
        $con->name('zhgsjsc')->where($where)->delete();
        $con->name('zhgyyst')->where($where)->delete();
    }

    protected function createSchemePic(SchemeBean $scheme)
    {
        $s_pic = $scheme->getHashCode();
        if (!RedisUtil::getInstance()->sIsMember('scheme_pic', $s_pic)) {
            $imageUtils = new ImageUtils();
            switch ($scheme->getScheme_type()) {
                case 'RQS':
                    $imageUtils->createRQSSchemeBasic($scheme, EXTEND_PATH, $s_pic);
                    break;
                case 'QW':
                    $imageUtils->createQWSchemeBasic($scheme, EXTEND_PATH, $s_pic);
                    break;
                case 'BZ':
                    $imageUtils->createBZSchemeAll($scheme, EXTEND_PATH, $s_pic);
                    break;
                case 'ZH':
                    $imageUtils->createZHGSchemeBasic($scheme, EXTEND_PATH, true, true, $s_pic);
                    break;
            }
        }
        $scheme->setScheme_pic($s_pic . '.png');
    }
}