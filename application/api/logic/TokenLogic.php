<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/11
 * Time: 17:16
 */

namespace app\api\logic;

use app\common\model\UserToken as TokenModel;
class TokenLogic
{
    public static function storeToken($userId)
    {
        $TokenModel = new TokenModel();
        $resultData = false;

        if (empty($userId)) {
            return $resultData;
        }

        $tokenData = [
            'user_id' => $userId,
            'token' => md5($userId . uniqid(mt_rand(), true)),
            'is_logout' => 'no',
            'create_time' => time(),
            'expiry_time' => time() + config('sys')['token_expiry_time'],
        ];

        $findData = $TokenModel->where(['user_id' => $userId])->find();
        if (empty($findData)) {
            // 新增数据
            $bool =  $TokenModel->allowField(true)->insert($tokenData);
        } else {
            // 更新数据
            if (config('app_debug')) {
                // 调试模式下，多端登录token不变
                $tokenData['token'] = $findData['token'];
            }
            $bool = $TokenModel->isUpdate(true)->allowField(true)->save($tokenData,['user_id'=>$userId]);
        }
        return empty($bool)?false:$tokenData['token'];
    }
}