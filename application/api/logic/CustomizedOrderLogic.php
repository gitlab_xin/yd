<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:19
 */

namespace app\api\logic;

use app\common\model\CustomizedOrder;
use app\common\model\CustomizedEvaluate as EvaluateModel;
use app\common\model\ShopOrderLogistics;
use app\common\model\ShopOrderOfficialLogistics;
use app\common\model\ShopOrderRefund;
use app\common\tools\RedisUtil;
use think\Config;
use think\Db;
use think\Exception;

class CustomizedOrderLogic
{
    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:根据用户获取我的订单
     * @param $data
     * @return array|bool
     */
    public function getOrdersByUser($data)
    {
        try {
            $result = ['orders' => [], 'count' => 0];
            $where = ['o.user_id' => $data['user_id']];
            //状态筛选
            switch ($data['status']) {
                case 'unpaid':
                    $where['o.status'] = '未支付';
                    break;
                case 'preparing':
                    $where['o.status'] = '待发货';
                    break;
                case 'delivering':
                    $where['o.status'] = '待收货';
                    break;
                case 'toevaluate':
                    $where['o.status'] = '待评价';
                    break;
                case 'refunded':
                    $where['o.status'] = ['in', ['退款中', '已退款']];
                    break;
            }
            //搜索关键字筛选
            if (isset($data['keyword'])) {
                $where[] = function ($query) use ($data) {
                    $query->where(['s.scheme_type_text' => ['exp', "REGEXP '{$data['keyword']}'"]]);
                };
            }
            $orders = CustomizedOrder::build()->alias('o')
                ->field([
                    'o.order_id',
                    'o.logistics_id',
                    'o.order_num',
                    'o.price',
                    'o.count',
                    'o.status',
                    'o.pay_type',
                    'o.create_time',
                    's.scheme_type_text',
                    's.scheme_pic',
                    's.scheme_width',
                    's.scheme_height',
                    's.scheme_deep'
                ])
                ->join('yd_customized_order_scheme s', 's.order_id = o.order_id')
                ->where($where)
                ->order(['o.order_id' => 'DESC'])
                ->page($data['page_index'], $data['page_size'])
                ->select()->toArray();

            if ($orders) {
                foreach ($orders as &$value) {
                    $value['pay_type'] = Config::get('mall.order')['pay_type'][$value['pay_type']];
                }
                $result['orders'] = $orders;
                $result['count'] = CustomizedOrder::build()->alias('o')
                    ->join('yd_customized_order_scheme s', 's.order_id = o.order_id')
                    ->where($where)->count();
            }
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:获取用户单个订单详情
     * @param $data
     * @return array
     */
    public function getOrderDetail($data)
    {
        $result = CustomizedOrder::build()->alias('o')
            ->field([
                'o.order_id',
                'o.order_num',
                'o.price',
                'o.count',
                'o.status',
                'o.user_remarks',
                'o.pay_type',
                'o.pay_time',
                'o.delivery_time',
                'o.create_time',
                'o.logistics_id',
                'o.scheme_id',
                'os.scheme_type_text',
                'os.scheme_pic',
                'os.scheme_width',
                'os.scheme_height',
                'os.scheme_deep',
                'oc.scheme_price',
                'oc.discount',
                'oc.scheme_dis_price',
                'oc.freight_charge',
                'oc.processing_charge',
                'oc.home_charge',
                'oc.service_charge',
                'oa.province',
                'oa.city',
                'oa.county',
                'oa.town',
                'oa.detail',
                'oa.username',
                'oa.phone',
                'o.cancel_reason',
                'or.content',
                'or.reason',
                'or.refuse_time',
                'or.refuse_content',
            ])
            ->join('yd_customized_order_charge oc', 'o.order_id = oc.order_id', 'left')
            ->join('yd_customized_order_scheme os', 'o.order_id = os.order_id', 'left')
            ->join('yd_customized_order_area oa', 'o.order_id = oa.order_id', 'left')
            ->join('order_refund or', 'o.order_id = or.order_id', 'left')
            ->where(['o.order_id' => $data['order_id'], 'o.user_id' => $data['user_id']])
            ->find();
        if ($result) {
            $result['pay_type'] = Config::get('mall.order')['pay_type'][$result['pay_type']];
            unset($result['img_src_list']);
        }
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:确认收货
     * @param $data
     * @return int
     */
    public function confirmOrder($data)
    {
        if (($result = CustomizedOrder::build()->where($data)->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '待收货') {
            //只有待收货的订单才能确认收货
            return 1;
        }
        $result->status = '待评价';
        Db::startTrans();
        $bool = $result->save();
        if (!$bool) {
            Db::rollback();
            return 2;
        }

        try {
            //查询物流状态 如果 neq 已签收 则修改为已签收
            $logistics = ShopOrderLogistics::build()->where(['logistics_id' => $result['logistics_id']])->find();
            if ($logistics['logistics_type'] != 'official') {
                Db::commit();
                return 9;
            }
            if ($logistics['status'] == '已签收') {
                Db::commit();
                return 9;
            }
            $bool = ShopOrderLogistics::build()->update(['status' => '已签收'],['logistics_id' => $result['logistics_id']]);
            if (!$bool) {
                Db::rollback();
                return 2;
            }
            //写入一条物流消息
            $bool = ShopOrderOfficialLogistics::build()->insert(
                ['user_id' => $logistics['user_id'], 'logistics_id' => $logistics['logistics_id'], 'content' => Config::get('logistics.endContent'),'create_time'=>time()]
            );
            if (!$bool) {
                Db::rollback();
                return 2;
            }

            //成功
            Db::commit();
            return 9;
        } catch (Exception $exception) {
            Db::rollback();
            return 2;
        }
    }

    /**
     * @author: Airon
     * @time: 2017年9月118日
     * description:修改规定时间内未支付订单状态
     */
    public function cancelExpiredOrders()
    {
        $deadline = Config::get('mall.order')['deadline'];
        $fields = ['order_id', 'count'];
        $where = [
            'status' => '未支付',
            'create_time' => ['lt', time() - $deadline]
        ];
        $lists = CustomizedOrder::build()->field($fields)->where($where)->select()->toArray();

        if ($lists) {
            $redis = RedisUtil::getInstance();
            $orderData = ['status' => '已取消'];
            foreach ($lists as $row) {
                Db::startTrans();
                try {
                    if (!CustomizedOrder::update($orderData, ['order_id' => $row['order_id']], true)) {
                        throw new \Exception('订单状态修改失败');
                    }
                    Db::commit();
                    $redis->hDel('order_pay_info', $row['bill_no']);
                } catch (\Exception $e) {
                    Db::rollback();
                }
            }
        }
    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:申请退款
     * @param $data
     * @return int
     */
    public function cancelOrder($data)
    {
        if (($result = CustomizedOrder::build()->where(['order_id'=>$data['order_id']])->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '待发货') {
            //只有待发货的订单才能申请退款
            return 1;
        } else {
            $result->status = '退款中';
            Db::startTrans();
            $bool = $result->save();
            if ($bool === false) {
                //失败
                Db::rollback();
                return 2;
            }
            $refundData = $data;
            $refundData['order_type'] = 'customized';
            $refundData['create_time'] = time();
            $bool = ShopOrderRefund::build()->insert($refundData);
            if ($bool) {
                //成功
                Db::commit();
                return 9;
            } else {
                //失败
                Db::rollback();
                return 2;
            }
        }

    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:评价订单
     * @param $data
     * @return int|mixed
     */
    public static function evaluate($data)
    {
        if (($order = CustomizedOrder::build()->where(['user_id' => $data['user_id'], 'order_id' => $data['order_id']])->find()) == null) {
            //订单不存在
            return 0;
        }
        if (EvaluateModel::build()->where(['user_id' => $data['user_id'], 'order_id' => $data['order_id']])->find()) {
            //已评价过
            return 2;
        }

        Db::startTrans();
        try {
            if (!$order->isUpdate()->save(['status' => '已完成'])) {
                throw new \Exception('', -2);
            }
            if (!EvaluateModel::create($data)) {
                throw new \Exception('', -1);
            }
            Db::commit();
            return 1;
        } catch (\Exception $e) {
            Db::rollback();
            return $e->getCode();
        }

    }


    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:获取订单评价
     * @param $data
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getOrderEvaluate($data)
    {
        return EvaluateModel::build()->alias('e')
            ->field([
                'os.scheme_type_text',
                'os.scheme_width',
                'os.scheme_height',
                'os.scheme_deep',
                'os.scheme_pic',
                'e.evaluate_id',
                'e.materials_star',
                'e.service_star',
                'e.process_star',
                'e.install_star',
                'e.delivery_star',
                'e.content',
                'e.create_time',
            ])
            ->join('yd_customized_order_scheme os', 'os.order_id = e.order_id')
            ->where(['e.user_id' => $data['user_id'], 'e.order_id' => $data['order_id']])
            ->find();
    }


    public function cancelUnpaidOrder($data)
    {
        $where = ['user_id'=>$data['user_id'],'order_id'=>$data['order_id']];
        if (($result = CustomizedOrder::build()->where($where)->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '未支付') {
            //只有未支付的订单才能取消订单
            return 1;
        } else {
            $result->status = '已取消';
            $result->cancel_reason = $data['cancel_reason'];
            if ($result->save()) {
                //成功
                return 9;
            } else {
                //失败
                return 2;
            }
        }

    }
}