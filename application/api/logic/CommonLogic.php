<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/17
 * Time: 15:19
 */

namespace app\api\logic;

use app\common\model\ForumArticle;
use app\common\model\Works;

class CommonLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:获取用户的点赞/评论/收藏总数
     * @param $user_id
     * @return mixed
     */
    public static function getEverythingSum($user_id)
    {
        $where['user_id'] = $user_id;
        $fields = ['sum(praise_count) as praise_count','sum(comment_count) as comment_count','sum(collect_count) as collect_count'];
        $forumCount = ForumArticle::build()->field($fields)->where($where)->find();
        $worksCount = Works::build()->field($fields)->where($where)->find();

        $result['praise_sum'] = $forumCount['praise_count'] + $worksCount['praise_count'];
        $result['comment_sum'] = $forumCount['comment_count'] + $worksCount['comment_count'];
        $result['collect_sum'] = $forumCount['collect_count'] + $worksCount['collect_count'];

        return $result;
    }
}