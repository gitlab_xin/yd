<?php
namespace app\api\logic;

use app\common\model\MyAssembleBuildingsLabel;
use app\common\model\UserLabel;

class SurveyLogic {

    public static function datas($user_id){
        $count = UserLabel::build()
            ->where(['user_id' => $user_id])
            ->count();
        $data['is_save'] = ($count >= 1) ? 1 : 0;
        $data['list'] = MyAssembleBuildingsLabel::build()
            ->where(['is_deleted' => '0','level' => '1'])
            ->field(['name','id answer'])
            ->select();

        return $data;
    }

    public static function save($data, $user_id){
        if (count($data['ids']) <= 0) {
            return true;
        }

        $info = UserLabel::build()
            ->where(['user_id' => $user_id])
            ->field(['id'])
            ->find();

        if (!empty($info)){
            UserLabel::build()
                ->where(['id' => $info['id']])
                ->update([
                    'label_ids' => json_encode($data['ids']),
                    'update_time' => time()
                ]);
        }else{
            UserLabel::build()
                ->insert([
                    'user_id' => $user_id,
                    'label_ids' => json_encode($data['ids']),
                    'create_time' => time()
                ]);
        }

        return true;

    }
}
