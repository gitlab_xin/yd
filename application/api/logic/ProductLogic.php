<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\ProductBean;
use app\common\tools\ImageUtils;
use app\common\tools\CommonMethod;

class ProductLogic
{
    public static $heightArr = [1715, 1395, 1075, 755, 435];

    /**
     * @param $rLeft
     * @param $rTop
     * @param $mLeft
     * @param $mTop
     * @param $deep
     * @param $height
     * @param $width
     * @param $colorNo
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_WCB($rLeft, $rTop, $mLeft, $mTop, $deep, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "ZC" . $height . "-" . $deep . "-" . $colorNo;
        $pNoNew = "ZC" . $height . "-" . $deep . "-00-" . $colorNo;
        $product = new ProductBean();
        $product->setProduct_name('自定义衣柜外侧板');
        $product->setSeq_type(100);
        $product->setSeq($height);
        $product->setR_left($rLeft);
        $product->setR_top($rTop);
        $product->setM_left(intval($mLeft / 8));
        $product->setM_left_mm($mLeft);
        $product->setM_top(intval($mTop / 8));
        $product->setM_top_mm($mTop);
        $product->setProduct_type('ZC');
        $product->setProduct_deep($deep);
        $product->setProduct_height($height);
        $product->setProduct_width($width);
        $product->setProduct_s_width(intval($deep / 8));
        $product->setProduct_s_width_mm($deep);
        $product->setProduct_s_height(intval($height / 8));
        $product->setProduct_s_height_mm($height);
        $product->setProduct_color_no($colorNo);
        $product->setProduct_no($pNoNew);
        $product->setProduct_pic(CommonMethod::getPicDir('r') . $pNo . '.jpg');
        $product->setProduct_color_name(CommonMethod::getSchemeColorName($colorNo));
        $product->setZ_index(2);
        $product->setProduct_category('W');
        $productKey = $product->getM_left() . "-" . $product->getM_top() . "-" . $product->getProduct_type();
        $product->setProduct_key($productKey);

        return $product;
    }

    /**
     * @param $rLeft
     * @param $rTop
     * @param $mLeft
     * @param $mTop
     * @param $deep
     * @param $height
     * @param $width
     * @param $colorNo
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_WCB_YM($rLeft, $rTop, $mLeft, $mTop, $deep, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "YC-" . $height . "-" . $colorNo;
        $pNoNew = "YC-" . $height . "-00-" . $colorNo;
        $product = new ProductBean();
        $product->setProduct_name('移门衣柜外侧板');
        $product->setSeq_type(100);
        $product->setSeq($height);
        $product->setR_left($rLeft);
        $product->setR_top($rTop);
        $product->setM_left(intval($mLeft / 8));
        $product->setM_left_mm($mLeft);
        $product->setM_top(intval($mTop / 8));
        $product->setM_top_mm($mTop);
        $product->setProduct_type('YC');
        $product->setProduct_deep($deep);
        $product->setProduct_height($height);
        $product->setProduct_width($width);
        $product->setProduct_s_width(intval($deep / 8));
        $product->setProduct_s_width_mm($deep);
        $product->setProduct_s_height(intval($height / 8));
        $product->setProduct_s_height_mm($height);
        $product->setProduct_color_no($colorNo);
        $product->setProduct_no($pNoNew);
        $product->setProduct_pic(CommonMethod::getPicDir('r') . $pNo . '.jpg');
        $product->setProduct_color_name(CommonMethod::getSchemeColorName($colorNo));
        $product->setZ_index(2);
        $product->setProduct_category('W');
        $productKey = $product->getM_left() . "-" . $product->getM_top() . "-" . $product->getProduct_type();
        $product->setProduct_key($productKey);

        return $product;
    }

    /**
     * @param $rLeft
     * @param $rTop
     * @param $mLeft
     * @param $mTop
     * @param $deep
     * @param $height
     * @param $width
     * @param $colorNo
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_WCB_DG($rLeft, $rTop, $mLeft, $mTop, $deep, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "YZ-W483-" . $colorNo;
        $pNoNew = "YZ-W483-00-" . $colorNo;
        $product = new ProductBean();
        $product->setProduct_name('顶柜外侧板');
        $product->setSeq_type(100);
        $product->setSeq($height);
        $product->setR_left($rLeft);
        $product->setR_top($rTop);
        $product->setM_left(intval($mLeft / 8));
        $product->setM_left_mm($mLeft);
        $product->setM_top(intval($mTop / 8));
        $product->setM_top_mm($mTop);
        $product->setProduct_type('YZ-W');
        $product->setProduct_deep($deep);
        $product->setProduct_height($height);
        $product->setProduct_width($width);
        $product->setProduct_s_width(intval($deep / 8));
        $product->setProduct_s_width_mm($deep);
        $product->setProduct_s_height(intval($height / 8));
        $product->setProduct_s_height_mm($height);
        $product->setProduct_color_no($colorNo);
        $product->setProduct_no($pNoNew);
        $product->setProduct_pic(CommonMethod::getPicDir('r') . $pNo . '.jpg');
        $product->setProduct_color_name(CommonMethod::getSchemeColorName($colorNo));
        $product->setZ_index(2);
        $product->setProduct_category('DG');
        $productKey = $product->getM_left() . "-" . $product->getM_top() . "-" . $product->getProduct_type();
        $product->setProduct_key($productKey);

        return $product;
    }

    /**
     * @param $rLeft
     * @param $rTop
     * @param $mLeft
     * @param $mTop
     * @param $deep
     * @param $height
     * @param $width
     * @param $colorNo
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_NCB_DG($rLeft, $rTop, $mLeft, $mTop, $deep, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "YZ-N483-" . $colorNo;
        $pNoNew = "YZ-N483-00-" . $colorNo;
        $product = new ProductBean();
        $product->setProduct_name('顶柜中侧板');
        $product->setSeq_type(100);
        $product->setSeq($height);
        $product->setR_left($rLeft);
        $product->setR_top($rTop);
        $product->setM_left(intval($mLeft / 8));
        $product->setM_left_mm($mLeft);
        $product->setM_top(intval($mTop / 8));
        $product->setM_top_mm($mTop);
        $product->setProduct_type('YZ-N');
        $product->setProduct_deep($deep);
        $product->setProduct_height($height);
        $product->setProduct_width($width);
        $product->setProduct_s_width(intval($deep / 8));
        $product->setProduct_s_width_mm($deep);
        $product->setProduct_s_height(intval($height / 8));
        $product->setProduct_s_height_mm($height);
        $product->setProduct_color_no($colorNo);
        $product->setProduct_no($pNoNew);
        $product->setProduct_pic(CommonMethod::getPicDir('r') . $pNo . '.jpg');
        $product->setProduct_color_name(CommonMethod::getSchemeColorName($colorNo));
        $product->setZ_index(2);
        $product->setProduct_category('DG');
        $productKey = $product->getM_left() . "-" . $product->getM_top() . "-" . $product->getProduct_type();
        $product->setProduct_key($productKey);

        return $product;
    }

    public function createZDYYG_DT($rLeft, $rTop, $mLeft, $mTop, $deep, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "YT-" . $height . "-" . $colorNo;
        // YT-{长度}-{风格}-{花色代码}
        $pNoNew = "YT-" . $height . "-00-" . $colorNo;
        $product = new ProductBean();
        $product->setProduct_name("移门衣柜挡条");
        $product->setSeq_type(200);
        $product->setSeq($width);
        $product->setR_left($rLeft);
        $product->setR_top($rTop);
        $product->setM_left(intval($mLeft / 8));
        $product->setM_left_mm($mLeft);
        $product->setM_top(intval($mTop / 8));
        $product->setM_top_mm($mTop);
        $product->setProduct_type('YT');
        $product->setProduct_deep($deep);
        $product->setProduct_height($height);
        $product->setProduct_width($width);
        $product->setProduct_s_width(intval($height / 8));
        $product->setProduct_s_width_mm($height);
        $product->setProduct_s_height(intval($width / 8));
        $product->setProduct_s_height_mm($width);
        $product->setProduct_color_no($colorNo);
        $product->setProduct_no($pNoNew);
        $product->setProduct_pic(CommonMethod::getPicDir('r') . $pNo . '.jpg');
        $product->setProduct_color_name(CommonMethod::getSchemeColorName($colorNo));
        $product->setZ_index(2);
        $product->setProduct_category('D');
        $productKey = $product->getM_left() . "-" . $product->getM_top() . "-" . $product->getProduct_type();
        $product->setProduct_key($productKey);

        return $product;
    }

    /**
     * @param $mLeft
     * @param $mTop
     * @param $height
     * @param $width
     * @param $colorNo
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_SK($mLeft, $mTop, $height, $width, $colorNo, $yyType = '')
    {
        $pNo = "SK-" . $height . "-25-" . $colorNo;

        $p = new ProductBean();

        $p->setProduct_name('收口');
        $p->setR_left($mLeft);
        $p->setR_top($mTop);
        $p->setM_left(intval($mLeft / 8));
        $p->setM_top(intval($mTop / 8));
        $p->setM_left_mm($mLeft);
        $p->setM_top_mm($mTop);
        $p->setProduct_type('SK');
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($colorNo);
        $p->setProduct_no($pNo);
        $p->setProduct_pic(getSchemeColorArrH($colorNo));
        $p->setZ_index(1);
        $p->setProduct_category('SK');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-SK');

        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_NCB($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType = '')
    {
        $p_no = "ZC" . strval($height) . "-" . $color_no;
        // ZC{长度}-{风格}-{花色代码}
        $p_no_new = "ZC" . strval($height) . "-00-" . $color_no;

        $p = new ProductBean();
        $p->setProduct_name("自定义衣柜中侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category("N");
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());
        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_BB($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType = '')
    {
        $width_img = $width - 24;
        $p_no = "ZB" . strval($width_img) . "-" . strval($height) . "-12-000";
//        $width = $width - 32;
        // ZB{单元宽度}-{宽度}-{风格}-{花色代码}
        $p_no_new = "ZB" . strval($width) . "-" . strval($height) . "-00-000";
        $p = new ProductBean();
        $p->setProduct_name("自定义衣柜背板");
        $p->setSeq_type(400);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZB");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8 - 3));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(0);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());
        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_BB_DG($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType = '')
    {
        $p_no = "YZ-B" . strval($width) . "-" . $color_no;
        // ZB{单元宽度}-{宽度}-{风格}-{花色代码}
        $p_no_new = "YZ-B" . strval($width+10) . "-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("顶柜背板");
        $p->setSeq_type(400);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("YZ-B");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_category("DG");
        $p->setZ_index(0);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());
        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @param $dir
     * @return ProductBean
     */
    public function createZDYYG_MB_DG($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType = '', $dir)
    {
        $p_no = "YZ-M" . strval($width + 26) . "-" . $color_no;
        // ZB{单元宽度}-{宽度}-{风格}-{花色代码}
        $p_no_new = "YZ-M" . strval($width) . "-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("顶柜门板");
        $p->setSeq_type(700);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("YZ-M");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        $p->setProduct_direction($dir);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '-' . $dir . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_category("DG");
        $p->setZ_index(2);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());
        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param $yyType
     * @param $up_down
     * @return ProductBean
     */
    public function createZDYYG_HGB_GD_DD($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $putType, $yyType = '', $up_down)
    {
        $p = new ProductBean();
        if ($up_down == 0) {
            $p->setProduct_name("顶底横隔板");
            $p_no = "YD-" . strval($width) . "-00-" . $color_no;
            $p_no_new = "YD-" . $width . "-01-00-" . $color_no;
        } else {
            $p->setProduct_name("顶底横隔板");
            $p_no = "YD-" . strval($width) . "-10-" . $color_no;
            $p_no_new = "YD-" . $width . "-01-00-" . $color_no;
        }

        $p->setSeq_type(201);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_put_type($putType);
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . "YD-" . $width . "-" . $color_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());
        $p->setCan_delete(0);
        $p->setProduct_move_type(0);
        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_HGB($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $p_no = "ZH" . strval($width) . "-" . strval($color_no);
        // ZH{单元宽度}-{风格}-{花色代码}
        $p_no_new = "ZH" . strval($width) . "-00-" . strval($color_no);
        $p = new ProductBean();
        $p->setProduct_name("自定义衣柜横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);// - 17
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '.jpg');
        $p->setProduct_move_type(1);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());

        return $p;
    }

    public function checkZDYYG_HGB(ProductBean $bean, $schemeHeight, $colIndex, &$pointsData)
    {
        $height = $bean->getProduct_height();
        $mTopMM = $bean->getM_top_mm();

        if ($mTopMM + 25 == $schemeHeight) {
            // 底板
            $holeIndex = count($pointsData[$colIndex]) - 1;
            $pointsData[$colIndex][$holeIndex] = [
                'is_use' => 1,
                'can_use' => 0
            ];
            return true;
        }
        if ($mTopMM % 32 == 0) {
            $holeIndex = $mTopMM / 32;
            if (!empty($bean->getM_left_mm())) {
                return true;
            }
            if (!$this->checkHole($pointsData, $colIndex, $holeIndex, 1)) {
                return $bean->getProduct_name() . "检查失败，点{$colIndex},{$holeIndex}不能使用";
            }
            $this->updateHoleUsed($pointsData, $colIndex, $holeIndex, 1);
            return true;
        }
        return $bean->getProduct_name() . "检查失败, 位置{$mTopMM}不吻合";
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_HGB_YTG($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $p_no = "ZH" . strval($width) . "-000ytg";
        // ZH{单元宽度}-{风格}-{花色代码}
        $p_no_new = "ZH" . strval($width) . "-00-000ytg";
        $p = new ProductBean();
        $p->setProduct_name("自定义衣柜横隔板衣通杆");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);// - 17
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZH-YTG");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8) + intval(50 / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep + 50);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r',0) . $p_no . '.png');
        $p->setProduct_move_type(1);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-ZH");

        return $p;
    }

    /**
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @return ProductBean
     */
    public function createZDYYG_ZLB($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType = '')
    {
        $p_no = "ZZ" . strval($height) . "-" . strval($color_no);
        // ZZ{长度}-{风格}-{花色代码}
        $p_no_new = "ZZ" . strval($height) . "-00-" . strval($color_no);
        $p = new ProductBean();
        $p->setProduct_name("自定义衣柜中立板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);// - 17
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ZZ");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('r') . $p_no . '.png');
        $p->setProduct_move_type(2);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());

        return $p;
    }

    public function checkZDYYG_HGB_YTG(ProductBean $bean, $schemeHeight, $colIndex, &$pointsData)
    {
        $height = $bean->getProduct_height();
        $mTopMM = $bean->getM_top_mm();

        if ($mTopMM % 32 == 0) {
            $holeIndex = $mTopMM / 32;
            if (!empty($bean->getM_left_mm())) {
                return true;
            }
            if (!$this->checkHole($pointsData, $colIndex, $holeIndex, 2)) {
                return $bean->getProduct_name() . "检查失败，点{$colIndex},{$holeIndex}不能使用";
            }
            $this->updateHoleUsed($pointsData, $colIndex, $holeIndex, 2);
            return true;
        }
        return $bean->getProduct_name() . "检查失败, 位置{$mTopMM}不吻合";
    }

    public function createZDYYG_LL($r_left, $r_top, $m_left, $m_top, $width, $color_no = '', $yyType = '')
    {
        // 拉篮只要白色的
        $p_no = "L-" . strval($width) . "-" . strval("00");
        // L-{单元宽度}-{风格}-{花色代码}
        $p_no_new = "L-" . strval($width) . "-00-" . strval("00");
        $p = new ProductBean();
        $p->setProduct_name($width . "拉篮");
        $p->setSeq_type(550);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("L" . "00");
        $p->setProduct_height(224);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(200 / 8);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm(200);
        //$p->setProduct_color_no("00");
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic("../Function/Customwardrobe/" . $p_no . ".png");
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-L");

        return $p;
    }

    public function checkZDYYG_LL(ProductBean $bean, $schemeHeight, $colIndex, &$pointsData)
    {
        $height = $bean->getProduct_height();
        $mTopMM = $bean->getM_top_mm();

        if ($mTopMM % 32 == 0) {
            $holeIndex = $mTopMM / 32;
            if ($this->checkHole($pointsData, $colIndex, $holeIndex, 7)) {
                return $bean->getProduct_name() . "检查失败，点{$colIndex},{$holeIndex}不能使用";
            }
            $this->updateHoleUsed($pointsData, $colIndex, $holeIndex, 7);
            return true;
        }
        return $bean->getProduct_name() . "检查失败, 位置{$mTopMM}不吻合";
    }

    public function createZDYYG_KC($r_left, $r_top, $m_left, $m_top, $width, $yyType = '')
    {
        $p_no = "K-" . strval($width);
        // L-{单元宽度}-{风格}-{花色代码}
//        $p_no_new = "K-" . strval($width) . "-00";
        $p_no_new = "K-" . strval($width) . "-00-" . strval("00");
        $p = new ProductBean();
        $p->setProduct_name("裤抽");
        $p->setSeq_type(560);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("K");
        $p->setProduct_height(60);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(60 / 8);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm(60);
        //$p->setProduct_color_no("00");
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic("../Function/Customwardrobe/" . $p_no . ".png");
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-K");

        return $p;
    }

    public function checkZDYYG_KC(ProductBean $bean, $schemeHeight, $colIndex, &$pointsData)
    {
        $height = $bean->getProduct_height();
        $mTopMM = $bean->getM_top_mm();

        if ($mTopMM % 32 == 0) {
            $holeIndex = $mTopMM / 32;
            if ($this->checkHole($pointsData, $colIndex, $holeIndex, 17)) {
                return $bean->getProduct_name() . "检查失败，点{$colIndex},{$holeIndex}不能使用";
            }
            $this->updateHoleUsed($pointsData, $colIndex, $holeIndex, 17);
            return true;
        }
        return $bean->getProduct_name() . "检查失败, 位置{$mTopMM}不吻合";
    }

    public function checkHole($pointsData, $colIndex, $holeIndex, $holeCount)
    {
        $result = true;
        foreach (range(0, $holeCount - 1) as $item) {
            $currentIndex = $holeIndex + $item;
            $result = $result && isset($pointsData[$colIndex][$currentIndex])
                && ($pointsData[$colIndex][$currentIndex]['can_use'] == 1);
            if (!$result) {
                return false;
            }
        }
        return true;
    }

    public function updateHoleUsed(&$pointsData, $colIndex, $holeIndex, $holeCount)
    {
        foreach (range(0, $holeCount - 1) as $item) {
            $currentIndex = $holeIndex + $item;
            $pointsData[$colIndex][$currentIndex] = [
                'is_use' => 1,
                'can_use' => 0
            ];
        }
    }


    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜外侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_WCB($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $p_no = "M" . $type . '-WC-' . $color_no;
        // MC-{长度}-{风格}/W-{花色代码}
        $p_no_new = "MC-" . $height . '-W-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜外侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MWC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('W');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜中侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_ZCB($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $p_no = "M" . $type . '-ZC-' . $color_no;
        // MC-N-{长度}-{风格}-{花色代码}
        $p_no_new = "MC-" . $height . '-N-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜中侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MZC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_move_type(0);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜组件外侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_WCB_ZJ($r_left, $r_top, $m_left, $m_top,
                                     $deep, $height, $width, $color_no, $yyType = '')
    {
        if (in_array($height, static::$heightArr)) {
            $height -= 16;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DC" . strval($height) . '-W-' . $color_no_img;
        // DC{长度}-W-{风格}-{花色代码}
        $p_no_new = "DC" . strval($height) . '-W-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜组件外侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('W');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:衣柜+书柜衔接侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_ZCB_XJ($r_left, $r_top, $m_left, $m_top,
                                      $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $p_no = "M" . $type . '-XC-' . $color_no;
        // MC-{长度}-X-{花色代码}
        $p_no_new = "MC" . $height . '-X-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("衣柜+书柜衔接侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MXC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_move_type(0);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜背板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_BB($r_left, $r_top, $m_left, $m_top,
                                  $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $b = 1201 == $height ? '1' : '2';
        $p_no = "M" . $type . '-' . strval($width) . '-B' . $b . '-000';
        // MB-{单元宽度}-{长度}-{风格}-{花色代码}
        $p_no_new = "MB-" . $width . '-' . $height . '-00-000';
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜背板");
        $p->setSeq_type(400);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MB");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(0);
        $p->setProduct_category('W');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜底担
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_DD($r_left, $r_top, $m_left, $m_top,
                                  $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $p_no = "M" . $type . '-' . strval(('24' == $type ? $width + 32 : $width)) . 'D-' . $color_no;
        // DD-{单元宽度}-{高度}-{厚度}-{风格}-{花色}
        $p_no_new = "DD-" . $width . '-' . $height . '-' . $deep . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜底担");
        $p->setSeq_type(300);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MD");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(($height > 80 ? 0 : 2));
        $p->setProduct_category('D');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());
        $p->setCan_delete(0);
        $p->setProduct_move_type(0);
        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:横隔板中
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_HGB($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $putType, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-' . strval(('24' == $type ? $width + 32 : $width)) . 'H-' . $color_no;
        // MC-{长度}-{风格}/W-{花色代码}
        $p_no_new = "MH-" . $width . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:横隔板顶
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_HGB_GD($r_left, $r_top, $m_left, $m_top,
                                      $deep, $height, $width, $color_no, $putType, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-' . strval(('24' == $type ? $width + 32 : $width)) . 'H-' . $color_no;
        // MC-{长度}-{风格}/W-{花色代码}
        $p_no_new = "MH-" . $width . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());
        $p->setCan_delete(0);
        $p->setProduct_move_type(0);

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:横隔板+衣通杆
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_HGB_YTG($r_left, $r_top, $m_left, $m_top,
                                       $deep, $height, $width, $color_no, $putType, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-' . strval(('24' == $type ? $width + 32 : $width)) . 'H-' . $color_no . 'ytg';
        // MC-{长度}-{风格}/W-{花色代码}
        $p_no_new = "MH-" . $width . '-00-' . $color_no . 'ytg';
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜衣通杆");
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MH-YTG");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep + 50);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8) + intval(50 / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ', 0) . $p_no . '.png');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-ZH');

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:横隔板+衣通杆
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_HGB_YTG_GD($r_left, $r_top, $m_left, $m_top,
                                          $deep, $height, $width, $color_no, $putType, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-' . strval(('24' == $type ? $width + 32 : $width)) . 'H-' . $color_no . 'ytg';
        // MC-{长度}-{风格}/W-{花色代码}
        $p_no_new = "MH-" . $width . '-00-' . $color_no . 'ytg';
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜衣通杆");
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MH-YTG");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep + 50);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8) + intval(50 / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.png');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-ZH');

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜中层板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_ZCengB_GD($r_left, $r_top, $m_left, $m_top,
                                         $deep, $height, $width, $color_no, $putType, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-' . strval($width + 32) . 'Z-' . $color_no;
        // MH-Z-{单元宽度}-{风格}-{花色代码}
        $p_no_new = "MH-" . $width . '-Z-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜中层板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MZCB");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:衣通杆
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_YTG($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        $p_no = 'ytg' . $width;
        $p = new ProductBean();
        $p->setProduct_name($width . "衣通杆");
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("YTG");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.png');
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:标准衣柜中立板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createBZYG_ZLB($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $yyType = '', $type)
    {
        //固定灰布纹
        $color_no = '000';

        $p_no = "M" . $type . '-ZC-01-' . $color_no;
        // MZ-{长度}-{风格}-{花色代码}
        $p_no_new = "MZ-" . $height . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("标准衣柜中立板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("MZC-01");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('BZ') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:拉篮
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createBZYG_LL($r_left, $r_top, $m_left, $m_top, $width, $color_no, $yyType = '')
    {
        $p_no = "LB-" . strval($width) . '-' . $color_no;
        // LB-{单元宽度}-风格-{花色代码}
        $p_no_new = "LB-" . strval($width) . '-00-'. strval("00");
        $p = new ProductBean();
        $p->setProduct_name($width . "拉篮");
        $p->setSeq_type(550);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("L00");
        $p->setProduct_height(224);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm(200);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval(200 / 8));
        //$p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic('../Function/Customwardrobe/' . $p_no . '.png');
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-L');

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:裤抽
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $width
     * @param string $yyType
     * @return ProductBean
     */
    public function createBZYG_KC($r_left, $r_top, $m_left, $m_top, $width, $yyType = '')
    {
        $p_no = "KB-" . strval($width);
        // KB-{单元宽度}-风格
        $p_no_new = "KB-" . strval($width) . '-00';
        $p = new ProductBean();
        $p->setProduct_name('裤抽');
        $p->setSeq_type(560);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type('K');
        $p->setProduct_height(60);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm(60);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval(60 / 8));
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic('../Function/Customwardrobe/K-' . strval($width) . '.png');
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-K');

        return $p;
    }


    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜背板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createZHG_BB($r_left, $r_top, $m_left, $m_top,
                                 $deep, $height, $width, $color_no, $yyType = '')
    {
        //图片名称处理
        $img_height = $height;
        if (in_array($height,['653','973'])) {
            $img_height = $height + 12;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DB" . strval($width) . '-' . $img_height . '-' . $color_no_img;
        // DB{单元宽度}-{长度}-{风格}-{花色代码}
        $p_no_new = "DB" . strval($width) . '-' . $height . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setSeq_type(400);
        $p->setSeq($width);
        $p->setProduct_name("综合柜背板");
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DB");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.png');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(0);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜横隔板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_HGB_GD($r_left, $r_top, $m_left, $m_top,
                                     $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DH" . strval($width) . '-' . $color_no_img;
        // DH{长度或单元宽度}-{风格}-{花色代码}
        $p_no_new = "DH" . strval($width) . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜中侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_ZCB($r_left, $r_top, $m_left, $m_top,
                                  $deep, $height, $width, $color_no, $yyType = '')
    {
        if (in_array($height, static::$heightArr)) {
            $height -= 16;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }

        $p_no = "DC" . strval($height) . '-' . $color_no_img;
        // DC{长度}-{风格}-{花色代码}
        $p_no_new = "DC" . strval($height) . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜中侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('N');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:垂直写字桌
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_ZZ($r_left, $r_top, $m_left, $m_top,
                                 $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no_img = 'Z-DZ1478-' . strval($color_no_img);
        $p_no = 'Z-DZ1478-' . strval($color_no);
        $p = new ProductBean();
        $p->setProduct_name("垂直写字桌");
        $p->setSeq_type(600);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8) + 2);
        $p->setProduct_type("ZZ");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no_img . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(5);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:平行写字桌桌面
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_ZZ_M($r_left, $r_top, $m_left, $m_top,
                                   $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }

        $p_no_img = "DZ" . strval(intval(($width - 25) / 2)) . '*2M-' . $color_no_img;
        $p_no = "DZ" . strval(intval(($width - 25) / 2)) . '*2M-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("平行写字桌桌面");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("ZM");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . str_replace('*', '-', $p_no_img) . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜底担
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_DD($r_left, $r_top, $m_left, $m_top,
                                 $deep, $height, $width, $color_no, $without_pic = 0)
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DD" . strval($width) . '-' . $color_no_img;
        // DD-{单元宽度}-{高度}-{厚度}-{风格}-{花色}
        $p_no_new = "DD-" . strval($width) . '-' . $height . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜底担");
        $p->setSeq_type(300);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DD");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_no($p_no_new);

        $p->setProduct_pic($without_pic ? '' : CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_category('D');
        $p->setZ_index(($height > 80 ? 0 : 2));
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月10日
     * description:平行写字桌桌面
     * @param $m_left
     * @param $m_top
     * @param $height
     * @param $width
     * @param $acc_id
     * @param $acc_name
     * @param $acc_type
     * @param $dir
     * @return ProductBean
     */
    public function createZHG_Acc($m_left, $m_top, $height, $width, $acc_id, $acc_name, $acc_type, $dir)
    {

        $p = new ProductBean();
        $p->setProduct_name($acc_name);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top());
        $p->setProduct_type("Acc");
        $p->setProduct_height(intval($height / 8));
        $p->setProduct_width(intval($width / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_no($acc_id);
        $p->setProduct_pic("../Adornment/Cabinetoffice/" . $acc_id . ".png");
        $p->setProduct_move_type(2);
        $p->setProduct_accType($acc_type);
        $p->setProduct_direction($dir);
        if (1 == $dir) {
            $p->setProduct_pic1("../Adornment/Cabinetoffice/" . str_replace('_L', '_R', $acc_id) . '.png');
        }
        $p->setZ_index(3);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-Acc');

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:综合柜底担
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_WCB($r_left, $r_top, $m_left, $m_top,
                                  $deep, $height, $width, $color_no, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DC" . strval($height) . '-' . strval($deep) . '-' . $color_no_img;
        // DC{长度}-{厚度}-{风格}-{花色代码}
        $p_no_new = "DC" . strval($height) . '-' . strval($deep) . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜外侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_no($p_no_new);

        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('W');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月29日
     * description:综合柜横隔板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_HGB($r_left, $r_top, $m_left, $m_top,
                                  $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DH" . strval($width) . '-' . $color_no_img;
        // DH{长度或单元宽度}-{风格}-{花色代码}
        $p_no_new = "DH" . strval($width) . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("DH");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setProduct_move_type(1);
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月29日
     * description:综合柜盖板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_GB($r_left, $r_top, $m_left, $m_top,
                                 $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $p_no = "GAI-" . strval($width) . '-' . $color_no;
        // W06-24-{尺寸}-{风格}-{花色代码}
        $p_no_new = "W06-24-" . strval($width) . '-00-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜盖板");
        $p->setSeq_type(210);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setProduct_type("GAI");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        // $p->setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $p->setProduct_pic($p_no . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        $imageUtil = new ImageUtils();
        $imageUtil->cut(EXTEND_PATH . DCOLOR . getSchemeColorArrH($color_no), EXTEND_PATH . PRO_PIC . $p_no . '.jpg', 10, 10, $p->getProduct_s_width(), $p->getProduct_s_height());
        return $p;
    }

    /**
     * @author: Airon
     * @time: 2017年10月10日
     * description:综合柜 视听柜衔接组件
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_HGB_GD_XJ($r_left, $r_top, $m_left, $m_top,
                                        $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }

        $p_no_img = "TVX-H-00-" . $color_no_img;
        $p_no = "TVX-H-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("视听柜衔接组件-横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("TVX-H");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir("ZH") . $p_no_img . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Airon
     * @time: 2017年10月10日
     * description:综合柜 视听柜衔接组件
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_HGB_XJ($r_left, $r_top, $m_left, $m_top,
                                     $deep, $height, $width, $color_no, $putType, $yyType = '')
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no_img = "TVX-H-00-" . $color_no_img;
        $p_no = "TVX-H-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("视听柜衔接组件-横隔板");
        $p->setSeq_type(200);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("TVX-H");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($deep / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($deep);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir("ZH") . $p_no_img . '.jpg');
        $p->setProduct_move_type(1);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Airon
     * @time: 2017年10月10日
     * description:综合柜矮抽屉
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createZHG_OneCT_AI($r_left, $r_top, $m_left, $m_top,
                                       $deep, $height, $width, $color_no, $putType, $yyType, $type)
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no_img = "CT-" . $width . "-" . $height . '-' . $color_no_img;
        $p_no = "CT-" . $width . "-" . $height . '-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜矮抽屉");
        $p->setSeq_type(500);
        $p->setSeq($width);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("ACT");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8) + 4);
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir("ZH") . $p_no_img . '.jpg');
        $p->setProduct_move_type(1);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }

    /**
     * @author: Airon
     * @time: 2017年10月10日
     * description:金属封边板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $putType
     * @param string $yyType
     * @param $type
     * @return ProductBean
     */
    public function createZHG_JSFBB($r_left, $r_top, $m_left, $m_top,
                                    $deep, $height, $width, $color_no, $putType, $yyType)
    {
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no_img = "JSFBB-" . $height . '-25-' . $color_no_img;
        $p_no = "JSFBB-" . $height . '-25-' . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("金属封边板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("JSFBB");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($width / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($width);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        $p->setProduct_put_type($putType);
        $p->setProduct_no($p_no);
        $p->setProduct_pic(CommonMethod::getPicDir("ZH") . $p_no_img . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(1);
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-SKS');

        return $p;
    }
    private function Containts($height)
    {
        foreach (static::$heightArr as $h) {
            if ($h == $height) {
                return true;
            }
        }
        return false;
    }


    /**
     * @author: Airon
     * @time: 2017年10月11日
     * description:综合柜视听柜衔接外侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_WCB_ST($r_left, $r_top, $m_left, $m_top,
                                     $deep, $height, $width, $color_no, $yyType = '')
    {
        if ($this->Containts($height)) {
            $height -= 16;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }

        $p_no_img = "TVX-WC-00-" . $color_no_img;
        $p_no = "TVX-WC-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("视听柜衔接组件-外侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("TVX-WC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        $p->setProduct_no($p_no);

        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no_img . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('W');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }
    /**
     * @author: Airon
     * @time: 2017年10月11日
     * description:综合柜衔接内侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param string $yyType
     * @return ProductBean
     */
    public function createZHG_ZCB_XJ($r_left, $r_top, $m_left, $m_top,
                                     $deep, $height, $width, $color_no, $yyType = '')
    {
        if ($this->Containts($height)) {
            $height -= 16;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no_img = "TVX-NC-00-" . $color_no_img;
        $p_no = "TVX-NC-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("视听柜衔接组件-内侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_top(intval($m_top / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top_mm($m_top);
        $p->setProduct_type("TVX-NC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        $p->setProduct_no($p_no);

        $p->setProduct_pic(CommonMethod::getPicDir('ZH') . $p_no_img . '.jpg');
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category('N');
        $p->setProduct_key($p->getM_left() . '-' . $p->getM_top() . '-' . $p->getProduct_type());

        return $p;
    }
    /**
     * 综合柜组件衔接侧板
     * @param $r_left
     * @param $r_top
     * @param $m_left
     * @param $m_top
     * @param $deep
     * @param $height
     * @param $width
     * @param $color_no
     * @param $yyType
     * @return ProductBean
     */
    public function createZHG_ZJXJCB($r_left, $r_top, $m_left, $m_top, $deep, $height, $width, $color_no, $yyType)
    {
        if ($this->Containts($height)) {
            $height -= 16;
        }
        $color_no_img = $color_no;
        switch ($color_no){
            case 'test2':
                $color_no_img = '044';
                break;
            case 'test1':
                $color_no_img = '001';
        }
        $p_no = "DC" . strval($height) . "-X-" . $color_no_img;
        // DC{长度}-X-{风格}-{花色代码}
        $p_no_new = "DC" . strval($height) . "-X-00-" . $color_no;
        $p = new ProductBean();
        $p->setProduct_name("综合柜组件衔接侧板");
        $p->setSeq_type(100);
        $p->setSeq($height);
        $p->setR_left($r_left);
        $p->setR_top($r_top);
        $p->setM_left(intval($m_left / 8));
        $p->setM_left_mm($m_left);
        $p->setM_top(intval($m_top / 8));
        $p->setM_top_mm($m_top);
        $p->setProduct_type("DC");
        $p->setProduct_deep($deep);
        $p->setProduct_height($height);
        $p->setProduct_width($width);
        $p->setProduct_s_width(intval($deep / 8));
        $p->setProduct_s_width_mm($deep);
        $p->setProduct_s_height(intval($height / 8));
        $p->setProduct_s_height_mm($height);
        $p->setProduct_color_no($color_no);
        // p.setProduct_no(p_no);
        $p->setProduct_no($p_no_new);
        $imgUrl= CommonMethod::getPicDir("ZH").$p_no.".jpg";
        $p->setProduct_pic($imgUrl);
        $p->setProduct_color_name(CommonMethod::getSchemeColorName($color_no));
        $p->setZ_index(2);
        $p->setProduct_category("N");
        $p->setProduct_key($p->getM_left() . "-" . $p->getM_top() . "-" . $p->getProduct_type());

        return $p;
    }
}
