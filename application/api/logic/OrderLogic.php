<?php
namespace app\api\logic;


use app\common\model\CustomizedOrder as OrderModel;
use app\common\model\CustomizedOrderArea as OrderAreaModel;
use app\common\model\CustomizedScheme;
use app\common\model\MyDecorateProduct;
use app\common\model\MyDecorateProductStandard;
use app\common\model\Order;
use app\common\model\OrderArea;
use app\common\model\OrderCustomizedScheme;
use app\common\model\OrderDetail;
use app\common\model\ShoppingCart;
use app\common\model\ShopProductStandard;
use app\common\model\UserArea as UserAreaModel;
use app\common\tools\alipayClient;
use app\common\tools\RedisUtil;
use app\common\tools\wechatPay;
use think\Config;
use think\Db;
use think\Exception;

class OrderLogic
{
    public static function preInfo($requestData){
        $where = ['c.user_id' => $requestData['user_id'], 'c.cart_id' => ['in', $requestData['cart_id']]];

        return ShoppingCart::build()
            ->alias('c')
            ->where($where)
            ->field([
                'c.cart_id id', 'c.from', 'c.count', 'c.product_id','c.product_id cart_product_info', 'c.type',
                'c.color_id', 'c.standard_id', 'c.decorate_product_id'
            ])
            ->order(['c.cart_id' => "desc"])
            ->select();

    }

    /**
     * User: zhaoxin
     * Date: 2020/4/10
     * Time: 4:08 下午
     * description
     * @param $requestData
     * @param $from 1商城，2易家整配，3定制
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function info($requestData, $from){

        $product_info = $requestData['type'] = MyDecorateProduct::build()
            ->where(['id' => $requestData['id']])
            ->field(['type', 'product_id', 'decorate_id'])
            ->find();

        //商城 || 易家整配商城
        if ($from == '1' || $product_info['type'] == '3'){
            $info = ShopProductStandard::build()
                ->alias('s')
                ->join('yd_shop_product p', 'p.product_id = s.product_id')
                ->where(['s.standard_id' => $requestData['standard_id'], 's.product_id' => $product_info['product_id']])
                ->field([
                    's.img', 'p.name', 's.name standard_name', 's.price', 's.is_deleted', 's.stocks'
                ])
                ->find();

            $data['cart_product_info'] = [
                'image' => $info['img'],
                'name' => $info['name'],
                'standard_name' => $info['standard_name'],
                'discount_price' => $info['price'],
                'is_delete' => $info['is_deleted'],
                'stocks' => $info['stocks']
            ];

        }
        //定制 || 易家整配用户自定义
        elseif ($from == '3' || $product_info['type'] == '1'){
            $info = CustomizedScheme::build()
                ->alias('s')
                ->join('yd_customized_color c', 'c.color_no = s.scheme_color_no')
                ->where(['s.id' => $product_info['product_id'], 'c.is_deleted' => '0'])
                ->field([
                    'c.color_name', 's.scheme_pic', 's.scheme_name', 's.scheme_price', 's.scheme_type',
                    's.scheme_b_type'
                ])
                ->find();
            $data['cart_product_info'] = [
                'image' => $info['scheme_pic'],
                'name' => $info['scheme_name'],
                'discount_price' => $info['scheme_price'],
                'is_delete' => '0',
                'standard_name' => $info['color_name'],
                'scheme_type' => $info['scheme_type'],
                'scheme_b_type' => $info['scheme_b_type'],
            ];
        }
        //易家整配
        elseif ($from == '2'){
            $info = MyDecorateProductStandard::build()
                ->alias('s')
                ->join('yd_customized_color c', 'c.color_id = s.color_id')
                ->join('yd_my_decorate_product p', 'p.id = s.decorate_product_id')
                ->join('yd_customized_scheme p_s','p_s.id = p.product_id')
                ->where(['s.color_id' => $requestData['color_id'], 's.decorate_product_id' => $requestData['id']])
                ->field([
                    'c.color_name', 's.product_img', 'p.name', 'p.discount_price', 'p.is_delete',
                    'p_s.scheme_type', 'p_s.scheme_b_type'
                ])
                ->find();
            $data['cart_product_info'] = [
                'image' => $info['product_img'],
                'name' => $info['name'],
                'discount_price' => $info['discount_price'],
                'is_delete' => $info['is_delete'],
                'standard_name' => $info['color_name'],
                'scheme_type' => $info['scheme_type'],
                'scheme_b_type' => $info['scheme_b_type'],
            ];

        }

        $data['from'] = $from;
        $data['standard_id'] = $requestData['standard_id'];
        $data['color_id'] = $requestData['color_id'];
        $data['product_id'] = $product_info['product_id'];
        $data['type'] = $product_info['type'];
        $data['count'] = $requestData['count'];
        $data['decorate_id'] = $product_info['decorate_id'];
        $data['decorate_product_id'] = $requestData['id'];

        return $data;

    }

    public static function create($requestData){
        $cart_info = [];
        $data['is_error'] = '1';
        $where = ['c.user_id' => $requestData['user_id'], 'c.cart_id' => ['in', $requestData['cart_id']]];

        $list = ShoppingCart::build()
            ->alias('c')
            ->where($where)
            ->field([
                'c.cart_id id', 'c.from', 'c.count', 'c.product_id','c.product_id cart_product_info', 'c.type',
                'c.color_id', 'c.standard_id', 'c.decorate_product_id', 'c.decorate_id'
            ])
            ->order(['c.cart_id' => "desc"])
            ->select();
        foreach ($list as $k => $v){
            //商城类商品判断库存
            if (($v['from'] == '1' || $v['type'] == '3') && ($v['cart_product_info']['stocks'] <= 0 || $v['cart_product_info']['stocks'] < $v['count'])){
                $data['message'] = $v['cart_product_info']['standard_name'].'的库存不足';
                return $data;
            }

            if ($v['cart_product_info']['is_delete'] == 1){
                $data['message'] = $v['cart_product_info']['standard_name'].'已被删除或下架';
                return $data;
            }

            $cart_info[] = [
                'cart_id' => $v['id'],
                'from' => $v['from'],
                'type' => $v['type'],
                'color_id' => $v['color_id'],
                'standard_id' => $v['standard_id'],
                'decorate_product_id' => $v['decorate_product_id'],
                'decorate_id' => $v['decorate_id'],
                'product_id' => $v['product_id'],
                'count' => $v['count'],
            ];
        }

        //总金额
        $total_price = 0;
        foreach ($list as $k => $v){
            $total_price += $v['cart_product_info']['discount_price'];
        }

        //收货地址
        $area = UserAreaModel::get($requestData['area_id'])->toArray();
        $order_num = substr(md5(uniqid(mt_rand(), true)), 0, 4) . time();

        Db::startTrans();
        try{
            foreach ($list as $k => $v){
                //商城类商品扣除库存
                if (($v['from'] == '1' || $v['type'] == '3')){
                    ShopProductStandard::build()
                        ->where(['standard_id' => $v['standard_id'], 'product_id' => $v['product_id']])
                        ->setDec('stocks', $v['count']);
                }
            }

            //插入订单数据
            $orderinfo = [
                'user_id' => $requestData['user_id'],
                'order_num' => $order_num,
                'price' => $total_price,
                'express_cost' => '0',
                'status' => '未支付',
                'pay_type' => $requestData['pay_type'],
                'user_remarks' => $requestData['user_remarks'],
                'delivery_time' => $requestData['delivery_time'],
                'create_time' => time(),
            ];
            $order_id = Order::build()
                ->insert($orderinfo, '', true);

            //存储购物车数据
            RedisUtil::getInstance()->set($order_num, serialize($cart_info), 60*32);


            $orderdetails = [];
            $scheme = [];//方案数据
            foreach ($list as $k => $v){
                $orderdetails[] = [
                    'order_id' => $order_id,
                    'product_id' => $v['product_id'],
                    'decorate_product_id' => $v['decorate_product_id'],
                    'decorate_id' => $v['decorate_id'],
                    'name' => $v['cart_product_info']['name'],
                    'standard_name' => $v['cart_product_info']['standard_name'],
                    'img' => $v['cart_product_info']['image'],
                    'price' => $v['cart_product_info']['discount_price'],
                    'type' => $v['type'],
                    'from' => $v['from'],
                    'standard_id' => $v['standard_id'],
                    'color_id' => $v['color_id'],
                    'count' => $v['count'],
                    'create_time' => time(),
                ];


                //1定制2整配
                if (in_array($v['type'], ['1', '2'])){
                    $scheme_temp = CustomizedScheme::get($v['product_id'])->toArray();
                    $scheme_temp['order_id'] = $order_id;
                    $scheme_temp['product_id'] = $v['product_id'];
                    unset($scheme_temp['id']);
                    $scheme[] = $scheme_temp;
                }

            }
            //插入订单详情数据
            if (count($orderdetails) >= 1){
                OrderDetail::build()
                    ->insertAll($orderdetails);
            }

            //插入订单方案详情数据
            if (count($scheme) >= 1){
                OrderCustomizedScheme::build()->insertAll($scheme);
            }


            //插入收货地址
            $area['order_id'] = $order_id;
            OrderArea::build()->create($area, true);


            //支付
            switch ($requestData['pay_type']) {
                case 'wechat':
                    $wechat_config = Config::get('wechat');

                    $body = $wechat_config['wx_pay_charge'];
                    $appId = $wechat_config['AppID'];
                    $mchId = $wechat_config['Pay']['MchId'];
                    $appKey = $wechat_config['Pay']['Key'];
                    $wechatPay = new WechatPay($appId, $mchId, $appKey);

                    $result = $wechatPay->order($order_num, 0.01, '', $body,
                        $order_id, $requestData['trade_type'], PUBLIC_PATH . '/index.php/api/Callback/orderWXPay');//todo::测试临时

                    if ($result === false) {
                        throw new \Exception('支付订单生成失败', 0);
                    }
                    break;
                //微信是后台调用接口拿到是否生成订单的返回结果
                case 'alipay':
                    $alipay_config = Config::get('alipay');

                    $appId = $alipay_config['AppID'];
                    $partner_public_key = $alipay_config['partnerPublicKey'];
                    $partner_private_key = $alipay_config['rsaPrivateKey'];
                    $pay_charge = $alipay_config['pay_charge'];
                    $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
                    if ($requestData['trade_type'] == 'NATIVE') {
                        $result = $alipayClient->webOrder($order_num, '0.01', $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/orderAlipay');
                    } else {
                        $result = $alipayClient->order($order_num, '0.01', $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/orderAlipay');
                    }
                    break;
                //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
                default:
                    $result = false;
                    break;
            }



            $data['is_error'] = '0';
            $data['result'] = $result;
            $data['order_id'] = $order_id;
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            $data['message'] = $e->getMessage();
        }


        return $data;

    }

    /**
     * User: zhaoxin
     * Date: 2020/4/10
     * Time: 4:11 下午
     * description
     * @param $requestData
     * @param $from 1商城，2易家整配，3定制
     * @return mixed
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function pay($requestData, $from){
        $cart_info = [];
        $data['is_error'] = '1';

        $list[] = self::info($requestData, $from);

        foreach ($list as $k => $v){
            //商城类商品判断库存
            if (($v['from'] == '1' || $v['type'] == '3') && ($v['cart_product_info']['stocks'] <= 0 || $v['cart_product_info']['stocks'] < $v['count'])){
                $data['message'] = $v['cart_product_info']['standard_name'].'的库存不足';
                return $data;
            }

            if ($v['cart_product_info']['is_delete'] == 1){
                $data['message'] = $v['cart_product_info']['standard_name'].'已被删除或下架';
                return $data;
            }

            $cart_info[] = [
                'from' => $v['from'],
                'type' => $v['type'],
                'color_id' => $v['color_id'],
                'standard_id' => $v['standard_id'],
                'decorate_product_id' => $v['decorate_product_id'],
                'decorate_id' => $v['decorate_id'],
                'product_id' => $v['product_id'],
                'count' => $v['count'],
            ];
        }

        //总金额
        $total_price = 0;
        foreach ($list as $k => $v){
            $total_price += $v['cart_product_info']['discount_price'] * $v['count'];
        }

        //收货地址
        $area = UserAreaModel::get($requestData['area_id'])->toArray();
        $order_num = substr(md5(uniqid(mt_rand(), true)), 0, 4) . time();

        Db::startTrans();
        try{
            foreach ($list as $k => $v){
                //商城类商品扣除库存
                if (($v['from'] == '1' || $v['type'] == '3')){
                    ShopProductStandard::build()
                        ->where(['standard_id' => $v['standard_id'], 'product_id' => $v['product_id']])
                        ->setDec('stocks', $v['count']);
                }
            }

            //插入订单数据
            $orderinfo = [
                'user_id' => $requestData['user_id'],
                'order_num' => $order_num,
                'price' => $total_price,
                'express_cost' => '0',
                'status' => '未支付',
                'pay_type' => $requestData['pay_type'],
                'user_remarks' => $requestData['user_remarks'],
                'delivery_time' => $requestData['delivery_time'],
                'create_time' => time(),
            ];
            $order_id = Order::build()
                ->insert($orderinfo, '', true);

            //存储购物车数据
            RedisUtil::getInstance()->set($order_num, serialize($cart_info), 60*32);


            $orderdetails = [];
            $scheme = [];//方案数据
            foreach ($list as $k => $v){
                $orderdetails[] = [
                    'order_id' => $order_id,
                    'product_id' => $v['product_id'],
                    'decorate_product_id' => $v['decorate_product_id'],
                    'decorate_id' => $v['decorate_id'],
                    'name' => $v['cart_product_info']['name'],
                    'standard_name' => $v['cart_product_info']['standard_name'],
                    'img' => $v['cart_product_info']['image'],
                    'price' => $v['cart_product_info']['discount_price'],
                    'type' => $v['type'],
                    'from' => $v['from'],
                    'standard_id' => $v['standard_id'],
                    'color_id' => $v['color_id'],
                    'count' => $v['count'],
                    'create_time' => time(),
                ];


                //1定制2整配
                if (in_array($v['type'], ['1', '2'])){
                    $scheme_temp = CustomizedScheme::get($v['product_id'])->toArray();
                    $scheme_temp['order_id'] = $order_id;
                    $scheme_temp['product_id'] = $v['product_id'];
                    unset($scheme_temp['id']);
                    $scheme[] = $scheme_temp;
                }

            }
            //插入订单详情数据
            if (count($orderdetails) >= 1){
                OrderDetail::build()
                    ->insertAll($orderdetails);
            }

            //插入订单方案详情数据
            if (count($scheme) >= 1){
                OrderCustomizedScheme::build()->insertAll($scheme);
            }


            //插入收货地址
            $area['order_id'] = $order_id;
            OrderArea::build()->create($area, true);


            //支付
            switch ($requestData['pay_type']) {
                case 'wechat':
                    $wechat_config = Config::get('wechat');

                    $body = $wechat_config['wx_pay_charge'];
                    $appId = $wechat_config['AppID'];
                    $mchId = $wechat_config['Pay']['MchId'];
                    $appKey = $wechat_config['Pay']['Key'];
                    $wechatPay = new WechatPay($appId, $mchId, $appKey);

                    $result = $wechatPay->order($order_num, 0.01, '', $body,
                        $order_id, $requestData['trade_type'], PUBLIC_PATH . '/index.php/api/Callback/orderWXPay');//todo::测试临时

                    if ($result === false) {
                        throw new \Exception('支付订单生成失败', 0);
                    }
                    break;
                //微信是后台调用接口拿到是否生成订单的返回结果
                case 'alipay':
                    $alipay_config = Config::get('alipay');

                    $appId = $alipay_config['AppID'];
                    $partner_public_key = $alipay_config['partnerPublicKey'];
                    $partner_private_key = $alipay_config['rsaPrivateKey'];
                    $pay_charge = $alipay_config['pay_charge'];
                    $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
                    if ($requestData['trade_type'] == 'NATIVE') {
                        $result = $alipayClient->webOrder($order_num, '0.01', $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/orderAlipay');
                    } else {
                        $result = $alipayClient->order($order_num, '0.01', $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/orderAlipay');
                    }
                    break;
                //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
                default:
                    $result = false;
                    break;
            }



            $data['is_error'] = '0';
            $data['result'] = $result;
            $data['order_id'] = $order_id;
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            $data['message'] = $e->getMessage();
        }


        return $data;

    }

    public static function index($requestData){
        $where = ['o.user_id' => $requestData['user_id']];

        if (!empty($requestData['status'])){
            $where['o.status'] = $requestData['status'];
        }


        return Order::build()
            ->alias('o')
            ->join("(select sum(count) as product_count, order_id from yd_order_detail group by order_id) d",'d.order_id = o.id')
            ->where($where)
            ->field([
                'o.id', 'o.price', 'o.status', 'o.pay_type','d.product_count', 'o.id order_product_info'
            ])
            ->order(['o.id' => 'DESC'])
            ->page($requestData['page_index'], $requestData['page_size'])
            ->select();
    }

    public static function read($requestData){
        $info = Order::build()
            ->alias('o')
            ->join("(select sum(count) as product_count, order_id from yd_order_detail group by order_id) d",'d.order_id = o.id')
            ->where(['o.id' => $requestData['id'], 'o.user_id' => $requestData['user_id']])
            ->field([
                'o.id', 'o.price', 'o.status', 'o.pay_type','d.product_count', 'o.id order_product_info',
                'o.create_time', 'o.pay_time', 'o.order_num', 'o.user_remarks', 'o.delivery_time'
            ])
            ->find();
        $info['create_time'] = date('Y-m-d H:i:s', $info['create_time']);
        $info['pay_time'] = (!empty($info['pay_time'])) ? date('Y-m-d H:i:s', $info['pay_time']) : '';
        $info['delivery_time'] = (!empty($info['delivery_time'])) ? date('Y-m-d H:i:s', $info['delivery_time']) : '';

        return $info;
    }

    public static function getOrderInfo($out_trade_no)
    {
        return Order::build()->where(['order_num' => $out_trade_no])->find();
    }

    public static function updateOrderStatus($out_trade_no, $data)
    {
        return Order::build()->update($data, ['order_num' => $out_trade_no]);
    }
}
