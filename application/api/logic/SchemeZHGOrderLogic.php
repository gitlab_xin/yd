<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/10/7
 * Time: 15:12
 */

namespace app\api\logic;

use app\common\model\ProductBean;
use app\common\model\SchemeBean;
use app\common\tools\CommonMethod;

class SchemeZHGOrderLogic
{
    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:统计综合柜功能件
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function scoreZHGSchemeProducts(SchemeBean $scheme)
    {
        $score_products = [];

        if (!empty($scheme->getScheme_g_products())) {
            foreach ($scheme->getScheme_wcb_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_g_products())) {
            foreach ($scheme->getScheme_g_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        /*
         * 统计外侧板
         */
        if (!empty($scheme->getScheme_wcb_products())) {
            foreach ($scheme->getScheme_wcb_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_wcb_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }
        /*
         * 统计内侧板
         */
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_ncb_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }
        /*
         * 统计盖板
         */
        if (!empty($scheme->getScheme_g_products())) {
            foreach ($scheme->getScheme_g_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_g_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }

        //统计列内组件
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            if (empty($s->getScheme_no())) {
                $scheme_b_type = $s->getScheme_b_type();
                foreach ($s->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    $product_type = $p->getProduct_type();
                    //CT:抽屉 ZZ:桌子 JJ:酒架
                    if (strpos($product_type, 'CT') === false && strpos($product_type, 'ZZ') === false && strpos($product_type, 'JJ') === false
                        &&strpos($scheme_b_type, 'YYST') === false &&strpos($scheme_b_type, 'CWSN') === false) {//电视柜或者书柜
                        $p->setProduct_is_score(0);
                        $scheme_products[] = $p;
                    }
                }
            } else {
                foreach ($s->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    //统计组件衔接侧板
                    if (strpos($p->getProduct_type(), 'TVX') !== false) {
                        $p->setProduct_is_score(0);
                        $scheme_products[] = $p;
                    }
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme_products as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        /*
         * 统计桌面
         */
//        foreach ($scheme->getScheme_schemes() as $s) {
//            $s = change2SchemeBean($s);
//            $scheme_no = $s->getScheme_no();
//            if (!empty($scheme_no) && strpos($scheme_no, 'Z-DZX') !== false) {
//                foreach ($s->getScheme_products() as $p) {
//                    $p = change2ProductBean($p);
//                    if ($p->getProduct_type() == 'ZM') {
//                        $p->setProduct_count(1);
//                        $score_products[] = $p;
//                    }
//                }
//            }
//        }

        $scheme->setScheme_score_products($score_products);

        return $scheme;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:统计综合柜组合功能件
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function scoreZHGSchemeZJProducts(SchemeBean $scheme)
    {
        $score_products = $doorSchemes = $doorSchemesMap = $jj_products = $jjProductsMap = $ct_products = $ctProductsMap = [];
        $totalPrice = $dis_totalPrice = 0;

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_no = $s->getScheme_no();
            /*
             * 门
             */
            if (!empty($s->getScheme_door_schemes())) {
                $doorSchemes = array_merge($doorSchemes, $s->getScheme_door_schemes());
            }

            foreach ($s->getScheme_products() as $p) {
                $p = change2ProductBean($p);
                /*
                 * 酒架
                 */
                if (str_has($p->getProduct_type(), 'JJ')) {
                    $jj_products[] = $p;
                }
                /*
                 * 抽屉组件
                 */
                if (empty($scheme_no) && str_has($p->getProduct_type(), 'CT')) {
                    $ct_products[] = $p;
                }
            }
        }
        /*
         * 门
         */
//        foreach ($doorSchemes as $d_s) {
//            $d_s = change2SchemeBean($d_s);
//            $scheme_no = $d_s->getScheme_no();
//            if (!isset($doorSchemesMap[$scheme_no])) {
//                $d_s->setScheme_count(($d_s->getScheme_width() == 806 ? 2 : 1));
//                $doorSchemesMap[$scheme_no] = $d_s;
//            } else {
//                $d_s = $doorSchemesMap[$scheme_no];
//                $d_s->setScheme_count(($d_s->getScheme_width() == 806 ? 2 : 1));
//            }
//        }

        foreach ($doorSchemes as $d_s) {
            $d_s = change2SchemeBean($d_s);
            if ($d_s->getScheme_is_score() == 1){
                continue;
            }
            $scheme_no = $d_s->getScheme_no();
            $d_s->setScheme_count(0);
            foreach ($doorSchemes as $d_s_t) {
                $d_s_t = change2SchemeBean($d_s_t);
                if ($d_s_t->getScheme_is_score() == 1){
                    continue;
                }
                if ($d_s->getScheme_no() != $d_s_t->getScheme_no()) {
                    continue;
                }
                $dolor_count = ($d_s->getScheme_width() == 806 ? 2 : 1);
                $d_s->setScheme_count($d_s->getScheme_count() + $dolor_count);
                $d_s_t->setScheme_is_score(1);
            }
            $d_s->setScheme_is_score(1);
            $doorSchemesMap[$scheme_no] = $d_s;

        }

        $scheme->setScheme_door_schemes(array_values($doorSchemesMap));

        foreach ($doorSchemesMap as $d_s) {
            $d_s = change2SchemeBean($d_s);
            switch ($d_s->getScheme_type()) {
                case 'PB':
                    $height = $d_s->getScheme_height();
                    $width = $d_s->getScheme_width();

                    $d_product = new ProductBean();
                    $d_product->setSeq_type(700);
                    $d_product->setSeq($width);
                    $d_product->setProduct_name($d_s->getScheme_name());
                    $d_product->setProduct_no($d_s->getScheme_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($d_s->getScheme_color_no()));
                    $d_product->setProduct_color_no($d_s->getScheme_color_no());
                    $d_product->setProduct_width($width);
                    $d_product->setProduct_height($height);
                    switch ($height) {
                        case 1917:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(390));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(410));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(310));
                                    break;
                            }
                            break;
                        case 1597:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(340));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(360));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(270));
                                    break;
                            }
                            break;
                        case 1277:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(290));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(310));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(230));
                                    break;
                            }
                            break;
                        case 957:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(240));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(260));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(190));
                                    break;
                            }
                            break;
                        case 637:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(190));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(210));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(150));
                                    break;
                            }
                            break;
                    }
                    $d_product->setProduct_count($d_s->getScheme_count());
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                    break;
                case 'BL':
                    $height = $d_s->getScheme_height();
                    $width = $d_s->getScheme_width();

                    $d_product = new ProductBean();
                    $d_product->setSeq_type(800);
                    $d_product->setSeq($width);
                    $d_product->setProduct_name($d_s->getScheme_name());
                    $d_product->setProduct_no($d_s->getScheme_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($d_s->getScheme_color_no()));
                    $d_product->setProduct_color_no($d_s->getScheme_color_no());
                    $d_product->setProduct_width($width);
                    $d_product->setProduct_height($height);
                    switch ($height) {
                        case 1917:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(680));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(700));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(660));
                                    break;
                            }
                            break;
                        case 1277:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(580));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(600));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(560));
                                    break;
                            }
                            break;
                        case 957:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(530));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(550));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(510));
                                    break;
                            }
                            break;
                        case 637:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(480));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(500));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(460));
                                    break;
                            }
                            break;
                    }
                    $d_product->setProduct_count($d_s->getScheme_count());
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    break;
                case 'LHJ':
                    $height = $d_s->getScheme_height();
                    $width = $d_s->getScheme_width();

                    $d_product = new ProductBean();
                    $d_product->setSeq_type(900);
                    $d_product->setSeq($width);
                    $d_product->setProduct_name($d_s->getScheme_name());
                    $d_product->setProduct_no($d_s->getScheme_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($d_s->getScheme_color_no()));
                    $d_product->setProduct_color_no($d_s->getScheme_color_no());
                    $d_product->setProduct_width($width);
                    $d_product->setProduct_height($height);
                    switch ($height) {
                        case 1917:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(860));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(880));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(840));
                                    break;
                            }
                            break;
                        case 1277:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(760));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(780));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(740));
                                    break;
                            }
                            break;
                        case 957:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(710));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(730));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(690));
                                    break;
                            }
                            break;
                        case 637:
                            switch ($width) {
                                case 454:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(660));
                                    break;
                                case 582:
                                    $d_product->setProduct_spec($height . '*' . $width . '*16');
                                    $d_product->setProduct_price(formatMoney(680));
                                    break;
                                case 806:
                                    $d_product->setProduct_spec($height . '*401*16');
                                    $d_product->setProduct_width(401);
                                    $d_product->setProduct_price(formatMoney(640));
                                    break;
                            }
                            break;
                    }
                    $d_product->setProduct_count($d_s->getScheme_count());
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    break;
                default:
                    break;
            }
        }
        /*
         * 酒架
         */
        if (count($jj_products) > 0) {
            foreach ($jj_products as $p) {
                $p = change2ProductBean($p);
                $product_no = $p->getProduct_no();
                if (isset($jjProductsMap[$product_no])) {
                    $jj_p = change2ProductBean($jjProductsMap[$product_no]);
                    $jj_p->setProduct_count($jj_p->getProduct_count() + 1);
                } else {
                    $p->setProduct_count(1);
                    $jjProductsMap[$product_no] = $p;
                }
            }
            foreach ($jjProductsMap as $jj_p) {
                $jj_p = change2ProductBean($jj_p);
                $width = $jj_p->getProduct_width();

                $d_product = new ProductBean();
                $d_product->setSeq_type(900);
                $d_product->setSeq($width);
                $d_product->setProduct_name($width . '酒架');
                $d_product->setProduct_color_no('004');
                $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($d_product->getProduct_color_no()));
                $d_product->setProduct_count($jj_p->getProduct_count());
                $d_product->setProduct_no($jj_p->getProduct_no());

                switch ($width) {
                    case 560:
                        $d_product->setProduct_price(formatMoney(470));
                        break;
                    case 784:
                        $d_product->setProduct_price(formatMoney(530));
                        break;
                    default:
                        break;
                }

                $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                $score_products[] = $d_product;
            }
        }

        /*
         * 抽屉组件
         */
        if (count($ct_products) > 0) {
            foreach ($ct_products as $p) {
                $p = change2ProductBean($p);
                $product_no = $p->getProduct_no();
                if (strpos($product_no, '-2') > 0) {
                    $pNo = str_replace('-2', '', $product_no);

                    if (isset($ctProductsMap[$pNo])) {
                        $ct_p = change2ProductBean($ctProductsMap[$pNo]);
                        $ct_p->setProduct_count($ct_p->getProduct_count() + 2);
                    } else {
                        $p->setProduct_count(2);
                        $ctProductsMap[$pNo] = $p;
                    }
                } else {
                    $pNo = str_replace('-2', '', $product_no);

                    //type 分为高矮两，使用编码会导致没有合并
                    $type = ($p->getProduct_type() == 'GCT') ? 'GCT' : 'ACT';
                    if (isset($ctProductsMap[$type])) {
                        $ct_p = change2ProductBean($ctProductsMap[$type]);
                        if ($p->getProduct_type() == 'SCT') {
                            $ct_p->setProduct_count($ct_p->getProduct_count() + 2);
                        } else {
                            $ct_p->setProduct_count($ct_p->getProduct_count() + 1);
                        }
                    } else {
                        if ($p->getProduct_type() == 'SCT') {
                            $p->setProduct_count(2);
                        } else {
                            $p->setProduct_count(1);
                        }
                        $ctProductsMap[$type] = $p;
                    }

                }
            }
            foreach ($ctProductsMap as $ct_p) {
                $ct_p = change2ProductBean($ct_p);
                if ($ct_p->getProduct_type() == 'GCT') {
                    $d_product = new ProductBean();
                    $width = $ct_p->getProduct_width() - 22;

                    $d_product->setSeq_type(500);
                    $d_product->setSeq($width);
                    $d_product->setProduct_name($width . '高抽屉');
                    $d_product->setProduct_deep(16);
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($ct_p->getProduct_color_no()));
                    $d_product->setProduct_color_no($ct_p->getProduct_color_no());
                    $d_product->setProduct_count($ct_p->getProduct_count());
                    $d_product->setProduct_width($width + 22);
                    $d_product->setProduct_height(317);
                    $d_product->setProduct_deep(16);
                    $d_product->setProduct_no("T-G-" . ($width) . "-00-" . $ct_p->getProduct_color_no());

                    switch ($width) {
                        case 432:
                            $d_product->setProduct_price(formatMoney(290));
                            break;
                        case 560:
                            $d_product->setProduct_price(formatMoney(320));
                            break;
                        case 784:
                            $d_product->setProduct_price(formatMoney(365));
                            break;
                        default:
                            break;
                    }
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
//                    $score_products[] = $d_product;

//                    $d_product_content = new ProductBean();
//
//                    $d_product_content->setSeq_type(540);
//                    $d_product_content->setSeq($width);
//                    $d_product_content->setProduct_name($width . '高抽屉后板');
//                    $d_product_content->setProduct_color_name('灰布纹');
//                    $d_product_content->setProduct_color_no('000');
//                    $d_product_content->setProduct_count($ct_p->getProduct_count());
//                    $d_product_content->setProduct_no("T-H-" . ($width - 50) . "-289-00-000");
//                    $d_product_content->setProduct_width($width - 50);
//                    $d_product_content->setProduct_height(289);
//                    $d_product_content->setProduct_deep(12);
//
//                    $product_content[] = $d_product_content;

                    $mflag = $hflag = $lflag = $rflag = $dflag = false;
                    $mpNo = "T-M-" . ($width + 22) . "-317-00-" . $ct_p->getProduct_color_no();
                    $hpNo = "T-H-" . ($width - 50) . "-289-00-000";
                    $lpNo = 'T-L-290-289-00-000';
                    $rpNo = 'T-R-290-289-00-000';
                    $dpNo = 'T-D-' . ($width - 40) . "-288-00-" . $scheme->getScheme_color_no();

                    foreach ($score_products as $sp) {
                        $sp = change2ProductBean($sp);

                        switch ($sp->getProduct_no()) {
                            case $mpNo://抽屉面板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $mflag = true;
                                break;
                            case $hpNo://抽屉后板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $lflag = true;
                                break;
                            case $lpNo://抽屉侧板(左)
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $lflag = true;
                                break;
                            case $rpNo://抽屉侧板(右)
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $rflag = true;
                                break;
                            case $dpNo://抽屉底板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $dflag = true;
                                break;
                            default:
                                break;
                        }
                    }
                    $product_content = [];
                    if (!$mflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name($width . '高抽屉面板');
                        $d_product_content->setSeq_type(500);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_color_name(CommonMethod::getSchemeColorName($ct_p->getProduct_color_no()));
                        $d_product_content->setProduct_deep(16);
                        $d_product_content->setProduct_color_no($ct_p->getProduct_color_no());
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($mpNo);
                        $d_product_content->setProduct_width($width + 22);
                        $d_product_content->setProduct_height(317);
                        $d_product_content->setProduct_deep(16);
                        $product_content[] = $d_product_content;
                    }

                    if (!$hflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name($width . '高抽屉后板');
                        $d_product_content->setSeq_type(540);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($hpNo);
                        $d_product_content->setProduct_width($width - 50);
                        $d_product_content->setProduct_height(289);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;
                    }

                    if (!$lflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name('高抽屉侧板(左)');
                        $d_product_content->setSeq_type(510);
                        $d_product_content->setSeq($ct_p->getProduct_height());
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($lpNo);
                        $d_product_content->setProduct_width(290);
                        $d_product_content->setProduct_height(289);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;

                    }
                    if (!$rflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name('高抽屉侧板(右)');
                        $d_product_content->setSeq_type(520);
                        $d_product_content->setSeq($ct_p->getProduct_height());
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($rpNo);
                        $d_product_content->setProduct_width(290);
                        $d_product_content->setProduct_height(289);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;

                    }
                    if (!$dflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name($width . '抽屉底板');
                        $d_product_content->setSeq_type(530);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                        $d_product_content->setProduct_color_no($scheme->getScheme_color_no());
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($dpNo);
                        $d_product_content->setProduct_width($width - 40);
                        $d_product_content->setProduct_height(288);
                        $d_product_content->setProduct_deep(5);
                        $product_content[] = $d_product_content;

                    }

                    $d_product->setProductContent($product_content);
                    $score_products[] = $d_product;
                } else {
                    $d_product = new ProductBean();
                    $width = $ct_p->getProduct_width() - 22;
//                    if ($ct_p->getProduct_type() == 'SCT') {
//                        $ct_p->setProduct_count($ct_p->getProduct_count() * 2);
//                    }

                    $d_product->setSeq_type(500);
                    $d_product->setSeq($width);
                    $d_product->setProduct_name($width . '矮抽屉');
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($ct_p->getProduct_color_no()));
                    $d_product->setProduct_color_no($ct_p->getProduct_color_no());
                    $d_product->setProduct_count($ct_p->getProduct_count());
                    $d_product->setProduct_width($width + 22);
                    $d_product->setProduct_height(157);
                    $d_product->setProduct_deep(16);
                    $d_product->setProduct_no("T-A-" . ($width) . "-00-" . $ct_p->getProduct_color_no());

                    switch ($width) {
                        case 432:
                            $d_product->setProduct_price(formatMoney(235));
                            break;
                        case 560:
                            $d_product->setProduct_price(formatMoney(260));
                            break;
                        case 784:
                            $d_product->setProduct_price(formatMoney(310));
                            break;
                        default:
                            break;
                    }
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
//                    $score_products[] = $d_product;

//                    $d_product = new ProductBean();


                    $aflag = $hflag = $lflag = $rflag = $dflag = false;
                    $apNp = "T-M-" . ($width + 22) . "-157-00-" . $ct_p->getProduct_color_no();
                    $hpNo = "T-H-" . ($width - 50) . "-129-00-000";
                    $lpNo = 'T-L-290-129-00-000';
                    $rpNo = 'T-R-290-129-00-000';
                    $dpNo = 'T-D-' . ($width - 40) . "-288-00-" . $scheme->getScheme_color_no();

                    foreach ($score_products as $sp) {
                        $sp = change2ProductBean($sp);

                        switch ($sp->getProduct_no()) {

                            case $apNp://抽屉后板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $aflag = true;
                                break;
                            case $hpNo://抽屉后板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $hflag = true;
                                break;
                            case $lpNo://抽屉侧板(左)
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $lflag = true;
                                break;
                            case $rpNo://抽屉侧板(右)
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $rflag = true;
                                break;
                            case $dpNo://抽屉底板
                                $sp->setProduct_count($sp->getProduct_count() + $ct_p->getProduct_count());
                                $dflag = true;
                                break;
                            default:
                                break;
                        }
                    }

                    $product_content = [];
                    if (!$aflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setSeq_type(500);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_name($width . '矮抽屉面板');
                        $d_product_content->setProduct_color_name(CommonMethod::getSchemeColorName($ct_p->getProduct_color_no()));
                        $d_product_content->setProduct_color_no($ct_p->getProduct_color_no());
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($apNp);
                        $d_product_content->setProduct_width($width + 22);
                        $d_product_content->setProduct_height(157);
                        $d_product_content->setProduct_deep(16);
                        $product_content[] = $d_product_content;
//                        continue;
                    }

                    if (!$hflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setSeq_type(540);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_name($width . '矮抽屉后板');
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($hpNo);
                        $d_product_content->setProduct_width($width - 50);
                        $d_product_content->setProduct_height(129);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;

                    }
                    if (!$lflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name('矮抽屉侧板(左)');
                        $d_product_content->setSeq_type(510);
                        $d_product_content->setSeq($ct_p->getProduct_height());
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($lpNo);
                        $d_product_content->setProduct_width(290);
                        $d_product_content->setProduct_height(129);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;

                    }
                    if (!$rflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name('矮抽屉侧板(右)');
                        $d_product_content->setSeq_type(520);
                        $d_product_content->setSeq($ct_p->getProduct_height());
                        $d_product_content->setProduct_color_name('灰布纹');
                        $d_product_content->setProduct_color_no('000');
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($rpNo);
                        $d_product_content->setProduct_width(290);
                        $d_product_content->setProduct_height(129);
                        $d_product_content->setProduct_deep(12);
                        $product_content[] = $d_product_content;
                    }
                    if (!$dflag) {
                        $d_product_content = new ProductBean();
                        $d_product_content->setProduct_name($width . '抽屉底板');
                        $d_product_content->setSeq_type(530);
                        $d_product_content->setSeq($width);
                        $d_product_content->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                        $d_product_content->setProduct_color_no($scheme->getScheme_color_no());
                        $d_product_content->setProduct_count($ct_p->getProduct_count());
                        $d_product_content->setProduct_no($dpNo);
                        $d_product_content->setProduct_width($width - 40);
                        $d_product_content->setProduct_height(288);
                        $d_product_content->setProduct_deep(5);
                        $product_content[] = $d_product_content;

                    }
                    $d_product->setProductContent($product_content);
                    $score_products[] = $d_product;
                }
            }
        }

        $scheme->setScheme_ct_products($ct_products);

        /*
         * 固定组件
         */
        foreach ($scheme->getScheme_schemes() as $g_s) {
            $g_s = change2SchemeBean($g_s);
            $scheme_no = $g_s->getScheme_no();
            if (!empty($scheme_no)) {
                if (str_has($scheme_no, 'Z-DZG432')) {//432*2垂直桌柜体组件
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(851));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;

                    //写字桌
//                    $d_product = new ProductBean();
//
//                    $d_product->setSeq_type(610);
//                    $d_product->setProduct_name('垂直写字桌');
//                    $d_product->setProduct_color_name(getSchemeColorName($g_s->getScheme_color_no()));
//                    $d_product->setProduct_color_no($g_s->getScheme_color_no());
//                    $d_product->setProduct_no('Z-DZ1478-' . $g_s->getScheme_color_no());
//                    $d_product->setProduct_count(1);
//                    $d_product->setProduct_price(980);
//                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
//                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
//                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
//                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
//                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
//                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'Z-DZG560')) {//560*2垂直桌柜体组件
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(1026));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;

                    //写字桌
//                    $d_product = new ProductBean();
//
//                    $d_product->setSeq_type(610);
//                    $d_product->setProduct_name('垂直写字桌');
//                    $d_product->setProduct_color_name($g_s->getScheme_color_name());
//                    $d_product->setProduct_color_no($g_s->getScheme_color_no());
//                    $d_product->setProduct_no('Z-DZ1478-' . $g_s->getScheme_color_no());
//                    $d_product->setProduct_count(1);
//                    $d_product->setProduct_price(980);
//                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
//                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
//                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
//                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
//                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
//                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'Z-DZX432')) {// 432*2写字桌组件（含桌面）
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(1046));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'Z-DZX560')) {//560*2写字桌组件（含桌面）
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(1302));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'Z-DZS560')) {// 两单元上部组件
                    list($name1, $name2) = explode(':', $g_s->getScheme_name());
                    list($no1, $no2) = explode(':', $g_s->getScheme_no());

                    $d_product = new ProductBean();
                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($name1);
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($no1);
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(1264));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;

                    $d_product = new ProductBean();
                    $d_product->setProduct_name($name2);
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($no2);
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(820));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'Z-DZS784')) {//784*2上部组件
                    // 提交过来的scheme_name数据样例：    scheme_name=1590视听柜:784*2上部组件
                    list($name1, $name2) = explode(':', $g_s->getScheme_name());
                    list($no1, $no2) = explode(':', $g_s->getScheme_no());

                    //1590视听柜
                    $d_product = new ProductBean();
                    $d_product->setSeq_type(600);
                    $d_product->setProduct_name($name1);
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($no1);
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(2295));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;

                    //784*2上部组件
                    $d_product = new ProductBean();
                    $d_product->setProduct_name($name2);
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($no2);
                    $d_product->setProduct_width($g_s->getScheme_width());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(818));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'G-TV114')) {// 1143视听柜
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(1000);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(1265));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'G-TV159')) {//1590视听柜
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(1000);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(2295));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                } elseif (str_has($scheme_no, 'G-TV200')) {//2001视听柜
                    $d_product = new ProductBean();

                    $d_product->setSeq_type(1000);
                    $d_product->setProduct_name($g_s->getScheme_name());
                    $d_product->setProduct_color_no($scheme->getScheme_color_no());
                    $d_product->setProduct_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $d_product->setProduct_no($g_s->getScheme_no());
                    $d_product->setProduct_count(1);
                    $d_product->setProduct_price(formatMoney(2735));
                    $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                    $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                    $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                    $score_products[] = $d_product;
                }
            }
        }

        /*
         * 写字桌
         */
        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_no = $s->getScheme_no();
            if (!empty($scheme_no)) {
                foreach ($s->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    if ($p->getProduct_type() == 'ZZ') {
                        $d_product = new ProductBean();

                        $d_product->setSeq_type(610);
                        $d_product->setProduct_name('垂直写字桌');
                        $d_product->setProduct_color_name($p->getProduct_color_name());
                        $d_product->setProduct_color_no($p->getProduct_color_no());
                        $d_product->setProduct_no('Z-DZ1478-' . $p->getProduct_color_no());
                        $d_product->setProduct_count(1);
                        $d_product->setProduct_price(formatMoney(980));
                        $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $scheme->getScheme_discount()));
                        $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                        $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                        $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                        $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                        $score_products[] = $d_product;
                    }
                }
            }
        }

        $scheme->setScheme_zj_price($totalPrice);
        $scheme->setScheme_dis_zj_price($dis_totalPrice);
        $scheme->setScheme_score_zj_products($score_products);

        return $scheme;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:统计综合柜板材面积
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function scoreZHGSchemeplates(SchemeBean $scheme)
    {
        $totalPrice = $dis_totalPrice = 0;
        $deep_arr = [25, 16, 12, 5];
        $scheme_plates = [];

        foreach ($deep_arr as $i => $deep) {
            $p_product = new ProductBean();
            $p_product->setProduct_deep($deep);
            $p_product->setProduct_name($deep . 'mm厚');

            foreach ($scheme->getScheme_score_products() as $p) {
                $p = change2ProductBean($p);
                if ($deep == $p->getProduct_deep()) {
                    if ($deep_arr[$i] == 5) { // 背板
                        $p_product->setProduct_area(
                            number_format(getLength($p->getProduct_width() + 10) * getLength($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    } else {
                        // 其他板
                        $p_product->setProduct_area(
                            number_format(getLength($p->getProduct_width()) * getLength($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    }
                }
            }

            $disCount = $scheme->getScheme_discount();
            if (0 < $p_product->getProduct_area()) {
                $p_product->setProduct_price(formatMoney(getPlatePrice($i)));
                $this->countPlateMoney(getPlatePrice($i), $disCount, $p_product, $totalPrice, $dis_totalPrice);
            }
            $scheme_plates[] = $p_product;
        }

        $scheme->setScheme_score_plates($scheme_plates);
        $scheme->setScheme_plate_price($totalPrice);
        $scheme->setScheme_dis_plate_price($dis_totalPrice);

        return $scheme;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:统计综合柜五金件
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function scoreZHGSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            if (!empty($s->getScheme_no())) {
                continue;
            }
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }
        if (!empty($scheme->getScheme_wcb_products())) {
            $scheme_products = array_merge($scheme_products, $scheme->getScheme_wcb_products());
        }
        if (!empty($scheme->getScheme_ncb_products())) {
            $scheme_products = array_merge($scheme_products, $scheme->getScheme_ncb_products());
        }

        $wujinMap = [];
        $t_p = null;

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            $product_type = $p->getProduct_type();
            if ($product_type == 'DB') {            //背板
                if (!isset($wujinMap['W02-01-3*25-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('25沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*25-00');
                    $t_p->setProduct_count(8);
                    $t_p->setProduct_price(formatMoney(0));
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(10);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*25-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 8);
                }
            } elseif ($product_type == 'DH') {        //层板 横隔板
                if (!isset($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('层板连接件');
                    $t_p->setProduct_no('W01-01-XX-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            } elseif ($product_type == 'DD') {       // 底担
                if (!isset($wujinMap['W01-03-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('底担连接件');
                    $t_p->setProduct_no('W01-03-XX-00');
                    $t_p->setProduct_count(2);
                    $t_p->setProduct_unit('套');
                    $t_p->setSeq(89);
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-03-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
                }

                if (!isset($wujinMap['W02-01-3*20-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('20沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*20-00');
                    $t_p->setProduct_price(formatMoney(0));
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(9);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*20-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*25-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('25沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*25-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_price(formatMoney(0));
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(10);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*25-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

            } elseif ($product_type == 'DC' || $product_type == 'TVX-NC' || $product_type == 'DC-X' || $product_type == 'DC-R' || $product_type == 'DC-L') {        // 外侧板 || 衔接内侧板 || 组件衔接侧板 || 中侧板_阶梯柜_右 || 中侧板_阶梯柜_左
                if (!isset($wujinMap['W04-29-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('脚垫');
                    $t_p->setProduct_no('W04-29-XX-00');
                    $t_p->setProduct_count(2);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(88);
                    $t_p->setProduct_price(formatMoney(1.5));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-29-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
                }
            } elseif (strpos($product_type, 'CT') !== false) {        // 抽屉
                $times = $p->getProduct_type() == 'SCT' ? 2 : 1;
                for ($i = 1; $i <= $times; $i++) {
                    if (!isset($wujinMap['WB-000'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('抽屉五金包');
                        $t_p->setProduct_no('WB-000');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(formatMoney(6.2));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-000'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

//                    if (!isset($wujinMap['W02-01-3*13-00'])) {
//                        $t_p = new ProductBean();
//                        $t_p->setProduct_name('13沉头自攻钉');
//                        $t_p->setProduct_no('W02-01-3*13-00');
//                        $t_p->setProduct_count(8);
//                        $t_p->setSeq(7);
//                        $wujinMap[$t_p->getProduct_no()] = $t_p;
//                    } else {
//                        $t_p = $wujinMap['W02-01-3*13-00'];
//                        $t_p = change2ProductBean($t_p);
//                        $t_p->setProduct_count($t_p->getProduct_count() + 8);
//                    }

                    if (!isset($wujinMap['W07-02-290-00'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('290三节滑轨');
                        $t_p->setProduct_no('W07-02-290-00');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('付');
                        $t_p->setSeq(117);
                        $t_p->setProduct_price(formatMoney(80));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['W07-02-290-00'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                    if (!isset($wujinMap['W08-02-64-00'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('64孔距拉手');
                        $t_p->setProduct_no('W08-02-64-00');
                        $t_p->setProduct_unit('个');
                        $t_p->setSeq(107);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_price(formatMoney(22));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['W08-02-64-00'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                }
            }
        }

        /*
         * 统计单独组件五金
         */
        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_no = $s->getScheme_no();
            if (!empty($scheme_no)) {

                if (str_has($scheme_no, 'Z-DZG')) {//垂直桌柜体组件

                    if (!isset($wujinMap['WB-001'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('垂直桌柜体组件五金包');
                        $t_p->setProduct_no('WB-001');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(formatMoney(174));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-001'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                    if (!isset($wujinMap['WB-002'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('垂直写字桌五金包');
                        $t_p->setProduct_no('WB-002');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(formatMoney(22));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-002'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                } elseif (str_has($scheme_no, 'Z-DZX')) {// 写字桌功能组件

                    if (!isset($wujinMap['WB-004'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('写字桌组件五金包');
                        $t_p->setProduct_no('WB-004');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(formatMoney(150));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-004'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                } elseif (str_has($scheme_no, 'Z-DZS')) {// 两单元上部组件

                    if (!isset($wujinMap['WB-003'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('两单元上部组件五金包');
                        $t_p->setProduct_no('WB-003');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(formatMoney(102));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-003'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

//                    if (str_has($scheme_no, 'Z-560')) {
//                        if (!isset($wujinMap['WB-005'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name('1143视听柜组件五金包');
//                            $t_p->setProduct_no('WB-005');
//                            $t_p->setProduct_count(1);
//                            $t_p->setProduct_unit('包');
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['WB-005'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                        }
//                    } else {
//                        if (!isset($wujinMap['WB-006'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name('1590视听柜组件五金包');
//                            $t_p->setProduct_no('WB-006');
//                            $t_p->setProduct_count(1);
//                            $t_p->setProduct_unit('包');
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['WB-006'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                        }
//                    }

                } elseif (str_has($scheme_no, 'G-TV114')) {// 视听柜
//                    if (!isset($wujinMap['WB-005'])) {
//                        $t_p = new ProductBean();
//                        $t_p->setProduct_name('1143视听柜组件五金包');
//                        $t_p->setProduct_no('WB-005');
//                        $t_p->setProduct_count(1);
//                        $t_p->setProduct_unit('包');
//                        $wujinMap[$t_p->getProduct_no()] = $t_p;
//                    } else {
//                        $t_p = $wujinMap['WB-005'];
//                        $t_p = change2ProductBean($t_p);
//                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                    }
                } elseif (str_has($scheme_no, 'G-TV159')) {// 视听柜
//                    if (!isset($wujinMap['WB-006'])) {
//                        $t_p = new ProductBean();
//                        $t_p->setProduct_name('1590视听柜组件五金包');
//                        $t_p->setProduct_no('WB-006');
//                        $t_p->setProduct_count(1);
//                        $t_p->setProduct_unit('包');
//                        $wujinMap[$t_p->getProduct_no()] = $t_p;
//                    } else {
//                        $t_p = $wujinMap['WB-006'];
//                        $t_p = change2ProductBean($t_p);
//                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                    }
                } elseif (str_has($scheme_no, 'G-TV200')) {// 视听柜
//                    if (!isset($wujinMap['WB-007'])) {
//                        $t_p = new ProductBean();
//                        $t_p->setProduct_name('2001视听柜组件五金包');
//                        $t_p->setProduct_no('WB-007');
//                        $t_p->setProduct_count(1);
//                        $t_p->setProduct_unit('包');
//                        $wujinMap[$t_p->getProduct_no()] = $t_p;
//                    } else {
//                        $t_p = $wujinMap['WB-007'];
//                        $t_p = change2ProductBean($t_p);
//                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                    }
                }
            }
        }

        /*
         * 统计盖板五金
         */
        if (!empty($scheme->getScheme_g_products())) {
            for ($i = 0, $count = count($scheme->getScheme_g_products()); $i < $count; $i++) {
                if (!isset($wujinMap['W02-01-3*25-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('25沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*25-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(10);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*25-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            }
        }

        /*
         * 统计门五金
         */
        if (!empty($scheme->getScheme_door_schemes())) {
            foreach ($scheme->getScheme_door_schemes() as $d_s) {

                $d_s = change2SchemeBean($d_s);
                $scheme_type = $d_s->getScheme_type();
                $scheme_height = $d_s->getScheme_height();

                if ($scheme_type != 'LHJ') {

                    if (!isset($wujinMap['W08-02-64-00'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('64孔距拉手');
                        $t_p->setProduct_no('W08-02-64-00');
                        $t_p->setProduct_unit('个');
                        $t_p->setSeq(107);
                        $t_p->setProduct_count(1 * $d_s->getScheme_count());
                        $t_p->setProduct_price(formatMoney(22));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['W08-02-64-00'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 1 * $d_s->getScheme_count());
                    }

                }

                $muti = 0;
                if ($scheme_height == 637 || $scheme_height == 957) {
                    $muti = 2;
                } elseif ($scheme_height == 1277) {
                    $muti = 3;
                } elseif ($scheme_height == 1917 || $scheme_height == 1597) {
                    $muti = 4;
                }
                if ($muti !== 0) {
                    if (!isset($wujinMap['W05-01-XX-00'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('门铰链');
                        $t_p->setProduct_no('W05-01-XX-00');
                        $t_p->setProduct_unit('套');
                        $t_p->setSeq(113);
                        $t_p->setProduct_count($muti * $d_s->getScheme_count());
                        $t_p->setProduct_price(formatMoney(12));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['W05-01-XX-00'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + $muti * $d_s->getScheme_count());
                    }
                }

            }
        }

        $wujinProducts = array_values($wujinMap);

        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($wujinProducts as $w_p) {
            $w_p = change2ProductBean($w_p);
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
        // Collections.sort(wujin_products, new ProductRptSort_Seq()); // todo:
        $scheme->setScheme_score_wujin_products($wujinProducts);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            $scheme->getScheme_dis_plate_price()
            + $scheme->getScheme_dis_wujin_price()
            + $scheme->getScheme_dis_zj_price()
        ));

        $scheme->setScheme_price(formatMoney($scheme->getScheme_gt_price()));
//        $scheme->setScheme_dis_price($scheme->getScheme_dis_gt_price());
        $scheme->setScheme_dis_price(formatMoney(getMoney($scheme->getScheme_price()*$scheme->getScheme_discount())));

        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));
        return $scheme;
    }

    private function countPlateMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price(formatMoney($price));
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_area())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_area())
        );

        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }

    public function scoreZHGBGDGSchemeProducts(){
        $d_product = new ProductBean();

        $d_product->setSeq_type(1000);
        $d_product->setProduct_name("壁挂吊柜");
        $d_product->setProduct_color_name("");
        $d_product->setProduct_color_no("000");
        $d_product->setProduct_no("BGDG-00-044");
        $d_product->setProduct_count(1);
        $d_product->setProduct_price(formatMoney(1264));
        $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * 0.68));
        $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
        $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
        $totalPrice = $d_product->getProduct_total_price();
        $dis_totalPrice = $d_product->getProduct_dis_total_price();
        $score_products[] = $d_product;

        $scheme['totalPrice'] = $totalPrice;
        $scheme['dis_totalPrice'] = $dis_totalPrice;
        $scheme['score_products'] = $score_products;

        return $scheme;
    }
}