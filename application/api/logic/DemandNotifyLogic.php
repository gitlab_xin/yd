<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/11
 * Time: 17:34
 */

namespace app\api\logic;

use app\common\model\DemandNotify as NotifyModel;

class DemandNotifyLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月11日
     * description:获取我的通知列变频
     * @param $user_id
     * @param $page_index
     * @param $page_size
     * @return array|bool
     */
    public static function getMyNotifications($user_id,$page_index,$page_size)
    {
        try{
            $result = NotifyModel::build()->alias('n')->field(['n.notify_id','n.demand_id','d.title','n.type','n.desc','n.update_time'])
                ->join('demand d','n.demand_id = d.demand_id','LEFT')
                ->where(['n.user_id'=>$user_id])->order(['n.update_time'=>'DESC'])->page($page_index,$page_size)->select()->toArray();
            return $result;
        }catch (\Exception $e){
            return false;
        }
    }
}