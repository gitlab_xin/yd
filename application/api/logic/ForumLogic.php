<?php
/**
 * Created by PhpStorm.
 * User: Jun
 * Date: 2017/7/25
 * Time: 15:53
 */

namespace app\api\logic;

use app\common\model\ForumArticle as ForumArticleModel;
use app\common\model\ForumArticle;
use app\common\model\ForumCollect;
use app\common\model\ForumPraise as ForumLikeModel;
use app\common\model\ForumSearchKey as SearchKeyModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\ShopOrder as OrderModel;
use app\common\model\ShopProduct as ProductModel;
use think\Config;

class ForumLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月1日
     * description:获取帖子列表
     * @param $data
     * @return false|\PDOStatement|string|\think\Collection
     *
     */
    public function getArticleList($data)
    {
        /*创建model对象*/
        $model = ForumArticleModel::build()->alias('art');

        /*声明查询条件*/
        $fields = [
            'art.article_id',
            'art.title',
            'art.classify',
            'art.content',
            'art.user_id',
            'art.is_essence',
            'art.is_top',
            'art.is_official',
            'art.praise_count',
            'art.comment_count',
            'art.collect_count',
            'art.create_time',
            'user.username',
            'user.avatar',
            'user.is_designer',
            'art.praise_count + art.comment_count + art.collect_count as order_count',
        ];
        $where = ['art.is_deleted' => 0];//默认条件拿未删除的
        $whereor = [];
        $order = ['art.is_top' => 'DESC'];//默认拿置顶的
        $join = [[//左连user表获取用户名和头像
            'yd_user user',
            'user.user_id=art.user_id',
            'left'
        ]];

        /*过滤参数*/
        //分类不为全部的情况下,添加进where条件
        if (isset($data['article_type']) && $data['article_type'] != 'all') {
            $where['art.classify'] = $data['article_type'];
        }
        //所属用户
        if (isset($data['user_id']) && $data['user_id'] != '') {
            $where['art.user_id'] = $data['user_id'];
        }
        //如果有keyword
        if (isset($data['keyword']) && $data['keyword'] != '') {
            $where['art.title'] = ['like', "%{$data['keyword']}%"];
//            $whereor['art.content'] = ['exp', "REGEXP '{$data['keyword']}'"];
        }
        //用户id为0的情况下, 说明没有用户登录, $fields和$join不用加入连表判断, >0则要连表, 此处判断避免无用户登录时进行无用的连表
        if ($data['at_user_id'] != 0) {
            $fields = array_merge($fields, ['IF(ISNULL(praise.id),"no","yes") as is_praise', 'IF(ISNULL(collect.id),"no","yes") as is_collect', 'IF(ISNULL(comment.comment_id),"no","yes") as is_comment']
            );
            $join = array_merge($join, [//左连点赞和收藏表
                    ['yd_forum_praise praise', 'praise.article_id=art.article_id and praise.user_id =' . $data['at_user_id'], 'left'],
                    ['yd_forum_collect collect', 'collect.article_id=art.article_id and collect.user_id =' . $data['at_user_id'], isset($data['collect']) && $data['collect'] == 1 ? 'right' : 'left'],
                    ['(SELECT article_id,comment_id from yd_forum_comment where user_id = ' . $data['at_user_id'] . ' and parent_comment_id = 0 GROUP BY article_id) comment', 'comment.article_id = art.article_id', 'left']
                ]
            );
        }

        //添加排序条件,热门则计算总和
        if ($data['sort'] == 'hot') {
            $order['order_count'] = 'DESC';
        } else {
            $order['art.create_time'] = 'DESC';
        }

        /*用model链式操作拼接sql*/
        foreach ($join as $row) {
            $model = $model->join($row[0], $row[1], $row[2]);
        }
        $result = $model->field($fields)->where($where)->whereOr($whereor)->order($order)->page($data['pageIndex'], $data['pageSize'])->select()->toArray();
        /*进行数据处理*/
        if ($result) {
//            $pattern = '/(href|src)=([\"|\']?)([^\"\'>]+.(jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG))/i';
            //改用断言正则
            $pattern = '/(?<=src=\").*(?=\")/Ui';
            $classifyMapping = Config::get('bbs.classify');
            foreach ($result as &$row) {
                //匹配富文本内容并获取前三个img里的src
                $match_count = preg_match_all($pattern, $row['content'], $match);

                $row['preview'] = $match_count == 0 ? [] : array_slice($match[0], 0, 3);

                //清空富文本html标签,并获取60个字符串作为简介
                $content = strip_tags($row['content']);
                $row['content'] = strlen($content) > 60 ? mb_substr($content, 0, 60) : $content;
                $row['classify'] = $classifyMapping[$row['classify']];

                //由于用户没登录时没有进行连表,所以在处理数据时加上字段是否点赞和是否收藏的字段返回
                if ($data['at_user_id'] == 0) {
                    $row['is_praise'] = $row['is_collect'] = $row['is_comment'] = 'no';
                }
            }
        }
        return $result;
    }

    /**
     * 帖子发布
     * @author: Jun
     * @time: 2017年7月26日
     * description: 用户发布帖子
     * @param $data
     * @return int|string
     */
    public function addArticle($data)
    {
        try {
            if ($data['scheme_id'] > 0 && SchemeModel::get($data['scheme_id']) == null) {
                return -2;
            } elseif ($data['product_id'] > 0 && OrderModel::build()->where(['product_id' => $data['product_id']])->count() == 0) {
                return -3;
            }

            // 改变数据下标对应数据库 & 加入创建时间等数据
            $add_data['user_id'] = $data['user_id'];
            $add_data['title'] = $data['article_title'];
            $add_data['classify'] = $data['article_classify'];
            $add_data['content'] = $data['article_content'];
            $add_data['scheme_id'] = $data['scheme_id'];
            $add_data['product_id'] = $data['product_id'];

            // model插入
            return ForumArticleModel::create($add_data) ? 1 : 0;
        } catch (\Exception $e) {
            return -1;
        }

    }

    /**
     * 获取帖子详情
     * @author: Jun
     * @time: 2017年7月26日
     * description: 获取帖子详情
     * @param array $requestData
     * @return mixed
     */
    public function getArticleDetail($requestData)
    {
        $detail = ForumArticleModel::getArticleDetail($requestData['article_id'], $requestData['at_user_id']);
        if ($detail != null) {
            $detail = $detail->toArray();
            if ($detail['scheme_id'] > 0 &&
                ($scheme_src = SchemeModel::build()->where(['id' => $detail['scheme_id']])->value('scheme_pic')) != null
            ) {
                $detail['scheme']['scheme_src'] = $scheme_src;
                $detail['scheme']['desc'] = '来看看我做的方案吧！';
                $detail['product'] = null;
            } elseif ($detail['product_id'] > 0 &&
                ($product = ProductModel::build()->alias('p')->field(['p.img_src_list', 'p.name', 'min(ps.price) as price'])
                    ->join('shop_product_standard ps', 'p.product_id = ps.product_id', 'LEFT')
                    ->where(['p.product_id' => $detail['product_id'], 'ps.is_deleted' => '0', 'p.is_deleted' => '0'])
                    ->find()) != null
            ) {
                $temp = explode('|', $product['img_src_list']);

                $detail['product']['name'] = $product['name'];
                $detail['product']['price'] = $product['price'];
                $detail['product']['img_src'] = empty($temp) ? '' : $temp[0];
                $detail['product']['desc'] = '来看我买过的商品吧！';
                $detail['scheme'] = null;

            } else {
                $detail['scheme'] = $detail['product'] = null;
            }
        }

        return $detail;
    }

    /**
     * 点赞 and 取消点赞
     * @author: Jun
     * @time: 2017年7月26日
     * description: 点赞和取消点赞，自动识别是否已经点赞
     * @param int $user_id 用户ID
     * @param int $article_id 帖子ID
     * @return int|string
     */
    public function articlePraise($user_id, $data)
    {
        // 验证是否已经点赞
        $isExists = ForumLikeModel::isExists($user_id, $data['article_id']);

        $result = false;
        switch ($data['praise_type']) {
            case 'praise':
                if (!$isExists) {//如果不存在的话才进行点赞
                    $result = ForumLikeModel::addPraise(['user_id' => $user_id, 'article_id' => $data['article_id']]);
                }
                break;
            case 'cancel':
                if ($isExists) {//如果存在的话才进行取消点赞
                    $result = ForumLikeModel::delPraise($user_id, $data['article_id']);
                }
                break;
            default:
                break;
        }
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2017年7月27日
     * description:检查文章是否存在
     * @param array $where
     * @return bool
     */
    public static function check($where)
    {
        return ForumArticleModel::build()->check($where);
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:收藏文章
     * @param $user_id
     * @param $article_id
     * @return bool|int|string
     */
    public static function collect($user_id, $article_id)
    {
        return ForumCollect::collect($user_id, $article_id);
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:收藏文章
     * @param $user_id
     * @param $article_id
     * @return bool|int|string
     */
    public static function cancel($user_id, $article_id)
    {
        return ForumCollect::cancel($user_id, $article_id);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取帖子总数
     * @param $data
     * @return int|string
     */
    public function getArticleCount($data)
    {
        $where = ['is_deleted' => 0];//默认条件拿未删除的
        $whereor = [];
        if (isset($data['article_type']) && $data['article_type'] != 'all') {
            $where['classify'] = $data['article_type'];
        }
        //如果有keyword
        if (isset($data['keyword']) && $data['keyword'] != '') {
            $where['title'] = ['like', "%{$data['keyword']}%"];
//            $whereor['content'] = ['exp', "REGEXP '{$data['keyword']}'"];
        }
        //所属用户
        if (isset($data['user_id']) && $data['user_id'] != '') {
            $where['user_id'] = $data['user_id'];
        }
        return ForumArticleModel::build()->where($where)->whereOr($whereor)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:删除帖子
     * @param $data
     * @return $this
     *
     */
    public function delArticle($data)
    {
        //由于直接用update

        if ($model = ForumArticle::build()->where($data)->find()) {
            $model->is_deleted = 1;
            if ($model->save()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:返回我收藏的帖子数量
     * @param $user_id
     * @return int|string
     *
     */
    public function getArticleCollectionCount($user_id)
    {
        return ForumCollect::build()->alias('c')->where(['c.user_id' => $user_id])->join('forum_article fa', 'fa.article_id = c.article_id AND fa.is_deleted = 0', 'right')->count();
//        return ForumCollect::whereCount(['user_id'=>$user_id]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:记录关键字
     * @param $keyword
     * @return bool|int|string|true
     *
     */
    public function markKeyword($keyword)
    {
        if ($keyword == "" || is_null($keyword)) {
            return true;
        }
        $data['key'] = $keyword;
        $check = SearchKeyModel::build()->where($data)->count();
        if (empty($check)) {
            $data['create_time'] = time();
            $bool = SearchKeyModel::build()->insert($data);
        } else {
            $bool = SearchKeyModel::build()->where($data)->setInc('count');
        }
        return $bool;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:获取热门搜索
     * @return array
     */
    public function getHotSearch()
    {
        return SearchKeyModel::build()
            ->field(['key'])
            ->order(['count' => 'DESC'])
            ->limit(10)
            ->select()->toArray();
    }

}