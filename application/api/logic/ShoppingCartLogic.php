<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 11:49
 */

namespace app\api\logic;

use app\common\model\ShoppingCart as ShoppingCartModel;
use app\common\model\ShoppingCart;

class ShoppingCartLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:添加商品到购物车
     * @param int $user_id 用户ID
     * @param int $product_id 商品ID
     * @param int $standard_id 规格ID
     * @param int $count 数量
     * @return int|string|true
     */
    public static function add($user_id, $product_id, $standard_id, $count = 1)
    {
        return ShoppingCartModel::add($user_id, $product_id, $standard_id, $count);
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:修改购物车数量
     * @param $cart_id
     * @param $user_id
     * @param int $count
     * @return int|string|true|bool
     */
    public static function setCount($cart_id, $user_id, $count = 1)
    {
        return ShoppingCartModel::setCount($cart_id, $user_id, $count);
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:获取购物车列表
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getList($user_id)
    {
        $ShoppingModel = new ShoppingCartModel();
        $field = [
            'supplier.supplier_id',
            'supplier.name as supplier_name'
        ];
        $supplier_list = $ShoppingModel->alias('cart')
            ->join(
                'yd_shop_product product',
                "product.product_id = cart.product_id")
            ->join('yd_shop_supplier supplier', "supplier.supplier_id = product.supplier_id")
            ->where(['cart.user_id' => $user_id])
            ->field($field)
            ->order('cart.create_time')
            ->group('supplier.supplier_id')
            ->select()->toArray();
        if (empty($supplier_list)) {
            return $supplier_list;
        }
        $field = [
            'cart.cart_id',
            'cart.count',
            'product.product_id',
            'product.name as product_name',
            'product.img_src_list',
            'stan.name as standard_name',
            'stan.price',
            'stan.stocks',
            'product.freight_charge',
            'product.processing_charge',
            'product.home_charge',
            'product.service_charge',
            'IF(ISNULL(collect.id),"no","yes") as is_collect',
            'product.is_sale','product.is_deleted','stan.is_deleted as stan_is_deleted'
        ];
        $collect_condition = "collect.product_id = cart.product_id AND collect.user_id = {$user_id}";
        foreach ($supplier_list as $key => $value) {
            $list = $ShoppingModel->alias('cart')
                ->join(
                    'yd_shop_product_standard stan',
                    "stan.standard_id = cart.standard_id AND stan.product_id = cart.product_id")
                ->join(
                    'yd_shop_product product',
                    "product.product_id = cart.product_id")
                ->join('yd_shop_supplier supplier', "supplier.supplier_id = product.supplier_id")
                ->join('yd_shop_collect collect', $collect_condition, 'LEFT')
                ->where(['cart.user_id' => $user_id, 'supplier.supplier_id' => $value['supplier_id']])
                ->field($field)
                ->order('cart.create_time')
                ->select()->toArray();
            if ($list) {
                foreach ($list as $key1 => $value1) {
                    $list[$key1]['img_src'] = empty($value1['img_src_list']) ? "" : explode('|',
                        $value1['img_src_list'])[0];
                    $list[$key1]['is_valid'] = ($value1['stan_is_deleted'] == '1'|| $value1['is_deleted']== '1' || $value1['is_sale'] == '0')?0:1;//是否有效
                    unset($list[$key1]['img_src_list'],$value1['stan_is_deleted'],$value1['is_deleted'],$value1['is_sale']);
                }
            }
            $supplier_list[$key]['cart_list'] = $list;
        }


        return $supplier_list;
    }

    /**
     * @author: Airon
     * @time: 2017年7月28日
     * description:获取购物车数量
     * @param $user_id
     * @return int|string
     */
    public static function getCount($user_id)
    {
        $ShoppingModel = new ShoppingCartModel();
        $count = $ShoppingModel
            ->where(['user_id' => $user_id])
            ->count();
        return $count;
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:删除购物车信息
     * @param $cart_id_list
     * @param $user_id
     * @return int
     */
    public static function delete($cart_id_list, $user_id)
    {
        $where['cart_id'] = ['in', $cart_id_list];
        $where['user_id'] = $user_id;
        $ShoppingCartModel = new ShoppingCartModel();

        return $ShoppingCartModel->where($where)->delete();
    }

    /**
     * @author: Airon
     * @time: 2017年8月4日
     * description:购物车信息
     * @param $cart_id
     * @param $user_id
     * @return int
     */
    public static function getInfo($cart_id, $user_id)
    {
        $where['cart_id'] = $cart_id;
        $where['user_id'] = $user_id;
        $ShoppingCartModel = new ShoppingCartModel();

        return $ShoppingCartModel->where($where)->find();
    }
}