<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/8
 * Time: 17:31
 */

namespace app\api\logic;

use app\common\model\ShoppingCart as CartModel;
use app\common\model\ShopProduct as ProductModel;
use app\common\model\ShopOrder as OrderModel;

class ShopPayLogic
{
    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取商品详情
     * @param $product_id
     * @param $standard_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function productInfo($product_id, $standard_id)
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.freight_charge',
            'p.processing_charge',
            'p.home_charge',
            'p.service_charge',
            'ps.stocks'
        ];
        $info = ProductModel::build()
            ->alias('p')
            ->join("yd_shop_product_standard ps",
                "p.product_id = ps.product_id AND ps.standard_id = {$standard_id}")
            ->where(['p.product_id' => $product_id, 'p.is_deleted' => '0', 'p.is_sale' => '1'])
            ->field($field)
            ->find();
        if (!empty($info)) {
            $info['img_src'] = empty($info['img_src_list']) ? "" : explode('|', $info['img_src_list'])[0];
            unset($info['img_src_list']);
        }
        return $info;
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:购物车 获取商品详情
     * @param $cart_id_list
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function productCartInfo($cart_id_list)
    {
        $field = [
            'cart.cart_id',
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.freight_charge',
            'p.processing_charge',
            'p.home_charge',
            'p.service_charge',
            'ps.stocks',
            'cart.count as pay_count',
            'ps.name as standard_name'
        ];
        $where = [
            'cart.cart_id' => ['in', $cart_id_list],
            'cart.form' => '1',
            'p.is_deleted' => '0',
            'p.is_sale' => '1',
            'ps.is_deleted ' => '0'
        ];
        $info = CartModel::build()->alias('cart')->join('yd_shop_product p', 'p.product_id = cart.product_id')
            ->join('yd_shop_product_standard ps', 'cart.standard_id = ps.standard_id')
            ->where($where)
            ->field($field)
            ->select();
        if (!empty($info)) {
            $info = self::get_img_src($info);
        }
        return $info;
    }

    /**
     * @author: Airon
     * @time: 2017年8月10日
     * description:获取订单详情
     * @param $bill_no
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getOrderInfo($bill_no)
    {
        return OrderModel::build()
            ->where(['bill_no' => $bill_no,'status'=>'未支付'])
            ->field('order_num,price,status,user_id')
            ->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年8月10日
     * description:支付回调修改订单状态
     * @param $bill_no
     * @param $updateData
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function updateOrderStatus($bill_no, $updateData)
    {
        return OrderModel::build()->update($updateData, ['bill_no' => $bill_no]);
    }

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:处理图片,将img_src_list里的多张图片 处理成单张图片的 img_src
     * @param $array
     * @return mixed
     */
    protected static function get_img_src($array)
    {
        if ($array) {
            foreach ($array as $key => $value) {
                $array[$key]['img_src'] = empty($value['img_src_list']) ? "" : explode('|', $value['img_src_list'])[0];
                unset($array[$key]['img_src_list']);
            }
        }
        return $array;
    }
}