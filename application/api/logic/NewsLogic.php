<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/29
 * Time: 11:28
 */

namespace app\api\logic;

use app\common\model\News as NewsModel;

class NewsLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:获取资讯列表
     * @param $pageIndex
     * @param $pageSize
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getList($pageIndex, $pageSize)
    {
        return NewsModel::build()
            ->field([
                'id as news_id',
                'title',
                'image',
                'synopsis',
                'look_count',
                'create_time'
            ])
            ->page($pageIndex, $pageSize)
            ->order(['id' => 'DESC'])
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取资讯数量
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getCount()
    {
        return NewsModel::whereCount();
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:获取资讯详情
     * @param int $news_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getInfo($news_id)
    {
        $result = NewsModel::build()
            ->where(['id' => $news_id])
            ->find();
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2017年8月1日
     * description:添加阅读量
     * @param int $news_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function incLookCount($news_id)
    {
        NewsModel::build()
            ->where(['id' => $news_id])
            ->setInc('look_count');
    }
}