<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 16:27
 */

namespace app\api\logic;

use think\Db;
use app\common\model\Works as WorksModel;
use app\common\model\WorksComment as CommentModel;

class WorksCommentLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月2日
     * description:获取回复列表
     * @param $data
     * @return false|mixed|\PDOStatement|string|\think\Collection
     */
    public function getComments($data)
    {
        $result = CommentModel::build()
            ->alias('c')
            ->field(['c.comment_id','c.user_id','c.content','c.create_time','u.username','u.avatar'])
            ->join('yd_user u','c.user_id = u.user_id')
            ->where(['c.works_id'=>$data['works_id'],'c.parent_comment_id'=>'0'])
            ->order(['c.create_time'=>'ASC'])
            ->page($data['pageIndex'],$data['pageSize'])
            ->select();

        //如果有回复
        if($result){
            $result = objToArray($result);
            $comment_ids = array_column($result,'comment_id');
            //获取前4条二级回复
            $second_comments = CommentModel::build()
                ->alias('t')
                ->field(['t.comment_id','t.user_id','t.by_user_id','t.parent_comment_id','t.content','t.create_time','t.by_comment_id',
                    'GROUP_CONCAT(`user`.`username`) as usernames','GROUP_CONCAT(`user`.`user_id`) as user_ids',
                    'GROUP_CONCAT(`user`.`avatar`) as avatars'])
                ->join('yd_user user','`user`.`user_id` = `t`.`user_id` OR `user`.`user_id` = `t`.`by_user_id`','left')
                ->where(['t.parent_comment_id'=>['in',$comment_ids],
                't.works_id' => $data['works_id']
//                    ,todo:: 迁移项目升级数据库后导致问题，迫于公司端午节活动 结束再修
//                '' => ['exp','4 > (SELECT COUNT(*) FROM yd_forum_comment WHERE parent_comment_id=t.parent_comment_id AND comment_id<t.comment_id)']
                ])
                ->group('t.comment_id')
                ->order(['t.create_time'=>'ASC'])
                ->select();

            //该数组key为一级评论id,value为二级评论列表
            $second_comments = $second_comments ? objToArray($second_comments):[];
            $belong_comment_id = $second_comments ? array_reduce($second_comments,function($v,$w){
                //转化回复人和被回复人
                $username = explode(',',$w['usernames']);
                $user_ids = explode(',',$w['user_ids']);
                $avatars = explode(',',$w['avatars']);

                $tem = array_combine($user_ids,$username);
                $avaTem = array_combine($user_ids,$avatars);

                $w['avatar'] = $avaTem[$w['user_id']];
                $w['username'] = $tem[$w['user_id']];
                $w['by_username'] = $w['parent_comment_id'] == $w['by_comment_id']?'':($w['by_user_id'] == 0?'':$tem[$w['by_user_id']]);
                $parent_id = $w['parent_comment_id'];
                unset($w['usernames'],$w['user_ids'],$w['by_user_id'],$w['parent_comment_id'],$w['by_comment_id'],$w['avatars']);
                $v[$parent_id][] = $w;
                return $v;
            }):[];

            //遍历一级评论数组并且添加second_comments的key值和对应的value
            array_walk($result,function(&$value) use($belong_comment_id){
                $value['second_comments'] = key_exists($value['comment_id'],$belong_comment_id)?$belong_comment_id[$value['comment_id']]:[];
            });

        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月2日
     * description:回复
     * @param $data
     * @return $this
     *
     */
    public function addComment($data)
    {
        $res = false;
        Db::startTrans();
        try{
            WorksModel::build()->where(['works_id'=>$data['works_id']])->setInc('comment_count');
            CommentModel::create($data,true);
            Db::commit();
            $res = true;
        }catch (\Exception $e){
            Db::rollback();
        }
        return $res;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月2日
     * description:获取单条回复
     * @param $data
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getCommentDetail($data)
    {
        $result = CommentModel::build()
            ->alias('t')
            ->field(['t.*','GROUP_CONCAT(`user`.`username`) as usernames','GROUP_CONCAT(`user`.`user_id`) as user_ids','GROUP_CONCAT(`user`.`avatar`) as avatars'])
            ->join('yd_user user','`user`.`user_id` = `t`.`user_id` OR `user`.`user_id` = `t`.`by_user_id`','left')
            ->where(['t.comment_id'=>$data['comment_id']])
            ->whereOr(['t.parent_comment_id'=>$data['comment_id']])
            ->group('t.comment_id')
            ->order(['t.parent_comment_id'=>'ASC','t.create_time'=>'ASC'])
            ->select();
        if($result){
            $comments = objToArray($result);
            $result = $comments[0];
            $username_map = explode(',',$result['usernames']);
            $user_ids_map = explode(',',$result['user_ids']);
            $avatars_map = explode(',',$result['avatars']);
            unset($result['by_user_id'],$result['parent_comment_id'],$result['by_comment_id'],$result['usernames'],$result['user_ids'],$result['avatars'],$comments[0]);
            $index = array_search($result['user_id'],$user_ids_map);
            $result['username'] = $username_map[$index];
            $result['avatar'] = $avatars_map[$index];
            $result['second_comments'] = [];
            if(count($comments)>0){
                sort($comments);
                array_walk($comments,function(&$value){
                    $username = explode(',',$value['usernames']);
                    $user_ids = explode(',',$value['user_ids']);
                    $avatars = explode(',',$value['avatars']);

                    $avaTem = array_combine($user_ids,$avatars);
                    $tem = array_combine($user_ids,$username);

                    $value['avatar'] = $avaTem[$value['user_id']];
                    $value['username'] = $tem[$value['user_id']];
                    $value['by_username'] = $value['parent_comment_id'] == $value['by_comment_id']?'':($value['by_user_id'] == 0?'':$tem[$value['by_user_id']]);
                    unset($value['usernames'],$value['user_ids'],$value['by_user_id'],$value['parent_comment_id'],$value['by_comment_id'],$value['article_id'],$value['avatars']);
                });
                $result['second_comments'] = $comments;
            }
        }
        return $result;
    }
}