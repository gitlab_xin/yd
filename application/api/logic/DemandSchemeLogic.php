<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/7
 * Time: 10:15
 */

namespace app\api\logic;

use think\Db;
use think\Config;
use app\common\model\User as UserModel;
use app\common\model\UserMoneyRecord as MoneyRecordModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\Demand as DemandModel;
use app\common\model\DemandScheme as DemandSchemeModel;
use app\common\model\DemandSchemeDetail as DemandSchemeDetailModel;

class DemandSchemeLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:设计师获取投稿方案列表
     * @param $user_id
     * @param $demand_id
     * @return array|bool
     */
    public static function getSchemesByGuest($user_id, $demand_id)
    {
        try {
            return DemandSchemeModel::build()->alias('ds')
                ->field(['ds.id as scheme_id', 'ds.demand_id', 'ds.is_accepted', 'ds.create_time', 'ds.scheme_pic', 'ds.classify',
                    'u.username', 'u.avatar', 'u.is_designer', 'ds.title'])
                ->join('user u', 'u.user_id = ds.user_id', 'LEFT')
                ->where(['ds.user_id' => $user_id, 'ds.demand_id' => $demand_id])
                ->order(['ds.id' => 'DESC'])->select()->toArray();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:获取方案列表
     * @param $user_id
     * @param $demand_id
     * @return array|bool|int
     */
    public static function getSchemesByMaster($user_id, $demand_id)
    {
        try {
            if (($demand = DemandModel::build()->where(['user_id' => $user_id, 'demand_id' => $demand_id])->find()) == null) {
                return -2;
            }
            if ($demand->is_deleted == 1) {
                return -3;
            }

            $result = DemandSchemeModel::build()->alias('ds')
                ->field(['ds.id as scheme_id', 'ds.demand_id', 'ds.is_accepted', 'ds.create_time', 'ds.scheme_pic', 'ds.classify', 'ds.is_praise', 'ds.message',
                    'u.username', 'u.avatar', 'u.is_designer', 'ds.title','ds.origin_scheme_id'])
                ->join('user u', 'u.user_id = ds.user_id', 'LEFT')
                ->where(['ds.demand_id' => $demand_id])
                ->order(['ds.id' => 'DESC'])->select()->toArray();

            foreach ($result as &$v) {
                $v['has_commented'] = ($v['is_praise'] == 0 && $v['message'] == '') ? 0 : 1;
                unset($v['is_praise'], $v['message']);
            }

            return $result;
        } catch (\Exception $e) {
            return -1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:上传稿件
     * @param $data
     * @return bool|int
     */
    public static function addScheme($data)
    {
        try {

            $demand_ower_id = DemandModel::build()->where(['demand_id' => $data['demand_id'], 'is_deleted' => 0])->value('user_id');
            //方案不存在
            if ($demand_ower_id == null) {
                return -1;
            }
            //不要自己给自己投稿
            if ($data['user_id'] == $demand_ower_id) {
                return -2;
            }
            $scheme = SchemeModel::build()->where(['id' => $data['origin_scheme_id']])->find();
            //不存在该方案
            if ($scheme == null) {
                return -3;
            }
            //该方案不属于你
            if ($scheme->user_id != $data['user_id']) {
                return -4;
            }
            $limit = Config::get('demand.contribute_limit');
            //超过投稿限制
            if (($count = DemandSchemeModel::build()->where(['user_id' => $data['user_id'], 'demand_id' => $data['demand_id']])->count()) >= $limit) {
                return -5;
            }
            /*
             * 1.开始事务
             * 2.copy方案
             * 3.获取copy表primary key 后,存入需求了方案表
             * 4.结束事务
             */
            $scheme = $scheme->toArray();
            unset($scheme['id']);
            Db::startTrans();
            //1
            if ((!$model = DemandSchemeDetailModel::create($scheme, true))) {
                Db::rollback();
                return -6;
            }
            //2
            $data['scheme_detail_id'] = $model->getLastInsID();
            $data['classify'] = $scheme['scheme_type_text'];
            $data['scheme_pic'] = $scheme['scheme_pic'];
            if (!DemandSchemeModel::create($data, true)) {
                Db::rollback();
                return -6;
            }
            Db::commit();
            return ++$count >= $limit ? 0 : 1;
        } catch (\Exception $e) {
            return -7;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:给方案点赞(评论)
     * @param $scheme_id
     * @param $user_id
     * @param string $message
     * @return int
     */
    public static function changeScheme($scheme_id, $user_id, $message = '')
    {
        try {
            $scheme = DemandSchemeModel::get($scheme_id);
            //方案不存在
            if ($scheme == null) {
                return -1;
            }
            $demand = DemandModel::get($scheme->demand_id);
            //需求不存在或未结束
            if ($demand == null || $demand->is_deleted == 1 || $demand->status != 3) {
                return -2;
            }
            if ($demand->user_id != $user_id) {
                return -3;
            }
            //已经赞过(评论)啦
            if ($message == '') {
                if ($scheme->is_praise == 1) {
                    return -4;
                }
                $scheme->is_praise = 1;
                if (!$scheme->isUpdate()->save()) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                if ($scheme->message != '') {
                    return -4;
                }
                $scheme->message = $message;
                if (!$scheme->isUpdate()->save()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:获取评论和赞
     * @param $scheme_id
     * @return array|int
     */
    public static function getComment($scheme_id)
    {
        try {
            $scheme = DemandSchemeModel::build()->field(['is_praise', 'message'])->where(['id' => $scheme_id])->find();
            if ($scheme == null) {
                return -1;
            } else {
                return $scheme->toArray();
            }
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:确认稿件
     * @param $scheme_id
     * @param $user_id
     * @return int
     */
    public static function confirmScheme($scheme_id, $user_id)
    {
        try {
            //方案不存在
            if (($scheme = DemandSchemeModel::get($scheme_id)) == null) {
                return -2;
            }
            $demand = DemandModel::get($scheme->demand_id);
            //需求不存在
            if ($demand == null || $demand->is_deleted == 1) {
                return -3;
            }
            //你不是需求发布者
            if ($demand->user_id != $user_id) {
                return -4;
            }
            //必须是发布中的需求才能被采纳
            if ($demand->status !== 1) {
                return -5;
            }
            /*
             * 开启事务前先获取好向相应的model和数据封装,减少事务执行时间
             * 1.开启事务
             * 2.方案状态修改 is_accepted = 1
             * 3.需求状态修改 status = 3
             * 4.用户增加赏金 money = money + reward
             * 5.用户账户流水记录表记录金额变化
             * 6.关闭事务
             */
            if (($user = UserModel::get($scheme->user_id)) == null) {
                return -6;
            }
            $record = ['user_id' => $scheme->user_id, 'before_change' => $user->money, 'after_change' => ($user->money = $user->money + $demand->reward),
                'change_var' => $demand->reward, 'type' => 'money_reward', 'change_type' => 1];
            $demand->status = 3;
            $scheme->is_accepted = 1;
            Db::startTrans();
            if (!$demand->isUpdate()->save()) {
                return -7;
            }
            if (!$scheme->isUpdate()->save()) {
                return -8;
            }
            if (!$user->isUpdate()->save()) {
                return -9;
            }
            if (!MoneyRecordModel::create($record, true)) {
                return -10;
            }
            Db::commit();
            return 1;
        } catch (\Exception $e) {
            return -1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:查看确认稿件
     * @param $demand_id
     * @param $user_id
     * @return array|int
     */
    public static function getAcceptedScheme($demand_id, $user_id)
    {
        try {
            $demand = DemandModel::get($demand_id);
            //需求不存在或被删除
            if ($demand == null || $demand->is_deleted == 1) {
                return -2;
            }
            //不是需求发布者
            if ($demand->user_id != $user_id) {
                return -3;
            }
            //需求状态不符合
            if ($demand->status != 3) {
                return -4;
            }

            $scheme = DemandSchemeModel::build()->alias('ds')->field(['u.username', 'u.avatar', 'ds.id as scheme_id', 'ds.classify', 'ds.scheme_detail_id', 'ds.title', 'ds.desc', 'ds.create_time'])
                ->join('user u', 'ds.user_id = u.user_id', 'LEFT')
                ->where(['ds.demand_id' => $demand_id, 'ds.is_accepted' => 1])->find();
            //方案不存在
            if ($scheme == null) {
                return -5;
            }

            return static::mergeScheme($scheme);
        } catch (\Exception $e) {
            return -1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:获取稿件详情
     * @param $scheme_id
     * @param $user_id
     * @return array|int
     */
    public static function getScheme($scheme_id, $user_id)
    {
        try {
            $scheme = DemandSchemeModel::build()->alias('ds')
                ->field(['ds.user_id', 'u.username', 'u.avatar', 'ds.classify', 'ds.id as scheme_id', 'ds.scheme_detail_id', 'ds.title', 'ds.is_accepted',
                    'ds.is_praise', 'ds.message', 'ds.desc', 'ds.create_time'])
                ->join('demand d', 'd.demand_id = ds.demand_id', 'LEFT')
                ->join('user u', 'ds.user_id = u.user_id', 'LEFT')
                ->where(['ds.id' => $scheme_id])
                ->where(function ($query) use ($user_id) {
                    $query->where(['d.user_id' => $user_id])->whereOr(['ds.user_id' => $user_id]);
                })
                ->find();

            if ($scheme == null) {
                return -2;
            }

            $scheme['has_commented'] = ($scheme->is_praise == 0 && $scheme->message == '') ? 0 : 1;
            unset($scheme['is_praise'], $scheme['message']);

            return static::mergeScheme($scheme);
        } catch (\Exception $e) {
            return -1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:合并稿件
     * @param $scheme
     * @return array
     */
    private static function mergeScheme($scheme)
    {
        //获取方案详情
        $detail = DemandSchemeDetailModel::build()->field(['scheme_detail_id', 'user_id', 'scheme_name', 'serial_array', 'create_time', 'update_time'],true)
            ->where(['scheme_detail_id' => $scheme->scheme_detail_id])->find();

        $result = array_merge($scheme->toArray(), $detail->toArray());

        return $result;
    }
}