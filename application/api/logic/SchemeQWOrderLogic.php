<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\CustomizedScheme;
use app\common\model\ProductBean;
use app\common\model\Scheme as SchemeModel;
use app\common\model\Product as ProductModel;
use app\common\model\SchemeBean;
use app\common\tools\ImageUtils;

class SchemeQWOrderLogic
{
    public $productLogic = null;

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    public function scoreQWSchemeZJProducts(SchemeBean $scheme)
    {

        $score_products = [];
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    $c_p->setProduct_is_score(0);
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (empty($c_p->getProduct_is_score())) {
                        $productNo = $c_p->getProduct_no();
                        if (is_starts_with($productNo, 'L-')
                            || is_starts_with($productNo, 'LB-')
                            || is_starts_with($productNo, 'K-')
                        ) {
                            $c_p->setProduct_color_no("");//组件没有颜色
                            $c_p->setProduct_count(1);
                            foreach ($scheme_products as $t_p) {
                                $t_p = change2ProductBean($t_p);
                                if (!empty($t_p->getCom_products())) {
                                    foreach ($t_p->getCom_products() as $t_c_p) {
                                        $t_c_p = change2ProductBean($t_c_p);
                                        if ($c_p != $t_c_p && empty($t_c_p->getProduct_is_score())) { // todo: 直接比较有可能出问题
                                            if ($c_p->getProduct_no() == $t_c_p->getProduct_no()) {
                                                $c_p->setProduct_count($c_p->getProduct_count() + 1);
                                                $t_c_p->setProduct_is_score(1);
                                            }
                                        }
                                    }
                                }
                            }
                            $c_p->setProduct_is_score(1);
//                            $score_products[] = $c_p;

                            if (isset($score_products[$productNo])) {
                                $s_p_1 = change2ProductBean($score_products[$productNo]);
                                $s_p_1->setProduct_count($c_p->getProduct_count() + $s_p_1->getProduct_count());
                            }else {
                                $score_products[$productNo] = $c_p;
                            }
                        }

                        //处理组合内的拉篮和裤抽 Near
                        if ($c_p->getProduct_type() == 'ZH') {
                            if (empty($c_p->getCom_products())) {
                                continue;
                            }
                            foreach ($c_p->getCom_products() as $c_p_c) {
                                $productNo = $c_p_c->getProduct_no();
                                if (is_starts_with($productNo, 'L-') || is_starts_with($productNo, 'LB-') || is_starts_with($productNo, 'K-')) {
                                    if ($c_p_c->getProduct_is_score() == 1) {
                                        continue;
                                    }
                                    $c_p_c->setProduct_color_no("");//组件没有颜色
                                    $c_p_c->setProduct_count(1);
                                    $c_p_c->setProduct_is_score(1);
                                    foreach ($scheme_products as $t_p) {
                                        $t_p = change2ProductBean($t_p);
                                        if (empty($t_p->getCom_products())){
                                            continue;
                                        }
                                        foreach ($t_p->getCom_products() as $t_c_p) {
                                            foreach ($t_c_p->getCom_products() as $t_c_p_c) {
                                                $t_c_p_c = change2ProductBean($t_c_p_c);
                                                if ($t_c_p_c->getProduct_is_score() == 1) {
                                                    continue;
                                                }
                                                if ($c_p_c->getProduct_no() != $t_c_p_c->getProduct_no()) {
                                                    continue;
                                                }
                                                $c_p_c->setProduct_count($c_p_c->getProduct_count() + 1);
                                                $t_c_p_c->setProduct_is_score(1);
                                            }
                                        }
                                    }
//                                    $score_products[] = $c_p_c;
                                    if (isset($score_products[$productNo])) {
                                        $s_p = change2ProductBean($score_products[$productNo]);
                                        $s_p->setProduct_count($c_p_c->getProduct_count() + $s_p->getProduct_count());
                                    }else {
                                        $score_products[$productNo] = $c_p_c;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

//        Collections.sort(score_products, new ProductRptSort_Seq()); // todo: 不排序可能有问题

        $width = 0;
        $totalPrice = 0;
        $dis_totalPrice = 0;
        $discount = $scheme->getScheme_discount();
        foreach ($score_products as $p) {
            $p = change2ProductBean($p);
            if (is_starts_with($p->getProduct_no(), "L")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(410, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(450, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(530, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(610, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(690, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            } elseif (is_starts_with($p->getProduct_no(), "K-")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(350, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(385, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(420, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(455, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(525, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            }
        }

        $scheme->setScheme_score_zj_products($score_products);
        $scheme->setScheme_zj_price($totalPrice);
        $scheme->setScheme_dis_zj_price($dis_totalPrice);

        return $scheme;
    }

    public function scoreQWSchemeProducts(SchemeBean $scheme)
    {
        $scheme->setScheme_price(0);
        $scheme->setScheme_dis_price(0);
        $score_products = [];

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            $p->setProduct_is_score(0);
        }
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_wcb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        foreach ($scheme->getScheme_ncb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_ncb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        $scheme_products = [];
        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            foreach ($s->getScheme_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $no = "";
        for ($i = 0; $i < count($scheme_products); $i++) {
            $p = $scheme_products[$i];
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                if(str_has($p->getProduct_type(),"ZH-YTG")){
                    if($this->isDDB($scheme,$p)){
                        $no = "YD-" . $p->getProduct_width() . "-01-00-000";
                        $p->setProduct_no($no);
                        $p->setProduct_name('墙外移门衣柜顶底横隔板');
                        $p->setProduct_color_no('000');
                        $p->setProduct_color_name('灰布纹');
                    } else {
                        $no = $p->getProduct_no();
                        $no = str_replace("ytg", "", $no);
                        $p->setProduct_no($no);
                        $p->setProduct_name("自定义衣柜横隔板");
                    }
                }elseif (str_has($p->getProduct_type(),"ZH")){
                    if($this->isDDB($scheme,$p)){
                        $no = $p->getProduct_no();
                        $p->setProduct_no($no);
                        $p->setProduct_name("顶底横隔板");
                    }
                }

                if ($p->getProduct_type() == 'ZZ') {
                    //处理组合内的横隔板
                    foreach ($p->getCom_products() as $c_p) {
                        if ($c_p->getProduct_is_score() == '1') {
                            continue;
                        }
                        if(str_has($c_p->getProduct_type(),"ZH-YTG")){
                            $no = $c_p->getProduct_no();
                            $no = str_replace("ytg", "", $no);
                            $c_p->setProduct_no($no);
                            $c_p->setProduct_name("自定义衣柜横隔板");
                        }
                        foreach ($scheme_products as $h_t_p) {
                            $h_t_p = change2ProductBean($h_t_p);
                            if (empty($h_t_p->getCom_products())) {
                                continue;
                            }
                            foreach ($h_t_p->getCom_products() as $h_t_c_p) {
                                $h_t_c_p = change2ProductBean($h_t_c_p);
                                if ($h_t_c_p->getProduct_is_score() == 1) {
                                    continue;
                                }
                                if(str_has($h_t_c_p->getProduct_type(),"ZH-YTG")){
                                    $no = $h_t_c_p->getProduct_no();
                                    $no = str_replace("ytg", "", $no);
                                    $h_t_c_p->setProduct_no($no);
                                }
                                if ($c_p->getProduct_no() != $h_t_c_p->getProduct_no()) {
                                    continue;
                                }
                                $c_p->setProduct_count($c_p->getProduct_count() + 1);
                                $h_t_c_p->setProduct_is_score(1);
                            }
                        }
                        $score_products[] = $c_p;
                    }
                    //处理组合内的中立板
                    foreach ($scheme_products as $z_t_p) {
                        $z_t_p = change2ProductBean($z_t_p);
                        if ($z_t_p->getProduct_is_score() == 1) {
                            continue;
                        }
                        if ($z_t_p->getProduct_type() != 'ZZ') {
                            continue;
                        }
                        if ($p->getProduct_no() != $z_t_p->getProduct_no()) {
                            continue;
                        }
                        $p->setProduct_count($p->getProduct_count() + 1);
                        $z_t_p->setProduct_is_score(1);
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }


                if ($p->getProduct_type() == 'ZZ') {
                    continue;
                }

                $p->setProduct_count(1);
                for ($j = $i + 1; $j < count($scheme_products); $j++) {
                    $t_p = $scheme_products[$j];
                    $t_p = change2ProductBean($t_p);
                    if (empty($t_p->getProduct_is_score())) {
                        if (str_has($t_p->getProduct_type(), 'ZH-YTG')) {
                            if($this->isDDB($scheme,$t_p)){
                                $no = "YD-" . $t_p->getProduct_width() . "-01-00-000";
                                $t_p->setProduct_no($no);
                                $t_p->setProduct_name('衣柜横隔板');
                                $t_p->setProduct_color_no('000');
                                $t_p->setProduct_color_name('灰布纹');
                            } else {
                                $no = $t_p->getProduct_no();
                                $no = str_replace("ytg", "", $no);
                                $t_p->setProduct_no($no);
                                $t_p->setProduct_name("自定义衣柜横隔板");
                            }
                        }elseif (str_has($t_p->getProduct_type(),"ZH")){
                            if($this->isDDB($scheme,$t_p)){
                                $no = $t_p->getProduct_no();
                                $t_p->setProduct_no($no);
                                $t_p->setProduct_name("顶底横隔板");
                            }
                        }
                        if ($p->getProduct_no() == $t_p->getProduct_no() && $t_p->getProduct_type() != 'ZZ' && $p->getProduct_type() != 'ZZ') {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $score_products[] = $p;
                $p->setProduct_is_score(1);
            }
        }
        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    if($p->getProduct_type() == "YZ-M"){
                        $p->setProduct_deep(16);
                    }
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dg_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $exist = false;
                    foreach ($score_products as $sp){
                        $sp = change2ProductBean($sp);
                        if($p->getProduct_no() == $sp->getProduct_no()){
                            $sp->setProduct_count($sp->getProduct_count()+$p->getProduct_count());
                            $exist = true;
                        }
                    }
                    $p->setProduct_is_score(1);
                    if(!$exist){
                        $score_products[] = $p;
                    }
                }
            }
        }
        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dt_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }


//		Collections.sort(score_products, new ProductRptSort_Seq());

        $scheme->setScheme_score_products($score_products);

        return $scheme;
    }

    public function scoreQWSchemePlates(SchemeBean $scheme)
    {
        $totalPrice = 0;
        $disTotalPrice = 0;

        $deep_arr = [ 25, 16, 12, 5 ];

        $p_product = null;
        $scheme_plates = [];
        for ($i = 0; $i < 4; $i++) {
            $p_product = new ProductBean();
            $p_product->setProduct_deep($deep_arr[$i]);
            $p_product->setProduct_name($deep_arr[$i] . 'mm厚');

            foreach ($scheme->getScheme_score_products() as $p) {
                $p = change2ProductBean($p);
                if ($deep_arr[$i] == $p->getProduct_deep()) {
                    if ($deep_arr[$i] == 12) { // 背板
                        $p_product->setProduct_area(
                            number_format(($p->getProduct_width()) * ($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    } else {
                        // 其他板
                        $p_product->setProduct_area(
                            number_format(($p->getProduct_width()) * ($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    }
                }
            }

            $disCount = $scheme->getScheme_discount();
            if (0 < $p_product->getProduct_area()) {
                $p_product->setProduct_price(formatMoney(getPlatePrice($i)));
                $this->countPlateMoney(getPlatePrice($i), $disCount, $p_product, $totalPrice, $disTotalPrice);
            }
            $scheme_plates[] = $p_product;
        }
        $scheme->setScheme_score_plates($scheme_plates);
        $scheme->setScheme_plate_price($totalPrice);
        $scheme->setScheme_dis_plate_price($disTotalPrice);

        return $scheme;
    }

    public function scoreQWSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $wujinMap = [];
        $t_p = null;
        if($scheme->getScheme_dg_products() != null && 0 < count($scheme->getScheme_dg_products())){
            $t_p = new ProductBean();
            $t_p->setProduct_name('墙外衣柜顶柜五金包');
            $t_p->setProduct_no('WB-011');
            $t_p->setProduct_count(1);
            $t_p->setProduct_unit('包');
            $t_p->setProduct_price(formatMoney(280));
            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
            $t_p->setSeq(0);
            $wujinMap[$t_p->getProduct_no()] = $t_p;
        }

        if($scheme->getScheme_door_count() != null && 0 < count($scheme->getScheme_door_count())) {
            for ($i = 1; $i <= $scheme->getScheme_door_count(); $i++) {
                if (!isset($wujinMap['WB-012']) || empty($wujinMap['WB-012'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('移动门五金包');
                    $t_p->setProduct_no('WB-012');
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit('包');
                    $t_p->setProduct_price(formatMoney(30));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $t_p->setSeq(0);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['WB-012'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            }
        }

        $t_p = new ProductBean();
        $t_p->setProduct_name("脚垫");
        $t_p->setProduct_no('W04-29-XX-00');
        $t_p->setProduct_count(6);
        $t_p->setProduct_unit('个');
        $t_p->setProduct_dis_price(formatMoney(1));
        $t_p->setProduct_price(formatMoney($t_p->getProduct_dis_price() / $scheme->getScheme_discount()));
        $t_p->setSeq(88);
        $wujinMap[$t_p->getProduct_no()] = $t_p;

//        $t_p = new ProductBean();
//        $t_p->setProduct_name("24连接杆");
//        $t_p->setProduct_no('W01-05-24-00');
//        $t_p->setProduct_count(8);
//        $t_p->setProduct_unit('个');
//        $t_p->setSeq(3);
//        $wujinMap[$t_p->getProduct_no()] = $t_p;
//
//        $t_p = new ProductBean();
//        $t_p->setProduct_name("15偏心轮");
//        $t_p->setProduct_no('W01-07-15-00');
//        $t_p->setProduct_count(8);
//        $t_p->setProduct_unit('个');
//        $t_p->setSeq(4);
//        $wujinMap[$t_p->getProduct_no()] = $t_p;

        $t_p = new ProductBean();
        $t_p->setProduct_name('墙外移门衣柜挡条五金包');
        $t_p->setProduct_no('WB-016');
        $t_p->setProduct_count(1);
        $t_p->setProduct_unit('包');
        $t_p->setProduct_price(formatMoney(12));
        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
        $t_p->setSeq(0);
        $wujinMap[$t_p->getProduct_no()] = $t_p;

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            //背板
            if ($p->getProduct_type() == 'ZB') {
                if (!isset($wujinMap['W01-02-XX-00']) || empty($wujinMap['W01-02-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("背板连接件");
                    $t_p->setProduct_no("W01-02-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setSeq(2);
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-02-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("13沉头自攻钉");
                    $t_p->setProduct_no("W02-01-3*13-00");
                    $t_p->setProduct_count(4 * 3);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(7);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*13-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                }
            } elseif ($p->getProduct_type() == 'ZH-YTG') {
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
                $p_no = "W06-01-" . ($p->getProduct_width() - 12) . "-00";
                if (!isset($wujinMap[$p_no]) || empty($wujinMap[$p_no])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通杆");
                    $t_p->setProduct_width($p->getProduct_width() - 12);
                    $t_p->setProduct_no($p_no);
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("支");
                    $t_p->setSeq($p->getProduct_width());
                    $t_p->setProduct_spec(($p->getProduct_width() - 12) . "mm");
                    $t_p->setProduct_price(formatMoney(
                        100 * ((getLength($p->getProduct_width() - 12)) / 1000)
                    ));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap[$p_no];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
                if (!isset($wujinMap['W04-07-XX-00']) || empty($wujinMap['W04-07-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通托五金包");
                    $t_p->setProduct_no("W04-07-XX-00");
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1200);
                    $t_p->setProduct_price(formatMoney(14));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-07-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
                //todo 已经包含在衣通托五金包
//                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("13沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*13-00");
//                    $t_p->setProduct_count(2);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(7);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*13-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                }
//                if (!isset($wujinMap['W02-01-3*25-00']) || empty($wujinMap['W02-01-3*25-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("25沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*25-00");
//                    $t_p->setProduct_count(4);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(10);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*25-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                }
            } elseif (($p->getProduct_type() == 'ZH') || (str_has($p->getProduct_no(),"YD") &&
                    str_has($p->getProduct_no(),"-01-"))){
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price()*$scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            }elseif($p->getProduct_type() == 'ZZ'){
                foreach ($p->getCom_products() as $c_p){
                    $c_p = change2ProductBean($c_p);
                    if ($c_p->getProduct_type() == 'ZB') {
                        if (!isset($wujinMap['W01-02-XX-00']) || empty($wujinMap['W01-02-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("背板连接件");
                            $t_p->setProduct_no("W01-02-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setProduct_price(formatMoney(7));
                            $t_p->setSeq(2);
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-02-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }

                        if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("13沉头自攻钉");
                            $t_p->setProduct_no("W02-01-3*13-00");
                            $t_p->setProduct_count(4 * 3);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(7);
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W02-01-3*13-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                        }
                    } elseif ($c_p->getProduct_type() == 'ZH') {
                        if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("层板连接件");
                            $t_p->setProduct_no("W01-01-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(1);
                            $t_p->setProduct_price(formatMoney(6));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-01-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }
                    } elseif ($c_p->getProduct_type() == 'ZH-YTG') {
                        if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("层板连接件");
                            $t_p->setProduct_no("W01-01-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(1);
                            $t_p->setProduct_price(formatMoney(6));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-01-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }
                        $p_no = "W06-01-" . ($c_p->getProduct_width() - 12) . "-00";
                        if (!isset($wujinMap[$p_no]) || empty($wujinMap[$p_no])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通杆");
                            $t_p->setProduct_width($c_p->getProduct_width() - 12);
                            $t_p->setProduct_no($p_no);
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("支");
                            $t_p->setSeq($c_p->getProduct_width());
                            $t_p->setProduct_spec(($c_p->getProduct_width() - 12) . "mm");
                            $t_p->setProduct_price(formatMoney(
                                100 * ((getLength($c_p->getProduct_width() - 12)) / 1000)
                            ));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap[$p_no];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                        if (!isset($wujinMap['W04-07-XX-00']) || empty($wujinMap['W04-07-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通托五金包");
                            $t_p->setProduct_no("W04-07-XX-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("套");
                            $t_p->setSeq(1200);
                            $t_p->setProduct_price(formatMoney(14));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W04-07-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                        //todo 已包含在五金包中
//                        if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name("13沉头自攻钉");
//                            $t_p->setProduct_no("W02-01-3*13-00");
//                            $t_p->setProduct_count(2);
//                            $t_p->setProduct_unit("个");
//                            $t_p->setSeq(7);
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['W02-01-3*13-00'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                        }
//                        if (!isset($wujinMap['W02-01-3*25-00']) || empty($wujinMap['W02-01-3*25-00'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name("25沉头自攻钉");
//                            $t_p->setProduct_no("W02-01-3*25-00");
//                            $t_p->setProduct_count(4);
//                            $t_p->setProduct_unit("个");
//                            $t_p->setSeq(10);
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['W02-01-3*25-00'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                        }
                    }
                }
            }

            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (is_starts_with($c_p->getProduct_no(), "L-")
                        || is_starts_with($c_p->getProduct_no(), "LB-")
                    ) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    } elseif (is_starts_with($c_p->getProduct_no(), "K-")
                    ) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    }

                    //处理组合内的拉篮和裤抽 Near
                    if ($c_p->getProduct_type() == 'ZH') {
                        if (!empty($c_p->getCom_products())) {
                            foreach ($c_p->getCom_products() as $c_p_c) {
                                $c_p_c = change2ProductBean($c_p_c);
                                if (is_starts_with($c_p_c->getProduct_no(), "L-")
                                    || is_starts_with($c_p_c->getProduct_no(), "LB-")
                                ) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                } elseif (is_starts_with($c_p_c->getProduct_no(), "K-")) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        $wujinProducts = [];
        foreach ($wujinMap as $productBean) {
            $wujinProducts[] = $productBean;
        }

        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($wujinProducts as $w_p) {
            $w_p = change2ProductBean($w_p);
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
//		Collections.sort(wujin_products, new ProductRptSort_Seq()); // todo:
        $scheme->setScheme_score_wujin_products($wujinProducts);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            $scheme->getScheme_dis_plate_price()
            + $scheme->getScheme_dis_wujin_price()
            + $scheme->getScheme_dis_zj_price()
        ));
        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));

        return $scheme;
    }


    public function scoreQWSchemeDoor(SchemeBean $scheme)
    {
        $doorCount = $scheme->getScheme_door_count();
        if ($doorCount == 0) {
            return $scheme;
        }

        $doorHeight = $scheme->getScheme_door_height();
        $doorWidth = $scheme->getScheme_door_width_one();

        $gridCount = 0;
        $gridHeight_300 = 0;
        $oneDoorArea = number_format(getLength($doorWidth) * getLength($doorHeight) / 1000000,2);
        $mod = ($doorHeight - 72) % 300;
        $gridHeight_300 = $gridCount = intval(($doorHeight - 72) / 300);

        if ($mod == 0) {

        } elseif ($mod >= 100) {
            $gridCount++;
        } else {
            $gridCount++;
            $gridHeight_300--;
        }

        $d_products = [];
        $temp = 0; // 门口宽

        $temp = $scheme->getScheme_width() - (2 * INNER_WIDTH);

        $d_p = new ProductBean();
        $d_p->setProduct_name("移门边料");
        $d_p->setProduct_no("W06-15-" . $doorHeight . "-00");
        $d_p->setProduct_height(DOOR_FRAME_WIDTH);
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count(2 * $doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-01");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-02");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上滑轨");
        $d_p->setProduct_no("W06-18-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下滑轨");
        $d_p->setProduct_no("W06-19-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("工字铝");
        $d_p->setProduct_no("W06-17-" . strval($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-00");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount * intval(($gridCount - 1)));
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        if ($gridHeight_300 == $gridCount) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * $gridCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;
        } elseif ($gridHeight_300 == $gridCount - 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 1));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*{$mod}*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height($mod);
            $d_p->setProduct_count($doorCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*{$mod}*9");
            $d_products[] = $d_p;
        } else {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 2));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                ."*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(intval(($mod + 300) / 2));
            $d_p->setProduct_count($doorCount * 2);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                ."*9");
            $d_products[] = $d_p;
        }

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("圆头不锈钢自攻钉");
//        $d_p->setProduct_no("W02-02-3*12-01");
//        $d_p->setProduct_count(2 * $doorCount * $gridCount);
//        $d_p->setProduct_unit("个");
//        $d_p->setProduct_spec("4*10");
//        $d_products[] = $d_p;

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("移门上下滑轮");
//        $d_p->setProduct_no("W04-19-XX-00");
//        $d_p->setProduct_count($doorCount);
//        $d_p->setProduct_unit("套");
//        $d_products[] = $d_p;

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("双面胶");
//        $d_p->setProduct_no("W04-38-XX-00");
//        $d_p->setProduct_count(1);
//        $d_p->setProduct_unit("条");
//        $d_p->setProduct_spec("适量");
//        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("缓冲条");
        $d_p->setProduct_no("W06-23-" . $doorHeight . "-00");
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count($doorCount * 2);
        $d_p->setProduct_unit("条");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $scheme->setScheme_door_spec($doorWidth . "*" . $doorHeight);
        $scheme->setScheme_s_door_area($oneDoorArea);
        $scheme->setScheme_door_has_count($doorCount);
        $scheme->setScheme_door_area($oneDoorArea * $doorCount);
        $scheme->setScheme_o_door_price(formatMoney(719));
        $scheme->setScheme_s_door_price(getMoney(getMoney(719 * $scheme->getScheme_discount()) * $scheme->getScheme_door_area() ));
        $scheme->setScheme_door_price(getMoney(719 * $scheme->getScheme_door_area()));
        $scheme->setScheme_dis_door_price(formatMoney($scheme->getScheme_door_price()*$scheme->getScheme_discount()));

        if ($scheme->getScheme_door_haveHCQ() == 1 && $scheme->getScheme_have_door() == 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("移门缓冲器");
            $d_p->setProduct_no("W04-20-XX-00");
            $d_p->setProduct_count(intval($doorCount/2));
            $d_p->setProduct_unit("套");
            $d_p->setProduct_price(formatMoney(199));
            $d_p->setProduct_dis_price(formatMoney($d_p->getProduct_price() * $scheme->getScheme_discount()));
            $d_p->setProduct_total_price(formatMoney($d_p->getProduct_price() * $d_p->getProduct_count()));
            $d_p->setProduct_dis_total_price(formatMoney($d_p->getProduct_dis_price() * $d_p->getProduct_count()));
            $scheme->setScheme_door_price(getMoney($scheme->getScheme_door_price() + $d_p->getProduct_total_price()));
            $scheme->setScheme_dis_door_price(getMoney($scheme->getScheme_dis_door_price() + $d_p->getProduct_dis_total_price()));
            $scheme->setScheme_door_mat_price($d_p->getProduct_total_price());
            $d_products[] = $d_p;
        }

        $scheme->setScheme_score_door_products($d_products);
        $scheme_total_price = $scheme->getScheme_price() + $scheme->getScheme_gt_price() + $scheme->getScheme_door_price() + $scheme->getScheme_sk_price();
        $scheme->setScheme_price(getMoney($scheme_total_price));
        $scheme_total_dis_price = $scheme->getScheme_dis_price() + $scheme->getScheme_dis_gt_price() +
            $scheme->getScheme_dis_door_price() + $scheme->getScheme_dis_sk_price();
        //$scheme->setScheme_dis_price(getMoney($scheme_total_dis_price));
        $scheme->setScheme_dis_price(getMoney($scheme->getScheme_price() * $scheme->getScheme_discount()));
        //计算总价格
        return $scheme;
    }

    private function countProductMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price($price);
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_count())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_count())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }

    private function countPlateMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price(formatMoney($price));
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_area())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_area())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }

    private function isDDB(SchemeBean $scheme,ProductBean $productBean){
        if(($productBean->getProduct_type() == "ZH")){
            if($productBean->getM_top() < 2){
                return true;
            }
            $height = $scheme->getScheme_height();

            if($height > 2410){
                $height -= 483;
            }
            if($productBean->getM_top() > ($height - 70) / 8){
                return true;
            }
        }
        return false;
    }
}
