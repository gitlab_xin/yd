<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\logic;


use app\common\model\MyHomeDecoratePoint;
use app\common\model\MyHomeDecorateProgram;
use app\common\model\MyHomeHouseType;
use think\Db;

class MyHomeHouseTypeLogic
{
    public static function checkHouseType($house_id)
    {
        return MyHomeHouseType::build()->where(['house_id' => $house_id, 'is_deleted' => 0])->find();
    }

    public static function insertHouseType($house_id, $requestData)
    {
        unset($requestData['house_id']);
        $data['render_img'] = $requestData['render_img'];
        $data['intro'] = $requestData['intro'];
        $data['serial_array'] = json_encode($requestData, JSON_UNESCAPED_UNICODE);
        return MyHomeHouseType::build()->update($data, ['house_id' => $house_id]);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型webgl模型
     * @param $house_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getHouseTypeModel($house_id)
    {
        return MyHomeHouseType::build()->where(['house_id' => $house_id, 'is_deleted' => 0])->value('serial_array');
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:写入装修方案模型
     * @param $requestData
     * @return int|string
     */
    public static function insertDecorateProgram($requestData)
    {
        $requestData['create_time'] = time();
        return MyHomeDecorateProgram::build()->insert($requestData);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:编辑装修方案模型
     * @param $program_id
     * @param $requestData
     * @return int|string
     */
    public static function saveDecorateProgram($program_id, $requestData)
    {
        $requestData['update_time'] = time();
        return MyHomeDecorateProgram::build()->update($requestData, ['program_id' => $program_id]);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案模型
     * @param $program_id
     * @return int|string
     */
    public static function getDecorateProgram($program_id)
    {
        return MyHomeDecorateProgram::build()->where(['program_id' => $program_id, 'is_deleted' => 0])
            ->field(['program_id', 'name', 'img_src', 'serial_array', 'create_time', 'house_id', 'shapespar_url'])->find();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案列表
     * @param $page
     * @param $num
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getDecorateProgramList($page, $num,$where = [])
    {
        $where['is_deleted'] = 0;
        return MyHomeDecorateProgram::build()->where($where)
            ->field(['program_id', 'name', 'img_src', 'create_time', 'house_id', 'shapespar_url'])->page($page, $num)->select();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型详情
     * @param $house_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getInfo($house_id)
    {
        return MyHomeHouseType::build()->where(['house_id' => $house_id, 'is_deleted' => 0])
            ->field(['house_id', 'name', 'bedroom', 'proportion', 'elevator_count', 'orientation', 'img_src', 'intro'])
            ->find();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型下装修亮点
     * @param $house_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getHouseDecorate($house_id)
    {
        return MyHomeDecoratePoint::build()->alias('d')
            ->join('yd_my_home_house_decorate hd', "hd.decorate_id = d.decorate_id")
            ->where(['hd.house_id' => $house_id, 'd.is_deleted' => 0])
            ->field(['d.decorate_id', 'd.name', 'd.img_src', 'd.sub_title'])->select();
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 获取三维环境 新增接口
     * @param $house_id
     * @return array|mixed
     */
    public static function get3DByCabinet($house_id)
    {
        $info = MyHomeHouseType::build()->where(['house_id' => $house_id])->value('environment_array');
        $environment_array = json_decode($info,true);

        return [
            'three' => [
                'camera' => &$environment_array['camera'],
                'light' => &$environment_array['lightGroup'],
                'render' => &$environment_array['render'],
                'control' => &$environment_array['orbitCtrl']
            ]];
    }

}
