<?php
/**
 * Created by PhpStorm.
 * User: ChiaHsiang
 * Date: 2018/9/3
 * Time: 14:44
 */

namespace app\api\logic\cms;

use app\common\tools\JPush;

class JPushLogic
{
    private $config = '';
    private $push_handle = '';

    public function __construct()
    {
        $this->config = config('jpush');
        $this->push_handle = new JPush($this->config['AppKey'], $this->config['MasterSecret']);
    }

    public function afterShippingPushDirect($alias = [], $order_id, $order_uuid) {
        $alert = "尊敬的客户，您的订单已发货，订单号为{$order_id}，请注意查收，祝您购物愉快！";
        $message = [
            'type' => 0,
            'order_id' => $order_uuid,

        ];

        return $this->push_handle
            ->push($alias, 'all', $alert, $message);
    }

    public function afterCustomerReceived($alias = [], $order_id, $order_uuid) {
        $alert = "尊敬的客户，您的订单已签收，订单号为{$order_id}，谢谢惠顾，下次光临！";
        $message = [
            'type' => 0,
            'order_id' => $order_uuid,

        ];

        return $this->push_handle
            ->push($alias, 'all', $alert, $message);
    }
}