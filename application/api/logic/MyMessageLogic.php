<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 15:18
 */

namespace app\api\logic;

use app\common\model\Demand as DemandModel;
use app\common\model\DemandPrivateMessage as DemandMessageModel;
use app\common\model\ShopOrderLogistics as LogisticsModel;
use app\common\model\ShopOrderOfficialLogistics as OfficialLogisticsModel;
use app\common\model\MessageOfficial as OfficialModel;
use app\common\model\MessageForum as ForumModel;
use app\common\model\DemandNotify as NotifyModel;
use think\Config;

class MyMessageLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取易道消息
     * @param $pageIndex
     * @param $pageSize
     * @return array
     */
    public static function getOfficialMessages($pageIndex,$pageSize)
    {
        return OfficialModel::build()->field(['id as message_id','title','img_src','activity_id','create_time'])->page($pageIndex,$pageSize)
            ->order(['id'=>'DESC'])->select()->toArray();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取易道消息详情
     * @param $id
     * @return array|int
     */
    public static function getOfficialMessageDetail($id)
    {
        $model = OfficialModel::build()->field(['title','content','activity_id','create_time'])->where(['id'=>$id])->find();
        if($model == null){
            return -1;
        }
        if($model->activity_id != 0){
            return 0;
        }
        unset($model->activity_id);
        return $model->toArray();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取论坛消息
     * @param $user_id
     * @param $pageIndex
     * @param $pageSize
     * @return array
     */
    public static function getForumMessages($user_id,$pageIndex,$pageSize)
    {
        try{
            $result =  ForumModel::build()
                ->alias('m')
                ->field(['m.article_id','m.by_user_id','m.type','m.create_time','f.title','f.is_essence','f.is_top','f.is_official','GROUP_CONCAT(u.user_id) as user_ids',
                    'GROUP_CONCAT(u.username) as usernames','GROUP_CONCAT(u.avatar) as avatars','GROUP_CONCAT(u.is_designer) as designers'])
                ->join('forum_article f','f.article_id = m.article_id','left')
                ->join('user u','f.user_id = u.user_id or m.by_user_id = u.user_id','left')
                ->where(['m.user_id'=>$user_id,'f.is_deleted'=>0])
                ->group('m.id')
                ->page($pageIndex,$pageSize)
                ->order(['m.id'=>'DESC'])->select()->toArray();
            if($result){
                foreach ($result as &$v){
                    $user_id_map = explode(',',$v['user_ids']);
                    $username_map = explode(',',$v['usernames']);
                    $avatar_map = explode(',',$v['avatars']);
                    $designer_map = explode(',',$v['designers']);

                    $by_user_index = array_search($v['by_user_id'],$user_id_map);
                    $article_user_index = (int)$by_user_index;
                    $v['article_username'] = $username_map[$article_user_index];
                    $v['article_avatar'] = $avatar_map[$article_user_index];
                    $v['article_designer'] = $designer_map[$article_user_index];
                    $v['by_username'] = $username_map[$by_user_index];
                    $v['by_avatar'] = $avatar_map[$by_user_index];
                    unset($user_id_map,$username_map,$avatar_map,$designer_map,$v['user_ids'],$v['usernames'],$v['avatars'],$v['designers']);
                }
            }
            return $result;
        }catch (\Exception $e){
            return false;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月21日
     * description:获取每一条信息
     * @param $user_id
     * @return array
     */
    public static function getEverySingleMessage($user_id)
    {
        $result = [];
        //易道消息
        $officialMessage = OfficialModel::build()->order(['id'=>'DESC'])->limit(1)->find();
        $result['official']['push_id'] = isset($officialMessage->activity_id)?($officialMessage->activity_id == 0?$officialMessage->id:$officialMessage->activity_id):0;
        $result['official']['title'] = isset($officialMessage->title)?$officialMessage->title:'暂无新消息';
        $result['official']['create_time'] = isset($officialMessage->create_time)?$officialMessage->create_time:0;


        //按需定制
        $messages = DemandMessageModel::build()->field(['demand_id','create_time'])->where(function($query)use($user_id){
           $query->where(['user_id'=>$user_id,'status'=>'master']);
        });
        if(($my_demand_ids = DemandModel::build()->where(['user_id'=>$user_id])->value('GROUP_CONCAT(demand_id)'))!= null){
            $messages->whereOr(function($query)use($my_demand_ids){
                $query->whereIn('demand_id',$my_demand_ids)->where(['status'=>'guest']);
            });
        }
        $message = $messages->order(['message_id'=>'DESC'])->find();
        $notify = NotifyModel::build()->field(['demand_id','update_time'])->where(['user_id'=>$user_id])->order(['notify_id'=>'DESC'])->find();
        if(($message != null && $notify == null) || ($message != null && $notify != null && $message['create_time'] >= $notify['update_time'])){
            $result['need']['push_id'] = $message['demand_id'];
            $result['need']['title'] = '有人给你发来了私信';
            $result['need']['create_time'] = $message['create_time'];
        }elseif(($notify != null && $message == null) || ($message != null && $notify != null && $message['create_time'] < $notify['update_time'])){
            $result['need']['push_id'] = $notify['demand_id'];
            $result['need']['title'] = '需求进度发生改变快来看看';
            $result['need']['create_time'] = $notify['update_time'];
        }else{
            $result['need']['push_id'] = $result['need']['title'] = 0;
            $result['need']['title'] = '暂无新的消息';
            $result['need']['create_time'] = 0;
        }

        //论坛消息
        $forumMessage = ForumModel::build()->alias('f')->field(['f.article_id','u.username','case f.type when 0 then "帖子" when 1 then "评论" end as type','f.create_time'])
            ->where(['f.user_id'=>$user_id])->order(['f.id'=>'DESC'])->join('user u','f.by_user_id = u.user_id','left')->limit(1)->find();
        $result['forum']['push_id'] = isset($forumMessage->article_id)?$forumMessage->article_id:0;
        $result['forum']['title'] = isset($forumMessage->username)?$forumMessage->username.'回复了你的'.$forumMessage->type.'，快去看看吧！':'暂无新消息';
        $result['forum']['create_time'] = isset($forumMessage->create_time)?$forumMessage->create_time:0;


        //物流信息
        $logistics = LogisticsModel::build()->field(['order_id','logistics_id'])->where(['user_id'=>$user_id,'logistics_type'=>'official'])
            ->order(['update_time'=>'DESC','logistics_id'=>'DESC'])->find();
        $logisticsMessage = $logistics != null?OfficialLogisticsModel::build()->where(['logistics_id'=>$logistics->logistics_id])->field(['content','create_time'])->order(['id'=>'DESC'])->find():null;
        $result['delivery']['push_id'] = $logistics != null?$logistics->order_id:0;
        $result['delivery']['title'] = $logisticsMessage != null?$logisticsMessage->content:'暂无新消息';
        $result['delivery']['create_time'] = $logisticsMessage != null?$logisticsMessage->create_time:0;


        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月25日
     * description:物流信息
     * @param $user_id
     * @param $pageIndex
     * @param $pageSize
     * @return array|bool
     */
    public static function getLogisticsMessages($user_id,$pageIndex,$pageSize)
    {
       // try{

            $logisticsList = LogisticsModel::build()->alias('l')
                ->join('yd_shop_order o','l.order_id = o.order_id','LEFT')
                ->join('yd_shop_order_product op','l.order_id = op.order_id AND l.order_type = "shop"','LEFT')
                ->join('yd_customized_order_scheme os','l.order_id = os.order_id AND l.order_type = "customized"','LEFT')
                ->field(['l.logistics_id as id','l.order_id','o.order_num','l.logistics_type','l.logistics_number',
                    'l.update_time','op.product_name','op.img_src_list','os.scheme_name','os.scheme_pic','l.order_type','l.status'])
                ->where(['l.user_id'=>$user_id])->order(['l.update_time'=>'DESC'])->page($pageIndex,$pageSize)->select()->toArray();

            if(!empty($logisticsList)){
                $ids = implode(',',array_column($logisticsList,'id'));
                $result = OfficialLogisticsModel::build()->alias('a')->field(['logistics_id','content'])
                    ->join("(SELECT MAX(id) as id FROM yd_shop_order_official_logistics WHERE user_id = '.$user_id.' AND logistics_id IN ('.$ids.') GROUP BY logistics_id) b",'a.id = b.id')
                    ->page($pageIndex,$pageSize)->select()->toArray();
                $result = array_column($result,'content','logistics_id');
                $mapping = Config::get('mall.logistics')['type'];
                foreach ($logisticsList as &$v){
                    $v['logistics_name'] = $mapping[$v['logistics_type']];
                    $v['content'] = isset($result[$v['id']])?$result[$v['id']]:'';
                    $v['logistics_id'] = $v['id'];
                    unset($v['id']);
                }
                foreach ($logisticsList as $key => $value) {
                    $logisticsList[$key]['img_src'] = empty($value['img_src_list']) ? "" : explode('|', $value['img_src_list'])[0];
                    unset($logisticsList[$key]['img_src_list']);
                }
            }
            return $logisticsList;
//        }catch (\Exception $e){
//
//            return false;
//        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月28日
     * description:获取我的需求定制私信 *该处查询较复杂,有好的办法可以直接重写*
     * @param $user_id
     * @return array|bool
     */
    public static function getDemandMessages($user_id)
    {
        try{
            $where = ['user_id' => $user_id];
            $whereOr = [];
            //查看是否自己有发布需求,如果有发布,作为查询条件之一
            if(($my_demand_ids = DemandModel::build()->where($where)->value('GROUP_CONCAT(demand_id)'))!= null){
                $whereOr['demand_id'] = ['in',$my_demand_ids];
            }
            //获取需要获取的需求id
            $messages = DemandMessageModel::build()
                ->field(['max(message_id) as message_id','demand_id'])
                ->where($where)
                ->whereOr($whereOr)
                ->group('demand_id')
                ->select()->toArray();
            //如果有私信
            if(!empty($messages)){
                $arr = array_column($messages,'demand_id','message_id');//key为message_id value为demand_id
                krsort($arr);//排序,此时value的需求id就能按照私信顺序排序
                //获取需求
                $demand = DemandModel::build()->field(['demand_id','title','classify','style_name','material_name','user_id'])->whereIn('demand_id',$arr)->select()->toArray();
                if(!empty($messages)){
                    $result = array_flip($arr);//key为demand_id value为message_id
                    $demand = array_column($demand,null,'demand_id');
                    foreach ($result as $k => &$v){
                        if(isset($demand[$k])){//如果有存在该需求
                            $message_id = $v;
                            $v = $demand[$k];

                            $sql = DemandMessageModel::build()->alias('m')->field(['m.*', 'u.username', 'u.avatar'])
                                ->join('user u', 'u.user_id = m.user_id', 'LEFT');
                            if($v['user_id'] != $user_id){//如果不是需求发布者,只能获取自己跟需求发布者的私信
                                $where = ['m.message_id'=>$message_id];
                            }else{//如果是需求发布者,则可以获取前4条
                                $where = ['m.demand_id' => $k];
                                $sql = $sql->join("(SELECT MAX(message_id) as message_id FROM yd_demand_private_message WHERE demand_id = " . $k . " GROUP BY user_id) a","a.message_id = m.message_id");
                            }

                            $v['messages'] = $sql
                                ->where($where)
                                ->whereNotNull('u.username')
                                ->order(['message_id' => 'DESC'])
                                ->limit(0,4)
                                ->select()->toArray();

                            unset($v['user_id']);
                        }else{
                            unset($result[$k]);
                        }
                        unset($v);
                    }
                    $result = array_values($result);
                }
            }
            return isset($result)?$result:[];
        }catch (\Exception $e){
            return false;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月31日
     * description:获取该需求下所有私信
     * @param $user_id
     * @param $demand_id
     * @return array|bool|false|\PDOStatement|string|\think\Model
     */
    public static function getAllDemandMessages($user_id,$demand_id)
    {
        try {
            $demand = DemandModel::get($demand_id);
            $where = ['m.demand_id' => $demand_id];

            if ($demand != null) {
                if ($demand->user_id != $user_id) {//自己是承接者,对于该需求,只获取最新的一条记录即可,因为你在该需求内只能跟需求者私信
                    $where['m.user_id'] = $user_id;

                    $result[] = DemandMessageModel::build()->alias('m')->field(['m.*', 'u.username', 'u.avatar'])
                        ->join('user u', 'u.user_id = m.user_id', 'LEFT')
                        ->order(['m.message_id' => 'DESC'])->where($where)->find();
                } else {
                    $result = DemandMessageModel::build()->alias('m')->field(['m.*', 'u.username', 'u.avatar'])
                        ->join("(SELECT MAX(message_id) as message_id FROM yd_demand_private_message WHERE demand_id = {$demand_id} GROUP BY user_id) pm",'pm.message_id = m.message_id')
                        ->join('user u', 'u.user_id = m.user_id', 'LEFT')
                        ->where($where)
                        ->whereNotNull('u.username')
                        ->order(['message_id' => 'DESC'])
                        ->select()->toArray();
                }

                return $result;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            return false;
        }
    }
}