<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/18
 * Time: 11:27
 */

namespace app\api\logic;

use app\common\model\ShopProduct as ProductModel;
use app\common\model\ShopClassify as ClassifyModel;
use app\common\model\ShopAdvertisement as AdvertisementModel;

class ShopClassifyLogic
{
    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取一级分类列表
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selectLevelOne()
    {
        return ClassifyModel::selectLevelOne();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:检查是否为一级分类
     * @param $requestData
     * @return bool
     */
    public static function checkIsParent($requestData)
    {
        return ClassifyModel::checkIsParent($requestData['classify_id']);
    }

    /**
     * @author: Airon
     * @time: 2017年7月18日
     * description:获取分类广告位轮播图
     * @param $requestData
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function bannerList($requestData)
    {
        return AdvertisementModel::build()
            ->where(['classify_id' => $requestData['classify_id']])
            ->field(['img_src', 'related'])
            ->order('create_time DESC')
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月18日
     * description:获取二三级分类列表
     * @param $requestData
     * @return false|\PDOStatement|string|\think\Collection|array
     */
    public static function appChild($requestData)
    {
        $field = ['classify_id', 'name', 'img_src'];
        $level_two = ClassifyModel::build()
            ->where(['parent_id' => $requestData['classify_id']])
            ->field($field)
            ->select();
        $array = array();
        foreach ($level_two as $key => $value) {
            $temp_array = $value->toArray();
            $temp_array['child'] = ClassifyModel::build()
                ->where(['parent_id' => $temp_array['classify_id']])
                ->field($field)
                ->select();
            $array[] = $temp_array;
            unset($temp_array);
        }

        return $array;
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取一二级分类列表或获取二三级分类列表
     * @param $classify_id
     * @return false|\PDOStatement|string|\think\Collection|array
     */
    public static function webChild($classify_id = 0)
    {
        $field = ['classify_id', 'name'];
        $level_two = ClassifyModel::build()
            ->where(['parent_id' => $classify_id])
            ->field($field)
            ->select();
        $array = array();
        foreach ($level_two as $key => $value) {
            $temp_array = $value->toArray();
            $temp_array['child'] = ClassifyModel::build()
                ->where(['parent_id' => $temp_array['classify_id']])
                ->field($field)
                ->select();
            $array[] = $temp_array;
            unset($temp_array);
        }

        return $array;
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取一二级分类列表或获取二三级分类列表
     * @param $classify_id
     * @return false|\PDOStatement|string|\think\Collection|array
     */
    public static function getChild($classify_id = 0)
    {
        $field = ['classify_id', 'name'];
        return ClassifyModel::build()
            ->where(['parent_id' => $classify_id])
            ->field($field)
            ->select();

    }

    /**
     * @author: Rudy
     * @time: 2017年8月10日
     * description:根据商品id获取其所有分类
     * @param int $product_id
     */
    public static function getClassifyNames($product_id = 0)
    {
        $res = ProductModel::build()->alias('p')
            ->field(['c.classify_id','c.name','c.parent_id'])
            ->join('shop_classify c','p.classify_one_id = c.classify_id OR p.classify_two_id = c.classify_id OR p.classify_three_id = c.classify_id','left')
            ->where(['p.product_id'=>$product_id])
            ->select()
            ->toArray();
        return $res;
    }
}