<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\CustomizedScheme;
use app\common\model\ProductBean;
use app\common\model\Scheme as SchemeModel;
use app\common\model\Product as ProductModel;
use app\common\model\SchemeBean;
use app\common\tools\ImageUtils;

class SchemeRQSOrderLogic
{
    public $productLogic = null;

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    /**
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function scoreRQSSchemeZJProducts(SchemeBean $scheme)
    {

        $score_products = [];
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    $c_p->setProduct_is_score(0);
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getCom_products())) {
                continue;
            }
            foreach ($p->getCom_products() as $c_p) {
                $c_p = change2ProductBean($c_p);
                if ($c_p->getProduct_is_score() == '1') {
                    continue;
                }
                $productNo = $c_p->getProduct_no();
                if (is_starts_with($productNo, 'L-') || is_starts_with($productNo, 'LB-') || is_starts_with($productNo, 'K-')) {
                    $c_p->setProduct_count(0);
                    foreach ($scheme_products as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if (empty($t_p->getCom_products())) {
                            continue;
                        }
                        foreach ($t_p->getCom_products() as $t_c_p) {
                            $t_c_p = change2ProductBean($t_c_p);
                            if ($t_c_p->getProduct_is_score() == 1) {
                                continue;
                            }
                            if ($c_p->getProduct_no() != $t_c_p->getProduct_no()) {
                                continue;
                            }
                            $c_p->setProduct_count($c_p->getProduct_count() + 1);
                            $t_c_p->setProduct_is_score(1);
                        }
                    }
                    $c_p->setProduct_is_score(1);

                    if (isset($score_products[$productNo])) {
                        $s_p_1 = change2ProductBean($score_products[$productNo]);
                        $s_p_1->setProduct_count($c_p->getProduct_count() + $s_p_1->getProduct_count());
                    }else {
                        $score_products[$productNo] = $c_p;
                    }

                }

                //处理组合内的拉篮和裤抽 Near
                if ($c_p->getProduct_type() == 'ZH') {
                    if (empty($c_p->getCom_products())) {
                        continue;
                    }
                    foreach ($c_p->getCom_products() as $c_p_c) {
                        $productNo = $c_p_c->getProduct_no();
                        if (is_starts_with($productNo, 'L-') || is_starts_with($productNo, 'LB-') || is_starts_with($productNo, 'K-')) {
                           if ($c_p_c->getProduct_is_score() == 1) {
                               continue;
                           }
                            $c_p_c->setProduct_color_no("");//组件没有颜色
                            $c_p_c->setProduct_count(1);
                            $c_p_c->setProduct_is_score(1);
                            foreach ($scheme_products as $t_p) {
                                $t_p = change2ProductBean($t_p);
                                if (empty($t_p->getCom_products())){
                                    continue;
                                }
                                foreach ($t_p->getCom_products() as $t_c_p) {
                                    foreach ($t_c_p->getCom_products() as $t_c_p_c) {
                                        $t_c_p_c = change2ProductBean($t_c_p_c);
                                        if ($t_c_p_c->getProduct_is_score() == 1) {
                                            continue;
                                        }
                                        if ($c_p_c->getProduct_no() != $t_c_p_c->getProduct_no()) {
                                            continue;
                                        }
                                        $c_p_c->setProduct_count($c_p_c->getProduct_count() + 1);
                                        $t_c_p_c->setProduct_is_score(1);
                                    }
                                }
                            }
                            if (isset($score_products[$productNo])) {
                                $s_p = change2ProductBean($score_products[$productNo]);
                                $s_p->setProduct_count($c_p_c->getProduct_count() + $s_p->getProduct_count());
                            }else {
                                $score_products[$productNo] = $c_p_c;
                            }
                        }
                    }
                }
            }
        }

//        Collections.sort(score_products, new ProductRptSort_Seq()); // todo: 不排序可能有问题
        $width = 0;
        $totalPrice = 0;
        $dis_totalPrice = 0;
        $discount = $scheme->getScheme_discount();
        foreach ($score_products as $p) {
            $p = change2ProductBean($p);
            if (is_starts_with($p->getProduct_no(), "L")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(410, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(450, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(530, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(610, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(690, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            } elseif (is_starts_with($p->getProduct_no(), "K-")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(350, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(385, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(420, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(455, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(525, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            }
        }

        $scheme->setScheme_score_zj_products($score_products);
        $scheme->setScheme_zj_price($totalPrice);
        $scheme->setScheme_dis_zj_price($dis_totalPrice);

        return $scheme;
    }

    public function scoreRQSSchemeProducts(SchemeBean $scheme)
    {
        $scheme->setScheme_price(0);
        $scheme->setScheme_dis_price(0);
        $score_products = [];

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            $p->setProduct_is_score(0);
        }
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_wcb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        foreach ($scheme->getScheme_ncb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_ncb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dg_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }

        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dt_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }

        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            foreach ($s->getScheme_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $no = "";
        for ($i = 0; $i < count($scheme_products); $i++) {
            $p = $scheme_products[$i];
            $p = change2ProductBean($p);

            //处理组合内的横隔板
            if ($p->getProduct_type() == 'ZZ') {
                //处理组合内的横隔板
                foreach ($p->getCom_products() as $c_p) {
                    if ($c_p->getProduct_is_score() == '1') {
                        continue;
                    }
                    foreach ($scheme_products as $h_t_p) {
                        $h_t_p = change2ProductBean($h_t_p);
                        if (empty($h_t_p->getCom_products())) {
                            continue;
                        }
                        foreach ($h_t_p->getCom_products() as $h_t_c_p) {
                            $h_t_c_p = change2ProductBean($h_t_c_p);
                            if ($h_t_c_p->getProduct_is_score() == 1) {
                                continue;
                            }
                            if ($c_p->getProduct_no() != $h_t_c_p->getProduct_no()) {
                                continue;
                            }
                            $c_p->setProduct_count($c_p->getProduct_count() + 1);
                            $h_t_c_p->setProduct_is_score(1);
                        }
                    }
                    $score_products[] = $c_p;
                }
            }

            if ($p->getProduct_is_score() == 1) {
                continue;
            }
            if (str_has($p->getProduct_type(), "ZH-YTG")) {
                $no = $p->getProduct_no();
                $no = str_replace("ytg", "", $no);
                $p->setProduct_no($no);
                $p->setProduct_name("自定义衣柜横隔板");

            }
            $p->setProduct_count(1);


            if ($p->getProduct_type() == 'ZZ') {
                //处理组合内的横隔板
//                foreach ($p->getCom_products() as $c_p) {
//                    if ($c_p->getProduct_is_score() == '1') {
//                        continue;
//                    }
//                    foreach ($scheme_products as $h_t_p) {
//                        $h_t_p = change2ProductBean($h_t_p);
//                        if (empty($h_t_p->getCom_products())) {
//                            continue;
//                        }
//                        foreach ($h_t_p->getCom_products() as $h_t_c_p) {
//                            $h_t_c_p = change2ProductBean($h_t_c_p);
//                            if ($h_t_c_p->getProduct_is_score() == 1) {
//                                continue;
//                            }
//                            if ($c_p->getProduct_no() != $h_t_c_p->getProduct_no()) {
//                                continue;
//                            }
//                            $c_p->setProduct_count($c_p->getProduct_count() + 1);
//                            $h_t_c_p->setProduct_is_score(1);
//                        }
//                    }
//                    $score_products[] = $c_p;
//                }
                //处理组合内的中立板
                $p->setProduct_is_score(1);
                foreach ($scheme_products as $z_t_p) {
                    $z_t_p = change2ProductBean($z_t_p);
                    if ($z_t_p->getProduct_is_score() == 1) {
                        continue;
                    }
                    if ($z_t_p->getProduct_type() != 'ZZ') {
                        continue;
                    }
                    if ($p->getProduct_no() != $z_t_p->getProduct_no()) {
                        continue;
                    }
                    $p->setProduct_count($p->getProduct_count() + 1);
                    $z_t_p->setProduct_is_score(1);
                }
                $score_products[] = $p;
            }


            if ($p->getProduct_type() == 'ZZ') {
                continue;
            }

            for ($j = $i + 1; $j < count($scheme_products); $j++) {
                $t_p = $scheme_products[$j];
                $t_p = change2ProductBean($t_p);

                if ($t_p->getProduct_is_score() == '1') {
                    continue;
                }
                if (str_has($t_p->getProduct_type(), 'ZH-YTG')) {
                    $no = $t_p->getProduct_no();
                    $no = str_replace("ytg", "", $no);
                    $t_p->setProduct_no($no);
                    $t_p->setProduct_name("自定义衣柜横隔板");
                }
                if ($p->getProduct_no() == $t_p->getProduct_no() && $t_p->getProduct_type() != 'ZZ' && $p->getProduct_type() != 'ZZ') {
                    $p->setProduct_count($p->getProduct_count() + 1);
                    $t_p->setProduct_is_score(1);
                }
            }

            $score_products[] = $p;
            $p->setProduct_is_score(1);





        }

//		Collections.sort(score_products, new ProductRptSort_Seq());

        $scheme->setScheme_score_products($score_products);

        return $scheme;
    }

    public function scoreRQSSchemePlates(SchemeBean $scheme)
    {
        $totalPrice = 0;
        $disTotalPrice = 0;

        $deep_arr = [25, 16, 12, 5];

        $p_product = null;
        $scheme_plates = [];
        for ($i = 0; $i < 4; $i++) {
            $p_product = new ProductBean();
            $p_product->setProduct_deep($deep_arr[$i]);
            $p_product->setProduct_name($deep_arr[$i] . 'mm厚');

            foreach ($scheme->getScheme_score_products() as $p) {
                $p = change2ProductBean($p);
                if ($deep_arr[$i] == $p->getProduct_deep()) {
                    if ($deep_arr[$i] == 12) { // 背板
                        $p_product->setProduct_area(
                            number_format(($p->getProduct_width()) * ($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    } else {
                        // 其他板
                        $p_product->setProduct_area(
                            number_format(($p->getProduct_width()) * ($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    }
                }
            }

            $disCount = $scheme->getScheme_discount();
            if (0 < $p_product->getProduct_area()) {
                $p_product->setProduct_price(formatMoney(getPlatePrice($i)));
                $this->countPlateMoney(getPlatePrice($i), $disCount, $p_product, $totalPrice, $disTotalPrice);
            }
            $scheme_plates[] = $p_product;
        }
        $scheme->setScheme_score_plates($scheme_plates);
        $scheme->setScheme_plate_price($totalPrice);
        $scheme->setScheme_dis_plate_price($disTotalPrice);

        return $scheme;
    }

    public function scoreRQSSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $wujinMap = [];
        $t_p = null;

        if($scheme->getScheme_door_count() != null && 0 < count($scheme->getScheme_door_count())) {
            for ($i = 1; $i <= $scheme->getScheme_door_count(); $i++) {
                if (!isset($wujinMap['WB-012']) || empty($wujinMap['WB-012'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('移动门五金包');
                    $t_p->setProduct_no('WB-012');
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit('包');
                    $t_p->setProduct_price(formatMoney(30));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $t_p->setSeq(0);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['WB-012'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if ($p->getProduct_type() == 'ZB') {
                if (!isset($wujinMap['W01-02-XX-00']) || empty($wujinMap['W01-02-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("背板连接件");
                    $t_p->setProduct_no("W01-02-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setSeq(2);
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-02-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("13沉头自攻钉");
                    $t_p->setProduct_no("W02-01-3*13-00");
                    $t_p->setProduct_count(4 * 3);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(7);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*13-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                }
            } elseif ($p->getProduct_type() == 'ZH') {
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            } elseif ($p->getProduct_type() == 'ZH-YTG') {
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
                $p_no = "W06-01-" . ($p->getProduct_width() - 12) . "-00";
                if (!isset($wujinMap[$p_no]) || empty($wujinMap[$p_no])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通杆");
                    $t_p->setProduct_width($p->getProduct_width() - 12);
                    $t_p->setProduct_no($p_no);
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("支");
                    $t_p->setSeq($p->getProduct_width());
                    $t_p->setProduct_spec(($p->getProduct_width() - 12) . "mm");
                    $t_p->setProduct_price(formatMoney(
                        100 * ((getLength($p->getProduct_width() - 12)) / 1000)
                    ));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap[$p_no];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
                if (!isset($wujinMap['W04-07-XX-00']) || empty($wujinMap['W04-07-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通托五金包");
                    $t_p->setProduct_no("W04-07-XX-00");
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1200);
                    $t_p->setProduct_price(formatMoney(14));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-07-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
//                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("13沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*13-00");
//                    $t_p->setProduct_count(2);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(7);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*13-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                }
//                if (!isset($wujinMap['W02-01-3*25-00']) || empty($wujinMap['W02-01-3*25-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("25沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*25-00");
//                    $t_p->setProduct_count(4);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(10);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*25-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                }
            }elseif($p->getProduct_type() == 'ZZ'){
                foreach ($p->getCom_products() as $c_p){
                    $c_p = change2ProductBean($c_p);
                    if ($c_p->getProduct_type() == 'ZB') {
                        if (!isset($wujinMap['W01-02-XX-00']) || empty($wujinMap['W01-02-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("背板连接件");
                            $t_p->setProduct_no("W01-02-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setProduct_price(formatMoney(7));
                            $t_p->setSeq(2);
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-02-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }

                        if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("13沉头自攻钉");
                            $t_p->setProduct_no("W02-01-3*13-00");
                            $t_p->setProduct_count(4 * 3);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(7);
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W02-01-3*13-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                        }
                    } elseif ($c_p->getProduct_type() == 'ZH') {
                        if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("层板连接件");
                            $t_p->setProduct_no("W01-01-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(1);
                            $t_p->setProduct_price(formatMoney(6));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-01-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }
                    } elseif ($c_p->getProduct_type() == 'ZH-YTG') {
                        if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("层板连接件");
                            $t_p->setProduct_no("W01-01-XX-00");
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(1);
                            $t_p->setProduct_price(formatMoney(6));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-01-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }
                        $p_no = "W06-01-" . ($p->getProduct_width() - 12) . "-00";
                        if (!isset($wujinMap[$p_no]) || empty($wujinMap[$p_no])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通杆");
                            $t_p->setProduct_width($p->getProduct_width() - 12);
                            $t_p->setProduct_no($p_no);
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("支");
                            $t_p->setSeq($p->getProduct_width());
                            $t_p->setProduct_spec(($p->getProduct_width() - 12) . "mm");
                            $t_p->setProduct_price(formatMoney(
                                100 * ((getLength($p->getProduct_width() - 12)) / 1000)
                            ));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap[$p_no];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                        if (!isset($wujinMap['W04-07-XX-00']) || empty($wujinMap['W04-07-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通托");
                            $t_p->setProduct_no("W04-07-XX-00");
                            $t_p->setProduct_count(2);
                            $t_p->setProduct_unit("个");
                            $t_p->setSeq(1200);
                            $t_p->setProduct_price(formatMoney(7));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W04-07-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 2);
                        }
//                        if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name("13沉头自攻钉");
//                            $t_p->setProduct_no("W02-01-3*13-00");
//                            $t_p->setProduct_count(2);
//                            $t_p->setProduct_unit("个");
//                            $t_p->setSeq(7);
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['W02-01-3*13-00'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                        }
//                        if (!isset($wujinMap['W02-01-3*25-00']) || empty($wujinMap['W02-01-3*25-00'])) {
//                            $t_p = new ProductBean();
//                            $t_p->setProduct_name("25沉头自攻钉");
//                            $t_p->setProduct_no("W02-01-3*25-00");
//                            $t_p->setProduct_count(4);
//                            $t_p->setProduct_unit("个");
//                            $t_p->setSeq(10);
//                            $wujinMap[$t_p->getProduct_no()] = $t_p;
//                        } else {
//                            $t_p = $wujinMap['W02-01-3*25-00'];
//                            $t_p = change2ProductBean($t_p);
//                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                        }
                    }
                }
            }


            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (is_starts_with($c_p->getProduct_no(), "L-")
                        || is_starts_with($c_p->getProduct_no(), "LB-")
                    ) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    } elseif (is_starts_with($c_p->getProduct_no(), "K-")) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    }

                    //处理组合内的拉篮和裤抽 Near
                    if ($c_p->getProduct_type() == 'ZH') {
                        if (!empty($c_p->getCom_products())) {
                            foreach ($c_p->getCom_products() as $c_p_c) {
                                $c_p_c = change2ProductBean($c_p_c);
                                if (is_starts_with($c_p_c->getProduct_no(), "L-")
                                    || is_starts_with($c_p_c->getProduct_no(), "LB-")
                                ) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                } elseif (is_starts_with($c_p_c->getProduct_no(), "K-")) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        $wujinProducts = array_values($wujinMap);

        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($wujinProducts as $w_p) {
            $w_p = change2ProductBean($w_p);
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
        // Collections.sort(wujin_products, new ProductRptSort_Seq()); // todo:
        $scheme->setScheme_score_wujin_products($wujinProducts);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            $scheme->getScheme_dis_plate_price()
            + $scheme->getScheme_dis_wujin_price()
            + $scheme->getScheme_dis_zj_price()
        ));
        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));

        return $scheme;
    }

    public function scoreRQSSchemeDoor(SchemeBean $scheme)
    {
        $doorCount = $scheme->getScheme_door_count();
        if ($doorCount == 0) {
            return $scheme;
        }

        $doorHeight = $scheme->getScheme_door_height();
        $doorWidth = $scheme->getScheme_door_width_one();

        $gridCount = 0;
        $gridHeight_300 = 0;

        $oneDoorArea = number_format(getLength($doorWidth) * getLength($doorHeight) / 1000000, 2, '.', '');
        $mod = ($doorHeight - 72) % 300;
        $gridHeight_300 = $gridCount = intval(($doorHeight - 72) / 300);

        if ($mod == 0) {

        } elseif ($mod >= 100) {
            $gridCount++;
        } else {
            $gridCount++;
            $gridHeight_300--;
        }

        $d_products = [];
        $temp = 0; // 门口宽

        if (!$scheme->getScheme_sk_color_no() != '-1') {
            $temp = $scheme->getScheme_hole_width() - (2 * CUSTOM_SHOUKOU_IN_OUT);
            if ($scheme->getIs_left_wall() || $scheme->getIs_left_wall_zt()) {
                $temp -= CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall() || $scheme->getIs_right_wall_zt()) {
                $temp -= CUSTOM_PADDING;
            }
        } else {
            $temp = $scheme->getScheme_hole_width();
        }
        $d_p = new ProductBean();
        $d_p->setProduct_name("移门边料");
        $d_p->setProduct_no("W06-15-" . $doorHeight . "-00");
        $d_p->setProduct_height(DOOR_FRAME_WIDTH);
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count(2 * $doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-01");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下横");
        $d_p->setProduct_no("W06-16-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-02");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("上滑轨");
        $d_p->setProduct_no("W06-18-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("下滑轨");
        $d_p->setProduct_no("W06-19-" . strval($temp) . "-00");
        $d_p->setProduct_width($temp);
        $d_p->setProduct_count(1);
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($temp));
        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("工字铝");
        $d_p->setProduct_no("W06-17-" . strval($doorWidth - 2 * DOOR_FRAME_WIDTH) . "-00");
        $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH);
        $d_p->setProduct_count($doorCount * ($gridCount - 1));
        $d_p->setProduct_unit("支");
        $d_p->setProduct_spec(strval($doorWidth - 2 * DOOR_FRAME_WIDTH));
        $d_products[] = $d_p;

        if ($gridHeight_300 == $gridCount) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * $gridCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;
        } elseif ($gridHeight_300 == $gridCount - 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 1));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*{$mod}*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height($mod);
            $d_p->setProduct_count($doorCount);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*{$mod}*9");
            $d_products[] = $d_p;
        } else {
            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(299);
            $d_p->setProduct_count($doorCount * ($gridCount - 2));
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*299*9");
            $d_products[] = $d_p;

            $d_p = new ProductBean();
            $d_p->setProduct_name("门芯板");
            $d_p->setProduct_color_no($scheme->getScheme_door_color_no());
            $d_p->setProduct_no("W06-20-" . ($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                . "*9-00-" . $scheme->getScheme_door_color_no());
            $d_p->setProduct_deep(9);
            $d_p->setProduct_width($doorWidth - 2 * DOOR_FRAME_WIDTH + 18);
            $d_p->setProduct_height(intval(($mod + 300) / 2));
            $d_p->setProduct_count($doorCount * 2);
            $d_p->setProduct_unit("块");
            $d_p->setProduct_spec(($doorWidth - 2 * DOOR_FRAME_WIDTH + 18) . "*"
                . intval(($mod + 300) / 2)
                . "*9");
            $d_products[] = $d_p;
        }

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("圆头不锈钢自攻钉");
//        $d_p->setProduct_no("W02-02-3*12-01");
//        $d_p->setProduct_count(2 * $doorCount * $gridCount);
//        $d_p->setProduct_unit("个");
//        $d_p->setProduct_spec("4*10");
//        $d_products[] = $d_p;

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("移门上下滑轮");
//        $d_p->setProduct_no("W04-19-XX-00");
//        $d_p->setProduct_count($doorCount);
//        $d_p->setProduct_unit("套");
//        $d_products[] = $d_p;

//        $d_p = new ProductBean();
//        $d_p->setProduct_name("双面胶");
//        $d_p->setProduct_no("W04-38-XX-00");
//        $d_p->setProduct_count(1);
//        $d_p->setProduct_unit("条");
//        $d_p->setProduct_spec("适量");
//        $d_products[] = $d_p;

        $d_p = new ProductBean();
        $d_p->setProduct_name("缓冲条");
        $d_p->setProduct_no("W06-23-" . $doorHeight . "-00");
        $d_p->setProduct_width($doorHeight);
        $d_p->setProduct_count($doorCount * 2);
        $d_p->setProduct_unit("条");
        $d_p->setProduct_spec(strval($doorHeight));
        $d_products[] = $d_p;

        $scheme->setScheme_door_spec($doorWidth . "*" . $doorHeight);
        $scheme->setScheme_s_door_area($oneDoorArea);
        $scheme->setScheme_door_has_count($doorCount);
        $scheme->setScheme_door_area($oneDoorArea * $doorCount);
        $scheme->setScheme_o_door_price(formatMoney(719));
        $scheme->setScheme_s_door_price(formatMoney(719 * $scheme->getScheme_discount()));
        $scheme->setScheme_dis_door_price(formatMoney($scheme->getScheme_s_door_price() * $scheme->getScheme_door_area()));
        $scheme->setScheme_door_price(formatMoney(719 * $scheme->getScheme_door_area()));

        if ($scheme->getScheme_door_haveHCQ() == 1 && $scheme->getScheme_have_door() == 1) {
            $d_p = new ProductBean();
            $d_p->setProduct_name("移门缓冲器");
            $d_p->setProduct_no("W04-20-XX-00");
            $d_p->setProduct_count(intval($doorCount/2));
            $d_p->setProduct_unit("套");
            $d_p->setProduct_price(formatMoney(199));
            $d_p->setProduct_dis_price(formatMoney($d_p->getProduct_price() * $scheme->getScheme_discount()));
            $d_p->setProduct_total_price(formatMoney($d_p->getProduct_price() * $d_p->getProduct_count()));
            $d_p->setProduct_dis_total_price(formatMoney($d_p->getProduct_dis_price() * $d_p->getProduct_count()));
            $scheme->setScheme_door_price(formatMoney($scheme->getScheme_door_price() + $d_p->getProduct_total_price()));
            $scheme->setScheme_dis_door_price(formatMoney($scheme->getScheme_dis_door_price() + $d_p->getProduct_dis_total_price()));
            $scheme->setScheme_door_mat_price(formatMoney($d_p->getProduct_total_price()));//todo near2018/09/15注释 不明用意 暂时恢复
            $d_products[] = $d_p;
        }
        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($d_products as $d_p) {
            $d_p = change2ProductBean($d_p);
            $d_p->setProduct_total_price(formatMoney($d_p->getProduct_price() * $d_p->getProduct_count()));
            $d_p->setProduct_dis_total_price(formatMoney($d_p->getProduct_dis_price() * $d_p->getProduct_count()));
            $totalPrice = $totalPrice + $d_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $d_p->getProduct_dis_total_price();
        }
        $scheme->setScheme_door_mat_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_door_mat_price(formatMoney($dis_totalPrice));

        $scheme->setScheme_score_door_products($d_products);
        return $scheme;
    }

    public function scoreRQSSchemeShouKou(SchemeBean $scheme)
    {
        $skColorNo = "";
        $skColorNo = ($scheme->getScheme_sk_color_no() == '000') ? "H" : "B";

        $totalPrice = 0;
        $dis_totalPrice = 0;

        if (empty($scheme->getScheme_dt_products()) && ($scheme->getScheme_sk_color_no() != '-1')) {
            $ttt = 0;
            $count = 0;// 垫料数量
            if ($scheme->getIs_left_wall()) {
                if ($scheme->getScheme_schemes()[0]->getIs_optimize()) {
                    $count++;
                } else {
                    $count += 2;
                }
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall()) {
                if ($scheme->getScheme_schemes()[count($scheme->getScheme_schemes()) - 1]->getIs_optimize()) {
                    $count++;
                } else {
                    $count += 2;
                }
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_left_wall_zt()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall_zt()) {
                $count++;
                $ttt += CUSTOM_PADDING;
            }
            $schemeDiscount = $scheme->getScheme_discount();
            $sk_products = [];
            $sk_p = null;

            // System.out.println(scheme.getScheme_sk_colorNo());
            // 洞口宽度-(左右基料+盖板厚度)-左右垫料厚度+ 20加工余量 "00/灰色01/白色"
            // W06-12-{尺寸}-{风格}-{花色}

            $shyjl = $scheme->getScheme_hole_width() - CUSTOM_SHOUKOU_IN_OUT * 2 - $ttt + 20;// 上缘基料长度

            // 洞口高度 + 20加工余量 W06-12-{尺寸}-{风格}-{花色}
            $zyjl = $scheme->getScheme_hole_sl_height() + 20;// 左右基料长度

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("洞口上缘基料");
            $sk_p->setProduct_no("W06-12-" . $shyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($shyjl);
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($shyjl . "mm");
            $sk_p->setProduct_price(formatMoney(59 * $shyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount, 2));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("洞口左右基料");
            $sk_p->setProduct_no("W06-12-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(59 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("左右基料盖板");
            // 洞口左右基料长度 W06-13-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-13-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(4);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(15 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("上缘外扣面板");
            // 洞口上缘基料长度 W06-14-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-14-" . $shyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($shyjl);
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($shyjl . "mm");
            $sk_p->setProduct_price(formatMoney(39 * $shyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("左右外扣面板");
            // 洞口左右基料长度 W06-14-{尺寸}-{风格}-{花色}
            $sk_p->setProduct_no("W06-14-" . $zyjl . "-00-" . $skColorNo);
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("支");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(39 * $zyjl / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("侧衔接塑胶条");
            // 洞口左右基料长度 W06-22-{尺寸}-{风格}
            $sk_p->setProduct_no("W06-22-" . $zyjl . "-00");
            $sk_p->setProduct_width($zyjl);
            $sk_p->setProduct_count(2);
            $sk_p->setProduct_unit("条");
            $sk_p->setProduct_spec($zyjl . "mm");
            $sk_p->setProduct_price(formatMoney(15 * $scheme->getScheme_hole_sl_height() / 1000, 2));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            if ($count != 0) {
                $sk_p = new ProductBean();
                $sk_p->setProduct_name("洞口侧向垫方");
                // W06-21-{尺寸}-{风格}
                $sk_p->setProduct_no("W06-21-35*78*" . $scheme->getScheme_hole_sl_height() . "-00");
                $sk_p->setProduct_width($scheme->getScheme_hole_sl_height());
                $sk_p->setProduct_count(4);
                $sk_p->setProduct_count($count);
                $sk_p->setProduct_unit("支");
                $sk_p->setProduct_spec("35*78*" . $scheme->getScheme_hole_sl_height());
                $sk_p->setProduct_price(formatMoney(26));
                $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
                $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
                $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
                $sk_products[] = $sk_p;
                $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
                $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();
            }

            $sk_p = new ProductBean();
            $sk_p->setProduct_name("现场施工费");
            $sk_p->setProduct_no("现场施工费-" . $scheme->getScheme_door_area());
            $sk_p->setProduct_count(1);
            $sk_p->setProduct_unit("");
            $sk_p->setProduct_price(formatMoney(56 * $scheme->getScheme_door_area()));
            $sk_p->setProduct_dis_price(formatMoney($sk_p->getProduct_price() * $schemeDiscount));
            $sk_p->setProduct_total_price(formatMoney($sk_p->getProduct_price() * $sk_p->getProduct_count()));
            $sk_p->setProduct_dis_total_price(formatMoney($sk_p->getProduct_dis_price() * $sk_p->getProduct_count()));
            $sk_products[] = $sk_p;
            $totalPrice = $totalPrice + $sk_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $sk_p->getProduct_dis_total_price();

            $scheme->setScheme_score_sk_products($sk_products);
            $scheme->setScheme_sk_price(formatMoney($totalPrice));
            $scheme->setScheme_dis_sk_Price(formatMoney($dis_totalPrice));
        }
        $scheme->setScheme_door_and_sk_price(formatMoney($scheme->getScheme_door_price() + $scheme->getScheme_sk_price()));
        $scheme->setScheme_dis_door_and_sk_price(formatMoney(
            $scheme->getScheme_dis_door_price() + $scheme->getScheme_dis_sk_price()
        ));

        // 计算总价格
        $scheme->setScheme_price($scheme->getScheme_price() + $scheme->getScheme_gt_price());
        $scheme->setScheme_price($scheme->getScheme_price() + $scheme->getScheme_door_price());
        $scheme->setScheme_price($scheme->getScheme_price() + $scheme->getScheme_sk_price());

//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_gt_price());
//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_door_price());
//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_sk_price());

        $scheme->setScheme_dis_price(getMoney($scheme->getScheme_price() * $scheme->getScheme_discount()));

        return $scheme;
    }

    private function countProductMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price($price);
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_count())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_count())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }

    private function countPlateMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price(formatMoney($price));
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_area())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_area())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }
}
