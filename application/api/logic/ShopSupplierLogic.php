<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/26
 * Time: 14:45
 */

namespace app\api\logic;

use app\common\model\ShopSupplier as ShopSupplierModel;

class ShopSupplierLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月26日
     * description:推荐品牌
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function recommend($limit = 6)
    {
        return ShopSupplierModel::build()
            ->where(['is_deleted' => 0])
            ->field(['supplier_id', 'name', 'logo_src'])
            ->orderRaw("rand()")
            ->limit($limit)
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月26日
     * description:用户买过的品牌
     * @param int $user_id
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function bought($user_id, $limit = 6)
    {
        $ShopSupplierModel = new ShopSupplierModel;
        return $ShopSupplierModel
            ->alias('s')
            ->join('yd_shop_order o', 'o.supplier_id = s.supplier_id')
            ->where(['o.user_id' => $user_id, 'o.status' => ['in', ['已完成', '待评价', '待收货', '待发货', '已退款']],'s.is_deleted' => 0])
            ->field(['s.supplier_id', 's.name', 's.logo_src'])
            ->orderRaw("rand()")
            ->group('s.supplier_id')
            ->limit($limit)
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:根据分类获取对应品牌
     * @param $classify_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function classify($classify_id)
    {
        return ShopSupplierModel::build()->alias('s')
            ->join('yd_shop_product p',
                "p.supplier_id = s.supplier_id AND p.classify_one_id = {$classify_id}")
            ->where(['s.is_deleted' => 0])
            ->field(['s.supplier_id', 's.name'])
            ->group('s.supplier_id')
            ->select();
    }

}