<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:26
 */

namespace app\api\logic;


use app\common\model\Config as ConfigModel;

class ConfigLogic
{
    /**
     * @author: Airon
     * @time: 2017年8月2日
     * description:获取教程模块推荐
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function Tutorial()
    {
        return ConfigModel::build()->where(['key' => 'tutorial_recommend'])->find();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取PC商城首页推荐
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function Store()
    {
        return ConfigModel::build()->where(['key' => 'store_recommend'])->find();
    }
}