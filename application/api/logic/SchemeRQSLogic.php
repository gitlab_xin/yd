<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\CustomizedColor;
use app\common\model\CustomizedScheme;
use app\common\model\SchemeRQSBean as SchemeBean;
use app\common\tools\CommonMethod;

class SchemeRQSLogic extends SchemeBaseLogic
{
    public $productLogic = null;

    public function getOriginParams($scheme)
    {
        $params = [];

        $params['b_width'] = isset($scheme['scheme_hole_left']) ? $scheme['scheme_hole_left'] : -1;
        $params['c_width'] = isset($scheme['scheme_hole_right']) ? $scheme['scheme_hole_right'] : -1;
        $params['hole_deep'] = isset($scheme['scheme_hole_deep']) ? $scheme['scheme_hole_deep'] : 0;
        $params['hole_height'] = isset($scheme['scheme_hole_height']) ? $scheme['scheme_hole_height'] : 0;
        $params['sl_height'] = isset($scheme['scheme_hole_sl_height']) ? $scheme['scheme_hole_sl_height'] : null;
        $params['hole_width'] = isset($scheme['scheme_hole_width']) ? $scheme['scheme_hole_width'] : 0;
        $params['hole_type'] = isset($scheme['scheme_hole_type']) ? $scheme['scheme_hole_type'] : 0;
        $params['sk_color_no'] = isset($scheme['scheme_sk_color_no']) ? $scheme['scheme_sk_color_no'] : null;
        $params['door_count'] = isset($scheme['scheme_door_count']) ? $scheme['scheme_door_count'] : null;
        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';

        return $params;
    }

    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_type' => 'RQS',
            'scheme_type_text' => '入墙式移门壁柜',
            'scheme_s_type' => -1,
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 590,
            'scheme_door_count' => $schemeData['scheme_door_count'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => $schemeData['scheme_sk_color_no'],
            'scheme_sk' => $schemeData['scheme_sk_color_no'],
            'scheme_hole_type' => $schemeData['scheme_hole_type'],
            'scheme_hole_width' => $schemeData['scheme_hole_width'],
            'scheme_hole_height' => $schemeData['scheme_hole_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_hole_sl_height'],
            'scheme_hole_deep' => 590,
            'scheme_hole_left' => $schemeData['scheme_hole_left'],
            'scheme_hole_right' => $schemeData['scheme_hole_right'],
            'is_left_wall' => $schemeBean->getIs_left_wall() ? '1' : '0',
            'is_right_wall' => $schemeBean->getIs_right_wall() ? '1' : '0',
            'is_left_wall_zt' => $schemeBean->getIs_left_wall_zt() ? '1' : '0',
            'is_right_wall_zt' => $schemeBean->getIs_right_wall_zt() ? '1' : '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model['id'];
    }

    public function update(SchemeBean $schemeBean, $schemeData, $schemeId, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_door_count' => $schemeData['scheme_door_count'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => $schemeData['scheme_sk_color_no'],
            'scheme_sk' => $schemeData['scheme_sk_color_no'],
            'scheme_hole_type' => $schemeData['scheme_hole_type'],
            'scheme_hole_width' => $schemeData['scheme_hole_width'],
            'scheme_hole_height' => $schemeData['scheme_hole_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_hole_sl_height'],
            'scheme_hole_deep' => '590',
            'scheme_hole_left' => $schemeData['scheme_hole_left'],
            'scheme_hole_right' => $schemeData['scheme_hole_right'],
            'is_left_wall' => $schemeBean->getIs_left_wall() ? '1' : '0',
            'is_right_wall' => $schemeBean->getIs_right_wall() ? '1' : '0',
            'is_left_wall_zt' => $schemeBean->getIs_left_wall_zt() ? '1' : '0',
            'is_right_wall_zt' => $schemeBean->getIs_right_wall_zt() ? '1' : '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];
        $model = new CustomizedScheme();
        $model->where(['id' => $schemeId])->update($schemeModelData);
        return;
    }

    public function buildScheme(&$schemeData, $requestData = null)
    {
        if ($requestData !== null) {
            if ($requestData['door_color_id'] == 0) {//没有门
                $schemeData['scheme_door_color_no'] = '000';
                $schemeData['scheme_door_have_hcq'] = 0;
                $schemeData['scheme_door_count'] = 0;
            } else {
                $schemeData['scheme_door_color_no'] = CustomizedColor::build()->where(['color_id' => $requestData['door_color_id'], 'is_door' => 1, 'is_deleted' => 0])->value('color_no');
                $schemeData['scheme_door_have_hcq'] = $requestData['scheme_door_have_hcq'];
            }
        }

        $scheme = SchemeBean::build($schemeData);

        if ($schemeData['scheme_door_count'] > 0) {
            $this->addZDYYGDoor($scheme);
            $schemeData['scheme_door_width'] = $scheme->getScheme_door_width();
            $schemeData['scheme_door_width_one'] = $scheme->getScheme_door_width_one();
            $schemeData['scheme_door_height'] = $scheme->getScheme_door_height();
            $schemeData['scheme_door_schemes'] = $scheme->getScheme_door_schemes_array();
        }

        return $scheme;
    }

    public function checkScheme(SchemeBean $schemeBean)
    {
        $colsList = $schemeBean->getScheme_schemes();
        $schemeHeight = $schemeBean->getScheme_height();

        $colsCount = count($colsList);
        $pointsCount = intval(($schemeHeight - 35) / 32) + 1;
        $checkPointData = $this->createCheckPointData($colsCount, $pointsCount);

        $currentCol = 0;
        $checkResult = true;

        foreach ($colsList as $colScheme) {
            $colScheme = change2SchemeBean($colScheme);
            $schemeProductsList = $colScheme->getScheme_products();
            foreach ($schemeProductsList as $productBean) {
                $productBean = change2ProductBean($productBean);
                // 检查横隔板
                if ($productBean->getProduct_type() == 'ZH') {
                    $checkResult = $this->getProductLogic()
                        ->checkZDYYG_HGB($productBean, $schemeHeight, $currentCol, $checkPointData);
                    if ($checkResult !== true) {
                        break;
                    }
                }
                if ($productBean->getProduct_type() == 'ZH-YTG') {
                    $checkResult = $this->getProductLogic()
                        ->checkZDYYG_HGB_YTG($productBean, $schemeHeight, $currentCol, $checkPointData);
                    if ($checkResult !== true) {
                        break;
                    }
                }
                if (!empty($productBean->getCom_products())) {
                    foreach ($productBean->getCom_products() as $comProduct) {
                        $comProduct = change2ProductBean($comProduct);
                        if ($comProduct->getProduct_type() == 'L00') {
                            $checkResult = $this->getProductLogic()
                                ->checkZDYYG_LL($productBean, $schemeHeight, $currentCol, $checkPointData);
                            if ($checkResult !== true) {
                                break;
                            }
                        }
                        if ($comProduct->getProduct_type() == 'K') {
                            $checkResult = $this->getProductLogic()
                                ->checkZDYYG_KC($productBean, $schemeHeight, $currentCol, $checkPointData);
                            if ($checkResult !== true) {
                                break;
                            }
                        }
                    }
                }
            }
            if ($checkResult !== true) {
                break;
            }
            $currentCol++;
        }

        return $checkResult;
    }

    /**
     * @param $data
     * @return array
     */
    public function createQRSScheme($data)
    {

        $index = base64_encode(http_build_query($data));
        $productList = $this->beforeCreate('rqs', $index);
        if (!empty($productList)) {
            return $productList;
        }

        $scheme = new SchemeBean();
        $scheme->setScheme_hole_width($data['hole_width']);
        $scheme->setScheme_hole_height($data['hole_height']);
        $scheme->setScheme_hole_sl_height($data['sl_height']);
        $scheme->setScheme_type('RQS');
        $scheme->setScheme_hole_deep(590);
        $scheme->setScheme_hole_left($data['b_width']);
        $scheme->setScheme_hole_right($data['c_width']);
        $scheme->setScheme_door_count($data['door_count']);
        $scheme->setScheme_door_count_init($data['door_count']);
        $scheme->setScheme_sk_color_no($data['sk_color_no']);
        $scheme->setScheme_width($data['hole_width'] - $data['b_width'] - $data['c_width']);
        $scheme->setScheme_height($data['hole_height']);
        $scheme->setScheme_hole_type($data['hole_type']);
        $scheme->setSearch_index($index);

        $this->createBasicScheme($scheme);

        $productList = $scheme->getScheme_schemes();

        if (!empty($productList)) {
            array_walk($productList, function (&$row) use ($index) {
                $row->setSearch_index($index);
                $row = $row->mainInfo();
            });
            $this->afterCreate('rqs', $productList);
        }

        return $productList;
    }

    /**
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function createBasicScheme(SchemeBean $scheme)
    {
        $scheme->setScheme_color_no('000');
        if (600 > $scheme->getScheme_hole_deep() || 1400 > $scheme->getScheme_width()) {
            $scheme->setScheme_no_door(1);
        }
        $doorCount = $scheme->getScheme_door_count();
        $holeWidth = $scheme->getScheme_hole_width();
        if (0 == $doorCount) {
            if ($holeWidth >= 1310 && $holeWidth <= 3000) {
                $doorCount = 2;
            } elseif ($holeWidth >= 3001 && $holeWidth <= 4500) {
                $doorCount = 3;
            } elseif ($holeWidth >= 4501 && $holeWidth <= 5000) {
                $doorCount = 4;
            } elseif ($holeWidth >= 5000 && $holeWidth <= 6150) {
                $doorCount = 5;
            } else {
                $doorCount = 0;
            }
        } elseif (-1 == $doorCount) {
            $doorCount = 0;
        }
        $this->createRQSSchemeBlank2($scheme, $doorCount);
    }

    public function createRQSSchemeBlank2(SchemeBean $scheme, $doorCount)
    {
        $listFcws = [];

        $leftObmWidth = $scheme->getScheme_hole_left();
        $rightObmWidth = $scheme->getScheme_hole_right();
        $furnitureWidth = $scheme->getScheme_width();
        $holeDeep = $scheme->getScheme_hole_deep();
        $inputWidth = $scheme->getScheme_hole_width();
        $inputHeight = $scheme->getScheme_hole_height();
        $slHeight = $scheme->getScheme_hole_sl_height();
        $skColorNo = $scheme->getScheme_sk_color_no();
        $colorNo = $scheme->getScheme_color_no();

        $isLeftWall = $scheme->getIs_left_wall();
        $isRightWall = $scheme->getIs_right_wall();
        $isLeftWallZt = $scheme->getIs_left_wall_zt();
        $isRightWallZt = $scheme->getIs_right_wall_zt();

        if (!empty($isLeftWall) && $skColorNo != -1) {
            $furnitureWidth -= CUSTOM_PADDING; // 减去收口垫方宽度
        }
        if (!empty($isRightWall) && $skColorNo != -1) {
            $furnitureWidth -= CUSTOM_PADDING;
        }
        if (!empty($isLeftWallZt) && $skColorNo != -1) {
            if ($leftObmWidth < CUSTOM_PADDING) {
                $furnitureWidth -= (CUSTOM_PADDING - $leftObmWidth);
            }
        }
        if (!empty($isRightWallZt) && $skColorNo != -1) {
            if ($rightObmWidth < CUSTOM_PADDING) {
                $furnitureWidth -= (CUSTOM_PADDING - $rightObmWidth);
            }
        }

        //进行优化方案
        if ($furnitureWidth < 432 && $skColorNo != -1) {
            if ($isLeftWall) {
                $furnitureWidth += CUSTOM_PADDING;
            }
            if ($isRightWall) {
                $furnitureWidth += CUSTOM_PADDING;
            }
            if ($furnitureWidth < 432 && ($isLeftWallZt || $isRightWallZt)) {
                $furnitureWidth = $inputWidth - $leftObmWidth - $rightObmWidth;
            }
            $scheme->setIs_optimize(1);
        }

        // 板宽度方案列表
        $listWidthResult = $this->getFurnitureProjects(
            $inputWidth,
            $leftObmWidth,
            $furnitureWidth,
            $rightObmWidth,
            $isLeftWall || $isLeftWallZt,
            $isRightWall || $isRightWallZt,
            $skColorNo,
            $doorCount
        );

        if (empty($listWidthResult)) {
            return null;
        }

        // 准备生成方案
        $height = $this->calFurnitureHeight($inputHeight);
        foreach ($listWidthResult as $item) {
            if (empty($item)) {
                continue;
            }
            // width：柜体真正宽度
            $ncbCount = count($item) - 3;
            $width = array_sum($item) + $ncbCount * INNER_WIDTH;

            if (abs($width - $furnitureWidth) > 32) {
                // 舍弃方案
                continue;
            }

            // 生成外侧板
            $wcbProducts = $this->calWcbProducts($item, $height, $width, $colorNo, $wcbType);
            // 计算柜体在洞的坐标
            list($fx, $fy) = $this->calFxAndFy($scheme, $height, $width);
            // 生成收口
            $skProducts = $this->calSkProducts($scheme, $fx, $fy);
            // 计算顶板下落多少、是否下落
            list($dHeight, $twoHgb) = $this->calDingBanMTop($slHeight, $inputHeight);
            // 计算列方案和内侧板
            list($colSchemes, $ncbProducts) =
                $this->calSchemeColsAndNcbProducts($item, $height, $twoHgb, $dHeight, $colorNo, $scheme->getIs_optimize());

            // 初始化方案
            $fcw = new SchemeBean();
            $fcw->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
            $fcw->setScheme_door_count($doorCount);
            $fcw->setM_left_mm($fx);
            $fcw->setM_top_mm($fy);
            $fcw->setM_left(intval($fx / 8));
            $fcw->setM_top(intval($fy / 8));
            $fcw->setScheme_width($width);
            $fcw->setScheme_height($height);
            $fcw->setScheme_hole_width($inputWidth);
            $fcw->setScheme_hole_height($inputHeight);
            $fcw->setScheme_hole_deep($holeDeep);
            $fcw->setScheme_hole_type($scheme->getScheme_hole_type());
            $fcw->setScheme_hole_left($scheme->getScheme_hole_left());
            $fcw->setScheme_hole_right($scheme->getScheme_hole_right());
            $fcw->setScheme_hole_sl_height($scheme->getScheme_hole_sl_height());
            $fcw->setScheme_wcb_type($wcbType);
            $fcw->setScheme_s_width($width);
            $fcw->setScheme_s_height($height);
            $fcw->setScheme_wcb_products($wcbProducts);
            $fcw->setScheme_ncb_products($ncbProducts);
            $fcw->setScheme_sk_products($skProducts);
            $fcw->setScheme_sk_color_no($skColorNo);
            $fcw->setScheme_color_no($colorNo);
            $fcw->setScheme_type($scheme->getScheme_type());
            $fcw->setScheme_error_range(32);
            $fcw->setScheme_ncb_products($ncbProducts);
            $fcw->setScheme_schemes($colSchemes);
            $fcw->setScheme_door_count($doorCount);
            $fcw->setScheme_door_count_init($doorCount);

            // 方案图
//            $s_pic = $fcw->getScheme_width()
//                . "_" . $height
//                . "_" . $colorNo
//                . "_" . (count($item) - 2)
//                . "_" . $doorCount
//                . "_" . getRandomInt();
            $this->createSchemePic($fcw);
            $fcw->setScheme_name("入墙式移门壁柜-" . getRandomInt());
            // 单个方案生成完成
            $listFcws[] = $fcw;
        }

        $scheme->setScheme_schemes($listFcws);
    }

    protected function getFurnitureProjects(
        $holeWidth,
        $leftZTWidth,
        $GTWidth,
        $rightZTWidth,
        $isLeftToWall,
        $isRightToWall,
        $shouKou,
        $doorCount
    )
    {
        $resultList = [];
        $colCountList = [];
        if ($GTWidth >= 3689) {
            $colCountList = [3, 4, 5, 6, 7, 8];
        } elseif ($GTWidth >= 2461 && $GTWidth <= 3706) {
            $colCountList = [2, 3, 4, 5, 6, 7];
        } elseif ($GTWidth >= 1985 && $GTWidth <= 2478) {
            $colCountList = [2, 3, 4, 5];
        } elseif ($GTWidth >= 1648 && $GTWidth <= 2002) {
            $colCountList = [2, 3, 4];
        } elseif ($GTWidth >= 1251 && $GTWidth <= 1665) {
            $colCountList = [2, 3];
        } elseif ($GTWidth >= 1192 && $GTWidth <= 1268) {
            $colCountList = [1, 2];
        } elseif ($GTWidth >= 796 && $GTWidth <= 1209) {
            $colCountList = [1, 2];
        } elseif ($GTWidth >= 400 && $GTWidth <= 813) {
            $colCountList = [1];
        }
        foreach ($colCountList as $colCount) {
            $resultList[] = $this->getDoorProject(
                $holeWidth,
                $leftZTWidth,
                $GTWidth,
                $rightZTWidth,
                $colCount,
                $isLeftToWall,
                $isRightToWall,
                $shouKou,
                $doorCount
            );
        }
        return $resultList;
    }

    /**
     * 计算板材宽度
     * @param $holeWidth
     * @param $leftZTWidth
     * @param $GTWidth
     * @param $rightZTWidth
     * @param $colCount
     * @param $isLeftToWall
     * @param $isRightToWall
     * @param $shouKou
     * @param $doorCount
     * @return array|null
     */
    protected function getDoorProject(
        $holeWidth,
        $leftZTWidth,
        $GTWidth,
        $rightZTWidth,
        $colCount,
        $isLeftToWall,
        $isRightToWall,
        $shouKou,
        $doorCount
    )
    {
        if ($doorCount == 0) {
            return $this->getWidths($colCount, $GTWidth);
        }

        if ($colCount < $doorCount) {
            return null;
        }

        $doorFrameWidth = 30; // 门框宽度
        $leftBoard = 16; // 左外侧板宽度
        $rightBoard = 16; // 右外侧版宽度
        $doorPosition = []; // 门框中心位置
        $doorTemp = 0; // 收口垫料
        $doorX = 0; // 门起初位置
        $furnitureX = 0; //
        $furnitureTemp = 0; // 柜体垫料
        if ($shouKou == '000' || $shouKou == '004') {
            $doorTemp += CUSTOM_SHOUKOU_IN_OUT * 2;
            $doorX += CUSTOM_SHOUKOU_IN_OUT;
            if ($isLeftToWall) {
                if ($leftZTWidth < CUSTOM_PADDING) {
                    $furnitureTemp += CUSTOM_PADDING - $leftZTWidth;
                }
                $doorTemp += CUSTOM_PADDING;
                $doorX += CUSTOM_PADDING;
                $furnitureX += CUSTOM_PADDING;
            }
            if ($isRightToWall) {
                if ($rightZTWidth < CUSTOM_PADDING) {
                    $furnitureTemp += CUSTOM_PADDING - $rightZTWidth;
                }
                $doorTemp += CUSTOM_PADDING;
            }
        }
        $furnitureX += $leftZTWidth;
        $furnitureX += intval(($holeWidth - $furnitureTemp - $leftZTWidth - $rightZTWidth - $GTWidth) / 2);
        for ($index = 0; $index < $doorCount - 1; $index++) {
            $doorPosition[$index] = $doorX
                + intval(($holeWidth - $doorTemp - ($doorCount + 1) * $doorFrameWidth) / $doorCount * ($index + 1))
                + ($index + 1) * $doorFrameWidth
                + $doorFrameWidth / 2; // 门框中心位置
        }

        $furnitureWidthHope = $GTWidth;

        $widthLists = [];
        for ($index = 0; $index < $doorCount; $index++) {
            $from = intval($colCount / $doorCount * $index);
            $to = intval($colCount / $doorCount * ($index + 1));
            $times = $to - $from;
            $widthLists[$index] = array_fill(0, $times, CUSTOM_COLUMN_MIN_WIDTH);
        }

        for ($index = 0; $index < $doorCount; $index++) {
            $canAdd = true;
            $index2 = 0;
            // 期望家具最大宽度>当前家具宽度+32
            // 先把第一组加到门框位置
            while ($furnitureWidthHope >= $this->currentWidth($leftBoard, $rightBoard, $widthLists, $colCount)) {
                if (!$canAdd) {
                    break;
                }
                // 第一组先从第一列开始加，大于1200后，下一列开始加
                if ($index2 < count($widthLists[$index])) {
                    if ($widthLists[$index][$index2] < CUSTOM_COLUMN_MAX_WIDTH) {
                        $widthLists[$index][$index2] += CUSTOM_COLUMN_WIDTH_STEP;
                    } else {
                        $index2 += 1;
                    }
                } else {
                    // 第一组所有列都1200后仍没有达到门框位置
                    $canAdd = false;
                }
                if ($index != ($doorCount - 1)) {
                    // 第一组位置与门框位置判断
                    if ($this->currentWidth2($furnitureX, $leftBoard, $widthLists, $index) >
                        $doorPosition[$index] - intval($doorFrameWidth / 2) + DRAW_SPACE
                    ) {
                        $canAdd = false;
                    }
                }
            }
        }
        for ($index = $doorCount - 2; $index >= 0; $index--) {
            $canAdd = true;
            $index2 = 0;
            while ($furnitureWidthHope >= $this->currentWidth($leftBoard, $rightBoard, $widthLists, $colCount)) {
                if (!$canAdd) {
                    break;
                }
                if ($index2 < count($widthLists[$index])) {
                    if ($widthLists[$index][$index2] < CUSTOM_COLUMN_MAX_WIDTH) {
                        $widthLists[$index][$index2] += CUSTOM_COLUMN_WIDTH_STEP;
                    } else {
                        $index2 += 1;
                    }
                } else {
                    $canAdd = false;
                }
            }
        }
        $furnitureWidth = $this->furnitureWidth($leftBoard, $rightBoard, $widthLists, $colCount);
        if ($furnitureWidthHope - $furnitureWidth >= 18) {
            $leftBoard = 25;
            $rightBoard = 25;
        } elseif ($furnitureWidthHope - $furnitureWidth >= 9) {
            $leftBoard = 25;
        }

        $resultList = [];
        $resultList[] = $leftBoard;
        $resultList = array_merge($resultList, $widthLists[0]);

        for ($index = 1; $index < $doorCount - 1; $index++) {
            sort($widthLists[$index]);
            $resultList = array_merge($resultList, $widthLists[$index]);
        }

        $resultList = array_merge($resultList, array_reverse($widthLists[$doorCount - 1]));
        $resultList[] = $rightBoard;
        if ($furnitureWidthHope - array_sum($resultList) - ($colCount - 1) * INNER_WIDTH >= 32
            || $furnitureWidthHope - array_sum($resultList) - ($colCount - 1) * INNER_WIDTH < 0
        ) {
            // 期望家具最大宽度与当前家具误差
            return null;
        }
        if ($doorCount == 2) {
            if (abs(array_sum($widthLists[0]) + $leftZTWidth - array_sum($widthLists[1]) - $rightZTWidth) > 137) {
//                 第一组与第二组误差
                return null;
            }
        }

        return $resultList;
    }

    protected function sum($widthList, $count)
    {
        $result = 0;
        for ($i = 0; $i < $count; $i++) {
            $result += array_sum($widthList[$i]);
        }
        return $result;
    }

    protected function currentWidth($leftBoard, $rightBoard, $widthLists, $colCount)
    {
        return $leftBoard
            + $rightBoard
            + $this->sum($widthLists, count($widthLists))
            + ($colCount - 1) * INNER_WIDTH + CUSTOM_COLUMN_WIDTH_STEP;
    }

    protected function currentWidth2($furnitureX, $leftBoard, $widthLists, $currentCol)
    {
        $count = 0;
        for ($i = 0; $i < $currentCol + 1; $i++) {
            $count += count($widthLists[$i]);
        }
        return $furnitureX + $leftBoard
            + $this->sum($widthLists, $currentCol + 1)
            + ($count - 1) * INNER_WIDTH + CUSTOM_COLUMN_WIDTH_STEP;
    }

    protected function furnitureWidth($leftBoard, $rightBoard, $widthLists, $colCount)
    {
        return $leftBoard
            + $rightBoard
            + $this->sum($widthLists, count($widthLists))
            + ($colCount - 1) * INNER_WIDTH;
    }

    /**
     * 无门方案板材宽度
     * @param $colCount
     * @param $holeWidth
     * @return array|null
     */
    protected function getWidths($colCount, $holeWidth)
    {
        $side1 = SIDE_WIDTH_1;
        $side2 = SIDE_WIDTH_2;
        $inner = INNER_WIDTH;
        $defaultWidth = CUSTOM_KEY_WIDTH_784;
        $array = []; // size colCount + 2
        $arrayLength = $colCount + 2;

        $array[0] = $side1; // 左侧板
        for ($index = 1; $index < $arrayLength; $index++) { // 列宽
            $array[$index] = $defaultWidth;
        }
        $array[$arrayLength - 1] = $side1; // 右侧板

        $width = array_sum($array) + $inner * ($colCount - 1);
        if ($width == $holeWidth) {
            $result = $array;
            return $result;
        }
        if ($width < $holeWidth) {
            $diffTimes32 = intval(($holeWidth - $width) / CUSTOM_COLUMN_WIDTH_STEP);
            for ($index = 1; $index < $arrayLength - 1; $index++) {
                $array[$index] += CUSTOM_COLUMN_WIDTH_STEP * $diffTimes32;
                if ($array[$index] <= CUSTOM_COLUMN_MAX_WIDTH) {
                    break;
                } else {
                    $diffTimes32 = intval(($array[$index] - CUSTOM_COLUMN_MAX_WIDTH) / CUSTOM_COLUMN_WIDTH_STEP);
                    $array[$index] = CUSTOM_COLUMN_MAX_WIDTH;
                    continue;
                }
            }
        } else {
            $diffTimes32 = intval(($width - $holeWidth) / CUSTOM_COLUMN_WIDTH_STEP);
            $temp = ($width - $holeWidth) % CUSTOM_COLUMN_WIDTH_STEP;
            if ($temp != 0) {
                $diffTimes32 = $diffTimes32 + 1;
                $temp = CUSTOM_COLUMN_WIDTH_STEP - $temp;
            }
            for ($index = 1; $index < $arrayLength - 1; $index++) {
                $array[$index] -= CUSTOM_COLUMN_WIDTH_STEP * $diffTimes32;
                if ($array[$index] >= CUSTOM_COLUMN_MIN_WIDTH) {
                    $diffTimes32 = 0;
                    break;
                } else {
                    $diffTimes32 = intval((CUSTOM_COLUMN_MAX_WIDTH - $array[$index]) / CUSTOM_COLUMN_WIDTH_STEP);
                    $array[$index] = CUSTOM_COLUMN_MIN_WIDTH;
                    continue;
                }
            }
            if ($diffTimes32 > 0) {
                return null;
            }
        }
        $width = array_sum($array) + $inner * ($colCount - 1);
        $temp = ($holeWidth - $width) % CUSTOM_COLUMN_WIDTH_STEP;
        if ($temp >= 18) {
            $array[0] = $side2;
            $array[$arrayLength - 1] = $side2;
        } elseif ($temp >= 9) {
            $array[0] = $side2;
        }
        $result = $array;
        return $result;
    }

    /**
     * 根据洞高计算柜体高度
     * @param $inputHeight string 洞口高度
     * @return int 柜体高度
     */
    protected function calFurnitureHeight($inputHeight)
    {
        $iRemainder = ($inputHeight - 17 - CUSTOM_MIN_HEIGHT) % CUSTOM_COLUMN_WIDTH_STEP;
        $height = $inputHeight - 17 - $iRemainder;

        if ($height > BOARD_MAX_HEIGHT) {
            $height = BOARD_MAX_HEIGHT;
        }

        return $height;
    }

    /**
     * 生成外侧板
     * @param $widthList array 各个板的宽度集合
     * @param $height string 柜体高度
     * @param $width string 柜体宽度
     * @param $schemeColorNo string 柜体花色号码
     * @param $wcbType string 外侧板类型
     * @return array
     */
    protected function calWcbProducts($widthList, $height, $width, $schemeColorNo, &$wcbType)
    {
        $wcbProducts = [];
        $leftWcbWidth = $widthList[0];
        $rightWcbWidth = $widthList[count($widthList) - 1];
        // 左外侧板
        $wcbProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB(0, 0, 0, 0, $leftWcbWidth, $height, 490, $schemeColorNo, 0);
        // 右外侧板
        $wcbProducts[] = $this->getProductLogic()
            ->createZDYYG_WCB(0, 0, ($width - $rightWcbWidth), 0, $rightWcbWidth, $height, 490, $schemeColorNo, 0);
        // 获取外侧板类型，0:两面都是25，1:一面25一面16，2:两面都是16
        $wcbType = 0;
        if ($leftWcbWidth + $rightWcbWidth == 50) {
            $wcbType = 0;
        } elseif ($leftWcbWidth + $rightWcbWidth == 32) {
            $wcbType = 2;
        } else {
            $wcbType = 1;
        }

        return $wcbProducts;
    }

    /**
     * 计算柜体坐标, 相对于洞
     * @param SchemeBean $scheme
     * @param $height string 柜体实际高度
     * @param $width string 柜体实际宽度
     * @return array
     */
    protected function calFxAndFy(SchemeBean $scheme, $height, $width)
    {
        $leftObmWidth = $scheme->getScheme_hole_left();
        $rightObmWidth = $scheme->getScheme_hole_right();
        $inputWidth = $scheme->getScheme_hole_width();
        $inputHeight = $scheme->getScheme_hole_height();
        $isLeftWall = $scheme->getIs_left_wall();
        $isRightWall = $scheme->getIs_right_wall();
        $skColorNo = $scheme->getScheme_sk_color_no();

        $fx = 0;
        $fy = $inputHeight - $height;

        if ($leftObmWidth == 0 && $rightObmWidth == 0) {
            if ($skColorNo == '000' || $skColorNo == '004') {
                $furnitureTemp = 0;
                if ($isLeftWall) {
                    $furnitureTemp += CUSTOM_PADDING;
                    $fx = CUSTOM_PADDING;
                }
                if ($isRightWall) {
                    $furnitureTemp += CUSTOM_PADDING;
                }
                $fx = $fx + intval(($inputWidth - $width - $furnitureTemp) / 2);
            } else {
                $fx = intval(($inputWidth - $width) / 2);
            }
        } elseif ($leftObmWidth != 0 && $rightObmWidth == 0) {
            if ($skColorNo == '000' || $skColorNo == '004') {
                $furnitureTemp = 0;
                if ($isRightWall) {
                    $furnitureTemp += CUSTOM_PADDING;
                }
                $fx = $leftObmWidth + intval(($inputWidth - $leftObmWidth - $width - $furnitureTemp) / 2);
            } else {
                $fx = $leftObmWidth + intval(($inputWidth - $leftObmWidth - $width) / 2);
            }
        } elseif ($leftObmWidth == 0 && $rightObmWidth != 0) {
            if ($skColorNo == '000' || $skColorNo == '004') {
                $furnitureTemp = 0;
                if ($isLeftWall) {
                    $fx = CUSTOM_PADDING;
                    $furnitureTemp += CUSTOM_PADDING;
                }
                $fx = $fx + intval(($inputWidth - $rightObmWidth - $width - $furnitureTemp) / 2);
            } else {
                $fx = intval(($inputWidth - $rightObmWidth - $width) / 2);
            }
        } else {
            $fx = $leftObmWidth + intval(($inputWidth - $leftObmWidth - $rightObmWidth - $width) / 2);
        }

        return array($fx, $fy);
    }

    /**
     * 生成收口
     * @param SchemeBean $scheme
     * @param $fx string 柜体x坐标
     * @param $fy string 柜体y坐标
     * @return array
     */
    protected function calSkProducts(SchemeBean $scheme, $fx, $fy)
    {
        $inputWidth = $scheme->getScheme_hole_width();
        $inputHeight = $scheme->getScheme_hole_height();
        $slHeight = $scheme->getScheme_hole_sl_height();
        $isLeftWall = $scheme->getIs_left_wall();
        $isRightWall = $scheme->getIs_right_wall();
        $isLeftWallZt = $scheme->getIs_left_wall_zt();
        $isRightWallZt = $scheme->getIs_right_wall_zt();

        $skColorNo = $scheme->getScheme_sk_color_no();

        // 收口
        $skProducts = []; // 收口显示在方案层里面，位置相对方案
        if ($skColorNo == '000' || $skColorNo == '004') {
            $mTop = $inputHeight - $slHeight - $fy;
            // 左收口
            if ($isLeftWall || $isLeftWallZt) {
                $mLeft = 0 - $fx;
            } else {
                $mLeft = -34 - $fx;
            }
            $skProducts[] = $this->getProductLogic()->createZDYYG_SK($mLeft, $mTop, $slHeight, 57, $skColorNo, 0);

            // 右收口
            if ($isRightWall || $isRightWallZt) {
                $mLeft = $inputWidth - 57 - $fx;
            } else {
                $mLeft = $inputWidth - 23 - $fx;
            }
            $skProducts[] = $this->getProductLogic()->createZDYYG_SK($mLeft, $mTop, $slHeight, 57, $skColorNo, 0);

            // 上收口
            if (($isLeftWall || $isLeftWallZt) && ($isRightWall || $isRightWallZt)) {
                $mLeft = 57 - $fx;
                $skWidth = $inputWidth - 57 - 57;
            } elseif ($isLeftWall || $isLeftWallZt) {
                $mLeft = 57 - $fx;
                $skWidth = $inputWidth - 57 - 23;
            } elseif ($isRightWall || $isRightWallZt) {
                $mLeft = -34 + 57 - $fx;
                $skWidth = $inputWidth - 57 - 23;
            } else {
                $mLeft = 23 - $fx;
                $skWidth = $inputWidth - 23 - 23;
            }
            $skProducts[] = $this->getProductLogic()->createZDYYG_SK($mLeft, $mTop, 57, $skWidth, $skColorNo, 0);
        }
        return $skProducts;
    }

    /**
     * 计算顶板下落多少、是否下落
     * @param $slHeight string 上梁高度
     * @param $holeHeight string 洞高
     * @return array
     */
    protected function calDingBanMTop($slHeight, $holeHeight)
    {
        $dHeight = 0;
        $twoHgb = false; // 是否有顶板
        if ($slHeight == $holeHeight) {
            $twoHgb = true;
            if ($holeHeight > 2420) {
                $dHeight = $holeHeight - 2403 - 17;
                if ($dHeight >= 350) {
                    $dHeight = 0;
                } else {
                    $dHeight = 350 - $dHeight;
                    $dHeight = 32 * intval($dHeight / 32);
                }
            }
        } else {
            $dHeight = $holeHeight - 2403;
            if ($dHeight >= 446) {
                $dHeight = 0;
            } else {
                $dHeight = 446 - $dHeight;
                $dHeight = 32 * intval($dHeight / 32);
            }

            $dHeight += intval(($holeHeight - $slHeight) / 32) * 32;
            if ($slHeight > 2410) {
                $twoHgb = true;
            }
        }
        /*
         * 如果上梁高度 == 洞口高度：
         *      必定生成顶部层板 + 挂衣层板
         *      如果洞口高度 - 2420 >= 350时：
         *          顶板高度 = 0;
         *      其余情况：
         *          顶板高度 = (350 - (洞口高度 - 2420))/32取商 × 32
         * 如果上梁高度 < 洞口高度：
         *      上梁高度 > 2410 时，生成顶部层板 + 挂衣层板
         *
         *      如果洞口高度 - 2403 >= 446时：
         *          顶板高度 = (洞口高度 - 上梁高度)/32取商 × 32;
         *      其余情况：
         *          顶板高度 = (446 - (洞口高度 - 2403))/32取商 × 32 + (洞口高度 - 上梁高度)/32取商 × 32
         *
         */
        return array($dHeight, $twoHgb);
    }

    /**
     * 计算列方案和内侧板
     * @param $widthList
     * @param $height
     * @param $twoHgb
     * @param $dHeight
     * @param $schemeColorNo
     * @return array
     */
    protected function calSchemeColsAndNcbProducts($widthList, $height, $twoHgb, $dHeight, $schemeColorNo, $is_optimize)
    {
        $colSchemes = [];
        $ncbProducts = [];

        $colCount = count($widthList) - 2; // 列数
        $x = $widthList[0]; // 每列起始x坐标，第一列为左外侧板宽度

        for ($i = 1; $i <= $colCount; $i++) {
            $s_products = [];
            $col_width = $widthList[$i]; // 该列宽度

            // 背板
            if ($height >= 2403) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1201), 12, 1201, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1201, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2371) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1201), 12, 1201, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1169, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2339) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1169), 12, 1169, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1169, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2307) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1169), 12, 1137, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1169, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2275) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1137), 12, 1137, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1137, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2243) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1137), 12, 1105, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1137, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2211) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1105), 12, 1105, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1105, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2147) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 1073 + 8, 12, 1073, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 8, 12, 1073, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2115) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1073), 12, 1041, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1073, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2083) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1041), 12, 1041, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1041, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2051) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1041), 12, 1009, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1041, $col_width + 24, $schemeColorNo, 0);
            } elseif ($height >= 2019) {
                // 下背板 全板
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1009), 12, 1009, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, 1009, $col_width + 24, $schemeColorNo, 0);
            } else {
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, ($height - 1201), 12, 1201, $col_width + 24, $schemeColorNo, 0);
                // 上背板  裁切
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_BB(0, 0, 0, 0, 12, $height - 1201 - 1, $col_width + 24, $schemeColorNo, 0);
            }
            // 横隔板
            if ($twoHgb) {
                // 横隔板
                $rTop = $height - $dHeight;
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_HGB(0, $rTop, 0, $dHeight, 25, 490, $col_width, $schemeColorNo, 0, 0);
                // 横隔板+衣通杆
                $rTop = $height - 352 - $dHeight;
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_HGB_YTG(0, $rTop, 0, 352 + $dHeight, 25, 490, $col_width, $schemeColorNo, 3, 0);
            } else {
                // 横隔板+衣通杆
                $rTop = $height - $dHeight;
                $s_products[] = $this->getProductLogic()
                    ->createZDYYG_HGB_YTG(0, $rTop, 0, $dHeight, 25, 490, $col_width, $schemeColorNo, 0, 0);
            }
            // 底横隔板
            $s_products[] = $this->getProductLogic()
                ->createZDYYG_HGB(0, 25, 0, ($height - 25), 25, 490, $col_width, $schemeColorNo, 2, 0);

            // 初始化该列方案
            $colScheme = new SchemeBean();
            $colScheme->setScheme_width($col_width);
            $colScheme->setScheme_height($height);
            $colScheme->setIs_optimize($is_optimize);
            $colScheme->setScheme_color_no($schemeColorNo);
            $colScheme->setScheme_products($s_products);
            $colSchemes[] = $colScheme;

            $x += $col_width; // 推进至下一列

            // 内侧板
            if ($i != $colCount) {
                // 非最后一列，追加内侧板
                $ncbProducts[] = $this->getProductLogic()
                    ->createZDYYG_NCB(0, 0, $x, 0, 25, $height, 490, $schemeColorNo, 0);
                $x += 25;
            }
            // 单列添加完成
        }

        return array($colSchemes, $ncbProducts);
    }

    public function addZDYYGDoor(SchemeBean $schemeBean)
    {
        $holeWidth = $schemeBean->getScheme_hole_width();
        $holeSlHeight = $schemeBean->getScheme_hole_sl_height();
        $furnitureWidth = $schemeBean->getScheme_width();
        $furnitureHeight = $schemeBean->getScheme_height();
        $hasShouKou = (!empty($schemeBean->getScheme_sk_products()) && count($schemeBean->getScheme_sk_products()) > 0) ? true : false;


        $doorCount = $schemeBean->getScheme_door_count();

        $doorWidth = CommonMethod::getDoorWidth($holeWidth, $furnitureWidth, $doorCount, 30, true, $hasShouKou,
            $schemeBean->getIs_left_wall() || $schemeBean->getIs_left_wall_zt(),
            $schemeBean->getIs_right_wall() || $schemeBean->getIs_right_wall_zt()
        );
        $doorHeight = CommonMethod::getDoorHeight($holeSlHeight, $furnitureHeight, true, $hasShouKou);

//        $unitCount = intval($doorHeight / 300);
//        $unitType = 0;
//        $mod = $doorHeight % 300;
//        if ($mod >= 100) {
//            $unitType = 1;
//        }
        $doorHoleWidth = $holeWidth;
        if ($hasShouKou) {
            $doorHoleWidth -= CUSTOM_SHOUKOU_IN_OUT * 2;
            if ($schemeBean->getIs_left_wall() || $schemeBean->getIs_left_wall_zt()) {
                $doorHoleWidth -= CUSTOM_PADDING;
            }
            if ($schemeBean->getIs_right_wall() || $schemeBean->getIs_right_wall_zt()) {
                $doorHoleWidth -= CUSTOM_PADDING;
            }
        }

        $schemeBean->setScheme_door_width($doorHoleWidth);
        $schemeBean->setScheme_door_width_one($doorWidth);
        $schemeBean->setScheme_door_height($doorHeight);

        $s_schemes = [];
        $schemeBean->setScheme_door_has_count($doorCount);

        foreach (range(0, $doorCount - 1) as $i) {
            $s_scheme = new SchemeBean();
            $s_scheme->setScheme_width($doorWidth);
            $s_scheme->setScheme_height($doorHeight);
            $s_scheme->setScheme_s_width(intval($doorWidth / 8));
            $s_scheme->setScheme_s_height(intval($doorHeight / 8));
            $s_scheme->setScheme_color_no($schemeBean->getScheme_door_color_no());
            $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($schemeBean->getScheme_door_color_no()));
//            $pNo = $doorWidth . "-" . $doorCount . "-" . $unitType . $s_scheme->getScheme_color_no() . $i . "-" . getRandomInt();
//            ImageUtils.createYiDoor1(
//                path
//                + SchemeConstant.dcolor
//                + SchemeConstant.schemeColorArrH[Integer
//                .parseInt(s_scheme.getScheme_color_no())],
//                path + SchemeConstant.proPic + pNo + ".jpg",
//                s_scheme.getScheme_s_width(),
//                s_scheme.getScheme_s_height(), unitCount, mod);
//            $s_scheme->setScheme_pic($pNo . ".jpg");
            $s_schemes[] = $s_scheme;
        }
        $schemeBean->setScheme_door_schemes($s_schemes);
        $schemeBean->setScheme_have_door(1);
    }

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    protected function createCheckPointData($colsCount, $pointsCount)
    {
        $checkPointData = range(0, $colsCount - 1);
        foreach ($checkPointData as &$colPointData) {
            $colPointData = range(0, $pointsCount - 1);
            foreach ($colPointData as &$pointInfo) {
                $pointInfo = [
                    'is_use' => 0,
                    'can_use' => 1,
                ];
            }
        }

        return $checkPointData;
    }
}
