<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/11
 * Time: 14:59
 */

namespace app\api\logic;

use app\common\model\User as UserModel;
use app\common\model\UserBlackList;
use app\common\model\UserLabel;

class UserLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月11日
     * description:检查用户是否存在
     * @param string $mobile
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function check_user($mobile)
    {
        return UserModel::build()->field(['user_id', 'mobile'])->where(['mobile' => $mobile, 'status' => 1])->find();
    }

    /**
     * @author: Airon
     * @time: 2017年7月11日
     * description:检查用户是否被禁用
     * @param string $mobile
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function check_ban($userModel)
    {
        $ban = UserBlackList::build()->where(['user_id'=>$userModel['user_id']])->order(['ban_end_time'=>"DESC"])->find();
        return $ban;
    }

    /**
     * @author: Airon
     * @time: 2017年7月11日
     * description:
     * @param string $mobile
     * @param string $password
     * @return bool
     */
    public static function register($mobile, $password)
    {

        $userModel = UserModel::create([
            'username' => config('sys.user_default_name') . get_random_num_str(),
            'mobile' => $mobile,
            'password' => $password,
        ]);

        return empty($userModel) ? false : true;
    }

    public static function get_info($where, $field)
    {
        $where['status'] = 1;//正常状态
        $info = UserModel::build()->where($where)->field($field)->find();

        $count = UserLabel::build()
            ->where(['user_id' => $info['user_id']])
            ->count();
//        $info['is_save'] = ($count >= 1) ? 1 : 0;
        $info['is_save'] = 0;

        return $info;
    }

    public static function editInfo($data)
    {
        return UserModel::update($data, ['user_id' => $data['user_id']], true);
    }

    public static function set_push_id($user_id, $push_id, $device_type)
    {
        $where = ['user_id' => $user_id];
        $data['push_id'] = $push_id;
        $data['device_type'] = $device_type;
        $set_old_push_id = UserModel::build()->update(
            ['push_id' => '', 'device_type' => ''],
            $data);//清除 这个push_id 原有的数据
        if ($set_old_push_id === false) {
            return false;
        }
        return UserModel::build()->update($data, $where);

    }
}