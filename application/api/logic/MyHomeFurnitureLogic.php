<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\logic;


use app\common\model\MyHomeFurniture;
use app\common\model\MyHomeFurnitureClassify;
use app\common\model\MyHomeFurnitureMaterial;
use think\Db;

class MyHomeFurnitureLogic
{
    public static function getClassifyList()
    {
        return MyHomeFurnitureClassify::build()->where(['is_deleted' => 0])->field(['classify_id', 'name', 'logo_src'])->select()->toArray();
    }

    public static function insertFurniture($requestData)
    {
        $current = time();
        $requestData['create_time'] = $current;
        $requestData['classify_id'] = $requestData['type'];
        unset($requestData['type']);

        $furniture_id = MyHomeFurniture::build()->insertGetId($requestData);
        if ($furniture_id === false) {
            return false;
        }
        return true;
    }

    public static function editFurniture($requestData)
    {
        $current = time();
        $requestData['update_time'] = $current;
        $requestData['classify_id'] = $requestData['type'];
        unset($requestData['type']);
        $bool = MyHomeFurniture::build()->update($requestData, ['furniture_id' => $requestData['furniture_id']]);
        return $bool;
    }

    public static function insertMaterial($requestData)
    {
        $current = time();
        $material['furniture_id'] = $requestData['furniture_id'];
        $material['name'] = $requestData['name'];
        $material['color'] = $requestData['color'];
        $material['render_img'] = $requestData['render_img'];
        $material['alpha_map'] = $requestData['alpha_map'];
        $material['create_time'] = $current;
        $material['serial_array'] = json_encode($requestData, JSON_UNESCAPED_UNICODE);
        $bool = MyHomeFurnitureMaterial::build()->insert($material);
        return $bool;
    }

    public static function editMaterial($requestData)
    {
        $current = time();
        $where['material_id'] = $requestData['material_id'];
        $material['name'] = $requestData['name'];
        $material['color'] = $requestData['color'];
        $material['render_img'] = $requestData['render_img'];
        $material['alpha_map'] = $requestData['alpha_map'];
        $material['update_time'] = $current;
        $material['serial_array'] = json_encode($requestData, JSON_UNESCAPED_UNICODE);
        $bool = MyHomeFurnitureMaterial::build()->update($material, $where);
        return $bool;
    }

    public static function checkFurniture($furniture_id)
    {
        return MyHomeFurniture::build()->where(['furniture_id' => $furniture_id, 'is_deleted' => 0])->find();
    }

    public static function checkMaterial($material_id)
    {
        return MyHomeFurnitureMaterial::build()->where(['material_id' => $material_id, 'is_deleted' => 0])->find();
    }

    public static function getFurnitureInfo($furniture_id)
    {
        return MyHomeFurniture::build()->where(['furniture_id' => $furniture_id, 'is_deleted' => 0])->field(['is_deleted', 'create_time', 'update_time'], true)->find()->toArray();
    }

    public static function getFurnitureList($classify_id)
    {
        return MyHomeFurniture::build()->where(['classify_id' => $classify_id, 'is_deleted' => 0])->field(['furniture_id', 'name', 'render_img','size'])->select()->toArray();
    }

    public static function getMaterialInfo($material_id)
    {
        return MyHomeFurnitureMaterial::build()->where(['material_id' => $material_id, 'is_deleted' => 0])->field(['is_deleted', 'create_time', 'update_time'], true)->find()->toArray();
    }

    public static function getMaterialList($furniture_id)
    {
        return MyHomeFurnitureMaterial::build()->where(['furniture_id' => $furniture_id, 'is_deleted' => 0])->field(['material_id', 'name', 'color', 'render_img'])->select()->toArray();
    }

    public static function deleteMaterial($material_id)
    {
        return MyHomeFurnitureMaterial::build()->update(['is_deleted' => 1], ['material_id' => $material_id, 'is_deleted' => 0]);
    }

}