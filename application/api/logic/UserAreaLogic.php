<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:49
 */

namespace app\api\logic;

use app\common\model\UserArea as UserAreasModel;
use app\common\model\UserArea;
use app\common\model\ConfigArea;
use think\Db;

class UserAreaLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:用户添加收货地址
     * @param $user_id
     * @param $data
     * @return int|string
     */
    public static function add($user_id, $data)
    {
        $data['user_id'] = $user_id;
        $data['create_time'] = time();
        $UserAreasModel = new UserAreasModel();
        if ($data['is_default'] == 1) {
            //设为默认时,将同一用户其他收货地址都修改为非默认
            Db::startTrans();
            $UserAreasModel->update(['is_default' => '0'], ['user_id' => $user_id]);
            $bool = $UserAreasModel->insert($data);
            if (empty($bool)) {
                Db::rollback();
            } else {
                Db::commit();
            }
            return $bool;
        } else {
            //判断是否已有默认地址,
//            $bool = $UserAreasModel->where(['is_default' => '1', 'user_id' => $user_id])->count();
//            if ($bool == 0) {
//                $data['is_default'] = 1;
//            }
            return $UserAreasModel->insert($data);
        }

    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:用户修改收货地址
     * @param $user_id
     * @param $data
     * @param $area_id
     * @return int|string
     */
    public static function edit($user_id, $area_id, $data)
    {
        $data['create_time'] = time();
        $UserAreasModel = new UserAreasModel();
        Db::startTrans();
        if ($data['is_default'] == 1) {
            //设为默认时,将同一用户其他收货地址都修改为非默认
            $UserAreasModel->update(['is_default' => '0'], ['user_id' => $user_id]);
        } else {
            //判断是否已有默认地址,
            $bool = $UserAreasModel->where(['is_default' => '1', 'user_id' => $user_id])->count();
            if ($bool == 0) {
                $data['is_default'] = 1;
            }
        }
        $bool = $UserAreasModel->update($data, ['area_id' => $area_id, 'user_id' => $user_id]);
        if (empty($bool)) {
            Db::rollback();
        } else {
            Db::commit();
        }
        return $bool;
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:检查用户的收货地址数量
     * @param $user_id
     * @return int|string
     */
    public static function checkCount($user_id)
    {
        return UserArea::whereCount(['user_id' => $user_id]);
    }

    /**
     * @author: Airon
     * @time: 2017年8月4日
     * description:获取用户的默认收货地址
     * @param $user_id
     * @return int|string|array
     */
    public static function getDefault($user_id)
    {
        return UserArea::build()
            ->where(['user_id' => $user_id, 'is_default' => '1'])
            ->field(['user_id', 'create_time', 'is_default'], true)->find();
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:获取用户的收货地址详情
     * @param $user_id
     * @param $area_id
     * @return int|string|array
     */
    public static function getInfo($user_id, $area_id)
    {
        return UserArea::build()
            ->where(['user_id' => $user_id, 'area_id' => $area_id])
            ->field(['area_id', 'user_id', 'create_time', 'is_default'], true)->find();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:获取收货地址列表
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getList($user_id)
    {
        $UserAreasModel = new UserAreasModel();
        return $UserAreasModel
            ->where(['user_id' => $user_id])
            ->field(['create_time', 'user_id'], true)
            ->order('is_default DESC')
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:
     * @param $user_id
     * @param $area_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function delete($user_id, $area_id)
    {
        $UserAreaModel = new UserAreasModel();
        $bool = $UserAreaModel
            ->where(['user_id' => $user_id, 'area_id' => $area_id])
            ->field(['is_default'])->find();
        if (empty($bool)) {
            //不存在了就当作删除成功了呗
            return true;
        }

        if ($bool['is_default'] == 0) {
            //不是默认地址就直接删了
            return $UserAreaModel->where(['user_id' => $user_id, 'area_id' => $area_id])->delete();
        }
        $count = $UserAreaModel->where(['user_id' => $user_id])->count();
        if ($count > 1) {
            //当用户删除默认地址且还存在其它地址信息时,则随机修改一个地址为默认地址(设计组说的)
            Db::startTrans();
            $bool = $UserAreaModel->where(['user_id' => $user_id, 'area_id' => $area_id])->delete();
            if (empty($bool)) {
                Db::rollback();
                return false;
            }
            $bool = $UserAreaModel
                ->where(['user_id' => $user_id])
                ->order(['create_time' => 'DESC'])->find();
            $bool = $UserAreaModel->update(['is_default' => '1'],
                ['user_id' => $user_id, 'area_id' => $bool['area_id']]);
            if (empty($bool)) {
                Db::rollback();
                return false;
            }
            Db::commit();
            return true;
        } else {
            //当用户删除默认地址且不存在其它地址信息时,就直接删了
            return $UserAreaModel->where(['user_id' => $user_id, 'area_id' => $area_id])->delete();
        }

    }

    public static function getAddressByParentId($parent_id)
    {
        return ConfigArea::build()->field(['id', 'code', 'name', 'level'])->where(['parent_id' => $parent_id])->select()->toArray();
    }

    public static function getAddressByName($name)
    {
        $parent_id = ConfigArea::build()->where(['name' => $name])->value('id');
        return ConfigArea::build()->field(['id', 'code', 'name', 'level'])->where(['parent_id' => $parent_id])->select()->toArray();
    }

    public static function getAddressByName2($keyword){
        $where = ['level' =>'2'];
        if (!empty($keyword)){
            $where['name'] = ['like', "%{$keyword}%"];
        }
        return ConfigArea::build()->field(['id', 'code', 'name', 'level'])->where($where)->select()->toArray();

    }
}