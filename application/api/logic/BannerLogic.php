<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 10:43
 */

namespace app\api\logic;

use app\common\model\Banner as BannerModel;
use app\common\model\DemandBanner as DemandBannerModel;

class BannerLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月14日
     * description:获取轮播图列表
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function get_list($where){
        return BannerModel::bannerList($where);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取需求banner
     * @return array|bool
     */
    public static function getDemandBanners()
    {
        try{
            return DemandBannerModel::build()->alias('b')->field(['b.banner_id','b.demand_id','b.scheme_pic','d.title','d.classify','d.reward'])
                ->join('demand d','d.demand_id = b.demand_id','LEFT')
                ->page(0,4)->order(['b.banner_id'=>'DESC'])->select()->toArray();
        }catch (\Exception $e){
            return false;
        }
    }
}