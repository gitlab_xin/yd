<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/11
 * Time: 14:56
 */

namespace app\api\logic;

use app\common\model\Sms as SmsModel;

class SmsLogic
{
    public static function SendSms($mobile, $sms_type)
    {
        return SmsModel::sendSms($mobile, get_random_num_str(), $sms_type);
    }

    public static function validateCode($requestData)
    {
        return SmsModel::validateCode(
            $requestData['mobile'],
            $requestData['code'],
            $requestData['sms_type']
        );
    }
}