<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 17:28
 */

namespace app\api\logic;

use app\common\model\HelpCenter as HelpCenterModel;
use app\common\model\HelpCenterClassify as HelpCenterClassifyModel;

class HelpCenterLogic
{
    public function getAllClassify()
    {
        return HelpCenterClassifyModel::build()->field(['id', 'name'])->select()->toArray();
    }

    public function getPostsByClassify($classify_id)
    {
        return HelpCenterModel::build()->field(['id', 'title'])->where(['classify_id' => $classify_id])->select()->toArray();
    }

    public function getPostDetail($post_id)
    {
        return HelpCenterModel::build()->find($post_id);
    }
}