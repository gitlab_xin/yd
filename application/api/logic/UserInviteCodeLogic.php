<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/29
 * Time: 11:28
 */

namespace app\api\logic;

use app\common\model\Activity as ActivityModel;
use app\common\model\ActivityApply as ApplyModel;
use app\common\model\ActivityApply;
use app\common\model\UserInviteCode;

class UserInviteCodeLogic
{
    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param $code
     * @return array|bool|false|\PDOStatement|string|\think\Model
     */
    public static function checkCode($code)
    {
        $result = UserInviteCode::build()
            ->where(['code' => $code, 'status' => 0])
            ->count();
        if (empty($result)) {
            return false;
        }
        return $result;
    }

    /**
     * @author: Airon
     * @time:   2019年6月
     * description
     * @param $code
     * @return array|bool|false|\PDOStatement|string|\think\Model
     */
    public static function useCode($code)
    {
        $result = UserInviteCode::build()
            ->where(['code' => $code, 'status' => 0])
            ->count();
        if (empty($result)) {
            return false;
        }
        $result = UserInviteCode::build()->where(['code'=>$code,'status'=>0])->update(['status'=>1,'update_time'=>time()]);
        return $result;
    }
}