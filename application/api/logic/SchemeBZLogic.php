<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/27
 * Time: 9:57
 */

namespace app\api\logic;

use app\common\model\SchemeBZBean as SchemeBean;
use app\common\model\CustomizedScheme;
use app\common\tools\CommonMethod;

class SchemeBZLogic extends SchemeBaseLogic
{
    private $productLogic = null;

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:获取productlogic
     * @return ProductLogic|null
     */
    public function getProductLogic()
    {
        if ($this->productLogic == null) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月17日
     * description:生成方案
     * @param $data
     * @return SchemeBean
     */
    public function createBZScheme($data)
    {
        $index = base64_encode(http_build_query($data));
        $product = $this->beforeCreate('bz', $index);
        if (!empty($product)) {
            return $product;
        }

        $scheme = new SchemeBean();

        $scheme->setScheme_width($data['width']);//宽
        $scheme->setScheme_height($data['height']);//高
        $scheme->setScheme_deep(583);//深
        $scheme->setScheme_door_type($data['type']);//柜体风格
        $scheme->setScheme_color_no($data['type'] == 0 ? '004' : '001');//花色
        $scheme->setScheme_s_type($data['table_type']);//桌子类型

        $funitureWidths = $this->calculate($scheme);
        if ($funitureWidths != null) {
            $this->generationProducts($scheme, $funitureWidths);
        }

        $scheme->setScheme_no('basic-0');
        $scheme->setScheme_type('BZ');
        $scheme->setScheme_name('平开门标准衣柜-' . getRandomInt());
        $scheme->setSearch_index($index);
        $result = $scheme->mainInfo();

        $this->afterCreate('bz', $result);

        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:重构成原来参数进行验证
     * @param $scheme
     * @return array
     */
    public function getOriginParams($scheme)
    {
        $params = [];

        $params['width'] = isset($scheme['scheme_width']) ? $scheme['scheme_width'] : null;
        $params['height'] = isset($scheme['scheme_height']) ? $scheme['scheme_height'] : null;
        $params['type'] = isset($scheme['scheme_door_type']) ? $scheme['scheme_door_type'] : null;
        $params['color_no'] = isset($scheme['scheme_color_no']) ? $scheme['scheme_color_no'] : null;
        $params['table_type'] = isset($scheme['scheme_s_type']) ? $scheme['scheme_s_type'] : null;
        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';

        return $params;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:参数转成成obj
     * @param $schemeData
     * @return SchemeBean
     */
    public function buildScheme($schemeData)
    {
        return SchemeBean::build($schemeData);
    }

    /**
     * @author: Rudy
     * @time: 2017年10月17日
     * description:保存方案
     * @param SchemeBean $schemeBean
     * @param $schemeData
     * @param $userId
     * @return mixed
     */
    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        if (($schemeBean->getScheme_door_schemes()) != null) {
            $schemeData['scheme_door_schemes'] = $schemeBean->getScheme_door_schemes_array();
            $schemeData['doorType'] = $schemeBean->getScheme_door_type();
            $schemeData['doorDirection'] = $schemeBean->getScheme_door_direction();
        }
        if (($schemeBean->getScheme_dm_schemes()) != null) {
            $schemeData['scheme_dm_schemes'] = $schemeBean->getScheme_dm_schemes_array();
        }
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_type' => 'BZ',
            'scheme_type_text' => '平开门标准衣柜',
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 583,
            'scheme_door_count' => $schemeData['scheme_door_count'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => '000',
            'scheme_sk' => -1,
            'scheme_hole_type' => 0,
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => 583,
            'scheme_hole_left' => 0,
            'scheme_hole_right' => 0,
            'is_left_wall' => '0',
            'is_right_wall' => '0',
            'is_left_wall_zt' => '0',
            'is_right_wall_zt' => '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model->getLastInsID();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月20日
     * description:更新桌子
     * @param SchemeBean $schemeBean
     * @param $schemeData
     * @param $userId
     * @param $scheme_id
     * @return $this
     */
    public function update(SchemeBean $schemeBean, $schemeData, $userId, $scheme_id)
    {
        if (($schemeBean->getScheme_door_schemes()) != null) {
            $schemeData['scheme_door_schemes'] = $schemeBean->getScheme_door_schemes_array();
            $schemeData['doorType'] = $schemeBean->getScheme_door_type();
            $schemeData['doorDirection'] = $schemeBean->getScheme_door_direction();
        }
        if (($schemeBean->getScheme_dm_schemes()) != null) {
            $schemeData['scheme_dm_schemes'] = $schemeBean->getScheme_dm_schemes_array();
        }
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
        ];
        if (isset($schemeData['scheme_name'])) {
            $schemeModelData['scheme_name'] = $schemeData['scheme_name'];
        }
        return CustomizedScheme::update($schemeModelData, ['user_id' => $userId, 'id' => $scheme_id]);
    }

    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:
     * @param SchemeBean $scheme
     * @param $doorType 0 平板 1欧式 2合金
     * @param $doorDirection -1 默认 0 右开 1左开
     * @return string
     */
    public function addBZYGDoor(SchemeBean $scheme, $doorType, $doorDirection = -1)
    {
        $color_no = $scheme->getScheme_color_no();

        $w = $doorWidth = $doorHeight = 0;
        $pNo = $pNo_new = '';
        $door_schemes = [];
        $onedoor = null;

        if ($doorType == 1) {//对门做验证，颜色验证已在controller做了
            foreach ($scheme->getScheme_schemes() as $index => $s) {
                $width = $s->getScheme_width();
                if ($width != 456 && $width != 912) {
                    return '宽度为456mm或912mm的柜体可以安装欧式门！';
                }
            }
            if ($index === 0 && $scheme->getScheme_schemes()[0]->getScheme_width() == 456) {
                return '至少两列时才能安装欧式门！';
            }
        }

        $handleflag = '';
        $schemes_count = count($scheme->getScheme_schemes());
        $scheme->setScheme_door_type($doorType);
        $scheme->setScheme_door_direction($doorDirection);
        foreach ($scheme->getScheme_schemes() as $i => $onrfur) {
            $onrfur = change2SchemeBean($onrfur);
            $w = $onrfur->getScheme_width();
            switch ($w) {
                //成人 单门
                case 456:
                    $doorWidth = 453;
                    $doorHeight = $doorType == 1 ? 2248 : 2333;

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm(-14.5);
                    $onedoor->setM_top_mm(3);

                    switch ($doorType) {
                        case 0://平板门
                            $onedoor->setScheme_name('24标准衣柜木板门');
                            // BM-{宽度}-{长度}-{风格/00}-{花色代码}

                            if ($i == 0) {
                                $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '0';
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $onedoor->setScheme_no_door(0);
                            } elseif ($i == ($schemes_count - 1)) {
                                $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '1';
                                $onedoor->setScheme_door_direction(1);//左开-把手在左
                                $onedoor->setScheme_no_door(0);
                            } else {
                                $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no;
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $onedoor->setScheme_no_door(1);
                            }
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;

                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        case 1://欧式门
                            // BM-{宽度}-{长度}-{风格/PLR}-{花色代码}
                            if ($i == 0) {
                                $pNo = 'M24-OM-R-004P';
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PR-' . $color_no;
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $onedoor->setScheme_no_door(0);
                                $onedoor->setScheme_name('24标准衣柜简欧门-右');
                                $onedoor->setScheme_pic('../Door/StandardWardrobe/door/JianOuShiMen.png');
                            } elseif ($i == ($schemes_count - 1)) {
                                $pNo = 'M24-OM-L-004P';
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PL-' . $color_no;
                                $onedoor->setScheme_door_direction(1);//右开-把手在右
                                $onedoor->setScheme_no_door(0);
                                $onedoor->setScheme_name('24标准衣柜简欧门-左');
                                $onedoor->setScheme_pic('../Door/StandardWardrobe/door/JianOuShiMenzuo.png');
                            } else {
                                $onedoor->setScheme_name('24标准衣柜简欧门-右');
                                $pNo = 'M24-OM-R-004P';
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PR-' . $color_no;
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $onedoor->setScheme_no_door(1);//欧式门把手在判断顶帽时，自动变换方向
                                $onedoor->setScheme_pic('../Door/StandardWardrobe/door/JianOuShiMen.png');
                            }
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setM_top_mm(88);
                            break;
                        case 2://合金门
                            // BM-{宽度}-{长度}-{风格/01L}-{花色代码}
                            if ($i == 0) {
                                $onedoor->setScheme_name('24标准衣柜铝边门-右');
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $pNo = 'M24-' . $doorWidth . 'M-01R-' . $color_no;
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01R-' . $color_no;
                                $onedoor->setScheme_no_door(0);
                            } elseif ($i == ($schemes_count - 1)) {
                                $onedoor->setScheme_name('24标准衣柜铝边门-左');
                                $onedoor->setScheme_door_direction(1);//右开-把手在右
                                $pNo = 'M24-' . $doorWidth . 'M-01L-' . $color_no;
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01L-' . $color_no;
                                $onedoor->setScheme_no_door(0);
                            } else {
                                $onedoor->setScheme_name('24标准衣柜铝边门-右');
                                $onedoor->setScheme_door_direction(0);//右开-把手在右
                                $pNo = 'M24-' . $doorWidth . 'M-01R-' . $color_no;
                                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01R-' . $color_no;
                                $onedoor->setScheme_no_door(1);
                            }
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        default:
                            break;
                    }
                    if (intval($doorDirection) !== -1) {
                        $this->addBZYGDoorOne($onedoor, $doorDirection);
                    }
                    $door_schemes[] = $onedoor;
                    break;
                //学生 双门
                case 784:
                    $doorWidth = 389;
                    $doorHeight = 1949;

                    $onedoor = new SchemeBean();
                    // BM-{宽度}-{长度}-{风格/00}-{花色代码}

                    $onedoor->setScheme_name("20标准衣柜木板门");
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm(-14.5);
                    $onedoor->setM_top_mm(3);


                    // 平板门
                    $pNo = "M20-M-00-" . $color_no . "0";
                    $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;

                    $onedoor->setScheme_door_direction(0);
                    $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                    $onedoor->setScheme_no($pNo_new);
                    $door_schemes[] = $onedoor;

                    $onedoor = new SchemeBean();

                    $onedoor->setScheme_name("20标准衣柜木板门");
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left(1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($doorWidth - 14.5 + 3);
                    $onedoor->setM_top_mm(3);


                    // 平板门
                    $pNo = "M20-M-00-" . $color_no . "1";
                    $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                    $onedoor->setScheme_door_direction(1);
                    $onedoor->setScheme_no($pNo_new);
                    $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                    $door_schemes[] = $onedoor;
                    break;
                //学生 三门
                case 1177:
                    $doorWidth = 389;
                    $doorHeight = 1949;

                    $handleflag = $onrfur->getScheme_schemes()[0]->getScheme_width() == 368 ? '0,0,1' : '0,1,1';

                    $handleDir = explode(',', $handleflag);

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_name("20标准衣柜木板门");
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($handleflag === '0,0,1' ? -13.5 : -14.5);//如果第一个柜是单开柜，则距离-13.5，如果不是，则是双开，则距离-14.5
                    $onedoor->setM_top_mm(3);
                    // 平板门
                    $pNo = "M20-M-00-" . $color_no . $handleDir[0];
                    $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                    $onedoor->setScheme_door_direction(0);
                    // onedoor.setScheme_no("M20-M-00-" + color_no);
                    $onedoor->setScheme_no($pNo_new);
                    $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                    $door_schemes[] = $onedoor;

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_name("20标准衣柜木板门");
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left(1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($handleflag === '0,0,1' ? -14.5 : $doorWidth - 14.5 + 3);//如果第一个柜是单开柜，则距离-13.5，如果不是，则是双开，则距左边门3mm
                    $onedoor->setM_top_mm(3);
                    // 平板门
                    $pNo = "M20-M-00-" . $color_no . $handleDir[1];
                    $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                    $onedoor->setScheme_door_direction($handleflag === '0,0,1' ? 0 : 1);
                    // onedoor.setScheme_no("M20-M-00-" + color_no);
                    $onedoor->setScheme_no($pNo_new);
                    $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                    $door_schemes[] = $onedoor;

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_name("20标准衣柜木板门");
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left(1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($handleflag === '0,0,1' ? $doorWidth - 14.5 + 3 : -7.5);//如果最后一个柜是单开柜，则距离-7.5，如果不是，则是双开，则距离左边门3mm
                    $onedoor->setM_top_mm(3);
                    // 平板门
                    $pNo = "M20-M-00-" . $color_no . $handleDir[2];
                    $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                    $onedoor->setScheme_door_direction(1);
                    // onedoor.setScheme_no("M20-M-00-" + color_no);
                    $onedoor->setScheme_no($pNo_new);
                    $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                    $door_schemes[] = $onedoor;
                    break;
                //成人 双门
                case 912:
                    $doorWidth = 453;
                    $doorHeight = $doorType == 1 ? 2248 : 2333;

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm(-14.5);
                    $onedoor->setM_top_mm(3);

                    switch ($doorType) {
                        case 0://平板门
                            $onedoor->setScheme_name('24标准衣柜木板门');
                            $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '0';
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');

                            break;
                        case 1://欧式门
                            if ($color_no == 4) {
                                $onedoor->setScheme_name('24标准衣柜简欧门-右');
                                $pNo = '../Door/StandardWardrobe/door/JianOuShiMen';
                            }
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PR-' . $color_no;
                            $onedoor->setScheme_pic($pNo . 'png');
                            $onedoor->setScheme_no($pNo_new);

                            $onedoor->setM_top_mm(88);

                            break;
                        case 2://合金门
                            $onedoor->setScheme_name('24标准衣柜铝边门-右');
                            $pNo = 'M24-' . $doorWidth . 'M-01R-' . $color_no;
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01R-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        default:
                            break;
                    }
                    $onedoor->setScheme_door_direction(0);
                    $door_schemes[] = $onedoor;


                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left(1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($doorWidth - 14.5 + 3);
                    $onedoor->setM_top_mm(3);

                    switch ($doorType) {
                        case 0://平板门
                            $onedoor->setScheme_name('24标准衣柜木板门');
                            $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '1';
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');

                            break;
                        case 1://欧式门
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PL-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_name('24标准衣柜简欧门-左');
                            $onedoor->setScheme_pic('../Door/StandardWardrobe/door/JianOuShiMenzuo.png');

                            $onedoor->setM_top_mm(88);

                            break;
                        case 2://合金门
                            $onedoor->setScheme_name('24标准衣柜铝边门-左');
                            $pNo = 'M24-' . $doorWidth . 'M-01L-' . $color_no;
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01L-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        default:
                            break;
                    }
                    $onedoor->setScheme_door_direction(1);
                    $door_schemes[] = $onedoor;

                    break;
                // 成人 双门 没有欧式
                case 976:
                    $doorWidth = 485;
                    $doorHeight = 2333;

                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm(-14.5);
                    $onedoor->setM_top_mm(3);

                    switch ($doorType) {
                        case 0://平板门
                            $onedoor->setScheme_name('24标准衣柜木板门');
                            $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '0';
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');

                            break;
                        case 2://合金门
                            $onedoor->setScheme_name('24标准衣柜铝边门-右');
                            $pNo = 'M24-' . $doorWidth . 'M-01R-' . $color_no;
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01R-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        default:
                            break;
                    }
                    $onedoor->setScheme_door_direction(0);
                    $door_schemes[] = $onedoor;


                    $onedoor = new SchemeBean();
                    $onedoor->setScheme_width($doorWidth);
                    $onedoor->setScheme_height($doorHeight);
                    $onedoor->setScheme_s_width(intval($doorWidth / 8));
                    $onedoor->setScheme_s_height(intval($doorHeight / 8));
                    $onedoor->setScheme_type($doorType);
                    $onedoor->setM_left(1);
                    $onedoor->setScheme_color_no($color_no);
                    $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                    $onedoor->setScheme_door_color_no($doorType);

                    $onedoor->setM_left_mm($doorWidth - 14.5 + 3);
                    $onedoor->setM_top_mm(3);

                    switch ($doorType) {
                        case 0://平板门
                            $onedoor->setScheme_name('24标准衣柜木板门');
                            $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no . '1';
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');

                            break;
                        case 2://合金门
                            $onedoor->setScheme_name('24标准衣柜铝边门-左');
                            $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01L-' . $color_no;
                            $pNo = 'M24-' . $doorWidth . 'M-01L-' . $color_no;
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                            break;
                        default:
                            break;
                    }
                    $onedoor->setScheme_door_direction(1);
                    $door_schemes[] = $onedoor;

                    break;
                // 784+914 784+1170 1177+914 1177+1170
                default:
                    $fur_count = count($onrfur->getScheme_schemes());
                    switch ($fur_count) {
                        case 3:
                            $doorWidth = 389;
                            $doorHeight = 1949;

                            $handleflag = $onrfur->getScheme_schemes()[0]->getScheme_width() == 368 ? '0,0,1' : '0,1,1';

                            $handleDir = explode(',', $handleflag);

                            $onedoor = new SchemeBean();
                            $onedoor->setScheme_name("20标准衣柜木板门");
                            $onedoor->setScheme_width($doorWidth);
                            $onedoor->setScheme_height($doorHeight);
                            $onedoor->setScheme_s_width(intval($doorWidth / 8));
                            $onedoor->setScheme_s_height(intval($doorHeight / 8));
                            $onedoor->setScheme_type($doorType);
                            $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                            $onedoor->setScheme_color_no($color_no);
                            $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                            $onedoor->setScheme_door_color_no($doorType);

                            $onedoor->setM_left_mm($handleflag === '0,0,1' ? -13.5 : -14.5);//如果第一个柜是单开柜，则距离-13.5，如果不是，则是双开，则距离-14.5
                            $onedoor->setM_top_mm(3);
                            // 平板门
                            $pNo = "M20-M-00-" . $color_no . $handleDir[0];
                            $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                            $onedoor->setScheme_door_direction(0);
                            // onedoor.setScheme_no("M20-M-00-" + color_no);
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                            $door_schemes[] = $onedoor;

                            $onedoor = new SchemeBean();
                            $onedoor->setScheme_name("20标准衣柜木板门");
                            $onedoor->setScheme_width($doorWidth);
                            $onedoor->setScheme_height($doorHeight);
                            $onedoor->setScheme_s_width(intval($doorWidth / 8));
                            $onedoor->setScheme_s_height(intval($doorHeight / 8));
                            $onedoor->setScheme_type($doorType);
                            $onedoor->setM_left(1);
                            $onedoor->setScheme_color_no($color_no);
                            $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                            $onedoor->setScheme_door_color_no($doorType);

                            $onedoor->setM_left_mm($handleflag === '0,0,1' ? -14.5 : $doorWidth - 14.5 + 3);//如果第一个柜是单开柜，则距离-13.5，如果不是，则是双开，则距左边门3mm
                            $onedoor->setM_top_mm(3);
                            // 平板门
                            $pNo = "M20-M-00-" . $color_no . $handleDir[1];
                            $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                            $onedoor->setScheme_door_direction($handleflag === '0,0,1' ? 0 : 1);
                            // onedoor.setScheme_no("M20-M-00-" + color_no);
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                            $door_schemes[] = $onedoor;

                            $onedoor = new SchemeBean();
                            $onedoor->setScheme_name("20标准衣柜木板门");
                            $onedoor->setScheme_width($doorWidth);
                            $onedoor->setScheme_height($doorHeight);
                            $onedoor->setScheme_s_width(intval($doorWidth / 8));
                            $onedoor->setScheme_s_height(intval($doorHeight / 8));
                            $onedoor->setScheme_type($doorType);
                            $onedoor->setM_left(1);
                            $onedoor->setScheme_color_no($color_no);
                            $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                            $onedoor->setScheme_door_color_no($doorType);

                            $onedoor->setM_left_mm($handleflag === '0,0,1' ? $doorWidth - 14.5 + 3 : -7.5);//如果最后一个柜是单开柜，则距离-7.5，如果不是，则是双开，则距离左边门3mm
                            $onedoor->setM_top_mm(3);
                            // 平板门
                            $pNo = "M20-M-00-" . $color_no . $handleDir[2];
                            $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                            $onedoor->setScheme_door_direction(1);
                            // onedoor.setScheme_no("M20-M-00-" + color_no);
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                            $door_schemes[] = $onedoor;
                            break;
                        case 2:
                            $doorWidth = 389;
                            $doorHeight = 1949;

                            $onedoor = new SchemeBean();
                            // BM-{宽度}-{长度}-{风格/00}-{花色代码}

                            $onedoor->setScheme_name("20标准衣柜木板门");
                            $onedoor->setScheme_width($doorWidth);
                            $onedoor->setScheme_height($doorHeight);
                            $onedoor->setScheme_s_width(intval($doorWidth / 8));
                            $onedoor->setScheme_s_height(intval($doorHeight / 8));
                            $onedoor->setScheme_type($doorType);
                            $onedoor->setM_left($onrfur == $scheme->getScheme_schemes()[0] ? 0 : 1);
                            $onedoor->setScheme_color_no($color_no);
                            $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                            $onedoor->setScheme_door_color_no($doorType);

                            $onedoor->setM_left_mm(-14.5);
                            $onedoor->setM_top_mm(3);

                            // 平板门
                            $pNo = "M20-M-00-" . $color_no . "0";
                            $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;

                            $onedoor->setScheme_door_direction(0);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                            $onedoor->setScheme_no($pNo_new);
                            $door_schemes[] = $onedoor;

                            $onedoor = new SchemeBean();

                            $onedoor->setScheme_name("20标准衣柜木板门");
                            $onedoor->setScheme_width($doorWidth);
                            $onedoor->setScheme_height($doorHeight);
                            $onedoor->setScheme_s_width(intval($doorWidth / 8));
                            $onedoor->setScheme_s_height(intval($doorHeight / 8));
                            $onedoor->setScheme_type($doorType);
                            $onedoor->setM_left(1);
                            $onedoor->setScheme_color_no($color_no);
                            $onedoor->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
                            $onedoor->setScheme_door_color_no($doorType);

                            $onedoor->setM_left_mm($doorWidth - 14.5 + 3);
                            $onedoor->setM_top_mm(3);

                            // 平板门
                            $pNo = "M20-M-00-" . $color_no . "1";
                            $pNo_new = "BM-" . $doorWidth . "-" . $doorHeight . "-00-" . $color_no;
                            $onedoor->setScheme_door_direction(1);
                            $onedoor->setScheme_no($pNo_new);
                            $onedoor->setScheme_pic(GetBZDoorPicDir() . $pNo . ".jpg");
                            $door_schemes[] = $onedoor;
                            break;
                        default:
                            break;
                    }
            }
        }

        $scheme->setScheme_door_color_no($color_no);
        $scheme->setScheme_door_width($doorWidth);
        $scheme->setScheme_door_height($doorHeight);
        $scheme->setScheme_have_door(1);
        $scheme->setScheme_door_schemes($door_schemes);

        /*
         * 生成顶帽
         */
        if ($doorType == 1) {
            $dm_schemes = [];
            $d_s = null;
            // BM-SM-{长度}-{宽度}-{风格/P}-{花色代码}
            $index = $this->onedoorcolindex($scheme);
            if (3 >= $schemes_count || $index == -1 || $index == 0 || $index == $schemes_count) {
                for ($i = 0; $i < $schemes_count; $i++) {
                    $col = change2SchemeBean($scheme->getScheme_schemes()[$i]);
                    if ($col->getScheme_width() == 912) {
                        if ($i + 1 < $schemes_count) {
                            $temp = $i + 1;
                            if (456 == $scheme->getScheme_schemes()[$temp]->getScheme_width()) {
                                $d_s = new SchemeBean();
                                $d_s->setScheme_type("3");
                                $d_s->setScheme_no("BM-SM-1368-138-P-004");
                                $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang3.png");
                                $d_s->setScheme_width(1368);
                                $d_s->setScheme_height(142);
                                $d_s->setScheme_s_width(intval((912 + 456) / 8));
                                $d_s->setScheme_s_height(intval(142 / 8));// 150
                                $i++;
                                $dm_schemes[] = $d_s;

                                $scheme->getScheme_door_schemes()[2 * $index]->setScheme_door_color_no('1');
//                                $scheme->getScheme_door_schemes()[2 * $index] = $this->addBZYGDoorOne($scheme->getScheme_door_schemes()[2 * $index], 1);
                                continue;
                            }
                        }
                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("2");
                        $d_s->setScheme_no("BM-SM-912-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang2.png");
                        $d_s->setScheme_width(912);
                        $d_s->setScheme_height(138);
                        $d_s->setScheme_s_width(intval(912 / 8));
                        $d_s->setScheme_s_height(intval(138 / 8));
                        $dm_schemes[] = $d_s;
                    } else {
                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("3");
                        $d_s->setScheme_no("BM-SM-1368-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang3.png");
                        $d_s->setScheme_width(1368);
                        $d_s->setScheme_height(142);
                        $d_s->setScheme_s_width(intval((912 + 456) / 8));
                        $d_s->setScheme_s_height(intval(142 / 8));
                        $dm_schemes[] = $d_s;
                    }
                }
            } elseif ($index == 1) {
                for ($i = 0; $i < $schemes_count; $i++) {
                    if ($i == $index) {

                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("3");
                        $d_s->setScheme_no("BM-SM-1368-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang3.png");
                        $d_s->setScheme_width(1368);
                        $d_s->setScheme_height(142);
                        $d_s->setScheme_s_width(intval((912 + 456) / 8));
                        $d_s->setScheme_s_height(intval(150 / 8));// 150
                        $dm_schemes[] = $d_s;
                        $i++;

                        $scheme->getScheme_door_schemes()[2 * $index]->setScheme_door_color_no('1');
//                        $scheme->getScheme_door_schemes()[2 * $index] = $this->addBZYGDoorOne($scheme->getScheme_door_schemes()[2 * $index], 0);
                        continue;
                    } else {
                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("2");
                        $d_s->setScheme_no("BM-SM-912-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang3.png");
                        $d_s->setScheme_width(912);
                        $d_s->setScheme_height(138);
                        $d_s->setScheme_s_width(intval(912 / 8));
                        $d_s->setScheme_s_height(intval(138 / 8));
                        $dm_schemes[] = $d_s;
                    }
                }
            } else {
                for ($i = 0; $i < $schemes_count; $i++) {
                    if ($i == $index - 1) {

                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("3");
                        $d_s->setScheme_no("BM-SM-1368-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang3.png");
                        $d_s->setScheme_width(1368);
                        $d_s->setScheme_height(142);
                        $d_s->setScheme_s_width(intval((912 + 456) / 8));
                        $d_s->setScheme_s_height(intval(150 / 8));// 150
                        $dm_schemes[] = $d_s;
                        $i++;

                        $scheme->getScheme_door_schemes()[2 * $index]->setScheme_door_color_no('1');
//                        $scheme->getScheme_door_schemes()[2 * $index] = $this->addBZYGDoorOne($scheme->getScheme_door_schemes()[2 * $index], 1);
                        continue;
                    } else {
                        $d_s = new SchemeBean();
                        $d_s->setScheme_type("2");
                        $d_s->setScheme_no("BM-SM-912-138-P-004");
                        $d_s->setScheme_pic("../Door/StandardWardrobe/door/JianOuShiMenShang2.png");
                        $d_s->setScheme_width(912);
                        $d_s->setScheme_height(138);
                        $d_s->setScheme_s_width(intval(912 / 8));
                        $d_s->setScheme_s_height(intval(138 / 8));
                        $dm_schemes[] = $d_s;
                    }
                }
            }
            $scheme->setScheme_dm_schemes($dm_schemes);
        }

        return null;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月23日
     * description:
     * @param SchemeBean $d_scheme
     * @param $direction 0右开  1左开
     * @return SchemeBean
     *
     */
    public function addBZYGDoorOne(SchemeBean $d_scheme, $direction)
    {
        $color_no = $d_scheme->getScheme_color_no();
        $doorType = $d_scheme->getScheme_door_color_no();

        $doorWidth = $d_scheme->getScheme_width();
        $doorHeight = $d_scheme->getScheme_height();

        $pNo = $pNo_new = "";
        // BM-{宽度}-{长度}-{风格/01L}-{花色代码}
        // BM-{宽度}-{长度}-{风格/01R}-{花色代码}
        // BM-{宽度}-{长度}-{风格/PL}-{花色代码}
        // BM-{宽度}-{长度}-{风格/PR}-{花色代码}
        // BM-SM-{长度}-{宽度}-{风格/P}-{花色代码}

        switch ($doorType) {
            case 0://平板门
                $pNo = 'M24-' . $doorWidth . 'M-00-' . $color_no;
                $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-00-' . $color_no;
                $d_scheme->setScheme_no($pNo_new);
                $d_scheme->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');

                break;
            case 1://欧式门
                if ($direction == 0) {
                    $pNo = '../Door/StandardWardrobe/door/JianOuShiMen';
                    $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PR-' . $color_no;
                    $d_scheme->setScheme_name('24标准衣柜简欧门-右');
                    $d_scheme->setScheme_no($pNo_new);
                } else {
                    $pNo = '../Door/StandardWardrobe/door/JianOuShiMenzuo';
                    $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-PL-' . $color_no;
                    $d_scheme->setScheme_name('24标准衣柜简欧门-左');
                    $d_scheme->setScheme_no($pNo_new);

                }
                $d_scheme->setScheme_pic($pNo . '.png');

                break;
            case 2://合金门
                if ($direction == 0) {
                    $pNo = 'M24-' . $doorWidth . 'M-01R-' . $color_no;
                    $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01R-' . $color_no;
                    $d_scheme->setScheme_name('24标准衣柜铝边门-右');
                } else {
                    $pNo = 'M24-' . $doorWidth . 'M-01L-' . $color_no;
                    $pNo_new = 'BM-' . $doorWidth . '-' . $doorHeight . '-01L-' . $color_no;
                    $d_scheme->setScheme_name('24标准衣柜铝边门-左');
                }
                $d_scheme->setScheme_no($pNo_new);
                $d_scheme->setScheme_pic(GetBZDoorPicDir() . $pNo . '.jpg');
                break;
            default:
                break;
        }
        $d_scheme->setScheme_no_door(0);
        $d_scheme->setScheme_door_direction($direction);

        return $d_scheme;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月27日
     * description:计算合适的衣柜组合
     * @param SchemeBean $scheme
     * @return array|mixed|null
     */
    private function calculate(SchemeBean $scheme)
    {
        $width = $scheme->getScheme_width();
        $height = $scheme->getScheme_height();

        $funitureWidths = $bestFuniCols = [];
        $errorRange = 200;//误差200

        switch ($height) {
            case 2403://成人衣柜
                $w976 = 976;
                $w912 = 912;
                $w456 = 456;
                $max456colcount = intval($width / $w456) + 1;
                $max912colcount = intval($width / $w912) + 1;
                $max976colcount = 0 == $scheme->getScheme_door_type() ? 0 : intval($width / $w976) + 1;//简欧风格没有976尺寸的板
                /*
                 * 计算误差在200内的组合算法
                 * 需要获取误差最小的组合,误差值一样的情况下,按照976,912,456的优先顺序获取组合
                 * 此处代码修正过,和java版的有误(在计算误差较小的列时),但效果一样
                 */
                for ($i976 = 0; $i976 <= $max976colcount; $i976++) {
                    for ($i912 = 0; $i912 <= $max912colcount; $i912++) {
                        for ($i456 = 0; $i456 <= $max456colcount; $i456++) {
                            $w = $i976 * $w976 + $i912 * $w912 + $i456 * $w456;
                            if (($range = abs($width - $w)) >= $errorRange) {//超过误差值就跳过
                                continue;
                            }
                            $temp = [];
                            for ($i = 0; $i < $i976; $i++) {
                                $temp[] = $w976;
                            }
                            for ($i = 0; $i < $i912; $i++) {
                                $temp[] = $w912;
                            }
                            for ($i = 0; $i < $i456; $i++) {
                                $temp[] = $w456;
                            }
                            $bestFuniCols[$range][] = $temp;
                        }
                    }
                }
                //如果没有计算出组合,则返回null
                if ($bestFuniCols == null || count($bestFuniCols) <= 0) {
                    return null;
                }
                ksort($bestFuniCols);
                $temp = array_shift($bestFuniCols);
                $funitureWidths = array_pop($temp);

                //查找是否有456的板,而且要获取最后一个,所以不用array_search
                if (!empty(($indexs = array_keys($funitureWidths, $w456)))) {
                    $index = array_pop($indexs);//最后一个456板的下标值
                    unset($funitureWidths[$index]);
                    array_splice($funitureWidths, intval(count($funitureWidths) / 2), 0, $w456);
                }
                break;
            case 2019:
                //学生衣柜
                if (($scheme->getScheme_s_type() != -1) && (784 + 914 == $width || 784 + 1170 == $width //带有桌子的
                        || 1186 + 914 == $width || 1186 + 1170 == $width)
                ) {
                    $funitureWidths[] = $width;
                } else {
                    $w1177 = 1177;
                    $w784 = 784;
                    $max1177colcount = 1;
                    $max784colcount = intval($width / $w784) + 1;

                    for ($i1177 = 0; $i1177 <= $max1177colcount; $i1177++) {
                        for ($i784 = 0; $i784 <= $max784colcount; $i784++) {
                            $w = $i1177 * $w1177 + $i784 * $w784;
                            if (($range = abs($width - $w)) >= $errorRange) {//超过误差值就跳过
                                continue;
                            }
                            $temp = [];
                            for ($i = 0; $i < $i1177; $i++) {
                                $temp[] = $w1177;
                            }
                            for ($i = 0; $i < $i784; $i++) {
                                $temp[] = $w784;
                            }
                            $bestFuniCols[$range][] = $temp;
                        }
                    }
                    //如果没有计算出组合,则返回null
                    if ($bestFuniCols == null || count($bestFuniCols) <= 0) {
                        return null;
                    }
                    ksort($bestFuniCols);
                    $temp = array_shift($bestFuniCols);
                    $funitureWidths = array_pop($temp);
                }
                break;
            default:
                return null;
        }

        return 0 == count($funitureWidths) ? null : $funitureWidths;

    }

    /**
     * @author: Rudy
     * @time: 2017年10月17日
     * description:生成product
     * @param SchemeBean $scheme
     * @param $funitureWidths
     */
    private function generationProducts(SchemeBean $scheme, $funitureWidths)
    {
        /*
         * 计算门和宽
         */
        $s_width = $d_count = 0;
        foreach ($funitureWidths as $outWidth) {
            if (456 == $outWidth) {
                $d_count += 1;
            } elseif (1177 == $outWidth) {
                $d_count += 3;
            } elseif (784 + 914 == $outWidth || 784 + 1170 == $outWidth) {
                $d_count += 2;
            } elseif (1186 + 914 == $outWidth || 1186 + 1170 == $outWidth) {
                $d_count += 2;
            } else {
                $d_count += 2;
            }
            $s_width = $s_width + $outWidth;
        }

        $fur_list = [];
        $height = $scheme->getScheme_height();
        $color_no = $scheme->getScheme_color_no();
        $isPingXingxzz = $scheme->getScheme_s_type() == 1 ? true : false;

        foreach ($funitureWidths as $outWidth) {
            $onefur = new SchemeBean();
            $onefur->setScheme_width($outWidth);
            $onefur->setScheme_height($height);
            $onefur->setScheme_color_no($color_no);

            $cols = $wcb_products = $ncb_products = [];
            $logic = $this->getProductLogic();
            if (1177 == $outWidth) {//学生柜
                //外侧板
                $wcb_products[] = $logic->createBZYG_WCB(0, 0, 0, 0, 16,
                    $height, 570, $color_no, 0, '20');
                $wcb_products[] = $logic->createBZYG_WCB($outWidth - 16, 0, $outWidth - 16, 0, 16,
                    $height, 570, $color_no, 0, '20');
                //中侧板
                $ncb_products[] = $logic->createBZYG_ZCB(752 + 16, $height, 752 + 16, 0, 25,
                    $height, 570, $color_no, 0, '20');
                //生成列
                $cols[] = $this->getCommonCol($color_no, 752, $height);
                $cols[] = $this->get368Col($color_no);

            } elseif (784 == $outWidth) {//学生柜
                //外侧板
                $wcb_products[] = $logic->createBZYG_WCB(0, 0, 0, 0, 16,
                    $height, 570, $color_no, 0, '20');
                //外侧板
                $wcb_products[] = $logic->createBZYG_WCB($outWidth - 16, 0, $outWidth - 16, 0, 16,
                    $height, 570, $color_no, 0, '20');
                //生成列
                $cols[] = $this->getCommonCol($color_no, 752, $height);

            } elseif (784 + 914 == $outWidth || 784 + 1170 == $outWidth) {//学生桌子柜 1698 1954
                //外侧板
                $wcb_products[] = $logic->createBZYG_WCB(0, 0, 0, 0, 16,
                    $height, 570, $color_no, 0, '20');
                //组合外侧板
                $wcb_products[] = $logic->createZHG_WCB_ZJ(0, 0, $outWidth - 25, 0, 25,
                    $height, 314, $color_no, 0);
                //标准衣柜衔接侧板
                $ncb_products[] = $logic->createBZYG_ZCB_XJ(752 + 16, $height, 752 + 16, 0, 25,
                    $height, 570, $color_no, 0, '20');
                //生成列
                $cols[] = $this->getCommonCol($color_no, 752, $height);
                $cols[] = $isPingXingxzz ? $this->getPingxingXZZCol($color_no, $outWidth - 784 - 25) : $this->getChuizhiXZZCol($color_no, $outWidth - 784 - 25);
            } elseif (1186 + 914 == $outWidth || 1186 + 1170 == $outWidth) {//学生桌子柜 2100 2356
                //外侧板
                $wcb_products[] = $logic->createBZYG_WCB(0, 0, 0, 0, 16, $height, 570, $color_no, 0, '20');
                //组件外侧板
                $wcb_products[] = $logic->createZHG_WCB_ZJ(0, 0, $outWidth - 25, 0, 25, $height, 314, $color_no, 0);
                //标准衣柜中侧板
                $ncb_products[] = $logic->createBZYG_ZCB(368 + 16, $height, 368 + 16, 0, 25, $height, 570, $color_no, 0, '20');
                //标准衣柜衔接侧板
                $ncb_products[] = $logic->createBZYG_ZCB_XJ(1186 - 25, $height, 1186 - 25, 0, 25, $height, 570, $color_no, 0, '20');

                $cols[] = $this->get368Col($color_no);
                $cols[] = $this->getCommonCol($color_no, 752, $height);
                $cols[] = $isPingXingxzz ? $this->getPingxingXZZCol($color_no, $outWidth - 1186 - 25) : $this->getChuizhiXZZCol($color_no, $outWidth - 1186 - 25);
            } else {//成人柜
                /*
                 * 源码中type为20，但20的图片长度永远不够，所以此处修改成24
                 */
                $wcb_products[] = $logic->createBZYG_WCB(0, 0, 0, 0, 16, $height, 570, $color_no, 0, '24');
                $wcb_products[] = $logic->createBZYG_WCB($outWidth - 16, 0, $outWidth - 16, 0, 16, $height, 570, $color_no, 0, '24');

                $cols[] = $this->getCommonCol($color_no, $outWidth - 32, $height);
            }

            $onefur->setScheme_schemes($cols);
            $onefur->setScheme_wcb_products($wcb_products);
            $onefur->setScheme_ncb_products($ncb_products);
            $fur_list[] = $onefur;
        }

        $scheme->setScheme_door_count($d_count);
        $scheme->setScheme_schemes($fur_list);
        $scheme->setScheme_type('BZ');
        $scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));


//        $s_pic = $scheme->getScheme_width() . '_' . $scheme->getScheme_height() . '_' . $scheme->getScheme_color_no() . '_' . count($fur_list) . '_' . $d_count . '_' . getRandomInt();
        $this->createSchemePic($scheme);
        $scheme->setScheme_deep(583);
        $scheme->setScheme_width(array_sum($funitureWidths));

    }

    /**
     * @author: Rudy
     * @time: 2017年9月28日
     * description:获取常用列
     * @param $color_no
     * @param $colWidth
     * @param $height
     * @return SchemeBean
     */
    private function getCommonCol($color_no, $colWidth, $height)
    {
        if ($height == 2403) {
            $fixed_height = 1169;//成人柜固定1169高度的背板
            $ding_to_ytg = 384;
            $type = '24';
        } else {
            $fixed_height = 977;//青少年柜固定977高度的背板
            $ding_to_ytg = 352;
            $type = '20';
        }

        $col_pro = [];
        $logic = $this->getProductLogic();
        //背板1
        $col_pro[] = $logic->createBZYG_BB(0, 0, 0, 0, 12,
            $fixed_height, $colWidth, '000', 0, $type);
        //背板2
        $col_pro[] = $logic->createBZYG_BB(0, 0, 0, $height - $fixed_height-74, 12,
            $fixed_height, $colWidth, '000', 0, $type);

        //横隔板顶
        $col_pro[] = $logic->createBZYG_HGB_GD(0, $height, 0, 0, 25,
            554, $colWidth, $color_no, 0, 0, $type);
        //横隔板+衣通杆
        $col_pro[] = $logic->createBZYG_HGB_YTG(0, $height - $ding_to_ytg, 0, $ding_to_ytg, 25,
            554, $colWidth, $color_no, 1, 0, $type);
        //横隔板中
        $col_pro[] = $logic->createBZYG_HGB(0, 25 + 74 + $ding_to_ytg, 0, $height - 25 - 74 - $ding_to_ytg, 25,
            554, $colWidth, $color_no, 2, 0, $type);
        //横隔板底
        $col_pro[] = $logic->createBZYG_HGB_GD(0, 25 + 74, 0, $height - 25 - 74, 25,
            554, $colWidth, $color_no, 2, 0, $type);

        //底担
        $col_pro[] = $logic->createBZYG_DD(0, 72, 0, $height - 72, 25,
            72, $colWidth, $color_no, 0, $type);

        $col = new SchemeBean();
        $col->setScheme_products($col_pro);
        $col->setScheme_width($colWidth);
        $col->setScheme_height($height);
        $col->setScheme_color_no($color_no);
        return $col;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月28日
     * description:获取宽度为368的列
     * @param $color_no
     * @return SchemeBean
     */
    private function get368Col($color_no)
    {
        $height = 2019;

        $col_pro = [];
        $logic = $this->getProductLogic();
        //背板3
        $col_pro[] = $logic->createBZYG_BB(0, 0, 0, 977, 12,
            977, 368, '000', 0, '20');
        //背板4
        $col_pro[] = $logic->createBZYG_BB(0, 0, 0, 0, 12,
            977, 368, '000', 0, '20');

        //横隔板
        $col_pro[] = $logic->createBZYG_HGB(0, $height - 352, 0, 352, 25,
            554, 368, $color_no, 2, 0, '20');
        $col_pro[] = $logic->createBZYG_HGB(0, $height - 832, 0, 832, 25,
            554, 368, $color_no, 2, 0, '20');
        $col_pro[] = $logic->createBZYG_HGB(0, $height - 1376, 0, 1376, 25,
            554, 368, $color_no, 2, 0, '20');

        //横隔板顶
        $col_pro[] = $logic->createBZYG_HGB_GD(0, $height, 0, 0, 25,
            554, 368, $color_no, 0, 0, '20');
        $col_pro[] = $logic->createBZYG_HGB_GD(0, 25 + 74, 0, $height - 25 - 74, 25,
            554, 368, $color_no, 2, 0, '20');

        //底担
        $col_pro[] = $logic->createBZYG_DD(0, 72, 0, $height - 72, 25,
            72, 368, $color_no, 0, '20');

        $col = new SchemeBean();
        $col->setScheme_products($col_pro);
        $col->setScheme_width(368);
        $col->setScheme_height(2019);
        $col->setScheme_color_no($color_no);
        return $col;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月10日
     * description:生成平行写字桌
     * @param $color_no
     * @param $colWidth
     * @return SchemeBean
     */
    private function getPingxingXZZCol($color_no, $colWidth)
    {
        $zhWidthArr = [432, 560];
        $i = -1;
        foreach ($zhWidthArr as $idx => $v) {
            if ($colWidth == $v * 2 + 25) {
                $i = $idx;
                break;
            }
        }
        $b_type = 'SJSC';
        $s_type = '1';
        $height = 2019;
        $logic = $this->getProductLogic();
        //固定功能体
        $col = new SchemeBean();
        $col->setScheme_width($colWidth);
        $col->setScheme_height($height);
        $col->setScheme_no('Z-DZX' . $zhWidthArr[$i] . '*2-' . $color_no);
        $col->setScheme_name($zhWidthArr[$i] . '*2写字桌组件');
        $col->setScheme_b_type($b_type);
        $col->setScheme_s_type($s_type);
        $col->setScheme_color_no($color_no);
        $col->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));

        $col_pros = [];
        //背板
        $col_pros[] = $logic->createZHG_BB(0, $height, 0, 0, 12, 320 * 2 + 25, $colWidth, $color_no, 0);
        //横隔板顶
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $colWidth, $color_no, 2, 0);
        //中侧板
        $col_pros[] = $logic->createZHG_ZCB($zhWidthArr[$i], 25, $zhWidthArr[$i], 25, 25, 320 * 2 - 25, 298, $color_no, 2);
        //横隔板中
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height - 320, 0, 320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);
        $col_pros[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height - 320, $zhWidthArr[$i] + 25, 320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);
        //横隔板底
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height - 320 * 2, 0, 320 * 2, 25, 298, $colWidth, $color_no, 0, 0);


        //桌面底担
        $col_pros[] = $logic->createZHG_DD(0, 745 - 25, 0, ($height - 745 + 25), 25, 381, $colWidth, $color_no, 0);
        $col_pros[] = $logic->createZHG_DD(0, 745 - 25, 0, ($height - 745 + 25), 16, 93, $colWidth, $color_no, 0);

        $pro_desk_h = $logic->createZHG_ZZ_M(0, 745, 0, $height - 745, 25, 550, $colWidth, $color_no, 0, 0);

        $desk_pros = [];
        $desk_pros[] = $logic->createZHG_Acc(($colWidth - 59 * 8) / 2 + 80, ($height - 745) - 40 * 8, 40 * 8, 59 * 8, 'FunctionUnit/notebook3', '电脑', 0, 0);
        $desk_pros[] = $logic->createZHG_Acc(($colWidth - 83 * 8) / 2 + 80, $height - 126 * 8 + 8 * 8, 126 * 8, 83 * 8, 'FunctionUnit/chair3', '椅子', 0, 0);

        $pro_desk_h->setAcc_products($desk_pros);

        $col_pros[] = $pro_desk_h;


        $col->setScheme_products($col_pros);
        return $col;
    }

    /**
     * @author: Rudy
     * @time: 2017年10月10日
     * description:生成垂直写字桌
     * @param $color_no
     * @param $colWidth
     * @return SchemeBean
     */
    private function getChuizhiXZZCol($color_no, $colWidth)
    {
        $zhWidthArr = [432, 560];
        $i = -1;
        foreach ($zhWidthArr as $idx => $v) {
            if ($colWidth == $v * 2 + 25) {
                $i = $idx;
                break;
            }
        }
        $b_type = 'SJSC';
        $s_type = '0';
        $height = 2019;
        $logic = $this->getProductLogic();
        //固定功能体
        $col = new SchemeBean();
        $col->setScheme_width($zhWidthArr[$i] * 2 + 25);
        $col->setScheme_height($height);
        $col->setScheme_no('Z-DZG' . $zhWidthArr[$i] . '*2-' . $color_no);
        $col->setScheme_name($zhWidthArr[$i] . '*2垂直桌柜体组件');
        $col->setScheme_b_type($b_type);
        $col->setScheme_s_type($s_type);
        $col->setScheme_color_no($color_no);
        $col->setScheme_color_name(CommonMethod::getSchemeColorName($color_no));
        $col->setScheme_haveZZ(1);

        $col_pros = [];
        //背板
        $col_pros[] = $logic->createZHG_BB(0, $height, 0, 0, 12, $height - 1379, $zhWidthArr[$i] * 2 + 25, $color_no, 0);
        //横隔板顶
//        $col_pros[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);
//        $col_pros[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height, $zhWidthArr[$i] + 25, 0, 25, 298, $zhWidthArr[$i], $color_no, 0, 0);
//        $col_pros[] = $logic->createZHG_ZCB($zhWidthArr[$i], $height, $zhWidthArr[$i], 0, 25, $height - 1379, 314, $color_no, 0);

        //多层横隔板
        //横隔板顶
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $colWidth, $color_no, 2, 0);
        //中侧板
        $col_pros[] = $logic->createZHG_ZCB($zhWidthArr[$i], 25, $zhWidthArr[$i], 25, 25, 320 * 2 - 25, 298, $color_no, 0);
        //横隔板中
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height - 320, 0, 320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);
        $col_pros[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height - 320, $zhWidthArr[$i] + 25, 320, 25, 298, $zhWidthArr[$i], $color_no, 2, 0);
        //横隔板底
        $col_pros[] = $logic->createZHG_HGB_GD(0, $height - 320 * 2, 0, 320 * 2, 25, 298, $colWidth, $color_no, 0, 0);

        //平托板1
        $col_pros[] = $logic->createZHG_HGB_GD(0, 1155, 0, $height - 1155, 25, 298, $zhWidthArr[$i] * 2 + 25, $color_no, 0, 0);
        //平托板2
        $col_pros[] = $logic->createZHG_HGB_GD(0, 739, 0, $height - 739, 25, 298, $zhWidthArr[$i] * 2 + 25, $color_no, 0, 0);
        //托撑
        $col_pros[] = $logic->createZHG_DD(0, 739 - 25, 0, $height - (739 - 25), 25, 381, $zhWidthArr[$i] * 2 + 25, $color_no, 0);


        $t_p = $logic->createZHG_ZZ(0, 739 + 25, 0, $height - (739 + 25), 739 + 25, 314, 682, $color_no, 0, 0);

        $acc_products = [];
        $acc_products[] = $logic->createZHG_Acc(30 * 8, ($height - (739 + 25)) - 36 * 8, 36 * 8, 48 * 8, 'FunctionUnit/notebook1', '电脑', 0, 0);
        $acc_products[] = $logic->createZHG_Acc(682 - 35 * 8, $height - 106 * 8 + 2 * 8, 106 * 8, 77 * 8, 'FunctionUnit/chair1', '椅子', 0, 0);
        $acc_products[] = $logic->createZHG_Acc(10 * 8, ($height - (739 + 25)) - 57 * 8, 57 * 8, 44 * 8, 'FunctionUnit/desklamp1', '台灯', 0, 0);

        $t_p->setAcc_products($acc_products);

        $t_p->setM_left(intval(($colWidth - $t_p->getProduct_width()) / 8) + 1);
        $t_p->setM_left_mm($colWidth - $t_p->getProduct_width());
        //桌子放右侧
        if ($t_p->getAcc_products() != null) {
            foreach ($t_p->getAcc_products() as $a_p) {
                $a_p = change2ProductBean($a_p);
                $product_name = $a_p->getProduct_name();
                switch ($product_name) {
                    case '电脑':
                        $a_p->setM_left(intval($colWidth / 8) - 30 - 48);
                        $a_p->setM_left_mm($colWidth - 30 * 8 - 48 * 8);
                        $a_p->setProduct_pic('../Adornment/Cabinetoffice/FunctionUnit/notebook2.png');
                        break;
                    case '椅子':
                        $a_p->setM_left(intval($colWidth / 8) - intval(682 / 8) - 77 + 35);
                        $a_p->setM_left_mm($colWidth - 682 - 77 * 8 + 35 * 8);
                        $a_p->setProduct_pic('../Adornment/Cabinetoffice/FunctionUnit/chair2.png');
                        break;
                    case '台灯':
                        $a_p->setM_left(intval($colWidth / 8) - 10 - 44);
                        $a_p->setM_left_mm($colWidth - 10 * 8 - 44 * 8);
                        $a_p->setProduct_pic('../Adornment/Cabinetoffice/FunctionUnit/desklamp2.png');
                        break;
                    default:
                        break;
                }
            }
        }
        $col_pros[] = $t_p;
        $col->setScheme_products($col_pros);
        return $col;
    }

    private function onedoorcolindex(SchemeBean $scheme)
    {
        if (($schemes = $scheme->getScheme_schemes()) != null) {
            foreach ($schemes as $i => $scheme) {
                if ($scheme->getScheme_width() == 456) {
                    return $i;
                }
            }
        } else {
            return -1;
        }

    }
}