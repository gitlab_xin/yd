<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 17:58
 */

namespace app\api\logic;


use app\common\model\User;
use app\common\model\UserMoneyRecord;
use app\common\model\Withdraw;
use think\Config;
use think\Db;

class WithdrawLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:提现申请
     * @param $data
     * @return int|mixed
     */
    public static function withdraw($data)
    {
        /*
         * 1.先检查余额是否足够
         * 2.开启事务,同时写入提现表/流水表,以及扣去用户相应的金额
         */
        $user = User::get($data['user_id']);
        if($data['money']>$user->money){
            //余额不足
            return -2;
        }

        Db::startTrans();
        try{
            $after_change = ($user->money * 100 - $data['money'] * 100)/100;
            //插入提现表
            if(!Withdraw::create($data)){
                throw new \Exception('提现表插入失败',-1);
            }
            //插入流水表
            if(!UserMoneyRecord::create(['user_id'=>$data['user_id'],'before_change'=>$user->money,'after_change'=>$after_change,
                'change_var'=>$data['money'],'change_type'=>0,'type'=>'withdraw_reduce'])){
                throw new \Exception('提现表插入失败',-1);
            }
            $user->money = $after_change;
            //扣去用户金额
            if(!$user->save()){
                throw new \Exception('用户扣去金额失败',-1);
            }
            Db::commit();
            return $after_change;
        }catch (\Exception $e){
            Db::rollback();
            return $e->getCode();
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:根据用户id获取提现记录
     * @param $data
     * @return array
     *
     */
    public static function getWithdrawByUser($data)
    {
        $result = Withdraw::build()->field(['id as withdraw_id','status','create_time','money'])->where(['user_id'=>$data['user_id']])
            ->page($data['page_index'],$data['page_size'])->order(['create_time'=>'DESC'])->select();
        if($result){
            $config = Config::get('withdraw.status');
            foreach ($result as &$value){
                $value['money'] = $value['status'] == 1?'+'.$value['money']:($value['status'] == 2?'-'.$value['money']:$value['money']);
                $value['status'] = $config[$value['status']];
            }
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益记录
     * @param $user_id
     * @return array
     */
    public static function getMyBenefit($user_id)
    {
        $result = UserMoneyRecord::build()->field(['type','sum(change_var) as sum'])
            ->where(['user_id'=>$user_id,'change_type'=>1,'type'=>['in',['money_reward']]])->group('type')->select();
        if($result){
            $config = Config::get('withdraw.type');
            foreach ($result as &$value){
                $value['type_name'] = isset($config[$value['type']])?$config[$value['type']]:$value['type'];
            }
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益总额
     * @param $data
     * @return float|int
     *
     */
    public static function getBenefitSum($data)
    {
        $data['type'] = ['in',['money_reward']];
        $sum = UserMoneyRecord::build()->where($data)->sum('change_var');
        return $sum === null?0:$sum;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益明细
     * @param $data
     * @return array
     */
    public static function getMyBenefitDetail($data)
    {
        $result = UserMoneyRecord::build()->field(['change_var','change_type','type','create_time'])
            ->where($data)->order(['create_time'=>'DESC'])->select();
        if($result){
            $config = Config::get('withdraw.type');
            foreach ($result as &$value){
                $value['type_name'] = isset($config[$value['type']])?$config[$value['type']]:$value['type'];
                $value['change_var'] = $value['change_type'] == 1?'+'.$value['change_var']:'-'.$value['change_var'];
                unset($value['change_type']);
            }
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:我的收益明细,web用
     * @param $data
     * @return array
     */
    public static function getMyBenefitWeb($data)
    {
        $data['type'] = ['in',['money_reward']];
        $result = UserMoneyRecord::build()->field(['change_var','change_type','type','create_time'])
            ->where($data)->order(['create_time'=>'DESC'])->select();
        if($result){
            $config = Config::get('withdraw.type');
            foreach ($result as &$value){
                $value['type_name'] = isset($config[$value['type']])?$config[$value['type']]:$value['type'];
                $value['change_var'] = $value['change_type'] == 1?'+'.$value['change_var']:'-'.$value['change_var'];
                unset($value['change_type']);
            }
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月21日
     * description:获取提现记录详细
     * @param $data
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getWithdrawDetail($data)
    {
        return Withdraw::build()->field(['status','case status when 0 then "提现审核中" when 1 then "提现申请成功" when 2 then "提现申请失败" end as status_name',
            'remark','create_time','update_time'])->where(['id'=>$data['withdraw_id']])->find();
    }
}