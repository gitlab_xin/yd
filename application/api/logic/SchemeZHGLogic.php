<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/27
 * Time: 9:57
 */

namespace app\api\logic;

use app\common\model\SchemeZHGBean as SchemeBean;
use app\common\model\CustomizedScheme;
use app\common\tools\CommonMethod;

class SchemeZHGLogic extends SchemeBaseLogic
{
    private $productLogic = null;
    private $set = [];

    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:重构成原来参数进行验证
     * @param $scheme
     * @return array
     */
    public function getOriginParams($scheme)
    {
        $params = [];

        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';

        return $params;
    }

    /**
     * @author: Rudy
     * @time: 2017年9月28日
     * description:获取productlogic
     * @return ProductLogic|null
     */
    public function getProductLogic()
    {
        if ($this->productLogic == null) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    public function buildScheme($schemeData)
    {
        return SchemeBean::build($schemeData);
    }

    /**
     * @author: Rudy
     * @time: 2017年9月29日
     * description:计算方案数量
     * @param $data
     * @return array
     */
    public function schemeMath($data)
    {

        $scheme = new SchemeBean();

        $scheme->setScheme_height($data['height']);//高

        $range_arr = [50 => 0, 100 => 0, 150 => 0];//误差值范围

        foreach ($range_arr as $range => &$value) {
            $scheme->setScheme_error_range($range);//设置误差值
            $scheme->setScheme_width($data['width']);//宽

            $this->createZHGCWSNScheme($scheme, true);

            $value = count($scheme->getScheme_schemes());
        }
        return $range_arr;
    }

    public function createZHGScheme($data)
    {
        $index = base64_encode(http_build_query($data));
        $productList = $this->beforeCreate('zhgcwsn', $index);
        if (!empty($productList)) {
            return $productList;
        }
        $scheme = new SchemeBean();

        $scheme->setScheme_width($data['width']);//宽
        $scheme->setScheme_height($data['height']);//高
        $scheme->setScheme_deep(314);//深度
        $scheme->setScheme_error_range($data['errorrange']);//设置误差值
        $scheme->setScheme_b_type('CWSN');//类别
        $scheme->setScheme_color_no('test2');//花色
        $scheme->setScheme_type('ZH');


        $this->createZHGCWSNScheme($scheme, false);

        $productList = $scheme->getScheme_schemes();
        if (!empty($productList)) {
            array_walk($productList, function (&$row) use ($index) {
                $row->setSearch_index($index);
                $row = $row->mainInfo();
            });
            $this->afterCreate('zhgcwsn', $productList);
        }
        return $productList;
    }

    private function createZHGCWSNScheme(SchemeBean $scheme, $isMath)
    {
        $inputwidth = $scheme->getScheme_width();//宽
        $height = $scheme->getScheme_height();//高
        if ($height < 2019) {
            $height -= 16;
        }
        $width = 0;//家具宽度
        $errorrange = $scheme->getScheme_error_range();//误差值

        $w432 = 432;
        $w560 = 560;
        $w784 = 784;
        $colcount = 0;//总列数
        $gridcount = intval(($height - 74) / 320);

        $max432colcount = intval(($inputwidth - 16) / ($w432 + 25)) + 1;
        $max560colcount = intval(($inputwidth - 16) / ($w560 + 25)) + 1;
        $max784colcount = intval(($inputwidth - 16) / ($w784 + 25)) + 1;

        $s_schemes = $s_scheme = $t_schemes = $t_scheme = $s_products = $wcb_products = $ncb_products = $gai_products = null;

        $scheme->setScheme_width($width);
        $scheme->setScheme_deep(314);

        $x = 0;
        for ($i784 = 0; $i784 <= $max784colcount; $i784++) {
            for ($i560 = 0; $i560 <= $max560colcount; $i560++) {
                for ($i432 = 0; $i432 <= $max432colcount; $i432++) {
                    $colcount = $i784 + $i560 + $i432;
                    $width = 25 * ($colcount - 1) + 2 * 16 + $i432 * $w432 + $i560 * $w560 + $i784 * $w784;
                    if (abs($inputwidth - $width) < $errorrange) {
                        $s_scheme = new SchemeBean();
                        if (!$isMath) {//如果不是计算的,则需要生成方案
                            $t_schemes = [];
                            $s_scheme->setScheme_name('综合柜');
                            $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                            $s_scheme->setScheme_hole_width($inputwidth);
                            $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                            $s_scheme->setScheme_width($width);
                            $s_scheme->setScheme_height(intval($scheme->getScheme_height()));
                            $s_scheme->setScheme_wcb_type(1);
                            $s_scheme->setScheme_deep(314);
                            $s_scheme->setScheme_wcb_products($wcb_products);
                            $s_scheme->setScheme_ncb_products($ncb_products);
                            $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                            $s_scheme->setScheme_type($scheme->getScheme_type());
                            $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                            $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                            $wcb_products = $ncb_products = $gai_products = [];
                            $logic = $this->getProductLogic();

                            //外侧板
                            $wcb_products[] = $logic->createZHG_WCB(0, 0, 0, 0,
                                16, $height, 314, $scheme->getScheme_color_no(), 0);
                            $wcb_products[] = $logic->createZHG_WCB(0, 0, $width - 16, 0,
                                16, $height, 314, $scheme->getScheme_color_no(), 0);

                            $x = 16;
                            //生成432宽的柜子
                            for ($i = 0; $i < $i432; $i++) {
                                $t_scheme = new SchemeBean();
                                $t_scheme->setScheme_width($w432);
                                $t_scheme->setScheme_height($height);
                                $t_scheme->setScheme_color_no($scheme->getScheme_color_no());

                                $s_products = [];
                                if (0 < count($t_schemes)) {
                                    $ncb_products[] = $logic->createZHG_ZCB($x, 0, $x, 0,
                                        25, $height, 314, $scheme->getScheme_color_no(), 0);
                                    $x = $x + $w432 + 25;
                                } else {
                                    $x = $x + $w432;
                                }

//                                背板
//                                柜体高度441，761,1081时，背板匹配高度分别为345，653,973各一块;安装规则为插入侧板槽中，
//                                上齐平横隔板，柜体高度分别为1401；1721；2025MM时，背板匹配上下两块，
//                                分别对应高度653MM两块；973MM+653MM各一块;973MM两块
                                if ($height <= 425) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 345, $w432, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 745) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1065) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1385) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                        5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1705) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 2025) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                }

                                //横隔板顶
                                $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0,
                                    25, 298, $w432, $scheme->getScheme_color_no(), 2, 0);
                                //横隔板底
                                $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74),
                                    25, 298, $w432, $scheme->getScheme_color_no(), 2, 0);
                                //底担
                                $s_products[] = $logic->createZHG_DD(0, 73, 0, ($height - 72),
                                    25, 72, $w432, $scheme->getScheme_color_no(), 0);
                                //多层横隔板
                                for ($g = 1; $g < $gridcount; $g++) {
                                    $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                        25, 298, $w432, $scheme->getScheme_color_no(), 2, 0);
                                }
                                $t_scheme->setScheme_products($s_products);
                                $t_schemes[] = $t_scheme;
                            }
                            //生成560宽的柜子
                            for ($i = 0; $i < $i560; $i++) {
                                $t_scheme = new SchemeBean();
                                $t_scheme->setScheme_width($w560);
                                $t_scheme->setScheme_height($height);
                                $t_scheme->setScheme_color_no($scheme->getScheme_color_no());

                                $s_products = [];
                                if (0 < count($t_schemes)) {
                                    $ncb_products[] = $logic->createZHG_ZCB($x, 0, $x, 0,
                                        25, $height, 314, $scheme->getScheme_color_no(), 0);
                                    $x = $x + $w560 + 25;
                                } else {
                                    $x = $x + $w560;
                                }

                                //背板
//                                柜体高度441，761,1081时，背板匹配高度分别为345，653,973各一块;安装规则为插入侧板槽中，
//                                上齐平横隔板，柜体高度分别为1401；1721；2025MM时，背板匹配上下两块，
//                                分别对应高度653MM两块；973MM+653MM各一块;973MM两块
                                if ($height <= 425) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 345, $w560, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 745) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1065) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1385) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                        5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1705) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 2025) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                }
                                //横隔板顶
                                $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0,
                                    25, 298, $w560, $scheme->getScheme_color_no(), 2, 0);
                                //横隔板底
                                $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74),
                                    25, 298, $w560, $scheme->getScheme_color_no(), 2, 0);
                                //底担
                                $s_products[] = $logic->createZHG_DD(0, 73, 0, ($height - 72),
                                    25, 72, $w560, $scheme->getScheme_color_no(), 0);
                                //多层横隔板
                                for ($g = 1; $g < $gridcount; $g++) {
                                    $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                        25, 298, $w560, $scheme->getScheme_color_no(), 2, 0);
                                }
                                $t_scheme->setScheme_products($s_products);
                                $t_schemes[] = $t_scheme;
                            }
                            //生成784宽的柜子
                            for ($i = 0; $i < $i784; $i++) {
                                $t_scheme = new SchemeBean();
                                $t_scheme->setScheme_width($w784);
                                $t_scheme->setScheme_height($height);
                                $t_scheme->setScheme_color_no($scheme->getScheme_color_no());

                                $s_products = [];
                                if (0 < count($t_schemes)) {
                                    $ncb_products[] = $logic->createZHG_ZCB($x, 0, $x, 0,
                                        25, $height, 314, $scheme->getScheme_color_no(), 0);
                                    $x = $x + $w784 + 25;
                                } else {
                                    $x = $x + $w784;
                                }

                                //背板
//                                柜体高度441，761,1081时，背板匹配高度分别为345，653,973各一块;安装规则为插入侧板槽中，
//                                上齐平横隔板，柜体高度分别为1401；1721；2025MM时，背板匹配上下两块，
//                                分别对应高度653MM两块；973MM+653MM各一块;973MM两块
                                if ($height <= 425) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 345, $w784, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 745) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1065) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1385) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                        5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 1705) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                } elseif ($height <= 2025) {
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                        5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                        5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                }
                                //横隔板顶
                                $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0,
                                    25, 298, $w784, $scheme->getScheme_color_no(), 2, 0);
                                //横隔板底
                                $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74),
                                    25, 298, $w784, $scheme->getScheme_color_no(), 2, 0);
                                //底担
                                $s_products[] = $logic->createZHG_DD(0, 73, 0, ($height - 72),
                                    25, 72, $w784, $scheme->getScheme_color_no(), 0);
                                //多层横隔板
                                for ($g = 1; $g < $gridcount; $g++) {
                                    $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                        25, 298, $w784, $scheme->getScheme_color_no(), 2, 0);
                                }
                                $t_scheme->setScheme_products($s_products);
                                $t_schemes[] = $t_scheme;
                            }
                            //盖板
                            if (2019 > $height) {
                                //单列
                                if ($width + 4 < 2434) {
                                    $gai_products[] = $logic->createZHG_GB(0, 0, -2, -16,
                                        16, 334, $width + 4, $scheme->getScheme_color_no(), 0, 0);
                                } else {
//                                    $g_width = 17;
//                                    $l = 0;
//                                    for ($i = 0; $i < count($t_schemes); $i++) {
//                                        //超出最大长度
//                                        $temp = change2SchemeBean($t_schemes[$i]);
//                                        if (2434 < $g_width + $temp->getScheme_width() + 25) {
//                                            $gai_products[] = $logic->createZHG_GB($l, 0, $l, -16,
//                                                16, 334, $g_width - 13, $scheme->getScheme_color_no(), 0, 0);
//                                            $l = $g_width - 13;
//                                            if ($i == count($t_schemes) - 1) {
//                                                $g_width = 13 + $temp->getScheme_width() + 17;
//                                            } else {
//                                                $g_width = 13 + $temp->getScheme_width() + 25;
//                                            }
//                                        } else {
//                                            if ($i == count($t_schemes) - 1) {
//                                                $g_width += $temp->getScheme_width() + 17;
//                                            } else {
//                                                $g_width += $temp->getScheme_width() + 25;
//                                            }
//                                        }
//                                    }
//                                    $gai_products[] = $logic->createZHG_GB($l, 0, $l, -16,
//                                        16, 334, $g_width, $scheme->getScheme_color_no(), 0, 0);


                                    $scheme_count = count($t_schemes);
                                    $scheme_g_width_arr = [];
                                    //计算出每个单元柜的盖板宽度,然后再进行最优组合的计算
                                    for ($i = 0; $i < $scheme_count; $i++) {
                                        //盖板需要超出外侧板2mm,所以18=16+2    每块盖板间隔1mm,且位于中侧板中间分割,所以12=(25-1)/2
                                        if ($i == 0) {
                                            $scheme_g_width_arr[] = 18 + $t_schemes[$i]->getScheme_width();
                                        } elseif( $i == $scheme_count - 1){
                                            $scheme_g_width_arr[] = 24 + $t_schemes[$i]->getScheme_width() + 18;
                                        } else {
                                            $scheme_g_width_arr[] = 24 + $t_schemes[$i]->getScheme_width();
                                        }
                                    }

                                    $this->set = [];
                                    $a = [];
                                    //整数拆分，获取所有可能性，目前保留2的情况,如 11=4+4+3 表示4块盖板+4块盖板+3块盖板,因为长度超过2434需要对盖板进行均分
                                    $this->q(count($scheme_g_width_arr), count($scheme_g_width_arr), 0, $a, 2);

                                    $g_arr = [];
                                    foreach ($this->set as $arr) {
                                        //获取里面所有可能性,并计算方差,获取方差最小的一个组合
                                        $last = 0;
                                        $single_one = [];
                                        foreach ($arr as $num) {
                                            $single_one[] = array_slice($scheme_g_width_arr, $last, $num);
                                            $last += $num;
                                        }
                                        if (($variance = $this->getVariance($single_one)) !== -1) {//此处会计算方差
                                            $g_arr[$variance][] = $single_one;
                                        }
                                    }
                                    if (empty($g_arr)) {
                                        $this->q(count($scheme_g_width_arr), count($scheme_g_width_arr), 0, $a, 3);

                                        $g_arr = [];
                                        foreach ($this->set as $arr) {
                                            //获取里面所有可能性,并计算方差,获取方差最小的一个组合
                                            $last = 0;
                                            $single_one = [];
                                            foreach ($arr as $num) {
                                                $single_one[] = array_slice($scheme_g_width_arr, $last, $num);
                                                $last += $num;
                                            }
                                            if (($variance = $this->getVariance($single_one)) !== -1) {//此处会计算方差
                                                $g_arr[$variance][] = $single_one;
                                            }
                                        }
                                    }
                                    ksort($g_arr);//排序,按照方案对数组进行从小到大的排序,然后获取第一个,方差最小代表盖板相对均分
                                    $best_list = array_shift($g_arr);
                                    $best = $best_list[0];

                                    $g_left = -2;
                                    foreach ($best as $g) {
                                        $gai_products[] = $logic->createZHG_GB(0, 0, $g_left, -16,
                                            16, 334, $g, $scheme->getScheme_color_no(), 0, 0);
                                        $g_left = $g_left + $g + 1;//m_left要算上中侧板的1mm距离
                                    }
                                }
//                                $l = 0;
//                                $w = 16;

                                /*
								 * int mW = 432; if (0 == i432) { if (0 == i560)
								 * { mW = 784; } else { mW = 560; } }
								 */
                                // 大于一块盖板
//                                $gai_products_count = count($gai_products);
//                                if (1 < $gai_products_count) {
//                                    $avg = intval(($width + 2) / $gai_products_count);
//                                    for ($p = 0; $p < $gai_products_count - 1; $p++) {
//                                        $product_temp = change2ProductBean($gai_products[$p]);
//                                        $l = $product_temp->getR_left();
//                                        for ($i = 0; $i < count($t_schemes); $i++) {
//                                            $t_scheme_temp = change2SchemeBean($t_schemes[$i]);
//
//                                            if (($w + $t_scheme_temp->getScheme_width()) > ($avg * ($p + 1))) {
//                                                $p_temp = change2ProductBean($gai_products[$p]);
//                                                $p_next_temp = change2ProductBean($gai_products[$p + 1]);
//
//                                                $gai_products[$p + 1] = $logic->createZHG_GB(($w - 12 + $l),
//                                                    0,
//                                                    ($w - 12 + $l),
//                                                    -16,
//                                                    16,
//                                                    334,
//                                                    ($p_temp->getProduct_width() - ($w - 12) + $p_next_temp->getProduct_width()),
//                                                    $scheme->getScheme_color_no(),
//                                                    0,
//                                                    0);
//
//                                                $gai_products[$p] = $logic->createZHG_GB($l, 0, $l, -16,
//                                                    16, 334, $w - 12, $scheme->getScheme_color_no(), 0, 0);
//
//                                                $l = $w - 12;
//                                                $w = 13;
//                                            } else {
//                                                $w += $t_scheme_temp->getScheme_width() + 25;
//                                            }
//                                        }
//                                    }
//                                }
                            }

                            $s_scheme->setScheme_error_range($errorrange);
                            $s_scheme->setScheme_wcb_products($wcb_products);
                            $s_scheme->setScheme_ncb_products($ncb_products);
                            $s_scheme->setScheme_g_products($gai_products);
                            $s_scheme->setScheme_schemes($t_schemes);

//                            $s_pic = 'CWSN' . $s_scheme->getScheme_width() . '_' . $s_scheme->getScheme_height() . '_' . $scheme->getScheme_color_no()
//                                . '_' . count($t_schemes) . '_' . getRandomInt();
                            $this->createSchemePic($s_scheme);

                            $s_scheme->setScheme_name('书柜及综合柜类-' . getRandomInt());
                        }
                        $s_schemes[] = $s_scheme;
                    }
                }
            }
        }
        if (!empty($s_schemes)) {
            $s_schemes = array_reverse($s_schemes);
        }
        $scheme->setScheme_schemes($s_schemes);
    }

    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'user_id' => $userId,
            'scheme_type' => 'ZH',
            'scheme_s_type' => -1,
            'scheme_b_type' => 'CWSN',
            'scheme_type_text' => '书柜及综合柜类',
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 314,
            'scheme_door_count' => 0,
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => '000',
            'scheme_sk' => -1,
            'scheme_hole_type' => 0,
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => 314,
            'scheme_hole_left' => 0,
            'scheme_hole_right' => 0,
            'is_left_wall' => '0',
            'is_right_wall' => '0',
            'is_left_wall_zt' => '0',
            'is_right_wall_zt' => '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model->getLastInsID();
    }

    public function update(SchemeBean $schemeBean, $schemeData, $userId, $scheme_id)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
        ];
        return CustomizedScheme::update($schemeModelData, ['user_id' => $userId, 'id' => $scheme_id]);
    }

    private function printPartition($i, &$a, $count)
    {
        $tem = array_slice($a, 0, $i + 1);
        $tem_count = count($tem);
        if ($tem_count == 1 || $tem_count > $count) {
            return;
        }
        $this->set[] = $tem;
    }

    private function q($n, $m, $i, &$a, $count)
    {
        if ($n < $m) {
            return $this->q($n, $n, $i, $a, $count);
        }
        $a[$i] = $m;
        if ($n == 0 || $m == 0) {
            $this->printPartition($i, $a, $count);
            return 0;
        }
        if ($n == 1 || $m == 1) {
            if ($n == 1) {

                $this->printPartition($i, $a, $count);
            } else {
                $this->q($n - 1, 1, $i + 1, $a, $count);
            }
            return 1;
        }
        if ($n == $m) {
            $this->printPartition($i, $a, $count);
            return 1 + $this->q($n, $n - 1, $i, $a, $count);
        }
        return $this->q($n - $m, $m, $i + 1, $a, $count) + $this->q($n, $m - 1, $i, $a, $count);
    }

    private function getVariance(&$arr)
    {
        foreach ($arr as &$tem) {
            $tem = array_sum($tem) + count($tem) - 1;//单个盖板之间整合需要填上中侧板的1mm
            if ($tem > 2434) {
                return -1;
            }
        }
        $avg = intval(array_sum($arr) / count($arr));

        $ret = 0;

        foreach ($arr as $single) {
            $ret += pow($single - $avg, 2);
        }
        return intval($ret / count($arr));
    }
}