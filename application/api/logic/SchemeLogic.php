<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 15:49
 */

namespace app\api\logic;

use app\common\model\MyDecorateProduct;
use app\common\model\MyDecorateProductCart;
use app\common\model\ShoppingCart;
use think\Config;
use app\common\model\CustomizedScheme as SchemeModel;

class SchemeLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取我的方案列表
     * @param $user_id
     * @param $scheme_type
     * @param $page_index
     * @param $page_size
     * @return array|bool
     */
    public static function getMySchemes($user_id, $scheme_type, $page_index, $page_size)
    {
        try {
            $where = ['user_id' => $user_id];
            $where['parent_id'] = 0;
            switch ($scheme_type) {
                case 'all':
                    break;
                default:
                    list($scheme_type, $scheme_b_type) = explode('-', $scheme_type . '-');
                    $where['scheme_type'] = $scheme_type;
                    $where['scheme_b_type'] = $scheme_b_type;
                    break;
            }
            $result = SchemeModel::build()
                ->field(['id as scheme_id', 'scheme_type', 'scheme_b_type', 'scheme_name', 'scheme_pic', 'create_time', 'update_time'])
                ->where($where)->page($page_index, $page_size)->order(['id' => 'DESC'])->select()->toArray();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:获取我的方案总数
     * @param $user_id
     * @param $scheme_type
     * @return int|string
     */
    public static function getMySchemesCount($user_id, $scheme_type)
    {
        $where = ['user_id' => $user_id];
        switch ($scheme_type) {
            case 'all':
                break;
            default:
                list($scheme_type, $scheme_b_type) = explode('-', $scheme_type . '-');
                $where['scheme_type'] = $scheme_type;
                $where['scheme_b_type'] = $scheme_b_type;
                break;
        }
        return SchemeModel::build()->where($where)->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取方案图片
     * @param $scheme_id
     * @return array|bool|false|\PDOStatement|string|\think\Model
     */
    public static function getSchemePic($scheme_id)
    {
        try {
            return SchemeModel::build()->field(['id as scheme_id', 'scheme_pic', 'create_time'])->where(['id' => $scheme_id])->find();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:获取方案详情
     * @param $scheme_id
     * @return array|int
     */
    public static function getSchemeDetail($scheme_id)
    {
        try {
//            $files = ['scheme_type_text as classify',//分类类型 如 入墙式
//                'scheme_hole_type as hole_type',//洞口类型
//                'scheme_hole_width as hole_width',//方案洞口宽度
//                'scheme_hole_height as hole_top_height',//方案洞口高度
//                'scheme_hole_sl_height as beam_height',//洞口上梁高度
//                'scheme_hole_left as left_cylinder_width',//方案洞口左边空间
//                'scheme_hole_right as right_cylinder_width',//方案洞口右边空间
//                'scheme_sk as end_cap',//收口条 -1无 004白色 000灰色
//                'scheme_door_count as door_count',//可安装的门数量
//                'scheme_pic'
//            ];

            $files = ['id','scheme_index', 'user_id', 'scheme_sk_color_no', 'is_left_wall', 'is_right_wall',
                'is_left_wall_zt', 'is_right_wall_zt', 'serial_array', 'statement_array','create_time','update_time'];


            return ($scheme = SchemeModel::build()->field($files, true)->where(['id' => $scheme_id])->find()) == null ? -2 : $scheme->toArray();

        } catch (\Exception $e) {
            return -1;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:删除我的方案
     * @param $user_id
     * @param $scheme_id
     * @return int
     */
    public static function deleteMyScheme($user_id, $scheme_id)
    {
        try {
            if (($model = SchemeModel::build()->where(['user_id' => $user_id, 'id' => $scheme_id])->find()) == null) {
                return -2;
            }
            return SchemeModel::build()->where(['user_id' => $user_id, 'id|parent_id' => $scheme_id])->delete();
        } catch (\Exception $e) {
            return -1;
        }
    }

    public static function updateProduct($requestData, $user_id){
        $info = MyDecorateProduct::build()
            ->where(['id' => $requestData['decorate_product_id']])
            ->field([
                'room_id', 'decorate_id', 'type', 'name', 'supplier_id'
            ])
            ->find();


        $scheme = SchemeModel::build()
            ->alias('s')
            ->join('yd_customized_color c','c.color_no = s.scheme_color_no')
            ->where(['s.id' => $requestData['scheme_id'],'c.is_deleted' => '0','c.is_show' => '1'])
            ->field(['s.scheme_pic','c.color_id'])
            ->find();

//        $data['type'] = '1';
//        $data['product_id'] = $requestData['scheme_id'];
//        $data['scheme_pic'] = $scheme['scheme_pic'];
        $price = CustomizedPayLogic::getCharge($requestData['scheme_id'], 1);
//        $data['original_price'] = $price['0']['scheme_dis_price'];
//        $data['discount_price'] = $price['0']['scheme_dis_price'];
//        $data['create_time'] = time();

        //定制类更新价格，图片等参数
        if ($info['type'] == '1'){
            MyDecorateProduct::build()
                ->where(['id' => $requestData['decorate_product_id']])
                ->update([
                    'color_id' => $scheme['color_id'],
                    'name' => $info['name'],
                    'image' => $scheme['scheme_pic'],
                    'original_price' =>  $price['0']['scheme_dis_price'],
                    'discount_price' =>  $price['0']['scheme_dis_price'],
                    'update_time' => time()
                ]);

            ShoppingCart::build()
                ->where(['decorate_product_id' => $requestData['decorate_product_id'], 'product_id' => $requestData['scheme_id']])
                ->update([
                    'color_id' => $scheme['color_id'],
                    'count' => $requestData['count'],
                    'update_time' => time()
                ]);
            return true;
        }

        MyDecorateProduct::build()
            ->insert([
                'room_id' => $info['room_id'],
                'decorate_id' => $info['decorate_id'],
                'supplier_id' => $info['supplier_id'],
                'type' => '1',
                'color_id' => $scheme['color_id'],
                'name' => $info['name'],
                'product_id' => $requestData['scheme_id'],
                'image' => $scheme['scheme_pic'],
                'original_price' =>  $price['0']['scheme_dis_price'],
                'discount_price' =>  $price['0']['scheme_dis_price'],
                'create_time' =>  time(),
            ]);
        $id =  MyDecorateProduct::build()->getLastInsID();

        ShoppingCart::build()
            ->insert([
                'decorate_product_id' => $id,
                'count' => $requestData['count'],
                'product_id' => $requestData['scheme_id'],
                'decorate_id' => $info['decorate_id'],
                'color_id' => $scheme['color_id'],
                'user_id' => $user_id,
                'type' => '1',
                'from' => '2',
                'create_time' => time()
            ]);


        return true;

    }

}