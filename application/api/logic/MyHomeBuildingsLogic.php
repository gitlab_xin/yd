<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\logic;


use app\admin\logic\MyHomeBuildingsFloorLogic;
use app\common\model\MyAssembleBuildingsLabel;
use app\common\model\MyAssemblyBuildings;
use app\common\model\MyAssemblyBuildingsConfig;
use app\common\model\MyAssemblyHouseConfig;
use app\common\model\MyAssemblyHouseDecorate;
use app\common\model\MyAssemblyHouseType;
use app\common\model\MyDecorateProduct;
use app\common\model\MyDecorateProductCart;
use app\common\model\MyDecorateProductCollect;
use app\common\model\MyDecorateRoom;
use app\common\model\MyHomeBuildings;
use app\common\model\BuildingsCollect as CollectModel;
use app\common\model\MyHomeBuildingsFloor;
use app\common\model\MyHomeBuildingsRidgepole;
use app\common\model\MyHomeBuildingsUnit;
use app\common\model\ShoppingCart;
use app\common\model\UserLabel;
use app\common\model\UserToken;
use app\common\tools\RedisUtil;
use app\common\tools\wechatUtil;
use think\Db;

class MyHomeBuildingsLogic
{
    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取楼盘列表
     * @param int $user_id
     * @param $province
     * @param string $city
     * @param int $page
     * @param int $num
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getBuildingsList($user_id = 0, $province, $city = "", $page = 1, $num = 20)
    {
        $where['b.is_deleted'] = 0;
        $where['b.province'] = $province;
        $collect_condition = " AND c.user_id = {$user_id}";
        !empty($city) ? $where['b.city'] = $city : true;
        return MyHomeBuildings::build()->alias('b')
            ->join('yd_my_home_buildings_collect c', 'c.buildings_id=b.buildings_id' . $collect_condition, 'left')
            ->where($where)
            ->field(['b.buildings_id', 'b.name', 'b.address', 'b.house_type', 'b.begin_time', 'b.poster_src', 'IF(ISNULL(c.id),"no","yes") as is_collect','b.province','b.city'])
            ->group('b.buildings_id')
            ->page($page, $num)
            ->select();
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/16
     * Time: 3:30 下午
     * description 易家整配列表
     * @param $province
     * @param string $city
     * @param int $page
     * @param int $num
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getBuildingsList2($requestData, $page = 1, $num = 20)
    {
        $where = [
            'b.is_deleted' => '0',
//            'b.province' => $requestData['province'],
            'b.city' => $requestData['city'],
        ];

        if (!empty($requestData['keyword'])){
            $where['b.name'] = ['like', "%{$requestData['keyword']}%"];
        }
        $list['list'] = MyAssemblyBuildings::build()
            ->alias('b')
            ->where($where)
            ->field([
                'b.buildings_id',
                'b.name',
                'b.house_type_src',
                'b.province',
                'b.city',
                'b.buildings_id house_count',
                'b.buildings_id decorate_count',
                ])
            ->group('b.buildings_id')
            ->page($page, $num)
            ->select();
        $list['count'] = MyAssemblyBuildings::build()
            ->alias('b')
            ->where($where)
            ->count();

        return $list;
    }

    /**
     * User: zhaoxin
     * Date: 2020/1/16
     * Time: 5:00 下午
     * description
     * @param $requestData
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getBuildingsInfo($requestData){
        $list =  MyAssemblyBuildings::build()
            ->where(['buildings_id' => $requestData['buildings_id']])
            ->field([
                'buildings_id house_count',
                'buildings_id decorate_count',
                'name',
                'city'
            ])
            ->find();

        $list['list'] = MyAssemblyBuildingsConfig::build()
            ->where(['buildings_id' => $requestData['buildings_id'],'level' => '1'])
            ->field([
                'id',
                'name',
                'id list'
            ])
            ->select();

        return $list;
    }

    public static function alls($requestData, $page = 1, $num = 20){
        $list['name'] =  MyAssemblyBuildings::build()
            ->where(['buildings_id' => $requestData['buildings_id']])
            ->value('name');
        $list['city'] =  MyAssemblyBuildings::build()
            ->where(['buildings_id' => $requestData['buildings_id']])
            ->value('city');
        $where = ['h.buildings_id' => $requestData['buildings_id'],'h.is_deleted' => '0'];

        //分布子id
        $config_ids = [];
        if (!empty($requestData['id'])){
            self::getHouseIds($requestData['id'], $config_ids);
        }

        if (count($config_ids) >= 1){
            //楼盘所有关联的户型ids
            $house_ids = MyAssemblyHouseConfig::build()
                ->where(['config_id' => ['in', $config_ids]])
                ->column('house_id');
            $where['h.house_id'] = ['in', $house_ids];
        }


        $list['list'] = MyAssemblyHouseType::build()
            ->alias('h')
            ->where($where)
            ->field([
                'h.house_id',
                'h.name',
                'h.proportion',
                'h.img_src',
                'h.bedroom',
                'h.house_id decorate_count',
            ])
            ->page($page, $num)
            ->select();

        $where['d.is_deleted'] = '0';
        $list['count'] = MyAssemblyHouseType::build()
            ->alias('h')
            ->join('yd_my_assembly_house_decorate d','d.house_id = h.house_id')
            ->where($where)
            ->count();

        return $list;
    }

    public static function getHouseIds($id, &$config_ids){
        //查询是否是最后一级
        $count = MyAssemblyBuildingsConfig::build()
            ->where(['parent_id' => $id])
            ->count();

        //最后一级
        if ($count <= 0){
            $config_ids[] = $id;
        } else {
            $ids = MyAssemblyBuildingsConfig::build()
                ->where(['parent_id' => $id])
                ->column('id');
            foreach ($ids as $k => $v){
                self::getHouseIds($v, $config_ids);
            }
        }

    }





    /**
     * @author: Airon
     * @time: 2018年4月
     * description:搜素楼盘
     * @param int $user_id
     * @param $keyword
     * @param int $page
     * @param int $num
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function searchBuildings($user_id = 0, $keyword, $page = 1, $num = 20)
    {
        $where['b.is_deleted'] = 0;
        $where['b.name'] = ['like', "%{$keyword}%"];
        $collect_condition = " AND c.user_id = {$user_id}";
        !empty($city) ? $where['b.city'] = $city : true;
        return MyHomeBuildings::build()->alias('b')
            ->join('yd_my_home_buildings_collect c', 'c.buildings_id=b.buildings_id' . $collect_condition, 'left')
            ->where($where)
            ->field(['b.buildings_id', 'b.name', 'b.address', 'b.house_type', 'b.begin_time', 'b.poster_src', 'IF(ISNULL(c.id),"no","yes") as is_collect','b.province','b.city'])
            ->group('b.buildings_id')
            ->page($page, $num)
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年8月9日
     * description: 收藏和取消收藏
     * @param int $user_id 用户ID
     * @param array $data
     * @return int|string
     */
    public static function buildingsCollect($user_id, $data)
    {
        // 验证是否已经点赞
        $isExists = CollectModel::isExists($user_id, $data['buildings_id']);

        $result = false;
        switch ($data['collect_type']) {
            case 'collect':
                if (!$isExists) {//如果不存在的话才进行点赞
                    $result = CollectModel::addCollect(['user_id' => $user_id, 'buildings_id' => $data['buildings_id']]);
                }
                break;
            case 'cancel':
                if ($isExists) {//如果存在的话才进行取消点赞
                    $result = CollectModel::delCollect($user_id, $data['buildings_id']);
                }
                break;
            default:
                break;
        }
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取栋
     * @param $buildings_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getRidgepole($buildings_id)
    {
        return MyHomeBuildingsRidgepole::build()->where(['buildings_id' => $buildings_id, 'is_deleted' => 0])->field(['ridgepole_id', 'name'])->select();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取楼层
     * @param $buildings_id
     * @param $ridgepole_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getFloor($buildings_id, $ridgepole_id)
    {
        return MyHomeBuildingsFloor::build()->where(['buildings_id' => $buildings_id, 'ridgepole_id' => $ridgepole_id, 'is_deleted' => 0])->field(['floor_id', 'name'])->select();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取单元
     * @param $buildings_id
     * @param $floor_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getUnit($buildings_id, $floor_id)
    {
        return MyHomeBuildingsUnit::build()->where(['buildings_id' => $buildings_id, 'floor_id' => $floor_id, 'is_deleted' => 0])->field(['unit_id', 'name'])->select();
    }

    public static function getHouseType($buildings_id, $ridgepole_id = 0, $floor_id = 0, $unit_id = 0)
    {
        $r_condition = empty($ridgepole_id) ? "" : " AND r.ridgepole_id = {$ridgepole_id}";
        $f_condition = empty($floor_id) ? "" : " AND f.floor_id = {$floor_id}";
        $u_condition = empty($unit_id) ? "" : " AND u.unit_id = {$unit_id}";
        return MyHomeBuildings::build()->alias('b')
            ->join('yd_my_home_buildings_ridgepole r', 'r.buildings_id = b.buildings_id' . $r_condition)
            ->join('yd_my_home_buildings_floor f', 'f.ridgepole_id = r.ridgepole_id' . $f_condition)
            ->join('yd_my_home_buildings_unit u', 'u.floor_id = f.floor_id' . $u_condition)
            ->join('yd_my_home_house_type h', 'h.house_id = u.house_id')
            ->where(['b.buildings_id' => $buildings_id, 'r.is_deleted'=>0,'b.is_deleted' => 0,'h.is_deleted' => 0,'u.is_deleted' => 0,'f.is_deleted' => 0])
            ->field(['h.house_id', 'h.name','f.floor_id', 'h.bedroom', 'h.proportion', 'h.elevator_count', 'h.orientation', 'h.img_src'])
            ->group('h.house_id')
            ->select();
    }

    public static function getInfo($buildings_id)
    {
        return MyHomeBuildings::build()
            ->where(['buildings_id' => $buildings_id, 'is_deleted' => 0])
            ->field(['create_time', 'update_time', 'collect_count','is_deleted'], true)
            ->find();
    }

    public static function getMyBuildingCollections($data){
        try {
            $result = ['collects' => [], 'count' => 0];

            $collects = CollectModel::build()->alias('c')
                ->field(['b.buildings_id', 'b.name', 'b.address', 'b.house_type', 'b.begin_time', 'b.poster_src', 'b.province', 'b.city', 'b.is_deleted'])
                ->join('yd_my_home_buildings b', 'c.buildings_id = b.buildings_id', 'right')
                ->where(['c.user_id' => $data['user_id']])
                ->group('c.buildings_id')
                ->page($data['page_index'], $data['page_size'])
                ->select()
                ->toArray();

            if ($collects) {
                foreach ($collects as $key => &$value) {
                    $value['is_valid'] = ($value['is_deleted'] == '1') ? 0 : 1;//是否有效
                }
                $result['buildings'] = $collects;
                $result['count'] = CollectModel::build()->where(['user_id' => $data['user_id']])->count();
            }
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getUnitDecorates($requestData, $page = 1, $num = 20){
        $house_id = MyAssemblyHouseConfig::build()
            ->where([
                'config_id' => $requestData['id']
            ])
            ->value('house_id');
        $data = self::getHouseDecorates($house_id, $page, $num, $requestData['id']);
        $info = MyAssemblyBuildingsConfig::build()
            ->alias('c')
            ->join('yd_my_assembly_buildings b','b.buildings_id = c.buildings_id')
            ->where([
                'id' => $requestData['id']
            ])
            ->field([
                'id config_name','b.name buildings_name'
            ])
            ->find();
        $data['info']['config_name_data'] = $info['config_name'];
        $data['info']['buildings_namessss_data'] = $info['buildings_name'];
        return $data;
    }

    public static function getHouseDecorates($house_id, $page, $num, $config_id = ''){
        $list['info'] = MyAssemblyHouseType::build()
            ->alias('t')
            ->join('yd_my_assembly_buildings b','b.buildings_id = t.buildings_id')
            ->where(['t.house_id' => $house_id,'t.is_deleted' => '0'])
            ->field([
                't.name house_name','t.proportion','t.bedroom','b.name buildings_name','t.img_src'
            ])
            ->find()
            ->toArray();

        $list['info']['config_name'] = '';
        if (!empty($config_id)){
            MyAssemblyHouseType::getConfigName($config_id, $config_name);
            $list['info']['config_name'] = $config_name;
        }



        $list['list'] = unserialize(RedisUtil::getInstance()->get("house_decorate_".$house_id));
        if (empty($list['list'])){
            $list['list'] = MyAssemblyHouseDecorate::build()
                ->where(['house_id' => $house_id,'is_deleted' => '0'])
                ->field([
                    'program_id', 'name', 'img_src', 'video', 'shape_spark_url', 'tag label_list',
                    'label',
                ])
                ->order(['create_time' => 'DESC'])
                ->select();

            $token = get_token();
            $user_config_ids = [];
            if (!empty($token)) {
                $userToken = new UserToken();
                $userToken = $userToken->getTokenInfo($token);
                $config_text = UserLabel::build()
                    ->where(['user_id' => $userToken['user_id']])
                    ->value('label_ids');
                $config_text = json_decode($config_text, true);
                //根据用户调研的文本数据，查询对应分类id
                $user_config_ids = MyAssembleBuildingsLabel::build()
                    ->where(['name' => ['in', $config_text], 'is_deleted' => '0', 'level' => '2'])
                    ->column('id');
            }
            foreach ($list['list'] as &$v){
                $v['matchcount'] = $user_config_ids;
            }
            $list['list'] = collection($list['list'])->toArray();
            array_multisort(array_column($list['list'],'matchcount'), SORT_DESC, $list['list']);

            RedisUtil::getInstance()->set("house_decorate_".$house_id, serialize($list['list']), 30);
        }




        return $list;
    }

    public static function getDeppConfigId($config_id_list, &$config_id){
        foreach ($config_id_list as $k => $v){
            $config_id = $v['id'];
            if (count($v['list']) <= 0){
                return $config_id;
            }
            self::getDeppConfigId($v['list'], $config_id);

        }
    }

    public static function getDecoratesInfo($id){

        $data = MyAssemblyHouseDecorate::build()
            ->alias('d')
            ->join('yd_my_assembly_house_type t','t.house_id = d.house_id')
            ->join('yd_my_assembly_buildings b','b.buildings_id = t.buildings_id')
            ->where(['d.program_id' => $id])
            ->field([
                'd.program_id','d.name decorate_name','d.img_src decorate_img_src','d.video decorate_video',
                'd.shape_spark_url decorate_shape_spark_url',
                't.name house_name','t.proportion','t.bedroom','b.name buildings_name','t.house_id config_name',
                'd.tag label_list','d.program_id product_room_list'
            ])
            ->find();

        $token = get_token();
        if (!empty($token)) {
            $userToken = new UserToken();
            $userToken = $userToken->getTokenInfo($token);

            $data['is_collect'] = MyDecorateProductCollect::build()
                ->where(['user_id' => $userToken['user_id'], 'decorate_product_id' => $id, 'type' => '2'])
                ->count();
        }

        return $data;
    }

    public static function getProductList($requestData){

        $where = ['p.decorate_id' => $requestData['program_id'], 'p.is_delete' => '0', 'p.type' => ['<>', '1']];

        if (!empty($requestData['room_id'])){
            $where['p.room_id'] = $requestData['room_id'];
        }
        $data['list'] = MyDecorateProduct::build()
            ->alias('p')
            ->join('yd_customized_scheme p_s','p_s.id = p.product_id', 'left')
            ->join('yd_shop_product_standard p_s_1','p_s_1.standard_id = p.standard_id', 'left')
            ->where($where)
            ->field([
                'p.id','p.type','p.name','p.original_price','IFNULL(p_s_1.price, p.discount_price) discount_price','p.image','p.color_id product_color_id',
                'p.product_id','p_s.scheme_type','p_s.scheme_b_type','p.standard_id'
            ])
            ->page($requestData['page_index'], $requestData['page_size'])
            ->select();

        $token = get_token();
        $data['cart_count'] = 0;
        $data['price_info'] = [];
        if (!empty($token)) {
            $userToken = new UserToken();
            $userToken = $userToken->getTokenInfo($token);
            $cart_info = ShoppingCart::build()
                ->alias('c')
                ->join('yd_my_decorate_product p','p.id = c.decorate_product_id')
                ->where(['c.user_id' => $userToken['user_id'],'c.decorate_id' => $requestData['program_id'], 'c.from' => '2'])
                ->field([
                    'count(c.cart_id) as cart_count', '(sum(p.original_price) * c.count) as total_original_price',
                    '(sum(p.discount_price) * c.count) as total_discount_price'
                ])
                ->group('c.decorate_id')
                ->select();

            $price_info = ShoppingCart::build()
                ->alias('c')
                ->join('yd_my_decorate_product p','p.id = c.decorate_product_id')
                ->join('yd_my_decorate_room r','r.id = p.room_id')
                ->where([
                    'user_id' => $userToken['user_id'],'c.decorate_id' => $requestData['program_id'], 'c.from' => '2'
                ])
                ->field([
                    'r.name','(sum(p.discount_price) * c.count) total_discount_price'
                ])
                ->group('p.room_id')
                ->select();
            if (count($cart_info) > 0){
                $data['cart_count'] = $cart_info['0']['cart_count'];
                $data['price_info']['total_original_price'] = $cart_info['0']['total_original_price'];
                $data['price_info']['total_discount_price'] = $cart_info['0']['total_discount_price'];
                $data['price_info']['detail'] = $price_info;
            }
        }

        return $data;
    }
}