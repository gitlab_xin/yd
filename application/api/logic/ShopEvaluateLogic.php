<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/7
 * Time: 14:50
 */

namespace app\api\logic;

use app\common\model\ShopEvaluate as EvaluateModel;
use app\common\model\ShopEvaluatePraise as EvaluatePraiseModel;
use app\common\model\ShopOrder;
use think\Db;

class ShopEvaluateLogic
{
    /**
     * @author: Airon
     * @time: 2017年8月7日
     * description:获取商品评价列表
     * @param $product_id
     * @param $type
     * @param int $user_id
     * @param $page
     * @param $num
     * @return array|bool
     */
    public static function getList($product_id, $type, $user_id = 0, $page, $num)
    {
        //$type all well mediocre bad image
        $where['e.product_id'] = $product_id;
        switch ($type) {
            case 'all':
                break;
            case 'well':
                $where['e.star'] = ['in', [4, 5]];
                break;
            case 'mediocre':
                $where['e.star'] = 3;
                break;
            case 'bad':
                $where['e.star'] = ['in', [1, 2]];
                break;
            case 'image':
                $where['e.img_src_list'] = ['neq',''];
                break;
        }
        $sql = EvaluateModel::build()->alias('e')
            ->join('yd_user u', 'u.user_id = e.user_id')
            ->join('yd_shop_product_standard stan', 'stan.standard_id = e.standard_id');
        $field = [
            'e.evaluate_id',
            'u.user_id',
            'u.username',
            'u.avatar',
            'stan.name as standard_name',
            'e.star',
            'e.content',
            'e.is_anonymous',
            'e.img_src_list',
            'e.praise_count',
            'e.create_time',
        ];
        if (!empty($user_id)) {
            $sql = $sql->join('yd_shop_evaluate_praise praise',
                "praise.evaluate_id = e.evaluate_id AND praise.user_id = {$user_id}",
                'LEFT');
            $field[] = 'IF(ISNULL(praise.id),"no","yes") as is_praise';

        }
        $sql = $sql->where($where)->field($field)->order('e.create_time', 'DESC')->page($page,
            $num)->select()->toArray();
        if ($sql === false) {
            return false;
        }
        if ($sql) {
            foreach ($sql as $key => $value) {
//                $sql[$key]['img_src'] = empty($value['img_src_list']) ? array() : explode('|', $value['img_src_list']);
//                unset($sql[$key]['img_src_list']);todo img_src_list 修改为 带有尺寸的字典格式,后台不再处理
                if ($value['is_anonymous'] == 1 && $value['user_id'] != $user_id) {
                    $sql[$key]['username'] = config('config')['shop_anonymous']['username'];
                    $sql[$key]['avatar'] = config('config')['shop_anonymous']['avatar'];
                }
                //由于用户没登录时没有进行连表,所以在处理数据时加上字段是否点赞和是否收藏的字段返回
                if (empty($user_id)) {
                    $sql[$key]['is_praise'] = 'no';
                }
            }

        }

        return $sql;
    }

    /**
     * @author: Airon
     * @time: 2017年8月7日
     * description:获取商品评价列表每种类型的数量
     * @param $product_id
     * @return $this|array|bool
     */
    public static function getTypeCount($product_id)
    {
        //$type all well mediocre bad image
        $result['all'] = EvaluateModel::whereCount(['product_id' => $product_id]);
        $result['well'] = EvaluateModel::whereCount(['product_id' => $product_id, 'star' => ['in', [4, 5]]]);
        $result['mediocre'] = EvaluateModel::whereCount(['product_id' => $product_id, 'star' => 3]);
        $result['bad'] = EvaluateModel::whereCount(['product_id' => $product_id, 'star' => ['in', [1, 2]]]);
        $result['image'] = EvaluateModel::whereCount(['product_id' => $product_id, 'img_src_list' => ['neq',""]]);
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2017年8月7日
     * description:商品评价 点赞/取消点赞
     * @param $user_id
     * @param $evaluate_id
     * @param $praise_type
     * @return array|bool
     */
    public static function articlePraise($user_id, $evaluate_id, $praise_type)
    {

        // 验证是否已经点赞
        $isExists = EvaluatePraiseModel::isExists($user_id, $evaluate_id);

        $result = true;
        switch ($praise_type) {
            case 'praise':
                if (!$isExists) {//如果不存在的话才进行点赞
                    $result = EvaluatePraiseModel::addPraise(['user_id' => $user_id, 'evaluate_id' => $evaluate_id]);
                }
                break;
            case 'cancel':
                if ($isExists) {//如果存在的话才进行取消点赞
                    $result = EvaluatePraiseModel::delPraise($user_id, $evaluate_id);
                }
                break;
            default:
                break;
        }
        return $result;

    }

    /**
     * @author: Rudy
     * @time: 2017年8月28日
     * description:
     * @param $data
     * @return int|mixed
     */
    public static function evaluate($data)
    {
        if(($order = ShopOrder::build()->where(['user_id'=>$data['user_id'],'order_id'=>$data['order_id']])->find()) == null){
            //订单不存在
            return 0;
        }
        if(EvaluateModel::build()->where(['user_id'=>$data['user_id'],'order_id'=>$data['order_id']])->find()){
            //已评价过
            return 2;
        }

        $data['product_id'] = $order->product_id;
        $data['standard_id'] = $order->standard_id;
        Db::startTrans();
        try{
            if(!$order->isUpdate()->save(['status'=>'已完成'])){
                throw new \Exception('',-2);
            }
            if(!EvaluateModel::create($data)){
                throw new \Exception('',-1);
            }
            Db::commit();
            return 1;
        }catch (\Exception $e){
            Db::rollback();
            return $e->getCode();
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月28日
     * description:获取我的商品评价
     * @param $data
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getMyEvaluate($data)
    {
        return EvaluateModel::build()->alias('e')
            ->field([
                'u.username',
                'u.avatar',
                'stan.name as standard_name',
                'e.evaluate_id',
                'e.star',
                'e.content',
                'e.is_anonymous',
                'e.img_src_list',
                'e.praise_count',
                'e.create_time',
                'IF(ISNULL(praise.id),"no","yes") as is_praise'
            ])
            ->join('user u','u.user_id = e.user_id','LEFT')
            ->join('yd_shop_product_standard stan', 'stan.standard_id = e.standard_id')
            ->join('yd_shop_evaluate_praise praise',
                "praise.evaluate_id = e.evaluate_id AND praise.user_id = {$data['user_id']}",
                'LEFT')
            ->where(['e.user_id'=>$data['user_id'],'e.order_id'=>$data['order_id']])
            ->find();
    }
}