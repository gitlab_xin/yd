<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\logic;

use app\common\model\CustomizedScheme;
use app\common\model\ProductBean;
use app\common\model\SchemeBean;

class SchemeYMJOrderLogic
{
    public $productLogic = null;

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    public function complete($scheme_id){
        $schemes = CustomizedScheme::build()->where(['parent_id'=>$scheme_id,'ymj_def'=>'1'])->select();
        $schemes_complete = [];
        foreach ($schemes as $key=>$schemeModel){
            $schemeData = json_decode($schemeModel['serial_array'], true);

            $schemeLogic = new SchemeYMJLogic();
            $schemeBean = $schemeLogic->buildScheme($schemeData);

            $orderLogic = new SchemeYMJOrderLogic();
            $schemeBean = $orderLogic->scoreRQSSchemeZJProducts($schemeBean);
            $schemeBean = $orderLogic->scoreRQSSchemeProducts($schemeBean);
            $schemeBean = $orderLogic->scoreRQSSchemePlates($schemeBean);
            $schemeBean = $orderLogic->scoreRQSSchemeWujin($schemeBean);

//            if ($schemeBean->getScheme_have_door()) {
//                $schemeBean = $orderLogic->scoreRQSSchemeDoor($schemeBean);
//            } else {//衣帽间的柜子都没有门
                $schemeBean->setScheme_door_price(0);
                $schemeBean->setScheme_dis_door_price(0);
                $schemeBean->setScheme_door_area(0);
                $schemeBean->setScheme_score_door_products(null);
//            }

            //柜体构件明细 -> 构件清单
            $panelProducts = [];
            foreach ($schemeBean->getScheme_score_products() as $item) {
                $item = change2ProductBean($item);
                $panelProducts[] = $item->scoreInfo();
            }
            //柜体构件明细 -> 用料明细
            $plateProducts = [];
            foreach ($schemeBean->getScheme_score_plates() as $item) {
                $item = change2ProductBean($item);
                $plateProducts[] = $item->scoreInfo();
            }

            //功能组件明细
            $zjProducts = [];
            foreach ($schemeBean->getScheme_score_zj_products() as $item) {
                $item = change2ProductBean($item);
                $zjProducts[] = $item->scoreInfo();
            }

            //五金附件明细
            $wujinProducts = [];
            foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
                $item = change2ProductBean($item);
                $wujinProducts[] = $item->scoreInfo();
            }

            $schemeBean->setScheme_price($schemeBean->getScheme_price() + $schemeBean->getScheme_gt_price());
            $schemeBean->setScheme_price($schemeBean->getScheme_price() + $schemeBean->getScheme_door_price());
            $schemeBean->setScheme_price($schemeBean->getScheme_price() + $schemeBean->getScheme_wujin_price());

//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_gt_price());
//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_door_price());
//        $scheme->setScheme_dis_price($scheme->getScheme_dis_price() + $scheme->getScheme_dis_sk_price());

            $schemeBean->setScheme_dis_price(getMoney($schemeBean->getScheme_price() * $schemeBean->getScheme_discount()));
            $scheme_complete = array();
            $scheme_complete['scheme_name'] = $schemeBean->getScheme_name();
            $scheme_complete['scheme_pic'] = $schemeBean->getScheme_pic();
            $scheme_complete['zj_products'] = $schemeBean->getScheme_name();
            $scheme_complete['scheme_name'] = $schemeBean->getScheme_name();
            $scheme_complete['scheme_pic'] = $schemeBean->getScheme_pic();
            $scheme_complete['zj_products'] = $zjProducts;
            $scheme_complete['score_products'] = $panelProducts;
            $scheme_complete['plate_products'] = $plateProducts;
            $scheme_complete['wujin_products'] = $wujinProducts;
            $scheme_complete['zj_price'] = formatMoney($schemeBean->getScheme_zj_price(), 2);
            $scheme_complete['dis_zj_price'] = formatMoney($schemeBean->getScheme_dis_zj_price(), 2);
            $scheme_complete['wujin_price'] = formatMoney($schemeBean->getScheme_wujin_price(), 2);
            $scheme_complete['dis_wujin_price'] = formatMoney($schemeBean->getScheme_dis_wujin_price(), 2);
            $scheme_complete['shoukou_price'] = formatMoney($schemeBean->getScheme_sk_price(), 2);
            $scheme_complete['dis_shoukou_price'] = formatMoney($schemeBean->getScheme_dis_sk_price(), 2);
            $scheme_complete['plate_price'] = formatMoney($schemeBean->getScheme_plate_price(), 2);
            $scheme_complete['plate_dis_price'] = formatMoney($schemeBean->getScheme_dis_plate_price(), 2);
            $scheme_complete['gt_price'] = formatMoney($schemeBean->getScheme_gt_price(), 2);
            $scheme_complete['gt_dis_price'] = formatMoney($schemeBean->getScheme_dis_gt_price(), 2);
            $scheme_complete['scheme_price'] = formatMoney($schemeBean->getScheme_price(), 2);
            $scheme_complete['scheme_dis_price'] = formatMoney($schemeBean->getScheme_dis_price(), 2);
            $scheme_complete['discount'] = formatMoney($schemeBean->getScheme_discount(), 2);
            $scheme_complete['install_structure_pic'] = empty($schemeData['install_structure_pic'])?"":$schemeData['install_structure_pic'];
            $schemes_complete[] = $scheme_complete;
        }
        return $schemes_complete;
    }
    public function scoreRQSSchemeZJProducts(SchemeBean $scheme)
    {

        $score_products = [];
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    $c_p->setProduct_is_score(0);
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (empty($c_p->getProduct_is_score())) {
                        $productNo = $c_p->getProduct_no();
                        if (is_starts_with($productNo, 'L-')
                            || is_starts_with($productNo, 'LB-')
                            || is_starts_with($productNo, 'K-')
                        ) {
                            $c_p->setProduct_count(1);
                            foreach ($scheme_products as $t_p) {
                                $t_p = change2ProductBean($t_p);
                                if (!empty($t_p->getCom_products())) {
                                    foreach ($t_p->getCom_products() as $t_c_p) {
                                        $t_c_p = change2ProductBean($t_c_p);
                                        if ($c_p != $t_c_p && empty($t_c_p->getProduct_is_score())) {
                                            if ($c_p->getProduct_no() == $t_c_p->getProduct_no()) {
                                                $c_p->setProduct_count($c_p->getProduct_count() + 1);
                                                $t_c_p->setProduct_is_score(1);
                                            }
                                        }
                                    }
                                }
                            }
                            $c_p->setProduct_is_score(1);
                            $score_products[] = $c_p;
                        }

                        //处理组合内的拉篮和裤抽 Near
                        if ($c_p->getProduct_type() == 'ZH') {
                            if (!empty($c_p->getCom_products())) {
                                foreach ($c_p->getCom_products() as $c_p_c) {
                                    $productNo = $c_p_c->getProduct_no();
                                    if (is_starts_with($productNo, 'L-')
                                        || is_starts_with($productNo, 'LB-')
                                        || is_starts_with($productNo, 'K-')
                                    ) {
                                        $c_p_c->setProduct_color_no("");//组件没有颜色
                                        $c_p_c->setProduct_count(1);
                                        foreach ($scheme_products as $t_p) {
                                            $t_p = change2ProductBean($t_p);
                                            if (!empty($t_p->getCom_products())) {
                                                foreach ($t_p->getCom_products() as $t_c_p) {
                                                    $t_c_p = change2ProductBean($t_c_p);
                                                    if ($c_p_c != $t_c_p && empty($t_c_p->getProduct_is_score())) {
                                                        if ($c_p_c->getProduct_no() == $t_c_p->getProduct_no()) {
                                                            $c_p_c->setProduct_count($c_p_c->getProduct_count() + 1);
                                                            $t_c_p->setProduct_is_score(1);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $c_p_c->setProduct_is_score(1);
                                        $score_products[] = $c_p_c;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

//        Collections.sort(score_products, new ProductRptSort_Seq()); // todo: 不排序可能有问题

        $width = 0;
        $totalPrice = 0;
        $dis_totalPrice = 0;
        $discount = $scheme->getScheme_discount();
        foreach ($score_products as $p) {
            $p = change2ProductBean($p);
            if (is_starts_with($p->getProduct_no(), "L")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(410, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(450, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(530, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(610, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(690, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            } elseif (is_starts_with($p->getProduct_no(), "K-")) {
                $width = $p->getProduct_width();
                if ($width < 496) {
                    $this->countProductMoney(350, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 592) {
                    $this->countProductMoney(385, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 688) {
                    $this->countProductMoney(420, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 784) {
                    $this->countProductMoney(455, $discount, $p, $totalPrice, $dis_totalPrice);
                } elseif ($width < 880) {
                    $this->countProductMoney(490, $discount, $p, $totalPrice, $dis_totalPrice);
                } else {
                    $this->countProductMoney(525, $discount, $p, $totalPrice, $dis_totalPrice);
                }
            }
        }

        $scheme->setScheme_score_zj_products($score_products);
        $scheme->setScheme_zj_price($totalPrice);
        $scheme->setScheme_dis_zj_price($dis_totalPrice);

        return $scheme;
    }

    public function scoreRQSSchemeProducts(SchemeBean $scheme)
    {
        $scheme->setScheme_price(0);
        $scheme->setScheme_dis_price(0);
        $score_products = [];

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            $p->setProduct_is_score(0);
        }
        if (!empty($scheme->getScheme_ncb_products())) {
            foreach ($scheme->getScheme_ncb_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }
        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
        }

        foreach ($scheme->getScheme_wcb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_wcb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        foreach ($scheme->getScheme_ncb_products() as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                $p->setProduct_count(1);
                foreach ($scheme->getScheme_ncb_products() as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

        if (!empty($scheme->getScheme_dg_products())) {
            foreach ($scheme->getScheme_dg_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dg_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }

        if (!empty($scheme->getScheme_dt_products())) {
            foreach ($scheme->getScheme_dt_products() as $p) {
                $p = change2ProductBean($p);
                if (empty($p->getProduct_is_score())) {
                    $p->setProduct_count(1);
                    foreach ($scheme->getScheme_dt_products() as $t_p) {
                        $t_p = change2ProductBean($t_p);
                        if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                            if ($p->getProduct_no() == $t_p->getProduct_no()) {
                                $p->setProduct_count($p->getProduct_count() + 1);
                                $t_p->setProduct_is_score(1);
                            }
                        }
                    }
                    $p->setProduct_is_score(1);
                    $score_products[] = $p;
                }
            }
        }

        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            foreach ($s->getScheme_products() as $p) {
                $p = change2ProductBean($p);
                $p->setProduct_is_score(0);
            }
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $no = "";

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if (empty($p->getProduct_is_score())) {
                if (str_has($p->getProduct_no(), 'ytg')) {
                    $no = $p->getProduct_no();
                    $no = substr($no, 0, strlen($no) - 3);
                    $p->setProduct_no($no);
                    $p->setProduct_name("自定义衣柜横隔板");
                }
                $p->setProduct_count(1);
                foreach ($scheme_products as $t_p) {
                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && empty($t_p->getProduct_is_score())) {
                        if (str_has($t_p->getProduct_no(), 'ytg')) {
                            $no = $t_p->getProduct_no();
                            $no = substr($no, 0, strlen($no) - 3);
                            $t_p->setProduct_no($no);
                        }
                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;
            }
        }

//		Collections.sort(score_products, new ProductRptSort_Seq());

        $scheme->setScheme_score_products($score_products);

        return $scheme;
    }

    public function scoreRQSSchemePlates(SchemeBean $scheme)
    {
        $totalPrice = 0;
        $disTotalPrice = 0;

        $deep_arr = [25, 16, 12, 5];

        $p_product = null;
        $scheme_plates = [];
        for ($i = 0; $i < 4; $i++) {
            $p_product = new ProductBean();
            $p_product->setProduct_deep($deep_arr[$i]);
            $p_product->setProduct_name($deep_arr[$i] . 'mm厚');

            foreach ($scheme->getScheme_score_products() as $p) {
                $p = change2ProductBean($p);
                if ($deep_arr[$i] == $p->getProduct_deep()) {
                    if ($deep_arr[$i] == 12) { // 背板
                        $p_product->setProduct_area(
                            number_format(getLength($p->getProduct_width() + 24) * getLength($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    } else {
                        // 其他板
                        $p_product->setProduct_area(
                            number_format(getLength($p->getProduct_width()) * getLength($p->getProduct_height()) / 1000000, 2, '.', '')
                            * $p->getProduct_count()
                            + $p_product->getProduct_area()
                        );
                    }
                }
            }

            $disCount = $scheme->getScheme_discount();
            if (0 < $p_product->getProduct_area()) {
                $p_product->setProduct_price(getPlatePrice($i));
                $this->countPlateMoney(getPlatePrice($i), $disCount, $p_product, $totalPrice, $disTotalPrice);
            }
            $scheme_plates[] = $p_product;
        }
        $scheme->setScheme_score_plates($scheme_plates);
        $scheme->setScheme_plate_price($totalPrice);
        $scheme->setScheme_dis_plate_price($disTotalPrice);

        return $scheme;
    }

    public function scoreRQSSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = [];

        foreach ($scheme->getScheme_schemes() as $s) {
            $s = change2SchemeBean($s);
            $scheme_products = array_merge($scheme_products, $s->getScheme_products());
        }

        $wujinMap = [];
        $t_p = null;
        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            if ($p->getProduct_type() == 'ZB') {
                if (!isset($wujinMap['W01-02-XX-00']) || empty($wujinMap['W01-02-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("背板连接件");
                    $t_p->setProduct_no("W01-02-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setSeq(2);
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-02-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("13沉头自攻钉");
                    $t_p->setProduct_no("W02-01-3*13-00");
                    $t_p->setProduct_count(4 * 3);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(7);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*13-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                }
            } elseif ($p->getProduct_type() == 'ZH') {
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            } elseif ($p->getProduct_type() == 'ZH-YTG') {
                if (!isset($wujinMap['W01-01-XX-00']) || empty($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("层板连接件");
                    $t_p->setProduct_no("W01-01-XX-00");
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
                $p_no = "W06-01-" . ($p->getProduct_width() - 12) . "-00";
                if (!isset($wujinMap[$p_no]) || empty($wujinMap[$p_no])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通杆");
                    $t_p->setProduct_width($p->getProduct_width() - 12);
                    $t_p->setProduct_no($p_no);
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("支");
                    $t_p->setSeq($p->getProduct_width());
                    $t_p->setProduct_spec(($p->getProduct_width() - 12) . "mm");
                    $t_p->setProduct_price(formatMoney(
                        100 * ((getLength($p->getProduct_width() - 12)) / 1000)
                    ));
                    $t_p->setProduct_dis_price($t_p->getProduct_price() * $scheme->getScheme_discount());
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap[$p_no];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
                if (!isset($wujinMap['W04-07-XX-00']) || empty($wujinMap['W04-07-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通托五金包");
                    $t_p->setProduct_no("W04-07-XX-00");
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1200);
                    $t_p->setProduct_price(formatMoney(14));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-07-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
//                if (!isset($wujinMap['W02-01-3*13-00']) || empty($wujinMap['W02-01-3*13-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("13沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*13-00");
//                    $t_p->setProduct_count(2);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(7);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*13-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                }
//                if (!isset($wujinMap['W02-01-3*25-00']) || empty($wujinMap['W02-01-3*25-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("25沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*25-00");
//                    $t_p->setProduct_count(4);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(10);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*25-00'];
//                    $t_p = change2ProductBean($t_p);
//                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                }
            }

            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (is_starts_with($c_p->getProduct_no(), "L-")
                        || is_starts_with($c_p->getProduct_no(), "LB-")
                    ) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    } elseif (is_starts_with($c_p->getProduct_no(), "K-")) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    }
                }
            }

        }
        $wujinProducts = array_values($wujinMap);

        $totalPrice = 0;
        $dis_totalPrice = 0;
        foreach ($wujinProducts as $w_p) {
            $w_p = change2ProductBean($w_p);
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
        // Collections.sort(wujin_products, new ProductRptSort_Seq()); // todo:
        $scheme->setScheme_score_wujin_products($wujinProducts);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            $scheme->getScheme_dis_plate_price()
            + $scheme->getScheme_dis_wujin_price()
            + $scheme->getScheme_dis_zj_price()
        ));
        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));

        return $scheme;
    }

    private function countProductMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price($price);
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_count())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_count())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }

    private function countPlateMoney($price, $discount, ProductBean $productBean, &$totalPrice, &$disTotalPrice)
    {
        $productBean->setProduct_price(formatMoney($price));
        $productBean->setProduct_total_price(
            formatMoney($productBean->getProduct_price() * $productBean->getProduct_area())
        );
        $productBean->setProduct_dis_price(
            formatMoney($productBean->getProduct_price() * $discount)
        );
        $productBean->setProduct_dis_total_price(
            formatMoney($productBean->getProduct_dis_price() * $productBean->getProduct_area())
        );
        $totalPrice = formatMoney($totalPrice + $productBean->getProduct_total_price());
        $disTotalPrice = formatMoney($disTotalPrice + $productBean->getProduct_dis_total_price());
    }
}
