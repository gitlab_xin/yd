<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:19
 */

namespace app\api\logic;

use app\common\model\ShopOrder;
use app\common\model\ShopOrderArea;
use app\common\model\ShopOrderCharge;
use app\common\model\ShopOrderLogistics;
use app\common\model\ShopOrderOfficialLogistics;
use app\common\model\ShopOrderProduct;
use app\common\model\ShopOrderRefund;
use app\common\model\ShoppingCart as ShoppingCartModel;
use app\common\model\ShoppingCart;
use app\common\model\ShopProduct;
use app\common\model\ShopProductStandard;
use app\common\model\ShopOrderLogistics as LogisticsModel;
use app\common\model\ShopOrderOfficialLogistics as OfficialLogisticsModel;
use app\common\tools\kdniaoUtil;
use app\common\tools\RedisUtil;
use think\Cache;
use think\Config;
use think\Db;
use think\Exception;
use app\common\tools\wechatPay;
use app\common\tools\alipayClient;

class ShopOrderLogic
{
    /**
     * @author: Airon
     * @time: 2017年8月4日
     * description:减少库存
     * @param $standard_id
     * @param $count
     * @return int|true
     */
    public function decStocks($standard_id, $count)
    {
        try {
            return ShopProductStandard::build()
                ->alias('s')
                ->where(array(
                    's.standard_id' => $standard_id,
                    's.is_deleted' => '0',
                    's.stocks' => ['egt', $count]
                ))->setDec('stocks', $count);
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @author: Airon
     * @time: 2017年8月1日
     * description:购买单件商品时验证商品是否已下架或者已删除以及商品规格是否存在
     * @param $product_id
     * @param $standard_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function checkProduct($product_id, $standard_id)
    {
        return ShopProduct::build()
            ->alias('p')
            ->join('yd_shop_product_standard s',
                "s.product_id = p.product_id AND s.is_deleted = '0'")
            ->where(array(
                'p.is_deleted' => '0',
                'p.is_sale' => '1',
                'p.product_id' => $product_id,
                's.standard_id' => $standard_id
            ))->field('p.product_id,p.supplier_id,s.standard_id,s.stocks')->find();
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:购物车 验证商品是否已下架或者已删除
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function cartDeletedOrSale($cart_id_list, $user_id)
    {
        return $deletedOrSale = ShoppingCartModel::build()
            ->alias('cart')
            ->join('yd_shop_product p',
                "p.product_id = cart.product_id AND (p.is_deleted = '1' OR p.is_sale = '0')")
            ->where(array(
                'cart.cart_id' => ['in', $cart_id_list],
                'cart.user_id' => $user_id
            ))->field('cart.cart_id,cart.product_id')->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:购物车 验证商品规格是否已被删除
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function cartStandardDelete($cart_id_list, $user_id)
    {
        return $deletedOrSale = ShoppingCartModel::build()
            ->alias('cart')
            ->join('yd_shop_product_standard s',
                "s.product_id = cart.product_id AND s.standard_id = cart.standard_id AND s.is_deleted = '1'")
            ->where(array(
                'cart.cart_id' => ['in', $cart_id_list],
                'cart.user_id' => $user_id
            ))->field('cart.cart_id,cart.product_id')->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:购物车 验证请求购物车内数据是否都存在和符合
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function cartCheckCount($cart_id_list, $user_id)
    {
        return $deletedOrSale = ShoppingCartModel::build()
            ->alias('cart')
            ->join('yd_shop_product p',
                "p.product_id = cart.product_id AND (p.is_deleted = '0' OR p.is_sale = '1')")
            ->join('yd_shop_product_standard s',
                "s.product_id = cart.product_id AND s.standard_id = cart.standard_id AND s.is_deleted = '0'")
            ->where(array(
                'cart.cart_id' => ['in', $cart_id_list],
                'cart.user_id' => $user_id,
                'cart.from' => '1'
            ))->field('cart.cart_id,cart.product_id')->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:购物车 验证商品是否库存充足
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function cartCheckStock($cart_id_list, $user_id)
    {
        $cart_id_list = implode("','", $cart_id_list);
        $where = "cart.cart_id in ('{$cart_id_list}')  AND cart.user_id = {$user_id} AND cart.count > s.stocks AND cart.from = 1";
        return $deletedOrSale = ShoppingCartModel::build()
            ->alias('cart')
            ->join('yd_shop_product_standard s',
                "s.product_id = cart.product_id AND s.standard_id = cart.standard_id")
            ->join('yd_shop_product p', 'p.product_id = cart.product_id')
            ->where($where)
            ->field('cart.cart_id,cart.product_id,s.stocks,p.name as product_name')->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:购物车 锁上对应库存数据
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function lockStock($cart_id_list, $user_id)
    {
        $where = [
            'cart.cart_id' => ['in', $cart_id_list],
            'cart.user_id' => $user_id
        ];
        return $deletedOrSale = ShopProductStandard::build()->lock(true)
            ->alias('ps')
            ->join('yd_shop_shopping_cart cart',
                "cart.product_id = ps.product_id AND ps.standard_id = cart.standard_id")
            ->where($where)
            ->field('cart.cart_id,cart.product_id')->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:购物车支付
     * @param $cart_id_list
     * @param $user_id
     * @param $out_trade_no
     * @param $userArea
     * @param $requestData
     * @return bool|array
     */
    public function cartAddOrder($cart_id_list, $user_id, $out_trade_no, $userArea, $requestData)
    {
        $result = array();
        try {
            $field = [
                'cart.cart_id',
                'cart.count',
                'p.product_id',
                'cart.standard_id',
                'ps.price',
                'p.supplier_id',
                'ps.price',
                'p.freight_charge',
                'p.processing_charge',
                'p.home_charge',
                'p.service_charge',
                'ps.stocks',
                'p.name as product_name',
                'ps.name as standard_name',
                'p.img_src_list',
            ];
            $where = [
                'cart.cart_id' => ['in', $cart_id_list],
                'cart.user_id' => $user_id,
                'cart.from' => '1',
                'p.is_deleted' => '0',
                'p.is_sale' => '1',
                'ps.is_deleted ' => '0'
            ];

            $info = ShoppingCartModel::build()->alias('cart')
                ->join('yd_shop_product p', 'p.product_id = cart.product_id')
                ->join('yd_shop_product_standard ps', 'cart.standard_id = ps.standard_id')
                ->where($where)
                ->field($field)
                ->select();
            if (empty($info)) {
                return false;
            }
            $orderSum = 0;
            $order_id_array = [];
            foreach ($info as $key => $value) {
                $sum = $value['price'] + $value['freight_charge']
                    + $value['processing_charge'] + $value['service_charge']
                    + $value['home_charge'];//商品单价+所有其它费用

                $order_info['product_id'] = $value['product_id'];
                $order_info['standard_id'] = $value['standard_id'];
                $order_info['pay_type'] = $requestData['pay_type'];
                $order_info['count'] = $value['count'];
                $order_info['price'] = round($sum * $value['count'], 2);//订单总价
                $order_info['supplier_id'] = $value['supplier_id'];
                $order_info['user_id'] = $user_id;
                $order_info['order_num'] = substr(md5(uniqid(mt_rand(), true)), 0, 4) . time();
                $order_info['bill_no'] = $out_trade_no;
                $order_info['create_time'] = time();
                $order_info['user_remarks'] = $this->cartGetUserRemarks($value['cart_id'], $requestData['cart_array']);
                $order_id = ShopOrder::build()->insertGetId($order_info);//写入订单主要信息
                if (empty($order_id)) {
                    return false;
                }

                $result['order_id'][] = $order_id;

                $dec_bool = $this->decStocks($value['standard_id'], $value['count']);//减库存
                if (empty($dec_bool)) {
                    return false;
                }
                //商城 加销量,  暂定于确认收货后添加销量

                $userArea['order_id'] = $order_id;
                $userArea['user_id'] = $user_id;
                $area_bool = ShopOrderArea::build()->insert($userArea);//写入收货人信息
                if (empty($area_bool)) {
                    return false;
                }

                $orderProduct['order_id'] = $order_id;
                $orderProduct['product_name'] = $value['product_name'];
                $orderProduct['standard_name'] = $value['standard_name'];
                $orderProduct['img_src_list'] = $value['img_src_list'];
                $area_bool = ShopOrderProduct::build()->insert($orderProduct);//写入商品信息
                if (empty($area_bool)) {
                    return false;
                }
                $charge = [
                    'order_id' => $order_id,
                    'product_price' => $value['price'],
                    'freight_charge' => $value['freight_charge'],
                    'processing_charge' => $value['processing_charge'],
                    'home_charge' => $value['home_charge'],
                    'service_charge' => $value['service_charge'],
                    'count' => $value['count']
                ];
                $charge_bool = ShopOrderCharge::build()->insert($charge);//写入价格信息
                if (empty($charge_bool)) {
                    return false;
                }
                $orderSum += $sum;
                $order_id_array[] = $order_id;
            }
            $result['order_id_array'] = $order_id_array;
            $result['fee'] = $orderSum;
            return $result;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:写入订单数据
     * @param $requestData
     * @param $out_trade_no
     * @param $user_id
     * @param $userArea
     * @return false|\PDOStatement|string|\think\Collection|array
     */
    public function addProductOrder($requestData, $user_id, $out_trade_no, $userArea)
    {
        $result = array();
        try {
            $product_info = ShopProduct::build()
                ->alias('p')
                ->join('yd_shop_product_standard s',
                    "s.product_id = p.product_id AND s.standard_id = {$requestData['standard_id']}")
                ->where(['p.product_id' => $requestData['product_id']])
                ->field([
                    'p.product_id',
                    's.standard_id',
                    's.price',
                    'p.supplier_id',
                    'p.freight_charge',
                    'p.processing_charge',
                    'p.service_charge',
                    'p.home_charge',
                    'p.name as product_name',
                    's.name as standard_name',
                    'p.img_src_list'
                ])->find();
            if (empty($product_info)) {
                return false;
            }
            $sum = $product_info['price'] + $product_info['freight_charge']
                + $product_info['processing_charge'] + $product_info['service_charge']
                + $product_info['home_charge'];//商品单价+所有其它费用

            $order_info = $requestData;
            $order_info['price'] = round($sum * $requestData['count'], 2);//订单总价
            $order_info['supplier_id'] = $product_info['supplier_id'];
            $order_info['user_id'] = $user_id;
            $order_info['order_num'] = substr(md5(uniqid(mt_rand(), true)), 0, 4) . time();;
            $order_info['bill_no'] = $out_trade_no;
            $order_info['create_time'] = time();
            $order_id = ShopOrder::build()->insertGetId($order_info);//写入订单主要信息
            if (empty($order_id)) {
                return false;
            }

            $result['order_id'] = $order_id;

            $dec_bool = $this->decStocks($product_info['standard_id'], $requestData['count']);//减库存
            if (empty($dec_bool)) {
                return false;
            }

            //商城 加销量,  暂定于确认收货后添加销量

            $userArea['order_id'] = $order_id;
            $userArea['user_id'] = $user_id;
            $area_bool = ShopOrderArea::build()->insert($userArea);//写入收货人信息
            if (empty($area_bool)) {
                return false;
            }

            $orderProduct['order_id'] = $order_id;
            $orderProduct['product_name'] = $product_info['product_name'];
            $orderProduct['standard_name'] = $product_info['standard_name'];
            $orderProduct['img_src_list'] = $product_info['img_src_list'];
            $area_bool = ShopOrderProduct::build()->insert($orderProduct);//写入商品信息
            if (empty($area_bool)) {
                return false;
            }
            $charge = [
                'order_id' => $order_id,
                'product_price' => $product_info['price'],
                'freight_charge' => $product_info['freight_charge'],
                'processing_charge' => $product_info['processing_charge'],
                'home_charge' => $product_info['home_charge'],
                'service_charge' => $product_info['service_charge']
            ];
            $charge_bool = ShopOrderCharge::build()->insert($charge);//写入价格信息
            if (empty($charge_bool)) {
                return false;
            }
            $result['fee'] = $sum;
            return $result;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月12日
     * description:修改规定时间内未支付订单状态
     */
    public function cancelExpiredOrders()
    {
        $deadline = Config::get('mall.order')['deadline'];
        $fields = ['order_id', 'standard_id', 'count', 'bill_no'];
        $where = [
            'status' => '未支付',
            'create_time' => ['lt', time() - $deadline]
        ];
        $lists = ShopOrder::build()->field($fields)->where($where)->select()->toArray();

        if ($lists) {
            $redis = RedisUtil::getInstance();
            $orderData = ['status' => '已取消'];
            foreach ($lists as $row) {
                Db::startTrans();
                try {
                    if (!ShopOrder::update($orderData, ['order_id' => $row['order_id']], true)) {
                        throw new \Exception('订单状态修改失败');
                    }
                    if (!ShopProductStandard::build()->where(['standard_id' => $row['standard_id']])->setInc('stocks',
                        $row['count'])
                    ) {
                        throw new \Exception('库存恢复失败');
                    }
                    Db::commit();
                    $redis->hDel('order_pay_info', $row['bill_no']);
                } catch (\Exception $e) {
                    Db::rollback();
                }
            }
        }
    }

    /**
     * @author: airon
     * @time: 2017年8月15日
     * @param $cart_id_list
     * @param $user_id
     * @return false|\PDOStatement|string|\think\Collection
     * description:生成订单后删除购物车数据
     */
    public function deleteShoppingCart($cart_id_list, $user_id)
    {
        return ShoppingCart::build()->where(['cart_id' => ['in', $cart_id_list], 'user_id' => $user_id])->delete();
    }

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:根据cart_id获取对应的user_remarks
     * @param $cart_id
     * @param $cart_array
     * @return string
     */
    private function cartGetUserRemarks($cart_id, $cart_array)
    {
        try {
            foreach ($cart_array as $key => $value) {
                if ($cart_id == $value['cart_id']) {
                    return $value['user_remarks'];
                }
            }
            return "";
        } catch (Exception $e) {
            return "";
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:根据用户获取我的订单
     * @param $data
     * @return array
     */
    public function getOrdersByUser($data)
    {
        try {
            $result = ['orders' => [], 'count' => 0];
            $where = ['o.user_id' => $data['user_id']];
            //状态筛选
            switch ($data['status']) {
                case 'unpaid':
                    $where['o.status'] = '未支付';
                    break;
                case 'preparing':
                    $where['o.status'] = '待发货';
                    break;
                case 'delivering':
                    $where['o.status'] = '待收货';
                    break;
                case 'toevaluate':
                    $where['o.status'] = '待评价';
                    break;
                case 'refunded':
                    $where['o.status'] = ['in', ['退款中', '已退款']];
                    break;
            }
            //搜索关键字筛选
            if (isset($data['keyword'])) {
                $where[] = function ($query) use ($data) {
                    $query->where(['s.name' => ['like', "%{$data['keyword']}%"]])
                        ->whereor(['op.product_name' => ['like', "%{$data['keyword']}%"]]);
                };//todo
            }
            $orders = ShopOrder::build()->alias('o')
                ->field([
                    'o.order_id',
                    'o.product_id',
                    'o.supplier_id',
                    'o.standard_id',
                    'o.logistics_id',
                    'o.order_num',
                    'o.price',
                    'o.count',
                    'o.status',
                    'o.pay_type',
                    'o.create_time',
                    'op.product_name',
                    'op.standard_name',
                    'op.img_src_list',
                    's.name as supplier_name'
                ])
                ->join('shop_order_product op', 'o.order_id = op.order_id', 'left')
                ->join('shop_supplier s', 's.supplier_id = o.supplier_id', 'left')
                ->where($where)
                ->order(['o.order_id' => 'DESC'])
                ->page($data['page_index'], $data['page_size'])
                ->select()->toArray();

            if ($orders) {
                foreach ($orders as &$value) {
                    $value['pay_type'] = Config::get('mall.order')['pay_type'][$value['pay_type']];
                    $value['img_src'] = empty($value['img_src_list']) ? "" : explode('|', $value['img_src_list'])[0];
                    unset($value['img_src_list']);
                }
                $result['orders'] = $orders;
                $result['count'] = ShopOrder::build()->alias('o')
                    ->join('shop_order_product op', 'o.order_id = op.order_id', 'left')
                    ->join('shop_supplier s', 's.supplier_id = o.supplier_id', 'left')
                    ->where($where)->count();
            }
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:获取用户单个订单详情
     * @param $data
     * @return array
     */
    public function getOrderDetail($data)
    {
        $result = ShopOrder::build()->alias('o')
            ->field([
                'o.order_id',
                'o.product_id',
                'o.supplier_id',
                'o.standard_id',
                'o.order_num',
                'ROUND(o.price,2) as price',
                'o.count',
                'o.status',
                'o.create_time',
                'o.pay_time',
                'o.user_remarks',
                'o.pay_type',
                'ROUND(oc.product_price,2) as product_price',
                'ROUND(oc.freight_charge,2) as freight_charge',
                'ROUND(oc.processing_charge,2) as processing_charge',
                'ROUND(oc.home_charge,2) as home_charge',
                'ROUND(oc.service_charge,2) as service_charge',
                'op.product_name',
                'op.standard_name',
                'op.img_src_list',
                's.name as supplier_name',
                'o.logistics_id',
                'o.cancel_reason',
                'or.content',
                'or.reason'
            ])
            ->join('shop_order_charge oc', 'o.order_id = oc.order_id', 'left')
            ->join('order_refund or', 'o.order_id = or.order_id', 'left')
            ->join('shop_order_product op', 'o.order_id = op.order_id', 'left')
            ->join('shop_supplier s', 'o.supplier_id = s.supplier_id', 'left')
            ->where(['o.order_id' => $data['order_id'], 'o.user_id' => $data['user_id']])
            ->find();
        if ($result) {
            $result['pay_type'] = Config::get('mall.order')['pay_type'][$result['pay_type']];
            $result['img_src'] = empty($result['img_src_list']) ? "" : explode('|', $result['img_src_list'])[0];
            unset($result['img_src_list']);
        }
        return $result;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:获取订单地址
     * @param $data
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function getOrderAddress($data)
    {
        return ShopOrderArea::build()->field(['id', 'order_id', 'user_id'], true)->where($data)->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:确认收货
     * @param $data
     * @return int
     */
    public function confirmOrder($data)
    {
        if (($result = ShopOrder::build()->where($data)->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '待收货') {
            //只有待收货的订单才能确认收货
            return 1;
        }
        $result->status = '待评价';
        Db::startTrans();
        $bool = $result->save();
        if (!$bool) {
            Db::rollback();
            return 2;
        }
        $bool = ShopProduct::build()->where(['product_id' => $result->product_id])->setInc('sales');
        if (!$bool) {
            Db::rollback();
            //失败
            return 2;
        }

        try {
            //查询物流状态 如果 neq 已签收 则修改为已签收
            $logistics = ShopOrderLogistics::build()->where(['logistics_id' => $result['logistics_id']])->find();
            if ($logistics['logistics_type'] != 'official') {
                Db::commit();
                return 9;
            }
            if ($logistics['status'] == '已签收') {
                Db::commit();
                return 9;
            }
            $bool = ShopOrderLogistics::build()->update(['status' => '已签收'],['logistics_id' => $result['logistics_id']]);
            if (!$bool) {
                Db::rollback();
                return 2;
            }
            //写入一条物流消息
            $bool = ShopOrderOfficialLogistics::build()->insert(
                ['user_id' => $logistics['user_id'], 'logistics_id' => $logistics['logistics_id'], 'content' => Config::get('logistics.endContent'),'create_time'=>time()]
            );
            if (!$bool) {
                Db::rollback();
                return 2;
            }

            //成功
            Db::commit();
            return 9;
        } catch (Exception $exception) {
            Db::rollback();
            return 2;
        }

    }

    /**
     * @author: Airon
     * @time: 2017年8月23日
     * description:查询订单
     * @param $out_trade_no
     * @return int|string
     */
    public static function checkOrder($out_trade_no)
    {
        //存在支付时间不为0的数据则说明已支付成功
        return ShopOrder::build()->where(['bill_no' => $out_trade_no, 'pay_time' => ['neq', 0]])->count();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月23日
     * description:获取物流消息简介
     * @param $data
     * @return array|false|int|\PDOStatement|string|\think\Model
     *
     */
    public function getLogistics($data)
    {
        try {
            if (($model = LogisticsModel::checkExists($data)) == null) {
                return -1;
            } elseif ($model->logistics_type != 'official') {
                /*
                 * 1.先获取缓存,有就直接返回
                 * 2.没有则接口获取,然后保存进缓存
                 */
                if ($return['detail'] = Cache::get($model->logistics_type . '-' . $model->logistics_number)) {
                    $return['detail'] = unserialize($return['detail']);
                }
                $util = new kdniaoUtil();
                $lists = $util->getOrderTracesByJson($model->logistics_type, $model->logistics_number);

                if ($lists !== false) {
                    foreach ($lists['Traces'] as &$v) {
                        $v['content'] = $v['AcceptStation'];
                        $v['create_time'] = strtotime($v['AcceptTime']);
                        unset($v['AcceptStation'], $v['AcceptTime']);
                    }
                    Cache::set($model->logistics_type . '-' . $model->logistics_number, serialize($lists['Traces']),
                        60 * 60 * 5);
                    $return['detail'] = $lists['Traces'];
                    unset($lists);
                } else {
                    $return['detail'] = [];
                }
                krsort($return['detail']);
                $return['detail'] = array_values($return['detail']);
            } else {
                $return['detail'] = OfficialLogisticsModel::build()->field([
                    'content',
                    'create_time'
                ])->where(['logistics_id' => $data['logistics_id']])->order(['id' => 'DESC'])->select()->toArray();
            }
            $return['order_num'] = $model->order_type == 'shop' ? $model->shopOrder->order_num : '';
            $return['contact'] = $model->toArray();
            $return['type'] = Config::get('mall.logistics')['type'][$return['contact']['logistics_type']];
            unset($return['contact']['shopOrder'], $return['contact']['id'], $return['contact']['order_id'], $return['contact']['logistics_id'], $return['contact']['order_type']);
            return $return;
        } catch (\Exception $e) {
            return 0;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:获取物流信息,(单条版)
     * @param $data
     * @return array|false|mixed|null|\PDOStatement|string|\think\Model
     */
    public function getLogisticsBrief($data)
    {
        if (($model = LogisticsModel::checkExists($data)) == null) {
            return null;
        } elseif ($model->logistics_type != 'official') {
            /*
             * 1.先获取缓存,有就直接返回
             * 2.没有则接口获取,然后保存进缓存
             */
            if ($lists = Cache::get($model->logistics_type . '-' . $model->logistics_number)) {
                $lists = unserialize($lists);
                return array_pop($lists);
            }

            $util = new kdniaoUtil();
            $lists = $util->getOrderTracesByJson($model->logistics_type, $model->logistics_number);

            if ($lists !== false) {
                foreach ($lists['Traces'] as &$v) {
                    $v['content'] = $v['AcceptStation'];
                    $v['create_time'] = strtotime($v['AcceptTime']);
                    unset($v['AcceptStation'], $v['AcceptTime']);
                }
                Cache::set($model->logistics_type . '-' . $model->logistics_number, serialize($lists['Traces']),
                    60 * 60 * 5);
                return array_pop($lists['Traces']);
            } else {
                return null;
            }
        } else {
            if (($info = OfficialLogisticsModel::build()->field([
                    'content',
                    'create_time'
                ])->where(['logistics_id' => $data['logistics_id']])->order(['id' => 'DESC'])->find()) != null
            ) {
                $info = $info->toArray();
            }
            return $info;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:批量确认订单
     */
    public function confirmOrders()
    {
        $deadline = Config::get('mall.order')['auto_confirm'];
        $where = [
            'status' => '待收货',
            'update_time' => ['lt', time() - $deadline]
        ];
        $lists = ShopOrder::build()->field(['order_id', 'product_id'])->where($where)->select()->toArray();

        if ($lists) {
            $orderData = ['status' => '待评价'];
            foreach ($lists as $val) {
                ShopOrder::update($orderData, ['order_id' => $val['order_id']]);
                ShopProduct::build()->where(['product_id' => $val['product_id']])->setInc('sales');
            }
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月13日
     * description:申请退款
     * @param $data
     * @return int
     */
    public function cancelOrder($data)
    {
        if (($result = ShopOrder::build()->where(['order_id'=>$data['order_id'],'user_id'=>$data['user_id']])->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '待发货') {
            //只有待发货的订单才能申请退款
            return 1;
        } else {
            $result->status = '退款中';
            Db::startTrans();
            $bool = $result->save();
            if ($bool === false) {
                //失败
                Db::rollback();
                return 2;
            }
            $refundData = $data;
            $refundData['order_type'] = 'shop';
            $refundData['create_time'] = time();
            $bool = ShopOrderRefund::build()->insert($refundData);
            if ($bool) {
                //成功
                Db::commit();
                return 9;
            } else {
                //失败
                Db::rollback();
                return 2;
            }
        }

    }

    public function cancelUnpaidOrder($data)
    {
        $where = ['user_id'=>$data['user_id'],'order_id'=>$data['order_id']];
        if (($result = ShopOrder::build()->where($where)->find()) == null) {
            //没有该订单
            return 0;
        } elseif ($result->status != '未支付') {
            //只有未支付的订单才能取消订单
            return 1;
        } else {
            $result->status = '已取消';
            $result->cancel_reason = $data['cancel_reason'];
            if ($result->save()) {
                //成功
                return 9;
            } else {
                //失败
                return 2;
            }
        }

    }

    public function payAgain($order_id, $user_id)
    {
        $order = ShopOrder::build()->where(['order_id' => $order_id, 'user_id' => $user_id, 'status' => '未支付'])->find();
        if ($order == null) {
            return 0;
        }
        $result = 0;
        $redis = RedisUtil::getInstance();
        $pay_info = $redis->hGet('order_pay_info', $order->bill_no);
        list($trade_type, $prepay_id) = explode('|', $pay_info);
        $order->bill_no = md5(uniqid(mt_rand(), true));//商户订单号
        switch ($order->pay_type) {
            case 'wechat':
                $body = config('wechat')['wx_pay_charge'];
                //$attach = implode(',',$order_num_list);

                $appId = config('wechat')['AppID'];
                $mchId = config('wechat')['Pay']['MchId'];
                $appKey = config('wechat')['Pay']['Key'];
                $wechatPay = new WechatPay($appId, $mchId, $appKey);

                $requestData = $wechatPay->order($order->bill_no, 0.01, $prepay_id, $body, "", $trade_type);//todo::测试临时

                if ($requestData !== false) {
                    $result = $requestData;
                }
                break;
            //微信是后台调用接口拿到是否生成订单的返回结果
            case 'alipay':
                $appId = config('alipay')['AppID'];
                $partner_public_key = config('alipay')['partnerPublicKey'];
                $partner_private_key = config('alipay')['rsaPrivateKey'];
                $pay_charge = config('alipay')['pay_charge'];
                $alipay_public_key = config('alipay')['alipayPublicKey'];
                $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key, $alipay_public_key);
                if ($trade_type == 'NATIVE') {
                    $result = $alipayClient->webOrder($order->bill_no, 0.01, $pay_charge, $pay_charge);
                } else {
                    $result = $alipayClient->order($order->bill_no, 0.01, $pay_charge, $pay_charge);
                }
                break;
            //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
        }
        $order->isUpdate(true)->save();
        RedisUtil::getInstance()->hSet('order_pay_info', $order->bill_no, $pay_info);
        return ['result'=>$result,'order_info'=>$order];
    }
}