<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 17-11-28
 * Time: 下午4:43
 */

namespace app\api\logic;


use think\Db;
use think\Config;
use app\common\tools\RedisUtil;
use app\common\tools\wechatPay;
use app\common\tools\alipayClient;
use app\common\model\UserArea as UserAreaModel;
use app\common\model\CustomizedOrder as OrderModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\CustomizedOrderArea as OrderAreaModel;
use app\common\model\CustomizedOrderCharge as OrderChargeModel;
use app\common\model\CustomizedOtherCharge as OtherChargeModel;
use app\common\model\CustomizedOrderScheme as OrderSchemeModel;

class CustomizedPayLogic
{
    /**
     * @author: Rudy
     * @time: 2017年12月25日
     * description:生成订单
     * @param $data
     * @return \app\common\tools\alipaySDK\提交表单HTML文本|array|bool|int|mixed|string
     */
    public static function buildOrder($data)
    {
        /*
         * 1.order表的写入（订单表写入
         * 2.order_area表的写入（订单地址
         * 3.order_charge表的写入（订单金额的写入
         * 4.order_scheme表的写入（写入付款时的方案
         * 5.支付接口
         * 6.清除缓存
         */
        try {
            //报表
            $statement = unserialize(RedisUtil::getInstance()->hGet('scheme_complete', $data['scheme_id']));
            //方案
            $scheme = SchemeModel::get($data['scheme_id'])->toArray();
            //运费等等费用
            $other_charge = OtherChargeModel::getChargeByType($scheme['scheme_type'] . ($scheme['scheme_b_type'] != '' ? '-' . $scheme['scheme_b_type'] : ''));
            //订单金额
            $total_price = ($statement['scheme_dis_price'] + array_sum($other_charge)) * $data['count'];//（折扣价格+运费）×2
            //收货地址
            $area = UserAreaModel::get($data['area_id'])->toArray();

            $other_charge['count'] = $data['count'];
            $other_charge['scheme_price'] = $statement['scheme_price'];
            $other_charge['discount'] = $statement['discount'];
            $other_charge['scheme_dis_price'] = $statement['scheme_dis_price'];


            $order_num = substr(md5(uniqid(mt_rand(), true)), 0, 4) . time();
            $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号
            RedisUtil::getInstance()->hSet('order_pay_info', $out_trade_no, $data['trade_type'] . '|' . $data['prepay_id']);

            Db::startTrans();
            //1.
            if (($order_id = OrderModel::build()->insertGetId([
                'scheme_id' => $data['scheme_id'], 'user_id' => $data['user_id'], 'count' => $data['count'],
                'order_num' => $order_num, 'pay_type' => $data['pay_type'],
                'bill_no' => $out_trade_no, 'delivery_time' => $data['delivery_time'],
                'price' => $total_price, 'create_time' => time(), 'user_remarks' => $data['user_remarks']
                ])) == 0) {
                throw new \Exception('订单生成失败', 0);
            }
            //2.
            $area['order_id'] = $order_id;
            if (!OrderAreaModel::create($area, true)) {
                throw new \Exception('订单收货地址写入失败', 0);
            }
            //3
            $other_charge['order_id'] = $order_id;
            if (!OrderChargeModel::create($other_charge, true)) {
                throw new \Exception('订单金额表写入失败', 0);
            }
            //4
            unset($scheme['id']);
            $scheme['scheme_id'] = $data['scheme_id'];
            $scheme['order_id'] = $order_id;
            $scheme['statement_array'] = json_encode($statement, JSON_UNESCAPED_UNICODE);
            if (!OrderSchemeModel::create($scheme, true)) {
                throw new \Exception('订单方案数据写入失败', 0);
            }
            //5
            switch ($data['pay_type']) {
                case 'wechat':
                    $wechat_config = Config::get('wechat');

                    $body = $wechat_config['wx_pay_charge'];
                    $appId = $wechat_config['AppID'];
                    $mchId = $wechat_config['Pay']['MchId'];
                    $appKey = $wechat_config['Pay']['Key'];
                    $wechatPay = new WechatPay($appId, $mchId, $appKey);

                    $result = $wechatPay->order($out_trade_no, 0.01 * $data['count'], $data['prepay_id'], $body,
                        $order_id, $data['trade_type'], PUBLIC_PATH . '/index.php/api/Callback/customizedWXPay');//todo::测试临时

                    if ($result === false) {
                        throw new \Exception('支付订单生成失败', 0);
                    }
                    break;
                //微信是后台调用接口拿到是否生成订单的返回结果
                case 'alipay':
                    $alipay_config = Config::get('alipay');

                    $appId = $alipay_config['AppID'];
                    $partner_public_key = $alipay_config['partnerPublicKey'];
                    $partner_private_key = $alipay_config['rsaPrivateKey'];
                    $pay_charge = $alipay_config['pay_charge'];
                    $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
                    if ($data['trade_type'] == 'NATIVE') {
                        $result = $alipayClient->webOrder($out_trade_no, '0.01' * $data['count'], $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/customizedAlipay');
                    } else {
                        $result = $alipayClient->order($out_trade_no, '0.01' * $data['count'], $pay_charge, $pay_charge, $order_id, dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/customizedAlipay');
                    }
                    break;
                //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
                default:
                    $result = false;
                    break;
            }


            Db::commit();
            $result_temp['result'] = $result;
            $result_temp['order_id'] = $order_id;
            //6
//todo 删掉结算数据就无法重新支付了            RedisUtil::getInstance()->hDel('scheme_complete', $data['scheme_id']);

            return $result_temp;

        } catch (\Exception $e) {
            Db::rollback();
            return $e->getCode();
        }
    }

    public static function getCharge($scheme_id, $count)
    {
        //报表
        $statement = unserialize(RedisUtil::getInstance()->hGet('scheme_complete', $scheme_id));
        //方案
        $scheme_type = SchemeModel::build()->where(['id' => $scheme_id])->value('IF(scheme_b_type != "",concat_ws("-",scheme_type,scheme_b_type),scheme_type)');
        //运费等等费用
        $other_charge = OtherChargeModel::getChargeByType($scheme_type);
        //订单金额
        $other_charge['total_price'] = ($statement['scheme_dis_price'] + array_sum($other_charge)) * $count;//（折扣价格+运费）×2
        $other_charge['count'] = $count;

        return [$statement, $other_charge];
    }

    /**
     * @author: Rudy
     * @time: 2017年12月25日
     * description:获取订单信息
     * @param $out_trade_no
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function getOrderInfo($out_trade_no)
    {
        return OrderModel::build()->where(['bill_no' => $out_trade_no, 'pay_time' => 0])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年12月25日
     * description:更改订单状态
     * @param $out_trade_no
     * @param $data
     * @return $this
     */
    public static function updateOrderStatus($out_trade_no, $data)
    {
        return OrderModel::build()->update($data, ['bill_no' => $out_trade_no]);
    }

    public static function getOtherCharge($scheme_type)
    {
        return OtherChargeModel::getChargeByType($scheme_type);
    }

    /**
     * @author: Rudy
     * @time: 2018年2月8日
     * description:重新调用支付
     */
    public static function payAgain($order_id, $user_id)
    {
        $order = OrderModel::build()->where(['order_id' => $order_id, 'user_id' => $user_id, 'status' => '未支付'])->find();
        if ($order == null) {
            return 0;
        }
        $result = 0;
        $redis = RedisUtil::getInstance();
        $pay_info = $redis->hGet('order_pay_info', $order->bill_no);
        list($trade_type, $prepay_id) = explode('|', $pay_info);
        $order->bill_no = md5(uniqid(mt_rand(), true));//商户订单号
        switch ($order->pay_type) {
            case 'wechat':
                $body = config('wechat')['wx_pay_charge'];
                $appId = config('wechat')['AppID'];
                $mchId = config('wechat')['Pay']['MchId'];
                $appKey = config('wechat')['Pay']['Key'];
                $wechatPay = new WechatPay($appId, $mchId, $appKey);

                $requestData = $wechatPay->order($order->bill_no, 0.01 * $order->count, $prepay_id, $body, "", $trade_type,PUBLIC_PATH . '/index.php/api/Callback/customizedWXPay');//todo::测试临时

                if ($requestData !== false) {
                    $result = $requestData;
                }
                break;
            //微信是后台调用接口拿到是否生成订单的返回结果
            case 'alipay':
                $appId = config('alipay')['AppID'];
                $partner_public_key = config('alipay')['partnerPublicKey'];
                $partner_private_key = config('alipay')['rsaPrivateKey'];
                $pay_charge = config('alipay')['pay_charge'];
                $alipay_public_key = config('alipay')['alipayPublicKey'];
                $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key, $alipay_public_key);
                if ($trade_type == 'NATIVE') {
                    $result = $alipayClient->webOrder($order->bill_no, 0.01 * $order->count, $pay_charge, $pay_charge,$order_id,dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/customizedAlipay');
                } else {
                    $result = $alipayClient->order($order->bill_no, 0.01 * $order->count, $pay_charge, $pay_charge,$order_id,dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/customizedAlipay');
                }
                break;
            //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
        }
        $order->isUpdate(true)->save();
        RedisUtil::getInstance()->hSet('order_pay_info', $order->bill_no, $pay_info);
        return ['result'=>$result,'order_info'=>$order];
    }

    /**
     * User: zhaoxin
     * Date: 2019/12/6
     * Time: 5:57 下午
     * description
     * @param $out_trade_no
     * @return int|string
     * @throws \think\Exception
     */
    public static function checkOrder($out_trade_no)
    {
        //存在支付时间不为0的数据则说明已支付成功
        return OrderModel::build()->where(['bill_no' => $out_trade_no, 'pay_time' => ['neq', 0]])->count();
    }
}