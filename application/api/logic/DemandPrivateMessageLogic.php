<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/25
 * Time: 15:59
 */

namespace app\api\logic;

use app\common\model\User as UserModel;
use app\common\model\Demand as DemandModel;
use app\common\model\DemandPrivateMessage as MessageModel;

class DemandPrivateMessageLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月25日
     * description:发送私信
     * @param $data
     * @return int
     */
    public static function addPrivateMessage($data)
    {
        /*
         * 1.查看需求可不可以发布(is_deleted = 0  and status = 1) 未删除和发布中的
         * 2.私信不能自己跟自己聊天
         */
        try{
            if(($user_id = DemandModel::build()->where(['is_deleted'=>0,'status'=>1,'demand_id'=>$data['demand_id']])->value('user_id')) == null){
                //不存在需求
                return -1;
            }elseif($data['login_user_id'] == $user_id && $data['user_id'] == 0){
                //不能自己给自己发私信
                return -2;
            }else{
                $data['user_id'] = $data['login_user_id'] == $user_id?$data['user_id']:$data['login_user_id'];
                $data['status'] = $data['login_user_id'] == $user_id?'master':'guest';
                return MessageModel::create($data,true)?1:0;
            }
        }catch (\Exception $e){
            dump($e->getMessage());exit;
            return 0;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:获取私信列表
     * @param $data
     * @return array|int
     */
    public static function getPrivateMessages($data)
    {
        try{
            if(DemandModel::build()->where(['is_deleted'=>0,'demand_id'=>$data['demand_id'],'user_id'=>$data['user_id']])->find() === null){
                //不存在需求
                return -1;
            }else{
                $result = MessageModel::build()->field(['m.demand_id','m.message_id','m.type','m.content','m.user_id','m.create_time','u.username','u.avatar'])->alias('m')
                    ->join("(SELECT MAX(message_id) as message_id FROM yd_demand_private_message WHERE demand_id = {$data['demand_id']} GROUP BY user_id) pm",'pm.message_id = m.message_id')
                    ->join('user u','u.user_id = m.user_id','LEFT')->select()->toArray();
                return $result;
            }
        }catch (\Exception $e){
            return 0;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:获取私信详情
     * @param $data
     * @param $pageSize
     * @return int
     */
    public static function getMessageDetail($data,$pageSize)
    {
        try{
            if(($owner = DemandModel::build()->where(['is_deleted'=>0,'demand_id'=>$data['demand_id']])->value('user_id')) === null){
                //不存在需求
                return -1;
            }elseif($owner == $data['user_id']){
                //如果是需求发布者来获取该接口,则需要把他传入的at_user_id当成要查询的user_id,如果是其他人,则直接使用$data['user_id']
                $data['user_id'] = $data['at_user_id'];
                unset($data['at_user_id']);
            }
            //获取私信
            $where = ['user_id'=>$data['user_id'],'demand_id'=>$data['demand_id']];
            if($data['last_message_id']>0){
                $where['message_id'] = ['<',$data['last_message_id']];
            }
                $messages = MessageModel::build()->field(['message_id','status','type','content','create_time'])
                ->where($where)->order(['message_id'=>'DESC'])
                ->page(0,$pageSize)->select()->toArray();
            //获取用户
            if(!empty($messages)){
                $user = UserModel::build()->field(['user_id','username','avatar'])
                    ->where(['user_id'=>['in',[$owner,$data['user_id']]]])->select()->toArray();
                $user = array_column($user,null,'user_id');
                foreach ($messages as &$v){
                    $v['master_username'] = $user[$owner]['username'];
                    $v['master_avatar'] = $user[$owner]['avatar'];
                    $v['guest_username'] = $user[$data['user_id']]['username'];
                    $v['guest_avatar'] = $user[$data['user_id']]['avatar'];
                }
            }
            sort($messages);
            return $messages;
        }catch(\Exception $e){
            return 0;
        }
    }
}