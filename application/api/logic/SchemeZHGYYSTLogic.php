<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/27
 * Time: 9:57
 */

namespace app\api\logic;

use app\common\model\ProductBean;
use app\common\model\SchemeZHGBean as SchemeBean;
use app\common\model\CustomizedScheme;
use app\common\tools\CommonMethod;

class SchemeZHGYYSTLogic extends SchemeBaseLogic
{
    private $productLogic = null;


    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:重构成原来参数进行验证
     * @param $scheme
     * @return array
     */
    public function getOriginParams($scheme)
    {
        $params = [];

        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';
        $params['width'] = isset($scheme['scheme_width']) ? $scheme['scheme_width'] : 0;
        $params['s_type'] = isset($scheme['scheme_s_type']) ? $scheme['scheme_s_type'] : -1;

        return $params;
    }

    /**
     * @author: Airon
     * @time: 2017年9月28日
     * description:获取productlogic
     * @return ProductLogic|null
     */
    public function getProductLogic()
    {
        if ($this->productLogic == null) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }

    public function buildScheme($schemeData)
    {
        return SchemeBean::build($schemeData);
    }

    /**
     * @author: Airon
     * @time: 2017年9月29日
     * description:计算方案数量
     * @param $data
     * @return array
     */
    public function schemeMath($data)
    {

        $range_arr = [50 => 0, 100 => 0, 150 => 0];//误差值范围
        foreach ($range_arr as $range => $value) {

            $scheme = new SchemeBean();
            $scheme->setScheme_width($data['width']);
            $scheme->setScheme_height(2019);//高
            $scheme->setScheme_s_type($data['s_type']);
            $scheme->setScheme_b_type("YYST");
            $scheme->setScheme_tv_size($data['tv_size']);
            $scheme->setScheme_tv_type('GS');
            $scheme->setScheme_color_no("000");
            $scheme->setScheme_error_range($range);//设置误差值
            $data['s_type'] == 0 ? $this->createZHGYYSTSchemeCD($scheme, true) : $this->createZHGYYSTSchemeBW($scheme, true);

            $range_arr[$range] = count($scheme->getScheme_schemes());
        }
        return $range_arr;
    }

    public function createZHGYYSTScheme($data)
    {
        $index = base64_encode(http_build_query($data));
        $productList = $this->beforeCreate('zhgyyst', $index);
        if (!empty($productList)) {
            return $productList;
        }

        $scheme = new SchemeBean();

        $scheme->setScheme_width($data['width']);//宽
        $scheme->setScheme_height(2019);//高
        $scheme->setScheme_deep(314);//深度
        $scheme->setScheme_error_range($data['error_range']);//设置误差值
        $scheme->setScheme_b_type('YYST');//类别
        $scheme->setScheme_s_type($data['s_type']);//
        $scheme->setScheme_color_no('test2');//花色
        $scheme->setScheme_tv_size($data['tv_size']);
        $scheme->setScheme_tv_type($data['tv_type']);
        $scheme->setScheme_type('ZH');

        $data['s_type'] == 0 ? $this->createZHGYYSTSchemeCD($scheme, false) : $this->createZHGYYSTSchemeBW($scheme, false);


        $productList = $scheme->getScheme_schemes();
        if (!empty($productList)) {
            array_walk($productList, function (&$row) use ($index, $data) {
                $row->setSearch_index($index);
                $row->setScheme_tv_size(intval($data['tv_size']));
                $row->setScheme_tv_type($data['tv_type']);
                $row = $row->mainInfo();
            });
            $this->afterCreate('zhgyyst', $productList);
        }
        return $productList;
    }

    /**生成侧拱式电视柜
     * @param SchemeBean $scheme
     * @param $isMath
     */
    private function createZHGYYSTSchemeCD(SchemeBean $scheme, $isMath)
    {
        $scheme->setScheme_height(1379 + 16);
        $inputWidth = $scheme->getScheme_width();//宽
        $height = 1379;//高
        $width = 0;//家具宽度
        $errorRange = $scheme->getScheme_error_range();//误差值

        $zhWidthArr = [1143, 1590, 2001];
        $colWidthArr = [432, 560, 784];

        $tvSize = $scheme->getScheme_tv_size();
        $tvLen = GetLongSide16Bi9($tvSize);
        $tvHeight = $tvLen / 1.5;

        $gridCount = intval(($height - 74) / 320);


        $s_scheme = $t_scheme = null;
        $s_schemes = $t_schemes = $s_products = $wcb_products = $ncb_products = $gai_products = [];

        $logic = $this->getProductLogic();

        for ($i = 0; $i < count($zhWidthArr); $i++) {
            // 单列
            $width = $zhWidthArr[$i];
            if (abs($inputWidth - $width) < $errorRange) {
                $s_scheme = new SchemeBean();
                if (!$isMath) {//如果不是计算的,则需要生成方案
                    $t_schemes = [];
                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $s_scheme->setScheme_hole_width($inputWidth);
                    $s_scheme->setScheme_hole_height(1379);
                    $s_scheme->setScheme_width($width);
                    $s_scheme->setScheme_height(1379);
                    $s_scheme->setScheme_wcb_type(1);
                    $s_scheme->setScheme_deep(314);
//                    $s_scheme->setScheme_wcb_products($wcb_products);
//                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                    $s_scheme->setScheme_type($scheme->getScheme_type());
                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                    $wcb_products = $ncb_products = [];

                    // 固定功能体
                    $t_scheme = $this->GetTV_ZJ($zhWidthArr[$i], $tvLen, $tvHeight, $scheme, 0, 1379);
                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());
                    $t_schemes[] = $t_scheme;

                    $s_scheme->setScheme_wcb_products($wcb_products);
                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_schemes($t_schemes);

                    $s_scheme->setScheme_error_range($errorRange);
//                    $s_pic = "YYST1" . $s_scheme->getScheme_width() . "_" . $s_scheme->getScheme_height() . '_' . $scheme->getScheme_color_no() . '_' . count($t_schemes) . '_' . getRandomInt();
                    $this->createSchemePic($s_scheme);
                    $s_scheme->setScheme_name('客厅电视柜组合-' . getRandomInt());
                }
                $s_schemes[] = $s_scheme;
            }
            if (1143 == $zhWidthArr[$i]) {
                continue;
            }
            // 两列
            for ($c = 0; $c < 3; $c++) {
                $width = $zhWidthArr[$i] + $colWidthArr[$c] + 25 + 16;
                if (abs($inputWidth - $width) < $errorRange) {

                    $s_scheme = new SchemeBean();
                    if (!$isMath) {
                        $t_schemes = [];
                        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                        $s_scheme->setScheme_hole_width($inputWidth);
                        $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                        $s_scheme->setScheme_width($width);
                        $s_scheme->setScheme_height($scheme->getScheme_height());
                        $s_scheme->setScheme_wcb_type(1);
                        $s_scheme->setScheme_deep(314);
//                        $s_scheme->setScheme_wcb_products($wcb_products);
//                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $s_scheme->setScheme_type($scheme->getScheme_type());
                        $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                        $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                        $wcb_products = $ncb_products = $gai_products = [];
                        //外侧板
                        $wcb_products[] = $logic->createZHG_WCB(0, 0, 0, 0, 16, $height, 314,
                            $scheme->getScheme_color_no(), 0);
                        $s_scheme->setScheme_wcb_type(1);
                        //标准列
                        $t_scheme = new SchemeBean();
                        $t_scheme->setScheme_width($colWidthArr[$c]);
                        $t_scheme->setScheme_height($height);
                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $s_products = [];
                        //背板
//                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 5, $height - 74, $colWidthArr[$c],
//                            $scheme->getScheme_color_no(), 0);
                        if ($height <= 425) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 345, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 745) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1065) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1385) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1705) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 2025) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        }
                        //横隔板顶
                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $colWidthArr[$c],
                            $scheme->getScheme_color_no(), 2, 0);
                        //横隔板底
                        $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74), 25, 298,
                            $colWidthArr[$c], $scheme->getScheme_color_no(), 2, 0);
                        //底担
                        $s_products[] = $logic->createZHG_DD(0, 72, 0, $height - 72, 25, 72, $colWidthArr[$c],
                            $scheme->getScheme_color_no(), 0);
                        //多层横隔板
                        for ($g = 1; $g < $gridCount; $g++) {
                            $s_products[] = $logic->createZHG_HGB(0, $height - $g * (320), 0, $g * (320), 25, 298,
                                $colWidthArr[$c], $scheme->getScheme_color_no(), 2, 0);
                        }
                        //衔接外侧板
                        $wcb_products[] = $logic->createZHG_WCB_ST($colWidthArr[$c] + 25 + 304, $height,
                            $colWidthArr[$c] + 25 + 304 + 16, 0, 25, 963, 314, $scheme->getScheme_color_no(), 0);
                        $t_scheme->setScheme_products($s_products);
                        $t_schemes[] = $t_scheme;

                        //固定功能体
                        $t_scheme = $this->GetTV_ZJ($zhWidthArr[$i], $tvLen, $tvHeight, $scheme, 304 + 25, 1379);
                        $t_schemes[] = $t_scheme;

                        //衔接内侧板
                        $x = 16;
                        for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                            $n_t_scheme = change2SchemeBean($t_schemes[$n]);
                            if ($n == 0) {
                                $x += $n_t_scheme->getScheme_width();
                            } else {
                                $x += $n_t_scheme->getScheme_width() + 25;
                            }
                            $ncb_products[] = $logic->createZHG_ZCB_XJ($x, 0, $x, 0, 25, $height, 314,
                                $scheme->getScheme_color_no(), 0);
                        }

                        //盖板
                        if (2019 > $height) {
                            $gai_products[] = $logic->createZHG_GB(0, 0, 0, -16, 16, 334,
                                17 + $colWidthArr[$c] + 25 + 304 + 26, $scheme->getScheme_color_no(), 0, 0);
                        }
                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_g_products($gai_products);
                        $s_scheme->setScheme_schemes($t_schemes);
                        $s_scheme->setScheme_error_range($errorRange);
//                        $s_pic = 'YYST1' . $s_scheme->getScheme_width()
//                            . "_" . $s_scheme->getScheme_height()
//                            . "_" . $s_scheme->getScheme_color_no()
//                            . "_" . count($t_schemes)
//                            . "_" . getRandomInt();
                        $this->createSchemePic($s_scheme);

                        $s_scheme->setScheme_name('客厅电视柜组合-' . getRandomInt());

                    }
                    $s_schemes[] = $s_scheme;
                }

            }
        }
        $s_schemes = array_reverse($s_schemes);
        if ($scheme->getScheme_schemes() == null) {
            $scheme->setScheme_schemes($s_schemes);
        } else {
            $temp = $scheme->getScheme_schemes();
            $temp[] = $s_schemes;
            $scheme->setScheme_schemes($temp);
        }
    }

    /**
     * 包围式电视柜
     * @param SchemeBean $scheme
     * @param $isMath
     */
    private function createZHGYYSTSchemeBW(SchemeBean $scheme, $isMath)
    {
        $inputWidth = $scheme->getScheme_width();//宽
        $height = $scheme->getScheme_height();//高
        if ($height < 2019) {
            $height = $height - 16;
        }
        $width = 0;//家具宽度
        $errorRange = $scheme->getScheme_error_range();//误差值

        $zhWidthArr = [560, 784];
        $tvWidthArr = [1143, 1590];
        $colWidthArr = [432, 560, 784];

        $tvSize = $scheme->getScheme_tv_size();
        $tvLen = GetLongSide16Bi9($tvSize);
        $tvHeight = $tvLen / 1.5;


        $s_scheme = $t_scheme = null;
        $s_schemes = $t_schemes = $s_products = $wcb_products = $ncb_products = $gai_products = [];

        $w432 = 432;
        $w560 = 560;
        $w784 = 784;

        $colCount = 1;
        $gridCount = intval(($height - 74) / 320);
        $logic = $this->getProductLogic();
        for ($i = 0; $i < count($zhWidthArr); $i++) {
            if (($zhWidthArr[$i] * 2 + 25 - $tvLen) <= 360) {//一侧18cm空间
                continue;
            }
            // 单列
            $width = $zhWidthArr[$i] * 2 + 25 + 50;
            if (abs($inputWidth - $width) < $errorRange) {
                $s_scheme = new SchemeBean();
                if (!$isMath) {//如果不是计算的,则需要生成方案
                    $t_schemes = [];
                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                    $s_scheme->setScheme_hole_width($inputWidth);
                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                    $s_scheme->setScheme_width($width);
                    $s_scheme->setScheme_height($scheme->getScheme_height());
                    $s_scheme->setScheme_wcb_type(1);
                    $s_scheme->setScheme_deep(314);
//                    $s_scheme->setScheme_wcb_products($wcb_products);
//                    $s_scheme->setScheme_ncb_products($ncb_products);
                    $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                    $s_scheme->setScheme_type($scheme->getScheme_type());
                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                    $wcb_products = $ncb_products = [];

                    //单列时，两侧都是使用组件外侧板
                    $wcb_products[] = $logic->createZHG_WCB_ZJ(0, 0, 0, 0, 25, $height, 314, $scheme->getScheme_color_no(),
                        0);
                    $wcb_products[] = $logic->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25, $height, 314,
                        $scheme->getScheme_color_no(), 0);
                    $s_scheme->setScheme_wcb_type(0);

                    // 固定功能体
                    $t_scheme = new  SchemeBean();
                    $t_scheme->setScheme_width($zhWidthArr[$i] * 2 + 25);
                    $t_scheme->setScheme_height($height);
                    $t_scheme->setScheme_no("G-TV" . intval($tvWidthArr[$i] / 10) . "-00-" . $scheme->getScheme_color_no() .
                        ":Z-DZS" . $zhWidthArr[$i] . "*2-" . $scheme->getScheme_color_no());
                    $t_scheme->setScheme_name($tvWidthArr[$i] . "视听柜:" . $zhWidthArr[$i] . "*2上部组件");
                    $t_scheme->setScheme_b_type($scheme->getScheme_b_type());
                    $t_scheme->setScheme_s_type($scheme->getScheme_s_type());
                    $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                    $t_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));

                    $s_products = [];
                    //背板
                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 12, $height - 1379, $zhWidthArr[$i] * 2 + 25,
                        $scheme->getScheme_color_no(), 0);
                    //横隔板顶
                    $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $zhWidthArr[$i],
                        $scheme->getScheme_color_no(), 2, 0);
                    $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height, $zhWidthArr[$i] + 25, 0, 25,
                        298, $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);
                    $s_products[] = $logic->createZHG_ZCB($zhWidthArr[$i], $height, $zhWidthArr[$i], 0, 25,
                        $height - 1379, 314, $scheme->getScheme_color_no(), 0);
                    //多层横隔板
                    for ($g = 1; $g < $gridCount - 4; $g++) {
                        $s_products[] = $logic->createZHG_HGB_GD(0, $height - $g * 320, 0, $g * 320, 25, 298,
                            $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);
                        $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height - $g * 320,
                            $zhWidthArr[$i] + 25,
                            $g * 320, 25, 298, $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);
                    }
                    $s_products[] = $logic->createZHG_HGB_GD(0, 1379, 0, $height - 1379, 25, 298,
                        $zhWidthArr[$i] * 2 + 25, $scheme->getScheme_color_no(), 0, 0);

                    //电视柜组件
                    $temp = $this->GetTV_ZJ($tvWidthArr[$i], $tvLen, $tvHeight, $scheme, 0, 2019)->getScheme_products();
                    $s_products = array_merge($s_products, $temp);

                    $t_scheme->setScheme_products($s_products);
                    $t_schemes[] = $t_scheme;
                    $s_scheme->setScheme_wcb_products($wcb_products);
                    $s_scheme->setScheme_schemes($t_schemes);
                    $s_scheme->setScheme_error_range($errorRange);
//                    $s_pic = "YYST2" . $s_scheme->getScheme_width() . "_" . $s_scheme->getScheme_height() . "_" . count($t_schemes) . "_" . getRandomInt();
                    $this->createSchemePic($s_scheme);
                    $s_scheme->setScheme_name('客厅电视柜组合-' . getRandomInt());

                }
                $s_schemes[] = $s_scheme;
            }
            // 两列
            for ($c = 0; $c < 3; $c++) {
                $width = $zhWidthArr[$i] * 2 + $colWidthArr[$c] + 25 * 3 + 16;
                if (abs($inputWidth - $width) < $errorRange) {
                    $s_scheme = new SchemeBean();
                    if (!$isMath) {
                        $t_schemes = [];
                        $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                        $s_scheme->setScheme_hole_width($inputWidth);
                        $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                        $s_scheme->setScheme_width($width);
                        $s_scheme->setScheme_height($scheme->getScheme_height());
                        $s_scheme->setScheme_wcb_type(1);
                        $s_scheme->setScheme_deep(314);
//                        $s_scheme->setScheme_wcb_products($wcb_products);
//                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $s_scheme->setScheme_type($scheme->getScheme_type());
                        $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                        $s_scheme->setScheme_s_type($scheme->getScheme_s_type());

                        $wcb_products = $ncb_products = [];
                        //外侧板
                        $wcb_products[] = $logic->createZHG_WCB(0, 0, 0, 0, 16, $height, 314,
                            $scheme->getScheme_color_no(), 0);
                        $s_scheme->setScheme_wcb_type(1);
                        $wcb_products[] = $logic->createZHG_WCB_ZJ(0, 0, ($width - 25), 0, 25, $height, 314,
                            $scheme->getScheme_color_no(), 0);

                        //标准列
                        $t_scheme = new SchemeBean();
                        $t_scheme->setScheme_width($colWidthArr[$c]);
                        $t_scheme->setScheme_height($height);
                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $s_products = [];
                        //背板
//                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 12, $height - 74, $colWidthArr[$c],
//                            $scheme->getScheme_color_no(), 0);
                        if ($height <= 425) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 345, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 745) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1065) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1385) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 653,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 1705) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 653, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        } elseif ($height <= 2025) {
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 973,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                            $s_products[] = $this->getProductLogic()->createZHG_BB(0, $height, 0, 0,
                                5, 973, $colWidthArr[$c], $scheme->getScheme_color_no(), 0);
                        }

                        //横隔板顶
                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $colWidthArr[$c],
                            $scheme->getScheme_color_no(), 2, 0);
                        //横隔板底
                        $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, ($height - 25 - 74), 25, 298,
                            $colWidthArr[$c], $scheme->getScheme_color_no(), 2, 0);
                        //底担
                        $s_products[] = $logic->createZHG_DD(0, 72, 0, $height - 72, 25, 72, $colWidthArr[$c],
                            $scheme->getScheme_color_no(), 0);
                        //多层横隔板
                        for ($g = 1; $g < $gridCount; $g++) {
                            $s_products[] = $logic->createZHG_HGB(0, $height - $g * (320), 0, $g * (320), 25, 298,
                                $colWidthArr[$c], $scheme->getScheme_color_no(), 2, 0);
                        }
                        $t_scheme->setScheme_products($s_products);
                        $t_schemes[] = $t_scheme;

                        //固定功能体
                        $t_scheme = new SchemeBean();
                        $t_scheme->setScheme_width($zhWidthArr[$i] * 2 + 25);
                        $t_scheme->setScheme_height($height);
                        $t_scheme->setScheme_no("G-TV" . intval($tvWidthArr[$i] / 10) . "-00-" . $scheme->getScheme_color_no() . ":Z-DZS" . $zhWidthArr[$i] . "*2-" . $scheme->getScheme_color_no());
                        $t_scheme->setScheme_name($tvWidthArr[$i] . "视听柜:" . $zhWidthArr[$i] . "*2上部组件");
                        $t_scheme->setScheme_b_type($scheme->getScheme_b_type());
                        $t_scheme->setScheme_s_type($scheme->getScheme_s_type());
                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                        $t_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));

                        $s_products = [];
                        //背板
                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 5, $height - 1379,
                            $zhWidthArr[$i] * 2 + 25,
                            $scheme->getScheme_color_no(), 0);

                        //横隔板顶
                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $zhWidthArr[$i],
                            $scheme->getScheme_color_no(), 2, 0);
                        //横隔板底
                        $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height, $zhWidthArr[$i] + 25, 0,
                            25, 298,
                            $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);

                        $s_products[] = $logic->createZHG_ZCB($zhWidthArr[$i], $height, $zhWidthArr[$i], 0, 25,
                            $height - 1379, 314, $scheme->getScheme_color_no(), 0);

                        //多层横隔板
                        for ($g = 1; $g < $gridCount - 4; $g++) {
                            $s_products[] = $logic->createZHG_HGB_GD(0, $height - $g * 320, 0, $g * 320, 25, 298,
                                $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);

                            $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height - $g * 320,
                                $zhWidthArr[$i] + 25, $g * 320, 25, 298, $zhWidthArr[$i], $scheme->getScheme_color_no(),
                                2, 0);
                        }
                        $s_products[] = $logic->createZHG_HGB_GD(0, 1379, 0, $height - 1379, 25, 298,
                            $zhWidthArr[$i] * 2 + 25, $scheme->getScheme_color_no(), 0, 0);
                        //电视组件
                        $s_products = array_merge($s_products,
                            $this->GetTV_ZJ($tvWidthArr[$i], $tvLen, $tvHeight, $scheme, 0,
                                2019)->getScheme_products());
                        $t_scheme->setScheme_products($s_products);
                        $t_schemes[] = $t_scheme;
                        // 内侧板
                        $x = 16;
                        for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                            if (0 == $n) {
                                $x = $x + $t_schemes[$n]->getScheme_width();
                            } else {
                                $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                            }

                            if (!empty($t_schemes[$n]->getScheme_no())
                                || !empty($t_schemes[$n + 1]->getScheme_no())
                            ) {
                                $ncb_products[] = $this->getProductLogic()->createZHG_ZJXJCB(
                                    $x,
                                    0,
                                    $x,
                                    0,
                                    25,
                                    $height,
                                    314,
                                    $scheme->getScheme_color_no(),
                                    0);
                            } else {
                                $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB($x, 0, $x, 0, 25, $height,
                                    314, $scheme->getScheme_color_no(), 0));
                            }
                        }
                        $s_scheme->setScheme_wcb_products($wcb_products);
                        $s_scheme->setScheme_ncb_products($ncb_products);
                        $s_scheme->setScheme_schemes($t_schemes);
                        $s_scheme->setScheme_error_range($errorRange);

//                        $s_pic = 'YYST2' . $s_scheme->getScheme_width()
//                            . "_" . $s_scheme->getScheme_height()
//                            . "_" . $s_scheme->getScheme_color_no()
//                            . "_" . count($t_schemes)
//                            . "_" . getRandomInt();
                        $this->createSchemePic($s_scheme);
                        $s_scheme->setScheme_name('客厅电视柜组合-' . getRandomInt());

                    }
                    $s_schemes[] = $s_scheme;
                }

            }
            //多于2列
            $max432colCount = intval(($inputWidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w432) + 1;
            $max560colCount = intval(($inputWidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w560) + 1;
            $max784colCount = intval(($inputWidth - $zhWidthArr[$i] * 2 - 25 - 50 - 32) / $w784) + 1;
            for ($i784 = 0; $i784 <= $max784colCount; $i784++) {
                for ($i560 = 0; $i560 <= $max560colCount; $i560++) {
                    for ($i432 = 0; $i432 <= $max432colCount; $i432++) {
                        $colCount = $i784 + $i560 + $i432 + 1;
                        if (3 <= $colCount) {
                            $width = 25 * ($colCount - 1) + $zhWidthArr[$i] * 2 + 25 + 2 * 16 + $i432 * $w432 + $i560 * $w560 + $i784 * $w784;
                            if (abs($inputWidth - $width) < $errorRange) {
                                $s_scheme = new SchemeBean();
                                if (!$isMath) {
                                    $t_schemes = [];
                                    $s_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));
                                    $s_scheme->setScheme_hole_width($inputWidth);
                                    $s_scheme->setScheme_hole_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_width($width);
                                    $s_scheme->setScheme_height($scheme->getScheme_height());
                                    $s_scheme->setScheme_wcb_type(1);
                                    $s_scheme->setScheme_deep(314);
//                                    $s_scheme->setScheme_wcb_type($wcb_products);
//                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_color_no($scheme->getScheme_color_no());
                                    $s_scheme->setScheme_type($scheme->getScheme_type());
                                    $s_scheme->setScheme_b_type($scheme->getScheme_b_type());
                                    $s_scheme->setScheme_s_type($scheme->getScheme_s_type());
                                    $wcb_products = $ncb_products = [];
                                    $wcb_products[] = $logic->createZHG_WCB(0, 0, 0, 0, 16, $height, 314,
                                        $scheme->getScheme_color_no(), 0);
                                    $s_scheme->setScheme_wcb_type(1);
                                    $wcb_products[] = $logic->createZHG_WCB(0, 0, ($width - 16), 0, 16, $height, 314,
                                        $scheme->getScheme_color_no(), 0);

                                    //组合功能单元
                                    for ($c = 0; $c < $i432; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w432);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                                        $s_products = [];

                                        //背板
//                                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 5, $height - 74, $w432,
//                                            $scheme->getScheme_color_no(), 0);
                                        if ($height <= 425) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w432, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w432, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 653, 560, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w432, $scheme->getScheme_color_no(), 0);
                                        }

                                        //横隔板顶
                                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $w432,
                                            $scheme->getScheme_color_no(), 2, 0);
                                        //横隔板底
                                        $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, $height - 25 - 74, 25,
                                            298, $w432, $scheme->getScheme_color_no(), 2, 0);
                                        //底担
                                        $s_products[] = $logic->createZHG_DD(0, 72, 0, $height - 72, 25, 72, $w432,
                                            $scheme->getScheme_color_no(), 0);
                                        //多层横隔板
                                        for ($g = 1; $g < $gridCount; $g++) {
                                            $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                                25, 298, $w432, $scheme->getScheme_color_no(), 2, 0);
                                        }
                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = $t_scheme;
                                    }
                                    for ($c = 0; $c < $i560; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w560);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                                        $s_products = [];
                                        //背板
//                                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 5, $height - 74, $w560,
//                                            $scheme->getScheme_color_no(), 0);
                                        if ($height <= 425) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w560, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 653, $w560, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w560, $scheme->getScheme_color_no(), 0);
                                        }

                                        //横隔板顶
                                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $w560,
                                            $scheme->getScheme_color_no(), 2, 0);
                                        //横隔板底
                                        $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, $height - 25 - 74, 25,
                                            298, $w560, $scheme->getScheme_color_no(), 2, 0);
                                        //底担
                                        $s_products[] = $logic->createZHG_DD(0, 72, 0, $height - 72, 25, 72, $w560,
                                            $scheme->getScheme_color_no(), 0);
                                        //多层横隔板
                                        for ($g = 1; $g < $gridCount; $g++) {
                                            $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                                25, 298, $w560, $scheme->getScheme_color_no(), 2, 0);
                                        }
                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = $t_scheme;
                                    }

                                    for ($c = 0; $c < $i784; $c++) {
                                        $t_scheme = new SchemeBean();
                                        $t_scheme->setScheme_width($w784);
                                        $t_scheme->setScheme_height($height);
                                        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                                        $s_products = [];
                                        //背板
//                                        $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 5, $height - 74, $w784,
//                                            $scheme->getScheme_color_no(), 0);
                                        if ($height <= 425) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 345, $w784, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 745) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1065) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1385) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 653,
                                                5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 1705) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 653, $w784, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                        } elseif ($height <= 2025) {
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 973,
                                                5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                            $s_products[] = $logic->createZHG_BB(0, $height, 0, 0,
                                                5, 973, $w784, $scheme->getScheme_color_no(), 0);
                                        }

                                        //横隔板顶
                                        $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $w784,
                                            $scheme->getScheme_color_no(), 2, 0);
                                        //横隔板底
                                        $s_products[] = $logic->createZHG_HGB_GD(0, 74 + 25, 0, $height - 25 - 74, 25,
                                            298, $w784, $scheme->getScheme_color_no(), 2, 0);
//                                        //底担
                                        $s_products[] = $logic->createZHG_DD(0, 72, 0, $height - 72, 25, 72, $w784,
                                            $scheme->getScheme_color_no(), 0);
                                        //多层横隔板
                                        for ($g = 1; $g < $gridCount; $g++) {
                                            $s_products[] = $logic->createZHG_HGB(0, $height - $g * 320, 0, $g * 320,
                                                25, 298, $w784, $scheme->getScheme_color_no(), 2, 0);
                                        }
                                        $t_scheme->setScheme_products($s_products);
                                        $t_schemes[] = $t_scheme;
                                    }

                                    //固定功能体
                                    $t_scheme = new SchemeBean();
                                    $t_scheme->setScheme_width(($zhWidthArr[$i] * 2 + 25));
                                    $t_scheme->setScheme_height($height);
                                    $t_scheme->setScheme_no("G-TV" . intval($tvWidthArr[$i] / 10) . "-00-" . $scheme->getScheme_color_no() . ":Z-DZS" . $zhWidthArr[$i] . "*2-" . $scheme->getScheme_color_no());
                                    $t_scheme->setScheme_name($tvWidthArr[$i] . "视听柜:" . $zhWidthArr[$i] . "*2上部组件");
                                    $t_scheme->setScheme_b_type($scheme->getScheme_b_type());
                                    $t_scheme->setScheme_s_type($scheme->getScheme_s_type());
                                    $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
                                    $t_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));

                                    $s_products = [];
                                    //背板
                                    $s_products[] = $logic->createZHG_BB(0, $height, 0, 0, 12, $height - 1379,
                                        $zhWidthArr[$i] * 2 + 25,
                                        $scheme->getScheme_color_no(), 0);

                                    //横隔板顶
                                    $s_products[] = $logic->createZHG_HGB_GD(0, $height, 0, 0, 25, 298, $zhWidthArr[$i],
                                        $scheme->getScheme_color_no(), 2, 0);
                                    //横隔板底
                                    $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25, $height,
                                        $zhWidthArr[$i] + 25, 0,
                                        25, 298,
                                        $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);

                                    $s_products[] = $logic->createZHG_ZCB($zhWidthArr[$i], $height, $zhWidthArr[$i], 0,
                                        25,
                                        $height - 1379, 314, $scheme->getScheme_color_no(), 0);

                                    //多层横隔板
                                    for ($g = 1; $g < $gridCount - 4; $g++) {
                                        $s_products[] = $logic->createZHG_HGB_GD(0, $height - $g * 320, 0, $g * 320, 25,
                                            298,
                                            $zhWidthArr[$i], $scheme->getScheme_color_no(), 2, 0);

                                        $s_products[] = $logic->createZHG_HGB_GD($zhWidthArr[$i] + 25,
                                            $height - $g * 320,
                                            $zhWidthArr[$i] + 25, $g * 320, 25, 298, $zhWidthArr[$i],
                                            $scheme->getScheme_color_no(),
                                            2, 0);
                                    }
                                    $s_products[] = $logic->createZHG_HGB_GD(0, 1379, 0, $height - 1379, 25, 298,
                                        $zhWidthArr[$i] * 2 + 25, $scheme->getScheme_color_no(), 0, 0);

                                    $s_products = array_merge($s_products,
                                        $this->GetTV_ZJ($tvWidthArr[$i], $tvLen, $tvHeight, $scheme, 0,
                                            2019)->getScheme_products());
                                    $t_scheme->setScheme_products($s_products);
                                    array_splice($t_schemes, intval((($colCount - 1) / 2)), 0, [$t_scheme]);
                                    // 内侧板
                                    $x = 16;
                                    for ($n = 0; $n < count($t_schemes) - 1; $n++) {
                                        if (0 == $n) {
                                            $x = $x + $t_schemes[$n]->getScheme_width();
                                        } else {
                                            $x = $x + $t_schemes[$n]->getScheme_width() + 25;
                                        }

                                        if (!empty($t_schemes[$n]->getScheme_no())
                                            || !empty($t_schemes[$n + 1]->getScheme_no())
                                        ) {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZJXJCB(
                                                $x,
                                                0,
                                                $x,
                                                0,
                                                25,
                                                $height,
                                                314,
                                                $scheme->getScheme_color_no(),
                                                0));
                                        } else {
                                            $ncb_products[] = ($this->getProductLogic()->createZHG_ZCB(
                                                $x,
                                                0,
                                                $x,
                                                0,
                                                25,
                                                $height,
                                                314,
                                                $scheme->getScheme_color_no(),
                                                0));
                                        }
                                    }
                                    $s_scheme->setScheme_wcb_products($wcb_products);
                                    $s_scheme->setScheme_ncb_products($ncb_products);
                                    $s_scheme->setScheme_schemes($t_schemes);


                                    $s_scheme->setScheme_error_range($errorRange);
//                                    $s_pic = 'YYST3' . $s_scheme->getScheme_width()
//                                        . "_" . $s_scheme->getScheme_height()
//                                        . "_" . $s_scheme->getScheme_color_no()
//                                        . "_" . count($t_schemes)
//                                        . "_" . getRandomInt();
                                    $this->createSchemePic($s_scheme);

                                    $s_scheme->setScheme_name('客厅电视柜组合-' . getRandomInt());

                                }
                                $s_schemes[] = $s_scheme;
                            }
                        }
                    }
                }
            }
        }

        if (!empty($s_schemes)) {
            $s_schemes = array_reverse($s_schemes);
        }
        $scheme->setScheme_schemes($s_schemes);
//        if ($scheme->getScheme_schemes() == null) {
//            $scheme->setScheme_schemes($s_schemes);
//        } else {
//            $temp = $scheme->getScheme_schemes();
//            $temp[] = $s_schemes;
//            $scheme->setScheme_schemes($temp);
//        }
    }

    /*
     * 获取电视柜组件
     */
    private function GetTV_ZJ($zhWidth, $tvLen, $tvHeight, SchemeBean $scheme, $leftWidth, $height)
    {
        $s_products = $acc_products = [];
        $hgb = new ProductBean();
        $ctWidth = 0;
        if ($zhWidth == 1590) {
            $ctWidth = 582;
        } else {
            $ctWidth = 806;
        }
        //固定功能体
        $t_scheme = new SchemeBean();
        $t_scheme->setScheme_width($zhWidth);
        $t_scheme->setScheme_height($height);
        $t_scheme->setScheme_no("G-TV" . intval($zhWidth / 10) . "-00-" . $scheme->getScheme_color_no());
        $t_scheme->setScheme_name($zhWidth . "视听柜组件");
        $t_scheme->setScheme_b_type($scheme->getScheme_b_type());
        $t_scheme->setScheme_s_type($scheme->getScheme_s_type());
        $t_scheme->setScheme_color_no($scheme->getScheme_color_no());
        $t_scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));


        $logic = $this->getProductLogic();

        if ($leftWidth != 0) {
            //衔接横隔板顶
            $s_products[] = $logic->createZHG_HGB_GD_XJ(0, $height, 0, 0,
                25, 314, 304, $scheme->getScheme_color_no(), 2, 0);
            //衔接多层横隔板
            for ($g = 1; $g <= 2; $g++) {
                $s_products[] = $logic->createZHG_HGB_XJ(0, $height - $g * 320, 0, $g * 320,
                    25, 314, 304, $scheme->getScheme_color_no(), 2, 0);
            }
        }

        //背板
        $s_products[] = $logic->createZHG_BB(25, 245, 25, $height - 245,
            5, 176, $zhWidth - 50, $scheme->getScheme_color_no(), 0);

        //横隔板顶
        $hgb = $logic->createZHG_HGB_GD(0, 416, 0, $height - 416, 25, 401, $zhWidth, $scheme->getScheme_color_no(), 2,
            0);

        if ($leftWidth == 0 || $leftWidth + $tvLen + 360 <= $zhWidth) {
            if ($scheme->getScheme_tv_type() == "TS") {
                $acc_products[] = $logic->createZHG_Acc(($leftWidth + ($zhWidth - $tvLen - $leftWidth) / 2),
                    ($height - 416 - $tvLen / 1.5),
                    $tvHeight, $tvLen,
                    "FunctionUnit/TS" . $scheme->getScheme_tv_size(), "电视", 0, 0);
            } else {
                $acc_products[] = $logic->createZHG_Acc(($leftWidth + ($zhWidth - $tvLen - $leftWidth) / 2),
                    ($height - 416 - $tvLen / 1.5) - 90,
                    $tvHeight, $tvLen,
                    "FunctionUnit/GS" . $scheme->getScheme_tv_size(), "电视", 0, 0);
            }


        } else {
            if ($scheme->getScheme_tv_type() == "TS") {
                $acc_products[] = $logic->createZHG_Acc(($leftWidth + 180),
                    ($height - 416 - $tvLen / 1.5),
                    $tvHeight, $tvLen,
                    "FunctionUnit/TS" . $scheme->getScheme_tv_size(), "电视", 0, 0);
            } else {
                $acc_products[] = $logic->createZHG_Acc(($leftWidth + 180),
                    ($height - 416 - $tvLen / 1.5) - 90,
                    $tvHeight, $tvLen,
                    "FunctionUnit/GS" . $scheme->getScheme_tv_size(), "电视", 0, 0);
            }
        }
        //两侧立板
        $s_products[] = $logic->createZHG_ZCB(0, 391, 0, $height - 391, 25, 391, 401, $scheme->getScheme_color_no(), 0);
        $s_products[] = $logic->createZHG_ZCB($zhWidth - 25, 391, $zhWidth - 25, $height - 391, 25, 391, 401,
            $scheme->getScheme_color_no(), 0);

        if ($zhWidth > 1143) {
            //柜内立板
            $s_products[] = $logic->createZHG_ZCB(intval($zhWidth / 2) - $ctWidth - 16, 391,
                intval($zhWidth / 2) - $ctWidth - 16, $height - 391, 16, 319, 401, $scheme->getScheme_color_no(), 0);
            $s_products[] = $logic->createZHG_ZCB(intval($zhWidth / 2) + $ctWidth, 391, intval($zhWidth / 2) + $ctWidth,
                $height - 391, 16, 319, 401, $scheme->getScheme_color_no(), 0);
            //抽屉
            $s_products[] = $logic->createZHG_OneCT_AI(intval($zhWidth / 2) - $ctWidth, 245,
                intval($zhWidth / 2) - $ctWidth,
                $height - 245, 25, 157, $ctWidth, $scheme->getScheme_color_no(), 0, 0, "");
            $acc_products[] = $logic->createZHG_Acc((intval($zhWidth / 2) - $ctWidth) + intval(($ctWidth - 40 * 8) / 2),
                ($height - 245) - 88, 88, 40 * 8, "000", "DVD", 0, 0);
            $s_products[] = $logic->createZHG_OneCT_AI(intval($zhWidth / 2), 245, intval($zhWidth / 2), $height - 245,
                25, 157, $ctWidth, $scheme->getScheme_color_no(), 0, 0, "");
            $acc_products[] = $logic->createZHG_Acc(intval($zhWidth / 2) + intval(($ctWidth - 54 * 8) / 2),
                ($height - 245) - 88, 88, 54 * 8, "001", "DVD", 0, 0);

            // 金属封边版
            // 中间上
            $s_products[] = $logic->createZHG_JSFBB(intval(($zhWidth) / 2) - 12, 391,
                intval($zhWidth / 2) - 12, $height - 391, 401, 144, 25, $scheme->getScheme_color_no(), 0, 0);
            // 中间下
            $s_products[] = $logic->createZHG_JSFBB(intval(($zhWidth) / 2) - 12, 72,
                intval($zhWidth / 2) - 12, $height - 72, 401, 72, 25, $scheme->getScheme_color_no(), 0, 0);
        } else {

            //柜内立板
            $s_products[] = $logic->createZHG_ZCB(intval(($zhWidth - $ctWidth) / 2) - 16, 391,
                intval(($zhWidth - $ctWidth) / 2) - 16, $height - 391, 16, 319, 401, $scheme->getScheme_color_no(), 0);
            $s_products[] = $logic->createZHG_ZCB(intval(($zhWidth - $ctWidth) / 2) + $ctWidth, 391,
                intval(($zhWidth - $ctWidth) / 2) + $ctWidth,
                $height - 391, 16, 319, 401, $scheme->getScheme_color_no(), 0);
            //抽屉
            $s_products[] = $logic->createZHG_OneCT_AI(intval(($zhWidth - $ctWidth) / 2), 245,
                intval(($zhWidth - $ctWidth) / 2),
                $height - 245, 25, 157, $ctWidth, $scheme->getScheme_color_no(), 0, 0, "");
            $acc_products[] = $logic->createZHG_Acc(intval(($zhWidth - $ctWidth) / 2) + intval(($ctWidth - 40 * 8) / 2),
                ($height - 245) - 88, 88, 40 * 8, "000", "DVD", 0, 0);

            // 金属封边版
            // 中间下
            $s_products[] = $logic->createZHG_JSFBB(intval(($zhWidth) / 2) - 12, 72,
                intval($zhWidth / 2) - 12, $height - 72, 401, 72, 25, $scheme->getScheme_color_no(), 0, 0);
        }
        $hgb->setAcc_products($acc_products);
        $s_products[] = $hgb;
        $t_scheme->setScheme_products($s_products);
        return $t_scheme;
    }

    public function store(SchemeBean $schemeBean, $schemeData, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_type' => 'ZH',
            'scheme_type_text' => '客厅电视柜组合',
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => 314,
            'scheme_door_count' => 0,
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_sk_color_no' => '000',
            'scheme_sk' => -1,
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_b_type' => 'YYST',
            'scheme_hole_type' => 0,
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => 314,
            'scheme_hole_left' => 0,
            'scheme_hole_right' => 0,
            'is_left_wall' => '0',
            'is_right_wall' => '0',
            'is_left_wall_zt' => '0',
            'is_right_wall_zt' => '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
            'user_id' => $userId
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model->getLastInsID();
    }

    public function update(SchemeBean $schemeBean, $schemeData, $userId, $scheme_id)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_color_no' => $schemeData['scheme_color_no'],
            'scheme_s_type' => $schemeData['scheme_s_type'],
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
        ];
        return CustomizedScheme::update($schemeModelData, ['user_id' => $userId, 'id' => $scheme_id]);
    }
}