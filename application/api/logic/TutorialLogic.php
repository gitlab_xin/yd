<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:08
 */

namespace app\api\logic;


use app\common\model\Tutorial as TutorialModel;

class TutorialLogic
{
    /**
     * @author: Airon
     * @time: 2017年8月2日
     * @param $page
     * @param $num
     * description:获取教程列表
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function get_list($page,$num){
        return TutorialModel::build()->order('create_time')->page($page,$num)->select();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取教程数量
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getCount()
    {
        return TutorialModel::whereCount();
    }
}