<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/23
 * Time: 18:44
 */

namespace app\api\logic;

use app\common\model\CooperativePartner as PartnerModel;

class CooperativePartnerLogic
{
    /**
     * @author: Rudy
     * @time: 2017年9月23日
     * description:获取合作伙伴
     * @param $pageIndex
     * @param $pageSize
     * @return bool|false|\PDOStatement|string|\think\Collection
     */
    public static function getPartners($pageIndex, $pageSize)
    {
        try {
            return PartnerModel::build()->field(['name','logo_src','link'])->page($pageIndex, $pageSize)->select();
        } catch (\Exception $e) {
            return false;
        }
    }
}