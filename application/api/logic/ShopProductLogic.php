<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 10:43
 */

namespace app\api\logic;

use app\common\model\ShopProduct as ProductModel;
use app\common\model\ShopEvaluate as EvaluateModel;
use app\common\model\ShopCollect as CollectModel;
use app\common\model\ShopOrder as OrderModel;
use app\common\model\ShopSupplierParams;
use app\common\model\ShopChangePriceRecord as PriceRecordModel;

class ShopProductLogic
{
    public $order_rule = [
        "composite" => "order_key",//综合
        "sales" => "sales",//销量
        "price" => "ps.price",//价格
    ];

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:搜索
     * @param $keyword
     * @param $order
     * @param $sort
     * @param $supplier_id
     * @param $max_price
     * @param $min_price
     * @param int $page
     * @param int $num
     * @return false|mixed|\PDOStatement|string|\think\Collection
     */
    public function searchByKeyword($keyword, $order, $sort, $supplier_id, $min_price, $max_price, $page = 1, $num = 20)
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.sales',
            'p.is_hot',
            'sup.supplier_id',
            'p.is_new',
            '(sales*0.4+like_count*0.3+collect_count*0.3) as order_key'
        ];
        $whereQuery['p.is_deleted'] = '0';
        $whereQuery['p.is_sale'] = '1';
        if ($supplier_id !== false) {
            $whereQuery['sup.supplier_id'] = $supplier_id;
        }
        if ($max_price !== false && $min_price !== false && $max_price >= $min_price) {
            $whereQuery['ps.price'] = ['between', [$min_price, $max_price]];
        } else if ($max_price === false && $min_price !== false) {
            $whereQuery['ps.price'] = ['egt', $min_price];
        } else if ($max_price !== false && $min_price === false) {
            $whereQuery['ps.price'] = ['elt', $max_price];
        }

        if ($order != 'hot') {
            $orderArray = [$this->order_rule[$order] => $sort, 'p.create_time' => 'desc'];
        } else {
            $orderArray = ['p.create_time' => 'desc'];
            $whereQuery['is_hot'] = '1';
        }
        $ProductList = ProductModel::build()->alias('p')
            ->join('yd_shop_supplier sup', 'sup.supplier_id = p.supplier_id')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->join('yd_shop_classify c1', 'p.classify_one_id = c1.classify_id')
            ->join('yd_shop_classify c2', 'p.classify_two_id = c2.classify_id')
            ->join('yd_shop_classify c3', 'p.classify_three_id = c3.classify_id')
            ->where($whereQuery);

        if ($keyword != '' && !is_null($keyword)) {
            $searchableKeys =
                "p.name|c1.name|c2.name|c3.name";
            $keyword = "REGEXP '{$keyword}'";
            $whereQuery[$searchableKeys] = ['exp', $keyword];
            $ProductList = $ProductList->where($searchableKeys,'exp',$keyword);
        }
        $ProductList = $ProductList->field($field)
            ->order($orderArray)
            ->page($page, $num)
            ->select();
        $ProductList = self::get_img_src($ProductList);
        return $ProductList;
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:网站搜索
     * @param $keyword
     * @param $classify_id
     * @param $classify_level
     * @param $supplier_id
     * @param $order
     * @param $sort
     * @param int $page
     * @param int $num
     * @return false|mixed|\PDOStatement|string|\think\Collection
     */
    public function searchWebByKeyword(
        $keyword,
        $classify_id,
        $classify_level,
        $supplier_id,
        $order,
        $sort,
        $page = 1,
        $num = 20
    )
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.sales',
            'p.is_hot',
            'p.is_new',
            '(sales*0.4+like_count*0.3+collect_count*0.3) as order_key'
        ];
        $whereQuery['p.is_deleted'] = '0';//是否已删除
        $whereQuery['p.is_sale'] = '1';//是否已下架

        if (!empty($classify_id) && !empty($classify_level)) {//判断是否选择了分类
            switch ($classify_level) {
                case 'one':
                    $whereQuery['p.classify_one_id'] = $classify_id;
                    break;
                case 'two':
                    $whereQuery['p.classify_two_id'] = $classify_id;
                    break;
                case 'three':
                    $whereQuery['p.classify_three_id'] = $classify_id;
                    break;
            }
            if ($keyword != '' && !is_null($keyword)) {//选择了分类就不搜索分类了
                $searchableKeys =
                    "p.name";
                $whereQuery[$searchableKeys] = ['like', "%{$keyword}%"];
            }
        } else {
            if ($keyword != '' && !is_null($keyword)) {//没选分类时就搜索分类
                $searchableKeys =
                    "p.name|c1.name|c2.name|c3.name";
                $searchableKeys =
                    "p.name";
                $whereQuery[$searchableKeys] = ['like', "%{$keyword}%"];
            }

        }

        if (!empty($supplier_id)) {//筛选品牌
            $whereQuery['p.supplier_id'] = $supplier_id;
        }
        if ($order != 'hot') {
            $orderArray = [$this->order_rule[$order] => $sort, 'p.create_time' => 'desc'];
        } else {
            $orderArray = ['p.create_time' => 'desc'];
            $whereQuery['is_hot'] = '1';
        }
        $ProductList = ProductModel::build()->alias('p')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0'GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->join('yd_shop_classify c1', 'p.classify_one_id = c1.classify_id')
            ->join('yd_shop_classify c2', 'p.classify_two_id = c2.classify_id')
            ->join('yd_shop_classify c3', 'p.classify_three_id = c3.classify_id')
            ->where($whereQuery)
            ->field($field)
            ->order($orderArray)
            ->page($page, $num)
            ->select()->toArray();
        $count = ProductModel::build()->alias('p')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0'GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->join('yd_shop_classify c1', 'p.classify_one_id = c1.classify_id')
            ->join('yd_shop_classify c2', 'p.classify_two_id = c2.classify_id')
            ->join('yd_shop_classify c3', 'p.classify_three_id = c3.classify_id')
            ->where($whereQuery)
            ->count();
        $ProductList = self::get_img_src($ProductList);
        $list['product_list'] = $ProductList;
        $list['count'] = $count;
        return $list;
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description: 按分类获取商品列表
     * @param int $classifyId 分类ID
     * @param string $classifyLevel 分类等级
     * @param string $order 排序规则
     * @param string $sort 正序倒序
     * @param int $supplier_id 品牌ID
     * @param int $min_price
     * @param int $max_price
     * @param int $page
     * @param int $num
     * @return false|mixed|\PDOStatement|string|\think\Collection
     */
    public function classifyProductList(
        $classifyId,
        $classifyLevel,
        $order,
        $sort,
        $supplier_id,
        $min_price,
        $max_price,
        $page,
        $num
    )
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.sales',
            'p.is_hot',
            'p.is_new',
            '(sales*0.4+like_count*0.3+collect_count*0.3) as order_key'
        ];
        $whereQuery = ["classify_{$classifyLevel}_id" => $classifyId, 'p.is_deleted' => '0', 'p.is_sale' => '1'];
        if ($supplier_id != 0) {
            $whereQuery['sup.supplier_id'] = $supplier_id;
        }
        if ($max_price != 0 && $min_price != 0 && $max_price >= $min_price) {
            $whereQuery['ps.price'] = ['between', [$min_price, $max_price]];
        } else if ($max_price == 0 && $min_price != 0) {
            $whereQuery['ps.price'] = ['egt', $min_price];
        } else if ($max_price != 0 && $min_price == 0) {
            $whereQuery['ps.price'] = ['elt', $max_price];
        }
        $ProductList = ProductModel::build()->alias('p')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->join('yd_shop_supplier sup', 'sup.supplier_id = p.supplier_id')
            ->where($whereQuery)
            ->field($field)
            ->order([$this->order_rule[$order] => $sort, 'p.create_time' => 'desc'])
            ->page($page, $num)
            ->select();
        $ProductList = self::get_img_src($ProductList);
        return $ProductList;
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:商城首页推荐
     * @return array
     */
    public static function home()
    {
        $result = ShopClassifyLogic::selectLevelOne();
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.classify_one_id',
            'p.is_hot',
            'p.is_new',
            '(p.sales*0.4+p.like_count*0.3+p.collect_count*0.3) as order_key'
        ];
        $order = [
            'order_key' => 'DESC',
            'p.create_time' => 'DESC',
        ];//排序规则 销量4 收藏3 好评3
        $array = array();
        foreach ($result as $key => $value) {
            $temp_array = $value->toArray();
            $temp_array['product_list'] = ProductModel::build()
                ->alias('p')
                ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                    'p.product_id = ps.product_id')
                ->where(['p.classify_one_id' => $value['classify_id'], 'p.is_deleted' => '0', 'p.is_sale' => '1'])
                ->field($field)
                ->order($order)
                ->limit(8)
                ->select();
            $temp_array['product_list'] = self::get_img_src($temp_array['product_list']);
            $array[] = $temp_array;
            unset($temp_array);
        }
        return $array;
    }

    public static function get_info($product_id, $user_id = 0)
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.classify_one_id',
            'p.classify_two_id',
            'p.classify_three_id',
            'p.img_src_list',
            'ps.price',
            'p.sales',
            'p.look_count',
            'p.like_count',
            'p.collect_count',
            'p.content',
            'p.freight_charge',
            'p.processing_charge',
            'p.home_charge',
            'p.service_charge',
            'p.is_hot',
            'p.province',
            'p.is_deleted',
            'p.is_sale',
            "GROUP_CONCAT(area.name SEPARATOR '') as address",
            'p.is_new',
            'supplier.supplier_id as supplier_id',
            'supplier.name as supplier_name',
            'supplier.logo_src as supplier_logo_src',
            'IF(ISNULL(collect.id),"no","yes") as is_collect',
            'supplier.notice'
        ];

        $collect_condition = "collect.product_id = p.product_id AND collect.user_id = {$user_id}";
        $info = ProductModel::build()
            ->alias('p')
            ->join("yd_config_area area", "p.province = area.id OR p.city = area.id")
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->join('yd_shop_supplier supplier', 'p.supplier_id = supplier.supplier_id')
            ->join('yd_shop_collect collect', $collect_condition, 'LEFT')
            ->where(['p.product_id' => $product_id])
            ->field($field)
            ->find();
        if ($info != null && ($info['is_deleted'] == '1' || $info['is_sale'] == '0')) {
            return -1;
        }
        if (!empty($info)) {
            $info['img_src'] = empty($info['img_src_list']) ? array() : explode('|', $info['img_src_list']);
            unset($info['img_src_list'], $info['is_deleted'], $info['is_sale']);
        }
        $info['after_sale_service'] = ShopSupplierParams::build()->where([
            'supplier_id' => [
                'in',
                [0, $info['supplier_id']]
            ]
        ])->field(['title', 'logo_src'])->select()->toArray();
        $info['evaluate'] = EvaluateModel::productInfo($product_id, $user_id);
        $info['last_price'] = PriceRecordModel::build()->where(['product_id'=>$product_id])->order(['id'=>'DESC'])->value('original_price');
        return $info;
    }

    /**
     * @author: Airon
     * @time: 2017年8月1日
     * description:商品保障
     * @param $supplier_id
     * @return array
     */
    public static function afterSaleService($supplier_id)
    {
        return ShopSupplierParams::build()->where(['supplier_id' => ['in', [0, $supplier_id]]])
            ->field(['title', 'content', 'logo_src'])->select()->toArray();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:PC首页获取热门商品
     * @param $page
     * @param $num
     * @return array
     */
    public static function webPopular($page, $num)
    {
        $field = [
            'p.product_id',
            'p.name',
            'p.img_src_list',
            'ps.price',
            'p.classify_one_id',
            'p.is_hot',
            'p.is_new',
            '(p.sales*0.4+p.like_count*0.3+p.collect_count*0.3) as order_key'
        ];
        $order = [
            'order_key' => 'DESC',
            'p.create_time' => 'DESC',
        ];//排序规则 销量4 收藏3 好评3
        $temp_array['product_list'] = ProductModel::build()
            ->alias('p')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->where(['p.is_deleted' => '0', 'p.is_sale' => '1'])
            ->field($field)
            ->order($order)
            ->page($page, $num)
            ->select();
        $temp_array['product_list'] = self::get_img_src($temp_array['product_list']);
        $temp_array['count'] = ProductModel::build()
            ->alias('p')
            ->join("(SELECT product_id,min(price) as price FROM yd_shop_product_standard WHERE is_deleted = '0' GROUP BY product_id) ps",
                'p.product_id = ps.product_id')
            ->where(['p.is_deleted' => '0', 'p.is_sale' => '1'])->count();


        return $temp_array;
    }

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:处理图片,将img_src_list里的多张图片 处理成单张图片的 img_src
     * @param $array
     * @return mixed
     */
    protected static function get_img_src($array)
    {
        if ($array) {
            foreach ($array as $key => $value) {
                $array[$key]['img_src'] = empty($value['img_src_list']) ? "" : explode('|', $value['img_src_list'])[0];
                unset($array[$key]['img_src_list']);
            }
        }
        return $array;
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:a
     * @param $data
     * @return array|mixed
     *
     */
    public static function getMyCollections($data)
    {
        try {
            $result = ['collects' => [], 'count' => 0];
            $collects = CollectModel::build()->alias('c')
                ->field(['p.product_id', 'p.name', 'p.img_src_list', 'min(ps.price) as price', 'p.is_deleted', 'p.is_sale'])
                ->join('shop_product p', 'c.product_id = p.product_id', 'right')
                ->join('shop_product_standard ps', 'p.product_id = ps.product_id', 'left')
                ->where(['c.user_id' => $data['user_id']])
                ->group('c.product_id')
                ->page($data['page_index'], $data['page_size'])
                ->select()
                ->toArray();
            if ($collects) {
                foreach ($collects as $key => &$value) {
                    $value['img_src'] = empty($value['img_src_list']) ? "" : explode('|', $value['img_src_list'])[0];
                    $value['is_valid'] = ($value['is_deleted'] == '1' || $value['is_sale'] == '0') ? 0 : 1;//是否有效
                    unset($value['img_src_list'], $value['is_sale'], $value['is_deleted']);
                }
                $result['collects'] = $collects;
                $result['count'] = CollectModel::build()->where(['user_id' => $data['user_id']])->count();
            }
            return $result;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取我购买过的商品
     * @param $user_id
     * @param $pageIndex
     * @param $pageSize
     * @return array|int
     */
    public static function getMyProductsHistory($user_id, $pageIndex, $pageSize)
    {
        try {
            $result = OrderModel::build()->alias('o')->field(['p.product_id', 'p.name', 'p.img_src_list', 'min(ps.price) as price'])
                ->join('shop_product p', 'o.product_id = p.product_id', 'LEFT')
                ->join('shop_product_standard ps', 'p.product_id = ps.product_id', 'LEFT')
                ->where(['o.status' => '已完成', 'o.user_id' => $user_id, 'p.is_deleted' => '0', 'ps.is_deleted' => '0'])
                ->group('o.product_id')
                ->order(['o.order_id' => 'DESC'])->page($pageIndex, $pageSize)->select()->toArray();

            if (!empty($result)) {
                foreach ($result as &$v) {
                    $temp = explode('|', $v['img_src_list']);
                    $v['img_src'] = empty($temp) ? '' : $temp[0];
                    unset($v['img_src_list']);
                }
            }

            return $result;
        } catch (\Exception $e) {
            return -1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年9月19日
     * description:批量取消我的收藏商品
     * @param $user_id
     * @param $product_ids
     * @return int
     */
    public static function batchCancelMyProductCollections($user_id, $product_ids)
    {
        try {
            return CollectModel::build()->where(['user_id' => $user_id])->whereIn('product_id', $product_ids)->delete();
        } catch (\Exception $e) {
            return -1;
        }
    }

}