<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:19
 */

namespace app\api\logic;

use think\Db;
use think\Config;
use app\common\tools\wechatUtil;
use app\common\tools\RefundUtil;
use app\common\model\Demand;
use app\common\model\DemandOrder;
use app\common\model\DemandCloseReason as ReasonModel;

class DemandOrderLogic
{
    public static function addOrder($demand_id, $out_trade_no, $user_id, $fee, $pay_type)
    {
        $bool = DemandOrder::build()->where(['user_id' => $user_id, 'demand_id' => $demand_id])->find();
        if ($bool) {
            $data['order_num'] = $out_trade_no;
            $data['bill_no'] = $out_trade_no;
            $data['pay_type'] = $pay_type;
            $data['money'] = $fee;
            $data['create_time'] = time();
            return DemandOrder::build()->update($data, ['user_id' => $user_id, 'demand_id' => $demand_id]);
        }
        $data['demand_id'] = $demand_id;
        $data['user_id'] = $user_id;
        $data['order_num'] = $out_trade_no;
        $data['bill_no'] = $out_trade_no;
        $data['pay_type'] = $pay_type;
        $data['money'] = $fee;
        $data['create_time'] = time();
        return DemandOrder::build()->insert($data);
    }

    public static function getOrderInfo($out_trade_no)
    {
        return DemandOrder::build()->where(['bill_no' => $out_trade_no, 'pay_time' => 0])->find();
    }

    public static function updateOrderStatus($out_trade_no, $data)
    {
        return DemandOrder::build()->update($data, ['bill_no' => $out_trade_no]);
    }

    /**
     * @author: Rudy
     * @time: 2017年月日
     * description:关闭我的需求
     * @param $user_id
     * @param $data
     * @return array
     */
    public static function cancelMyDemand($user_id, $data)
    {
        /*
         * 1.检测过去的规定时间内是否有超过关闭需求的次数限制,若操作限制了,则直接返回
         * 2.检测需求是否存在,不存在直接返回 is_deleted = 0 and demand_id = $data['demand_id'] and user_id = $used_id and status = 1
         * 3.获取需求订单,若存在则需要在后续进行退款操作,不存在也不需要返回  demand_id = $data['demand_id'] and user_id = $user_id
         * 4.开启事务
         * 5.更新需求状态 status = 2
         * 6.生成随机退款单号,并更新需求订单状态 refund_no = random()  refund_time = time()
         * 7.退款操作
         * 8.关闭事务
         */
        $now = time();
        $config = Config::get('demand.cancel_limit');


        //第一步 :不能超过限制规则
        if (DemandOrder::build()->where(['user_id' => $user_id, 'refund_no' => ['neq', ''], 'refund_time' => ['gt', $now - $config['time']]])->count() >= $config['count']) {
            return ['code' => -1, 'msg' => $config['time'] . 's之内只能关闭' . $config['count'] . '个需求'];
        }

        //第二步 :需求状态判断
        if (($demand = Demand::build()->where(['is_deleted' => 0, 'demand_id' => $data['demand_id'], 'user_id' => $user_id])->find()) == null) {
            return ['code' => -2, 'msg' => '该需求不存在'];
        }
        if ($demand->status != 1) {
            return ['code' => -3, 'msg' => '只能关闭发布中的需求'];
        } else {
            $demand->status = 2;
        }

        //第三步 :需求订单判断
        $order = DemandOrder::build()->where(['demand_id' => $data['demand_id'], 'user_id' => $user_id])->find();

        //第四步 :开启事务
        Db::startTrans();
        try {
            //第五步 :更新需求状态
            if (!$demand->isUpdate()->save()) {
                throw new \Exception('需求状态更新失败', -4);
            }

            //填写关闭原因
            if (!ReasonModel::create($data, true)) {
                throw new \Exception('关闭原因插入失败', -7);
            }


            if ($order != null && $order->refund_no == '') {
                $randomNum = wechatUtil::getRandomString(32);
                //第六步 :生成随机退款单号,并更新需求订单状态
                if (!$order->isUpdate()->save(['refund_no' => $randomNum, 'refund_time' => $now])) {
                    throw new \Exception('需求订单状态更新失败' - 5);
                }
                //第七步 :退款操作
                //测试金额,正式环境需要按照金额比例进行退款   fee=($order->money)*$config['precent']/100
                if (!RefundUtil::getInstance()->setProps(['payType' => $order->pay_type, 'sum' => 0.01, 'fee' => 0.01, 'billNum' => $order->bill_no])->exec()) {
                    throw new \Exception('退款失败', -6);
                }
            }

            Db::commit();
            Demand::sendNotify();
            return ['code' => 1, 'msg' => '关闭成功'];
        } catch (\Exception $e) {
            Db::rollback();
            Demand::clearNotify();
            return ['code' => $e->getCode(), 'msg' => $e->getMessage()];
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月30日
     * description:检测需求是否已付款成功
     * @param $user_id
     * @param $demand_id
     * @return bool
     *
     */
    public static function checkOrder($user_id, $demand_id)
    {
        return Demand::build()->where(['user_id' => $user_id, 'demand_id' => $demand_id, 'status' => 1])->find() &&
            DemandOrder::build()->where(['user_id' => $user_id, 'demand_id' => $demand_id, 'pay_time' => ['neq', 0]])->find();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月30日
     * description:自动关闭需求
     */
    public function autoCancelDemandOrders()
    {
        $config = Config::get('demand.cancel_limit');
        $now = time();

        $list = Demand::build()->where(['status' => 1, 'create_time' => ['elt', $now - $config['auto_cancel']]])->select();
        if (!empty($list)) {
            foreach ($list as $row) {
                //查找相应订单
                $row->status = 2;
                $row->closed_type = 'overtime';
                $order = DemandOrder::build()->where(['demand_id' => $row['demand_id']])->find();
                Db::startTrans();
                try {
                    //更新需求
                    if (!$row->isUpdate()->save()) {
                        throw new \Exception('需求状态修改失败');
                    }
                    //更新订单
                    if ($order != null && $order->refund_no == '') {
                        if (!$order->isUpdate()->save(['refund_no' => wechatUtil::getRandomString(32), 'refund_time' => $now])) {
                            throw new \Exception('订单状态更新失败');
                        }
                        //todo 测试金额,正式环境需要按照金额比例进行退款   fee=($order->money)*$config['precent']/100
                        if (!RefundUtil::getInstance()
                            ->setProps(['payType' => $order->pay_type, 'sum' => 0.01, 'fee' => 0.01, 'billNum' => $order->bill_no])->exec()) {
                            throw new \Exception('退款失败');
                        }
                    }
                    Db::commit();
                    Demand::sendNotify();
                } catch (\Exception $e) {
                    Db::rollback();
                    Demand::clearNotify();
                }
            }
        }
    }
}