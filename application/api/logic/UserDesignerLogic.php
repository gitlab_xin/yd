<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/24
 * Time: 14:50
 */

namespace app\api\logic;

use app\common\model\UserDesigner as DesignerModel;

class UserDesignerLogic
{
    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:添加设计师审核资料
     * @param $data
     * @return int
     */
    public static function addDesigner($data)
    {
        $count = DesignerModel::build()->where(['user_id'=>$data['user_id'],'status'=>['in',[0,1]]])->count();
        if($count > 0){
            return 0;
        }elseif(DesignerModel::create($data,true)){
            return 1;
        }
    }

    /**
     * @author: Rudy
     * @time: 2017年8月24日
     * description:修改设计师状态
     * @param $user
     */
    public static function checkStatus($user)
    {
        if($user != null && $user->designer_status == 0 && DesignerModel::build()->where(['user_id'=>$user->user_id,'status'=>0])->count() > 0){
            $user->designer_status = 2;
        }
    }
}