<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/27
 * Time: 9:57
 */

namespace app\api\logic;

use app\common\model\ProductBean;
use app\common\model\SchemeZHGBean as SchemeBean;
use app\common\model\CustomizedScheme;
use app\common\model\User;
use app\common\tools\CommonMethod;

class SchemeZHGBGDGLogic extends SchemeBaseLogic
{
    private $productLogic = null;

    private static $discount = 0.68;

    private static $wujinPriceTable = [
        'WB-592/784-000' => 0,//吊柜五金包
        'WB-000' => 6.2,//抽屉五金包
        'W05-01-XX-00' => 12,//直臂铰链
        'W07-02-290-00' => 80,//290三节滑轨
        'W08-02-64-00' => 22,//64把手
        'HW-001' => 0,//连接件
        'HW-002' => 0,//24连接件杆
        'W02-01-3*12-00' => 0,//φ3mm 12mm 沉头自攻钉
        'HW-005' => 0,//φ4mm 25mm 把手配套螺丝
        'W02-01-3*30-00' => 0,//φ3mm 30mm 沉头自攻钉
        'W02-01-3.5*15-00' => 0,//φ3.5mm 15mm 沉头自攻钉
        'HW-008' => 0,//吊装连接器
        'WB-013' => 46,//吊柜柜门五金包
        'WB-014' => 54.8,//单层吊柜五金包
        'WB-015' => 59.2,//双层吊柜五金包
    ];

    private static $wujinUnitTable = [
        'WB-592/784-000' => "包",//吊柜五金包
        'WB-000' => "包",//抽屉五金包
        'W05-01-XX-00' => "套",//直臂铰链
        'W07-02-290-00' => "付",//290三节滑轨
        'W08-02-64-00' => "个",//64把手
        'HW-001' => "",//连接件
        'HW-002' => "",//24连接件杆
        'W02-01-3*12-00' => "个",//φ3mm 12mm 沉头自攻钉
        'HW-005' => "个",//φ4mm 25mm 把手配套螺丝
        'W02-01-3*30-00' => "个",//φ3mm 30mm 沉头自攻钉
        'W02-01-3.5*15-00' => "个",//φ3.5mm 15mm 沉头自攻钉
        'HW-008' => "个",//吊装连接器
        'WB-013' => "包",//吊柜柜门五金包
        'WB-014' => "包",//单层吊柜五金包
        'WB-015' => "包",//双层吊柜五金包
    ];

    private static $platePriceArr = [
        5 => 0,//5mm
        12 => 0,//12mm
        16 => 272,//16mm
        25 => 0,//16mm
    ];//板材价格


    /**
     * @author: Rudy
     * @time: 2017年10月21日
     * description:重构成原来参数进行验证
     * @param $scheme
     * @return array
     */
    public function getOriginParams($scheme)
    {
        $params = [];

        $params['scheme_name'] = isset($scheme['scheme_name']) ? $scheme['scheme_name'] : '';
        $params['scheme_pic'] = isset($scheme['scheme_pic']) ? $scheme['scheme_pic'] : '';
        $params['scheme_width'] = isset($scheme['scheme_width']) ? $scheme['scheme_width'] : '';
        $params['scheme_deep'] = isset($scheme['scheme_deep']) ? $scheme['scheme_deep'] : '';
        $params['scheme_height'] = isset($scheme['scheme_height']) ? $scheme['scheme_height'] : '';
        $params['scheme_hole_type'] = isset($scheme['scheme_hole_type']) ? $scheme['scheme_hole_type'] : '';

        return $params;
    }

    public function store($schemeData, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_type' => 'ZH',
            'scheme_type_text' => '壁挂吊柜',
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => $schemeData['scheme_deep'],
            'scheme_door_count' => 0,
            'scheme_color_no' => $schemeData['color_no'],
            'scheme_sk_color_no' => '000',
            'scheme_sk' => -1,
            'scheme_s_type' => '-1',
            'scheme_b_type' => 'BGDG',
            'scheme_hole_type' => $schemeData['scheme_hole_type'],
            'scheme_hole_width' => $schemeData['scheme_width'],
            'scheme_hole_height' => $schemeData['scheme_height'],
            'scheme_hole_sl_height' => $schemeData['scheme_height'],
            'scheme_hole_deep' => $schemeData['scheme_deep'],
            'scheme_hole_left' => 0,
            'scheme_hole_right' => 0,
            'is_left_wall' => '0',
            'is_right_wall' => '0',
            'is_left_wall_zt' => '0',
            'is_right_wall_zt' => '0',
            'scheme_pic' => $schemeData['scheme_pic'],
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
            'user_id' => $userId
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model->getLastInsID();
    }

    public function update($schemeData, $userId, $scheme_id)
    {
        $schemeModelData = [
            'scheme_color_no' => $schemeData['color_no'],
            'scheme_name' => $schemeData['scheme_name'],
            'scheme_width' => $schemeData['scheme_width'],
            'scheme_height' => $schemeData['scheme_height'],
            'scheme_deep' => $schemeData['scheme_deep'],
            'scheme_pic' => $schemeData['scheme_pic'],
            'scheme_hole_type' => $schemeData['scheme_hole_type'],
            'serial_array' => json_encode($schemeData, JSON_UNESCAPED_UNICODE),
        ];
        return CustomizedScheme::update($schemeModelData, ['user_id' => $userId, 'id' => $scheme_id]);
    }

    public function buildScheme($data)
    {
        $bean = new SchemeBean();
        $bean->setScheme_no(isset($data['scheme_no']) && !empty($data['scheme_no']) ? $data['scheme_no'] : '');
        $bean->setScheme_name(isset($data['scheme_name']) && !empty($data['scheme_name']) ? $data['scheme_name'] : '');
        $bean->setScheme_type(isset($data['scheme_type']) && !empty($data['scheme_type']) ? $data['scheme_type'] : '');
        $bean->setScheme_pic(isset($data['scheme_pic']) && !empty($data['scheme_pic']) ? $data['scheme_pic'] : '');
        $bean->setScheme_width(isset($data['scheme_width']) && !empty($data['scheme_width']) ? $data['scheme_width'] : 0);
        $bean->setScheme_height(isset($data['scheme_height']) && !empty($data['scheme_height']) ? $data['scheme_height'] : 0);
        $bean->setScheme_hole_width(isset($data['scheme_hole_width']) && !empty($data['scheme_hole_width']) ? $data['scheme_hole_width'] : 0);
        $bean->setM_left_mm(isset($data['m_left_mm']) && !empty($data['m_left_mm']) ? $data['m_left_mm'] : 0);
        $bean->setM_top_mm(isset($data['m_top_mm']) && !empty($data['m_top_mm']) ? $data['m_top_mm'] : 0);
        $bean->setScheme_color_no(isset($data['scheme_color_no']) && !empty($data['scheme_color_no']) ? $data['scheme_color_no'] : '');
        $bean->setScheme_color_name(isset($data['scheme_color_name']) && !empty($data['scheme_color_name']) ? $data['scheme_color_name'] : '');
        $bean->setScheme_sk_color_no(isset($data['scheme_sk_color_no']) && !empty($data['scheme_sk_color_no']) ? $data['scheme_sk_color_no'] : '');
        $bean->setScheme_door_count(isset($data['scheme_door_count']) && !empty($data['scheme_door_count']) ? $data['scheme_door_count'] : 0);
        $bean->setScheme_door_color_no(isset($data['scheme_door_color_no']) ? $data['scheme_door_color_no'] : '');
        $bean->setScheme_door_direction(isset($data['scheme_door_direction']) ? $data['scheme_door_direction'] : '');

        // scheme_schemes
        if (isset($data['scheme_schemes']) && is_array($data['scheme_schemes'])) {
            $schemeSchemesList = [];

            foreach ($data['scheme_schemes'] as $item) {
                $colScheme = new SchemeBean();
                $schemeProductsList = $schemedoorsList = [];
                foreach ($item['scheme_products'] as $productData) {
                    $schemeProductsList[] = ProductBean::build($productData);
                }
                if (isset($item['scheme_door_schemes'])) {
                    foreach ($item['scheme_door_schemes'] as $doorData) {
                        $schemedoorsList[] = SchemeBean::build($doorData);
                    }
                }
                $colScheme->setScheme_products($schemeProductsList);
                $colScheme->setScheme_door_schemes($schemedoorsList);
                $colScheme->setScheme_name(isset($item['scheme_name']) ? $item['scheme_name'] : '');
                $colScheme->setScheme_no(isset($item['scheme_no']) ? $item['scheme_no'] : '');
//                $colScheme->setScheme_color_no(isset($item['scheme_color_no']) ? $item['scheme_color_no'] : '');
//                $colScheme->setScheme_color_name(CommonMethod::getSchemeColorName($colScheme->getScheme_color_no()));
                $colScheme->setScheme_b_type(isset($item['scheme_b_type']) ? $item['scheme_b_type'] : '');
                $colScheme->setM_top_mm(isset($item['m_top_mm']) ? $item['m_top_mm'] : '');
                $colScheme->setM_left_mm(isset($item['m_left_mm']) ? $item['m_left_mm'] : '');
                $colScheme->setScheme_width(isset($item['scheme_width']) ? $item['scheme_width'] : '');
                $colScheme->setScheme_height(isset($item['scheme_height']) ? $item['scheme_height'] : '');

                $schemeSchemesList[] = $colScheme;
            }

            $bean->setScheme_schemes($schemeSchemesList);
        }
        // wcb
        if (isset($data['scheme_wcb_products']) && is_array($data['scheme_wcb_products'])) {
            $wcbList = [];

            foreach ($data['scheme_wcb_products'] as $wcbData) {
                $wcbList[] = ProductBean::build($wcbData);
            }

            $bean->setScheme_wcb_products($wcbList);
        }
        // ncb
        if (isset($data['scheme_ncb_products']) && is_array($data['scheme_ncb_products'])) {
            $ncbList = [];

            foreach ($data['scheme_ncb_products'] as $ncbData) {
                $ncbList[] = ProductBean::build($ncbData);
            }

            $bean->setScheme_ncb_products($ncbList);
        }
        // g 盖板
        if (isset($data['scheme_g_products']) && is_array($data['scheme_g_products'])) {
            $gList = [];

            foreach ($data['scheme_g_products'] as $gData) {
                $gList[] = ProductBean::build($gData);
            }

            $bean->setScheme_g_products($gList);
        }

        return $bean;
    }

    /**
     * @author: Airon
     * @time:   2019年5月
     * description 获取推荐方案列表
     * @param $scheme_width
     * @param $scheme_height
     * @param $page_index
     * @param $page_size
     * @return array|bool
     */
    public static function getRecommendSchemes($scheme_width, $scheme_height, $page_index, $page_size)
    {
        try {
            $where['scheme_width'] = ['between', [$scheme_width / 2, $scheme_width]];
            $where['scheme_height'] = ['elt', $scheme_height];
            $where['parent_id'] = 0;
            $where['scheme_type'] = "ZH";
            $where['scheme_b_type'] = "BGDG";
            $result = CustomizedScheme::build()
                ->alias('cs')
                ->join('yd_user u', 'u.user_id = cs.user_id')
                ->field(['cs.id as scheme_id', 'scheme_type', 'scheme_b_type', 'scheme_name', 'scheme_pic', 'create_time', 'update_time', 'u.username', 'u.avatar', 'cs.user_id'])
                ->where($where)->page($page_index, $page_size)->order(['id' => 'DESC'])->select()->toArray();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Airon
     * @time:   2019年5月
     * description 获取推荐方案详情
     * @param $scheme_id
     * @param $field
     * @return array|bool
     */
    public static function getRecommendSchemeInfo($scheme_id, $field)
    {
        try {
            $where['id'] = $scheme_id;
            $where['parent_id'] = 0;
            $where['scheme_type'] = "ZH";
            $where['scheme_b_type'] = "BGDG";
            $field[] = 'user_id';
            $result = CustomizedScheme::build()
                ->field($field)
                ->where($where)
                ->find();
            if (empty($result)) {
                return false;
            }
            $user = User::build()->where(['user_id' => $result['user_id']])->field(['username', 'avatar'])->find();
            $result['username'] = $user['username'];
            $result['avatar'] = $user['avatar'];
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function score($schemeData)
    {
        $data = [];
        $data['discount'] = static::$discount;
        $data['scheme_pic'] = $schemeData['scheme_pic'];

        //柜体板材计算
        $scoreKey = [];
        $score_products = [];
        foreach ($schemeData['productList']['cupBoardTable'] as $value) {
            foreach ($value['children'] as $v) {
                if (empty($v['colorData'])) {
                    //五金件不参与本次计算
                    continue;
                }
                if (in_array($v['id'], $scoreKey)) {
                    //已计算类型板直接增加数量和价格
                    $score_products[$v['id']]['product_count'] += $v['number'];
                    $score_products[$v['id']]['product_total_price'] = formatMoney($score_products[$v['id']]['product_price'] * $score_products[$v['id']]['product_count']);
                    $score_products[$v['id']]['product_dis_total_price'] = formatMoney($score_products[$v['id']]['product_total_price'] * $data['discount']);
                    continue;
                }
                //未计算 新增数组对象
                $temp = [];
                $temp['color_name'] = $v['colorData']['name'];
                $temp['color_no'] = $v['colorData']['color_no'];
                $temp['product_area'] = $v['area'];
                $temp['product_count'] = $v['number'];
                $temp['product_deep'] = $v['depth'];
                $temp['product_height'] = 0;
                $temp['product_name'] = $v['name'];
                $temp['product_no'] = $v['id'];
                $temp['product_price'] = formatMoney(static::$platePriceArr[$v['depth']] * $v['area']);
                $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
                $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
                $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
                $temp['product_spec'] = "";
                $temp['product_unit'] = "";
                $temp['product_width'] = 0;
                $scoreKey[] = $v['id'];
                $score_products[$v['id']] = $temp;
            }
        }

        //盖板
        foreach ($schemeData['productList']['coverBoardTable'] as $v) {
//            foreach ($value['children'] as $v) {
                if (empty($v['colorData'])) {
                    //五金件不参与本次计算
                    continue;
                }
                if (in_array($v['id'], $scoreKey)) {
                    //已计算类型板直接增加数量和价格
                    $score_products[$v['id']]['product_count'] += $v['number'];
                    $score_products[$v['id']]['product_total_price'] = formatMoney($score_products[$v['id']]['product_price'] * $score_products[$v['id']]['product_count']);
                    $score_products[$v['id']]['product_dis_total_price'] = formatMoney($score_products[$v['id']]['product_total_price'] * $data['discount']);
                    continue;
                }
                //未计算 新增数组对象
                $temp = [];
                $temp['color_name'] = $v['colorData']['name'];
                $temp['color_no'] = $v['colorData']['color_no'];
                $temp['product_area'] = $v['area'];
                $temp['product_count'] = $v['number'];
                $temp['product_deep'] = $v['depth'];
                $temp['product_height'] = 0;
                $temp['product_name'] = '盖板';
                $temp['product_no'] = $v['id'];
                $temp['product_price'] = formatMoney(static::$platePriceArr[$v['depth']] * $v['area']);
                $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
                $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
                $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
                $temp['product_spec'] = "";
                $temp['product_unit'] = "";
                $temp['product_width'] = 0;
                $scoreKey[] = $v['id'];
                $score_products[$v['id']] = $temp;
//            }
        }

        $data['score_products'] = $data['plate_products'] = [];
        $plateAreaArray = [5=>0,12=>0,16=>0,25=>0];

        //处理柜体板材清单数组 计算用料面积
        foreach ($score_products as $key => $value) {
            $plateAreaArray[$value['product_deep']] += $value['product_area'] * $value['product_count'];
            $data['score_products'][] = $value;
        }

        $data['plate_price'] = $data['dis_plate_price'] = 0;
        //计算板材清单明细
        foreach ($plateAreaArray as $key => $value) {
            //计算各种规格板材用料面积和对应价格
            $temp = [];
            $temp['color_name'] = "";
            $temp['color_no'] = "";
            $temp['product_area'] = formatMoney($value, 2);
            $temp['product_count'] = 0;
            $temp['product_deep'] = $key;
            $temp['product_height'] = 0;
            $temp['product_name'] = "{$key}mm厚";
            $temp['product_no'] = "";
            $temp['product_price'] = static::$platePriceArr[$key];
            $temp['product_dis_price'] = formatMoney(static::$platePriceArr[$key] * $data['discount']);
            $temp['product_total_price'] = formatMoney($temp['product_area'] * $temp['product_price']);
            $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
            $temp['product_spec'] = "";
            $temp['product_unit'] = "";
            $temp['product_width'] = 0;
            $data['plate_products'][] = $temp;
            $data['plate_price'] += $temp['product_total_price'];
            $data['dis_plate_price'] += $temp['product_dis_total_price'];
        }

        //组件计算
        $zjKey = $zjKey2 = [];
        $zjProducts  = $zjProductsContent = [];
        $data['dis_zj_price'] = $data['zj_price'] = $zjTotal = 0;
        foreach ($schemeData['productList']['doorTable'] as $value) {
            foreach ($value['children'] as $v) {
                if (empty($v['colorData'])) {
                    //五金件不参与本次计算
                    continue;
                }
                if (in_array($v['id'], $zjKey)) {
                    //已计算类型板直接增加数量和价格
                    $score_products[$v['id']]['product_count'] += $v['number'];
                    $score_products[$v['id']]['product_total_price'] = formatMoney($score_products[$v['id']]['product_price'] * $score_products[$v['id']]['product_count']);
                    $score_products[$v['id']]['product_dis_total_price'] = formatMoney($score_products[$v['id']]['product_total_price'] * $data['discount']);
                    continue;
                }

                if (str_has($value['id'], 'DM432')){
                    $name = '432吊柜柜门';
                    if (str_has($value['id'], '397')){
                        $price = 76;
                    }elseif(str_has($value['id'], '589')){
                        $price = 113;
                    }elseif(str_has($value['id'], '781')){
                        $price = 123;
                    }

                } elseif (str_has($value['id'], 'DM560')){
                    $name = '560吊柜柜门';
                    if (str_has($value['id'], '397')){
                        $price = 81;
                    }elseif(str_has($value['id'], '589')){
                        $price = 120;
                    }elseif(str_has($value['id'], '781')){
                        $price = 160;
                    }
                }
                //未计算 新增数组对象
                $temp = [];
                $temp['color_name'] = $v['colorData']['name'];
                $temp['color_no'] = $v['colorData']['color_no'];
                $temp['product_area'] = $v['area'];
                $temp['product_count'] = $v['number'];
                $temp['product_deep'] = $v['depth'];
                $temp['product_height'] = 0;
                $temp['product_name'] = $name;
                $temp['product_no'] = $v['id'];
                $temp['product_price'] = formatMoney($price);
                $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
                $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
                $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
                $temp['product_spec'] = "";
                $temp['product_unit'] = "";
                $temp['product_width'] = 0;
                $zjKey[] = $v['id'];
                $zjProducts[$v['id']] = $temp;
            }
        }

        foreach ($schemeData['productList']['drawerTable'] as $value) {
            foreach ($value['children'] as $v) {
                if (empty($v['colorData'])) {
                    //五金件不参与本次计算
                    continue;
                }
                if (in_array($v['id'], $zjKey)) {
                    //已计算类型板直接增加数量和价格
                    $zjProductsContent[$v['id']]['product_count'] += $v['number'];
//                    $zjProductsContent[$v['id']]['product_total_price'] = formatMoney($zjProductsContent[$v['id']]['product_price'] * $zjProductsContent[$v['id']]['product_count']);
//                    $zjProductsContent[$v['id']]['product_dis_total_price'] = formatMoney($zjProductsContent[$v['id']]['product_total_price'] * $data['discount']);
                    continue;
                }
                //未计算 新增数组对象
                $temp = [];
                $temp['color_name'] = $v['colorData']['name'];
                $temp['color_no'] = $v['colorData']['color_no'];
                $temp['product_area'] = $v['area'];
                $temp['product_count'] = $v['number'];
                $temp['product_deep'] = $v['depth'];
                $temp['product_height'] = 0;
                $temp['product_name'] = $v['name'];
                $temp['product_no'] = $v['id'];
                $temp['product_price'] = formatMoney(static::$platePriceArr[$v['depth']] * $v['area']);
                $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
//                $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
//                $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
                $temp['product_spec'] = "";
                $temp['product_unit'] = "";
                $temp['product_width'] = 0;
                $zjKey[] = $v['id'];
                $zjProductsContent[$v['id']] = $temp;
            }

            if (in_array($value['id'], $zjKey2)) {
                //已计算类型板直接增加数量和价格
                $zjProducts[$v['id']]['product_count'] += $value['number'];
                $zjProducts[$v['id']]['product_total_price'] = formatMoney($zjProducts[$v['id']]['product_price'] * $zjProducts[$v['id']]['product_count']);
                $zjProducts[$v['id']]['product_dis_total_price'] = formatMoney($zjProducts[$v['id']]['product_total_price'] * $data['discount']);
                continue;
            }

            $zjKey2[] = $value['id'];

            $temp = [];
            if (str_has($value['id'], 'DT-A')){
                if (str_has($value['id'], '432')){
                    $name = '432吊柜矮抽屉';
                    $price = 230;
                }else{
                    $name = '560吊柜矮抽屉';
                    $price = 255;
                }

            } elseif (str_has($value['id'], 'DT-G')){
                if (str_has($value['id'], '432')){
                    $name = '432吊柜高抽屉';
                    $price = 280;
                }else{
                    $name = '560吊柜高抽屉';
                    $price = 310;
                }
            }
            $temp['color_name'] = $value['colorData']['name'];
            $temp['color_no'] = $value['colorData']['color_no'];
            $temp['product_count'] = $value['number'];
            $temp['product_name'] = $name;
            $temp['product_no'] = $value['id'];
            $temp['product_price'] = formatMoney($price);
            $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
            $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
            $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);
            $temp['product_spec'] = "";
            $temp['product_unit'] = "";
            $temp['product_width'] = 0;
            $temp['product_content'] = $zjProductsContent;
            $zjKey2[] = $value['id'];
            $zjProducts[$value['id']] = $temp;

        }
        //处理柜体板材清单数组 计算用料面积
        $data['zj_products'] = [];
        $data['zj_price'] = $data['dis_zj_price'] = 0;
        foreach ($zjProducts as $key => $value) {
            $data['zj_products'][] = $value;
            $data['zj_price'] += $value['product_total_price'];
            $data['dis_zj_price'] += $value['product_dis_total_price'];
        }
        //五金
        $data['wujin_products'] = [];
        $wujinTotal = 0;
        foreach ($schemeData['productList']['wjTable'] as $value) {
            try {
                $temp = [];
                $temp['color_name'] = "";
                $temp['color_no'] = "";
                $temp['product_area'] = 0;
                $temp['product_count'] = $value['number'];
                $temp['product_deep'] = 0;
                $temp['product_height'] = 0;
                $temp['product_name'] = $value['name'];
                $temp['product_no'] = $value['id'];
                $temp['product_price'] = static::$wujinPriceTable[$value['id']];
                $temp['product_dis_price'] = formatMoney($temp['product_price'] * $data['discount']);
                $temp['product_total_price'] = formatMoney($temp['product_price'] * $temp['product_count']);
                $temp['product_dis_total_price'] = formatMoney($temp['product_total_price'] * $data['discount']);;
                $temp['product_spec'] = "";
                $temp['product_unit'] = static::$wujinUnitTable[$value['id']];
                $temp['product_width'] = 0;
                $data['wujin_products'][] = $temp;
                $wujinTotal += $temp['product_total_price'];
            } catch (\Exception $exception) {
                continue;
            }
        }


        $data['wujin_price'] = formatMoney($wujinTotal);
        $data['dis_wujin_price'] = formatMoney($data['discount'] * $wujinTotal);
        $data['zj_price'] = formatMoney($data['zj_price']);
        $data['dis_zj_price'] = formatMoney($data['dis_zj_price']);
        $data['plate_price'] = formatMoney($data['plate_price']);
        $data['dis_plate_price'] = formatMoney($data['dis_plate_price']);
        $data['gt_price'] = formatMoney($data['wujin_price']+$data['zj_price']+$data['plate_price']);
        $data['dis_gt_price'] = formatMoney($data['dis_wujin_price']+$data['dis_zj_price']+$data['dis_plate_price']);
        return $data;
    }
}