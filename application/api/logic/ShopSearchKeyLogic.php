<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/20
 * Time: 10:46
 */

namespace app\api\logic;

use app\common\model\ShopSearchKey as SearchKeyModel;

class ShopSearchKeyLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:添加搜索记录
     * @param $keyword
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function add($keyword)
    {
        if ($keyword == "" || is_null($keyword)) {
            return true;
        }
        $data['key'] = $keyword;
        $check = SearchKeyModel::build()->where($data)->count();
        if (empty($check)) {
            $data['create_time'] = time();
            $bool = SearchKeyModel::build()->insert($data);
        } else {
            $bool = SearchKeyModel::build()->where($data)->setInc('count');
        }
        return $bool;
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取热门搜索 默认10条
     * @return false|\PDOStatement|array|\think\Collection
     */
    public static function getHotSearch()
    {
        return SearchKeyModel::build()
            ->field(['key'])
            ->order(['count' => 'DESC'])
            ->limit(10)
            ->select()->toArray();
    }
}