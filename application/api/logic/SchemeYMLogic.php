<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 */

namespace app\api\logic;

use app\common\tools\CommonMethod;
use app\common\model\CustomizedScheme;
use app\common\model\SchemeRQSBean as SchemeBean;

class SchemeYMLogic
{
    public $productLogic = null;

    public function store(SchemeBean $schemeBean, $userId)
    {
        $schemeModelData = [
            'scheme_name' => $schemeBean->getScheme_name(),
            'user_id' => $userId,
            'scheme_type' => 'YM',
            'scheme_type_text' => '移门定制',
            'scheme_s_type' => -1,
            'scheme_width' => $schemeBean->getScheme_width(),
            'scheme_height' => $schemeBean->getScheme_height(),
            'scheme_deep' => $schemeBean->getScheme_hole_deep(),
            'scheme_door_count' => $schemeBean->getScheme_door_count(),
            'scheme_color_no' => $schemeBean->getScheme_color_no(),
            'scheme_sk_color_no' => $schemeBean->getScheme_sk_color_no(),
            'scheme_sk' => $schemeBean->getScheme_sk_color_no(),
            'scheme_hole_type' => $schemeBean->getScheme_hole_type(),
            'scheme_hole_width' => $schemeBean->getScheme_hole_width(),
            'scheme_hole_height' => $schemeBean->getScheme_hole_height(),
            'scheme_hole_sl_height' => $schemeBean->getScheme_hole_sl_height(),
            'scheme_hole_deep' => $schemeBean->getScheme_hole_deep(),
            'scheme_hole_left' => $schemeBean->getScheme_hole_left(),
            'scheme_hole_right' => $schemeBean->getScheme_hole_right(),
            'is_left_wall' => $schemeBean->getIs_left_wall() ? '1' : '0',
            'is_right_wall' => $schemeBean->getIs_right_wall() ? '1' : '0',
            'is_left_wall_zt' => $schemeBean->getIs_left_wall_zt() ? '1' : '0',
            'is_right_wall_zt' => $schemeBean->getIs_right_wall_zt() ? '1' : '0',
            'scheme_pic' => $schemeBean->getScheme_pic(),
            'serial_array' => json_encode($schemeBean->mainInfo(), JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
//            'scheme_door_width' => $schemeBean->getScheme_door_width(),
//            'scheme_door_width_one' => $schemeBean->getScheme_door_width_one(),
//            'scheme_door_height' => $schemeBean->getScheme_door_height()
        ];
        $model = CustomizedScheme::create($schemeModelData);
        return $model['id'];
    }

    public function update($data)
    {
        $scheme = CustomizedScheme::get($data['scheme_id']);

        $scheme->scheme_name = $data['scheme_name'];
        $scheme->scheme_pic = $data['scheme_pic'];
        $scheme->scheme_color_no = $data['door_color_no'];

        $origin_data = json_decode($scheme->serial_array, true);
        $origin_data['scheme_color_no'] = $data['door_color_no'];
        $origin_data['scheme_color_name'] = CommonMethod::getSchemeColorName($data['door_color_no']);
        $origin_data['scheme_door_color_no'] = $data['door_color_no'];
        $origin_data['scheme_door_have_hcq'] = $data['door_hcq'];

        foreach ($origin_data['scheme_schemes'] as &$door) {
            $door['scheme_color_no'] = $data['door_color_no'];
        }
        $scheme->serial_array = json_encode($origin_data, JSON_UNESCAPED_UNICODE);
        $scheme->update_time = time();


        return $scheme->isUpdate(true)->save() ? $scheme->id : 0;
    }

    public function buildScheme($scheme_id)
    {
        $serial_array = CustomizedScheme::build()->where(['id' => $scheme_id])->value('serial_array');

        $serial_array = json_decode($serial_array, true);

        $scheme = SchemeBean::build($serial_array);

        $scheme->setScheme_door_schemes($scheme->getScheme_schemes());
        $scheme->setScheme_have_door(1);
        $scheme->setScheme_price(0);
        $scheme->setScheme_dis_price(0);

        return $scheme;
    }

    /**
     * @param $data
     * @return SchemeBean
     */
    public function createYMScheme($data)
    {
        $scheme = new SchemeBean();
        $scheme->setScheme_type('YM');
        $scheme->setScheme_hole_width($data['hole_width']);//洞口宽
        $scheme->setScheme_hole_height($data['hole_height']);//洞口高
        $scheme->setScheme_hole_sl_height($data['hole_height']);//上梁高
        $scheme->setScheme_hole_deep(600);//洞深

        /*
         * 柱体默认宽度为15
         * b_width -> left
         * c_width -> right
         */
        if ($data['hole_type'] >= 5 && $data['hole_type'] <= 8) {
            $data['b_width'] = 15;
            $data['c_width'] = 0;
        } elseif ($data['hole_type'] >= 9 && $data['hole_type'] <= 12) {
            $data['b_width'] = 0;
            $data['c_width'] = 15;
        } elseif ($data['hole_type'] > 12) {
            $data['b_width'] = $data['c_width'] = 15;
        } else {
            $data['b_width'] = $data['c_width'] = 0;
        }
        $scheme->setScheme_hole_left($data['b_width']);//左柱体
        $scheme->setScheme_hole_right($data['c_width']);//右柱体

        $scheme->setScheme_door_count($data['door_count']);//移门数量
        $scheme->setScheme_door_count_init($data['door_count']);//移门数量
        $scheme->setScheme_have_door(isset($data['door_color_no']) ? 1 : 0);//是否有门
        $scheme->setScheme_door_haveHCQ(isset($data['door_hcq']) ? $data['door_hcq'] : 0);//是否有缓冲器

        $scheme->setScheme_width($data['hole_width'] - $data['b_width'] - $data['c_width']);//实际宽
        $scheme->setScheme_height($data['hole_height']);//实际高
        $scheme->setScheme_hole_type($data['hole_type']);//洞口类型

        $scheme->setScheme_name(isset($data['scheme_name']) ? $data['scheme_name'] : '移门定制-' . getRandomInt());//设置方案名称
        $scheme->setScheme_color_no(isset($data['door_color_no']) ? $data['door_color_no'] : '001');//主色调
        $scheme->setScheme_color_name(CommonMethod::getSchemeColorName($scheme->getScheme_color_no()));//设置颜色名称
        $scheme->setScheme_door_color_no(isset($data['door_color_no']) ? $data['door_color_no'] : '001');//门的颜色
        $scheme->setScheme_sk_color_no($data['sk_color_no']);//收口条颜色

        if (isset($data['scheme_pic'])) {
            $scheme->setScheme_pic($data['scheme_pic']);
        }

        $this->createBasicScheme($scheme);

        return $scheme;
    }

    /**
     * @param SchemeBean $scheme
     * @return SchemeBean
     */
    public function createBasicScheme(SchemeBean $scheme)
    {

        $doorCount = $scheme->getScheme_door_count();
        $hole_width = $scheme->getScheme_hole_width();
        $hole_height = $scheme->getScheme_hole_height();
        $hole_sl_height = $scheme->getScheme_hole_sl_height();
        $furniture_Width = $scheme->getScheme_width();
        $furniture_Height = $scheme->getScheme_height();

        if (0 == $doorCount) {
            if ($hole_width <= 3000) {
                $doorCount = 2;
            } elseif ($hole_width >= 3001 && $hole_width <= 4500) {
                $doorCount = 3;
            } else {
                $doorCount = 4;
            }
        }

        $sk_colorNo = $scheme->getScheme_sk_color_no();//收口颜色
        $holetype = $scheme->getScheme_hole_type();//洞口类型

        /*
         * 计算门的长宽
         */
        $has_ShouKou = $sk_colorNo == -1 ? false : true; //是否有收口条
        $doorleft = $has_ShouKou ? CUSTOM_SHOUKOU_IN_OUT : 0;
        $t = 0;
        $holetype = $holetype % 4;
        $isleftToWall = $isrightToWall = false;
        switch ($holetype) {
            case 0://左右都是墙体
                $isleftToWall = $isrightToWall = true;
                if ($has_ShouKou) {
                    $t = 70 + 25 * 2;
                    $doorleft += 35;
                }
                break;
            case 1://左右都不是墙体
                if ($has_ShouKou) {
                    $t = 25 * 2;
//                    $doorleft += 35;
                }
                break;
            case 2://右墙体
                $isrightToWall = true;
                if ($has_ShouKou) {
                    $t = 35 + 25 * 2;
                }
                break;
            default://左墙体
                $isleftToWall = true;
                if ($has_ShouKou) {
                    $t = 35 + 25 * 2;
                    $doorleft += 35;
                }
                break;
        }
        $doorWidth = CommonMethod::getDoorWidth($hole_width, $furniture_Width, $doorCount, 30, true, $has_ShouKou,
            $scheme->getIs_left_wall() || $scheme->getIs_left_wall_zt(), $scheme->getIs_right_wall() || $scheme->getIs_right_wall_zt());

        $doorHoleWidth = $scheme->getScheme_hole_width();
        if ($has_ShouKou) {
            $doorHoleWidth -= CUSTOM_SHOUKOU_IN_OUT * 2;
            if ($scheme->getIs_left_wall() || $scheme->getIs_left_wall_zt()) {
                $doorHoleWidth -= CUSTOM_PADDING;
            }
            if ($scheme->getIs_right_wall() || $scheme->getIs_right_wall_zt()) {
                $doorHoleWidth -= CUSTOM_PADDING;
            }
        }
        $doorHeight = CommonMethod::getDoorHeight($hole_sl_height, $furniture_Height, true, $has_ShouKou);

        $scheme->setScheme_width($hole_width - $t);//门口实际宽度
        $scheme->setScheme_s_width(intval($scheme->getScheme_width() / 8));//设置显示的宽度
        $scheme->setScheme_door_width($doorHoleWidth);//设置门的宽度
        $scheme->setScheme_door_width_one($doorWidth);//设置门的宽度
        $scheme->setScheme_door_height($doorHeight);//设置门的高度
        $scheme->setScheme_door_count($doorCount);//门的数量
        $scheme->setM_left(intval($doorleft / 8));//设置坐标开始的地方
        $scheme->setM_left_mm($doorleft);//设置坐标开始的地方


        /*
         * 设置门参数
         */
        $schemeSchemes = null;//方案中的方案,简称案中案
        for ($i = 0; $i < $doorCount; $i++) {
            $doorScheme = new SchemeBean();

            $doorScheme->setScheme_width($doorWidth);//方案宽度
            $doorScheme->setScheme_height($doorHeight);//方案高度
            $doorScheme->setScheme_door_width($doorWidth);//门宽度
            $doorScheme->setScheme_door_height($doorHeight);//门高度
            $doorScheme->setScheme_s_width(intval($doorWidth / 8));//设置显示的门宽
            $doorScheme->setScheme_s_height(intval($doorHeight / 8));//设置显示的门高
            $doorScheme->setScheme_color_no($scheme->getScheme_color_no());//设置门的颜色

            $schemeSchemes[] = $doorScheme;
        }
        $scheme->setScheme_schemes($schemeSchemes);

        /*
         * 设置收口
         */
        $fx = $fy = 0;
        if ($has_ShouKou) {
            $sk_products = [];
            $productLogic = $this->getProductLogic();
            $mTop = $hole_height - $hole_sl_height - $fy;
            //左收口
            $mLeft = $isleftToWall ? 0 - $fx : -34 - $fx;
            $sk_products[] = $productLogic->createZDYYG_SK($mLeft, $mTop, $hole_sl_height, 57, $sk_colorNo, 0);
            //右收口
            $mLeft = $isrightToWall ? $hole_width - 57 - $fx : $hole_width - 23 - $fx;
            $sk_products[] = $productLogic->createZDYYG_SK($mLeft, $mTop, $hole_sl_height, 57, $sk_colorNo, 0);
            //上收口
            if ($isleftToWall && $isrightToWall) {
                $mLeft = 57 - $fx;
                $skWidth = $hole_width - 57 * 2;
            } elseif ($isleftToWall) {
                $mLeft = 57 - $fx;
                $skWidth = $hole_width - 57 - 23;
            } elseif ($isrightToWall) {
                $mLeft = -34 + 57 - $fx;
                $skWidth = $hole_width - 57 - 23;
            } else {
                $mLeft = 23 - $fx;
                $skWidth = $hole_width - 23 - 23;
            }
            $sk_products[] = $this->getProductLogic()->createZDYYG_SK($mLeft, $mTop, 57, $skWidth, $sk_colorNo, 0);

            $scheme->setScheme_sk_products($sk_products);
        }
    }

    /**
     * @return ProductLogic
     */
    public function getProductLogic()
    {
        if (empty($this->productLogic)) {
            $this->productLogic = new ProductLogic();
        }
        return $this->productLogic;
    }
}
