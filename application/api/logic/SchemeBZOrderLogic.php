<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/10/20
 * Time: 20:37
 */

namespace app\api\logic;


use app\common\model\ProductBean;
use app\common\model\SchemeBean;

class SchemeBZOrderLogic
{
    /**
     * @author: Rudy
     * @time: 2017年10月23日
     * description:统计标准衣柜功能件
     * @param SchemeBean $schemeall
     */
    public function scoreBZYGSchemeProducts(SchemeBean $schemeall)
    {
        $score_products = $scheme_products = [];

        foreach ($schemeall->getScheme_schemes() as $onefur) {
            if ($onefur->getScheme_wcb_products() != null) {
                foreach ($onefur->getScheme_wcb_products() as $p) {
                    $p = change2ProductBean($p);
                    $p->setProduct_is_score(0);
                }
                $scheme_products = array_merge($scheme_products, $onefur->getScheme_wcb_products());
            }

            if ($onefur->getScheme_ncb_products() != null) {
                foreach ($onefur->getScheme_ncb_products() as $p) {
                    $p->setProduct_is_score(0);
                }
                $scheme_products = array_merge($scheme_products, $onefur->getScheme_ncb_products());
            }

            foreach ($onefur->getScheme_schemes() as $col) {
                if ($col->getScheme_b_type() == 'SJSC') {// 排除两单元写字桌集成组件，统计桌面板
//                    foreach ($col->getScheme_products() as $p) {
//                        if($p->getProduct_type() == 'ZM'){
//                            $p->setProduct_count(1);
//                            $scheme_products[] = $p;
//                        }
//                    }
                    continue;
                }

                foreach ($col->getScheme_products() as $p) {
                    $p->setProduct_is_score(0);
                    if ($p->getCom_products() != null) {
                        foreach ($p->getCom_products() as $c_p) {
                            $c_p->setProduct_is_score(0);
                        }
                    }
                }

                $scheme_products = array_merge($scheme_products, $col->getScheme_products());
            }
        }

        $no = '';

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            $product_type = $p->getProduct_type();
            if ($product_type == 'YTG') {
                continue;
            }
            if ($p->getProduct_is_score() == 0) {
                if (strpos($p->getProduct_no(), 'ytg') > 0) {
                    $p->setProduct_no(substr($p->getProduct_no(), 0, -3));
                    $p->setProduct_name('标准衣柜横隔板');
                }

                $p->setProduct_count($p->getProduct_type() == 'MD' ? 2 : 1);

                foreach ($scheme_products as $t_p) {
//                    $t_p = change2ProductBean($t_p);
                    if ($p != $t_p && $t_p->getProduct_is_score() == 0) {
                        if (strpos($t_p->getProduct_no(), 'ytg') > 0) {
                            $t_p->setProduct_no(substr($t_p->getProduct_no(), 0, -3));
                            $t_p->setProduct_name('标准衣柜横隔板');
                        }

                        if ($p->getProduct_no() == $t_p->getProduct_no()) {
                            $p->setProduct_count($p->getProduct_type() == 'MD' ? $p->getProduct_count() + 2 : $p->getProduct_count() + 1);
                            $t_p->setProduct_is_score(1);
                        }
                    }
                }
                $p->setProduct_is_score(1);
                $score_products[] = $p;

                //处理组合内的横隔板
                if ($p->getProduct_type() == 'MZC-01') {
                    //处理组合内的横隔板
                    foreach ($p->getCom_products() as $c_p) {
                        if ($c_p->getProduct_is_score() == '1') {
                            continue;
                        }
                        if(str_has($c_p->getProduct_type(),"YTG")){
                            continue;
                        }
                        foreach ($scheme_products as $h_t_p) {
                            $h_t_p = change2ProductBean($h_t_p);
                            if (empty($h_t_p->getCom_products())) {
                                continue;
                            }
                            foreach ($h_t_p->getCom_products() as $h_t_c_p) {
                                $h_t_c_p = change2ProductBean($h_t_c_p);
                                if ($h_t_c_p->getProduct_is_score() == 1) {
                                    continue;
                                }
                                if(str_has($h_t_c_p->getProduct_type(),"YTG")){
                                    continue;
                                }
                                if ($c_p->getProduct_no() != $h_t_c_p->getProduct_no()) {
                                    continue;
                                }
                                $c_p->setProduct_count($c_p->getProduct_count() + 1);
                                $h_t_c_p->setProduct_is_score(1);
                            }
                        }
                        $score_products[] = $c_p;
                    }
                }

            }
        }
        $schemeall->setScheme_score_products($score_products);
    }

    /**
     * @author: Rudy
     * @time: 2017年10月24日
     * description:统计标准衣柜组合件
     * @param SchemeBean $schemeall
     */
    public function scoreBZYGSchemeZJProducts(SchemeBean $schemeall)
    {
        $score_products = [];
        $totalPrice = $dis_totalPrice = 0;

        /*
         * 拉篮裤抽、写字桌组件
         */
        foreach ($schemeall->getScheme_schemes() as $onefur) {
            foreach ($onefur->getScheme_schemes() as $col) {
                $col = change2SchemeBean($col);
                $scheme_no = $col->getScheme_no();

                if (strpos($scheme_no, 'Z-DZX432') !== false) {

                    $d_product = $this->buildTableProduct($col, 1046, $schemeall->getScheme_discount());

                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    continue;
                } elseif (strpos($scheme_no, 'Z-DZX560') !== false) {
                    $d_product = $this->buildTableProduct($col, 1302, $schemeall->getScheme_discount());

                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    continue;
                } elseif (strpos($scheme_no, 'Z-DZG432') !== false) {
                    $d_product = $this->buildTableProduct($col, 851, $schemeall->getScheme_discount());

                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    foreach ($col->getScheme_products() as $zzProduct) {
                        $zzProduct = change2ProductBean($zzProduct);
                        if ($zzProduct->getProduct_type() == 'ZZ') {
                            $d_product = new ProductBean();

                            $d_product->setSeq_type(610);
                            $d_product->setProduct_name('垂直写字桌');
                            $d_product->setProduct_color_name($zzProduct->getProduct_color_name());
                            $d_product->setProduct_color_no($zzProduct->getProduct_color_no());
                            $d_product->setProduct_no('Z-DZ1478-' . $zzProduct->getProduct_color_no());
                            $d_product->setProduct_width($col->getScheme_width());
                            $d_product->setProduct_count(1);
                            $d_product->setProduct_price(980.00);
                            $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
                            $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                            $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                            $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                            $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                            $score_products[] = $d_product;
                            break;
                        }
                    }
                    continue;
                } elseif (strpos($scheme_no, 'Z-DZG560') !== false) {
                    $d_product = $this->buildTableProduct($col, 1026, $schemeall->getScheme_discount());

                    $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                    $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();

                    $score_products[] = $d_product;
                    foreach ($col->getScheme_products() as $zzProduct) {
                        $zzProduct = change2ProductBean($zzProduct);
                        if ($zzProduct->getProduct_type() == 'ZZ') {
                            $d_product = new ProductBean();

                            $d_product->setSeq_type(610);
                            $d_product->setProduct_name('垂直写字桌');
                            $d_product->setProduct_color_name($zzProduct->getProduct_color_name());
                            $d_product->setProduct_color_no($zzProduct->getProduct_color_no());
                            $d_product->setProduct_no('Z-DZ1478-' . $zzProduct->getProduct_color_no());
                            $d_product->setProduct_width($col->getScheme_width());
                            $d_product->setProduct_count(1);
                            $d_product->setProduct_price(980.00);
                            $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
                            $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                            $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));
                            $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                            $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                            $score_products[] = $d_product;
                            break;
                        }
                    }
                    continue;
                }

                foreach ($col->getScheme_products() as $p) {
                    $p = change2ProductBean($p);
                    if ($p->getCom_products() != null) {
                        foreach ($p->getCom_products() as $c_p) {
                            $c_p = change2ProductBean($c_p);
                            if ($c_p->getProduct_is_score() == 0) {
                                $c_p_product_no = $c_p->getProduct_no();
                                if (strpos($c_p_product_no, 'L-') !== false || strpos($c_p_product_no, 'K-') !== false) {
                                    $c_p->setProduct_count(1);
                                    $repeat = false;
                                    foreach ($score_products as $rep) {
                                        if ($rep->getProduct_no() == $c_p->getProduct_no()) {
                                            $repeat = true;
                                            $rep->setProduct_count($rep->getProduct_count() + 1);
                                            break;
                                        }
                                    }
                                    if (!$repeat) {
                                        $score_products[] = $c_p;
                                    }
                                    $c_p->setProduct_is_score(1);
                                }
                            }

                            //处理组合内的拉篮和裤抽 Near
                            if ($c_p->getProduct_type() == 'MH') {
                                if (empty($c_p->getCom_products())) {
                                    continue;
                                }
                                foreach ($c_p->getCom_products() as $c_p_c) {
                                    $productNo = $c_p_c->getProduct_no();
                                    if (is_starts_with($productNo, 'L-') || is_starts_with($productNo, 'LB-') || is_starts_with($productNo, 'K-')) {
                                        if ($c_p_c->getProduct_is_score() == 1) {
                                            continue;
                                        }
                                        $c_p_c->setProduct_color_no("");//组件没有颜色
                                        $c_p_c->setProduct_count(1);
                                        $c_p_c->setProduct_is_score(1);
                                        foreach ($col->getScheme_products() as $t_p) {
                                            $t_p = change2ProductBean($t_p);
                                            if (empty($t_p->getCom_products())){
                                                continue;
                                            }
                                            foreach ($t_p->getCom_products() as $t_c_p) {
                                                foreach ($t_c_p->getCom_products() as $t_c_p_c) {
                                                    $t_c_p_c = change2ProductBean($t_c_p_c);
                                                    if ($t_c_p_c->getProduct_is_score() == 1) {
                                                        continue;
                                                    }
                                                    if ($c_p_c->getProduct_no() != $t_c_p_c->getProduct_no()) {
                                                        continue;
                                                    }
                                                    $c_p_c->setProduct_count($c_p_c->getProduct_count() + 1);
                                                    $t_c_p_c->setProduct_is_score(1);
                                                }
                                            }
                                        }
                                        if (isset($score_products[$productNo])) {
                                            $s_p = change2ProductBean($score_products[$productNo]);
                                            $s_p->setProduct_count($c_p_c->getProduct_count() + $s_p->getProduct_count());
                                        }else {
                                            $score_products[$productNo] = $c_p_c;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        /*
         * 简欧上帽
         */
        if ($schemeall->getScheme_dm_schemes() != null) {
            foreach ($schemeall->getScheme_dm_schemes() as $dm_s) {
                $dm_s = change2SchemeBean($dm_s);
                if ($dm_s->getScheme_type() == 2) {
                    $exist = false;
                    foreach ($score_products as $s) {
                        if ($s->getProduct_no() == 'BM-SM-912-138-P-004') {
                            $s->setProduct_count($s->getProduct_count() + 1);
                            $s->setProduct_total_price(formatMoney($s->getProduct_price() * $s->getProduct_count()));
                            $s->setProduct_dis_total_price(formatMoney($s->getProduct_dis_price() * $s->getProduct_count()));

                            $totalPrice = $totalPrice + $s->getProduct_price();
                            $dis_totalPrice = $dis_totalPrice + $s->getProduct_dis_price();

                            $exist = true;
                            break;
                        }
                    }

                    if (!$exist) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('简欧上帽');
                        $t_p->setProduct_no('BM-SM-912-138-P-004');
                        $t_p->setProduct_color_name('暖白');
                        $t_p->setProduct_color_no('004');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_price(85.00);
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $schemeall->getScheme_discount()));
                        $t_p->setProduct_total_price(formatMoney($t_p->getProduct_price() * $t_p->getProduct_count()));
                        $t_p->setProduct_dis_total_price(formatMoney($t_p->getProduct_dis_price() * $t_p->getProduct_count()));

                        $totalPrice = $totalPrice + $t_p->getProduct_total_price();
                        $dis_totalPrice = $dis_totalPrice + $t_p->getProduct_dis_total_price();

                        $score_products[] = $t_p;
                    }

                } else {
                    $exist = false;
                    foreach ($score_products as $s) {
                        if ($s->getProduct_no() == 'BM-SM-1368-138-P-004') {
                            $s->setProduct_count($s->getProduct_count() + 1);
                            $s->setProduct_total_price(formatMoney($s->getProduct_price() * $s->getProduct_count()));
                            $s->setProduct_dis_total_price(formatMoney($s->getProduct_dis_price() * $s->getProduct_count()));

                            $totalPrice = $totalPrice + $s->getProduct_price();
                            $dis_totalPrice = $dis_totalPrice + $s->getProduct_dis_price();

                            $exist = true;
                            break;
                        }
                    }

                    if (!$exist) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('简欧上帽');
                        $t_p->setProduct_no('BM-SM-1368-138-P-004');
                        $t_p->setProduct_color_name('暖白');
                        $t_p->setProduct_color_no('004');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_price(formatMoney(85 / $schemeall->getScheme_discount(), 2));
                        $t_p->setProduct_dis_price(85);
                        $t_p->setProduct_total_price(formatMoney($t_p->getProduct_price() * $t_p->getProduct_count()));
                        $t_p->setProduct_dis_total_price(formatMoney($t_p->getProduct_dis_price() * $t_p->getProduct_count()));

                        $totalPrice = $totalPrice + $t_p->getProduct_total_price();
                        $dis_totalPrice = $dis_totalPrice + $t_p->getProduct_dis_total_price();

                        $score_products[] = $t_p;
                    }
                }
            }

        }

        foreach ($score_products as $p) {
            $pNo = $p->getProduct_no();
            if (strpos($pNo, 'L') !== false) {
                $width = $p->getProduct_width();
                if ($width <= 464) {
                    $p->setProduct_price(410);
                } elseif ($width <= 560) {
                    $p->setProduct_price(450);
                } elseif ($width <= 656) {
                    $p->setProduct_price(490);
                } elseif ($width <= 752) {
                    $p->setProduct_price(530);
                } elseif ($width <= 848) {
                    $p->setProduct_price(610);
                } else {
                    $p->setProduct_price(690);
                }
                $p->setProduct_width($width);
                $p->setProduct_dis_price(formatMoney($p->getProduct_price() * $schemeall->getScheme_discount()));
                $p->setProduct_total_price(formatMoney($p->getProduct_price() * $p->getProduct_count()));
                $p->setProduct_dis_total_price(formatMoney($p->getProduct_dis_price() * $p->getProduct_count()));

                $totalPrice = $totalPrice + $p->getProduct_total_price();
                $dis_totalPrice = $dis_totalPrice + $p->getProduct_dis_total_price();

            } elseif (strpos($pNo, 'K') !== false) {
                $width = $p->getProduct_width();
                if ($width <= 464) {
                    $p->setProduct_price(350);
                } elseif ($width <= 560) {
                    $p->setProduct_price(385);
                } elseif ($width <= 656) {
                    $p->setProduct_price(420);
                } elseif ($width <= 752) {
                    $p->setProduct_price(455);
                }elseif ($width <= 848) {
                    $p->setProduct_price(490);
                } else {
                    $p->setProduct_price(525);
                }
                $p->setProduct_width($width);
                $p->setProduct_dis_price(formatMoney($p->getProduct_price() * $schemeall->getScheme_discount()));
                $p->setProduct_total_price(formatMoney($p->getProduct_price() * $p->getProduct_count()));
                $p->setProduct_dis_total_price(formatMoney($p->getProduct_dis_price() * $p->getProduct_count()));

                $totalPrice = $totalPrice + $p->getProduct_total_price();
                $dis_totalPrice = $dis_totalPrice + $p->getProduct_dis_total_price();
            }
        }

        $d_product = $d_s = null;

        if ($schemeall->getScheme_height() == 2019) {//20青少年衣柜木板门?
            $d_s = $schemeall->getScheme_door_schemes()[0];
            $d_s = change2SchemeBean($d_s);

            $d_product = new ProductBean();
            $d_product->setProduct_name($d_s->getScheme_name());
            $d_product->setProduct_color_name($d_s->getScheme_color_name());
            $d_product->setProduct_color_no($d_s->getScheme_color_no());
            $d_s->setScheme_no(str_replace('MB', 'BM', $d_s->getScheme_no()));
            $d_product->setProduct_no($d_s->getScheme_no());
            $d_product->setProduct_spec($d_s->getScheme_height() . '*' . $d_s->getScheme_width() . '*16');
            $d_product->setProduct_count(count($schemeall->getScheme_door_schemes()));
            $d_product->setProduct_width($d_s->getScheme_width());
            $d_product->setProduct_height($d_s->getScheme_height());
            $d_product->setProduct_deep(16);
            $d_product->setProduct_price(288.00);
            $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
            $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
            $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));

            $totalPrice = $totalPrice + $d_product->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
            $score_products[] = $d_product;
        } else {
            $d_s = $schemeall->getScheme_door_schemes()[0];
            $d_s = change2SchemeBean($d_s);

            if ($d_s->getScheme_type() == 0) {
                foreach ($schemeall->getScheme_door_schemes() as $d_s_i) {
                    $d_s_i = change2SchemeBean($d_s_i);
                    $d_s_i->setScheme_no(str_replace('MB', 'BM', $d_s_i->getScheme_no()));
                    $inscore = false;
                    if (count($score_products) > 0) {
                        foreach ($score_products as $score) {
                            if ($d_s_i->getScheme_no() == $score->getProduct_no()) {
                                $score->setProduct_count($score->getProduct_count() + 1);
                                $score->setProduct_total_price(formatMoney($score->getProduct_price() * $score->getProduct_count()));
                                $score->setProduct_dis_total_price(formatMoney($score->getProduct_dis_price() * $score->getProduct_count()));

                                $totalPrice = $totalPrice + $score->getProduct_price();
                                $dis_totalPrice = $dis_totalPrice + $score->getProduct_dis_price();

                                $inscore = true;
                            }
                        }
                    }

                    if (!$inscore) {//
                        $d_product = new ProductBean();
                        $d_product->setProduct_name($d_s_i->getScheme_name());
                        $d_product->setProduct_color_name($d_s_i->getScheme_color_name());
                        $d_product->setProduct_color_no($d_s_i->getScheme_color_no());
                        $d_s_i->setScheme_no(str_replace('MB', 'BM', $d_s_i->getScheme_no()));
                        $d_product->setProduct_no($d_s_i->getScheme_no());
                        $d_product->setProduct_count(1);
                        $d_product->setProduct_width($d_s_i->getScheme_width());
                        $d_product->setProduct_height($d_s_i->getScheme_height());
                        $d_product->setProduct_deep(16);
                        if ($d_s_i->getScheme_width() > 460) {//24标准衣柜木板门
                            $d_product->setProduct_price(395.00);
                            $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
                        } else {
                            $d_product->setProduct_price(370.00);
                            $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
                        }
                        $d_product->setProduct_spec($d_s_i->getScheme_height() . '*' . $d_s_i->getScheme_width() . '*16');
                        $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                        $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));

                        $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                        $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                        $score_products[] = $d_product;
                    }
                }
            } else {
                foreach ($schemeall->getScheme_door_schemes() as $d_s_i) {
                    $d_s_i = change2SchemeBean($d_s_i);
                    $d_s_i->setScheme_no(str_replace('MB', 'BM', $d_s_i->getScheme_no()));
                    $inscore = false;
                    if (count($score_products) > 0) {
                        foreach ($score_products as $score) {
                            if ($d_s_i->getScheme_no() == $score->getProduct_no()) {
                                $score->setProduct_count($score->getProduct_count() + 1);
                                $score->setProduct_total_price(formatMoney($score->getProduct_price() * $score->getProduct_count()));
                                $score->setProduct_dis_total_price(formatMoney($score->getProduct_dis_price() * $score->getProduct_count()));

                                $totalPrice = $totalPrice + $score->getProduct_price();
                                $dis_totalPrice = $dis_totalPrice + $score->getProduct_dis_price();

                                $inscore = true;
                            }
                        }
                    }

                    if (!$inscore) {
                        $d_product = new ProductBean();
                        $d_product->setProduct_name($d_s_i->getScheme_name());
                        $d_product->setProduct_color_name($d_s_i->getScheme_color_name());
                        $d_product->setProduct_color_no($d_s_i->getScheme_color_no());
                        $d_s_i->setScheme_no(str_replace('MB', 'BM', $d_s_i->getScheme_no()));
                        $d_product->setProduct_no($d_s_i->getScheme_no());
                        $d_product->setProduct_count(1);
                        $d_product->setProduct_width($d_s_i->getScheme_width());
                        $d_product->setProduct_height($d_s_i->getScheme_height());
                        $d_product->setProduct_deep(16);
                        if ($d_s_i->getScheme_type() == 1) {
                            $d_product->setProduct_price(698);
                        } else {
                            if ($d_s_i->getScheme_width() > 460) {
                                $d_product->setProduct_price(645.00);
                            } else {
                                $d_product->setProduct_price(615.00);
                            }
                        }
                        $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $schemeall->getScheme_discount()));
                        $d_product->setProduct_spec($d_s_i->getScheme_height() . '*' . $d_s_i->getScheme_width() . '*16');
                        $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
                        $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));

                        $totalPrice = $totalPrice + $d_product->getProduct_total_price();
                        $dis_totalPrice = $dis_totalPrice + $d_product->getProduct_dis_total_price();
                        $score_products[] = $d_product;
                    }
                }
            }
        }
        $totalPrice = $dis_totalPrice = 0;
        foreach ($score_products as $v){
            $totalPrice += $v->getProduct_total_price();
            $dis_totalPrice += $v->getProduct_dis_total_price();
        }
        $schemeall->setScheme_zj_price($totalPrice);
        $schemeall->setScheme_dis_zj_price($dis_totalPrice);
        $schemeall->setScheme_score_zj_products($score_products);

    }

    /**
     * @author: Rudy
     * @time: 2017年10月24日
     * description:统计综合柜板材面积
     * @param SchemeBean $scheme
     */
    public function scoreBZYGSchemeplates(SchemeBean $scheme)
    {
        $totalPrice = $dis_totalPrice = 0;
        $deep_arr = [25, 16, 12, 5];

        $scheme_plates = [];
        $score_products = $scheme->getScheme_score_products();
        foreach ($deep_arr as $i => $deep) {
            $p_product = new ProductBean();
            $p_product->setProduct_deep($deep);
            $p_product->setProduct_name($deep . 'mm厚');

            foreach ($score_products as $p) {
                $p = change2ProductBean($p);
                if ($deep == $p->getProduct_deep()) {
                    $p_product->setProduct_area(
                        number_format(getLength($p->getProduct_width()) * getLength($p->getProduct_height()) / 1000000, 2, '.', '')
                        * $p->getProduct_count()
                        + $p_product->getProduct_area()
                    );
                }
            }
            if ($p_product->getProduct_area() > 0) {
                $p_product->setProduct_price(formatMoney(getPlatePrice($i)));
                $p_product->setProduct_dis_price(
                    formatMoney($p_product->getProduct_price() * $scheme->getScheme_discount())
                );
                $p_product->setProduct_total_price(
                    formatMoney($p_product->getProduct_price() * $p_product->getProduct_area())
                );
                $p_product->setProduct_dis_total_price(
                    formatMoney($p_product->getProduct_dis_price() * $p_product->getProduct_area())
                );

                $totalPrice = formatMoney($totalPrice + $p_product->getProduct_total_price());
                $dis_totalPrice = formatMoney($dis_totalPrice + $p_product->getProduct_dis_total_price());
            }
            $scheme_plates[] = $p_product;
        }

        $scheme->setScheme_score_plates($scheme_plates);
        $scheme->setScheme_plate_price($totalPrice);
        $scheme->setScheme_dis_plate_price($dis_totalPrice);
    }

    /**
     * @author: Rudy
     * @time: 2017年10月24日
     * description:统计综合柜五金件
     * @param SchemeBean $scheme
     */
    public function scoreBZYGSchemeWujin(SchemeBean $scheme)
    {
        $scheme_products = $wujinMap = [];

        foreach ($scheme->getScheme_schemes() as $onebzg) {
            foreach ($onebzg->getScheme_schemes() as $col) {
                $col = change2SchemeBean($col);

                if (strpos($col->getScheme_no(), 'Z-DZG') !== false) {// 垂直桌柜体组件
                    if (!isset($wujinMap['WB-001'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('垂直桌柜体组件五金包');
                        $t_p->setProduct_no('WB-001');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(174);
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-001'];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                    if (!isset($wujinMap['WB-002'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('垂直写字桌五金包');
                        $t_p->setProduct_no('WB-002');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(22);
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-002'];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (strpos($col->getScheme_no(), 'Z-DZX') !== false) {// 写字桌功能组件

                    if (!isset($wujinMap['WB-004'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('写字桌组件五金包');
                        $t_p->setProduct_no('WB-004');
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit('包');
                        $t_p->setProduct_price(150);
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['WB-004'];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }

                } else {
                    $scheme_products = array_merge($scheme_products, $col->getScheme_products());
                }
            }
        }

        foreach ($scheme_products as $p) {
            $p = change2ProductBean($p);
            $product_type = $p->getProduct_type();
            if ($product_type == 'MWC' || $product_type == 'MZC' || $product_type == 'MXC' || $product_type == 'DC') {// 外侧板 || 中侧板 || 衣柜+书柜衔接侧板 || 综合柜组件外侧板
                if (!isset($wujinMap['W04-29-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('脚垫');
                    $t_p->setProduct_no('W04-29-XX-00');
                    $t_p->setProduct_count(2);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(88);
                    $t_p->setProduct_price(1.4);
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-29-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
                }
            } elseif ($product_type == 'MZC-01') {
                if (!isset($wujinMap['WB-017'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('垂直螺丝五金包');
                    $t_p->setProduct_no('WB-017');
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit('包');
                    $t_p->setSeq(100);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['WB-017'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            } elseif ($product_type == 'MB') {// 背板
                if (!isset($wujinMap['W01-02-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('背板连接件');
                    $t_p->setProduct_no('W01-02-XX-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(2);
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-02-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*35-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('35沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*35-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(11);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*35-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*13-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('13沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*13-00');
                    $t_p->setProduct_count(4 * 3);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(7);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*13-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4 * 3);
                }
            } elseif ($product_type == 'MH' || $product_type == 'MZCB' || $product_type == 'MX') {// 横隔板 层板 || 中层板 || 小层板
                if (!isset($wujinMap['W01-01-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('层板连接件');
                    $t_p->setProduct_no('W01-01-XX-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('套');
                    $t_p->setSeq(1);
                    $t_p->setProduct_price(formatMoney(6));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-01-XX-00'];
                    $t_p = change2ProductBean($t_p);
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }
            } elseif ($product_type == 'MD') {// 底担
                if (!isset($wujinMap['W01-03-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('底担连接件');
                    $t_p->setProduct_no('W01-03-XX-00');
                    $t_p->setProduct_count(4);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(89);
                    $t_p->setProduct_price(formatMoney(7));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W01-03-XX-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
                }

                if (!isset($wujinMap['W02-01-3*20-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('20沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*20-00');
                    $t_p->setProduct_count(8);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(9);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*20-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 8);
                }

                if (!isset($wujinMap['W02-01-3*25-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('25沉头自攻钉');
                    $t_p->setProduct_no('W02-01-3*25-00');
                    $t_p->setProduct_count(8);
                    $t_p->setProduct_unit('个');
                    $t_p->setSeq(10);
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W02-01-3*25-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 8);
                }
            } elseif (($index = strpos($product_type, 'YTG')) !== false) {// 衣通杆
                if ($index > 0) {
                    if (!isset($wujinMap['W01-01-XX-00'])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name('层板连接件');
                        $t_p->setProduct_no('W01-01-XX-00');
                        $t_p->setProduct_count(4);
                        $t_p->setProduct_unit('套');
                        $t_p->setSeq(1);
                        $t_p->setProduct_price(formatMoney(6));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap['W01-01-XX-00'];
                        $t_p = change2ProductBean($t_p);
                        $t_p->setProduct_count($t_p->getProduct_count() + 4);
                    }
                }

                $width = $p->getProduct_width();

                if (abs($width - 468 - 12) < 30) {
                    $p_no = 'W06-01-468-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(468);
                        $t_p->setSeq(1120);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(468)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (abs($width - 412 - 12) < 30) {
                    $p_no = 'W06-01-412-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(412);
                        $t_p->setSeq(1119);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(412)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (abs($width - 868 - 12) < 30) {
                    $p_no = 'W06-01-868-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(868);
                        $t_p->setSeq(1123);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(868)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (abs($width - 932 - 12) < 30) {
                    $p_no = 'W06-01-932-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(932);
                        $t_p->setSeq(1124);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(932)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (abs($width - 740 - 12) < 30) {
                    $p_no = 'W06-01-740-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(740);
                        $t_p->setSeq(1122);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(740)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                } elseif (abs($width - 516 - 12) < 30) {
                    $p_no = 'W06-01-516-00';
                    if (!isset($wujinMap[$p_no])) {
                        $t_p = new ProductBean();
                        $t_p->setProduct_name("衣通杆");
                        $t_p->setProduct_no($p_no);
                        $t_p->setProduct_count(1);
                        $t_p->setProduct_unit("支");
                        $t_p->setProduct_width(516);
                        $t_p->setSeq(1121);
                        $t_p->setProduct_price(formatMoney(
                            100 * ((getLength(516)) / 1000)
                        ));
                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                    } else {
                        $t_p = $wujinMap[$p_no];
                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                    }
                }

                if (!isset($wujinMap['W04-07-XX-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name("衣通托五金包");
                    $t_p->setProduct_no("W04-07-XX-00");
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_unit("套");
                    $t_p->setSeq(1200);
                    $t_p->setProduct_price(formatMoney(14));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W04-07-XX-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }

//                if (!isset($wujinMap['W02-01-3*13-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("13沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*13-00");
//                    $t_p->setProduct_count(2);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(7);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*13-00'];
//                    $t_p->setProduct_count($t_p->getProduct_count() + 2);
//                }
//
//                if (!isset($wujinMap['W02-01-3*25-00'])) {
//                    $t_p = new ProductBean();
//                    $t_p->setProduct_name("25沉头自攻钉");
//                    $t_p->setProduct_no("W02-01-3*25-00");
//                    $t_p->setProduct_count(4);
//                    $t_p->setProduct_unit("个");
//                    $t_p->setSeq(10);
//                    $wujinMap[$t_p->getProduct_no()] = $t_p;
//                } else {
//                    $t_p = $wujinMap['W02-01-3*25-00'];
//                    $t_p->setProduct_count($t_p->getProduct_count() + 4);
//                }
            }

            // 有拉篮 裤抽
//            if ($p->getCom_products() != null) {
//                foreach ($p->getCom_products() as $c_p) {
//                    if (!isset($wujinMap['W07-03-450-00'])) {
//                        $t_p = new ProductBean();
//                        $t_p->setProduct_name("450三节阻尼滑轨");
//                        $t_p->setProduct_no("W07-03-450-00");
//                        $t_p->setProduct_count(1);
//                        $t_p->setProduct_unit("付");
//                        $t_p->setSeq(120);
//                        $t_p->setProduct_price(formatMoney(130));
//                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
//                        $wujinMap[$t_p->getProduct_no()] = $t_p;
//                    } else {
//                        $t_p = $wujinMap['W07-03-450-00'];
//                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
//                    }
//                }
//            }

            if (!empty($p->getCom_products())) {
                foreach ($p->getCom_products() as $c_p) {
                    $c_p = change2ProductBean($c_p);
                    if (!empty($c_p->getCom_products())) {
                        foreach ($c_p->getCom_products() as $c_p_c){
                            if (is_starts_with($c_p_c->getProduct_no(), "L-") || is_starts_with($c_p_c->getProduct_no(), "LB-")) {
                                if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                    $t_p = new ProductBean();
                                    $t_p->setProduct_name("450三节阻尼滑轨");
                                    $t_p->setProduct_no("W07-03-450-00");
                                    $t_p->setProduct_count(1);
                                    $t_p->setProduct_unit("付");
                                    $t_p->setSeq(120);
                                    $t_p->setProduct_price(formatMoney(130));
                                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                                } else {
                                    $t_p = $wujinMap['W07-03-450-00'];
                                    $t_p = change2ProductBean($t_p);
                                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                }
                            } elseif (is_starts_with($c_p_c->getProduct_no(), "K-")
                            ) {
                                if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                    $t_p = new ProductBean();
                                    $t_p->setProduct_name("450三节阻尼滑轨");
                                    $t_p->setProduct_no("W07-03-450-00");
                                    $t_p->setProduct_count(1);
                                    $t_p->setProduct_unit("付");
                                    $t_p->setSeq(120);
                                    $t_p->setProduct_price(formatMoney(130));
                                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                                } else {
                                    $t_p = $wujinMap['W07-03-450-00'];
                                    $t_p = change2ProductBean($t_p);
                                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                }
                            }
                        }

                    }
                        if (is_starts_with($c_p->getProduct_no(), "L-") || is_starts_with($c_p->getProduct_no(), "LB-")) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    } elseif (is_starts_with($c_p->getProduct_no(), "K-")
                    ) {
                        if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("450三节阻尼滑轨");
                            $t_p->setProduct_no("W07-03-450-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("付");
                            $t_p->setSeq(120);
                            $t_p->setProduct_price(formatMoney(130));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W07-03-450-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    }

                    //处理组合内的拉篮和裤抽 Near
                    if ($c_p->getProduct_type() == 'ZH') {
                        if (!empty($c_p->getCom_products())) {
                            foreach ($c_p->getCom_products() as $c_p_c) {
                                $c_p_c = change2ProductBean($c_p_c);
                                if (is_starts_with($c_p_c->getProduct_no(), "L-")
                                    || is_starts_with($c_p_c->getProduct_no(), "LB-")
                                ) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                } elseif (is_starts_with($c_p_c->getProduct_no(), "K-")) {
                                    if (!isset($wujinMap['W07-03-450-00']) || empty($wujinMap['W07-03-450-00'])) {
                                        $t_p = new ProductBean();
                                        $t_p->setProduct_name("450三节阻尼滑轨");
                                        $t_p->setProduct_no("W07-03-450-00");
                                        $t_p->setProduct_count(1);
                                        $t_p->setProduct_unit("付");
                                        $t_p->setSeq(120);
                                        $t_p->setProduct_price(formatMoney(130));
                                        $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                                        $wujinMap[$t_p->getProduct_no()] = $t_p;
                                    } else {
                                        $t_p = $wujinMap['W07-03-450-00'];
                                        $t_p = change2ProductBean($t_p);
                                        $t_p->setProduct_count($t_p->getProduct_count() + 1);
                                    }
                                }
                            }
                        }
                    }

                    //处理中层板YTG
                    if ($c_p->getProduct_type() == 'YTG'){
                        $p_no = 'W06-01-'.$c_p->getProduct_width().'-00';
                        if (!isset($wujinMap[$p_no])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通杆");
                            $t_p->setProduct_no($p_no);
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("支");
                            $t_p->setProduct_width($c_p->getProduct_width());
                            $t_p->setSeq(1121);
                            $t_p->setProduct_price(formatMoney(
                                100 * ((getLength($c_p->getProduct_width())) / 1000)
                            ));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap[$p_no];
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                        if (!isset($wujinMap['W04-07-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name("衣通托五金包");
                            $t_p->setProduct_no("W04-07-XX-00");
                            $t_p->setProduct_count(1);
                            $t_p->setProduct_unit("套");
                            $t_p->setSeq(1200);
                            $t_p->setProduct_price(formatMoney(14));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W04-07-XX-00'];
                            $t_p->setProduct_count($t_p->getProduct_count() + 1);
                        }
                    }

                    //处理横隔板MH
                    if ($c_p->getProduct_type() == 'MH') {
                        if (!isset($wujinMap['W01-01-XX-00'])) {
                            $t_p = new ProductBean();
                            $t_p->setProduct_name('层板连接件');
                            $t_p->setProduct_no('W01-01-XX-00');
                            $t_p->setProduct_count(4);
                            $t_p->setProduct_unit('套');
                            $t_p->setSeq(1);
                            $t_p->setProduct_price(formatMoney(6));
                            $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                            $wujinMap[$t_p->getProduct_no()] = $t_p;
                        } else {
                            $t_p = $wujinMap['W01-01-XX-00'];
                            $t_p = change2ProductBean($t_p);
                            $t_p->setProduct_count($t_p->getProduct_count() + 4);
                        }
                    }

                }
            }
        }
        /*
         * 门五金
         */
        foreach ($scheme->getScheme_door_schemes() as $d_s) {
            $d_s = change2SchemeBean($d_s);
            if (!isset($wujinMap['W05-01-XX-00'])) {
                $t_p = new ProductBean();
                $t_p->setProduct_name('门铰链');
                $t_p->setProduct_no('W05-01-XX-00');
                $t_p->setProduct_unit("套");
                $t_p->setSeq(113);
                $t_p->setProduct_count($scheme->getScheme_height() > 2019 ? 5 : 4);
                $t_p->setProduct_price(formatMoney(12));
                $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                $wujinMap[$t_p->getProduct_no()] = $t_p;
            } else {
                $t_p = $wujinMap['W05-01-XX-00'];
                $t_p->setProduct_count($t_p->getProduct_count() + ($scheme->getScheme_height() > 2019 ? 5 : 4));
            }

            if (strpos($d_s->getScheme_name(), '简欧') > 0){
                if (!isset($wujinMap['W08-01-180-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('欧式拉手');
                    $t_p->setProduct_no('W08-01-180-00');
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(111);
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_price(formatMoney(30));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W08-01-180-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            }else{
                if (!isset($wujinMap['W08-02-160-00'])) {
                    $t_p = new ProductBean();
                    $t_p->setProduct_name('160孔距拉手');
                    $t_p->setProduct_no('W08-02-160-00');
                    $t_p->setProduct_unit("个");
                    $t_p->setSeq(109);
                    $t_p->setProduct_count(1);
                    $t_p->setProduct_price(formatMoney(37));
                    $t_p->setProduct_dis_price(formatMoney($t_p->getProduct_price() * $scheme->getScheme_discount()));
                    $wujinMap[$t_p->getProduct_no()] = $t_p;
                } else {
                    $t_p = $wujinMap['W08-02-160-00'];
                    $t_p->setProduct_count($t_p->getProduct_count() + 1);
                }
            }
        }

        $totalPrice = $dis_totalPrice = 0;

        foreach ($wujinMap as $w_p) {
            $w_p->setProduct_total_price(formatMoney($w_p->getProduct_price() * $w_p->getProduct_count()));
            $w_p->setProduct_dis_total_price(formatMoney($w_p->getProduct_dis_price() * $w_p->getProduct_count()));
            $totalPrice = $totalPrice + $w_p->getProduct_total_price();
            $dis_totalPrice = $dis_totalPrice + $w_p->getProduct_dis_total_price();
        }

        $scheme->setScheme_wujin_price(formatMoney($totalPrice));
        $scheme->setScheme_dis_wujin_price(formatMoney($dis_totalPrice));
        $scheme->setScheme_score_wujin_products($wujinMap);
        $scheme->setScheme_gt_price(formatMoney(
            $scheme->getScheme_plate_price() + $scheme->getScheme_wujin_price() + $scheme->getScheme_zj_price()
        ));
        $scheme->setScheme_dis_gt_price(formatMoney(
            formatMoney($scheme->getScheme_gt_price() * $scheme->getScheme_discount())
        ));

        $scheme->setScheme_price(formatMoney($scheme->getScheme_gt_price()));
        $scheme->setScheme_dis_price(formatMoney($scheme->getScheme_dis_gt_price()));

        $scheme->setScheme_show_discount(formatMoney(strval(10 * $scheme->getScheme_discount())));
    }

    /**
     * @author: Rudy
     * @time: 2017年10月24日
     * description:生成桌子product
     * @param SchemeBean $col
     * @param $price
     * @param $dis
     * @return ProductBean
     */
    private function buildTableProduct(SchemeBean $col, $price, $dis)
    {
        $d_product = new ProductBean();
        $d_product->setSeq_type(600);
        $d_product->setProduct_name($col->getScheme_name());
        $d_product->setProduct_color_name($col->getScheme_color_name());
        $d_product->setProduct_color_no($col->getScheme_color_no());
        $d_product->setProduct_width($col->getScheme_width());
        $d_product->setProduct_no($col->getScheme_no());
        $d_product->setProduct_count(1);
        $d_product->setProduct_price($price);
        $d_product->setProduct_dis_price(formatMoney($d_product->getProduct_price() * $dis));
        $d_product->setProduct_total_price(formatMoney($d_product->getProduct_price() * $d_product->getProduct_count()));
        $d_product->setProduct_dis_total_price(formatMoney($d_product->getProduct_dis_price() * $d_product->getProduct_count()));

        return $d_product;
    }
}