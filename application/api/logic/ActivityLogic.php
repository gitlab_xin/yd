<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/29
 * Time: 11:28
 */

namespace app\api\logic;

use app\common\model\Activity as ActivityModel;
use app\common\model\ActivityApply as ApplyModel;
use app\common\model\ActivityApply;

class ActivityLogic
{
    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:获取活动列表
     * @param $pageIndex
     * @param $pageSize
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getList($pageIndex, $pageSize)
    {
        return ActivityModel::build()->alias('a')
            ->join("(SELECT count(activity_id) as count,activity_id FROM yd_activity_apply WHERE status ='yes' GROUP BY activity_id) as `apply`",
                'apply.activity_id = a.activity_id', 'LEFT')
            ->where(['a.is_deleted' => 0])
            ->field(['a.activity_id', 'a.title', 'a.bg_src', 'IF(ISNULL(apply.count),0,apply.count) as count'])
            ->page($pageIndex, $pageSize)
            ->select();
    }

    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:获取活动详情
     * @param int $activity_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getInfo($activity_id)
    {
        $result = ActivityModel::build()->alias('a')
            ->join("(SELECT count(activity_id) as count,activity_id FROM yd_activity_apply WHERE status ='yes' GROUP BY activity_id) as `apply`",
                'apply.activity_id = a.activity_id', 'LEFT')
            ->where(['a.activity_id' => $activity_id, 'a.is_deleted' => 0])
            ->field([
                'a.activity_id',
                'a.title',
                'a.bg_src',
                'a.content',
                'IF(ISNULL(apply.count),0,apply.count) as count',
                'a.end_time',
                'a.activity_begin_time',
                'a.activity_end_time',
            ])
            ->find();
        if (empty($result)) {
            return false;
        }
        $result['is_end'] = ($result['end_time'] <= time()) ? 'yes' : 'no';
        return $result;
    }

    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:检查报名活动是否合法
     * @param $user_id
     * @param $activity_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public static function applyCheck($user_id, $activity_id)
    {
        $bool = ActivityModel::build()
            ->where([
                'activity_id' => $activity_id,
                'is_deleted' => 0
            ])
            ->field([
                'activity_id',
                'end_time'
            ])->find();
        if (empty($bool)) {
            return -1;//活动不存在
        }
        if ($bool['end_time'] <= time()) {
            return -2;//活动已结束
        }

        $result =  ApplyModel::build()->where(['user_id' => $user_id, 'activity_id' => $activity_id,'status' => ['in',['yes','wait']]])->count();
        if($result >0){
            return -3;//您已报名
        }
    }

    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:写入报名数据
     * @param $requestData
     * @return int|string
     */
    public static function apply($requestData)
    {
        return ApplyModel::build()->insert($requestData);
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取活动数量
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getCount()
    {
        return ActivityModel::whereCount();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的活动
     * @param $data
     * @return array
     */
    public static function getMyList($data)
    {
        //先获取用户已经审核通过后的活动ids
        $result = ActivityApply::build()->field(['activity_id'])->where(['user_id'=>$data['user_id'],'status'=>'yes'])
            ->group('activity_id')
            ->select()
            ->toArray();
        if($result){
            $where = ['a.is_deleted' => 0,'a.activity_id'=>['in',array_column($result,'activity_id')]];
            switch ($data['type']){
                case 'ongoing':
                    $where['a.end_time'] = ['gt',time()];
                    break;
                case 'end':
                    $where['a.end_time'] = ['lt',time()];
                    break;
                default:
                    break;
            }
            $result = ActivityModel::build()->alias('a')
                ->join("(SELECT count(activity_id) as count,activity_id FROM yd_activity_apply WHERE status ='yes' GROUP BY activity_id) as `apply`",
                    'apply.activity_id = a.activity_id', 'LEFT')
                ->where($where)
                ->field(['a.activity_id', 'a.title', 'a.bg_src', 'IF(ISNULL(apply.count),0,apply.count) as count'])
                ->page($data['pageIndex'], $data['pageSize'])
                ->select()
                ->toArray();
        }
        return $result;
    }

    public static function getMyListCount($data)
    {
        $result = ActivityApply::build()->field(['activity_id'])->where(['user_id'=>$data['user_id'],'status'=>'yes'])
            ->group('activity_id')
            ->select()
            ->toArray();
        if($result){
            $where = ['a.is_deleted' => 0,'a.activity_id'=>['in',array_column($result,'activity_id')]];
            switch ($data['type']){
                case 'ongoing':
                    $where['a.end_time'] = ['gt',time()];
                    break;
                case 'end':
                    $where['a.end_time'] = ['lt',time()];
                    break;
                default:
                    break;
            }
            $result = ActivityModel::build()->alias('a')
                ->where($where)
                ->count();
        }else{
            $result = 0;
        }
        return $result;
    }
    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的活动
     * @param $data
     * @return array
     */
    public static function getMyAppInfo($data)
    {
        return ActivityApply::build()->field(['id','status'],true)->where($data)->where(['status'=>'yes'])->find();
    }
}