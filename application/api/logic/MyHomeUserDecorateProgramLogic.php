<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\logic;

use app\common\model\MyHomeUserDecorateProgram;
use app\common\model\MyHomeHouseType;
use think\Db;

class MyHomeUserDecorateProgramLogic
{
    public static function checkHouseType($house_id)
    {
        return MyHomeHouseType::build()->where(['house_id' => $house_id, 'is_deleted' => 0])->find();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:写入装修方案模型
     * @param $requestData
     * @return int|string
     */
    public static function insertDecorateProgram($requestData)
    {
        $requestData['create_time'] = time();
        return MyHomeUserDecorateProgram::build()->insert($requestData);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:编辑装修方案模型
     * @param $program_id
     * @param $requestData
     * @return int|string
     */
    public static function saveDecorateProgram($program_id,$requestData)
    {
        $requestData['update_time'] = time();
        return MyHomeUserDecorateProgram::build()->update($requestData,['program_id'=>$program_id]);
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案模型
     * @param $program_id
     * @return int|string
     */
    public static function getDecorateProgram($program_id)
    {
        return MyHomeUserDecorateProgram::build()->where(['program_id' => $program_id, 'is_deleted' => 0])
            ->field(['program_id', 'name', 'img_src', 'serial_array', 'create_time', 'house_id'])->find();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取我的装修方案列表
     * @param int $user_id
     * @param int $page_index
     * @param int $page_size
     * @return int|string
     */
    public static function getMyProgram($user_id,$page_index,$page_size)
    {
        return MyHomeUserDecorateProgram::build()->where(['user_id' => $user_id, 'is_deleted' => 0])
            ->field(['program_id', 'name', 'img_src', 'create_time', 'house_id'])->page($page_index,$page_size)->select();
    }
}
