<?php
namespace app\api\logic;


use app\common\model\CustomizedColor as ColorModel;
use app\common\model\CustomizedScheme as SchemeModel;
use app\common\model\MyDecorateProduct;
use app\common\model\MyDecorateProductCart;
use app\common\model\MyDecorateProductCollect;
use app\common\model\MyDecorateProductStandard;
use app\common\model\ShoppingCart;
use app\common\model\ShopProduct;
use app\common\model\UserToken;

class MyDecorateProductCartLogic
{


    public static function add($requestData){

        $insertAll = [];
        foreach ($requestData['cart_list'] as $k => $v){
            $where = ['user_id' => $requestData['user_id'],'decorate_product_id' => $v['product_id']];

            if (!empty($v['color_id'])){
                $where['color_id'] = $v['color_id'];
            }

            if (!empty($v['standard_id'])){
                $where['standard_id'] = $v['standard_id'];
            }

            if (!empty($requestData['color_id'])){
                //查询是否有对应花色规格
                $is_replace = MyDecorateProductStandard::build()
                    ->alias('s')
                    ->join('yd_customized_color c','c.color_id = s.color_id')
                    ->where(['s.decorate_product_id' => $v['product_id'], 's.color_id' => $requestData['color_id'],'c.is_deleted' => 0,'c.is_show' => 1])
                    ->count();

                if ($is_replace > 0){
                    $v['color_id'] = $requestData['color_id'];
                }
            }

            $count = ShoppingCart::build()
                ->where($where)
                ->value('count');

            if ($count >= 1){
                ShoppingCart::build()
                    ->where($where)
                    ->update([
                        'count' => $count + 1,
                        'update_time' => time()
                    ]);
            }else{
                //$v['product_id']是易家整配id、product_id是易家整配添加的产品id
                ShoppingCart::build()
                    ->insert([
                        'decorate_product_id' => $v['product_id'],
                        'decorate_id' => $requestData['program_id'],
                        'product_id' =>  MyDecorateProduct::build()->where(['id' => $v['product_id']])->value('product_id'),
                        'color_id' => $v['color_id'],
                        'standard_id' => $v['standard_id'],
                        'user_id' => $requestData['user_id'],
                        'type' => $v['type'],
                        'from' => '2',
                        'create_time' => time(),
                    ]);
            }

        }

        return true;
    }

    public static function info($requestData){
        $where = ['c.user_id' => $requestData['user_id'], 'p.is_delete' => '0', 'c.from' => '2'];

        if (!empty($requestData['program_id'])){
            $where['c.decorate_id'] = $requestData['program_id'];
        }

        $info['total'] = ShoppingCart::build()
            ->alias('c')
            ->join('yd_my_decorate_product p', 'p.id = c.decorate_product_id')
            ->where($where)
            ->count();
        //定制
        $info['total_1'] = ShoppingCart::build()
            ->alias('c')
            ->join('yd_my_decorate_product p', 'p.id = c.decorate_product_id')
            ->where($where)
            ->where(['c.type' => '1'])
            ->count();
        //装配
        $info['total_2'] = ShoppingCart::build()
            ->alias('c')
            ->join('yd_my_decorate_product p', 'p.id = c.decorate_product_id')
            ->where($where)
            ->where(['c.type' => '2'])
            ->count();
        //非定制
        $info['total_3'] = ShoppingCart::build()
            ->alias('c')
            ->join('yd_my_decorate_product p', 'p.id = c.decorate_product_id')
            ->where($where)
            ->where(['c.type' => '3'])
            ->count();

        return $info;
    }

    public static function lists($requestData){
        $where = ['c.user_id' => $requestData['user_id'], 'p.is_delete' => '0', 'c.from' => '2'];

        if (!empty($requestData['program_id'])){
            $where['c.decorate_id'] = $requestData['program_id'];
        }

        if (in_array($requestData['type'], ['1', '2', '3'])){
            $where['c.type'] = $requestData['type'];
        }

        return ShoppingCart::build()
            ->alias('c')
            ->join('yd_my_decorate_product p', 'p.id = c.decorate_product_id')
            ->join('yd_my_decorate_product_standard s',['s.color_id = c.color_id','s.decorate_product_id = c.decorate_product_id'],'left')
            ->join('yd_customized_scheme p_s','p_s.id = p.product_id', 'left')
            ->join('yd_shop_product_standard s_p_s','s_p_s.standard_id = c.standard_id','left')
            ->where($where)
            ->field([
                'c.cart_id id','p.type','IFNULL(s.color_id, c.color_id) color_name','IFNULL(s_p_s.price, p.discount_price) discount_price',
                'c.count','IFNULL(s.product_img, IFNULL(s_p_s.img, p.image)) image', 'p.name', 'c.decorate_product_id',
                'p.product_id','p_s.scheme_type','p_s.scheme_b_type','s_p_s.standard_id','s_p_s.name standard_name'
            ])
            ->order(['c.cart_id' => "desc"])
            ->select();

    }

    public static function allLists($requestData){
        $where = ['c.user_id' => $requestData['user_id']];

        return ShoppingCart::build()
            ->alias('c')
            ->where($where)
            ->field([
                'c.cart_id id', 'c.from', 'c.count', 'c.product_id','c.product_id cart_product_info', 'c.type',
                'c.color_id', 'c.standard_id', 'c.decorate_product_id'
            ])
            ->order(['c.cart_id' => "desc"])
            ->select();

    }

    public static function delete($requestData){
        return ShoppingCart::build()
            ->where(['user_id' => $requestData['user_id'], 'cart_id' =>['in', $requestData['id']]])
            ->delete();
    }

    public static function update($requestData){
        $updateData = ['update_time' => time()];

        if (!empty($requestData['color_id'])) {
            $updateData['color_id'] = $requestData['color_id'];
        }

        if (!empty($requestData['count'])) {
            $updateData['count'] = $requestData['count'];
        }

        if (!empty($requestData['standard_id'])) {
            $updateData['standard_id'] = $requestData['standard_id'];
        }

        return ShoppingCart::build()
            ->where(['cart_id' => $requestData['id'], 'user_id' => $requestData['user_id']])
            ->update($updateData);
    }

    public static function read($requestData){
        $data['is_collect'] = 0;
        $data['cart_count'] = 0;
        $data['info'] = MyDecorateProduct::build()
            ->alias('p')
            ->join('yd_shop_product s_p','s_p.product_id = p.product_id', 'left')
            ->join('yd_shop_product_standard s_p_s','s_p_s.standard_id = p.standard_id','left')
            ->where(['p.id' => $requestData['decorate_product_id']])
            ->field([
                'p.id', 'p.product_id', 'p.type', 'p.decorate_id', 'IFNULL(s_p.name ,p.name) name', 'p.code', 'p.room_id room_name',
                'p.original_price', 'p.discount_price', 'IFNULL(s_p.content, p.intro) intro',
                'p.image', 'p.views', 'p.sales', 'p.type','p.standard_id','p.color_id','s_p_s.name standard_name'
            ])
            ->find();
        if (!empty($data['info']['code'])) {
            $data['detail'][] = ['key' => '编码', 'value' => $data['info']['code']];
        }
        if (in_array($data['info']['type'], ['1', '2'])){
            $data['color_info'] =  MyDecorateProductStandard::build()
                ->alias('s')
                ->join('yd_customized_color c', 'c.color_id = s.color_id')
                ->where(['c.is_deleted' => '0', 's.decorate_product_id' => $data['info']['id']])
                ->field(['s.color_id','s.product_img','c.origin_img_src','c.color_name'])
                ->order(['c.is_default' => 'ASC','c.color_no' => 'ASC'])
                ->select();

            $info = $info = SchemeModel::build()
                ->alias('p')
                ->join('yd_customized_color c','c.color_no = p.scheme_color_no')
                ->field([
                    "CONCAT_WS('*',p.scheme_width,p.scheme_height,p.scheme_deep) as 'standard_name'",'c.color_name',
                    'p.scheme_name', 'p.scheme_type','c.color_id','p.scheme_b_type'
                ])
                ->where(['p.id' => $data['info']['product_id'], 'c.is_deleted' => '0', 'c.is_show' => '1'])
                ->find();

            $data['info']['color_name'] = ($data['info']['type'] == '1') ? ColorModel::build()->where(['color_id' => $data['info']['color_id']])->value('color_name') : $info['color_name'];
//            $data['info']['color_id'] = $info['color_id'];
            $data['info']['scheme_type'] = $info['scheme_type'];
            $data['info']['scheme_b_type'] = $info['scheme_b_type'];
            $data['detail'][] = ['key' => '名称', 'value' => $data['info']['name']];
            $data['detail'][] = ['key' => '空间', 'value' => $data['info']['room_name']];
            $data['detail'][] = ['key' => '尺寸', 'value' => $info['standard_name']];

        } else{
            $info= ShopProduct::build()
                ->where(['product_id' => $data['info']['product_id']])
                ->field('name ,product_param')
                ->find();

            $info['product_param'] = (empty($info['product_param'])) ? [] : unserialize($info['product_param']);
            $data['detail'][] = ['key' => '商品名称', 'value' => $info['name']];

            foreach ($info['product_param'] as $k => $v){
                if (empty($v)){
                    continue;
                }
                $temp = [
                    'key' => $v['key'],
                    'value' => $v['value'],
                ];
                $data['detail'][] = $temp;
            }

        }

        $token = get_token();
        if (!empty($token)) {
            $userToken = new UserToken();
            $userToken = $userToken->getTokenInfo($token);

            $data['cart_count'] = ShoppingCart::build()
                ->alias('c')
                ->join('yd_my_decorate_product p','p.id = c.decorate_product_id')
                ->where(['c.user_id' => $userToken['user_id'], 'c.decorate_id' => $data['info']['decorate_id'], 'c.from' => '2'])
                ->count();

            $data['is_collect'] = MyDecorateProductCollect::build()
                ->where(['user_id' => $userToken['user_id'], 'decorate_product_id' => $data['info']['id'], 'type' => '1'])
                ->count();
        }

        MyDecorateProduct::build()
            ->where(['id' => $requestData['decorate_product_id']])
            ->setInc('views');

        return $data;
    }

    public static function collect($requestData){
        $count =  MyDecorateProductCollect::build()
            ->where(['user_id' => $requestData['user_id'], 'decorate_product_id' => $requestData['decorate_product_id'], 'type' => $requestData['type']])
            ->count();
        $data = [
            'user_id' => $requestData['user_id'],
            'decorate_product_id' => $requestData['decorate_product_id'],
            'type' => $requestData['type']
        ];

        return ($count >= 1) ? MyDecorateProductCollect::build()->where($data)->delete() : MyDecorateProductCollect::build()->insert($data);
    }

}