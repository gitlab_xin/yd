<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 18-1-27
 * Time: 下午4:51
 */

namespace app\api\logic;


use app\common\model\CustomizedScheme;
use app\common\model\Scheme;
use app\common\model\Works;
use app\common\tools\CommonMethod;
use app\common\model\SchemeRQSBean as SchemeBean;
use think\Db;
use think\Exception;

class SchemeYMJLogic extends SchemeRQSLogic
{
//    public $cross = 490;
//    public $index_map = [];

    /**
     * 保存房间
     * @param $data
     * @param $user_id
     * @return int
     */
    public function saveRoom($data, $user_id)
    {
        $schemeModelData = [
            'scheme_name' => '衣帽间',
            'user_id' => $user_id,
            'scheme_type' => 'YMJ',
            'scheme_type_text' => '衣帽间',
            'scheme_width' => $data['size']['x'],
            'scheme_height' => $data['size']['y'],
            'scheme_deep' => $data['size']['z'],
            'serial_array' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
            'barriers_array' => ''
        ];
        Db::startTrans();
        try {
            $model = CustomizedScheme::create($schemeModelData);
//            foreach ($data['children'] as $key => $wall) {
////                $wall['width']//墙宽度 $wall['height']//墙高度
//                if (!empty($wall['type']) && $wall['type'] == 'mesh') {
//                    foreach ($wall['cabinetSpace'] as $index => $cabinet) {
//                        //算出柜体的左右边界，然后通过障碍物循环判断是否存在于这个柜体中
//                        $left = $cabinet['data_position']['x'] - $cabinet['data_size']['width'] / 2;
//                        $right = $cabinet['data_position']['x'] + $cabinet['data_size']['width'] / 2;
//                        $blocks = [];
//                        $leftDis = abs(-$wall['data_size']['width']/2-$left);
//                        $rightDis = abs($wall['data_size']['width']/2 - $right);
//                        foreach ($wall['barriers'] as $block) {
////                            dump($block);
//                            $block_left = $block['data_position']['x'] - $block['data_size']['width'] / 2;
//                            $block_right = $block['data_position']['x'] + $block['data_size']['width'] / 2;
//                            if ($block_left > $left && $block_right < $right) {
//                                /*
//                                 * 因为障碍物需要和柜体混合进行计算
//                                 * 如果是属于这个柜体的话，则存放进对应的数组，方便后继的计算
//                                 */
//                                $blocks[] = ['name' => $block['name'], 'm_left_mm' => abs($left - $block_left),
//                                    'm_top_mm' => ($cabinet['data_position']['y'] + $cabinet['data_size']['height'] / 2) - ($block['data_position']['y'] + $block['data_size']['height'] / 2),
//                                    'width' => $block['data_size']['width'], 'height' => $block['data_size']['height'], 'deep' => $block['data_size']['deep'],
//                                    'right_limit' => abs($left - $block_left) + $block['data_size']['width']];//right_limit 障碍物右侧偏移量(以左侧为起始点)
//                            }
//                        }
//                        $affect = $wall['affectSpace'];
//                        $data = [
//                            'scheme_name' => '衣帽间-入墙式移门壁柜-' . chr(ord('A') + $key) . $index,
//                            'user_id' => $user_id,
//                            'parent_id' => $model['id'],
//                            'scheme_type' => 'RQS',
//                            'scheme_type_text' => '入墙式移门壁柜',
//                            'scheme_s_type' => -1,
//                            'scheme_width' => $cabinet['data_size']['width'],
//                            'scheme_height' => $cabinet['data_size']['height'],
//                            'scheme_deep' => $cabinet['data_size']['deep'],
//                            'scheme_color_no' => '000',
//                            'scheme_hole_width' => $cabinet['data_size']['width'],
//                            'scheme_hole_height' => $cabinet['data_size']['height'],
//                            'scheme_hole_sl_height' => $cabinet['data_size']['height'],
//                            'scheme_hole_deep' => $cabinet['data_size']['deep'],
//                            'serial_array' => '',
//                            'barriers_array' => json_encode(['blocks'=>$blocks,'affect'=>$affect,'cabinet_name'=>$cabinet['cabinet_name']]),
//                            'statement_array' => "",
//                            'scheme_hole_left' => $leftDis,
//                            'scheme_hole_right' => $rightDis
//                        ];
//                        //leftDis和rightDis代表柜子距离当前对应的墙的左边距离和右边距离
//                        if ($leftDis == 0) {
//                            $data['is_left_wall'] = 1;
//                            $data['is_left_wall_zt'] = 1;
//                        }
//                        if ($rightDis == 0) {
//                            $data['is_right_wall'] = 1;
//                            $data['is_right_wall_zt'] = 1;
//                        }
//                        CustomizedScheme::create($data);
//                    }
//                }
//            }
            Db::commit();
            return $model['id'];
        } catch (\Exception $e) {
            Db::rollback();
            return 0;
        }

    }

    public function createRQSScheme($data, $room_id, $user_id)
    {
//                $wall['width']//墙宽度 $wall['height']//墙高度
        //算出柜体的左右边界，然后通过障碍物循环判断是否存在于这个柜体中
        $left = $data['data_position']['x'] - $data['data_size']['width'] / 2;
        $right = $data['data_position']['x'] + $data['data_size']['width'] / 2;
        $blocks = [];
        $leftDis = abs(-$data['data_size']['width'] / 2 - $left);
        $rightDis = abs($data['data_size']['width'] / 2 - $right);
        foreach ($data['occlusion'] as $block) {
            $block_left = $block['data_position']['x'] - $block['data_size']['width'] / 2;
            $block_right = $block['data_position']['x'] + $block['data_size']['width'] / 2;
            if ($block_left > $left && $block_right < $right) {
                /*
                 * 因为障碍物需要和柜体混合进行计算
                 * 如果是属于这个柜体的话，则存放进对应的数组，方便后继的计算
                 */
                $blocks[] = ['name' => $block['name'], 'm_left_mm' => abs($left - $block_left),
                    'm_top_mm' => ($data['data_position']['y'] + $data['data_size']['height'] / 2) - ($block['data_position']['y'] + $block['data_size']['height'] / 2),
                    'width' => $block['data_size']['width'], 'height' => $block['data_size']['height'], 'deep' => empty($block['data_size']['deep'])?0:$block['data_size']['deep'],
                    'right_limit' => abs($left - $block_left) + $block['data_size']['width']];//right_limit 障碍物右侧偏移量(以左侧为起始点)
            }
        }
        $data = [
            'scheme_name' => '衣帽间-入墙式移门壁柜-' . chr(ord('A')).'-'.$data['key'],
            'user_id' => $user_id,
            'parent_id' => $room_id,
            'scheme_type' => 'RQS',
            'scheme_type_text' => '入墙式移门壁柜',
            'scheme_s_type' => -1,
            'scheme_width' => $data['data_size']['width'],
            'scheme_height' => $data['data_size']['height'],
            'scheme_deep' => $data['data_size']['deep'],
            'scheme_color_no' => '000',
            'scheme_hole_width' => $data['data_size']['width'],
            'scheme_hole_height' => $data['data_size']['height'] + 32 + 17,
            'scheme_hole_sl_height' => $data['data_size']['height'] + 32 + 17,
            'scheme_hole_deep' => $data['data_size']['deep'],
            'serial_array' => '',
            'barriers_array' => json_encode(['blocks' => $blocks, 'cabinet_name' => $data['name']]),
            'statement_array' => "",
            'scheme_hole_left' => $leftDis,
            'scheme_hole_right' => $rightDis,
            'ymj_def' => '1',
            'ymj_key' => $data['key'],
            'create_time' =>time()
        ];
        //leftDis和rightDis代表柜子距离当前对应的墙的左边距离和右边距离
        if ($leftDis == 0) {
            $data['is_left_wall'] = 1;
            $data['is_left_wall_zt'] = 1;
        }
        if ($rightDis == 0) {
            $data['is_right_wall'] = 1;
            $data['is_right_wall_zt'] = 1;
        }
        Db::startTrans();
        CustomizedScheme::build()->where(['parent_id'=>$room_id,'ymj_key'=>$data['ymj_key']])->update(['ymj_def'=>0]);

        $scheme_id = CustomizedScheme::build()->insertGetId($data);

        $scheme = $this->create($scheme_id,$user_id);
        Db::commit();
        return ['scheme' => $scheme,'scheme_id' => $scheme_id];
    }

    /**
     * 编辑房间
     * @param $data
     * @param $user_id
     * @param $scheme_id
     * @return int
     */
    public function updateRoom($data, $user_id, $scheme_id)
    {
        $schemeModelData = [
            'scheme_width' => $data['size']['x'],
            'scheme_height' => $data['size']['y'],
            'scheme_deep' => $data['size']['z'],
            'serial_array' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'statement_array' => '',
            'barriers_array' => ''
        ];
        Db::startTrans();
        try {
            $model = CustomizedScheme::update($schemeModelData, ['id' => $scheme_id, 'scheme_type' => 'YMJ', 'user_id' => $user_id]);
            Db::commit();
            return $model;
        } catch (\Exception $e) {
            Db::rollback();
            return 0;
        }

    }

    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:获取推荐方案
     * @param $data
     * @return array|bool
     */
    public static function getRecommend($data)
    {
        try {
            $where['work.type'] = 1;
            $where['detail.scheme_type'] = "RQS";
            $order = ["order_key" => "DESC"];

//            $where['detail.scheme_hole_sl_height'] = ['between', [$data['sl_height'] - 96, $data['hole_height']]];
            $where['detail.scheme_width'] = $data['width'];//宽度
            $where['detail.scheme_height'] = $data['height'];//高度

            $result = Works::build()->alias("work")->join("yd_works_scheme_detail detail", "work.scheme_id = detail.scheme_detail_id")
                ->where($where)->field(['work.works_id', 'scheme_pic', '(work.view_count*0.2+work.praise_count*0.2+work.collect_count*0.3 + work.comment_count*0.3) as order_key'])->order($order)->page(1, 10)->select();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Airon
     * @time:   2019年12月
     * description 我的定制
     * @param $room_id
     * @param $key
     * @param $user_id
     * @return bool|false|\PDOStatement|string|\think\Collection
     */
    public static function getCustomize($room_id,$key,$user_id)
    {
        try {
            $where['parent_id'] = $room_id;
            $where['ymj_key'] = $key;
            $where['user_id'] = $user_id;
            $where['serial_array'] =['neq',''];
            $order = ["ymj_def" => "DESC",'create_time'=>'DESC'];

//            $where['detail.scheme_hole_sl_height'] = ['between', [$data['sl_height'] - 96, $data['hole_height']]];

            $result = CustomizedScheme::build()
                ->where($where)->field(['id as scheme_id', 'scheme_pic','ymj_key','ymj_def'])->order($order)->page(1, 10)->select();
            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function setDefault($room_id,$scheme_id,$key,$user_id){
        $where['user_id'] = $user_id;
        $where['parent_id'] = $room_id;
        $where['ymj_key'] = $key;
        Db::startTrans();
        try{
            $result = CustomizedScheme::build()->where($where)->update(['ymj_def'=>0]);
            $where['id'] = $scheme_id;
            $result = CustomizedScheme::build()->where($where)->update(['ymj_def'=>1]);
            Db::commit();
            return $result;
        }catch (Exception $exception){
            Db::rollback();
            return false;
        }
    }
    /**
     * 获取衣帽间下所有的柜子
     * @param $scheme_id
     * @param $user_id
     * @return array
     */
    public function getCabinets($scheme_id, $user_id)
    {
        try {
            $result = CustomizedScheme::build()->field(['id', 'serial_array', 'barriers_array','ymj_key'])
                ->where(['parent_id' => $scheme_id, 'user_id' => $user_id,'ymj_def'=>1])->select()->toArray();
        } catch (\Exception $e) {
            return null;
        }

        foreach ($result as &$cabinet) {
            $cabinet['serial_array'] = json_decode($cabinet['serial_array']);
            $cabinet['barriers_array'] = json_decode($cabinet['barriers_array']);
            $cabinet['barriers_array'] = objToArray($cabinet['barriers_array']);
        }
        return $result;
    }

    /**
     * 获取衣帽间三维数据
     * @param $scheme_id
     * @param $user_id
     * @return array
     */
    public function getYMJ($scheme_id, $user_id)
    {
        try {
            $result = CustomizedScheme::build()->field(['id', 'serial_array'])
                ->where(['id' => $scheme_id, 'user_id' => $user_id])->find()->toArray();
        } catch (\Exception $e) {
            return null;
        }
        $result['serial_array'] = json_decode($result['serial_array']);

        return $result;
    }

    /**
     * 创建方案
     * @param $scheme_id
     * @param $user_id
     * @return array
     */
    public function create($scheme_id, $user_id)
    {
        try {
            $customizedScheme = CustomizedScheme::build()->where(['id' => $scheme_id, 'user_id' => $user_id])->find();
        } catch (\Exception $e) {
            return null;
        }
        if ($customizedScheme != null) {
            $YMJLogic = new self();
            $scheme = $YMJLogic->createQRSScheme1($customizedScheme);

            $productList = $scheme->getScheme_schemes();
            if (!empty($productList)) {
                array_walk($productList, function (&$row) {
                    $row = $row->mainInfo();
                });
            }

            return $productList;
        }
    }

    /**
     * @param $data
     * @return SchemeBean
     */
    public function createQRSScheme1($data)
    {
        $scheme = new SchemeBean();
        $scheme->setScheme_hole_width($data['scheme_hole_width']);
        $scheme->setScheme_hole_height($data['scheme_hole_height']);
        $scheme->setScheme_hole_sl_height($data['scheme_hole_sl_height']);
        $scheme->setScheme_type('RQS');
        $scheme->setScheme_hole_deep($data['scheme_deep']);
        $scheme->setScheme_hole_left($data['scheme_hole_left']);
        $scheme->setScheme_hole_right($data['scheme_hole_right']);
        $scheme->setScheme_door_count(0);
        $scheme->setScheme_color_no($data['scheme_color_no']);
        $scheme->setScheme_sk_color_no($data['scheme_sk_color_no']);
        $scheme->setScheme_width($data['scheme_width']);
        $scheme->setScheme_height($data['scheme_height']);
        $scheme->setScheme_hole_type($data['scheme_hole_type']);
        $scheme->setIs_left_wall($data['is_left_wall']);
        $scheme->setIs_right_wall($data['is_right_wall']);
        $scheme->setScheme_products(json_decode($data['barriers_array'], true));
        $Scheme_schemes = $this->generateSchemes($scheme);
        $scheme->setScheme_schemes($Scheme_schemes);
        return $scheme;
    }

    /**
     * 生成方案
     * @param SchemeBean $scheme
     * @return array
     */
    public function generateSchemes(SchemeBean $scheme)
    {
        $resultList = $this->getSchemesList($scheme);


        $height = $this->calFurnitureHeight($scheme->getScheme_hole_height());
        $colorNo = $scheme->getScheme_color_no();
        $wcbType = 0;

        $listFcws = [];
        foreach ($resultList as $item) {
            if (empty($item)) {
                continue;
            }
            // width：柜体真正宽度
            $ncbCount = count($item) - 3;
            $width = array_sum($item) + $ncbCount * INNER_WIDTH;

//            if (abs($width - $scheme->getScheme_hole_width()) > 32) {
//                // 舍弃方案 剩余位置超过32mm
//                continue;
//            } todo

            // 生成外侧板
            $wcbProducts = $this->calWcbProducts($item, $height, $width, $colorNo, $wcbType);
            // 计算顶板下落多少、是否下落
            list($dHeight, $twoHgb) = $this->calDingBanMTop($scheme->getScheme_hole_height(), $scheme->getScheme_hole_height());
            // 计算列方案和内侧板
            list($colSchemes, $ncbProducts) =
                $this->calSchemeColsAndNcbProducts($item, $height, $twoHgb, $dHeight, $colorNo, $scheme->getIs_optimize());

            // 初始化方案
            $fcw = new SchemeBean();
            $fcw->setScheme_color_name(CommonMethod::getSchemeColorName($colorNo));
            $fcw->setScheme_door_count(0);
            $fcw->setM_left_mm(0);
            $fcw->setM_top_mm(0);
            $fcw->setM_left(0);
            $fcw->setM_top(0);
            $fcw->setScheme_width($width);
            $fcw->setScheme_height($height);
            $fcw->setScheme_hole_width($scheme->getScheme_hole_width());
            $fcw->setScheme_hole_height($scheme->getScheme_hole_height());
            $fcw->setScheme_hole_deep($scheme->getScheme_hole_deep());
            $fcw->setScheme_hole_type($scheme->getScheme_hole_type());
            $fcw->setScheme_hole_left($scheme->getScheme_hole_left());
            $fcw->setScheme_hole_right($scheme->getScheme_hole_right());
            $fcw->setScheme_hole_sl_height($scheme->getScheme_hole_sl_height());
            $fcw->setScheme_wcb_type($wcbType);
            $fcw->setScheme_s_width($width);
            $fcw->setScheme_s_height($height);
            $fcw->setScheme_wcb_products($wcbProducts);
            $fcw->setScheme_ncb_products($ncbProducts);
            $fcw->setScheme_sk_color_no('-1');
            $fcw->setScheme_color_no($colorNo);
            $fcw->setScheme_type($scheme->getScheme_type());
            $fcw->setScheme_error_range(32);
            $fcw->setScheme_ncb_products($ncbProducts);
            $fcw->setScheme_schemes($colSchemes);
            $fcw->setScheme_door_count(0);

            // 方案图
//            $s_pic = $fcw->getScheme_width()
//                . "_" . $height
//                . "_" . $colorNo
//                . "_" . (count($item) - 2)
//                . "_" . $doorCount
//                . "_" . getRandomInt();
            $this->createSchemePic($fcw);
            $fcw->setScheme_name("入墙式移门壁柜-" . getRandomInt());
            // 单个方案生成完成
            $listFcws[] = $fcw;
        }
        return $listFcws;
    }

    /**
     * 获取所有列组合的可能性
     * @param SchemeBean $scheme
     * @return array
     */
    private function getSchemesList(SchemeBean $scheme)
    {
        $resultList = [];
        $hole_width = $scheme->getScheme_hole_width();
        $barriers_array = $scheme->getScheme_products();
        if (empty($barriers_array['blocks'])) {//如果没有障碍物
            $colCountList = $this->getColCountList($hole_width);
            foreach ($colCountList as $colCount) {
                $set = $this->getWidthSet($colCount, $hole_width);
//                if (!empty($set)) {
//                    $this->widthSetFilter($scheme, $set);
//                } todo::逻辑不明 影响生成方案
                $resultList[] = $set;
            }
        } else {//如果有障碍物
            //按照m_left_mm排序
            $blocks = $barriers_array['blocks'];
            array_multisort(array_column($blocks, 'm_left_mm'), SORT_ASC, $blocks);

            //合并障碍物
            $new_blocks = $this->gatherBlocks($blocks);

            /*
             * 计算一个柜体列的可能性
             */
            $list = [];//存放柜体排列的可能性数据
            $block_indexs = [];//记录哪一列是障碍物
            foreach ($new_blocks as $block) {
                $sum = array_sum($list);//计算前面一共占去了多少宽度
                if ($sum > $block['m_left_mm']) {//如果前方占据的宽度已经超过当前障碍物的最左边，代表会有一部分重叠，此时需要向前方的障碍物增加宽度
                    array_pop($list);//去掉数组最后一个障碍物
                    array_pop($block_indexs);
                    $block_width = $this->getClostetWidth($block['right_limit'] - array_sum($list)) + 25;//重新计算障碍物列
                } else {//如果没有超过，则代表没有重叠，这个时候需要判断上一列距离这个障碍物有多远，如果距离超过450（单个最小列空间+2个内侧板）则添加一正常列，否则增加障碍物列
                    $gap = $block['m_left_mm'] - $sum;//当前障碍物距离上一列有多少距离
                    if ($gap > 450) {//空间超过450了，新增正常列和计算障碍物列
                        $list[] = $gap;
                        $block_width = $this->getClostetWidth($block['width']) + 25;
                    } else {//如果没超过，则计算障碍物列
                        $block_width = $this->getClostetWidth($block['right_limit'] - $sum) + 25;
                    }
                }

                $block_indexs[] = array_push($list, $block_width) - 1;//往数组尾部插入障碍物，并记录障碍物的下标

            }
            //判断剩余空间，若剩余空间超过450，则添加正常列，否则合并障碍物
            $last_space = $hole_width - array_sum($list);
            if ($last_space > 450) {
                $list[] = $last_space;
            } else {
                array_pop($list);//去掉数组最后一个障碍物
                array_pop($block_indexs);
                $block_width = $this->getClostetWidth($hole_width - array_sum($list));//重新计算障碍物列
                $block_indexs[] = array_push($list, $block_width) - 1;//往数组尾部插入障碍物，并记录障碍物的下标
            }

            /*
             * 计算所有的可能性
             */
            $normal_cols = [];
            foreach ($list as $index => $value) {
                if (!in_array($index, $block_indexs)) {
                    $colCountList = $this->getColCountList($value);
                    if ($index == 0) {//第一个正常列在最左边，左边16，右边25
                        $side = [SIDE_WIDTH_1, SIDE_WIDTH_2];
                    } elseif ($index == count($list) - 1) {//最后一个正常列在最右边,左边25，右边16
                        $side = [SIDE_WIDTH_2, SIDE_WIDTH_1];
                    } else {//其它列时，左右都是25，这个25是内侧板
                        $side = [SIDE_WIDTH_2, SIDE_WIDTH_2];
                    }

                    foreach ($colCountList as $colCount) {
                        $set = $this->getWidthSet($colCount, $value, $side);
                        if ($set != null) {
                            if ($index == 0) {//第一个正常列在最左边，左边16，右边25
                                array_pop($set);
                            } elseif ($index == count($list) - 1) {//最后一个正常列在最右边,左边25，右边16
                                array_shift($set);
                            } else {//其它列时，左右都是25，这个25是内侧板
                                array_pop($set);
                                array_shift($set);
                            }
                            $normal_cols[$index][$colCount] = $set;
                        }
                    }
                } else {
                    $normal_cols[$index][0] = [$value - 25];
                }
            }
            //组合算法，对所有可能性进行在组合
            $this->getCombintions($normal_cols, 0, [], $resultList);
        }

        return $resultList;
    }

    /**
     *
     * 把一些障碍物合并
     * 如一些靠得比较近的障碍物，或者悬空的障碍物
     * 这里先把首个障碍物当作合并障碍物处理
     * @param $blocks
     * @return array
     */
    private function gatherBlocks($blocks)
    {
        $new_blocks[] = array_shift($blocks);

        for ($count = count($blocks), $i = 0; $i < $count; $i++) {
            $last_new_block_index = count($new_blocks) - 1;

            if (($blocks[$i]['m_left_mm'] < $new_blocks[$last_new_block_index]['right_limit'] && $blocks[$i]['right_limit'] > $new_blocks[$last_new_block_index]['right_limit'])
                ||
                ($blocks[$i]['m_left_mm'] <= $new_blocks[$last_new_block_index]['right_limit'] + 25 + 20 * 3)
            ) {
                /*
                 * 1.如果障碍物的最左边在合并障碍物的里，则代表这个障碍物是悬空的，且当前循环的障碍物右边的界限超过合并障碍物，则合并障碍物
                 *
                 * 2.若障碍物之间间隔排放，但间隔太近，则合并障碍物
                 */
                $new_blocks[$last_new_block_index]['right_limit'] = $blocks[$i]['right_limit'];
                $new_blocks[$last_new_block_index]['width'] = $blocks[$i]['right_limit'] + $new_blocks[$last_new_block_index]['m_left_mm'];
            } elseif ($blocks[$i]['m_left_mm'] > $new_blocks[$last_new_block_index]['right_limit'] + 25 + 20 * 3) {
                /*
                 * 如果障碍物的最左边在合并障碍物右边，则把当前循环的障碍物当作合并障碍物新增
                 */
                $new_blocks[] = $blocks[$i];
            }
        }

        return $new_blocks;
    }

    /**
     * 排列组合
     * @param $arr
     * @param int $index
     * @param array $temp
     * @param $res
     */
    private function getCombintions($arr, $index = 0, $temp = [], &$res)
    {
        if (count($arr) < $index + 1) {
            return;
        }
        foreach ($arr[$index] as $value) {
            if (count($arr) == $index + 1) {
                $res[] = array_merge($temp, $value);
            } else {
                $this->getCombintions($arr, $index + 1, array_merge($temp, $value), $res);
            }
        }
    }

    /**
     * 通过宽度计算最大有多少列
     * @param $GTWidth
     * @return array
     */
    private function getColCountList($GTWidth)
    {
        if ($GTWidth >= 3689) {
            return [3, 4, 5, 6, 7, 8];
        } elseif ($GTWidth >= 2461 && $GTWidth <= 3706) {
            return [2, 3, 4, 5, 6, 7];
        } elseif ($GTWidth >= 1985 && $GTWidth <= 2478) {
            return [2, 3, 4, 5];
        } elseif ($GTWidth >= 1648 && $GTWidth <= 2002) {
            return [2, 3, 4];
        } elseif ($GTWidth >= 1251 && $GTWidth <= 1665) {
            return [2, 3];
        } elseif ($GTWidth >= 1192 && $GTWidth <= 1268) {
            return [1, 2];
        } elseif ($GTWidth >= 796 && $GTWidth <= 1209) {
            return [1, 2];
        } elseif ($GTWidth >= 400 && $GTWidth <= 813) {
            return [1];
        }
    }

    /**
     * 计算每一列的宽度
     * @param $colCount
     * @param $holeWidth
     * @param $side 是否指定侧板宽度,null不指定
     * @return array|null
     */
    private function getWidthSet($colCount, $holeWidth, $side = null)
    {
        $arrayLength = $colCount + 2;
        $array = array_fill(1, $colCount, CUSTOM_KEY_WIDTH_784);// 列宽
        if ($side === null) {
            $array[0] = $array[$arrayLength - 1] = SIDE_WIDTH_1; // 左侧板和右侧板
        } else {
            $array[0] = $side[0];//第一个是左边板
            $array[$arrayLength - 1] = $side[1];//第二个是右边板
        }

        $width = array_sum($array) + INNER_WIDTH * ($colCount - 1);
        if ($width == $holeWidth) {
            return $array;
        }
        if ($width < $holeWidth) {
            $diffTimes32 = intval(($holeWidth - $width) / CUSTOM_COLUMN_WIDTH_STEP);
            for ($index = 1; $index < $arrayLength - 1; $index++) {
                $array[$index] += CUSTOM_COLUMN_WIDTH_STEP * $diffTimes32;
                if ($array[$index] <= CUSTOM_COLUMN_MAX_WIDTH) {
                    break;
                } else {
                    $diffTimes32 = intval(($array[$index] - CUSTOM_COLUMN_MAX_WIDTH) / CUSTOM_COLUMN_WIDTH_STEP);
                    $array[$index] = CUSTOM_COLUMN_MAX_WIDTH;
                    continue;
                }
            }
        } else {
            $diffTimes32 = intval(($width - $holeWidth) / CUSTOM_COLUMN_WIDTH_STEP);
            $temp = ($width - $holeWidth) % CUSTOM_COLUMN_WIDTH_STEP;
            if ($temp != 0) {
                $diffTimes32 = $diffTimes32 + 1;
                $temp = CUSTOM_COLUMN_WIDTH_STEP - $temp;
            }

            for ($index = 1; $index < $arrayLength - 1; $index++) {
                $array[$index] -= CUSTOM_COLUMN_WIDTH_STEP * $diffTimes32;
                if ($array[$index] >= CUSTOM_COLUMN_MIN_WIDTH) {
                    $diffTimes32 = 0;
                    break;
                } else {
                    $diffTimes32 = intval((CUSTOM_COLUMN_MAX_WIDTH - $array[$index]) / CUSTOM_COLUMN_WIDTH_STEP);
                    $array[$index] = CUSTOM_COLUMN_MIN_WIDTH;
                    continue;
                }
            }
            if ($diffTimes32 > 0) {
                return null;
            }
        }
        $width = array_sum($array) + INNER_WIDTH * ($colCount - 1);
        $temp = ($holeWidth - $width) % CUSTOM_COLUMN_WIDTH_STEP;
        if ($side === null) {
            if ($temp >= 18) {
                $array[0] = SIDE_WIDTH_2;
                $array[$arrayLength - 1] = SIDE_WIDTH_2;
            } elseif ($temp >= 9) {
                $array[0] = SIDE_WIDTH_2;
            }
        } else {
            $result = array_keys($array, SIDE_WIDTH_1);
            if ($temp >= 9 && !empty($result)) {
                $array[$result[0]] = SIDE_WIDTH_2;
                if ($temp >= 18 && isset($result[1])) {
                    $array[$result[1]] = SIDE_WIDTH_2;
                }
            }
        }
        ksort($array);
        return $array;
    }

    /**
     * 调整列宽组合，若柜体靠墙，则需要将靠墙的那一列调整成1200，即最大列宽
     * @param SchemeBean $scheme
     * @param $set 这个set的数据一般为：[16|25,784,784,784,16|25]之类的组合，即外侧板+列宽*count+外侧板
     */
    private function widthSetFilter(SchemeBean $scheme, &$set)
    {
        $is_left = $scheme->getIs_left_wall();
        $is_right = $scheme->getIs_right_wall();
        /*
         * 两边都不靠墙时，不需要进行操作，又或者只有一列时，不需要操作
         * 其中一边靠墙时，最靠墙的那一列宽为1200
         */
        if (($is_left == 0 && $is_right == 0) || count($set) <= 3) {
            return;
        }

        ksort($set);
        $gap = 0;//需要调整的值
        $begin_index = 1;//第一柜列下标，由于0为外侧板，所以该处为1
        $end_index = count($set) - 2;//最后一柜列下标，由于count-1为外侧板，所以该处-2
        //如果左边靠墙，把第一列设置为1200,并均分到其余列
        if ($is_left) {
            $gap += CUSTOM_COLUMN_MAX_WIDTH - $set[$begin_index];
            $set[$begin_index] = CUSTOM_COLUMN_MAX_WIDTH;
            $begin_index++;
        }
        //如果右边靠墙，把最后一列设置为1200,并均分到其余列
        if ($is_right) {
            $gap += CUSTOM_COLUMN_MAX_WIDTH - $set[$end_index];
            $set[$end_index] = CUSTOM_COLUMN_MAX_WIDTH;
            $end_index--;
        }
        //假如结尾列的下标已经在开始列前面，则该方案作废
        if ($end_index < $begin_index) {
            $set = null;
            return;
        }

        $need_to_adjust = array_slice($set, $begin_index, $end_index - $begin_index + 1);//取出需要重新调整的列
        $avg_width = (array_sum($need_to_adjust) - $gap) / count($need_to_adjust);//计算重新调整后的单个列宽
        if ($avg_width > CUSTOM_COLUMN_MAX_WIDTH || $avg_width < CUSTOM_COLUMN_MIN_WIDTH) {//如果单个列宽大于1200，则表示这个方案不成立
            $set = null;
            return;
        }

        //重新给非靠墙列赋值，由于可能会出现浮点的情况，所以要进行处理
        $clostet_width = CUSTOM_COLUMN_MIN_WIDTH + CUSTOM_COLUMN_WIDTH_STEP * intval(($avg_width - CUSTOM_COLUMN_MIN_WIDTH) / CUSTOM_COLUMN_WIDTH_STEP);
        for (; $begin_index <= $end_index; $begin_index++) {
            $set[$begin_index] = $clostet_width;
        }
        $temp = array_sum($need_to_adjust) - $gap - $clostet_width * count($need_to_adjust);
        if ($temp != 0) {
            $set[$end_index] += $temp;
        }
    }


    /**
     * 获取最接近的列宽  如  200的宽，会返回最小列宽400   如431，会返回最接近的列宽432
     * @param $block_width
     * @return float|int
     */
    private function getClostetWidth($block_width)
    {
        if ($block_width < CUSTOM_COLUMN_MIN_WIDTH) {
            $block_width = CUSTOM_COLUMN_MIN_WIDTH;
        } else {
            $block_width = (intval(($block_width - CUSTOM_COLUMN_MIN_WIDTH) / CUSTOM_COLUMN_WIDTH_STEP) + 1) * CUSTOM_COLUMN_WIDTH_STEP + CUSTOM_COLUMN_MIN_WIDTH;
        }
        return $block_width;
    }

//    public function buildRooms($data)
//    {
//        $walls = ['A' => $data['children']['A'], 'B' => $data['children']['B'], 'C' => $data['children']['C'], 'D' => $data['children']['D']];
//
//        /*
//         * 计算障碍物,例如A墙最左有柱体,则在D墙也加上柱体
//         */
//        foreach ($walls as $k => $wall) {
//            $map = $this->getSpecMap($k);//获取属性映射
//            foreach ($wall['children'] as $block) {
//                $space = $wall['data_size'][$map['width']] / 2 - abs($block['position'][$map['width']]) - $block['data_size'][$map['width']] / 2;//计算障碍物到它所属的墙体边缘的距离
//                if ($space > $this->cross && $block['type'] != 'temp') {//如果距离超过了cross，则不需要往其它相邻墙体添加障碍物
//                    continue;
//                } else {
//                    $siblings_key = $this->getSiblings($k, $this->getPointer($k, $block['position'][$map['width']]));//获取相邻的墙体坐标
//
//                    $block['type'] = 'temp';//指定类型为temp，到时候返回数据时进行过滤
//                    $block['position'] = $this->recalculatePosition($block['position'], $wall['position'], $walls[$siblings_key]['position']);
//
//                    $walls[$siblings_key]['children'][] = $block;
//                }
//            }
//        }
//
//
//        /*
//         * 计算可用空间
//         */
//        foreach ($walls as $k => $wall) {
//            $map = $this->getSpecMap($k);//获取属性映射
//            $wall_width = $wall['data_size'][$map['width']];//墙宽
//            $wall_height = $wall['data_size'][$map['height']];//墙高
//
//            $left = $right = $wall_width >> 2;//墙体左右两边的宽度
//            foreach ($wall['children'] as $block) {
//                $point_name = $this->getPointer($k, $block['position'][$map['width']]) < 0 ? 'left' : 'right';
//                $space = abs($block['position'][$map['width']]) - $block['data_size'][$map['width']] / 2;
//                $$point_name = $space < $$point_name ? $space : $$point_name;
//            }
//            echo $left;
//            exit;
//        }
//    }

//    /**
//     * 获取相邻的墙体
//     * @param $char 哪一面墙：A.B.C.D
//     * @param $pointer 指向：-1,左边的墙，1，右边的墙
//     * @return mixed 返回墙体的坐标：A.B.C.D
//     */
//    private function getSiblings($char, $pointer)
//    {
//        if (empty($this->index_map)) {
//            $this->index_map = [
//                ord('A') % 4 => 'A',
//                ord('B') % 4 => 'B',
//                ord('C') % 4 => 'C',
//                ord('D') % 4 => 'D',
//            ];
//        }
//        return $this->index_map[(ord($char) + $pointer) % 4];
//    }
//
//    /**
//     * 获取不同墙体上，对应二维的宽高厚的对应关系
//     * @param $char
//     * @return array
//     */
//    private function getSpecMap($char)
//    {
//        $result = ['height' => 'y'];
//        switch ($char) {
//            case 'A':
//            case 'C':
//                $result['width'] = 'x';
//                $result['deep'] = 'z';
//                break;
//            case 'B':
//            case 'D':
//                $result['width'] = 'z';
//                $result['deep'] = 'x';
//                break;
//        }
//        return $result;
//    }
//
//
//    /**
//     * 获取和障碍物最接近的墙体方向
//     * @param $char
//     * @param $width
//     * @return int -1代表左边,1代表右边
//     */
//    private function getPointer($char, $width)
//    {
//        switch ($char) {
//            case 'B':
//            case 'D':
//                $width = -$width;
//                break;
//        }
//        return $width < 0 ? -1 : 1;//-1代表左边,1代表右边
//    }
//
//    /**
//     * 计算障碍物相对于其它墙体时的position
//     * @param $block_position
//     * @param $relative_position
//     * @param $target_position
//     * @return array
//     */
//    private function recalculatePosition($block_position, $relative_position, $target_position)
//    {
//        return ['x' => $relative_position['x'] + $block_position['x'] - $target_position['x'],
//            'y' => $relative_position['y'] + $block_position['y'] - $target_position['y'],
//            'z' => $relative_position['z'] + $block_position['z'] - $target_position['z']
//        ];
//    }
}