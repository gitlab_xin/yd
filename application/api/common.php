<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

//todo 移到外层的common，前后台都需要相同参数
//define('PRO_PIC', DS . 'proPic' . DS);
//define('DCOLOR', DS . 'dcolor' . DS);
//define('SCHEME_PIC', EXTEND_PATH . 'scheme_pic' . DS);
///**
// * 收口基料厚度
// */
//define('CUSTOM_SHOUKOU_IN', 20);
///**
// * 收口基料+盖板厚度
// */
//define('CUSTOM_SHOUKOU_IN_OUT', 23);
///**
// * 收口垫料(垫方宽度) 35
// */
//define('CUSTOM_PADDING', 35);
///**
// * 自定义衣柜列最小宽度400
// */
//define('CUSTOM_COLUMN_MIN_WIDTH', 400);
///**
// * 内侧板宽度 25mm
// */
//define('INNER_WIDTH', 25);
///**
// * 自定义衣柜列宽度步进值 32mm
// */
//define('CUSTOM_COLUMN_WIDTH_STEP', 32);
///**
// * 自定义衣柜列高度步进值 32mm
// */
//define('CUSTOM_COLUMN_HEIGHT_STEP', 32);
///**
// * 自定义衣柜列最大宽度1200
// */
//define('CUSTOM_COLUMN_MAX_WIDTH', 1200);
///**
// * 抽拉组件两侧各有10mm的空间
// */
//define('DRAW_SPACE', 10);
///**
// * 外侧板宽度1 16mm
// */
//define('SIDE_WIDTH_1', 16);
///**
// * 外侧板宽度2 25mm
// */
//define('SIDE_WIDTH_2', 25);
///**
// * 标准宽度 400mm
// */
//define('CUSTOM_KEY_WIDTH_400', 400);
///**
// * 标准宽度 784mm
// */
//define('CUSTOM_KEY_WIDTH_784', 784);
///**
// * 自定义衣柜洞口最大高度 2900mm
// */
//define('CUSTOM_MAX_HEIGHT', 2900);
///**
// * 自定义衣柜最低高度 2019mm
// */
//define('CUSTOM_MIN_HEIGHT', 2019);
///**
// * 自定义衣柜板材最大高度 2403mm
// */
//define('BOARD_MAX_HEIGHT', 2403);
///*
// * 自定义衣柜门框宽度 30mm
// */
//define('DOOR_FRAME_WIDTH', 30);
//
//function getSchemeColorArrH($color)
//{
//    $array = [
//        "XiaTeHuiBuWenh.jpg",
//        "XiaTeLiMuh.jpg",
//        "XiaTeQianHuTaoh.jpg",
//        "XiaTeShenHuTaoh.jpg",
//        "XiaWangNuanBaih.jpg",
//        "5",
//        "6",
//        "7",
//        "8",
//        "9",
//        "10",
//        "11",
//        "12",
//        "13",
//        "14",
//        "15",
//        "16",
//        "17",
//        "18",
//        "19",
//        "hs20h.jpg",
//        "hs21h.jpg",
//        "hs22h.jpg",
//        "hs23h.jpg",
//        "hs24h.jpg",
//        "hs25h.jpg",
//        "hs26h.jpg",
//        "27",
//        "28",
//        "29",
//        "30",
//        "31",
//        "32",
//        "33",
//        "34",
//        "35",
//        "36",
//        "37",
//        "38",
//        "39",
//        "40",
//        "41",
//        "42",
//        "43",
//        "hs44h.jpg"
//    ];
//    return $array[intval($color)];
//}
//
//function getPlatePrice($index)
//{
//    $platePriceArr = [
//        350,
//        272,
//        190,
//        160
//    ];
//
//    return $platePriceArr[intval($index)];
//}
//
//function getRandomInt()
//{
//    return mt_rand() % 876;
//}
//
//function GetBZDoorPicDir()
//{
//    return 'bz_' . (getRandomInt() % 20);
//}
//
//function change2SchemeBean($scheme)
//{
//    return false ? new \app\common\model\SchemeBean() : $scheme;
//}
//
//function change2ProductBean($product)
//{
//    return false ? new \app\common\model\ProductBean() : $product;
//}
//
//function is_starts_with($str, $pattern)
//{
//    return strpos($str, $pattern) === 0;
//}
//
//function str_has($str, $pattern)
//{
//    return strpos($str, $pattern) !== false;
//}
//
//function formatMoney($money, $decimals = 2)
//{
//    return number_format(floatval($money), $decimals, '.', '');
//}
//
///**
// * 进位
// */
//function getLength($l)
//{
//    return ceil($l / 10) * 10;
//}
//
///**
// * 进位
// */
//function getMoney($l)
//{
//    return ceil($l * 10) / 10;
//}
//
///**
// * 计算16:9电视长边
// *
// * @param x
// * @return float
// */
//function GetLongSide16Bi9($x)
//{
//    $temp = (16 / 9) * (16 / 9);
//    $longerSide = sqrt((($x * 25.4) * ($x * 25.4) * $temp) / (1 + $temp));
//    return $longerSide;
//}
