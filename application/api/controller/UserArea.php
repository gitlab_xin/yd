<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\controller;


use app\api\logic\UserAreaLogic;

class UserArea extends Base
{
    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:添加收货地址
     * @return \think\response\Json
     */
    public function add()
    {
        $this->validateToken();
        $requestData = $this->selectParam([
            'username',
            'phone',
            'province',
            'city',
            'county',
            'town',
            'detail',
            'code',
            'is_default',
        ]);
        $this->check($requestData, 'UserArea.add');

        $count = UserAreaLogic::checkCount($this->getUserId());
        if ($count >= 20) {//收货地址数量最多为20条
            $this->setRenderCode(500);
            $this->setRenderMessage('收货地址数量不能超过20条');
            return $this->getRenderJson();
        }

        $result = UserAreaLogic::add($this->getUserId(), $requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('添加成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:修改收货地址
     * @return \think\response\Json
     */
    public function edit()
    {
        $this->validateToken();
        $requestData = $this->selectParam([
            'area_id',
            'username',
            'phone',
            'province',
            'city',
            'county',
            'town',
            'detail',
            'code',
            'is_default',
        ]);
        $this->check($requestData, 'UserArea.edit');

        $result = UserAreaLogic::edit($this->getUserId(), $requestData['area_id'], $requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('修改成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:获取收货地址列表
     * @return \think\response\Json
     */
    public function getList()
    {
        $this->validateToken();
        $result = UserAreaLogic::getList($this->getUserId());

        $this->addRenderData('area_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:删除收货地址详情
     * @return \think\response\Json
     */
    public function delete()
    {
        $this->validateToken();
        $requestData = $this->selectParam([
            'area_id',
        ]);
        $this->check($requestData, 'UserArea.delete');

        $result = UserAreaLogic::delete($this->getUserId(), $requestData['area_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('删除失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('删除成功');
        return $this->getRenderJson();
    }

    public function getConfigAddress()
    {
        $requestData = $this->selectParam(['parent_id' => 0,'name'=>""]);
        if(!empty($requestData['name'])){
            $result = UserAreaLogic::getAddressByName($requestData['name']);
        }else{
            $result = UserAreaLogic::getAddressByParentId($requestData['parent_id']);
        }
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:根据省名字获取城市列表
     * @return \think\response\Json
     */
    public function getCity()
    {
        $requestData = $this->selectParam(['province' => ""]);

        $result = UserAreaLogic::getAddressByName($requestData['province']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }

    public function getCity2()
    {
        $requestData = $this->selectParam(['keyword' => ""]);

        $result = UserAreaLogic::getAddressByName2($requestData['keyword']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }
}