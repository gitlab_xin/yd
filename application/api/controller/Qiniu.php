<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:05
 */

namespace app\api\controller;

use app\common\model\Qiniu as QiniuModel;

class Qiniu extends Base
{
    public function index()
    {
        $QiniuModel = new QiniuModel();
        $bucket = $QiniuModel->getBucketDomain();
        $token = $QiniuModel->getToken();
        $config['bucket_domain'] = $bucket;
        $config['token'] = $token;
        $this->addRenderData('config', $config);
        return $this->getRenderJson();
    }

    public function getBucketDomain()
    {
        $QiniuModel = new QiniuModel();
        $bucket = $QiniuModel->getBucketDomain();
        $this->addRenderData('bucket_domain', $bucket);
        return $this->getRenderJson();
    }

    public function getToken()
    {
        $QiniuModel = new QiniuModel();
        $bucket = $QiniuModel->getToken();
        $this->addRenderData('token', $bucket);
        return $this->getRenderJson();
    }
}