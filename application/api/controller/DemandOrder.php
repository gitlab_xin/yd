<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/25
 * Time: 15:33
 */

namespace app\api\controller;

use app\api\logic\DemandLogic;
use app\api\logic\DemandOrderLogic;
use think\Db;
use app\common\tools\wechatPay;
use app\common\tools\alipayClient;

class DemandOrder extends Base
{

    public function pay()
    {
        $this->validateToken();
        $requestData = $this->selectParam([
            'demand_id',
            'reward',
            'trade_type',
            'pay_type',
        ]);
        $requestData['trade_type'] = empty($requestData['trade_type']) ? 'APP' : $requestData['trade_type'];
        $this->check($requestData, 'DemandOrder.pay');
        $demand_info = DemandLogic::checkDemand($this->getUserId(), $requestData['demand_id']);
        if (empty($demand_info)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('需求内容不存在');
            return $this->getRenderJson();
        }
        Db::startTrans();

        $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号
        $fee = $requestData['reward'];
        $bool = DemandLogic::setDemand($requestData['demand_id'], ['reward' => $fee]);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据处理失败');
            return $this->getRenderJson();
        }
        //生成订单
        $bool = DemandOrderLogic::addOrder($requestData['demand_id'], $out_trade_no, $this->getUserId(), $fee,
            $requestData['pay_type']);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据处理失败');
            return $this->getRenderJson();
        }
        switch ($requestData['pay_type']) {
            case 'wechat':
                $body = "易道魔方";
                //$attach = implode(',',$order_num_list);

                $appId = config('wechat')['AppID'];
                $mchId = config('wechat')['Pay']['MchId'];
                $appKey = config('wechat')['Pay']['Key'];
                $wechatPay = new WechatPay($appId, $mchId, $appKey);

                // $requestData = $wechatPay->order($out_trade_no, $fee, $userOpenid, $body, "");
                $requestData = $wechatPay->order($out_trade_no, 0.01, "", $body, "",
                    $requestData['trade_type'], PUBLIC_PATH . '/index.php/api/Callback/demandWXPay');//todo::测试临时

                if ($requestData === false) {
                    Db::rollback();
                    $this->setRenderCode(500);
                    $this->setRenderMessage('生成订单失败');
                    return $this->getRenderJson();
                }
                break;
            //微信是后台调用接口拿到是否生成订单的返回结果
            case 'alipay':
                $appId = config('alipay')['AppID'];
                $partner_public_key = config('alipay')['partnerPublicKey'];
                $partner_private_key = config('alipay')['rsaPrivateKey'];
                $pay_charge = "易道魔方";
                $alipay_public_key = config('alipay')['alipayPublicKey'];
                $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key, $alipay_public_key);
                if ($requestData['trade_type'] == 'NATIVE') {
                    $requestData = $alipayClient->webOrder($out_trade_no, '0.01', $pay_charge, $pay_charge,
                        "",dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/demandAlipay');
                } else {
                    $requestData = $alipayClient->order($out_trade_no, '0.01', $pay_charge, $pay_charge,
                        "",dirname("https://" . G_HTTP_HOST . $_SERVER['SCRIPT_NAME']) . '/index.php/api/Callback/demandAlipay');
                }
                break;
            //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
        }
        Db::commit();
        $this->setRenderCode(200);
        $this->setRenderMessage('生成订单成功');
        $this->addRenderData('result', $requestData, false);
        return $this->getRenderJson();
    }

    public function checkOrder()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['demand_id']);
        $this->check($requestData, 'DemandOrder.detail');
        $order_bool = DemandOrderLogic::checkOrder($this->getUserId(),$requestData['demand_id']);
        $is_pay = empty($order_bool)?'no':'yes';
        $this->setRenderCode(200);
        $this->addRenderData('is_pay', $is_pay, false);
        return $this->getRenderJson();
    }
}