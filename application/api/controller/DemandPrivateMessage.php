<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/25
 * Time: 15:55
 */

namespace app\api\controller;

use app\api\logic\DemandPrivateMessageLogic as MessageLogic;
use app\api\logic\DemandNotifyLogic as NotifyLogic;

class DemandPrivateMessage extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年8月25日
     * description:发送私信
     * @return \think\response\Json
     */
    public function sendMessage()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id','type','content','user_id']);
        $data['user_id'] = empty($data['user_id'])?0:$data['user_id'];
        $data['login_user_id'] = $this->getUserId();

        $this->check($data,'DemandPrivateMessage.send');

        $result = MessageLogic::addPrivateMessage($data);
        switch ($result){
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('发送失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('不存在该需求');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('暂不支持自己跟自己聊骚');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('发送成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:获取私信列表
     * @return \think\response\Json
     */
    public function getMyMessages()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);
        $data['user_id'] = $this->getUserId();

        $this->check($data,'DemandPrivateMessage.get');

        $result = MessageLogic::getPrivateMessages($data);
        switch ($result){
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('该需求不存在或已被删除');
                break;
            default:
                $this->setRenderCode(200);
                $this->addRenderData('newest_messages', $result ,false);
                $this->setRenderMessage('获取成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月28日
     * description:获取两人聊天记录
     * @return \think\response\Json
     */
    public function getMessageDetail()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id','at_user_id','last_message_id']);
        $data['last_message_id'] = empty($data['last_message_id'])?0:$data['last_message_id'];
        $data['user_id'] = $this->getUserId();

        $this->check($data,'DemandPrivateMessage.detail');

        $result = MessageLogic::getMessageDetail($data,$this->pageSize);
        switch ($result){
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('该需求不存在或已被删除');
                break;
            default:
                $this->setRenderCode(200);
                $this->addRenderData('newest_messages', $result ,false);
                $this->setRenderMessage('获取成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月11日
     * description:
     * @return \think\response\Json
     */
    public function getNotifications()
    {
        $this->validateToken();

        $result = NotifyLogic::getMyNotifications($this->getUserId(),$this->pageIndex,$this->pageSize);

        if($result === false){
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        }else{
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list',$result,false);
        }
        return $this->getRenderJson();
    }
}