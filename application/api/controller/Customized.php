<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/9/11
 * Time: 15:10
 */

namespace app\api\controller;

use app\admin\behavior\CheckAuth;
use app\api\logic\CustomizedLogic;
use app\common\model\Config;
use app\common\model\CustomizedFloorColor;
use app\common\model\CustomizedItem;
use app\common\model\CustomizedItemClassify;
use app\common\model\CustomizedDecoration;
use app\common\model\CustomizedShare;
use app\common\model\CustomizedWallColor;
use app\common\model\CustomizedComponent;
use app\common\model\CustomizedColor;
use app\common\model\CustomizedScheme;
use app\common\model\WebglModule;

class Customized extends Base
{
    /**
     * @author: Airon
     * @time: 2017年9月11日
     * description:移门花色列表
     * @return \think\response\Json
     */
    public function doorColor()
    {
        $result = CustomizedColor::getDoorList();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        foreach ($result as $key => $value)
        {
            $result[$key]['material'] = json_decode($value['material'],true);
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月11日
     * description:墙花色列表
     * @return \think\response\Json
     */
    public function wallColor()
    {
        $result = CustomizedWallColor::getList();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月11日
     * description:地板花色列表
     * @return \think\response\Json
     */
    public function floorColor()
    {
        $result = CustomizedFloorColor::getList();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月13日
     * description:物品列表
     * @return \think\response\Json
     */
    public function item()
    {
        $requestData = $this->selectParam(['classify_id']);
        $this->check($requestData, 'CustomizedItemClassify.item');
        $result = CustomizedItem::getList($requestData['classify_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月13日
     * description:物品列表
     * @return \think\response\Json
     */
    public function itemClassify()
    {
        $requestData = $this->selectParam(['cabinet_type' => 'YG']);

        $result = CustomizedItemClassify::getList($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * 组件
     * @return \think\response\Json
     */
    public function getComponentsCombine()
    {
        $result = CustomizedComponent::getList(['type' => 'combine']);
//        dump($result);exit;
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function getComponentsModule()
    {
        $data = $this->selectParam(['scheme_type', 'height']);

        $data['type'] = 'module';
        $result = CustomizedComponent::getList($data);
//        dump($result);exit;
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * 推荐案例
     */
    public function recommendWork()
    {
        $requestData = $this->selectParam(['scheme_type', 'scheme_b_type', 'scheme_s_type', 'width', 'height',
            'sl_height', 'hole_height', 'range' => 150]);
        $this->check($requestData, 'CustomizedRecommendWork.get');
        $result = CustomizedLogic::getRecommend($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * 推荐案例详情
     */
    public function recommendWorkInfo()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_detail_id']);
        $this->check($requestData, 'CustomizedRecommendWork.info');
        $result = CustomizedLogic::getRecommendInfo($requestData['scheme_detail_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $result = empty($result['serial_array']) ? [] : json_decode($result['serial_array'], true);
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    public function getColors()
    {
        $result = CustomizedColor::getList();
//        dump($result);exit;
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function getCabinetColor()
    {
        $requestData = $this->selectParam(['scheme_type']);

        $this->check($requestData, 'CustomizedCabinet.getCabinetColorList');
        if ($requestData['scheme_type'] == 'DG'){
            $requestData['scheme_type'] = 'ZH-BGDG';
        }
        $result = CustomizedLogic::getColorByCabinetType($requestData['scheme_type']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        foreach ($result as $key => $value)
        {
            $result[$key]['material'] = json_decode($value['material'],true);
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function getCabinetDoorColor()
    {
        $requestData = $this->selectParam(['scheme_type', 'color_no']);

        $this->check($requestData, 'CustomizedCabinet.getDoorColorList');
        $result = CustomizedLogic::getDoorColorByCabinetType($requestData['scheme_type'], $requestData['color_no']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        foreach ($result as $key => $value)
        {
            $result[$key]['material'] = json_decode($value['material'],true);
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function getScenes()
    {
        $result = WebglModule::getObjs();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function getColorPicsByColorNo()
    {
        $requestData = $this->selectParam(['color_no']);
        $this->check($requestData, 'CustomizedCabinet.getColorDetail');

        $result = CustomizedLogic::getColorDetail($requestData['color_no']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        if(!empty($result)){
            $result['material'] = json_decode($result['material'],true);
        }
        $this->addRenderData('detail', $result, false);
        return $this->getRenderJson();
    }

    //根据颜色获取详情
    public function getColorByColorId()
    {
        $requestData = $this->selectParam(['color_id']);
        if(empty($requestData['color_id'])){
            $this->setRenderMessage('颜色ID不能为空');
            $this->setRenderCode(401);
            return $this->getRenderJson();
        }
        $result = CustomizedLogic::getColorDetailById($requestData['color_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        if(!empty($result)){
            $result['material'] = json_decode($result['material'],true);
        }
        $this->addRenderData('detail', $result, false);
        return $this->getRenderJson();
    }

    //设置颜色材质 后台管理用
    public function setColorMaterial()
    {
        $requestData = $this->selectParam(['color_id','material'=>""]);
        if(empty($requestData['color_id'])){
            $this->setRenderMessage('颜色ID不能为空');
            $this->setRenderCode(401);
            return $this->getRenderJson();
        }

        $result = CustomizedLogic::setColorMaterial($requestData['color_id'],$requestData['material']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        return $this->getRenderJson();
    }

    public function get3DByCabinet()
    {
        $requestData = $this->selectParam(['scheme_type']);

        $result = CustomizedLogic::get3DByCabinet($requestData['scheme_type']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('data3D', $result, false);
        return $this->getRenderJson();
    }

    public function get3DByCabinetTemp()
    {
        $requestData = $this->selectParam(['scheme_type']);

        $result = CustomizedLogic::get3DByCabinetTemp($requestData['scheme_type']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('data3D', $result, false);
        return $this->getRenderJson();
    }

    public function get3DByCabinetAdmin()
    {
        $requestData = $this->selectParam(['scene_id']);

        $result = CustomizedLogic::get3DByCabinetAdmin($requestData['scene_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('data3D', $result, false);
        return $this->getRenderJson();
    }

    public function get3DByHouseType()
    {
        $requestData = $this->selectParam(['house_id']);

        $result = CustomizedLogic::get3DByHouseType($requestData['house_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('data3D', $result, false);
        return $this->getRenderJson();
    }


    public function set3DByHouseType()
    {
        $requestData = $this->selectParam(['house_id','data']);
        if(empty($requestData['data'])){
            $this->setRenderCode(402);
            $this->setRenderMessage('data数据为空');
            return $this->getRenderJson();
        }
        if(empty($requestData['house_id'])){
            $this->setRenderCode(402);
            $this->setRenderMessage('户型ID为空');
            return $this->getRenderJson();
        }
        $result = CustomizedLogic::set3DByHouseType($requestData['house_id'],$requestData['data']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('设置数据失败');
            return $this->getRenderJson();
        }
        return $this->getRenderJson();
    }

    public function getComponentDetail()
    {
        $requestData = $this->selectParam(['component_id']);

        $result = CustomizedLogic::getComponentDetail($requestData['component_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    public function saveComponent()
    {
        $auth = new CheckAuth();
        $auth->checkToken();

        $requestData = $this->selectParam(['component']);
        $result = CustomizedLogic::saveComponent($requestData['component']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    public function getFloorDetail()
    {
        $requestData = $this->selectParam(['floor_id']);

        $result = CustomizedFloorColor::get($requestData['floor_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    public function getWallDetail()
    {
        $requestData = $this->selectParam(['wall_id']);

        $result = CustomizedWallColor::get($requestData['wall_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    public function getAllItems()
    {
        $requestData = $this->selectParam(['cabinet_type' => 'YG']);

        $result = CustomizedItemClassify::getList($requestData, true);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function saveShare()
    {
        $this->validateToken();

        $requestData = $this->selectParam(['scheme_id', 'scheme_pic_two', 'expired_time']);
        $this->check($requestData, 'CustomizedShare.save');

        $requestData['token'] = get_token();
        $model = CustomizedShare::create($requestData);

        $this->addRenderData('share_id', intval($model->getLastInsID()));
        return $this->getRenderJson();
    }

    public function getShare()
    {
        $requestData = $this->selectParam(['share_id']);
        $model = CustomizedShare::build()->where(['share_id' => $requestData['share_id']])->field(['scheme_id', 'scheme_pic_two', 'scheme_pic_three', 'expired_time'])->find();
        if ($model == null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分享信息不存在');
            return $this->getRenderJson();
        } elseif ($model->expired_time < time()) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分享信息已过期');
        } else {
            $this->addRenderData('detail', $model);
        }

        return $this->getRenderJson();
    }

//    public function demo()
//    {
//        $url = 'https://api.dankal.cn/snip/web';
//        $headers = [
//            "Content-type: application/json;charset='utf-8'", "Accept: application/json"
//        ];
//        $data = [
//            'web_url' => 'http://three.inffur.com:8011/?type=show&scheme_id=5220&token=4a9d29a6639ad466f7b2177ce8771575',
//            'delay' => 0,
//            'viewport' => [
//                'width' => 1200,
//                'height' => 680
//            ]
//        ];
//        $handle = curl_init();
//        curl_setopt($handle, CURLOPT_URL, $url);
//        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
//        curl_setopt($handle, CURLOPT_POST, 1);
//        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
//        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 2);
//
//        $return = curl_exec($handle);
//        curl_close($handle);
//        dump($return);
//        exit;
//    }

    public function getShareInfo()
    {
        $requestData = $this->selectParam(['share_id']);
        $scheme_id = CustomizedShare::build()->where(['share_id' => $requestData['share_id']])->value('scheme_id', 0);
        if ($scheme_id == 0) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分享内容不存在');
            return $this->getRenderJson();
        }
        $data = CustomizedScheme::getInfo(
            ['id' => $scheme_id],
            ['id as scheme_id', 'scheme_name', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'serial_array']);
        if (empty($data)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('方案不存在');
            return $this->getRenderJson();
        }
        $data['serial_array'] = json_decode($data['serial_array'], true);
        $this->setRenderCode(200);
        $this->addRenderData('scheme_id', $data['scheme_id'], false);
        $this->addRenderData('scheme_name', $data['scheme_name'], false);
        $this->addRenderData('scheme_type', $data['scheme_type'], false);
        $this->addRenderData('scheme_s_type', $data['scheme_s_type'], false);
        $this->addRenderData('scheme_b_type', $data['scheme_b_type'], false);
        $this->addRenderData('serial_array', $data['serial_array'], false);
        return $this->getRenderJson();
    }

    public function getAllDecorations()
    {
        $result = CustomizedDecoration::getList();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    public function setCabinetModel(){
        $requestData = $this->selectParam(['model_id','type','name','uuid','objName','mtlName','pics','scale','rotation','position','size','bottom_to_top','material']);
        $requestData['scale'] = json_encode($requestData['scale']);
        $requestData['rotation'] = json_encode($requestData['rotation']);
        $requestData['position'] = json_encode($requestData['position']);
        $requestData['size'] = json_encode($requestData['size']);
        $requestData['material'] = json_encode($requestData['material']);
        $requestData['pics'] = $requestData['pics'] == '' ? [] : implode(',', $requestData['pics']);
        $this->check($requestData,'CustomizedCabinetModel.set');
        $bool = CustomizedLogic::saveModel($requestData,$requestData['model_id']);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('编辑失败');
            return $this->getRenderJson();
        }
        return $this->getRenderJson();
    }

    public function getCabinetModel(){
        $requestData = $this->selectParam(['model_id']);
        if(empty($requestData['model_id'])){
            $this->setRenderMessage('模型ID不能为空');
            $this->setRenderCode(401);
            return $this->getRenderJson();
        }
        $bool = CustomizedLogic::getModel($requestData['model_id']);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('模型不存在');
            return $this->getRenderJson();
        }

        $this->addRenderData('model', $bool, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 获取三维插件版本信息
     */
    public function getThreePackageConfig(){
        $value = Config::build()->where(['key' => 'three_package_version'])->value('value');
        if (empty($value)) {
            return $this->getRenderJson();
        }
        $value = json_decode($value, true);
        $this->addRenderData('version', $value, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 设置三维插件版本信息
     */
    public function setThreePackageConfig(){
        $requestData = $this->selectParam(['force','download_url','version_code']);
        if(!isset($requestData['force']) || empty($requestData['download_url']) || empty($requestData['version_code'])){
            $this->setRenderMessage('参数不能为空');
            $this->setRenderCode(401);
            return $this->getRenderJson();
        }
        $requestData['version_code'] = intval($requestData['version_code']);
        $value = json_encode($requestData,JSON_UNESCAPED_UNICODE);
        $value = Config::build()->where(['key' => 'three_package_version'])->update(['value'=>$value]);
        $this->setRenderCode(200);
        $this->setRenderMessage('success');
        return $this->getRenderJson();
    }
}