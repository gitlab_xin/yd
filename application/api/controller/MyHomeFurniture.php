<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 16:02
 */

namespace app\api\controller;

use app\api\logic\MyHomeFurnitureLogic as FurnitureLogic;

class MyHomeFurniture extends Base
{
    /**
     * @author: Airon
     * 家具分类列表
     * @return \think\response\Json
     */
    public function classifyList()
    {
        $result = FurnitureLogic::getClassifyList();

        $this->addRenderData('classify_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 上传家具模型
     * @return \think\response\Json
     */
    public function insertFurniture()
    {
        $requestData = $this->selectParam(['name', 'type', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro','render_img']);
        $this->check($requestData, 'MyHomeFurniture.insert');
        $requestData['size'] = json_encode($requestData['size']);
        $requestData['position'] = json_encode($requestData['position']);
        $requestData['scale'] = json_encode($requestData['scale']);
        $requestData['rotation'] = json_encode($requestData['rotation']);
        $result = FurnitureLogic::insertFurniture($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('上传成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 编辑家具模型
     * @return \think\response\Json
     */
    public function editFurniture()
    {
        $requestData = $this->selectParam(['furniture_id', 'name', 'type', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro','render_img']);
        $this->check($requestData, 'MyHomeFurniture.edit');
        $check = FurnitureLogic::checkFurniture($requestData['furniture_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('家具不存在');
            return $this->getRenderJson();
        }

        $requestData['size'] = json_encode($requestData['size']);
        $requestData['position'] = json_encode($requestData['position']);
        $requestData['scale'] = json_encode($requestData['scale']);
        $requestData['rotation'] = json_encode($requestData['rotation']);
        $result = FurnitureLogic::editFurniture($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('编辑失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('编辑成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 上传家具款式模型
     * @return \think\response\Json
     */
    public function insertMaterial()
    {
        $requestData = $this->selectParam([
            "furniture_id",
            "type",
            "name",
            "color",
            "map",
            "repeat",
            "offset",
            "roughness",
            "roughness_map",
            "metalness",
            "metalness_map",
            "light_map",
            "light_map_intensity",
            "ao_map",
            "ao_map_intensity",
            "emissive",
            "emissive_intensity",
            "emissive_map",
            "bump_map",
            "bump_scale",
            "normal_map",
            "normal_scale",
            "displacement_map",
            "displacement_scale",
            "displacement_bias",
            "env_map",
            "env_map_intensity",
            "render_img",
            "alpha_map"]);

        $this->check($requestData, 'MyHomeFurnitureMaterial.insert');

        $check = FurnitureLogic::checkFurniture($requestData['furniture_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('家具不存在');
            return $this->getRenderJson();
        }

        $result = FurnitureLogic::insertMaterial($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('上传成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 编辑家具款式模型
     * @return \think\response\Json
     */
    public function editMaterial()
    {
        $requestData = $this->selectParam([
            "material_id",
            "type",
            "name",
            "color",
            "map",
            "repeat",
            "offset",
            "roughness",
            "roughness_map",
            "metalness",
            "metalness_map",
            "light_map",
            "light_map_intensity",
            "ao_map",
            "ao_map_intensity",
            "emissive",
            "emissive_intensity",
            "emissive_map",
            "bump_map",
            "bump_scale",
            "normal_map",
            "normal_scale",
            "displacement_map",
            "displacement_scale",
            "displacement_bias",
            "env_map",
            "env_map_intensity",
            "render_img",
            "alpha_map"]);

        $this->check($requestData, 'MyHomeFurnitureMaterial.edit');

        $check = FurnitureLogic::checkMaterial($requestData['material_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('款式不存在');
            return $this->getRenderJson();
        }

        $result = FurnitureLogic::editMaterial($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('编辑失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('编辑成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 获取家具模型详情
     * @return \think\response\Json
     */
    public function getFurnitureInfo()
    {
        $requestData = $this->selectParam(['furniture_id']);
        $this->check($requestData, 'MyHomeFurniture.getInfo');
        $result = FurnitureLogic::getFurnitureInfo($requestData['furniture_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $result['size'] = json_decode($result['size'],true);
        $result['position'] = json_decode($result['position'],true);
        $result['scale'] = json_decode($result['scale'],true);
        $result['rotation'] = json_decode($result['rotation'],true);
        $this->addRenderData('furniture_info', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 获取家具模型列表
     * @return \think\response\Json
     */
    public function getFurnitureList()
    {
        $requestData = $this->selectParam(['classify_id']);
        $this->check($requestData, 'MyHomeFurniture.getList');
        $result = FurnitureLogic::getFurnitureList($requestData['classify_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        foreach ($result as $key=>$value){
            $result[$key]['size'] = json_decode($value['size']);
        }
        $this->addRenderData('furniture_list', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 获取家具款式详情
     * @return \think\response\Json
     */
    public function getMaterialInfo()
    {
        $requestData = $this->selectParam(['material_id']);
        $this->check($requestData, 'MyHomeFurnitureMaterial.getInfo');
        $result = FurnitureLogic::getMaterialInfo($requestData['material_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $info = json_decode($result['serial_array'], true);
        $info['material_id'] = $result['material_id'];
        $info['alpha_map'] = $result['alpha_map'];
        $this->setRenderMessage('获取成功');
        $this->addRenderData('material_info', $info, false);
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 删除家具款式
     * @return \think\response\Json
     */
    public function deleteMaterial()
    {
        $requestData = $this->selectParam(['material_id']);
        $this->check($requestData, 'MyHomeFurnitureMaterial.getInfo');
        $result = FurnitureLogic::checkMaterial($requestData['material_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('款式不存在');
            return $this->getRenderJson();
        }
        $info['material_id'] = $result['material_id'];
        FurnitureLogic::deleteMaterial($requestData['material_id']);
        $this->setRenderMessage('删除成功');
        return $this->getRenderJson();
    }


    /**
     * @author:Airon
     * 获取家具款式列表
     * @return \think\response\Json
     */
    public function getMaterialList()
    {
        $requestData = $this->selectParam(['furniture_id']);
        $this->check($requestData, 'MyHomeFurnitureMaterial.getList');
        $result = FurnitureLogic::getMaterialList($requestData['furniture_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('material_list', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }
}