<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/14
 * Time: 14:51
 */

namespace app\api\controller;

use think\Url;
use think\Request;
use app\common\model\CustomizedCabinetScene as SceneModel;

class CustomizedCabinetScene extends Base
{
    public function update()
    {
        $requestData = $this->selectParam(['scene_id', 'data', 'type']);
        $this->check($requestData, 'CustomizedCabinetScene.update');
        $info = SceneModel::build()->where(['scene_id' => $requestData['scene_id']])->field(['environment_array'])->find();
        if (empty($info)) {
            $this->setRenderCode(402);
            $this->setRenderMessage('场景不存在');
            return $this->getRenderJson();
        }
        $environment_array = $info['environment_array'];
        $environment_array = json_decode($environment_array, true);
        $environment_array[$requestData['type']] = $requestData['data'];
        $bool = SceneModel::build()
            ->update(['environment_array' => json_encode($environment_array, JSON_UNESCAPED_UNICODE)], ['scene_id' => $requestData['scene_id']]);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        return $this->getRenderJson();
    }

    public function get(){
        $requestData = $this->selectParam(['scene_id', 'type']);
        $this->check($requestData, 'CustomizedCabinetScene.get');
        $info = SceneModel::build()->where(['scene_id' => $requestData['scene_id']])->field(['environment_array'])->find();
        if (empty($info)) {
            $this->setRenderCode(402);
            $this->setRenderMessage('场景不存在');
            return $this->getRenderJson();
        }
        $environment_array = $info['environment_array'];
        $environment_array = json_decode($environment_array, true);
        $data = &$environment_array[$requestData['type']];
        $this->setRenderCode(200);
        $this->addRenderData("data", $data,false);
        return $this->getRenderJson();
    }
}