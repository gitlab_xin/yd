<?php
namespace app\api\controller;


use app\api\logic\MyDecorateProductCartLogic;
use app\common\model\CustomizedCabinet as CabinetModel;
use app\common\model\CustomizedColor as ColorModel;
use app\common\model\MyDecorateProductStandard;
use app\common\model\ShopProductStandard;

class MyDecorateProductCart extends Base
{
    public function add(){
        $this->validateToken();
        $requestData = $this->selectParam(['cart_list', 'program_id', 'color_id' => '']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'MyDecorateProduct.cart_list');
        foreach ($requestData['cart_list'] as &$v){
            $v['color_id'] = (!isset($v['color_id'])) ? '': $v['color_id'];
            $v['standard_id'] = (!isset($v['standard_id'])) ? '': $v['standard_id'];
            $this->check($v, 'MyDecorateProduct.cart_info');
        }

        $result = MyDecorateProductCartLogic::add($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('添加失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('添加成功');
        return $this->getRenderJson();

    }

    public function info(){
        $this->validateToken();
        $requestData = $this->selectParam(['program_id']);
        $requestData['user_id'] = $this->getUserId();

        $result = MyDecorateProductCartLogic::info($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('添加成功');
        $this->addRenderData('cart_info', $result, false);

        return $this->getRenderJson();
    }

    public function lists(){
        $this->validateToken();
        $requestData = $this->selectParam(['program_id', 'type']);
        $requestData['page_index'] = $this->pageIndex;
        $requestData['page_size'] = $this->pageSize;
        $requestData['user_id'] = $this->getUserId();

        $result = MyDecorateProductCartLogic::lists($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('添加成功');
        $this->addRenderData('cart_list', $result, false);

        return $this->getRenderJson();
    }

    public function allLists(){
        $this->validateToken();
        $requestData = $this->selectParam(['program_id', 'type']);
        $requestData['page_index'] = $this->pageIndex;
        $requestData['page_size'] = $this->pageSize;
        $requestData['user_id'] = $this->getUserId();

        $result = MyDecorateProductCartLogic::allLists($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('cart_list', $result, false);

        return $this->getRenderJson();
    }

    public function colorList(){

        $requestData = $this->selectParam(['decorate_product_id' => '']);
        if (!empty($requestData['decorate_product_id'])){
            $list = MyDecorateProductStandard::build()
                ->alias('s')
                ->join('yd_customized_color c','c.color_id = s.color_id')
                ->where(['c.is_deleted' => 0, 'c.is_show' => 1,'s.decorate_product_id' => $requestData['decorate_product_id']])
                ->field(['c.color_id','c.origin_img_src','c.color_name', 's.product_img'])
                ->order(['c.is_default' => 'ASC','c.color_no' => 'ASC'])
                ->select();
        } else {
            $list = ColorModel::build()
                ->where(['is_deleted' => 0,'is_show' => 1])
                ->field(['color_id','origin_img_src','color_name'])
                ->order(['is_default' => 'ASC','color_no' => 'ASC'])
                ->select();
        }


        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('color_list', $list, false);

        return $this->getRenderJson();
    }

    public function standardList(){
        $requestData = $this->selectParam(['product_id' => '']);

        $list = ShopProductStandard::build()
            ->where(['product_id' => $requestData['product_id'], 'is_deleted' => '0'])
            ->field([
                'standard_id', 'img', 'name', 'price', 'stocks', 'status'
            ])
            ->order(['status' => 'DESC'])
            ->select();
        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('color_list', $list, false);

        return $this->getRenderJson();

    }

    public function delete(){
        $this->validateToken();
        $requestData = $this->selectParam(['id']);
        $this->check($requestData, ['id' => 'require']);
        $requestData['user_id'] = $this->getUserId();

        $result = MyDecorateProductCartLogic::delete($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('删除失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('删除成功');

        return $this->getRenderJson();

    }

    public function update(){
        $this->validateToken();
        $requestData = $this->selectParam(['id', 'color_id' => '', 'count' => '', 'standard_id' => '']);
        $this->check($requestData, ['id' => 'require']);
        $requestData['user_id'] = $this->getUserId();

        $result = MyDecorateProductCartLogic::update($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('更新失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('更新成功');

        return $this->getRenderJson();
    }

    public function read(){
        $requestData = $this->selectParam(['decorate_product_id', 'cart_id']);
        $this->check($requestData, ['decorate_product_id' => 'require']);

        $result = MyDecorateProductCartLogic::read($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('添加成功');
        $this->addRenderData('product_info', $result, false);

        return $this->getRenderJson();
    }

    public function collect(){
        $this->validateToken();

        $requestData = $this->selectParam(['decorate_product_id', 'type' => '1']);
        $this->check($requestData, ['decorate_product_id' => 'require', 'type' => 'require']);
        $requestData['user_id'] = $this->getUserId();


        $result = MyDecorateProductCartLogic::collect($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('操作成功');

        return $this->getRenderJson();
    }


}