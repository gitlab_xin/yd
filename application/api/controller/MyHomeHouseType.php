<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 16:02
 * 户型
 */

namespace app\api\controller;

use app\api\logic\MyHomeBuildingsLogic;
use app\api\logic\MyHomeHouseTypeLogic;
use app\common\model\MyHomeHouseType as HouseModel;

class MyHomeHouseType extends Base
{

    /**
     * @author:Airon
     * 上传/编辑 户型模型
     * @return \think\response\Json
     */
    public function insertHouseType()
    {
        $requestData = $this->selectParam(['house_id', 'name', 'type', 'obj_url', 'mtl_url', 'size', 'position', 'scale', 'rotation', 'intro', 'render_img', 'texture']);
        $this->check($requestData, 'MyHomeHouseType.insertMode');
        $check = MyHomeHouseTypeLogic::checkHouseType($requestData['house_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }
        $result = MyHomeHouseTypeLogic::insertHouseType($requestData['house_id'], $requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('上传成功');
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * 获取家具模型详情
     * @return \think\response\Json
     */
    public function getHouseTypeInfo()
    {
        $requestData = $this->selectParam(['house_id']);
        $this->check($requestData, 'MyHomeHouseType.getInfo');
        $result = MyHomeHouseTypeLogic::getHouseTypeModel($requestData['house_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $result = json_decode($result, true);
        $this->addRenderData('info', $result, false);
        $this->setRenderMessage('获取成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:上传装修方案
     * @return \think\response\Json
     */
    public function insertDecorateProgram()
    {
        $requestData = $this->selectParam(['house_id', 'name', 'img_src', 'serial_array']);
        $this->check($requestData, 'MyHomeDecorateProgram.insert');
        $check = MyHomeHouseTypeLogic::checkHouseType($requestData['house_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }

        $requestData['serial_array'] = json_encode($requestData['serial_array'], JSON_UNESCAPED_UNICODE);
        $result = MyHomeHouseTypeLogic::insertDecorateProgram($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('上传成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:编辑装修方案
     * @return \think\response\Json
     */
    public function saveDecorateProgram()
    {
        $requestData = $this->selectParam(['program_id', 'name', 'img_src', 'serial_array']);
        $this->check($requestData, 'MyHomeDecorateProgram.save');

        $requestData['serial_array'] = json_encode($requestData['serial_array'], JSON_UNESCAPED_UNICODE);
        $result = MyHomeHouseTypeLogic::saveDecorateProgram($requestData['program_id'], $requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('编辑失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('编辑成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案详情
     * @return \think\response\Json
     */
    public function getDecorateProgram()
    {
        $requestData = $this->selectParam(['program_id']);
        $this->check($requestData, 'MyHomeDecorateProgram.getInfo');
        $result = MyHomeHouseTypeLogic::getDecorateProgram($requestData['program_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $result['serial_array'] = json_decode($result['serial_array'], JSON_UNESCAPED_UNICODE);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案列表
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getDecorateProgramList()
    {
        $requestData = $this->selectParam(['house_id' => 0]);
        $this->check($requestData, 'MyHomeHouseType.getInfo');
        $where = empty($requestData['house_id']) ? [] : ['house_id' => $requestData['house_id']];
        $result = MyHomeHouseTypeLogic::getDecorateProgramList($this->pageIndex, $this->pageSize, $where);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('获取成功');
        $this->addRenderData('program_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型详情
     * @return \think\response\Json
     */
    public function getInfo()
    {
        $requestData = $this->selectParam(['house_id', 'buildings_id']);
        $this->check($requestData, 'MyHomeHouseType.getInfo');
        $check = MyHomeHouseTypeLogic::checkHouseType($requestData['house_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }
        $house_info = MyHomeHouseTypeLogic::getInfo($requestData['house_id']);
        if ($house_info === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $decorate_list = MyHomeHouseTypeLogic::getHouseDecorate($requestData['house_id']);
        if ($decorate_list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        if (!empty($requestData['buildings_id'])) {
            $buildings_info = MyHomeBuildingsLogic::getInfo($requestData['buildings_id']);
            if ($decorate_list === false) {
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                return $this->getRenderJson();
            }
        }
        $house_info['decorate_list'] = $decorate_list;
        $this->addRenderData('house_type_list', $house_info, false);
        $this->addRenderData('buildings_info', $buildings_info, false);
        return $this->getRenderJson();
    }


    public function updateEnvironment()
    {
        $requestData = $this->selectParam(['house_id', 'data', 'type']);
        $requestData['environment_type'] = $requestData['type'];
        $this->check($requestData, 'MyHomeHouseType.updateEnvironment');
        $info = HouseModel::build()->where(['house_id' => $requestData['house_id']])->field(['environment_array'])->find();
        if (empty($info)) {
            $this->setRenderCode(402);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }
        $environment_array = $info['environment_array'];
        $environment_array = json_decode($environment_array, true);
        $environment_array[$requestData['type']] = $requestData['data'];
        $bool = HouseModel::build()
            ->update(['environment_array' => json_encode($environment_array, JSON_UNESCAPED_UNICODE)], ['house_id' => $requestData['house_id']]);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        return $this->getRenderJson();
    }

    public function getEnvironment(){
        $requestData = $this->selectParam(['house_id', 'type']);
        $requestData['environment_type'] = $requestData['type'];
        $this->check($requestData, 'MyHomeHouseType.getEnvironment');
        $info = HouseModel::build()->where(['house_id' => $requestData['house_id']])->field(['environment_array'])->find();
        if (empty($info)) {
            $this->setRenderCode(402);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }
        $environment_array = $info['environment_array'];
        $environment_array = json_decode($environment_array, true);
        $this->setRenderCode(200);
        $this->addRenderData("data", $environment_array[$requestData['type']] ,false);
        return $this->getRenderJson();
    }

    public function get3DByCabinet()
    {
        $requestData = $this->selectParam(['house_id']);
        $this->check($requestData, 'MyHomeHouseType.getInfo');
        $result = MyHomeHouseTypeLogic::get3DByCabinet($requestData['house_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('data3D', $result, false);
        return $this->getRenderJson();
    }
}