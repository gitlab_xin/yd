<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\controller;

use app\api\logic\SchemeQWLogic;
use app\api\logic\SchemeQWOrderLogic;
use app\common\model\CustomizedColor;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

class SchemeQW extends Base
{
    public function step1()
    {
        $requestParam = $this->selectParam([
            'width' => '2027',
            'height' => '2560',
        ]);
        $this->check($requestParam, "schemeQW.create");
        $logic = new SchemeQWLogic();
        $productList = $logic->createQWScheme($requestParam);

        $this->addRenderData('schemes', $productList, false);
        return $this->getRenderJson();
    }

    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme']);

        $schemeData = $requestData['scheme'];
        $schemeLogic = new SchemeQWLogic();

        $originParams = $schemeLogic->getOriginParams($schemeData);

        $this->check($originParams, 'SchemeQW.store');
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $checkResult = $schemeLogic->checkScheme($schemeBean);
        if (!$checkResult) {
            $this->setRenderCode(500);
            $this->setRenderMessage("保存失败, 数据非法");
            return $this->getRenderJson();
        }
        // 添加缓冲器和门
        $schemeId = $schemeLogic->store($schemeBean, $schemeData, $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'scheme_id']);
        $user_id = $this->getUserId();
        $schemeData = $requestData['scheme'];
        $schemeLogic = new SchemeQWLogic();

        $originParams = $schemeLogic->getOriginParams($schemeData);
        $originParams['user_id'] = $user_id;
        $originParams['scheme_id'] = $requestData['scheme_id'];

        $this->check($originParams, 'SchemeQW.update');
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $checkResult = $schemeLogic->checkScheme($schemeBean);
        if (!$checkResult) {
            $this->setRenderCode(500);
            $this->setRenderMessage("修改失败, 数据非法");
            return $this->getRenderJson();
        }

        $schemeLogic->update($schemeBean, $schemeData, $requestData['scheme_id'], $this->getUserId());

        $this->setRenderMessage('修改成功');
        $this->addRenderData("id", $requestData['scheme_id']);

        return $this->getRenderJson();
    }

    public function complete()
    {
        $this->validateToken();

        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'SchemeQW.complete');


        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);

        $schemeLogic = new SchemeQWLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeQWOrderLogic();
        $schemeBean = $orderLogic->scoreQWSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreQWSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }
        $doorProducts = [];
        foreach ($schemeBean->getScheme_score_door_products() as $item) {
            $item = change2ProductBean($item);
            $doorProducts[] = $item->scoreInfo();
        }

        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('scheme_name', $schemeBean->getScheme_name(), false);
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('door_area', $schemeBean->getScheme_door_area());
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('s_door_area', $schemeBean->getScheme_s_door_area());
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($schemeBean->getScheme_dis_price(), 2)]);

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();

        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'SchemeQW.complete2');


        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);

        $schemeLogic = new SchemeQWLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeQWOrderLogic();
        $schemeBean = $orderLogic->scoreQWSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreQWSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreQWSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }
        $doorProducts = [];
        foreach ($schemeBean->getScheme_score_door_products() as $item) {
            $item = change2ProductBean($item);
            $doorProducts[] = $item->scoreInfo();
        }

        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('scheme_name', $schemeBean->getScheme_name(), false);
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('door_area', $schemeBean->getScheme_door_area());
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('s_door_area', $schemeBean->getScheme_s_door_area());
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}
