<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/11
 * Time: 14:32
 */

namespace app\api\controller;

use app\api\logic\UserLogic;
use app\api\logic\SmsLogic;

class Sms extends Base
{
    /**
     * 发送验证码短信
     * @requestExample {"mobile": "13112341234", "sms_type": "reg"}
     * @responseExample {"code":200,"message":"短信发送成功","data":{}}
     * @return \think\response\Json
     */
    public function sendSms()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['mobile', 'sms_type' => 'reg']);
        $this->check($requestData, 'Base.sendSms');
        # 验证账户有效性
        $userModel = UserLogic::check_user($requestData['mobile']);
        switch ($requestData['sms_type']) {
            case 'reg':
            case 'bind':
                if (!empty($userModel)) {
                    $this->setRenderCode(400);
                    $this->setRenderMessage('该手机号已被注册');
                    return $this->getRenderJson();
                }
                break;
            case 'pass':
                if (empty($userModel)) {
                    $this->setRenderCode(400);
                    $this->setRenderMessage('该手机号还没注册');
                    return $this->getRenderJson();
                }
                break;
        }
        # 发送短信 记录发送
        $sendResult = SmsLogic::sendSms($requestData['mobile'], $requestData['sms_type']);
        if (!$sendResult) {
            $this->setRenderCode(500);
            $this->setRenderMessage('短信发送失败，请稍后再试');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('短信发送成功');
        return $this->getRenderJson();
    }

    /**
     * 验证短信验证码
     * @requestExample {"mobile": "13112341234", "sms_type": "reg", "code": "1386"}
     * @responseExample {"code":200,"message":"验证成功","data":{}}
     * @return \think\response\Json
     */
    public function validateCode()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['mobile', 'sms_type', 'code']);
        $this->check($requestData, 'Base.validateCode');

        # 验证验证码
        $validateResult = SmsLogic::validateCode($requestData);
        if (!$validateResult) {
            $this->setRenderCode(400);
            $this->setRenderMessage('验证码错误');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('验证成功');
        return $this->getRenderJson();
    }
}