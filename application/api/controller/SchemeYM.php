<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/24
 * Time: 14:48
 */

namespace app\api\controller;

use app\common\tools\RedisUtil;
use app\api\logic\SchemeYMLogic;
use app\api\logic\SchemeYMOrderLogic;

class SchemeYM extends Base
{
    public function create()
    {
        $data = $this->selectParam(['hole_type', 'hole_width', 'hole_height', 'door_count', 'sk_color_no']);

        $this->check($data, 'SchemeYM.create');
        $logic = new SchemeYMLogic();

        $scheme = $logic->createYMScheme($data);

        $this->addRenderData('scheme', $scheme->mainInfo(), false);

        return $this->getRenderJson();
    }

    public function store()
    {
        $this->validateToken();
        $data = $this->selectParam(['hole_type', 'hole_width', 'hole_height', 'door_count', 'sk_color_no', 'door_color_no', 'door_hcq', 'scheme_name', 'scheme_pic']);

        $this->check($data, 'SchemeYM.store');
        $logic = new SchemeYMLogic();

        $scheme = $logic->createYMScheme($data);

        $scheme_id = $logic->store($scheme, $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $scheme_id);

        return $this->getRenderJson();
    }

    public function update()
    {
        $this->validateToken();
        $data = $this->selectParam(['scheme_id', 'door_color_no', 'door_hcq', 'scheme_name', 'scheme_pic']);
        $data['user_id'] = $this->getUserId();

        $this->check($data, 'SchemeYM.update');
        $logic = new SchemeYMLogic();

        $scheme_id = $logic->update($data);

        if ($scheme_id) {
            $this->setRenderMessage('修改成功');
            $this->addRenderData("id", $scheme_id);

        }else{
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');

        }

        return $this->getRenderJson();
    }

    public function complete()
    {
        $this->validateToken();
        $data = $this->selectParam(['scheme_id']);
        $data['user_id'] = $this->getUserId();

        $this->check($data, 'SchemeYM.complete');

        $logic = new SchemeYMLogic();
        $orderLogic = new SchemeYMOrderLogic();

        $schemeBean = $logic->buildScheme($data['scheme_id']);

        $schemeBean = $orderLogic->scoreYMSchemeWujin($schemeBean);
        $orderLogic->scoreYMSchemeDoor($schemeBean);
        $orderLogic->scoreYMSchemeShouKou($schemeBean);

        $doorProducts = [];
        if (is_array($schemeBean->getScheme_score_door_products())) {
            foreach ($schemeBean->getScheme_score_door_products() as $item) {
                $item = change2ProductBean($item);
                $doorProducts[] = $item->scoreInfo();
            }
        }
        $shouKouProducts = [];
        if (is_array($schemeBean->getScheme_score_sk_products())) {
            foreach ($schemeBean->getScheme_score_sk_products() as $item) {
                $item = change2ProductBean($item);
                $shouKouProducts[] = $item->scoreInfo();
            }
        }

        //五金附件明细
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        $this->addRenderData('scheme_name', $schemeBean->getScheme_name());
        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('shoukou_products', $shouKouProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('gt_price', 0);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('shoukou_price', formatMoney($schemeBean->getScheme_sk_price(), 2));
        $this->addRenderData('dis_gt_price', 0.00);
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price() ,2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price() ,2));
        $this->addRenderData('dis_shoukou_price', formatMoney($schemeBean->getScheme_dis_sk_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $data['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }


    public function complete2()
    {
//        $this->validateToken();
        $data = $this->selectParam(['scheme_id']);
//        $data['user_id'] = $this->getUserId();

        $this->check($data, 'SchemeYM.complete2');

        $logic = new SchemeYMLogic();
        $orderLogic = new SchemeYMOrderLogic();

        $schemeBean = $logic->buildScheme($data['scheme_id']);

        $schemeBean = $orderLogic->scoreYMSchemeWujin($schemeBean);
        $orderLogic->scoreYMSchemeDoor($schemeBean);
        $orderLogic->scoreYMSchemeShouKou($schemeBean);

        $doorProducts = [];
        if (is_array($schemeBean->getScheme_score_door_products())) {
            foreach ($schemeBean->getScheme_score_door_products() as $item) {
                $item = change2ProductBean($item);
                $doorProducts[] = $item->scoreInfo();
            }
        }
        $shouKouProducts = [];
        if (is_array($schemeBean->getScheme_score_sk_products())) {
            foreach ($schemeBean->getScheme_score_sk_products() as $item) {
                $item = change2ProductBean($item);
                $shouKouProducts[] = $item->scoreInfo();
            }
        }

        //五金附件明细
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        $this->addRenderData('scheme_name', $schemeBean->getScheme_name());
        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('shoukou_products', $shouKouProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('gt_price', 0);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('shoukou_price', formatMoney($schemeBean->getScheme_sk_price(), 2));
        $this->addRenderData('dis_gt_price', 0.00);
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price() ,2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price() ,2));
        $this->addRenderData('dis_shoukou_price', formatMoney($schemeBean->getScheme_dis_sk_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $data['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}