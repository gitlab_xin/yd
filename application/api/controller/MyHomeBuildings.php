<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 16:02
 * 户型
 */

namespace app\api\controller;

use app\api\logic\MyHomeBuildingsLogic;

class MyHomeBuildings extends Base
{

    public function getBuildingsList()
    {
        $requestData = $this->selectParam(['province', 'city', 'user_id' => 0]);
        $this->check($requestData, 'MyHomeBuildings.getList');
        $list = MyHomeBuildingsLogic::getBuildingsList($requestData['user_id'], $requestData['province'], $requestData['city'], $this->pageIndex, $this->pageSize);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function lists(){
        $requestData = $this->selectParam(['city', 'keyword']);
        $this->check($requestData, 'MyHomeBuildings.getList2');
        $list = MyHomeBuildingsLogic::getBuildingsList2($requestData, $this->pageIndex, $this->pageSize);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function alls(){
        $requestData = $this->selectParam(['buildings_id','id' => ""]);
        $this->check($requestData, 'MyHomeBuildings.getInfo');
        $list = MyHomeBuildingsLogic::alls($requestData, $this->pageIndex, $this->pageSize);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function getUnitDecorates(){
        $requestData = $this->selectParam(['id']);
        $this->check($requestData, 'MyHomeBuildings.getUnitDecorates');
        $list = MyHomeBuildingsLogic::getUnitDecorates($requestData, $this->pageIndex, $this->pageSize);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function getHouseDecorates(){
        $requestData = $this->selectParam(['house_id','config_id' => '']);
        $this->check($requestData, 'MyHomeBuildings.getInfo2');
        $list = MyHomeBuildingsLogic::getHouseDecorates($requestData['house_id'], $this->pageIndex, $this->pageSize, $requestData['config_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function getDecoratesInfo(){
        $requestData = $this->selectParam(['program_id']);
        $this->check($requestData, 'MyHomeBuildings.getInfo3');
        $list = MyHomeBuildingsLogic::getDecoratesInfo($requestData['program_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function getBuildingsInfo(){
        $requestData = $this->selectParam(['buildings_id']);
        $this->check($requestData, 'MyHomeBuildings.getInfo');
        $list = MyHomeBuildingsLogic::getBuildingsInfo($requestData);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    public function searchBuildings()
    {
        $requestData = $this->selectParam(['keyword', 'user_id' => 0]);
        $this->check($requestData, 'MyHomeBuildings.search');
        $list = MyHomeBuildingsLogic::searchBuildings($requestData['user_id'], $requestData['keyword'], $this->pageIndex, $this->pageSize);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('buildings_list', $list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 收藏和取消收藏
     * @return \think\response\Json
     */
    public function buildingsCollect()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['buildings_id', 'collect_type']);

        $this->check($requestData, 'MyHomeBuildings.collect');

        $result = MyHomeBuildingsLogic::buildingsCollect($user_id, $requestData);

        $msg = $requestData['collect_type'] == 'collect' ? '收藏' : '取消收藏';

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage($msg . '失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage($msg . '成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取栋
     * @return \think\response\Json
     */
    public function getRidgepole()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['buildings_id']);
        $this->check($requestData, 'MyHomeBuildings.getRidgepole');
        $list = MyHomeBuildingsLogic::getRidgepole($requestData['buildings_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('ridgepole_list', $list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取楼层
     * @return \think\response\Json
     */
    public function getFloor()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['buildings_id', 'ridgepole_id']);
        $this->check($requestData, 'MyHomeBuildings.getFloor');
        $list = MyHomeBuildingsLogic::getFloor($requestData['buildings_id'], $requestData['ridgepole_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('floor_list', $list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取单元
     * @return \think\response\Json
     */
    public function getUnit()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['buildings_id', 'floor_id']);
        $this->check($requestData, 'MyHomeBuildings.getUnit');
        $list = MyHomeBuildingsLogic::getUnit($requestData['buildings_id'], $requestData['floor_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('unit_list', $list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取户型列表
     * @return \think\response\Json
     */
    public function getHouseType()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['buildings_id', 'ridgepole_id' => 0, 'floor_id' => 0, 'unit_id' => 0]);
        $this->check($requestData, 'MyHomeBuildings.getUnit');
        $list = MyHomeBuildingsLogic::getHouseType($requestData['buildings_id'], $requestData['ridgepole_id'], $requestData['floor_id'], $requestData['unit_id']);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('house_type_list', $list, false);
        return $this->getRenderJson();
    }

    /**
     * User: zhaoxin
     * Date: 2019/11/27
     * Time: 10:17 上午
     * description 获取我收藏的楼盘
     * @return \think\response\Json
     */
    public function getMyBuildingCollections()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = MyHomeBuildingsLogic::getMyBuildingCollections($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('buildings', $result['buildings'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    public function getProductList(){
        $data = $this->selectParam(['room_id' => '','program_id']);
        $this->check($data, ['program_id' => 'require']);
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = MyHomeBuildingsLogic::getProductList($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('product_info', $result, false);
        return $this->getRenderJson();
    }
}