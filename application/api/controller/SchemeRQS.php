<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\controller;

use app\api\logic\SchemeRQSLogic;
use app\api\logic\SchemeRQSOrderLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

class SchemeRQS extends Base
{
    public function step1()
    {
        $requestParam = $this->selectParam([
            'hole_width' => '2500',
            'hole_height' => '2410',
            'sl_height' => '2410',
            'b_width' => '0',
            'c_width' => '0',
            'hole_type' => '',
            'door_count' => '0',
            'sk_color_no' => '000'
        ]);
        $this->check($requestParam, 'SchemeRQS.create');

        $logic = new SchemeRQSLogic();

        $productList = $logic->createQRSScheme($requestParam);

        $this->addRenderData('schemes', $productList, false);
        return $this->getRenderJson();
    }

    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'door_color_id' => 0, 'scheme_door_have_hcq' => 0]);
        $schemeData = array_shift($requestData);

        $schemeLogic = new SchemeRQSLogic();

        $originParams = $schemeLogic->getOriginParams($schemeData);

        $this->check(array_merge($originParams, $requestData), 'SchemeRQS.store');

        $schemeBean = $schemeLogic->buildScheme($schemeData, $requestData);
        $checkResult = $schemeLogic->checkScheme($schemeBean);

        if ($checkResult !== true) {
            $this->setRenderCode(500);
            $this->setRenderMessage("保存失败，" . $checkResult);
            return $this->getRenderJson();
        }

        $schemeId = $schemeLogic->store($schemeBean, $schemeData, $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'door_color_id' => 0, 'scheme_door_have_hcq' => 0, 'scheme_id']);
        $schemeData = array_shift($requestData);
        $user_id = $this->getUserId();

        $schemeLogic = new SchemeRQSLogic();

        $originParams = $schemeLogic->getOriginParams($schemeData);
        $originParams['user_id'] = $user_id;

        $this->check(array_merge($originParams, $requestData), 'SchemeRQS.update');

        $schemeBean = $schemeLogic->buildScheme($schemeData, $requestData);
        $checkResult = $schemeLogic->checkScheme($schemeBean);

        if ($checkResult !== true) {
            $this->setRenderCode(500);
            $this->setRenderMessage("修改失败，" . $checkResult);
            return $this->getRenderJson();
        }

        $schemeLogic->update($schemeBean, $schemeData, $requestData['scheme_id'], $user_id);

        $this->setRenderMessage('修改成功');
        $this->addRenderData("id", $requestData['scheme_id']);

        return $this->getRenderJson();
    }

    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeRQS.complete');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);

        $schemeLogic = new SchemeRQSLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeRQSOrderLogic();
        $schemeBean = $orderLogic->scoreRQSSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreRQSSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }

        $schemeBean = $orderLogic->scoreRQSSchemeShouKou($schemeBean);


        //柜体构件明细 -> 构件清单
        $panelProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $panelProducts[] = $item->scoreInfo();
        }
        //柜体构件明细 -> 用料明细
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }

        //功能组件明细
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }

        //五金附件明细
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        //滑动门明细 -> 材料清单
        $doorProducts = [];
        if (is_array($schemeBean->getScheme_score_door_products())) {
            foreach ($schemeBean->getScheme_score_door_products() as $item) {
                $item = change2ProductBean($item);
                $doorProducts[] = $item->scoreInfo();
            }
        }
        //滑动门明细 -> 用料明细
        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        //收口明细
        $shouKouProducts = [];
        if (is_array($schemeBean->getScheme_score_sk_products())) {
            foreach ($schemeBean->getScheme_score_sk_products() as $item) {
                $item = change2ProductBean($item);
                $shouKouProducts[] = $item->scoreInfo();
            }
        }


        $this->addRenderData('scheme_name', $schemeBean->getScheme_name());
        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $panelProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('shoukou_products', $shouKouProducts, false);
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('door_mat_price', formatMoney($schemeBean->getScheme_door_mat_price(), 2));
        $this->addRenderData('dis_door_mat_price', formatMoney($schemeBean->getScheme_dis_door_mat_price(),2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price() ,2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price() ,2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price() ,2));
        $this->addRenderData('shoukou_price', formatMoney($schemeBean->getScheme_sk_price() ,2));
        $this->addRenderData('dis_shoukou_price', formatMoney($schemeBean->getScheme_dis_sk_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('plate_dis_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($schemeBean->getScheme_dis_price(), 2)]);

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeRQS.complete2');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);

        $schemeLogic = new SchemeRQSLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeRQSOrderLogic();
        $schemeBean = $orderLogic->scoreRQSSchemeZJProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeProducts($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemePlates($schemeBean);
        $schemeBean = $orderLogic->scoreRQSSchemeWujin($schemeBean);

        if ($schemeBean->getScheme_have_door()) {
            $schemeBean = $orderLogic->scoreRQSSchemeDoor($schemeBean);
        } else {
            $schemeBean->setScheme_door_price(0);
            $schemeBean->setScheme_dis_door_price(0);
            $schemeBean->setScheme_door_area(0);
            $schemeBean->setScheme_score_door_products(null);
        }

        $schemeBean = $orderLogic->scoreRQSSchemeShouKou($schemeBean);


        //柜体构件明细 -> 构件清单
        $panelProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $panelProducts[] = $item->scoreInfo();
        }
        //柜体构件明细 -> 用料明细
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }

        //功能组件明细
        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }

        //五金附件明细
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        //滑动门明细 -> 材料清单
        $doorProducts = [];
        if (is_array($schemeBean->getScheme_score_door_products())) {
            foreach ($schemeBean->getScheme_score_door_products() as $item) {
                $item = change2ProductBean($item);
                $doorProducts[] = $item->scoreInfo();
            }
        }
        //滑动门明细 -> 用料明细
        $doorInfo = [
            'door_spec' => $schemeBean->getScheme_door_spec(),
            's_door_area' => $schemeBean->getScheme_s_door_area(),
            'o_door_price' => formatMoney($schemeBean->getScheme_o_door_price()),
            'door_has_count' => $schemeBean->getScheme_door_has_count(),
            'door_area' => $schemeBean->getScheme_door_area(),
            's_door_price' => formatMoney($schemeBean->getScheme_s_door_price()),
            'door_price' => formatMoney($schemeBean->getScheme_door_price() - $schemeBean->getScheme_door_mat_price()),
            'dis_door_price' => formatMoney($schemeBean->getScheme_dis_door_price()),
        ];

        //收口明细
        $shouKouProducts = [];
        if (is_array($schemeBean->getScheme_score_sk_products())) {
            foreach ($schemeBean->getScheme_score_sk_products() as $item) {
                $item = change2ProductBean($item);
                $shouKouProducts[] = $item->scoreInfo();
            }
        }


        $this->addRenderData('scheme_name', $schemeBean->getScheme_name());
        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $panelProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('door_products', $doorProducts, false);
        $this->addRenderData('shoukou_products', $shouKouProducts, false);
        $this->addRenderData('door', $doorInfo, false);
        $this->addRenderData('door_price', formatMoney($schemeBean->getScheme_door_price(), 2));
        $this->addRenderData('dis_door_price', formatMoney($schemeBean->getScheme_dis_door_price(), 2));
        $this->addRenderData('door_mat_price', formatMoney($schemeBean->getScheme_door_mat_price(), 2));
        $this->addRenderData('dis_door_mat_price', formatMoney($schemeBean->getScheme_dis_door_mat_price(),2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price() ,2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price() ,2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price() ,2));
        $this->addRenderData('shoukou_price', formatMoney($schemeBean->getScheme_sk_price() ,2));
        $this->addRenderData('dis_shoukou_price', formatMoney($schemeBean->getScheme_dis_sk_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('plate_dis_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}
