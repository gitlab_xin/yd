<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 16:25
 */

namespace app\api\controller;

use app\api\logic\WorksCommentLogic as CommentLogic;

class WorksComment extends Base
{

    public $commentLogic;

    public function __construct()
    {
        parent::__construct();
        $this->commentLogic = new CommentLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:
     * @return \think\response\Json
     */
    public function getWorksComments()
    {
        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['works_id']);
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;

        $this->check($requestData, 'WorksComment.commentList');

        $result = $this->commentLogic->getComments($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('comments_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:回复帖子
     * @return \think\response\Json
     *
     */
    public function replyWorks()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['works_id','by_user_id','parent_comment_id','by_comment_id','content']);
        $requestData['user_id'] = $user_id;

        // 上级评论id
        $requestData['parent_comment_id'] = empty($requestData['parent_comment_id']) ? 0 : $requestData['parent_comment_id'];
        // 被回复user_id
        $requestData['by_user_id'] = empty($requestData['by_user_id']) ? 0 : $requestData['by_user_id'];
        // 被回复comment_id
        $requestData['by_comment_id'] = empty($requestData['by_comment_id']) ? 0 : $requestData['by_comment_id'];

        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;

        $this->check($requestData, 'WorksComment.reply');

        $result = $this->commentLogic->addComment($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('回复失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('回复成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description:获取一级回复详情
     * @return \think\response\Json
     *
     */
    public function getCommentDetail()
    {
        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['comment_id']);

        $this->check($requestData, 'WorksComment.commentDetail');

        $result = $this->commentLogic->getCommentDetail($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('comment_detail', $result, false);
        return $this->getRenderJson();
    }
}