<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/18
 * Time: 11:27
 */

namespace app\api\controller;


use app\api\logic\ShopClassifyLogic;
use app\api\logic\ShopSupplierLogic;

class ShopClassify extends Base
{
    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取一级分类列表
     */
    public function parent()
    {
        $result = ShopClassifyLogic::selectLevelOne();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('parent', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取二三级分类列表 包括广告位图
     * @return \think\response\Json
     */
    public function child()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['classify_id']);
        $this->check($requestData, 'Classify.classify_id');

        $bool = ShopClassifyLogic::checkIsParent($requestData);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分类ID错误');
            return $this->getRenderJson();
        }
        $banner_list = ShopClassifyLogic::bannerList($requestData);
        $this->addRenderData('banner_list', $banner_list, false);

        $classify_list = ShopClassifyLogic::appChild($requestData);
        $this->addRenderData('classify_list', $classify_list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取一二级分类列表或获取二三级分类列表
     * @return \think\response\Json
     */
    public function webChild()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['classify_id']);
        $requestData['classify_id'] = empty($requestData['classify_id']) ? 0 : $requestData['classify_id'];
        $this->check($requestData, 'Classify.classify_id');

        if ($requestData['classify_id'] !== 0) {
            $bool = ShopClassifyLogic::checkIsParent($requestData);
            if ($bool === false) {
                $this->setRenderCode(500);
                $this->setRenderMessage('分类ID错误');
                return $this->getRenderJson();
            }
        }

        $classify_list = ShopClassifyLogic::webChild($requestData['classify_id']);
        $this->addRenderData('classify_list', $classify_list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:根据一级分类获取所有相关品牌
     * @return \think\response\Json
     */
    public function supplier()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['classify_id']);
        $this->check($requestData, 'Classify.classify_id');

        $bool = ShopClassifyLogic::checkIsParent($requestData);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分类ID错误');
            return $this->getRenderJson();
        }

        $supplier_list = ShopSupplierLogic::classify($requestData['classify_id']);
        $this->addRenderData('supplier_list', $supplier_list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:根据上级分类获取下级分类
     * @return \think\response\Json
     */
    public function getChild()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['classify_id']);
        $requestData['classify_id'] = empty($requestData['classify_id']) ? 0 : $requestData['classify_id'];
        $this->check($requestData, 'Classify.classify_id');


        $classify_list = ShopClassifyLogic::getChild($requestData['classify_id']);
        if ($classify_list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('分类ID错误');
            return $this->getRenderJson();
        }

        $this->addRenderData('classify_list', $classify_list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月10日
     * description:根据商品id获取分类名称
     * @return \think\response\Json
     */
    public function getClassifyByProductId()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['product_id']);
        $this->check($requestData, 'Classify.classifyNames');


        $classify_list = ShopClassifyLogic::getClassifyNames($requestData['product_id']);
        if ($classify_list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('商品ID错误');
            return $this->getRenderJson();
        }

        $this->addRenderData('classify_list', $classify_list, false);
        return $this->getRenderJson();
    }
}