<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 9:47
 */

namespace app\api\controller;

use app\api\logic\DemandLogic;
use app\api\logic\DemandOrderLogic;
use app\api\logic\FeedbackLogic;
use app\api\logic\UserDesignerLogic;
use app\api\logic\UserLogic;
use app\api\logic\ShopProductLogic;
use app\api\logic\WithdrawLogic;
use app\common\tools\JPush;
use app\common\tools\kdniaoUtil;

class User extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:修改头像
     * @return \think\response\Json
     */
    public function changeAvatar()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['avatar']);

        $this->check($requestData, 'User.avatar');

        $requestData['user_id'] = $user_id;
        $result = UserLogic::editInfo($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('修改成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:验证密码
     * @return \think\response\Json
     */
    public function validatePassword()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['password']);

        $this->check($requestData, 'User.password');

        $requestData['user_id'] = $user_id;
        $result = UserLogic::get_info($requestData, 'password');

        if (!isset($result['user_id']) || empty($result['user_id'])) {
            $this->setRenderCode(500);
            $this->setRenderMessage('密码错误');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('密码正确');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:修改密码
     * @return \think\response\Json
     *
     */
    public function changePassword()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['password']);

        $this->check($requestData, 'User.password');

        $requestData['user_id'] = $user_id;
        $result = UserLogic::editInfo($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('修改成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:修改昵称
     * @return \think\response\Json
     */
    public function changeUsername()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['username']);

        $this->check($requestData, 'User.username');

        $requestData['user_id'] = $user_id;
        $result = UserLogic::editInfo($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('修改成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取用户信息
     * @return \think\response\Json
     *
     */
    public function getUserInfo()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData['user_id'] = $user_id;
        $result = UserLogic::get_info($requestData,
            ['user_id', 'username', 'mobile', 'avatar', 'email', 'money', 'gender', 'is_designer as designer_status', 'reg_time']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        UserDesignerLogic::checkStatus($result);
        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('user_info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:获取我的收藏商品
     * @return \think\response\Json
     */
    public function getMyProductCollections()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = ShopProductLogic::getMyCollections($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('products', $result['collects'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:提现操作
     * @return \think\response\Json
     */
    public function withdraw()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['money', 'account_name', 'bank', 'card_num']);
        $data['user_id'] = $this->getUserId();

        $this->check($data, 'Withdraw.withdraw');
        $result = WithdrawLogic::withdraw($data);

        switch ($result) {
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('余额不足');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('提现申请失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->addRenderData('money', $result, false);
                $this->setRenderMessage('提现申请成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:我的提现记录
     * @return \think\response\Json
     */
    public function getMyWithdrawRecord()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;
        $data['user_id'] = $this->getUserId();

        $result = WithdrawLogic::getWithdrawByUser($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('record', $result, false);

        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益记录
     * @return \think\response\Json
     */
    public function getMyBenefit()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $result = WithdrawLogic::getMyBenefit($user_id);
        $sum = WithdrawLogic::getBenefitSum(['user_id' => $user_id, 'change_type' => 1]);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('record', $result, false);
            $this->addRenderData('sum', $sum, false);

        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益明细
     * @return \think\response\Json
     */
    public function getMyBenefitDetail()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['type']);
        $this->check($data, 'withdraw.flow');
        $data['user_id'] = $this->getUserId();


        $result = WithdrawLogic::getMyBenefitDetail($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');

        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取我的收益明细
     * @return \think\response\Json
     */
    public function getMyBenefitDetailWeb()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $data = ['user_id' => $user_id, 'change_type' => 1];
        $result = WithdrawLogic::getMyBenefitWeb($data);
        $sum = WithdrawLogic::getBenefitSum($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('record', $result, false);
            $this->addRenderData('sum', $sum, false);

        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:我的提现记录
     * @return \think\response\Json
     */
    public function getMyWithdrawDetail()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['withdraw_id']);
        $this->check($data, 'Withdraw.detail');
        $data['user_id'] = $this->getUserId();

        $result = WithdrawLogic::getWithdrawDetail($data);

        if ($result === null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('查询不到相应的记录');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('detail', $result, false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月21日
     * description:设置极光推送 对应设备ID
     */
    public function set_push_id()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();
        $data = $this->selectParam(['push_id', 'device_type']);
        $this->check($data, 'user.push');

        $bool = UserLogic::set_push_id($user_id, $data['push_id'], $data['device_type']);
        if ($bool) {
            $this->setRenderCode(200);
            $this->setRenderMessage('设置成功');
        } else {
            $this->setRenderCode(500);
            $this->setRenderMessage('设置失败');
        }
        return $this->getRenderJson();
    }

    public function demo()
    {
        exit;
        //极光推送DEMO
        $appKey = config('jpush')['AppKey'];
        $Secret = config('jpush')['MasterSecret'];
        $jpush = new JPush($appKey, $Secret);

        //$bool = $jpush->push('140fe1da9e9ed5c561d', 'android', '测试', array('data' => '123', 'aaa' => '321'));//单发
        $bool = $jpush->allPush('abc',array('data' => '123', 'aaa' => '321'));
        dump($bool);exit;
        $jpush->temp();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月21日
     * description:反馈意见,无论如何只返回成功
     * @return \think\response\Json
     */
    public function feedback()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['content']);
        $data['user_id'] = $this->getUserId();
        $this->check($data, 'Feedback.feedback');
        FeedbackLogic::feedback($data);

        $this->setRenderCode(200);
        $this->setRenderMessage('反馈成功');
        return $this->getRenderJson();

    }

    /**
     * @author: Rudy
     * @time: 2017年8月29日
     * description:申请成为设计师
     * @return \think\response\Json
     */
    public function toBeDesigner()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam([ 'name','identity_num','identity_front_src','identity_back_src','certificate_src','desc']);
        $data['user_id'] = $this->getUserId();
        $this->check($data, 'UserDesigner.add');
        $result = UserDesignerLogic::addDesigner($data);
        switch ($result){
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('您已经是设计师或在审核流程中');
                break;
            case 1:
                $this->setRenderCode(200);
                $this->setRenderMessage('提交申请成功');
                break;
            default:
                $this->setRenderCode(500);
                $this->setRenderMessage('提交申请失败');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月29日
     * description:获取我的需求列表
     * @return \think\response\Json
     */
    public function getMyDemandList()
    {
        $this->validateToken();

        $data = $this->selectParam(['show_status']);

        $this->check($data,'Demand.list');
        $data['show_status'] = empty($data['show_status'])?1:$data['show_status'];
        $data['user_id'] = $this->getUserId();

        $result = DemandLogic::getListByUser($this->getUserId(),$data['show_status'],$this->pageIndex,$this->pageSize);


        if($result === false){
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        }else{
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('lists',$result,false);
            $this->addRenderData('count',DemandLogic::getDemandCount($this->getUserId(),$data['show_status']),false);
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月30日
     * description:关闭我的需求
     * @return \think\response\Json
     */
    public function cancelMyDemand()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id','reason','desc']);

        $this->check($data,'DemandCloseReason.add');

        $result = DemandOrderLogic::cancelMyDemand($this->getUserId(),$data);

        switch ($result['code']){
            case 1:
                $this->setRenderCode(200);
                break;
            default:
                $this->setRenderCode(500);
                break;
        }
        $this->setRenderMessage($result['msg']);

        return $this->getRenderJson();
    }

    public function logisticsDemo(){
        exit;
        //todo::第三方物流接口调用DEMO

        //todo::即时查询
        $kdniaoUtil = new kdniaoUtil();
        $resultData = $kdniaoUtil->getOrderTracesByJson('YTO','886110343171670057');
        if($resultData === false){
            //查询失败
        }
        // API地址 http://www.kdniao.com/api-track#api
//        array(2) {
//            ["State"] => string(1) "3"   //物流状态：2-在途中,3-签收,4-问题件
//            ["Traces"] => array(13) {    //物流详情
//                [0] => array(2) {
//                    ["AcceptTime"] => string(19) "2017-08-23 22:11:15"
//                    ["AcceptStation"] => string(67) "【北京市朝阳区工体公司】 取件人: 郎丽娜 已收件"
//                   }
//            }
//} Reason	String	失败原因	O  不一定有这个字段返回


        //todo::单号识别
        $resultData = $kdniaoUtil->getLogisticCompany('236869333662');
//        array{
//        [0] => array(2) {
//            ["ShipperCode"] => string(3) "LHT"
//            ["ShipperName"] => string(15) "联昊通速递"
//          }
//        }
        dump($resultData);

    }

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:获取我的承接列表
     * @return \think\response\Json
     */
    public function getMyContributions()
    {
        $this->validateToken();

        $data = $this->selectParam(['show_status']);

        $this->check($data,'Demand.list');
        $data['show_status'] = empty($data['show_status'])?1:$data['show_status'];
        $data['user_id'] = $this->getUserId();

        $result = DemandLogic::getContributionsByUser($this->getUserId(),$data['show_status'],$this->pageIndex,$this->pageSize);


        if($result === false){
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        }else{
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('lists',$result,false);
            $this->addRenderData('count',DemandLogic::getContributionsCount($this->getUserId(),$data['show_status']),false);
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月14日
     * description:获取我购买过的商品列表(已完成订单里)
     * @return \think\response\Json
     */
    public function getMyProductsHistory()
    {
        $this->validateToken();

        $result = ShopProductLogic::getMyProductsHistory($this->getUserId(),$this->pageIndex,$this->pageSize);

        switch ($result){
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('list',$result,false);
                break;
        }
        return $this->getRenderJson();
    }

    public function batchCancelMyProductCollections()
    {
        $this->validateToken();
        
        $data = $this->selectParam(['product_ids']);

        $this->check($data,'ShopProduct.batchCancel');

        $result = ShopProductLogic::batchCancelMyProductCollections($this->getUserId(),$data['product_ids']);

        switch ($result){
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('异常错误');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('取消失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('取消成功');
                break;
        }

        return $this->getRenderJson();
    }
}