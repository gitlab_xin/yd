<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/26
 * Time: 16:32
 */

namespace app\api\controller;


use app\api\logic\SchemeBZLogic;
use app\api\logic\SchemeBZOrderLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

class SchemeBZ extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年10月19日
     * description:创建方案
     * @return \think\response\Json
     */
    public function create()
    {
        $data = $this->selectParam(['width', 'height', 'type', 'table_type']);

        $this->check($data, 'SchemeBZ.create');

        $logic = new SchemeBZLogic();

        $result = $logic->createBZScheme($data);

        $this->addRenderData('scheme', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月19日
     * description:保存方案
     * @return \think\response\Json
     */
    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'doorType', 'doorDirection']);
        $schemeData = array_shift($requestData);

        $schemeLogic = new SchemeBZLogic();
        $params = $schemeLogic->getOriginParams($schemeData);

        $this->check(array_merge($params, $requestData), 'SchemeBZ.store');

        $schemeBean = $schemeLogic->buildScheme($schemeData);
        $result = $schemeLogic->addBZYGDoor($schemeBean, $requestData['doorType'], $requestData['doorDirection']);

        if ($result != null) {
            $this->setRenderCode(500);
            $this->setRenderMessage($result);
            return $this->getRenderJson();
        }

        $schemeId = $schemeLogic->store($schemeBean, $schemeData, $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:保存方案
     * @return \think\response\Json
     */
    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'scheme_id', 'doorType', 'doorDirection']);
        $schemeData = array_shift($requestData);
        $requestData['user_id'] = $this->getUserId();

        $schemeLogic = new SchemeBZLogic();
        $params = $schemeLogic->getOriginParams($schemeData);

        $this->check(array_merge($params, $requestData), 'SchemeBZ.update');

        $schemeBean = $schemeLogic->buildScheme($schemeData);
        $result = $schemeLogic->addBZYGDoor($schemeBean, $requestData['doorType'], $requestData['doorDirection']);

        if ($result != null) {
            $this->setRenderCode(500);
            $this->setRenderMessage($result);
            return $this->getRenderJson();
        }

        $schemeLogic->update($schemeBean, $schemeData, $requestData['user_id'], $requestData['scheme_id']);
        $this->setRenderMessage('修改成功');
        $this->addRenderData("id", $requestData['scheme_id']);


        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:结算
     * @return \think\response\Json
     */
    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'SchemeBZ.complete');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeBZLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeBZOrderLogic();


        //统计标准衣柜功能件
        $orderLogic->scoreBZYGSchemeProducts($schemeBean);
        //统计标准衣柜组合件
        $orderLogic->scoreBZYGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $orderLogic->scoreBZYGSchemeplates($schemeBean);
        //统计综合柜五金件
        $orderLogic->scoreBZYGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($schemeBean->getScheme_dis_price(), 2)]);

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'SchemeBZ.complete2');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeBZLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeBZOrderLogic();


        //统计标准衣柜功能件
        $orderLogic->scoreBZYGSchemeProducts($schemeBean);
        //统计标准衣柜组合件
        $orderLogic->scoreBZYGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $orderLogic->scoreBZYGSchemeplates($schemeBean);
        //统计综合柜五金件
        $orderLogic->scoreBZYGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}