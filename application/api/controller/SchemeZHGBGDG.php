<?php
/**
 * 壁挂吊柜
 */

namespace app\api\controller;


use app\api\logic\SchemeZHGBGDGLogic;
use app\api\logic\SchemeZHGOrderLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

/**
 * 壁柜吊柜
 * Class SchemeZHGBGDG
 * @package app\api\controller
 */
class SchemeZHGBGDG extends Base
{

    /**
     * @author: Airon
     * @time: 2017年10月23日
     * description:结算
     * @return \think\response\Json
     */
    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHGBGDG.complete');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGBGDGLogic();

        $data = $schemeLogic->score($schemeData);//结算
        $zjProducts = $data['zj_products'];
        $plateProducts = $data['plate_products'];
        $wujinProducts = $data['wujin_products'];
        $scoreProducts = $data['score_products'];

        $this->addRenderData('scheme_pic', $schemeModel['scheme_pic']);
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($data['gt_price']));
        $this->addRenderData('dis_gt_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('zj_price', formatMoney($data['zj_price']));
        $this->addRenderData('dis_zj_price', formatMoney($data['dis_zj_price']));
        $this->addRenderData('wujin_price', formatMoney($data['wujin_price']));
        $this->addRenderData('dis_wujin_price', formatMoney($data['dis_wujin_price']));
        $this->addRenderData('plate_price', formatMoney($data['plate_price']));
        $this->addRenderData('dis_plate_price', formatMoney($data['dis_plate_price']));
        $this->addRenderData('scheme_price', formatMoney($data['gt_price']));
        $this->addRenderData('scheme_dis_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('discount', 0.68);

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($data['dis_gt_price'], 2)]);

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年10月21日
     * description:保存方案
     * @return \think\response\Json
     */
    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme']);

        $schemeLogic = new SchemeZHGBGDGLogic();

        $params = $schemeLogic->getOriginParams($requestData['scheme']);
        $this->check($params, 'SchemeZHGBGDG.store');

        $schemeId = $schemeLogic->store($requestData['scheme'], $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年10月21日
     * description:修改方案
     * @return \think\response\Json
     */
    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'scheme_id']);
        $schemeData = array_shift($requestData);
        $requestData['user_id'] = $this->getUserId();

        $schemeLogic = new SchemeZHGBGDGLogic();

        $params = $schemeLogic->getOriginParams($schemeData);
        $this->check(array_merge($params, $requestData), 'SchemeZHGBGDG.update');

        if (!$schemeLogic->update($schemeData, $requestData['user_id'], $requestData['scheme_id'])) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
        } else {
            $this->setRenderMessage('修改成功');
            $this->addRenderData("id", $requestData['scheme_id']);
        }


        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取推荐方案列表
     * @return \think\response\Json
     */
    public function getCommendSchemes()
    {
//        $this->validateToken();
        $data = $this->selectParam(['scheme_width','scheme_height']);

        $this->check($data, ['scheme_width'=>'require|gt:0|number','scheme_height'=>'require|gt:0|number']);

        $schemeLogic = new SchemeZHGBGDGLogic();
        $result = $schemeLogic->getRecommendSchemes($data['scheme_width'], $data['scheme_height'], $this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取推荐方案详情
     * @return \think\response\Json
     */
    public function getCommendSchemeInfo()
    {
//        $this->validateToken();
        $request = $this->selectParam(['scheme_id']);

        $this->check($request, ['scheme_id'=>'require|gt:0|number']);

        $schemeLogic = new SchemeZHGBGDGLogic();
        $data = $schemeLogic->getRecommendSchemeInfo($request['scheme_id'],
            ['id as scheme_id', 'scheme_name', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'serial_array']);

        if ($data === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        }

        if (empty($data)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('方案不存在');
            return $this->getRenderJson();
        }
        $data['serial_array'] = json_decode($data['serial_array'], true);
        $this->setRenderCode(200);
        $this->addRenderData('scheme_id', $data['scheme_id'], false);
        $this->addRenderData('scheme_name', $data['scheme_name'], false);
        $this->addRenderData('scheme_type', $data['scheme_type'], false);
        $this->addRenderData('scheme_s_type', $data['scheme_s_type'], false);
        $this->addRenderData('scheme_b_type', $data['scheme_b_type'], false);
        $this->addRenderData('username', $data['username'], false);
        $this->addRenderData('avatar', $data['avatar'], false);
        $this->addRenderData('serial_array', $data['serial_array'], false);

        return $this->getRenderJson();
    }

    public function getPrice(){
        $this->validateToken();
        $requestData = $this->selectParam(['product_list']);
        $this->check($requestData, 'SchemeZHGBGDG.price');

        $schemeLogic = new SchemeZHGBGDGLogic();

        $data = $schemeLogic->score(['productList' => json_decode($requestData['product_list'],true), 'scheme_pic' => '']);//结算
        $zjProducts = $data['zj_products'];
        $plateProducts = $data['plate_products'];
        $wujinProducts = $data['wujin_products'];
        $scoreProducts = $data['score_products'];

        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($data['gt_price']));
        $this->addRenderData('dis_gt_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('zj_price', formatMoney($data['zj_price']));
        $this->addRenderData('dis_zj_price', formatMoney($data['dis_zj_price']));
        $this->addRenderData('wujin_price', formatMoney($data['wujin_price']));
        $this->addRenderData('dis_wujin_price', formatMoney($data['dis_wujin_price']));
        $this->addRenderData('plate_price', formatMoney($data['plate_price']));
        $this->addRenderData('dis_plate_price', formatMoney($data['dis_plate_price']));
        $this->addRenderData('scheme_price', formatMoney($data['gt_price']));
        $this->addRenderData('scheme_dis_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('discount', 0.68);

        $this->setRenderMessage('计算成功');

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHGBGDG.complete2');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);
        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGBGDGLogic();

        $data = $schemeLogic->score($schemeData);//结算
        $zjProducts = $data['zj_products'];
        $plateProducts = $data['plate_products'];
        $wujinProducts = $data['wujin_products'];
        $scoreProducts = $data['score_products'];

        $this->addRenderData('scheme_pic', $schemeModel['scheme_pic']);
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($data['gt_price']));
        $this->addRenderData('dis_gt_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('zj_price', formatMoney($data['zj_price']));
        $this->addRenderData('dis_zj_price', formatMoney($data['dis_zj_price']));
        $this->addRenderData('wujin_price', formatMoney($data['wujin_price']));
        $this->addRenderData('dis_wujin_price', formatMoney($data['dis_wujin_price']));
        $this->addRenderData('plate_price', formatMoney($data['plate_price']));
        $this->addRenderData('dis_plate_price', formatMoney($data['dis_plate_price']));
        $this->addRenderData('scheme_price', formatMoney($data['gt_price']));
        $this->addRenderData('scheme_dis_price', formatMoney($data['dis_gt_price']));
        $this->addRenderData('discount', 0.68);

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}