<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/18
 * Time: 9:47
 */

namespace app\api\controller;

use app\api\logic\CommonLogic;
use app\api\logic\ConfigLogic;
use app\api\logic\ShoppingCartLogic;
use app\api\logic\ShopSupplierLogic;
use app\common\model\ShopProduct as ProductModel;
use app\api\logic\ShopProductLogic;
use app\api\logic\ShopSearchKeyLogic;
use app\common\model\ShopPopular as PopularModel;
use app\common\model\ShopCollect as CollectModel;
use app\common\model\ShopProductStandard as StandardModel;
use app\common\model\ShopSupplier as SupplierModel;
use app\common\model\ShopSupplierBadRecord as SupplierBadRecordModel;
use app\common\model\ShopChangePriceRecord as ChangePriceRecordModel;
use app\api\logic\CooperativePartnerLogic as PartnerLogic;


class ShopIndex extends Base
{
    /**
     * @author: Airon
     * @time: 2017年7月18日
     * description:热门商品
     * @return \think\response\Json
     */
    public function popular()
    {
        $result = PopularModel::getList(4);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('banner_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:首页商品推荐
     * @return \think\response\Json
     */
    public function home()
    {
        $result = ShopProductLogic::home();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('shop_classify', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:按分类获取商品列表
     * @return \think\response\Json
     */
    public function classifyProductList()
    {
        $requestData = $this->selectParam(['classify_id', 'classify_level', 'order_type', 'sort', 'supplier_id', 'min_price', 'max_price']);
//        $requestData['supplier_id'] = !empty($_POST['supplier_id']) ? $requestData['supplier_id'] : false;
//        $requestData['min_price'] = isset($_GET['min_price']) ? $requestData['min_price'] : false;
//        $requestData['max_price'] = isset($_GET['max_price']) ? $requestData['max_price'] : false;
        $this->check($requestData, 'ShopProduct.classifyProduct');
        $ProductLogic = new ShopProductLogic();
        $result = $ProductLogic->classifyProductList(
            $requestData['classify_id'],
            $requestData['classify_level'],
            $requestData['order_type'],
            $requestData['sort'],
            $requestData['supplier_id'],
            $requestData['min_price'],
            $requestData['max_price'],
            $this->pageIndex,
            $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('product_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月19日
     * description:搜索商品列表
     * @return \think\response\Json
     */
    public function searchByKeyword()
    {
        $requestData = $this->selectParam(['keyword', 'order_type', 'sort', 'supplier_id', 'min_price', 'max_price']);

        $requestData['supplier_id'] = !empty($_POST['supplier_id']) ? $requestData['supplier_id'] : false;
        $requestData['min_price'] = isset($_POST['min_price']) ? $requestData['min_price'] : false;
        $requestData['max_price'] = isset($_POST['max_price']) ? $requestData['max_price'] : false;

        $this->check($_POST, 'ShopProduct.searchByKeyword');
        $ProductLogic = new ShopProductLogic();
        $result = $ProductLogic->searchByKeyword(
            $requestData['keyword'],
            $requestData['order_type'],
            $requestData['sort'],
            $requestData['supplier_id'],
            $requestData['min_price'],
            $requestData['max_price'],
            $this->pageIndex,
            $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        ShopSearchKeyLogic::add($requestData['keyword']);
        $this->addRenderData('product_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:商品详情
     * @return \think\response\Json
     */
    public function productInfo()
    {
        //$this->validateToken();
        $requestData = $this->selectParam(['product_id', 'user_id']);
        $requestData['user_id'] = empty($requestData['user_id']) ? 0 : $requestData['user_id'];
        $this->check($requestData, 'ShopProduct.product_info');
        $ProductLogic = new ShopProductLogic();
        $result = $ProductLogic->get_info($requestData['product_id'], $requestData['user_id']);
        if ($result === -1) {
            $this->setRenderCode(501);
            $this->setRenderMessage('该商品已失效');
            return $this->getRenderJson();
        }
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        ProductModel::build()->where(['product_id' => $requestData['product_id']])->setInc('look_count');
        $this->addRenderData('product_info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:收藏商品
     * @return \think\response\Json
     */
    public function collect()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['product_id', 'collect_type']);
        $this->check($requestData, 'ShopProduct.collect');

        $bool = ProductModel::build()->check($requestData['product_id']);
        if ($bool === false && $requestData['collect_type'] == 'collect') {
            $this->setRenderCode(500);
            $this->setRenderMessage('商品不存在');
            return $this->getRenderJson();
        }
        $bool = CollectModel::check($this->getUserId(), $requestData['product_id']);
        $msg = "";
        switch ($requestData['collect_type']) {
            case "collect":
                $msg = "收藏";
                if (!empty($bool)) {
                    $result = true;
                    break;
                }
                $result = CollectModel::collect($this->getUserId(), $requestData['product_id']);
                ProductModel::build()->where(['product_id' => $requestData['product_id']])->setInc('collect_count');
                break;
            case "cancel":
                $msg = "取消收藏";
                if (empty($bool)) {
                    $result = true;
                    break;
                }
                $result = CollectModel::cancel($this->getUserId(), $requestData['product_id']);
                ProductModel::build()->where(['product_id' => $requestData['product_id']])->setDec('collect_count');
                break;
            default:
                $result = false;
        }
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage($msg . '成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月20日
     * description:购物车内收藏商品
     * @return \think\response\Json
     */
    public function shoppingCartCollect()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id_list']);
        $this->check($requestData, 'ShopPay.settlementCart');
        $cart_id_list = explode(',', $requestData['cart_id_list']);
        foreach ($cart_id_list as $key => $value) {
            $cart_info = ShoppingCartLogic::getInfo($value, $this->getUserId());

            $bool = ProductModel::build()->check($cart_info['product_id']);
            if ($bool === false) {
                $this->setRenderCode(500);
                $this->setRenderMessage('商品不存在');
                return $this->getRenderJson();
            }
            $bool = CollectModel::check($this->getUserId(), $cart_info['product_id']);
            if (empty($bool)) {
                CollectModel::collect($this->getUserId(), $cart_info['product_id']);
                ProductModel::build()->where(['product_id' => $cart_info['product_id']])->setInc('collect_count');
            }

        }
        $this->setRenderMessage('收藏成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:获取商品规格
     * @return \think\response\Json
     */
    public function standard()
    {
        $requestData = $this->selectParam(['product_id']);
        $this->check($requestData, 'ShopProduct.product_id');
        $result = StandardModel::getProductStandard($requestData['product_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('standard_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:获取商品参数
     * @return \think\response\Json
     */
    public function param()
    {
        $requestData = $this->selectParam(['product_id']);
        $this->check($requestData, 'ShopProduct.product_id');
        $result = ProductModel::getParam($requestData['product_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('param_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:供应商详情
     * @return \think\response\Json
     */
    public function supplier()
    {
        $requestData = $this->selectParam(['supplier_id']);
        $this->check($requestData, 'ShopSupplier.supplier_id');
        $result = SupplierModel::getInfo($requestData['supplier_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:供货商不良记录列表
     * @return \think\response\Json
     */
    public function supplierBadRecord()
    {
        $requestData = $this->selectParam(['supplier_id']);
        $this->check($requestData, 'ShopSupplier.supplier_id');
        $result = SupplierBadRecordModel::getList($requestData['supplier_id'], $this->pageIndex, $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月26日
     * description:推荐品牌
     * @return \think\response\Json
     */
    public function supplierRecommend()
    {
        $requestData = $this->selectParam(['count']);
        $this->check($requestData, 'ShopProduct.count');
        $result = ShopSupplierLogic::recommend($requestData['count']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月7日
     * description:买过的品牌
     * @return \think\response\Json
     */
    public function supplierBought()
    {
        $requestData = $this->selectParam(['count']);
        $this->check($requestData, 'ShopProduct.count');
        $this->validateToken();
        $result = ShopSupplierLogic::bought($this->getUserId(), $requestData['count']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:供货商不良记录详情
     * @return \think\response\Json
     */
    public function supplierBadRecordInfo()
    {
        $requestData = $this->selectParam(['br_id']);
        $this->check($requestData, 'ShopSupplier.br_id');
        $result = SupplierBadRecordModel::getInfo($requestData['br_id']);
        if (empty($result)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('内容不存在');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月21日
     * description:修改价格记录
     * @return \think\response\Json
     */
    public function changePriceRecord()
    {
        $requestData = $this->selectParam(['product_id', 'standard_id']);
        $this->check($requestData, 'ShopProduct.changePriceRecord');
        $result = ChangePriceRecordModel::getList($requestData['product_id'], $requestData['standard_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('price_record', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月25日
     * description:获取热门搜索
     * @return \think\response\Json
     */
    public function getHotSearch()
    {
        $result = ShopSearchKeyLogic::getHotSearch();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $result = array_column($result, 'key');
        $this->addRenderData('key_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月1日
     * description:获取商品保障
     * @return \think\response\Json
     */
    public function afterSaleService()
    {
        $requestData = $this->selectParam(['supplier_id']);
        $this->check($requestData, 'ShopSupplier.supplier_id');
        $result = ShopProductLogic::afterSaleService($requestData['supplier_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_info', $result, false);
        return $this->getRenderJson();
    }

    /* 以下是PC商城特有的接口*/
    /**
     * @author: Airon
     * @time: 2017年8月2日
     * description:PC商城首页推荐
     */
    public function recommend()
    {
        $result = ConfigLogic::Store();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $result = json_decode($result['value'], JSON_UNESCAPED_UNICODE);
        $this->addRenderData('recommend', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:获取热门商品
     * @return \think\response\Json
     */
    public function webPopular()
    {
        $result = ShopProductLogic::webPopular($this->pageIndex, $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('recommend', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月3日
     * description:PC搜索商品列表
     * @return \think\response\Json
     */
    public function searchWebByKeyword()
    {
        $requestData = $this->selectParam([
            'keyword',
            'order_type',
            'sort',
            'supplier_id',
            'classify_level',
            'classify_id'
        ]);
        $requestData['keyword'] = empty($requestData['keyword']) ? "" : $requestData['keyword'];
        $requestData['supplier_id'] = empty($requestData['supplier_id']) ? 0 : $requestData['supplier_id'];
        $requestData['classify_level'] = empty($requestData['classify_level']) ? "" : $requestData['classify_level'];
        $requestData['classify_id'] = empty($requestData['classify_id']) ? 0 : $requestData['classify_id'];

        $this->check($requestData, 'ShopProduct.searchByKeyword');
        $ProductLogic = new ShopProductLogic();
        $result = $ProductLogic->searchWebByKeyword(
            $requestData['keyword'],
            $requestData['classify_id'],
            $requestData['classify_level'],
            $requestData['supplier_id'],
            $requestData['order_type'],
            $requestData['sort'],
            $this->pageIndex,
            $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据为空');
            return $this->getRenderJson();
        }
        if (!empty($requestData['keyword'])) {
            ShopSearchKeyLogic::add($requestData['keyword']);
        }
        $this->addRenderData('product_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月9日
     * description:pc端供货商不良记录
     * @return \think\response\Json
     */
    public function webSupplierBadRecord()
    {
        $requestData = $this->selectParam(['supplier_id']);
        $this->check($requestData, 'ShopSupplier.supplier_id');
        $result = SupplierBadRecordModel::webGetList($requestData['supplier_id'], $this->pageIndex, 1);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('请求失败');
            return $this->getRenderJson();
        }
        $count = SupplierBadRecordModel::whereCount(['supplier_id' => $requestData['supplier_id']]);
        $this->addRenderData('supplier_info', $result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:获取用户点赞/评论/收藏总和
     * @return \think\response\Json
     */
    public function getEverythingSum()
    {
        $requestData = $this->selectParam(['user_id']);

        $this->check($requestData, 'User.user');
        $result = CommonLogic::getEverythingSum($requestData['user_id']);
        if (empty($result)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('请求失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('sum', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月23日
     * description:获取合作伙伴
     * @return \think\response\Json
     */
    public function getCooperativePartners()
    {
        $result = PartnerLogic::getPartners($this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
        }

        return $this->getRenderJson();
    }
}