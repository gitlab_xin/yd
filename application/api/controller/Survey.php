<?php
namespace app\api\controller;

use app\api\logic\SurveyLogic;

class Survey extends Base
{
    public function datas(){
        $this->validateToken();
        $user_id = $this->getUserId();
        $list = SurveyLogic::datas($user_id);
        if ($list === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('info', $list, false);
        return $this->getRenderJson();
    }

    public  function save(){
        $this->validateToken();
        $user_id = $this->getUserId();
        $data = $this->selectParam(['ids' => []]);
        SurveyLogic::save($data, $user_id);
        $this->addRenderData('info', '提交成功', false);
        return $this->getRenderJson();
    }

}