<?php
/**
 * Created by PhpStorm.
 * User: fio
 */

namespace app\api\controller;

use app\api\logic\CustomizedLogic;
use app\api\logic\SchemeRQSOrderLogic;
use app\api\logic\SchemeYMJLogic;
use app\api\logic\SchemeYMJOrderLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;
use think\Request;

class SchemeYMJ extends Base
{
    /**
     * 生成房间
     * 由于半路需求变动，已废弃
     * 但也未完成
     * 暂时进行封印，防止需求突然又变动
     * @return \think\response\Json
     */
//    public function buildRooms()
//    {
//
//        $requestParam = $this->selectParam(['room']);
//
//        $logic = new SchemeYMJLogic();
//
//        $productList = $logic->buildRooms($requestParam['room']);
//
//        $this->addRenderData('schemes', $productList, false);
//        return $this->getRenderJson();
//    }


    /**
     * 保存衣帽间信息
     * @return \think\response\Json
     */
    public function saveRoom()
    {
        $this->validateToken();
        $requestParam = $this->selectParam(['room']);

        $logic = new SchemeYMJLogic();

        $scheme_id = $logic->saveRoom($requestParam['room'], $this->getUserId());

        if ($scheme_id) {
            $this->setRenderCode(200);
            $this->setRenderMessage("保存成功");
            $this->addRenderData('scheme_id', $scheme_id, false);
        } else {
            $this->setRenderCode(500);
            $this->setRenderMessage("保存失败");
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2019年12月
     * description 创建单个柜体并生成方案
     * @return \think\response\Json
     */
    public function step1()
    {
        $this->validateToken();
        $requestParam = $this->selectParam(['data', 'room_id']);
        $this->check($requestParam, ['room_id'=>'require|number']);
        $logic = new SchemeYMJLogic();
        $scheme = $logic->createRQSScheme($requestParam['data'],$requestParam['room_id'],$this->getUserId());
        $this->addRenderData('schemes', $scheme['scheme'], false);
        $this->addRenderData('scheme_id', $scheme['scheme_id'], false);
        return $this->getRenderJson();
    }


    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['room', 'scheme_id']);
        $user_id = $this->getUserId();

        $schemeLogic = new SchemeYMJLogic();

        $originParams['user_id'] = $user_id;


        $result = $schemeLogic->updateRoom($requestData['room'], $user_id,$requestData['scheme_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('修改成功');
        $this->addRenderData("id", $requestData['scheme_id']);

        return $this->getRenderJson();
    }

    /**
     * 推荐案例
     */
    public function recommendWork()
    {
        $requestData = $this->selectParam(['width', 'height', 'range' => 150]);
        $this->check($requestData, ['width'=>'require|number','height'=>'require|number']);
        $result = SchemeYMJLogic::getRecommend($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2019年12月
     * description 我的定制
     * @return \think\response\Json
     */
    public function myCustomize(){
        $this->validateToken();
        $requestData = $this->selectParam(['room_id','key']);
        $this->check($requestData, ['room_id'=>'require|number','key'=>'require|max:32']);
        $result = SchemeYMJLogic::getCustomize($requestData['room_id'],$requestData['key'],$this->getUserId());
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2019年12月
     * description 设置默认柜体
     * @return \think\response\Json
     */
    public function setDefault(){
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id','room_id','key']);
        $this->check($requestData,['scheme_id'=>"require|number",'room_id' => "require|number",'key'=>'require']);
        $result = SchemeYMJLogic::setDefault($requestData['room_id'],$requestData['scheme_id'],$requestData['key'],$this->getUserId());
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('设置失败');
            return $this->getRenderJson();
        }
        return $this->getRenderJson();
    }
    /**
     * 获取衣帽间详情
     * @return \think\response\Json
     */
    public function getInfo()
    {
        $this->validateToken();
        $requestParam = $this->selectParam(['scheme_id']);

        $logic = new SchemeYMJLogic();
        $schemes = $logic->getYMJ($requestParam['scheme_id'],$this->getUserId());
        $schemes['schemes'] = $logic->getCabinets($requestParam['scheme_id'], $this->getUserId());
        foreach ($schemes['schemes'] as &$cabinet) {
            $cabinet['cabinet_name'] = empty($cabinet['barriers_array']['cabinet_name'])?"":$cabinet['barriers_array']['cabinet_name'];
            unset($cabinet['barriers_array']);
        }
        $this->addRenderData('scheme', $schemes, false);
        return $this->getRenderJson();
    }


    /**
     * 获取衣帽间下所有的柜子
     * @return \think\response\Json
     */
    public function getCabinets()
    {
        $this->validateToken();
        $requestParam = $this->selectParam(['scheme_id']);

        $logic = new SchemeYMJLogic();

        $schemes = $logic->getCabinets($requestParam['scheme_id'], $this->getUserId());

        $this->addRenderData('schemes', $schemes, false);
        return $this->getRenderJson();
    }

    /**
     * 生成入墙式方案
     * @return \think\response\Json
     */
    public function create()
    {
        $this->validateToken();
        $requestParam = $this->selectParam(['scheme_id']);

        $logic = new SchemeYMJLogic();

        $schemes = $logic->create($requestParam['scheme_id'], $this->getUserId());

        $this->addRenderData('scheme', $schemes, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:结算
     * @return \think\response\Json
     */
    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeYMJ.complete');

        $orderLogic = new SchemeYMJOrderLogic();
        $schemes_complete = $orderLogic->complete($requestData['scheme_id']);

        $this->addRenderData('complete', $schemes_complete,false);

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeYMJ.complete2');

        $orderLogic = new SchemeYMJOrderLogic();
        $schemes_complete = $orderLogic->complete($requestData['scheme_id']);

        $this->addRenderData('complete', $schemes_complete,false);

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}
