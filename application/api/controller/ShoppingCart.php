<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/24
 * Time: 11:31
 */

namespace app\api\controller;


use app\api\logic\ShoppingCartLogic;
use app\common\model\ShopProductStandard;

class ShoppingCart extends Base
{
    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:添加购物车
     * @return \think\response\Json
     */
    public function add()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['product_id', 'standard_id', 'count']);
        $this->check($requestData, 'ShoppingCart.add');
        $bool = ShopProductStandard::check($requestData['product_id'], $requestData['standard_id']);
        if (empty($bool)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('商品规格不存在');
            return $this->getRenderJson();
        }
        $result = ShoppingCartLogic::add(
            $this->getUserId(),
            $requestData['product_id'],
            $requestData['standard_id'],
            $requestData['count']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('添加成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:修改购物车数量
     * @return \think\response\Json
     */
    public function setCount()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id', 'count', 'type']);
        $this->check($requestData, 'ShoppingCart.setCount');

        $result = ShoppingCartLogic::setCount(
            $requestData['cart_id'],
            $this->getUserId(),
            $requestData['count']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('操作成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:获取购物车列表
     * @return \think\response\Json
     */
    public function getList()
    {
        $this->validateToken();
        $result = ShoppingCartLogic::getList($this->getUserId());
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('supplier_list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:获取购物商品数量
     * @return \think\response\Json
     */
    public function getCount()
    {
        $this->validateToken();
        $result = ShoppingCartLogic::getCount($this->getUserId());
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('count', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月24日
     * description:删除购物车中的商品
     * @return \think\response\Json
     */
    public function delete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id_list']);
        $this->check($requestData, 'ShoppingCart.delete');
        $result = ShoppingCartLogic::delete($requestData['cart_id_list'], $this->getUserId());

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('操作成功');
        return $this->getRenderJson();
    }
}