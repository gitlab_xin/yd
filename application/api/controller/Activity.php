<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/29
 * Time: 10:58
 */

namespace app\api\controller;

use app\api\logic\ActivityLogic;


class Activity extends Base
{
    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:获取活动列表
     * @return \think\response\Json
     */
    public function getList()
    {
        $result = ActivityLogic::getList($this->pageIndex, $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('activity_list', $result, false);
        $this->addRenderData('count', ActivityLogic::getCount(), false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:活动详情
     * @return \think\response\Json
     */
    public function getInfo()
    {
        $requestData = $this->selectParam(['activity_id']);
        $this->check($requestData, 'Activity.getInfo');
        $result = ActivityLogic::getInfo($requestData['activity_id']);
        if (empty($result)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('活动不存在');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('article_detail', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月29日
     * description:活动报名
     * @return \think\response\Json
     */
    public function apply()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $requestData = $this->selectParam([
            'activity_id',
            'name',
            'phone',
            'age',
            'gender',
            'email' => '',
            'organize_type',
            'school_name' => '',
            'company_name' => '',
            'company_position' => '',
            'remarks' => '',
        ]);
        $requestData['user_id'] = $this->getUserId();
        $requestData['organize_type'] = empty($requestData['organize_type']) ? 'empty' : $requestData['organize_type'];
        $this->check($requestData, 'ActivityApply.apply');
        switch ($requestData['organize_type']) {
            case 'empty':
                unset($requestData['school_name']);
                unset($requestData['company_name']);
                unset($requestData['company_position']);
                break;
            case 'school':
                unset($requestData['company_name']);
                unset($requestData['company_position']);
                break;
            case 'company':
                unset($requestData['school_name']);
                break;
        }
        $bool = ActivityLogic::applyCheck($requestData['user_id'], $requestData['activity_id']);
        if($bool === -1){
            //活动不存在
            $this->setRenderCode(500);
            $this->setRenderMessage('活动不存在');
            return $this->getRenderJson();
        }
        if($bool === -2){
            //活动已结束
            $this->setRenderCode(500);
            $this->setRenderMessage('活动已结束');
            return $this->getRenderJson();
        }
        if($bool === -3){
            //已报名
            $this->setRenderCode(500);
            $this->setRenderMessage('您已报名过该活动');
            return $this->getRenderJson();
        }
        $result = ActivityLogic::apply($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('报名失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->setRenderMessage('报名成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的活动列表
     * @return \think\response\Json
     */
    public function getMyList()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $requestData = $this->selectParam(['type']);
        $requestData['user_id'] = $this->getUserId();
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;

        $this->check($requestData, 'Activity.myList');

        $result = ActivityLogic::getMyList($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('activity_list', $result, false);
        $this->addRenderData('count',ActivityLogic::getMyListCount($requestData),false);
        return $this->getRenderJson();

    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的报名信息
     * @return \think\response\Json
     */
    public function getMyAppInfo()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $requestData = $this->selectParam(['activity_id']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'ActivityApply.applicationInfo');

        $result = ActivityLogic::getMyAppInfo($requestData);
        if ($result === null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('app_info', $result, false);
        return $this->getRenderJson();
    }
}