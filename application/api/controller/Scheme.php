<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/6
 * Time: 15:58
 */

namespace app\api\controller;

use app\api\logic\SchemeBZLogic;
use app\api\logic\SchemeLogic;
use app\api\logic\SchemeQWLogic;
use app\api\logic\SchemeRQSLogic;
use app\common\model\CustomizedScheme;
use app\common\model\SchemeRQSBean;
use app\common\tools\RedisUtil;

class Scheme extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取我的方案列表
     * @return \think\response\Json
     */
    public function getMySchemes()
    {
        $this->validateToken();
        $data = $this->selectParam(['scheme_type' => 'all']);

        $this->check($data, 'scheme.list');

        $result = SchemeLogic::getMySchemes($this->getUserId(), $data['scheme_type'], $this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
            $this->addRenderData('count', SchemeLogic::getMySchemesCount($this->getUserId(), $data['scheme_type']), false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年5月6日
     * description:获取官方方案列表
     * @return \think\response\Json
     */
    public function getCommonSchemes()
    {
        $data = $this->selectParam(['scheme_type' => 'all']);

        $this->check($data, 'scheme.list');
        //todo 暂定写死官方用户ID为46 主要适用于测试环境
        $result = SchemeLogic::getMySchemes(46, $data['scheme_type'], $this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
            $this->addRenderData('count', SchemeLogic::getMySchemesCount($this->getUserId(), $data['scheme_type']), false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取方案图片
     * @return \think\response\Json
     */
    public function getSchemePic()
    {
        $data = $this->selectParam(['scheme_id']);

        $this->check($data, 'scheme.detail');

        $result = SchemeLogic::getSchemePic($data['scheme_id']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('info', $result, false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:获取方案详情
     * @return \think\response\Json
     */
    public function getSchemeDetail()
    {
        $data = $this->selectParam(['scheme_id']);

        $this->check($data, 'scheme.detail');

        $result = SchemeLogic::getSchemeDetail($data['scheme_id']);

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('detail', $result, false);
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月15日
     * description:删除我的方案
     * @return \think\response\Json
     */
    public function deleteMyScheme()
    {
        $this->validateToken();

        $data = $this->selectParam(['scheme_id']);

        $this->check($data, 'scheme.detail');

        $result = SchemeLogic::deleteMyScheme($this->getUserId(), $data['scheme_id']);


        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('异常错误');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            default:
            $this->setRenderCode(200);
            $this->setRenderMessage('删除成功');
            break;
        }
        return $this->getRenderJson();
    }


    public function getInfo()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id','type' => 'own']);
        $this->check($requestData, 'scheme.detail');
        $user_id = $this->getUserId();
        $where['id'] = $requestData['scheme_id'];
        if ($requestData['type'] == 'own') {
            $where['user_id'] = $user_id;
        }
        $data = CustomizedScheme::getInfo(
            $where,
            ['id as scheme_id', 'scheme_name', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'serial_array']);
        if (empty($data)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('方案不存在');
            return $this->getRenderJson();
        }
        $data['serial_array'] = json_decode($data['serial_array'], true);
        $this->setRenderCode(200);
        $this->addRenderData('scheme_id', $data['scheme_id'], false);
        $this->addRenderData('scheme_name', $data['scheme_name'], false);
        $this->addRenderData('scheme_type', $data['scheme_type'], false);
        $this->addRenderData('scheme_s_type', $data['scheme_s_type'], false);
        $this->addRenderData('scheme_b_type', $data['scheme_b_type'], false);
        $this->addRenderData('serial_array', $data['serial_array'], false);
        return $this->getRenderJson();
    }

    /**
     * todo 获取官方方案详情 暂时使用
     * @return \think\response\Json
     */
    public function getCommonInfo()
    {
        $requestData = $this->selectParam(['scheme_id']);
        $this->check($requestData, 'scheme.detail');
        $user_id = $this->getUserId();
        $data = CustomizedScheme::getInfo(
            ['user_id' => 46, 'id' => $requestData['scheme_id']],
            ['id as scheme_id', 'scheme_name', 'scheme_type', 'scheme_s_type', 'scheme_b_type', 'serial_array']);
        if (empty($data)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('方案不存在');
            return $this->getRenderJson();
        }
        $data['serial_array'] = json_decode($data['serial_array'], true);
        $this->setRenderCode(200);
        $this->addRenderData('scheme_id', $data['scheme_id'], false);
        $this->addRenderData('scheme_name', $data['scheme_name'], false);
        $this->addRenderData('scheme_type', $data['scheme_type'], false);
        $this->addRenderData('scheme_s_type', $data['scheme_s_type'], false);
        $this->addRenderData('scheme_b_type', $data['scheme_b_type'], false);
        $this->addRenderData('serial_array', $data['serial_array'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time:   2018年5月
     * description 保存方案临时数据
     */
    public function storeTemp(){
        $requestData = $this->selectParam(['scheme', 'door_color_id' => 0, 'scheme_door_have_hcq' => 0,'doorType', 'doorDirection']);
        $schemeData = $requestData['scheme'];
        $uuid = uuid();
        switch ($requestData['scheme']['scheme_type']){
            case "RQS":
                $schemeBean = (new SchemeRQSLogic())->buildScheme($schemeData,$requestData);
                break;
            case "QW":
                $schemeBean = (new SchemeQWLogic())->buildScheme($schemeData);
                break;
            case "BZ":
                $schemeBean = (new SchemeBZLogic())->buildScheme($schemeData);
                (new SchemeBZLogic())->addBZYGDoor($schemeBean, $requestData['doorType'], $requestData['doorDirection']);
                if (($schemeBean->getScheme_door_schemes()) != null) {
                    $schemeData['scheme_door_schemes'] = $schemeBean->getScheme_door_schemes_array();
                    $schemeData['doorType'] = $schemeBean->getScheme_door_type();
                    $schemeData['doorDirection'] = $schemeBean->getScheme_door_direction();
                }
                if (($schemeBean->getScheme_dm_schemes()) != null) {
                    $schemeData['scheme_dm_schemes'] = $schemeBean->getScheme_dm_schemes_array();
                }
                break;
        }
        RedisUtil::getInstance()->SET($uuid, serialize($schemeData),3600*2);
        $this->addRenderData('temp_uuid', $uuid, false);
        return $this->getRenderJson();
    }
    /**
     * @author: Airon
     * @time:   2018年5月
     * description 保存方案临时数据
     */
    public function getTemp(){
        $requestData = $this->selectParam(['temp_uuid']);
        $data = RedisUtil::getInstance()->get($requestData['temp_uuid']);
        $this->addRenderData('scheme', unserialize($data), false);
        return $this->getRenderJson();
    }

    /**
     * User: zhaoxin
     * Date: 2020/3/12
     * Time: 10:45 上午
     * description 易家整配新增定制产品
     */
    public function updateProduct(){
        $this->validateToken();
        $requestData = $this->selectParam(['decorate_product_id', 'scheme_id','count' => '1']);
        $this->check($requestData, ['decorate_product_id' => 'require', 'scheme_id' => 'require']);

        SchemeLogic::updateProduct($requestData, $this->getUserId());

        $this->setRenderCode(200);
        return $this->getRenderJson();
    }
}
