<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/12
 * Time: 16:49
 */

namespace app\api\controller;

use think\Log;
use think\Controller;
use app\api\logic\CustomizedOrderLogic;
use app\api\logic\ShopOrderLogic;
use app\api\logic\DemandOrderLogic;
use app\api\logic\SchemeBaseLogic;

class Crontab extends Controller
{
    protected function _initialize()
    {
        Log::init([
            'path' =>  RUNTIME_PATH . 'crontablog' . DS,
        ]);
    }

    /**
     * @author: Rudy
     * @time: 2017年8月12日
     * description:检查未支付订单是否过期
     *
     */
    public function checkOrders()
    {
        $logic = new ShopOrderLogic();
        $logic->cancelExpiredOrders();
    }

    /**
     * @author: Airon
     * @time: 2017年9月18日
     * description:检查未支付订单是否过期
     *
     */
    public function checkCustomizeOrders()
    {
        $logic = new CustomizedOrderLogic();
        $logic->cancelExpiredOrders();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月30日
     * description:自动关闭需求
     */
    public function checkDemandOrders()
    {
        $logic = new DemandOrderLogic();
        $logic->autoCancelDemandOrders();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月1日
     * description:自动收货
     */
    public function confirmOrders()
    {
        $logic = new ShopOrderLogic();
        $logic->confirmOrders();
    }

    public function flushMongoCache()
    {
        $logic = new SchemeBaseLogic();
        $logic->flushMongoCache();
    }
}