<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/11
 * Time: 14:32
 */

namespace app\api\controller;

use app\api\logic\TokenLogic;
use app\api\logic\UserLogic;
use app\api\logic\SmsLogic;
use app\common\model\Oauth;
use app\common\model\User;
use app\common\model\UserLabel;
use app\common\model\UserToken;
use app\common\tools\qqUtil;
use app\common\tools\wechatUtil;
use think\Exception;

class Index extends Base
{
    /**
     * 用户注册, 需要短信验证码
     * @requestExample {"mobile": "13164720321", "password": "123456", "code": "1386", "province":"广东省", "city":"深圳市"}
     * @responseExample {"code":200,"message":"注册成功","data":{}}
     * @return \think\response\Json
     */
    public function register()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['mobile', 'code', 'password']);
        $this->check($requestData, 'Base.register');
        $requestData['sms_type'] = 'reg';
        # 验证验证码
        $validateResult = SmsLogic::validateCode($requestData);
        if (!$validateResult) {
            $this->setRenderCode(400);
            $this->setRenderMessage('验证码错误');
            return $this->getRenderJson();
        }

        # 再次验证手机号码有效性
        $checkExist = UserLogic::check_user($requestData['mobile']);
        if (!empty($checkExist)) {
            $this->setRenderCode(400);
            $this->setRenderMessage('该手机号已被注册');
            return $this->getRenderJson();
        }

        # 注册用户
        $registerResult = UserLogic::register(
            $requestData['mobile'],
            $requestData['password']
        );
        if (!$registerResult) {
            $this->setRenderCode(500);
            $this->setRenderMessage('注册失败了');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('注册成功');
        return $this->getRenderJson();
    }

    /**
     * 用户登录
     * @requestExample {"mobile": "13112341234", "password": "123456"}
     * @return \think\response\Json
     */
    public function login()
    {
        $userData = $this->selectParam(['mobile', 'password']);

        $this->check($userData, 'User.login');

        $userInfoField = [
            'user_id',
            'username',
            'avatar',
            'mobile',
            'email',
            'gender',
            'money',
        ];

        $userModel = UserLogic::get_info($userData, $userInfoField);
        if (!isset($userModel['user_id']) || empty($userModel['user_id'])) {
            $this->setRenderCode(400);
            $this->setRenderMessage('帐号或密码错误');
            return $this->getRenderJson();
        }
        $ban = UserLogic::check_ban($userModel);
        if(!empty($ban) && $ban['ban_end_time'] > time()){
            $this->setRenderCode(400);
            $this->setRenderMessage('账号已被禁用,'."解禁时间:".date("Y-m-d H:i:s",$ban['ban_end_time']));
            return $this->getRenderJson();
        }
        $token = TokenLogic::storeToken($userModel['user_id']);
        if ($token === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'store token error');
            return $this->getRenderJson();
        }
        $oauth_type = Oauth::checkOauthType($userModel['user_id']);
        $this->addRenderData('oauth_type', $oauth_type);
        $this->addRenderData('token', $token);

        $userInfoArray = $userModel;
        $this->addRenderData('user_info', $userInfoArray);

        return $this->getRenderJson();
    }

    /**
     * 找回密码
     * @requestExample "mobile": "13164720321", "password": "123456", "code": "1386"}
     * @responseExample {"code":200,"message":"修改成功","data":{}}
     * @return \think\response\Json
     */
    public function resetPassword()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['mobile', 'code', 'password']);
        $this->check($requestData, 'Base.reset_password');
        $requestData['sms_type'] = 'pass';
        # 验证验证码
        $validateResult = SmsLogic::validateCode($requestData);
        if (!$validateResult) {
            $this->setRenderCode(400);
            $this->setRenderMessage('验证码错误');
            return $this->getRenderJson();
        }

        $userModel = UserLogic::check_user($requestData['mobile']);
        if (empty($userModel)) {
            $this->setRenderCode(400);
            $this->setRenderMessage('该帐号不存在');
            return $this->getRenderJson();
        }
        $userModel->isUpdate(true)->save(['password' => $requestData['password']]);

        $this->setRenderMessage('修改成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月17日
     * description:第三方登录 APP
     * @return \think\response\Json
     */
    public function oauthLogin()
    {
        $requestData = $this->selectParam(
            ['oauth_type', 'oauth_id', 'nick_name', 'avatar', 'access_token','union_id']
        );
        $this->check($requestData, 'User.oauth');

        if ($requestData['oauth_type'] == 'qq') {
            $accessToken = $requestData['access_token'];
            if (empty($accessToken)) {
                $this->setRenderCode(401);
                $this->setRenderMessage('认证失败');
                return $this->getRenderJson();
            }
            $url = "https://graph.qq.com/oauth2.0/me?access_token={$accessToken}&unionid=1";
            $res = getCurl($url);
            if (empty($res)) {
                $this->setRenderCode(500);
                $this->setRenderMessage('第三方登录失败');
                return $this->getRenderJson();
            }

            $validate_result = qqUtil::validateResult($res);
            $resArray = json_decode($validate_result, true);
            if (empty($resArray['openid'])) {
                $this->setRenderCode(500);
                $this->setRenderMessage('第三方登录失败');
                return $this->getRenderJson();
            }
            $requestData['oauth_id'] = $resArray['openid'];
            $requestData['union_id'] = $resArray['unionid'];
        }

        $userId = Oauth::oauthLogin(
            $requestData['oauth_type'],
            $requestData['oauth_id'],
            $requestData['nick_name'],
            $requestData['avatar'],
            $requestData['union_id'],
            'app'
        );
        $ban = UserLogic::check_ban(['user_id'=>$userId]);
        if(!empty($ban) && $ban['ban_end_time'] > time()){
            $this->setRenderCode(400);
            $this->setRenderMessage('账号已被禁用,'."解禁时间:".date("Y-m-d H:i:s",$ban['ban_end_time']));
            return $this->getRenderJson();
        }
        $token = TokenLogic::storeToken($userId);
        if (empty($token)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'store token error');
            return $this->getRenderJson();
        }
        $this->addRenderData('token', $token);

        $userInfoField = [
            'user_id',
            'username',
            'avatar',
            'money',
            'mobile',
            'email',
            'gender'
        ];
        $userInfoArray = User::get($userId)->visible($userInfoField)->toArray();
        $count = UserLabel::build()
            ->where(['user_id' => $userInfoArray['user_id']])
            ->count();
//        $userInfoArray['is_save'] = ($count >= 1) ? 1 : 0;
        $userInfoArray['is_save'] = 0;

        $this->addRenderData('user_info', $userInfoArray);

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月7日
     * description:第三方登录 web
     * @return \think\response\Json
     */
    public function webOauthLogin()
    {
        $requestData = $this->selectParam(
            ['code', 'state']
        );
        //PRIVATE_KEY
        if ($requestData['state'] != md5(PRIVATE_KEY)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('state验证失败');
            return $this->getRenderJson();
        }

        $code = $requestData['code'];

        try {
            $app_id = config('wechat')['WebAppID'];
            $app_secret = config('wechat')['WebAppSecret'];
            $result = wechatUtil::getWebAccessToken($app_id, $app_secret, $code);
//        array(6) {
//        ["access_token"] =&gt; string(86) "9bTqIU5jYgoPw_Syxv4a0h1BB9qPCyVyQyZQEsFNL-YIAZ5ApzmRBB4X06LMsfLzfoUX4RIVLeEUdI2qosBu_Q"
//        ["expires_in"] =&gt; int(7200)
//        ["refresh_token"] =&gt; string(86) "-3jYh8a72zaFzNE18HzqdzVMfDnp_9aVt-UYeFTJgvhHbzWtqUDa2CdlqAVg_ETGFgMi4zeaK5CbigAmQhMNCg"
//        ["openid"] =&gt; string(28) "of-4Hv7sKMRFznldEskrORspa5gM"
//        ["scope"] =&gt; string(12) "snsapi_login"
//        ["unionid"] =&gt; string(28) "oGK_VwqyzOFbS8Mh_ilImxNmBWYQ"
//        }
            $result = json_decode($result, true);
            if (!empty($result['errcode'])) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效code');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
            $union_id = $result['unionid'];
            $result = wechatUtil::getWebUserInfo($result['access_token'], $result['openid']);
            $result = json_decode($result, true);
            if (!empty($result['errcode'])) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效access_token');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
//        array(10) {
//        ["openid"] =&gt; string(28) "of-4Hv7sKMRFznldEskrORspa5gM"
//        ["nickname"] =&gt; string(8) "airon"
//        ["sex"] =&gt; int(1)
//        ["language"] =&gt; string(5) "zh_CN"
//        ["city"] =&gt; string(8) "Shenzhen"
//        ["province"] =&gt; string(9) "Guangdong"
//        ["country"] =&gt; string(2) "CN"
//        ["headimgurl"] =&gt; string(122) "http://wx.qlogo.cn/mmopen/vi_32/9ZxTP8bzWfpFYeocZvMm1K4udqlptLZ9iblFl21vlOl6mgLXXHvum09jLw9BLKMq20x4VYQDiaPWhvJsMKmc7vZw/0"
//        ["privilege"] =&gt; array(0) {
//        }
//     ["unionid"] =&gt; string(28) "oGK_VwqyzOFbS8Mh_ilImxNmBWYQ"
//        }
            $user_info['oauth_type'] = 'wechat';
            $user_info['oauth_id'] = $result['openid'];
            $user_info['nickname'] = $result['nickname'];
            $user_info['avatar'] = $result['headimgurl'];
        } catch (Exception $e) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'wechat error');
            return $this->getRenderJson();
        }
        $userId = Oauth::oauthLogin(
            $user_info['oauth_type'],
            $user_info['oauth_id'],
            $user_info['nickname'],
            $user_info['avatar'],
            $union_id,
            'pc'
        );
        $ban = UserLogic::check_ban(['user_id'=>$userId]);
        if(!empty($ban) && $ban['ban_end_time'] > time()){
            $this->setRenderCode(400);
            $this->setRenderMessage('账号已被禁用,'."解禁时间:".date("Y-m-d H:i:s",$ban['ban_end_time']));
            return $this->getRenderJson();
        }
        $token = TokenLogic::storeToken($userId);
        if (empty($token)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'store token error');
            return $this->getRenderJson();
        }
        $this->addRenderData('token', $token);

        $userInfoField = [
            'user_id',
            'username',
            'avatar',
            'money',
            'mobile',
            'email',
            'gender'
        ];
        $userInfoArray = User::get($userId)->visible($userInfoField)->toArray();
        $this->addRenderData('user_info', $userInfoArray);

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月7日
     * description:PC微信网页获取登录二维码链接
     * @return \think\response\Json
     */
    public function wechatWebLoginUrl()
    {
        $requestData = $this->selectParam(
            ['callback_url']
        );
        $appId = config('wechat')['WebAppID'];
        $redirect_uri = urlencode($requestData['callback_url']);
        $state = md5(PRIVATE_KEY);
        $login_url = "https://open.weixin.qq.com/connect/qrconnect?appid={$appId}&redirect_uri={$redirect_uri}&response_type=code&scope=snsapi_login&state={$state}";
        $this->setRenderCode(200);
        $this->addRenderData('login_url', $login_url);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月7日
     * description:PC QQ网页获取登录链接
     * @return \think\response\Json
     */
    public function qqWebLoginUrl()
    {
        $requestData = $this->selectParam(
            ['callback_url']
        );
        $appId = config('qq')['WebAppID'];
        $redirect_uri = urlencode($requestData['callback_url']);
        $state = md5(PRIVATE_KEY);
        $login_url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id={$appId}&redirect_uri={$redirect_uri}&state={$state}";
        $this->setRenderCode(200);
        $this->addRenderData('login_url', $login_url);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月7日
     * description:第三方登录 QQ web
     * @return \think\response\Json
     */
    public function webQQOauthLogin()
    {
        $requestData = $this->selectParam(
            ['code', 'state', 'callback_url']
        );
        //PRIVATE_KEY
        if ($requestData['state'] != md5(PRIVATE_KEY)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('state验证失败');
            return $this->getRenderJson();
        }

        $code = $requestData['code'];
        $redirect_uri = urlencode($requestData['callback_url']);
        try {
            $app_id = config('qq')['WebAppID'];
            $app_secret = config('qq')['WebAppKey'];
            $result = qqUtil::getWebAccessToken($app_id, $app_secret, $code, $redirect_uri);
            $validate_result = qqUtil::validateResult($result);
            if ($validate_result === false) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效code');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
            $arr_query = qqUtil::convertUrlQuery($result);
            if (empty($arr_query['access_token'])) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效code');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
            $access_token = $arr_query['access_token'];
            $result = qqUtil::getOpenId($access_token);
            $validate_result = qqUtil::validateResult($result);

            if ($validate_result === false || $result === false) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效code');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
            $arr_query = json_decode($validate_result, true);
            if (empty($arr_query['openid'])) {
                $this->setRenderCode(500);
                $this->setRenderMessage('无效code');
                $this->addRenderData('info', 'code error');
                return $this->getRenderJson();
            }
            $open_id = $arr_query['openid'];
            $union_id = @$arr_query['unionid'];
            $result = qqUtil::getWebUserInfo($access_token, $open_id);
// "{
//    \"ret\": 0,
//    \"msg\": \"\",
//    \"is_lost\":0,
//    \"nickname\": \"若能长久\",
//    \"gender\": \"男\",
//    \"province\": \"广东\",
//    \"city\": \"深圳\",
//    \"year\": \"1996\",
//    \"figureurl\": \"http:\/\/qzapp.qlogo.cn\/qzapp\/101428494\/FF5A14B732050CFE78AEE2AA988B0929\/30\",
//    \"figureurl_1\": \"http:\/\/qzapp.qlogo.cn\/qzapp\/101428494\/FF5A14B732050CFE78AEE2AA988B0929\/50\",
//    \"figureurl_2\": \"http:\/\/qzapp.qlogo.cn\/qzapp\/101428494\/FF5A14B732050CFE78AEE2AA988B0929\/100\",
//    \"figureurl_qq_1\": \"http:\/\/q.qlogo.cn\/qqapp\/101428494\/FF5A14B732050CFE78AEE2AA988B0929\/40\",
//    \"figureurl_qq_2\": \"http:\/\/q.qlogo.cn\/qqapp\/101428494\/FF5A14B732050CFE78AEE2AA988B0929\/100\",
//    \"is_yellow_vip\": \"0\",
//    \"vip\": \"0\",
//    \"yellow_vip_level\": \"0\",
//    \"level\": \"0\",
//    \"is_yellow_year_vip\": \"0\"
//}"
            $result = json_decode($result, true);
            if (!empty($result['ret']) && $result['ret'] != 0) {
                $this->setRenderCode(500);
                $this->setRenderMessage('get_user_info失败');
                return $this->getRenderJson();
            }
            $user_info['oauth_type'] = 'qq';
            $user_info['oauth_id'] = $open_id;
            $user_info['nickname'] = $result['nickname'];
            $user_info['avatar'] = empty($result['figureurl_qq_2']) ? $result['figureurl_qq_1'] : $result['figureurl_qq_2'];
            //大小为100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100×100的头像，但40×40像素则是一定会有。
        } catch (Exception $e) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'wechat error');
            return $this->getRenderJson();
        }
        $userId = Oauth::oauthLogin(
            $user_info['oauth_type'],
            $user_info['oauth_id'],
            $user_info['nickname'],
            $user_info['avatar'],
            $union_id,
            'pc'
        );
        $ban = UserLogic::check_ban(['user_id'=>$userId]);
        if(!empty($ban) && $ban['ban_end_time'] > time()){
            $this->setRenderCode(400);
            $this->setRenderMessage('账号已被禁用,'."解禁时间:".date("Y-m-d H:i:s",$ban['ban_end_time']));
            return $this->getRenderJson();
        }
        $token = TokenLogic::storeToken($userId);
        if (empty($token)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器异常');
            $this->addRenderData('info', 'store token error');
            return $this->getRenderJson();
        }
        $this->addRenderData('token', $token);

        $userInfoField = [
            'user_id',
            'username',
            'avatar',
            'money',
            'mobile',
            'email',
            'gender'
        ];
        $userInfoArray = User::get($userId)->visible($userInfoField)->toArray();
        $this->addRenderData('user_info', $userInfoArray);

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:第三方登录绑定手机
     * @return \think\response\Json
     */
    public function bind()
    {
        $token = $this->request->header('token');
        // 验证token
        if (empty($token)) {
            $this->setRenderCode(401);
            $this->setRenderMessage('无效会话，请重新登录');
            return $this->getRenderJson();
        }
        $this->tokenModel = $this->getTokenModel()->getTokenInfo($token);
        if (empty($this->tokenModel)) {
            $this->setRenderCode(401);
            $this->setRenderMessage('会话已过期，请重新登录');
            return $this->getRenderJson();
        }
        $this->userId = $this->tokenModel['user_id'];

        # 获取参数 验证参数
        $requestData = $this->selectParam(['mobile', 'code', 'password']);
        $this->check($requestData, 'Base.bind');
        $requestData['sms_type'] = 'bind';

        # 验证验证码
        $validateResult = SmsLogic::validateCode($requestData);
        if (!$validateResult) {
            $this->setRenderCode(400);
            $this->setRenderMessage('验证码错误');
            return $this->getRenderJson();
        }

        # 再次验证手机号码有效性
        $checkExist = User::build()
            ->field(['user_id', 'mobile'])->where(['mobile' => $requestData['mobile'],'status' => 1])->find();
        if (!empty($checkExist)) {
            $this->setRenderCode(400);
            $this->setRenderMessage('该手机号已被注册');
            return $this->getRenderJson();
        }

        # 注册用户
        $registerResult = User::bindUser(
            $this->userId,
            $requestData['mobile'],
            $requestData['password']
        );
        if (!$registerResult) {
            $this->setRenderCode(500);
            $this->setRenderMessage('绑定失败了');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('绑定成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月14日
     * description:用户退出登录
     * @return \think\response\Json
     */
    public function logout()
    {
        $this->getTokenModel()->disableToken(get_token());
        $this->getTokenModel()->setPushId(get_token());
        $this->setRenderCode(200);
        return $this->getRenderJson();
    }
}