<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/31
 * Time: 14:54
 */

namespace app\api\controller;

use app\api\logic\NewsLogic;


class News extends Base
{
    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:获取资讯列表
     * @return \think\response\Json
     */
    public function getList()
    {
        $result = NewsLogic::getList($this->pageIndex, $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('news_list', $result, false);
        $this->addRenderData('count', NewsLogic::getCount(), false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月31日
     * description:资讯详情
     * @return \think\response\Json
     */
    public function getInfo()
    {
        $requestData = $this->selectParam(['news_id']);
        $this->check($requestData, 'News.getInfo');
        $result = NewsLogic::getInfo($requestData['news_id']);
        if (empty($result)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('资讯不存在');
            return $this->getRenderJson();
        }
        NewsLogic::incLookCount($requestData['news_id']);
        $result['news_id'] = $result['id'];
        unset($result['id']);
        $this->setRenderCode(200);
        $this->addRenderData('news_detail', $result, false);
        return $this->getRenderJson();
    }
}