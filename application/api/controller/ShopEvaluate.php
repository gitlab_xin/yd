<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 10:25
 */

namespace app\api\controller;

use app\api\logic\ShopEvaluateLogic as EvaluateLogic;

class ShopEvaluate extends Base
{
    /**
     * @author: Airon
     * @time: 2017年8月7日
     * description:商品评价列表
     * @return \think\response\Json
     */
    public function getList()
    {
        $requestData = $this->selectParam(['product_id', 'evaluate_type', 'user_id']);
        $requestData['evaluate_type'] = empty($requestData['evaluate_type']) ? 'all' : $requestData['evaluate_type'];
        $requestData['user_id'] = empty($requestData['user_id']) ? 0 : $requestData['user_id'];
        $this->check($requestData, 'ShopProduct.evaluateType');
        $result = EvaluateLogic::getList(
            $requestData['product_id'],
            $requestData['evaluate_type'],
            $requestData['user_id'],
            $this->pageIndex,
            $this->pageSize);
        $count_result = EvaluateLogic::getTypeCount($requestData['product_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('evaluate_list', $result, false);
        $this->addRenderData('count_list', $count_result, false);
        return $this->getRenderJson();
    }

    public function praise()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['evaluate_id', 'praise_type']);

        $this->check($requestData, 'ShopProduct.evaluatePraise');

        $result = EvaluateLogic::articlePraise($this->getUserId(),$requestData['evaluate_id'], $requestData['praise_type']);

        $msg = $requestData['praise_type'] == 'praise' ? '点赞' : '取消点赞';

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage($msg . '失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage($msg . '成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:商品评价
     * @return \think\response\Json
     */
    public function evaluate()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['order_id', 'star','content','img_src_list','is_anonymous']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'ShopEvaluate.evaluate');

        $result = EvaluateLogic::evaluate($requestData);

        switch ($result){
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('订单状态修改失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('评价失败');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('订单不存在');
                break;
            case 1:
                $this->setRenderCode(200);
                $this->setRenderMessage('评价成功');
                break;
            case 2:
                $this->setRenderCode(500);
                $this->setRenderMessage('请勿重复评价');
                break;
        }
        return $this->getRenderJson();
    }

    public function getMyEvaluate()
    {
        $this->validateToken();

        $requestData = $this->selectParam(['order_id']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'ShopEvaluate.myEvaluate');

        $result = EvaluateLogic::getMyEvaluate($requestData);

        if($result == null){
            $this->setRenderCode(500);
            $this->setRenderMessage('数据不存在');
        }else{
            $this->setRenderCode(200);
            $this->addRenderData('detail',$result,false);
            $this->setRenderMessage('获取成功');
        }
        return $this->getRenderJson();
    }
}