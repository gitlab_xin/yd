<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:20
 */

namespace app\api\controller;

use app\api\logic\BannerLogic;

class Banner extends Base
{
    public function index()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['module_type', 'device_type']);
        $this->check($requestData, 'Banner.list');

        $result = BannerLogic::get_list($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('banner_list', $result ,false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:获取定制需求banner
     * @return \think\response\Json
     */
    public function getDemandBanners()
    {
        $result = BannerLogic::getDemandBanners();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('banner_list', $result ,false);
        return $this->getRenderJson();
    }
}