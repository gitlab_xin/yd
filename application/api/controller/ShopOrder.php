<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/15
 * Time: 10:36
 */

namespace app\api\controller;

use app\api\logic\ShopOrderLogic as OrderLogic;
use app\common\model\ConfigOrderCancelReason;
use think\Request;

class ShopOrder extends Base
{
    private $_orderLogic;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
        $this->_orderLogic = new OrderLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:获取我的订单
     * @return \think\response\Json
     */
    public function getMyOrders()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['status']);
        $this->check($data, 'ShopOrder.list');
        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = $this->_orderLogic->getOrdersByUser($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('orders', $result['orders'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:查看我的订单详情
     * @return \think\response\Json
     */
    public function getOrderDetail()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id']);
        $this->check($data, 'ShopOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->_orderLogic->getOrderDetail($data);
        $address = $this->_orderLogic->getOrderAddress($data);

        if ($result == null || $address == null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('查询不到该订单信息');
            return $this->getRenderJson();
        }
        $logistics = $this->_orderLogic->getLogisticsBrief(['logistics_id' => $result['logistics_id'], 'user_id' => $data['user_id']]);
        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('order_detail', $result, false);
        $this->addRenderData('order_address', $address, false);
        $this->addRenderData('order_logistics', $logistics, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:确认收货
     * @return \think\response\Json
     */
    public function confirmOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id']);
        $this->check($data, 'ShopOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->_orderLogic->confirmOrder($data);

        $this->setRenderCode(500);
        switch ($result) {
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有待收货的订单才能确认收货');
                break;
            case 2:
                $this->setRenderMessage('确认失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('确认成功');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月15日
     * description:搜索订单
     * @return \think\response\Json
     */
    public function searchOrders()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['status', 'keyword']);
        $data['status'] = 'all';//需求修改,订单不需要分状态查询,所以预定义为all,以后需求改动则注释即可
        $this->check($data, 'ShopOrder.search');
        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = $this->_orderLogic->getOrdersByUser($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('orders', $result['orders'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月26日
     * description:获取物流详情
     * @return \think\response\Json
     */
    public function getLogisticsDetail()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['logistics_id']);
        $this->check($data, 'ShopOrderLogistics.detail');
        $data['user_id'] = $this->getUserId();

        $result = $this->_orderLogic->getLogistics($data);

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('不存在该订单的物流信息');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('order_num', $result['order_num'], false);
                $this->addRenderData('type', $result['type'], false);
                $this->addRenderData('detail', $result['detail'], false);
                $this->addRenderData('contact', $result['contact'], false);
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月13日
     * description:申请退款
     * @return \think\response\Json
     */
    public function cancelOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id','content','reason']);
        $this->check($data, 'ShopOrder.refund');
        $data['user_id'] = $this->getUserId();
        $result = $this->_orderLogic->cancelOrder($data);

        $this->setRenderCode(500);
        switch ($result) {
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有待发货的订单才能申请退款');
                break;
            case 2:
                $this->setRenderMessage('确认失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('确认成功');
                break;
        }

        return $this->getRenderJson();
    }

    public function cancelUnpaidOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id', 'cancel_reason']);
        $this->check($data, 'ShopOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->_orderLogic->cancelUnpaidOrder($data);

        $this->setRenderCode(500);
        switch ($result) {
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有未支付的订单才能取消订单');
                break;
            case 2:
                $this->setRenderMessage('取消失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('取消成功');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function configCancelReason()
    {
        $requestData = $this->selectParam(['type'=>'unpaid']);

        $data = ConfigOrderCancelReason::build()->where(['type'=>$requestData['type']])->field(['content as cancel_reason'])->select()->toArray();
        if ($data === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');

        }
        $data = array_column($data, 'cancel_reason');
        $this->setRenderCode(200);
        $this->addRenderData('cancel_reason',$data,false);
        return $this->getRenderJson();
    }
}