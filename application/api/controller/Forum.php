<?php
/**
 * Created by PhpStorm.
 * User: Jun
 * Date: 2017/7/25
 * Time: 15:23
 */

namespace app\api\controller;

use \app\api\logic\ForumLogic;
use app\common\model\ForumArticle as ForumArticleModel;
use app\common\model\ForumCollect as ForumCollectModel;

class Forum extends Base
{

    public $forumLogic;

    /**
     * @author: Jun
     * @time: 2017年7月25日 16:28
     * @description:初始化ForumLogic
     */
    public function __construct()
    {
        parent::__construct();
        $this->forumLogic = new ForumLogic();
    }

    /**
     * 获取帖子列表
     * @author: Jun
     * @time: 2017年7月25日
     * description: 获取不同类型的帖子列表
     * @return \think\response\Json
     */
    public function getArticleList()
    {
        $requestData = $this->selectParam(['article_type', 'at_user_id','sort']);

        // 用户未登录默认值为 "0"
        $requestData['at_user_id'] = empty($requestData['at_user_id']) ? 0 : $requestData['at_user_id'];
        // 没有sort参数时默认为hot
        $requestData['sort'] = empty($requestData['sort']) ? 'hot' : $requestData['sort'];

        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;
        $this->check($requestData, 'ForumArticle.list');

        $model_result = $this->forumLogic->getArticleList($requestData);
        $count = $this->forumLogic->getArticleCount($requestData);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('article_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * 用户帖子发表
     * @author: Jun
     * @time: 2017年7月26日
     * description: 用户发表帖子
     * @return \think\response\Json
     */
    public function addArticle()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收用户输入，验证器验证
        $requestData = $this->selectParam(['article_title', 'article_classify', 'article_content','scheme_id','product_id']);
        $requestData['scheme_id'] = $requestData['scheme_id'] == ''?0:$requestData['scheme_id'];
        $requestData['product_id'] = $requestData['product_id'] == ''?0:$requestData['product_id'];

        $this->check($requestData, 'ForumArticle.add');

        $requestData['user_id'] = $user_id;
        $result = $this->forumLogic->addArticle($requestData);


        switch ($result){
            case 1:
                $this->setRenderCode(200);
                $this->setRenderMessage('发表成功');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('发表失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('异常错误');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('这个方案不属于你');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('你没购买过这件商品');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * 帖子详情
     * @author: Jun
     * @time: 2017年7月26日
     * description: 查看帖子详情
     * @return \think\response\Json
     */
    public function articleDetail()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['article_id', 'at_user_id']);
        $requestData['at_user_id'] = empty($requestData['at_user_id']) ? 0 : $requestData['at_user_id'];
        $this->check($requestData, 'ForumArticle.info');
        $result = $this->forumLogic->getArticleDetail($requestData);
        $this->addRenderData('article_detail', $result, false);
        return $this->getRenderJson();
    }

    /**
     * 点赞 and 取消点赞
     * @author: Ju
     * @time: 2017年7月26日
     * description: 点赞和取消点赞，自动识别是否已经点赞
     * @return \think\response\Json
     */
    public function articlePraise()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['article_id','praise_type']);

        $this->check($requestData, 'ForumArticle.praise');

        $result = $this->forumLogic->articlePraise($user_id, $requestData);

        $msg = $requestData['praise_type'] == 'praise'?'点赞':'取消点赞';

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage($msg.'失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage($msg.'成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年7月27日
     * description:收藏帖子
     * @return \think\response\Json
     */
    public function articleCollect()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['article_id', 'collect_type']);

        $this->check($requestData, 'ForumArticle.collect');

        $bool = $this->forumLogic->check(['article_id'=>$requestData['article_id']]);
        if ($bool === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('帖子不存在');
            return $this->getRenderJson();
        }
        $bool = ForumCollectModel::check($this->getUserId(),$requestData['article_id']);
        switch ($requestData['collect_type']) {
            case "collect":
                if(!empty($bool)){
                    $result = true;
                    break;
                }
                $result = forumLogic::collect($this->getUserId(), $requestData['article_id']);
                ForumArticleModel::build()->where(['article_id'=>$requestData['article_id']])->setInc('collect_count');

                break;
            case "cancel":
                if(empty($bool)){
                    $result = true;
                    break;
                }
                $result = forumLogic::cancel($this->getUserId(), $requestData['article_id']);
                ForumArticleModel::build()->where(['article_id'=>$requestData['article_id']])->setDec('collect_count');

                break;
            default:
                $result = false;
        }
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('操作失败');
            return $this->getRenderJson();
        }
        $message = $requestData['collect_type'] == 'collect'?"收藏成功":"取消收藏成功";
        $this->setRenderMessage($message);
        return $this->getRenderJson();

    }

    /**
     * @author: Rudy
     * @time: 2017年8月2日
     * description:帖子搜索
     * @return \think\response\Json
     */
    public function articleSearch()
    {
        $requestData = $this->selectParam(['article_type','keyword','at_user_id','sort']);

        // 获取关键字
        $requestData['keyword'] = empty($requestData['keyword']) ? '' : $requestData['keyword'];
        // 用户未登录默认值为 "0"
        $requestData['at_user_id'] = empty($requestData['at_user_id']) ? 0 : $requestData['at_user_id'];
        // 没有sort参数时默认为hot
        $requestData['sort'] = empty($requestData['sort']) ? 'hot' : $requestData['sort'];

        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;
        $this->check($requestData, 'ForumArticle.search');

        $model_result = $this->forumLogic->getArticleList($requestData);
        $count = $this->forumLogic->getArticleCount($requestData);
        $this->forumLogic->markKeyword($requestData['keyword']);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->addRenderData('article_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description: 获取我的帖子列表
     * @return \think\response\Json
     */
    public function getMyArticleList()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData['article_type'] = 'all';
        $requestData['user_id'] = $requestData['at_user_id'] = $user_id;
        $requestData['sort'] = 'time';
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;
        $this->check($requestData, 'ForumArticle.list');

        $model_result = $this->forumLogic->getArticleList($requestData);
        $count = $this->forumLogic->getArticleCount($requestData);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('article_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:删除帖子(修改帖子状态is_deleted状态为1)
     * @return \think\response\Json
     */
    public function delOwnerArticle()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData = $this->selectParam(['article_id']);
        $this->check($requestData, 'ForumArticle.delete');
        $requestData['user_id'] = $user_id;

        $model_result = $this->forumLogic->delArticle($requestData);
        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('删除失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('删除成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description: 获取我的帖子列表
     * @return \think\response\Json
     */
    public function getMyArticleCollections()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData['article_type'] = 'all';
        $requestData['at_user_id'] = $user_id;
        $requestData['sort'] = 'time';
        $requestData['collect'] = 1;
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;
        $this->check($requestData, 'ForumArticle.list');

        $model_result = $this->forumLogic->getArticleList($requestData);
        $count = $this->forumLogic->getArticleCollectionCount($user_id);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('article_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月17日
     * description:获取热门搜索
     * @return \think\response\Json
     */
    public function getHotSearch()
    {
        $result = $this->forumLogic->getHotSearch();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $result = array_column($result, 'key');
        $this->addRenderData('key_list', $result, false);
        return $this->getRenderJson();
    }
}