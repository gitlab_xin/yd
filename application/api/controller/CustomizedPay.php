<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 17-11-28
 * Time: 上午11:45
 */

namespace app\api\controller;


use app\api\logic\CustomizedPayLogic;
use app\api\logic\UserAreaLogic;

class CustomizedPay extends Base
{
    public function buildOrder()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id', 'delivery_time', 'count', 'pay_type', 'user_remarks', 'area_id', 'prepay_id', 'trade_type']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'CustomizedPay.buildOrder');

        $result = CustomizedPayLogic::buildOrder($requestData);

        if (is_int($result)) {
            $this->setRenderCode(500);
        } else {
            $this->setRenderMessage('生成订单成功');
            $this->addRenderData('result', $result['result'], false);
            $this->addRenderData('order_id', $result['order_id'], false);
        }

        return $this->getRenderJson();
    }

    public function settlement()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id', 'count']);
        $requestData['user_id'] = $this->getUserId();
//        $this->check($requestData, 'CustomizedPay.confirmOrder');
        $area_info = UserAreaLogic::getDefault($this->getUserId());
        if (empty($area_info)) {
            $area_info = null;
        }
        list($statement, $charge) = CustomizedPayLogic::getCharge($requestData['scheme_id'], $requestData['count']);

        $this->setRenderCode(200);
        $this->addRenderData('area_info', $area_info, false);
        $this->addRenderData('statement', $statement, false);
        $this->addRenderData('charge', $charge, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2018年2月8日
     * description:重新调用支付
     * @return \think\response\Json
     */
    public function payAgain()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['order_id']);

        $result = CustomizedPayLogic::payAgain($requestData['order_id'], $this->getUserId());

        if (is_int($result)) {
            $this->setRenderCode(500);
            switch ($result) {
                case 0:
                    $this->setRenderMessage('订单信息不存在');
                    break;
                case 1:
                    $this->setRenderMessage('发起支付失败');
                    break;
            }
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('生成订单成功');
            $this->addRenderData('result', $result['result'], false);
            $this->addRenderData('pay_type', $result['order_info']['pay_type'], false);
            $this->addRenderData('order_id', $requestData['order_id'], false);
        }

        return $this->getRenderJson();
    }

    /**
     * User: zhaoxin
     * Date: 2019/12/6
     * Time: 5:57 下午
     * description
     * @return \think\response\Json
     * @throws \think\Exception
     */
    public function checkOrder()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['out_trade_no']);
        $this->check($requestData, 'ShopPay.outTradeNo');
        $order_bool = CustomizedPayLogic::checkOrder($requestData['out_trade_no']);
        $is_pay = empty($order_bool) ? 'no' : 'yes';
        $this->setRenderCode(200);
        $this->addRenderData('is_pay', $is_pay, false);
        return $this->getRenderJson();
    }
}