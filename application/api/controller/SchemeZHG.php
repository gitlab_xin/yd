<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/26
 * Time: 16:32
 */

namespace app\api\controller;


use app\api\logic\SchemeZHGLogic;
use app\api\logic\SchemeZHGOrderLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

/**
 * 综合柜类
 * Class SchemeZHG
 * @package app\api\controller
 */
class SchemeZHG extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年9月29日
     * description:计算不同宽度下误差值的方案数量
     * @return \think\response\Json
     */
    public function math()
    {
        $data = $this->selectParam(['width', 'height']);

        $this->check($data, 'SchemeZHG.math');

        $logic = new SchemeZHGLogic();

        $result = $logic->schemeMath($data);

        $this->addRenderData('count', $result, false);

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月30日
     * description:生成推荐方案
     * @return \think\response\Json
     */
    public function create()
    {
        $data = $this->selectParam(['width', 'height', 'errorrange']);

        $this->check($data, 'SchemeZHG.create');

        $logic = new SchemeZHGLogic();

        $productList = $logic->createZHGScheme($data);

        $this->addRenderData('schemes', $productList, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:保存方案
     * @return \think\response\Json
     */
    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme']);

        $schemeLogic = new SchemeZHGLogic();

        $params = $schemeLogic->getOriginParams($requestData['scheme']);
        $this->check($params, 'SchemeZHG.store');

        $schemeBean = $schemeLogic->buildScheme($requestData['scheme']);

        $schemeId = $schemeLogic->store($schemeBean, $requestData['scheme'], $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:保存方案
     * @return \think\response\Json
     */
    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'scheme_id']);
        $schemeData = array_shift($requestData);
        $requestData['user_id'] = $this->getUserId();

        $schemeLogic = new SchemeZHGLogic();

        $params = $schemeLogic->getOriginParams($schemeData);
        $this->check(array_merge($params, $requestData), 'SchemeZHG.update');

        $schemeBean = $schemeLogic->buildScheme($schemeData);

        if (!$schemeLogic->update($schemeBean, $schemeData, $requestData['user_id'], $requestData['scheme_id'])) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
        } else {
            $this->setRenderMessage('修改成功');
            $this->addRenderData("id", $requestData['scheme_id']);
        }


        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年10月7日
     * description:结算
     * @return \think\response\Json
     */
    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHG.complete');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);

        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($schemeBean->getScheme_dis_price(), 2)]);

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHG.complete2');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);

        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price(), 2));
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('gt_dis_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('结算成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }


}