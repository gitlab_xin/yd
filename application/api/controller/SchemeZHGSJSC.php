<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/9/26
 * Time: 16:32
 */

namespace app\api\controller;


use app\api\logic\SchemeZHGOrderLogic;
use app\api\logic\SchemeZHGSJSCLogic;
use app\common\model\CustomizedScheme;
use app\common\tools\RedisUtil;

/**
 * 书柜+写字桌组合
 * Class SchemeZHGSJSC
 * @package app\api\controller
 */

class SchemeZHGSJSC extends Base
{
    public function math()
    {
        $data = $this->selectParam(['width', 'scheme_s_type']);
        // scheme_s_type
        // 平行布局：1 垂直布局：0

        $this->check($data, 'SchemeZHGSJSC.math');

        $logic = new SchemeZHGSJSCLogic();

        $result = $logic->schemeMath($data);

        $this->addRenderData('count', $result, false);

        return $this->getRenderJson();
    }

    public function create()
    {
        $data = $this->selectParam([
            'width', 'scheme_s_type',
            'error_range',
        ]);

        $this->check($data, 'SchemeZHGSJSC.create');

        $logic = new SchemeZHGSJSCLogic();

        $productList = $logic->schemeCreate($data);

        $this->addRenderData('schemes', $productList, false);
        return $this->getRenderJson();
    }

    public function store()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme']);

        $schemeLogic = new SchemeZHGSJSCLogic();

        $params = $schemeLogic->getOriginParams($requestData['scheme']);
        $this->check($params, 'SchemeZHGSJSC.store');

        $schemeBean = $schemeLogic->buildScheme($requestData['scheme']);

        $schemeId = $schemeLogic->store($schemeBean, $requestData['scheme'], $this->getUserId());

        $this->setRenderMessage('保存成功');
        $this->addRenderData("id", $schemeId);

        return $this->getRenderJson();
    }

    public function update()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme', 'scheme_id']);
        $schemeData = array_shift($requestData);
        $requestData['user_id'] = $this->getUserId();


        $schemeLogic = new SchemeZHGSJSCLogic();

        $params = $schemeLogic->getOriginParams($schemeData);
        $this->check(array_merge($params, $requestData), 'SchemeZHGSJSC.update');

        $schemeBean = $schemeLogic->buildScheme($schemeData);

        if (!$schemeLogic->update($schemeBean, $schemeData, $requestData['user_id'], $requestData['scheme_id'])) {
            $this->setRenderCode(500);
            $this->setRenderMessage('修改失败');
        } else {
            $this->setRenderMessage('修改成功');
            $this->addRenderData("id", $requestData['scheme_id']);
        }


        return $this->getRenderJson();
    }

    public function complete()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHGSJSC.complete');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);

        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGSJSCLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price()), 2);
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('保存成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        CustomizedScheme::build()
            ->where(['id' => $requestData['scheme_id']])
            ->update(['scheme_price' => formatMoney($schemeBean->getScheme_dis_price(), 2)]);

        return $this->getRenderJson();
    }

    public function complete2()
    {
//        $this->validateToken();
        $requestData = $this->selectParam(['scheme_id']);
//        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'SchemeZHGSJSC.complete2');

        $schemeModel = CustomizedScheme::get($requestData['scheme_id']);

        $schemeData = json_decode($schemeModel['serial_array'], true);
        $schemeLogic = new SchemeZHGSJSCLogic();
        $schemeBean = $schemeLogic->buildScheme($schemeData);

        $orderLogic = new SchemeZHGOrderLogic();
        //统计综合柜功能件
        $schemeBean = $orderLogic->scoreZHGSchemeProducts($schemeBean);
        //统计综合柜组合功能件
        $schemeBean = $orderLogic->scoreZHGSchemeZJProducts($schemeBean);
        //统计综合柜板材面积
        $schemeBean = $orderLogic->scoreZHGSchemeplates($schemeBean);
        //统计综合柜五金件
        $schemeBean = $orderLogic->scoreZHGSchemeWujin($schemeBean);


        $zjProducts = [];
        foreach ($schemeBean->getScheme_score_zj_products() as $item) {
            $item = change2ProductBean($item);
            $zjProducts[] = $item->scoreInfo();
        }
        $scoreProducts = [];
        foreach ($schemeBean->getScheme_score_products() as $item) {
            $item = change2ProductBean($item);
            $scoreProducts[] = $item->scoreInfo();
        }
        $plateProducts = [];
        foreach ($schemeBean->getScheme_score_plates() as $item) {
            $item = change2ProductBean($item);
            $plateProducts[] = $item->scoreInfo();
        }
        $wujinProducts = [];
        foreach ($schemeBean->getScheme_score_wujin_products() as $item) {
            $item = change2ProductBean($item);
            $wujinProducts[] = $item->scoreInfo();
        }

        $this->addRenderData('scheme_pic', $schemeBean->getScheme_pic());
        $this->addRenderData('zj_products', $zjProducts, false);
        $this->addRenderData('score_products', $scoreProducts, false);
        $this->addRenderData('plate_products', $plateProducts, false);
        $this->addRenderData('wujin_products', $wujinProducts, false);
        $this->addRenderData('gt_price', formatMoney($schemeBean->getScheme_gt_price(), 2));
        $this->addRenderData('dis_gt_price', formatMoney($schemeBean->getScheme_dis_gt_price(), 2));
        $this->addRenderData('zj_price', formatMoney($schemeBean->getScheme_zj_price(), 2));
        $this->addRenderData('dis_zj_price', formatMoney($schemeBean->getScheme_dis_zj_price()), 2);
        $this->addRenderData('wujin_price', formatMoney($schemeBean->getScheme_wujin_price(), 2));
        $this->addRenderData('dis_wujin_price', formatMoney($schemeBean->getScheme_dis_wujin_price(), 2));
        $this->addRenderData('plate_price', formatMoney($schemeBean->getScheme_plate_price(), 2));
        $this->addRenderData('dis_plate_price', formatMoney($schemeBean->getScheme_dis_plate_price(), 2));
        $this->addRenderData('scheme_price', formatMoney($schemeBean->getScheme_price(), 2));
        $this->addRenderData('scheme_dis_price', formatMoney($schemeBean->getScheme_dis_price(), 2));
        $this->addRenderData('discount', formatMoney($schemeBean->getScheme_discount(), 2));

        $this->setRenderMessage('保存成功');

        //写入缓存
        $schemeComplete = $this->renderData;
        RedisUtil::getInstance()->hSet("scheme_complete", $requestData['scheme_id'], serialize($schemeComplete));

        return $this->getRenderJson();
    }
}