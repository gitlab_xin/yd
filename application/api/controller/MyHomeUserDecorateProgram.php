<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2018/4
 * Time: 16:02
 * 户型
 */

namespace app\api\controller;

use app\api\logic\MyHomeBuildingsLogic;
use app\api\logic\MyHomeHouseTypeLogic;
use app\api\logic\MyHomeUserDecorateProgramLogic;

class MyHomeUserDecorateProgram extends Base
{

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:上传装修方案
     * @return \think\response\Json
     */
    public function insertDecorateProgram()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['house_id','name', 'img_src', 'serial_array']);
        $this->check($requestData, 'MyHomeUserDecorateProgram.insert');
        $check = MyHomeHouseTypeLogic::checkHouseType($requestData['house_id']);
        if (empty($check)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('户型不存在');
            return $this->getRenderJson();
        }

        $requestData['serial_array'] = json_encode($requestData['serial_array'], JSON_UNESCAPED_UNICODE);
        $requestData['user_id'] = $this->getUserId();
        $result = MyHomeUserDecorateProgramLogic::insertDecorateProgram($requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('上传成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:编辑装修方案
     * @return \think\response\Json
     */
    public function saveDecorateProgram()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['program_id', 'name', 'img_src', 'serial_array']);
        $this->check($requestData, 'MyHomeDecorateProgram.save');

        $requestData['serial_array'] = json_encode($requestData['serial_array'], JSON_UNESCAPED_UNICODE);
        $requestData['user_id'] = $this->getUserId();
        $result = MyHomeUserDecorateProgramLogic::saveDecorateProgram($requestData['program_id'], $requestData);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('编辑失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('编辑成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2018年4月
     * description:获取装修方案详情
     * @return \think\response\Json
     */
    public function getDecorateProgram()
    {
        $requestData = $this->selectParam(['program_id']);
        $this->check($requestData, 'MyHomeDecorateProgram.getInfo');
        $result = MyHomeUserDecorateProgramLogic::getDecorateProgram($requestData['program_id']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $result['serial_array'] = json_decode($result['serial_array'], JSON_UNESCAPED_UNICODE);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('info', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author:Airon
     * @time:2018年5月
     * description:获取装修方案列表
     * @return \think\response\Json
     */
    public function getProgramList(){
        $this->validateToken();
        $result = MyHomeUserDecorateProgramLogic::getMyProgram($this->getUserId(),$this->pageIndex,$this->pageSize  );
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }
        $this->setRenderMessage('获取成功');
        $this->addRenderData('program_list', $result, false);
        return $this->getRenderJson();
    }
}