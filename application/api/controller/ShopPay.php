<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 15:10
 */

namespace app\api\controller;

use app\api\logic\ShopPayLogic;
use app\api\logic\UserAreaLogic;
use app\common\tools\RedisUtil;
use think\Db;
use app\api\logic\ShopOrderLogic;
use app\common\tools\wechatPay;
use app\common\tools\alipayClient;

class ShopPay extends Base
{
    public function demo()
    {
//        exit;
        //支付宝APP支付DEMO
        $appId = config('alipay')['AppID'];
        $partner_public_key = config('alipay')['partnerPublicKey'];
        $partner_private_key = config('alipay')['rsaPrivateKey'];
        $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
        $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号
        $requestData = $alipayClient->order($out_trade_no, '0.01', config('alipay')['pay_charge'], config('alipay')['pay_charge'], 123);
        $this->setRenderCode(200);
        $this->addRenderData('sign', $requestData);
        return $this->getRenderJson();
    }

    public function alipayRefundDemo()
    {
//        exit;
        //支付宝退款操作DEMO
        $appId = config('alipay')['AppID'];
        $partner_public_key = config('alipay')['partnerPublicKey'];
        $partner_private_key = config('alipay')['rsaPrivateKey'];
        $alipay_public_key = config('alipay')['alipayPublicKey'];
        $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key, $alipay_public_key);
        $out_trade_no = "77293a8aee5b83799a544dfa28aae333";//bill_no
        $out_refund_no = md5(uniqid(mt_rand(), true));//退款订单号 单笔多退需要保证唯一性
        $requestData = $alipayClient->refund($out_trade_no, '0.01', $out_refund_no, "订单已取消");
        dump($requestData);
    }

    public function alipayWebPay()
    {
//        exit;
        //支付宝网页支付DEMO
        $appId = config('alipay')['AppID'];
        $partner_public_key = config('alipay')['partnerPublicKey'];
        $partner_private_key = config('alipay')['rsaPrivateKey'];
        $pay_charge = config('alipay')['pay_charge'];
        $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
        $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号
        $requestData = $alipayClient->webOrder($out_trade_no, '0.01', $pay_charge, $pay_charge, 'https://test.inffur.com/index.php/api/Callback/alipay');
        echo $requestData;
    }

    /**
     * @author: Airon
     * @time: 2017年7月28日
     * description:购物车支付
     * @return \think\response\Json
     */
    public function shoppingCartCharge()
    {
        $requestData = $this->selectParam(['cart_array', 'pay_type', 'area_id', 'prepay_id', 'trade_type']);
        $requestData['trade_type'] = empty($requestData['trade_type']) ? 'APP' : $requestData['trade_type'];
        $requestData['cart_array'] = json_decode($requestData['cart_array'], true);//验证前转数组,不然要解析两次JSON
        $this->check($requestData, 'ShopPay.shoppingCart');

        $this->validateToken();

        $cart_id_list = array_column($requestData['cart_array'], 'cart_id');
        if (!is_array($cart_id_list)) {
            $this->setRenderCode(402);
            $this->setRenderMessage('参数不合法');
            return $this->getRenderJson();
        }

        $ShopOrderLogic = new ShopOrderLogic();

        Db::startTrans();

        $userArea = UserAreaLogic::getInfo($this->getUserId(), $requestData['area_id']);//收货地址信息
        if (empty($userArea)) {
            Db::rollback();
            $this->setRenderCode(500);
            $this->setRenderMessage('收货地址不存在');
            return $this->getRenderJson();
        }

        $userArea = $userArea->toArray();//object转array
        unset($userArea['code']);

        $cartCheckCount = $ShopOrderLogic->cartCheckCount($cart_id_list, $this->getUserId());
        //判断商品是否已下架或者已删除
        $deletedOrSale = $ShopOrderLogic->cartDeletedOrSale($cart_id_list, $this->getUserId());
        if (!empty($deletedOrSale)) {
            Db::rollback();
            $this->setRenderCode(501);
            $this->setRenderMessage('部分结算商品已下架');
            $this->addRenderData('error_cart', $deletedOrSale, false);
            return $this->getRenderJson();
        }

        //判断商品规格是否已删除
        $standardDelete = $ShopOrderLogic->cartStandardDelete($cart_id_list, $this->getUserId());
        if (!empty($standardDelete)) {
            Db::rollback();
            $this->setRenderCode(501);
            $this->setRenderMessage('部分结算商品规格已下架');
            $this->addRenderData('error_cart', $standardDelete, false);
            return $this->getRenderJson();
        }

        if (is_array($cartCheckCount) && count($cartCheckCount) != count($cart_id_list)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('结算商品与购物车内数据不符合');
            return $this->getRenderJson();
        }

        //判断库存量是否足够
        $checkStock = $ShopOrderLogic->cartCheckStock($cart_id_list, $this->getUserId());
        if (!empty($checkStock)) {
            Db::rollback();
            $this->setRenderCode(502);
            $this->setRenderMessage('部分结算商品库存不足');
            $this->addRenderData('error_cart', $checkStock, false);
            return $this->getRenderJson();
        }

        $ShopOrderLogic->lockStock($cart_id_list, $this->getUserId());//锁库存数据,实测联表时锁不生效

        $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号

        //写入生成订单数据
        $result = $ShopOrderLogic->cartAddOrder($cart_id_list, $this->getUserId(), $out_trade_no, $userArea, $requestData);
        //减去库存 增加销量

        if ($result === false || !$result['fee']) {
            Db::rollback();
            $this->setRenderCode(500);
            $this->setRenderMessage('生成订单失败');
            return $this->getRenderJson();
        }
        RedisUtil::getInstance()->hSet('order_pay_info', $out_trade_no, $requestData['trade_type'] . '|' . $requestData['prepay_id']);

        switch ($requestData['pay_type']) {
            case 'wechat':
                $body = config('wechat')['wx_pay_charge'];
                //$attach = implode(',',$order_num_list);

                $appId = config('wechat')['AppID'];
                $mchId = config('wechat')['Pay']['MchId'];
                $appKey = config('wechat')['Pay']['Key'];
                $wechatPay = new WechatPay($appId, $mchId, $appKey);

                // $requestData = $wechatPay->order($out_trade_no, $fee, requestData['prepay_id'], $body, "",$requestData['trade_type']);
                $requestData = $wechatPay->order($out_trade_no, 0.01 * count($cart_id_list), $requestData['prepay_id'], $body,
                    implode(",", $cart_id_list), $requestData['trade_type']);//todo::测试临时

                if ($requestData === false) {
                    Db::rollback();
                    $this->setRenderCode(500);
                    $this->setRenderMessage('生成订单失败');
                    return $this->getRenderJson();
                }
                break;
            //微信是后台调用接口拿到是否生成订单的返回结果
            case 'alipay':
                $appId = config('alipay')['AppID'];
                $partner_public_key = config('alipay')['partnerPublicKey'];
                $partner_private_key = config('alipay')['rsaPrivateKey'];
                $pay_charge = config('alipay')['pay_charge'];
                $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key);
                if ($requestData['trade_type'] == 'NATIVE') {
                    $requestData = $alipayClient->webOrder($out_trade_no, '0.01' * count($cart_id_list), $pay_charge, $pay_charge, implode(",", $cart_id_list));
                } else {
                    $requestData = $alipayClient->order($out_trade_no, '0.01' * count($cart_id_list), $pay_charge, $pay_charge, implode(",", $cart_id_list));
                }
                break;
            //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
        }

        Db::commit();
        $this->setRenderCode(200);
        $this->setRenderMessage('生成订单成功');
        $this->addRenderData('result', $requestData, false);
        $this->addRenderData('order_id_array', $result['order_id'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:立即购买
     * @return \think\response\Json
     */
    public function productCharge()
    {
        $this->validateToken();
        $requestData = $this->selectParam([
            'product_id',
            'standard_id',
            'count',
            'pay_type',
            'prepay_id',
            'area_id',
            'user_remarks',
            'trade_type'
        ]);
        $requestData['trade_type'] = empty($requestData['trade_type']) ? 'APP' : $requestData['trade_type'];
        $this->check($requestData, 'ShopPay.product');
        $ShopOrderLogic = new ShopOrderLogic();
        Db::startTrans();
        //获取用户地址
        $userArea = UserAreaLogic::getInfo($this->getUserId(), $requestData['area_id']);
        if (empty($userArea)) {
            Db::rollback();
            $this->setRenderCode(500);
            $this->setRenderMessage('收货地址不存在');
            return $this->getRenderJson();
        }
        $userArea = $userArea->toArray();//object转array
        unset($userArea['code']);

        $ShopOrderLogic->lockStock($requestData['product_id'], $this->getUserId());//锁库存数据,实测联表时锁不生效

        //判断商品是否已下架或者已删除
        $checkProduct = $ShopOrderLogic->checkProduct($requestData['product_id'], $requestData['standard_id']);
        if (empty($checkProduct)) {
            Db::rollback();
            $this->setRenderCode(501);
            $this->setRenderMessage('商品或对应规格已下架');
            return $this->getRenderJson();
        }//TODO 这个部分测试通过

        if ($checkProduct['stocks'] < $requestData['count']) {
            Db::rollback();
            $this->setRenderCode(502);
            $this->setRenderMessage('商品库存不足');
            return $this->getRenderJson();
        }
        $data['product_id'] = $requestData['product_id'];
        $data['standard_id'] = $requestData['standard_id'];
        $data['count'] = $requestData['count'];
        $data['pay_type'] = $requestData['pay_type'];
        $data['user_remarks'] = $requestData['user_remarks'];
        //生成订单
        $out_trade_no = md5(uniqid(mt_rand(), true));//商户订单号
        $result = $ShopOrderLogic->addProductOrder($data, $this->getUserId(), $out_trade_no, $userArea);
        if ($result === false) {
            Db::rollback();
            $this->setRenderCode(500);
            $this->setRenderMessage('生成订单失败');
            return $this->getRenderJson();
        }
        RedisUtil::getInstance()->hSet('order_pay_info', $out_trade_no, $requestData['trade_type'] . '|' . $requestData['prepay_id']);

        switch ($requestData['pay_type']) {
            case 'wechat':
                $body = config('wechat')['wx_pay_charge'];
                //$attach = implode(',',$order_num_list);

                $appId = config('wechat')['AppID'];
                $mchId = config('wechat')['Pay']['MchId'];
                $appKey = config('wechat')['Pay']['Key'];
                $wechatPay = new WechatPay($appId, $mchId, $appKey);

                // $requestData = $wechatPay->order($out_trade_no, $fee, $userOpenid, $body, "");
                $requestData = $wechatPay->order($out_trade_no, 0.01, $requestData['prepay_id'], $body, "",
                    $requestData['trade_type']);//todo::测试临时

                if ($requestData === false) {
                    Db::rollback();
                    $this->setRenderCode(500);
                    $this->setRenderMessage('生成订单失败');
                    return $this->getRenderJson();
                }
                break;
            //微信是后台调用接口拿到是否生成订单的返回结果
            case 'alipay':
                $appId = config('alipay')['AppID'];
                $partner_public_key = config('alipay')['partnerPublicKey'];
                $partner_private_key = config('alipay')['rsaPrivateKey'];
                $pay_charge = config('alipay')['pay_charge'];
                $alipay_public_key = config('alipay')['alipayPublicKey'];
                $alipayClient = new alipayClient($appId, $partner_private_key, $partner_public_key, $alipay_public_key);
                if ($requestData['trade_type'] == 'NATIVE') {
                    $requestData = $alipayClient->webOrder($out_trade_no, '0.01', $pay_charge, $pay_charge);
                } else {
                    $requestData = $alipayClient->order($out_trade_no, '0.01', $pay_charge, $pay_charge);
                }
                break;
            //支付宝这里只是生成签名返回给终端,终端直接调支付界面,
        }
        Db::commit();
        $this->setRenderCode(200);
        $this->setRenderMessage('生成订单成功');
        $this->addRenderData('result', $requestData, false);
        $this->addRenderData('order_id', $result['order_id'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月8日
     * description:商品详情跳到立即支付界面
     */
    public function settlement()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['product_id', 'standard_id', 'count']);
        $this->check($requestData, 'ShopPay.settlement');
        $area_info = UserAreaLogic::getDefault($this->getUserId());
        if ($area_info === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器错误');
            return $this->getRenderJson();
        }
        if (empty($area_info)) {
            $area_info = null;
        }
        $product_info = ShopPayLogic::productInfo($requestData['product_id'], $requestData['standard_id']);
        if (empty($product_info)) {
            $this->setRenderCode(501);
            $this->setRenderMessage('商品不存在');
            return $this->getRenderJson();
        }
        $product_info['pay_count'] = intval($requestData['count']);
        if ($product_info['stocks'] < $requestData['count']) {
            $this->setRenderCode(502);
            $this->setRenderMessage('商品库存不足');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('area_info', $area_info, false);
        $this->addRenderData('product_info', $product_info, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月11日
     * description:购物车跳到立即支付界面
     */
    public function settlementCart()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id_list']);
        $this->check($requestData, 'ShopPay.settlementCart');

        $cart_id_list = explode(',', $requestData['cart_id_list']);
        if (!is_array($cart_id_list)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('请选择结算商品');
            return $this->getRenderJson();
        }
        $area_info = UserAreaLogic::getDefault($this->getUserId());
        if ($area_info === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('服务器错误');
            return $this->getRenderJson();
        }
        if (empty($area_info)) {
            $area_info = null;
        }

        $ShopOrderLogic = new ShopOrderLogic();
        $cartCheckCount = $ShopOrderLogic->cartCheckCount($cart_id_list, $this->getUserId());
        if (is_array($cartCheckCount) && count($cartCheckCount) != count($cart_id_list)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('结算商品与购物车内数据不符合');
            //$this->addRenderData('success_cart', $cartCheckCount);
            return $this->getRenderJson();
        }


        //判断商品是否已下架或者已删除
        $deletedOrSale = $ShopOrderLogic->cartDeletedOrSale($cart_id_list, $this->getUserId());
        if (!empty($deletedOrSale)) {
            $this->setRenderCode(501);
            $this->setRenderMessage('部分结算商品已下架');
            $this->addRenderData('error_cart', $deletedOrSale, false);
            return $this->getRenderJson();
        }

        //判断商品规格是否已删除
        $standardDelete = $ShopOrderLogic->cartStandardDelete($cart_id_list, $this->getUserId());
        if (!empty($standardDelete)) {
            $this->setRenderCode(501);
            $this->setRenderMessage('部分结算商品规格已下架');
            $this->addRenderData('error_cart', $standardDelete, false);
            return $this->getRenderJson();
        }

        //判断库存量是否足够
        $checkStock = $ShopOrderLogic->cartCheckStock($cart_id_list, $this->getUserId());
        if (!empty($checkStock)) {
            $this->setRenderCode(502);
            $this->setRenderMessage('部分结算商品库存不足');
            $this->addRenderData('error_cart', $checkStock, false);
            return $this->getRenderJson();
        }
        $product_info = ShopPayLogic::productCartInfo($requestData['cart_id_list']);
        $this->setRenderCode(200);
        $this->addRenderData('area_info', $area_info, false);
        $this->addRenderData('product_list', $product_info, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年月日
     * description:查询订单是否已支付成功 (用于PC微信支付)
     * @return \think\response\Json
     */
    public function checkOrder()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['out_trade_no']);
        $this->check($requestData, 'ShopPay.outTradeNo');
        $order_bool = ShopOrderLogic::checkOrder($requestData['out_trade_no']);
        $is_pay = empty($order_bool) ? 'no' : 'yes';
        $this->setRenderCode(200);
        $this->addRenderData('is_pay', $is_pay, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2018年2月8日
     * description:重新调用支付
     * @return \think\response\Json
     */
    public function payAgain()
    {
        $this->validateToken();
        $requestData = $this->selectParam(['order_id']);

        $logic = new ShopOrderLogic();
        $result = $logic->payAgain($requestData['order_id'], $this->getUserId());

        if (is_int($result)) {
            $this->setRenderCode(500);
            switch ($result) {
                case 0:
                    $this->setRenderMessage('订单信息不存在');
                    break;
                case 1:
                    $this->setRenderMessage('发起支付失败');
                    break;
            }
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('生成订单成功');
            $this->addRenderData('result', $result['result'], false);
            $this->addRenderData('pay_type', $result['order_info']['pay_type'], false);
            $this->addRenderData('order_id', $requestData['order_id'], false);
        }

        return $this->getRenderJson();
    }

}