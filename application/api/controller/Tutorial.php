<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/2
 * Time: 17:06
 */

namespace app\api\controller;

use app\api\logic\ConfigLogic;
use app\api\logic\TutorialLogic;

class Tutorial extends Base
{
    public function getList()
    {
        # 获取参数 验证参数
        $result = TutorialLogic::get_list($this->pageIndex, $this->pageSize);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->addRenderData('tutorial_list', $result, false);
        $this->addRenderData('count', TutorialLogic::getCount(), false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年8月2日
     * description:教程首页推荐
     */
    public function recommend()
    {
        $result = ConfigLogic::Tutorial();
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $result = json_decode($result['value'],JSON_UNESCAPED_UNICODE);
        $this->addRenderData('recommend', $result, false);
        return $this->getRenderJson();
    }
}