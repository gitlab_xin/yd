<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/25
 * Time: 15:33
 */

namespace app\api\controller;

use think\Request;
use app\api\logic\CustomizedOrderLogic as OrderLogic;
use app\api\logic\ShopOrderLogic;

class CustomizedOrder extends Base
{
    public $orderLogic;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
        $this->orderLogic = new OrderLogic();
    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:获取我的订单列表
     * @return \think\response\Json
     */
    public function getMyOrders()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $data = $this->selectParam(['status']);
        $this->check($data, 'CustomizedOrder.list');
        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = $this->orderLogic->getOrdersByUser($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('orders', $result['orders'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月15日
     * description:查看我的订单详情
     * @return \think\response\Json
     */
    public function getOrderDetail()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id']);
        $this->check($data, 'CustomizedOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->orderLogic->getOrderDetail($data);

        if ($result == null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('查询不到该订单信息');
            return $this->getRenderJson();
        }
        $ShopOrderLogic = new ShopOrderLogic();
        $logistics = $ShopOrderLogic->getLogisticsBrief(['logistics_id'=>$result['logistics_id'],'user_id'=>$data['user_id']]);

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('order_detail', $result, false);
        $this->addRenderData('order_logistics', $logistics, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:搜索我的订单列表
     * @return \think\response\Json
     */
    public function searchMyOrders()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $data = $this->selectParam(['status', 'keyword']);
        $data['status'] = 'all';
        $this->check($data, 'CustomizedOrder.search');
        $data['user_id'] = $this->getUserId();
        $data['page_index'] = $this->pageIndex;
        $data['page_size'] = $this->pageSize;

        $result = $this->orderLogic->getOrdersByUser($data);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('orders', $result['orders'], false);
        $this->addRenderData('count', $result['count'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月18日
     * description:确认收货
     * @return \think\response\Json
     */
    public function confirmOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id']);
        $this->check($data, 'CustomizedOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->orderLogic->confirmOrder($data);

        $this->setRenderCode(500);
        switch ($result) {
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有待收货的订单才能确认收货');
                break;
            case 2:
                $this->setRenderMessage('确认失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('确认成功');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:申请退款
     * @return \think\response\Json
     */
    public function cancelOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id','content','reason']);
        $this->check($data, 'ShopOrder.refund');
        $data['user_id'] = $this->getUserId();
        $result = $this->orderLogic->cancelOrder($data);

        $this->setRenderCode(500);
        switch ($result) {
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有待发货的订单才能申请退款');
                break;
            case 2:
                $this->setRenderMessage('确认失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('确认成功');
                break;
        }

        return $this->getRenderJson();
    }


    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:订单评价
     * @return \think\response\Json
     */
    public function evaluate()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        // 接收参数，验证器验证
        $requestData = $this->selectParam([
            'order_id',
            'materials_star',
            'service_star',
            'process_star',
            'install_star',
            'delivery_star',
            'content'
        ]);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'CustomizedEvaluate.evaluate');

        $result = $this->orderLogic->evaluate($requestData);

        switch ($result) {
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('订单状态修改失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('评价失败');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('订单不存在');
                break;
            case 1:
                $this->setRenderCode(200);
                $this->setRenderMessage('评价成功');
                break;
            case 2:
                $this->setRenderCode(500);
                $this->setRenderMessage('请勿重复评价');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Airon
     * @time: 2017年9月16日
     * description:获取我的定制订单评价
     * @return \think\response\Json
     */
    public function getOrderEvaluate()
    {
        $this->validateToken();

        $requestData = $this->selectParam(['order_id']);
        $requestData['user_id'] = $this->getUserId();

        $this->check($requestData, 'CustomizedEvaluate.getOrderEvaluate');

        $result = $this->orderLogic->getOrderEvaluate($requestData);

        if ($result == null) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据不存在');
        } else {
            $this->setRenderCode(200);
            $this->addRenderData('detail', $result, false);
            $this->setRenderMessage('获取成功');
        }
        return $this->getRenderJson();
    }

    public function cancelUnpaidOrder()
    {
        // 验证token，获取用户ID
        $this->validateToken();

        $data = $this->selectParam(['order_id','cancel_reason']);
        $this->check($data,'CustomizedOrder.detail');
        $data['user_id'] = $this->getUserId();
        $result = $this->orderLogic->cancelUnpaidOrder($data);

        $this->setRenderCode(500);
        switch ($result){
            case 0:
                $this->setRenderMessage('该订单不存在');
                break;
            case 1:
                $this->setRenderMessage('只有未支付的订单才能取消订单');
                break;
            case 2:
                $this->setRenderMessage('取消失败');
                break;
            case 9:
                $this->setRenderCode(200);
                $this->setRenderMessage('取消成功');
                break;
        }

        return $this->getRenderJson();
    }
}