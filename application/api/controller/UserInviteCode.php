<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/14
 * Time: 11:20
 */

namespace app\api\controller;

use app\api\logic\UserInviteCodeLogic;

class UserInviteCode extends Base
{
    public function useCode()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam(['invite_code']);

        if(empty($requestData['invite_code'])){
            $this->setRenderCode(403);
            $this->setRenderMessage("邀请码不能为空");
            return $this->getRenderJson();
        }

        $result = UserInviteCodeLogic::useCode($requestData['invite_code']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('邀请码已失效');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->setRenderMessage('success');
        return $this->getRenderJson();
    }

    public function checkCode()
    {
        $requestData = $this->selectParam(['invite_code']);
        if(empty($requestData['invite_code'])){
            $this->setRenderCode(403);
            $this->setRenderMessage("邀请码不能为空");
            return $this->getRenderJson();
        }
        $result = UserInviteCodeLogic::checkCode($requestData['invite_code']);
        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('邀请码已失效');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->setRenderMessage('验证成功');
        return $this->getRenderJson();
    }
}