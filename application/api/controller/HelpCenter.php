<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/16
 * Time: 17:27
 */

namespace app\api\controller;

use app\api\logic\HelpCenterLogic;

class HelpCenter extends Base
{

    /**
     * @author: Rudy
     * @time: 2017年8月16日
     * description:获取帮助中心所有分类
     * @return \think\response\Json
     *
     */
    public function getHelperClassify()
    {

        $result = (new HelpCenterLogic)->getAllClassify();

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * description:获取帮助中心所有文章标题和id
     * @return \think\response\Json
     */
    public function getClassifyPosts()
    {
        $data = $this->selectParam(['id']);
        $this->check($data, 'HelpCenterClassify.getPosts');
        $result = (new HelpCenterLogic)->getPostsByClassify($data['id']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('list', $result, false);
        return $this->getRenderJson();
    }

    /**
     * 获取文章详情
     * @return \think\response\Json
     */
    public function getPostDetail()
    {
        $data = $this->selectParam(['id']);
        $this->check($data, 'HelpCenter.detail');
        $result = (new HelpCenterLogic)->getPostDetail($data['id']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('获取成功');
        $this->addRenderData('detail', $result, false);
        return $this->getRenderJson();
    }
}