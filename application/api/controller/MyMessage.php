<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/18
 * Time: 15:16
 */

namespace app\api\controller;

use app\api\logic\MyMessageLogic;

class MyMessage extends Base
{
    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取官方消息
     * @return \think\response\Json
     */
    public function getOfficialMessages()
    {
        $result = MyMessageLogic::getOfficialMessages($this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('official_messages', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取易道消息详情
     * @return \think\response\Json
     */
    public function getOfficialMessageDetail()
    {
        $data = $this->selectParam(['message_id']);

        $this->check($data,'MessageOfficial.detail');

        $result = MyMessageLogic::getOfficialMessageDetail($data['message_id']);
        switch ($result){
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('不存在该消息');
                break;
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('该消息属于活动类型，请调用相应活动接口');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('message_detail', $result, false);
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月18日
     * description:获取论坛消息
     * @return \think\response\Json
     */
    public function getForumMessages()
    {
        $this->validateToken();

        $result = MyMessageLogic::getForumMessages($this->getUserId(),$this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('forum_messages', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月21日
     * description:获取消息盒子
     * @return \think\response\Json
     */
    public function myMessageBox()
    {
        $this->validateToken();

        $result = MyMessageLogic::getEverySingleMessage($this->getUserId());

        $this->setRenderCode(200);
        $this->addRenderData('messages', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月25日
     * description:获取物流信息
     * @return \think\response\Json
     */
    public function getLogisticsMessages()
    {
        $this->validateToken();

        $result = MyMessageLogic::getLogisticsMessages($this->getUserId(),$this->pageIndex, $this->pageSize);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('logistics_messages', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月28日
     * description:获取我的私信
     * @return \think\response\Json
     */
    public function getDemandMessages()
    {
        $this->validateToken();

        $result = MyMessageLogic::getDemandMessages($this->getUserId());

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('demand_messages', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月31日
     * description:获取需求的全部私信
     * @return \think\response\Json
     */
    public function getAllDemandMessages()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);
        $this->check($data,'DemandOrder.detail');
        $result = MyMessageLogic::getAllDemandMessages($this->getUserId(),$data['demand_id']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->addRenderData('messages', $result, false);
        return $this->getRenderJson();
    }

    public function getDemandNotify()
    {
        
    }
}