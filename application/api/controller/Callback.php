<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/7/28
 * Time: 10:24
 */

namespace app\api\controller;

use app\api\logic\DemandLogic;
use app\api\logic\DemandOrderLogic;
use app\api\logic\OrderLogic;
use app\api\logic\ShopOrderLogic;
use app\api\logic\ShopPayLogic;
use app\api\logic\CustomizedPayLogic;
use app\common\model\MyDecorateProduct;
use app\common\model\MyDecorateProductStandard;
use app\common\model\ShoppingCart;
use app\common\model\ShopProduct;
use app\common\model\ShopProductStandard;
use app\common\tools\alipayClient;
use app\common\tools\RedisUtil;
use app\common\tools\wechatPay;
use app\common\tools\wechatUtil;

use think\Controller;
use think\Db;
use think\Exception;
use think\exception\HttpResponseException;
use think\Request;
use think\Response;

class Callback extends Controller
{
    public function temp()
    {
        $appId = config('wechat')['AppID'];
        $mchId = config('wechat')['Pay']['MchId'];
        $appKey = config('wechat')['Pay']['Key'];
        $wechatPay = new WechatPay($appId, $mchId, $appKey);
        dump($wechatPay->refund("da41806a66ca26fa1e9cbf32f87c3321", "da41806a66ca26fa1e9cbf32f87c332f", 0.01));
        dump($wechatPay->errorDetail);
        dump($wechatPay->errorMessage);//todo 退款实例
    }

    public function wxPay()
    {
        $requestXml = Request::instance()->getContent();
        $appId = config('wechat')['AppID'];
        $mchId = config('wechat')['Pay']['MchId'];
        $appKey = config('wechat')['Pay']['Key'];
        $wechatPay = new WechatPay($appId, $mchId, $appKey);
        $requestArray = $wechatPay->handleNotify($requestXml);//验证支付结果
        if ($requestArray === false) {
            log_file(time(), 'handle error', 'wxpay');
            $this->replyNotify();
        }
        $current = time();
        $updateData = [
            'bill_no' => $requestArray['out_trade_no'],
            'status' => '待发货',
            'update_time' => $current,
            'pay_time' => $current
        ];


        $order_info = ShopPayLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($updateData, 'charge not found', 'wxpay');
            $this->replyNotify();
        }
        $order_fee = 0;
        foreach ($order_info as $key => $value) {
            $order_fee += $value['price'];
            if ($value['status'] != '未支付') {
                // already handle
                log_file($order_info, 'already handle', 'wxpay');
                $this->replyNotify();
            }
        }

        //        if ($requestArray['total_fee'] != round($order_fee, 2) * 100) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'wxpay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01
        Db::startTrans();
        if (!empty($requestArray['attach'])) {//删除购物车信息
            $cart_id_list = explode(",", $requestArray['attach']);
            $ShopOrderLogic = new ShopOrderLogic();
            $deleteCart = $ShopOrderLogic->deleteShoppingCart($cart_id_list, $order_info[0]['user_id']);
            if ($deleteCart === false) {
                Db::rollback();
                log_file($cart_id_list, 'delete shoppingcart fail', 'wxpay');
                $this->replyNotify();
            }
        }

        $updateResult = ShopPayLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            // update fail
            Db::rollback();
            log_file($updateResult, 'update fail', 'wxpay');
            $this->replyNotify();
        }
        Db::commit();
        $this->replyNotify(true);
    }

    public function demandWXPay()
    {
        $requestXml = Request::instance()->getContent();
        $appId = config('wechat')['AppID'];
        $mchId = config('wechat')['Pay']['MchId'];
        $appKey = config('wechat')['Pay']['Key'];
        $wechatPay = new WechatPay($appId, $mchId, $appKey);
        $requestArray = $wechatPay->handleNotify($requestXml);//验证支付结果
        if ($requestArray === false) {
            log_file(time(), 'handle error', 'wxpay');
            $this->replyNotify();
        }
        $current = time();
        $updateData = [
            'pay_time' => $current
        ];


        $order_info = DemandOrderLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($requestArray, 'charge not found', 'wxpay');
            $this->replyNotify();
        }

        $fee = $order_info['money'];
        if ($order_info['pay_time'] != 0) {
            // already handle
            log_file($requestArray, 'already handle', 'wxpay');
            $this->replyNotify();
        }


        //        if ($requestArray['total_fee'] != round($fee, 2) * 100) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'wxpay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01
        $updateResult = DemandLogic::setDemand($order_info['demand_id'], ['status' => 1]);//修改订单状态
        if (!$updateResult) {
            // update fail
            log_file($updateResult, 'update fail', 'wxpay');
            $this->replyNotify();
        }
        $updateResult = DemandOrderLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            // update fail
            log_file($updateResult, 'update fail', 'wxpay');
            $this->replyNotify();
        }

        $this->replyNotify(true);
    }

    public function customizedWXPay()
    {
        $requestXml = Request::instance()->getContent();
        $appId = config('wechat')['AppID'];
        $mchId = config('wechat')['Pay']['MchId'];
        $appKey = config('wechat')['Pay']['Key'];
        $wechatPay = new WechatPay($appId, $mchId, $appKey);
        $requestArray = $wechatPay->handleNotify($requestXml);//验证支付结果
        if ($requestArray === false) {
            log_file(time(), 'handle error', 'wxpay');
            $this->replyNotify();
        }
        $current = time();
        $updateData = [
            'pay_time' => $current,
            'status' => '待发货'
        ];


        $order_info = CustomizedPayLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($requestArray, 'charge not found', 'wxpay');
            $this->replyNotify();
        }

        if ($order_info['pay_time'] != 0) {
            // already handle
            log_file($requestArray, 'already handle', 'wxpay');
            $this->replyNotify();
        }


        //        if ($requestArray['total_fee'] != round($fee, 2) * 100) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'wxpay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01
        $updateResult = CustomizedPayLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            // update fail
            log_file($updateResult, 'update fail', 'wxpay');
            $this->replyNotify();
        }

        $this->replyNotify(true);
    }

    public function orderWXPay()
    {
        $requestXml = Request::instance()->getContent();
        $appId = config('wechat')['AppID'];
        $mchId = config('wechat')['Pay']['MchId'];
        $appKey = config('wechat')['Pay']['Key'];
        $wechatPay = new WechatPay($appId, $mchId, $appKey);
        $requestArray = $wechatPay->handleNotify($requestXml);//验证支付结果
        if ($requestArray === false) {
            log_file(time(), 'handle error', 'wxpay');
            $this->replyNotify();
        }
//        $requestArray['out_trade_no'] = 'e9c91586507613';

        $cart_info = unserialize(RedisUtil::getInstance()->get($requestArray['out_trade_no']));

        $current = time();
        $updateData = [
            'pay_time' => $current,
            'status' => '待发货'
        ];


        $order_info = OrderLogic::getOrderInfo($requestArray['out_trade_no']);

        if (empty($order_info)) {
            log_file($requestArray, 'charge not found', 'wxpay');
            $this->replyNotify();
        }

        if ($order_info['pay_time'] != 0) {
            // already handle
            log_file($requestArray, 'already handle', 'wxpay');
            $this->replyNotify();
        }


        //        if ($requestArray['total_fee'] != round($fee, 2) * 100) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'wxpay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01
        Db::startTrans();
        try{
            $updateResult = OrderLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

            //增加销量
            if ($cart_info){
                $cart_ids = [];

                foreach ($cart_info as $k => $v){

                    //商城类
                    if ($v['type'] == '3' || $v['from'] == '1'){
                        ShopProduct::build()
                            ->where(['product_id' => $v['product_id']])
                            ->setInc('sales', $v['count']);
                        ShopProductStandard::build()
                            ->where(['product_id' => $v['product_id'], 'standard_id' => $v['standard_id']])
                            ->setInc('sales', $v['count']);
                    }

                    //易家整配
                    if ($v['type'] == '2' && $v['from'] == '2') {
                        MyDecorateProduct::build()
                            ->where(['product_id' => $v['product_id']])
                            ->setInc('sales', $v['count']);
                        MyDecorateProductStandard::build()
                            ->where(['decorate_product_id' => $v['decorate_product_id'], 'color_id' => $v['color_id']])
                            ->setInc('sales', $v['count']);
                    }
                    if (isset($v['cart_id'])){
                        $cart_ids[] = $v['cart_id'];
                    }
                }

                if (count($cart_ids) >= 1){
                    ShoppingCart::build()->where(['cart_id' => ['in',$cart_ids]])->delete();//删除购物车
                }
            }


            Db::commit();
        }catch (Exception $exception){
            log_file($updateResult, 'update fail', 'wxpay');
            Db::rollback();
            $this->replyNotify();
        }


        $this->replyNotify(true);
    }

    public function alipayOauth()
    {
        $request = Request::instance()->getContent();
        log_file($request, 'charge not found', 'alipay');
        $this->replyNotify();
        return json(['code' => 200, 'message' => 'success']);
    }

    public function alipay()
    {
        $requestData = $_POST;
        $request = Request::instance()->getContent();
        log_file($requestData, 'begin', 'alipay');
        log_file($request, 'begin2', 'alipay');
        $appId = config('aliapy')['AppID'];
        $rsaPrivateKey = config('alipay')['rsaPrivateKey'];
        $partnerPublicKey = config('alipay')['partnerPublicKey'];
        $alipayPublicKey = config('alipay')['alipayPublicKey'];
        $alipayPay = new alipayClient($appId, $rsaPrivateKey, $partnerPublicKey, $alipayPublicKey);
        //此处验签方式必须与下单时的签名方式一致
        //验签通过后再实现业务逻辑，比如修改订单表中的支付状态。
        /**
         *  ①验签通过后核实如下参数out_trade_no、total_amount、seller_id
         *  ②修改订单表
         **/
        $requestArray = $alipayPay->handleNotify($requestData);//验证支付结果

        if ($requestArray === false) {
            return 'success';//返回给支付宝表示已收到回调
        }
        $current = time();
        $updateData = [
            'bill_no' => $requestArray['out_trade_no'],
            'status' => '待发货',
            'update_time' => $current,
            'pay_time' => $current
        ];


        $order_info = ShopPayLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($updateData, 'charge not found', 'alipay');
            return 'success';//返回给支付宝表示已收到回调
        }
        $order_fee = 0;
        foreach ($order_info as $key => $value) {
            $order_fee += $value['price'];
        }

        //        if ($requestArray['total_fee'] != round($order_fee, 2)) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'alipay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01

        Db::startTrans();
        if (!empty($requestArray['passback_params'])) {//删除购物车信息
            $cart_id_list = explode(",", $requestArray['passback_params']);
            $ShopOrderLogic = new ShopOrderLogic();
            $deleteCart = $ShopOrderLogic->deleteShoppingCart($cart_id_list, $order_info[0]['user_id']);
            if ($deleteCart === false) {
                Db::rollback();
                log_file($cart_id_list, 'delete shoppingcart fail', 'alipay');
                return 'success';
            }
        }

        $updateResult = ShopPayLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            Db::rollback();
            // update fail
            log_file($updateResult, 'update fail', 'alipay');
        } else {
            Db::commit();
        }

        return 'success';
    }

    public function demandAlipay()
    {
        $requestData = $_POST;
        log_file($requestData, 'begin', 'alipay_demand');
        $appId = config('aliapy')['AppID'];
        $rsaPrivateKey = config('alipay')['rsaPrivateKey'];
        $partnerPublicKey = config('alipay')['partnerPublicKey'];
        $alipayPublicKey = config('alipay')['alipayPublicKey'];
        $alipayPay = new alipayClient($appId, $rsaPrivateKey, $partnerPublicKey, $alipayPublicKey);
        //此处验签方式必须与下单时的签名方式一致
        //验签通过后再实现业务逻辑，比如修改订单表中的支付状态。
        /**
         *  ①验签通过后核实如下参数out_trade_no、total_amount、seller_id
         *  ②修改订单表
         **/
        $requestArray = $alipayPay->handleNotify($requestData);//验证支付结果

        if ($requestArray === false) {
            return 'success';//返回给支付宝表示已收到回调
        }
        $current = time();
        $updateData = [
            'pay_time' => $current
        ];


        $order_info = DemandOrderLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($requestData, 'charge not found', 'alipay_demand');
            return 'success';//返回给支付宝表示已收到回调
        }
        $order_fee = $order_info['money'];


        //        if ($requestArray['total_fee'] != round($order_fee, 2)) {
//            // fee not right
//            log_file($updateData, 'fee not right - ' . $order_fee, 'alipay');
//            $this->replyNotify();
//        }todo 测试阶段注释  价格修改为0,01
        $updateResult = DemandLogic::setDemand($order_info['demand_id'], ['status' => 1]);//修改订单状态
        if (!$updateResult) {
            // update fail
            log_file($updateResult, 'update fail', 'alipay_demand');
            $this->replyNotify();
        }
        $updateResult = DemandOrderLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            // update fail
            log_file($requestData, 'update fail', 'alipay_demand');
            $this->replyNotify();
        }
        return 'success';
    }

    public function customizedAlipay()
    {
        $requestData = $_POST;
        log_file($requestData, 'begin', 'alipay_customized');
        $appId = config('aliapy')['AppID'];
        $rsaPrivateKey = config('alipay')['rsaPrivateKey'];
        $partnerPublicKey = config('alipay')['partnerPublicKey'];
        $alipayPublicKey = config('alipay')['alipayPublicKey'];
        $alipayPay = new alipayClient($appId, $rsaPrivateKey, $partnerPublicKey, $alipayPublicKey);
        //此处验签方式必须与下单时的签名方式一致
        //验签通过后再实现业务逻辑，比如修改订单表中的支付状态。
        /**
         *  ①验签通过后核实如下参数out_trade_no、total_amount、seller_id
         *  ②修改订单表
         **/
        $requestArray = $alipayPay->handleNotify($requestData);//验证支付结果

        if ($requestArray === false) {
            return 'success';//返回给支付宝表示已收到回调
        }
        $current = time();
        $updateData = [
            'pay_time' => $current,
            'status' => '待发货'
        ];


        $order_info = CustomizedPayLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($requestData, 'charge not found', 'alipay_demand');
            return 'success';//返回给支付宝表示已收到回调
        }

        $updateResult = CustomizedPayLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

        if (!$updateResult) {
            // update fail
            log_file($requestData, 'update fail', 'alipay_demand');
            $this->replyNotify();
        }
        return 'success';
    }

    public function orderAlipay()
    {
        $requestData = $_POST;
        log_file($requestData, 'begin', 'alipay_customized');
        $appId = config('aliapy')['AppID'];
        $rsaPrivateKey = config('alipay')['rsaPrivateKey'];
        $partnerPublicKey = config('alipay')['partnerPublicKey'];
        $alipayPublicKey = config('alipay')['alipayPublicKey'];
        $alipayPay = new alipayClient($appId, $rsaPrivateKey, $partnerPublicKey, $alipayPublicKey);
        //此处验签方式必须与下单时的签名方式一致
        //验签通过后再实现业务逻辑，比如修改订单表中的支付状态。
        /**
         *  ①验签通过后核实如下参数out_trade_no、total_amount、seller_id
         *  ②修改订单表
         **/
        $requestArray = $alipayPay->handleNotify($requestData);//验证支付结果

//        $requestArray['out_trade_no'] = 'b6361586507562';

        $cart_info = unserialize(RedisUtil::getInstance()->get($requestArray['out_trade_no']));

        if ($requestArray === false) {
            return 'success';//返回给支付宝表示已收到回调
        }
        $current = time();
        $updateData = [
            'pay_time' => $current,
            'status' => '待发货'
        ];


        $order_info = OrderLogic::getOrderInfo($requestArray['out_trade_no']);
        if (empty($order_info)) {
            log_file($requestData, 'charge not found', 'alipay_demand');
            return 'success';//返回给支付宝表示已收到回调
        }

        Db::startTrans();
        try{
            OrderLogic::updateOrderStatus($requestArray['out_trade_no'], $updateData);//修改订单状态

            //增加销量
            if ($cart_info){
                $cart_ids = [];

                foreach ($cart_info as $k => $v){

                    //商城类
                    if ($v['type'] == '3' || $v['from'] == '1'){
                        ShopProduct::build()
                            ->where(['product_id' => $v['product_id']])
                            ->setInc('sales', $v['count']);
                        ShopProductStandard::build()
                            ->where(['product_id' => $v['product_id'], 'standard_id' => $v['standard_id']])
                            ->setInc('sales', $v['count']);
                    }

                    //易家整配
                    if ($v['type'] == '2' && $v['from'] == '2') {
                        MyDecorateProduct::build()
                            ->where(['product_id' => $v['product_id']])
                            ->setInc('sales', $v['count']);
                        MyDecorateProductStandard::build()
                            ->where(['decorate_product_id' => $v['decorate_product_id'], 'color_id' => $v['color_id']])
                            ->setInc('sales', $v['count']);
                    }
                    if (isset($v['cart_id'])){
                        $cart_ids[] = $v['cart_id'];
                    }
                }

                if (count($cart_ids) >= 1){
                    ShoppingCart::build()->where(['cart_id' => ['in',$cart_ids]])->delete();//删除购物车
                }
            }


            Db::commit();
        }catch (Exception $exception){
            log_file($requestData, 'update fail', 'alipay_demand');
            Db::rollback();
            $this->replyNotify();
        }

        return 'success';
    }

    private function replyNotify($isSuccess = false)
    {
        $result = [];
        if ($isSuccess) {
            $result['return_code'] = 'SUCCESS';
            $result['return_msg'] = 'OK';
        } else {
            $result['return_code'] = 'FAIL';
            $result['return_msg'] = 'FAIL';
        }
        $result = wechatUtil::toXml($result);
        throw new HttpResponseException(Response::create($result));
    }
}
