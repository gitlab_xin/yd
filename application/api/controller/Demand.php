<?php
/**
 * Created by PhpStorm.
 * User: Airon
 * Date: 2017/8/25
 * Time: 9:23
 */

namespace app\api\controller;

use app\api\logic\DemandLogic;
use app\api\logic\DemandSchemeLogic;
use app\common\model\DemandFurnitureStyle;
use app\common\model\CustomizedMaterial;
use app\common\tools\CommonMethod;

class Demand extends Base
{
    /**
     * @author: Airon
     * @time: 2017年8月25日
     * description:入墙式移门壁柜写入需求
     * @return \think\response\Json
     */
    public function RQSRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_hole_type',
            'scheme_hole_width',
            'scheme_hole_height',
            'scheme_hole_sl_height',
            'scheme_hole_left',
            'scheme_hole_right',
            'scheme_color_no',
            'scheme_sk',
            'scheme_door_count',
        ]);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'DemandRQS.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);

        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '入墙式移门壁柜', 'RQS');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function QWRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width',
            'scheme_height',
            'scheme_color_no',
            'env_height',
            'env_width',
        ]);
        $this->check($requestData, 'DemandQW.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);
        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '墙外独立移门壁柜', 'QW');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function BZRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width',
            'scheme_height',
            'scheme_color_no',
            'env_height',
            'env_width',
        ]);
        $this->check($requestData, 'DemandBZ.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);
        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '平开门标准衣柜', 'BZ');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function ZHGCWSNRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width',
            'scheme_height',
            'scheme_color_no',
            'env_height',
            'env_width',
        ]);
        $this->check($requestData, 'DemandZHG.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);
        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '书柜及综合柜类', 'ZH', 'CWSN');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function ZHGSJSCRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width',
            'scheme_height',
            'scheme_s_type',
            'scheme_color_no',
            'env_height',
            'env_width',
        ]);
        $this->check($requestData, 'DemandZHGSJSC.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);
        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '书桌+写字桌', 'ZH', 'SJSC');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function ZHGYYSTRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_width',
            'scheme_s_type',
            'tv_size',
            'tv_type',
            'scheme_color_no',
            'env_height',
            'env_width',
        ]);
        $this->check($requestData, 'DemandZHGYYST.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);
        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '客厅电视柜组合', 'ZH', 'YYST');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function YMRelease()
    {
        $this->validateToken();
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'title',
            'content',
            'cost_min',
            'cost_max',
            'img_src_list',
            'scheme_hole_type',
            'scheme_hole_width',
//            'scheme_hole_height',
            'scheme_hole_sl_height',
//            'scheme_hole_left',
//            'scheme_hole_right',
            'scheme_color_no',
            'scheme_sk',
            'scheme_door_count',
        ]);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'DemandYM.info');

        $requestData['material_name'] = CommonMethod::getSchemeColorName($requestData['scheme_color_no']);

        if ($requestData['material_name'] == null) {
            $this->setRenderCode(402);
            $this->setRenderMessage('花色不存在');
            return $this->getRenderJson();
        }
        $demand_id = DemandLogic::buildDemand($this->getUserId(), $requestData, '移门定制', 'YM');
        if ($demand_id === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('数据上传失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_id', $demand_id, false);
        return $this->getRenderJson();
    }

    public function getInfo()
    {
        $requestData = $this->selectParam(['demand_id']);
        $this->check($requestData, 'Demand.info');
        $info = DemandLogic::getInfo($requestData['demand_id']);
        if ($info === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('需求内容不存在');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('info', $info, false);
        return $this->getRenderJson();
    }

    public function getList()
    {
        # 获取参数 验证参数
        $requestData = $this->selectParam([
            'keyword',
            'order_type' => 'new',
            'reward_min',
            'reward_max',
            'scheme_type',
            'style_name',
            'material_name',
            'scheme_hole_width_min',
            'scheme_hole_width_max',
            'scheme_hole_height_min',
            'scheme_hole_height_max',
        ]);
        $requestData['keyword'] = trim($requestData['keyword'], '?;\n\t(){}|&');
        $this->check($requestData, 'Demand.RQS');
        $data = DemandLogic::getListRQS($requestData, $this->pageIndex, $this->pageSize);
        if ($data === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('demand_list', $data['demand_list'], false);
        $this->addRenderData('count', $data['count'], false);
        return $this->getRenderJson();
    }

    public function furnitureStyle()
    {
        $list = DemandFurnitureStyle::getList();
        $this->setRenderCode(200);
        $this->addRenderData('furniture_style', $list, false);
        return $this->getRenderJson();
    }

    public function material()
    {
        $list = CustomizedMaterial::getList();
        $this->setRenderCode(200);
        $this->addRenderData('material', $list, false);
        return $this->getRenderJson();
    }

    public function closeReason()
    {
        $list = DemandLogic::getCloseReason();
        $this->setRenderCode(200);
        $this->addRenderData('reason', $list, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:
     * @return \think\response\Json
     */
    public function getBannerDetail()
    {
        $requestData = $this->selectParam(['banner_id']);
        $this->check($requestData, 'DemandBanner.detail');
        $info = DemandLogic::getBannerDetail($requestData['banner_id']);
        if ($info === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('需求内容不存在');
            return $this->getRenderJson();
        }
        $this->setRenderCode(200);
        $this->addRenderData('detail', $info['detail'], false);
        $this->addRenderData('good_schemes', $info['good_schemes'], false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:设计师获取需求里的已投稿件
     * @return \think\response\Json
     */
    public function getSchemesByGuest()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);

        $this->check($data, 'DemandScheme.detail');

        $result = DemandSchemeLogic::getSchemesByGuest($this->getUserId(), $data['demand_id']);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
        } else {
            $this->setRenderCode(200);
            $this->setRenderMessage('获取成功');
            $this->addRenderData('list', $result, false);
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:需求方获取需求里的稿件列表
     * @return \think\response\Json
     */
    public function getSchemesByMaster()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);

        $this->check($data, 'DemandScheme.detail');

        $result = DemandSchemeLogic::getSchemesByMaster($this->getUserId(), $data['demand_id']);

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -2;
                $this->setRenderCode(500);
                $this->setRenderMessage('查找不到对应的需求');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('该需求已被删除');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('list', $result, false);
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:上传稿件
     * @return \think\response\Json
     */
    public function submitScheme()
    {
        $this->validateToken();
        $data = $this->selectParam(['title', 'desc', 'origin_scheme_id', 'demand_id']);

        $this->check($data, 'DemandScheme.add');
        $data['user_id'] = $this->getUserId();

        $result = DemandSchemeLogic::addScheme($data);

        switch ($result) {
            case -7:
                $this->setRenderCode(500);
                $this->setRenderMessage('上传失败');
                break;
            case -6:
                $this->setRenderCode(500);
                $this->setRenderMessage('数据库处理失败');
                break;
            case -5:
                $this->setRenderCode(500);
                $this->setRenderMessage('超过投稿限制');
                break;
            case -4:
                $this->setRenderCode(500);
                $this->setRenderMessage('该原始方案不属于你');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('原始方案不存在');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('不要自己给自己投稿好吗');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('该方案不存在');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('上传成功');
                $this->addRenderData('detail', ['can_continue' => $result], false);
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:为方案点赞
     * @return \think\response\Json
     */
    public function praiseScheme()
    {
        $this->validateToken();
        $data = $this->selectParam(['scheme_id']);

        $this->check($data, 'DemandScheme.praise');

        $result = DemandSchemeLogic::changeScheme($data['scheme_id'], $this->getUserId());

        switch ($result) {
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('点赞失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求不存在或未结束');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('你不是该需求发布者');
                break;
            case -4:
                $this->setRenderCode(500);
                $this->setRenderMessage('已经赞过啦');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('点赞成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:评论方案
     * @return \think\response\Json
     */
    public function commentScheme()
    {
        $this->validateToken();
        $data = $this->selectParam(['scheme_id', 'message']);

        $this->check($data, 'DemandScheme.comment');

        $result = DemandSchemeLogic::changeScheme($data['scheme_id'], $this->getUserId(), $data['message']);

        switch ($result) {
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('评论失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求不存在或未结束');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('你不是该需求发布者');
                break;
            case -4:
                $this->setRenderCode(500);
                $this->setRenderMessage('已经评论过啦');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('评论成功');
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月7日
     * description:获取评论和赞
     * @return \think\response\Json
     */
    public function getSchemeComment()
    {
        $data = $this->selectParam(['scheme_id']);
        $this->check($data, 'DemandScheme.praise');

        $result = DemandSchemeLogic::getComment($data['scheme_id']);

        switch ($result) {
            case 0:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('detail', $result, false);
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:确认稿件
     * @return \think\response\Json
     */
    public function confirmScheme()
    {
        $this->validateToken();

        $data = $this->selectParam(['scheme_id']);
        $this->check($data, 'DemandScheme.praise');

        $result = DemandSchemeLogic::confirmScheme($data['scheme_id'], $this->getUserId());

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('确认失败');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('案例不存在');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求不存在或已被删除');
                break;
            case -4:
                $this->setRenderCode(500);
                $this->setRenderMessage('你不是该需求的发布者');
                break;
            case -5:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求的状态必须是发布中的');
                break;
            case -6:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案提供者不存在');
                break;
            case -7:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求状态处理失败');
                break;
            case -8:
                $this->setRenderCode(500);
                $this->setRenderMessage('稿件状态处理失败');
                break;
            case -9:
                $this->setRenderCode(500);
                $this->setRenderMessage('赏金处理失败');
                break;
            case -10:
                $this->setRenderCode(500);
                $this->setRenderMessage('流水记录处理失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('确认成功');
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:获取确认稿件
     * @return \think\response\Json
     */
    public function getAcceptedScheme()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);

        $this->check($data, 'DemandScheme.detail');

        $result = DemandSchemeLogic::getAcceptedScheme($data['demand_id'], $this->getUserId());

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('需求不存在或被删除');
                break;
            case -3:
                $this->setRenderCode(500);
                $this->setRenderMessage('你不是该需求发布者');
                break;
            case -4:
                $this->setRenderCode(500);
                $this->setRenderMessage('该需求尚未结束');
                break;
            case -5:
                $this->setRenderCode(500);
                $this->setRenderMessage('方案不存在');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('detail', $result, false);
                break;
        }

        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月8日
     * description:查看稿件详情
     * @return \think\response\Json
     */
    public function getScheme()
    {
        $this->validateToken();

        $data = $this->selectParam(['scheme_id']);

        $this->check($data, 'DemandScheme.praise');

        $result = DemandSchemeLogic::getScheme($data['scheme_id'], $this->getUserId());

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            case -2:
                $this->setRenderCode(500);
                $this->setRenderMessage('只有需求方和承接方才能查看该稿件,或稿件不存在');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('detail', $result, false);
                break;
        }
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月9日
     * description:获取登陆者对该需求的状态
     * @return \think\response\Json
     *
     */
    public function checkDemandStatus()
    {
        $this->validateToken();

        $data = $this->selectParam(['demand_id']);

        $this->check($data, 'Demand.info');

        $result = DemandLogic::checkDemandStatus($this->getUserId(), $data['demand_id']);

        switch ($result) {
            case -1:
                $this->setRenderCode(500);
                $this->setRenderMessage('获取失败');
                break;
            default:
                $this->setRenderCode(200);
                $this->setRenderMessage('获取成功');
                $this->addRenderData('detail', $result, false);
                break;
        }

        return $this->getRenderJson();
    }
}