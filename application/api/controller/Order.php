<?php
namespace app\api\controller;


use app\api\logic\MyDecorateProductCartLogic;
use app\api\logic\OrderLogic;
use app\api\logic\UserAreaLogic;
use app\common\model\MyDecorateProduct;

class Order extends Base{

    /**
     * User: zhaoxin
     * Date: 2020/4/9
     * Time: 3:06 下午
     * description 预定订单
     * @return \think\response\Json
     */
    public function preInfo(){
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id']);
        $requestData['user_id'] = $this->getUserId();


        $result = OrderLogic::preInfo($requestData);
        $area_info = UserAreaLogic::getDefault($this->getUserId());
        if (empty($area_info)) {
            $area_info = null;
        }

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $total = 0;
        foreach ($result as $k => $v){
            $total += $v['cart_product_info']['discount_price'] * $v['count'];
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('pre_info', $result, false);
        $this->addRenderData('area_info', $area_info, false);
        $this->addRenderData('total_price', $total, false);

        return $this->getRenderJson();
    }

    /**
     * User: zhaoxin
     * Date: 2020/4/9
     * Time: 3:16 下午
     * description 生成订单
     */
    public function create(){
        $this->validateToken();
        $requestData = $this->selectParam(['cart_id', 'delivery_time', 'user_remarks', 'area_id', 'trade_type', 'pay_type']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'order.create');

        $result = OrderLogic::create($requestData);

        if ($result['is_error'] == '1') {
            $this->setRenderCode(500);
            $this->setRenderMessage($result['message']);
            return $this->getRenderJson();
        } else {
            $this->setRenderMessage('生成订单成功');
            $this->addRenderData('result', $result['result'], false);
            $this->addRenderData('order_id', $result['order_id'], false);
        }

        return $this->getRenderJson();
    }


    /**
     * User: zhaoxin
     * Date: 2020/4/10
     * Time: 3:49 下午
     * description 易家整配订单信息
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $this->validateToken();
        $requestData = $this->selectParam(['id', 'count', 'standard_id', 'color_id']);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'order.info');

        $result = OrderLogic::info($requestData, '2');
        $area_info = UserAreaLogic::getDefault($this->getUserId());
        if (empty($area_info)) {
            $area_info = null;
        }

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('pre_info', $result, false);
        $this->addRenderData('area_info', $area_info, false);
        $this->addRenderData('total_price', $result['cart_product_info']['discount_price'], false);

        return $this->getRenderJson();
    }

    public function pay(){
        $this->validateToken();
        $requestData = $this->selectParam([
            'id', 'count', 'standard_id', 'color_id', 'delivery_time', 'user_remarks',
            'area_id', 'trade_type', 'pay_type'
        ]);
        $requestData['user_id'] = $this->getUserId();
        $this->check($requestData, 'order.pay');

        $result = OrderLogic::pay($requestData, '2');

        if ($result['is_error'] == '1') {
            $this->setRenderCode(500);
            $this->setRenderMessage($result['message']);
            return $this->getRenderJson();
        } else {
            $this->setRenderMessage('生成订单成功');
            $this->addRenderData('result', $result['result'], false);
            $this->addRenderData('order_id', $result['order_id'], false);
        }

        return $this->getRenderJson();
    }

    public function index(){
        $this->validateToken();
        $requestData = $this->selectParam([
            'status'
        ]);
        $requestData['user_id'] = $this->getUserId();
        $requestData['page_index'] = $this->pageIndex;
        $requestData['page_size'] = $this->pageSize;
        $result = OrderLogic::index($requestData);

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('list', $result, false);

        return $this->getRenderJson();
    }

    public function read(){
        $this->validateToken();
        $requestData = $this->selectParam([
            'id'
        ]);
        $requestData['user_id'] = $this->getUserId();

        $result = OrderLogic::read($requestData);


        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage('查询成功');
        $this->addRenderData('info', $result, false);

        return $this->getRenderJson();
    }
}