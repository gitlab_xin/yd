<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/9
 * Time: 14:33
 */

namespace app\api\controller;

use app\api\logic\WorksLogic;

class Works extends Base
{

    private $_worksLogic;

    public function __construct()
    {
        parent::__construct();
        $this->_worksLogic = new WorksLogic();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 获取案例列表
     * @return \think\response\Json
     */
    public function getWorksList()
    {
        $requestData = $this->selectParam(['at_user_id', 'sort', 'keyword', 'scheme_type', 'material_name', 'scheme_hole_width_min', 'scheme_hole_width_max', 'scheme_hole_sl_height_min', 'scheme_hole_sl_height_max']);
        $requestData['keyword'] = trim($requestData['keyword'], '?;\n\t(){}|&');
        // 用户未登录默认值为 "0"
        $requestData['at_user_id'] = empty($requestData['at_user_id']) ? 0 : $requestData['at_user_id'];
        // 没有sort参数时默认为recommended
        $requestData['sort'] = empty($requestData['sort']) ? 'recommended' : $requestData['sort'];

        $this->check($requestData, 'Works.list');

        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;

        $model_result = $this->_worksLogic->getWorksList($requestData);
        $count = $this->_worksLogic->getWorksCount($requestData);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('works_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 查看案例详情
     * @return \think\response\Json
     */
    public function worksDetail()
    {
        // 接收参数，验证器验证
        $requestData = $this->selectParam(['works_id', 'at_user_id']);
        $requestData['at_user_id'] = empty($requestData['at_user_id']) ? 0 : $requestData['at_user_id'];
        $this->check($requestData, 'Works.detail');
        $result = $this->_worksLogic->getWorksDetail($requestData);
        $this->addRenderData('works_detail', $result, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 点赞和取消点赞
     * @return \think\response\Json
     */
    public function worksPraise()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['works_id', 'praise_type']);

        $this->check($requestData, 'Works.praise');

        $result = $this->_worksLogic->worksPraise($user_id, $requestData);

        $msg = $requestData['praise_type'] == 'praise' ? '点赞' : '取消点赞';

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage($msg . '失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage($msg . '成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月9日
     * description: 点赞和取消点赞
     * @return \think\response\Json
     */
    public function worksCollect()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        // 接收参数，验证器验证
        $requestData = $this->selectParam(['works_id', 'collect_type']);

        $this->check($requestData, 'Works.collect');

        $result = $this->_worksLogic->worksCollect($user_id, $requestData);

        $msg = $requestData['collect_type'] == 'collect' ? '收藏' : '取消收藏';

        if ($result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage($msg . '失败');
            return $this->getRenderJson();
        }

        $this->setRenderCode(200);
        $this->setRenderMessage($msg . '成功');
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的推荐案例列表
     * @return \think\response\Json
     *
     */
    public function getMyWorksList()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData['user_id'] = $requestData['at_user_id'] = $user_id;
        $requestData['sort'] = 'time';
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;
        $this->check($requestData, 'Works.list');

        $model_result = $this->_worksLogic->getWorksList($requestData);
        $count = $this->_worksLogic->getWorksCount($requestData);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('works_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年8月14日
     * description:获取我的推荐案例列表
     * @return \think\response\Json
     *
     */
    public function getMyWorksCollections()
    {
        // 验证token，获取用户ID
        $this->validateToken();
        $user_id = $this->getUserId();

        $requestData['collect'] = 1;
        $requestData['at_user_id'] = $user_id;
        $requestData['sort'] = 'time';
        $requestData['pageIndex'] = $this->pageIndex;
        $requestData['pageSize'] = $this->pageSize;

        $model_result = $this->_worksLogic->getWorksList($requestData);
        $count = $this->_worksLogic->getWorksCollectionCount($user_id);

        if ($model_result === false) {
            $this->setRenderCode(500);
            $this->setRenderMessage('获取数据失败');
            return $this->getRenderJson();
        }

        $this->setRenderMessage('获取数据成功');
        $this->addRenderData('works_list', $model_result, false);
        $this->addRenderData('count', $count, false);
        return $this->getRenderJson();
    }

    /**
     * @author: Rudy
     * @time: 2017年9月6日
     * description:申请案例
     * @return \think\response\Json
     */
    public function toBeWorks()
    {
        $this->validateToken();

        $data = $this->selectParam(['title', 'content', 'scene_src', 'origin_scheme_id']);
        $this->check($data, 'works.add');
        $data['user_id'] = $this->getUserId();
        $result = $this->_worksLogic->addWorks($data);

        if (!$result) {
            $this->setRenderCode(500);
            $this->setRenderMessage('申请失败');
        }

        switch ($result['code']) {
            case 1:
                $this->setRenderCode(200);
                break;
            default:
                $this->setRenderCode(500);
                break;
        }

        $this->setRenderMessage($result['msg']);

        return $this->getRenderJson();
    }

    public function getSchemeDetailByWorks()
    {
        $requestData = $this->selectParam(['works_id']);

        $data = $this->_worksLogic->getSchemeDetailByWorks($requestData['works_id']);
        if (empty($data)) {
            $this->setRenderCode(500);
            $this->setRenderMessage('方案不存在');
            return $this->getRenderJson();
        }
        $data['serial_array'] = json_decode($data['serial_array'], true);
        $this->setRenderCode(200);
        $this->addRenderData('scheme_name', $data['scheme_name'], false);
        $this->addRenderData('scheme_type', $data['scheme_type'], false);
        $this->addRenderData('scheme_s_type', $data['scheme_s_type'], false);
        $this->addRenderData('scheme_b_type', $data['scheme_b_type'], false);
        $this->addRenderData('serial_array', $data['serial_array'], false);
        return $this->getRenderJson();
    }
}