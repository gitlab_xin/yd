<?php
/**
 * Created by PhpStorm.
 * User: rudy
 * Date: 18-2-1
 * Time: 下午5:24
 */

namespace app\api\command;

use app\common\model\CustomizedShare;
use app\common\model\Qiniu;
use app\common\model\UserToken;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Log;

class CreateThree extends Command
{
    private $url = 'https://api.dankal.cn/snip/web';
    private $headers = ["Content-type: application/json;charset='utf-8'", "Accept: application/json"];
    private $web_url = 'http://three.inffur.com/?';
    private $data = [
        'delay' => 1,
        'viewport' => [
            'width' => 1200,
            'height' => 680
        ]
    ];
    private $three_param = "&test=1";

    protected function configure()
    {
        $this->setName('create_three')->setDescription('Here is the remark ');
        Log::init([
            'path' => RUNTIME_PATH . 'clilog' . DS,
        ]);
    }

    protected function execute(Input $input, Output $output)
    {
        $result = CustomizedShare::build()->field(['share_id', 'scheme_id', 'token'])
            ->where(['scheme_pic_three' => '', 'status' => 0])->select()->toArray();

        if (empty($result)) {
            $output->writeln('no task to do');
        } else {
            CustomizedShare::update(['status' => 1], ['share_id' => ['in', array_column($result, 'share_id')]]);
            foreach ($result as $row) {
                $img = $this->getImg($row['scheme_id'], $row['token']);
                if ($img != '') {
                    $Qiniu = new Qiniu();
                    $img = $Qiniu->uploadUrl($img, get_only() . "jpg");
                    CustomizedShare::update(['scheme_pic_three' => $img, 'status' => 2], ['share_id' => $row['share_id']]);
                    Log::record('share_id:' . $row['share_id'] . ':success');
                } else {
                    Log::record('share_id:' . $row['share_id'] . ':failed');
                }
            }
        }

    }

    private function getImg($scheme_id, $token)
    {
        $data['web_url'] = $this->web_url . 'type=show&screenshot=30&scheme_id=' . $scheme_id . '&token=' . $token . $this->three_param;
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $this->url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($handle, CURLOPT_TIMEOUT, 40);
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode(array_merge($data, $this->data)));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 2);

        $return = json_decode(curl_exec($handle), 1);
        curl_close($handle);

        return isset($return['img']) ? $return['img'] : '';
    }
}