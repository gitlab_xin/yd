<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 16:26
 * Description: 记录关于bbs的配置
 */

return [
    'classify'=>[
        'choiceness' => '商城精选',
        'strategy' => '易道攻略',
        'customization' => '定制精选'
    ],
];