<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 15:35
 * Description: 记录关于活动的配置
 */

return [
    'status'=>[
        0 => '提现审核中',
        1 => '提现申请成功',
        2 => '提现申请失败'
    ],
    'type' => [
        'withdraw_increase' => '提现申请未通过',
        'withdraw_reduce' => '提现申请',
        'money_reward' => '赏金收益'
    ]
];