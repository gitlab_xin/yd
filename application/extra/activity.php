<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/29
 * Time: 15:35
 * Description: 记录关于活动的配置
 */

return [
    'apply_status'=>[
        'yes' => '已通过',
        'no' => '未通过',
        'wait' => '待审核'
    ],
    'organize_type' => [
        'empty' => '无',
        'school' => '学校',
        'company' => '公司'
    ]
];