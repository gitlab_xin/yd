<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/10/19
 * Time: 20:24
 * Description: 记录
 */

return [
    'item_type' => [
        'KC' => '可挂于库抽',
        'YTG' => '可挂于衣通杆',
        'other' => '其它',
        'shoe' => '鞋子',
        'outside' => '外部',
        'book' => '书本',
        'fold' => '折叠',
        'tie' => '领带',
        'vase' => '花瓶摆设',
        'botany' => '植物',
        'baby' => '娃娃',
    ],
    'item_position' => [
        'top' => '柜体顶部',
        'left' => '柜体左边',
        'right' => '柜体右边',
        'front' => '柜体前方',
    ],
    'scheme_type' => [
        'RQS' => '入墙式移门壁柜',
        'QW' => '墙外独立移门衣柜',
        'BZ' => '平开门标准衣柜',
        'YM' => '移门定制',
        'ZH-CWSN' => '书柜及综合柜类',
        'ZH-SJSC' => '书柜+写字桌组合',
        'ZH-YYST' => '客厅电视桌组合',
        'DG' => '壁挂吊柜',
        'ZH-BGDG' => '壁挂吊柜'
    ],
    'cabinet_type' => [
        'YG' => '衣柜',
        'ZH' => '综合柜'
    ]
];