<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/11
 * Time: 9:56
 * Description: log mapping
 */

return [
    //可接受的action
    'allowable_action' => [
        'add' => ['POST' =>
            ['desc'=>'添加']
        ],
        'detail' => ['GET' => ['desc' => '查看详情',
            'params' => 'id']],
        'edit' => ['POST' => ['desc' => '编辑',
            'params' => 'id']],
        'del' => ['GET'=>['desc' => '删除',
            'params' => 'id']],
    ],
    'mapping'=>[
        //活动管理
        'activity' => [
            'end' => ['GET'=>['desc' => '提前结束活动',
                'params' => 'id']],
        ],
        //活动申请管理
        'activity_apply' => [
            'pass' => ['GET'=>['desc' => '通过申请',
                'params' => 'id']],
            'stop' => ['GET'=>['desc' => '拒绝申请',
                'params' => 'id']],
        ],
        //配送区域管理
        'config_area' => [
            'quickset' => ['GET'=>['desc' => '快速设置非配送',
                'params' => 'id']],
            'quickreset' => ['GET'=>['desc' => '快速设置非配送',
                'params' => 'id']],
        ],
        //帖子管理
        'forum_article' => [
            'delreply' => ['GET'=>['desc' => '删除回复',
                'params' => 'id']],
            'revert' => ['GET'=>['desc' => '恢复帖子',
                'params' => 'id']],
            'setessence' => ['POST'=>['desc' => '(取消)精华设置',
                'params' => 'id']],
            'settop' => ['POST'=>['desc' => '(取消)置顶设置',
                'params' => 'id']],
        ],
        //管理组模块
        'group' => [
            'editrule' => ['POST'=>['desc' => '编辑权限',
                'params' => 'id']],
        ],
        //登录模块
        'login' => [
            'logout' => ['GET'=>['desc' => '退出登录',
                'params' => 'id']],
            'login' => ['POST'=>['desc' => '登录',
                'params' => 'id']],
        ],
        //商品管理模块
        'shop_product' => [
            'changesale' => ['GET'=>['desc' => '上下架商品',
                'params' => 'id']],
        ],
        //订单管理模块
        'shop_order' => [
            'refund' => ['GET'=>['desc' => '退款',
                'params' => 'id']],
            'delivery' => ['POST'=>['desc' => '发货',
                'params' => 'order_id']],
            'address' => ['GET'=>['desc' => '查看收货地址',
                'params' => 'id']],
        ],
        //用户管理模块
        'user' => [
            'ban' => ['POST'=>['desc' => '黑名单操作',
                'params' => 'id']],
        ],
        //案例管理模块
        'works' => [
            'reject' => ['POST'=>['desc' => '拒绝审核',
                'params' => 'id']],
            'pass' => ['POST'=>['desc' => '通过审核',
                'params' => 'id']],
            'delreply' => ['GET'=>['desc' => '删除评论',
                'params' => 'id']],
            'setrecommended' => ['POST'=>['desc' => '(取消)推荐设置',
                'params' => 'id']],
        ],
        //提现管理模块
        'withdraw' => [
            'reject' => ['POST'=>['desc' => '拒绝审核',
                'params' => 'id']],
            'pass' => ['POST'=>['desc' => '通过审核',
                'params' => 'id']],
        ],
        //意见反馈模块
        'feedback' => [
            'setstatus' => ['GET'=>['desc' => '设置未读',
                'params' => 'id']]
        ],
        //官方物流管理模块
        'shop_order_logistics' => [
            'updatelogistics' => ['POST'=>['desc' => '更新物流信息',
                'params' => 'logistics_id']],
        ],
        //定制订单管理
        'customized_order' => [
            'refund' => ['GET'=>['desc' => '退款',
                'params' => 'id']],
            'delivery' => ['GET'=>['desc' => '发货',
                'params' => 'id']],
            'address' => ['GET'=>['desc' => '查看收货地址',
                'params' => 'id']],
            'scheme' => ['GET'=>['desc' => '查看方案详情',
                'params' => 'id']],
        ],
        'customized_cabinet_color' => [
            'quickset' => ['GET'=>['desc' => '快速添加所有颜色',
                'params' => 'id']]
        ],
        'customized_cabinet_door_color' => [
            'quickset' => ['GET'=>['desc' => '快速添加所有颜色',
                'params' => 'id']]
        ],
    ],
    //controller对应数组
    'controller_mapping' => [
        'activity' => '活动管理',
        'activity_apply' => '活动申请管理',
        'admin' => '管理员管理',
        'banner' => '轮播图管理',
        'config' => '推荐配置管理',
        'config_area' => '配送区域管理',
        'forum_article' => '论坛帖子管理',
        'group' => '管理组管理',
        'login' => '登录模块',
        'menu' => '菜单管理',
        'news' => '资讯管理',
        'shop_advertisement' => '分类广告管理',
        'shop_classify' => '商品分类管理',
        'shop_evaluate' => '商品评价管理',
        'shop_order' => '商品订单管理',
        'shop_popular' => '热门商品管理',
        'shop_product' => '商品管理',
        'shop_product_standard' => '商品规格管理',
        'shop_supplier' => '商家管理',
        'shop_supplier_bad_record' => '商家历史记录管理',
        'shop_supplier_params' => '商家默认参数管理',
        'tutorial' => '教程管理',
        'user' => '用户管理',
        'user_black_list' => '用户黑名单管理',
        'works' => '案例管理',
        'withdraw' => '提现管理',
        'help_center' => '帮助中心',
        'message' => '易道消息',
        'feedback' => '意见反馈',
        'shop_order_logistics' => '物流信息',
        'demand' => '需求定制',
        'demand_banner' => '需求定制轮播图',
        'customized_door_color' => '定制门颜色参数管理',
        'customized_floor_color' => '定制地板参数管理',
        'customized_wall_color' => '定制墙壁参数管理',
        'customized_item_classify' => '物品分类管理',
        'customized_item' => '物品管理',
        'customized_other_charge' => '费用管理',
        'customized_order' => '定制订单管理',
        'customized_evaluate' => '方案评价管理',
        'cooperative_partner' => '合作伙伴管理',
        'customized_component_combine' => '组件管理',
        'customized_component_module' => '模块管理',
        'customized_color' => '颜色管理',
        'customized_cabinet' => '柜体管理',
        'customized_cabinet_color' => '柜体颜色管理',
        'customized_cabinet_door_color' => '柜体门颜色管理',
        'webgl' => '三维模型管理',
        'customized_cabinet_environment' => '三维环境管理',
        'customized_cabinet_texture' => '三维材质管理',
        'customized_cabinet_model' => '柜体三维模型管理',
    ]
];