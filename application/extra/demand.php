<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/8/26
 * Time: 17:09
 * Description:
 */

return [
    'status' => [
        0 => '未支付', 1 => '发布完成', 2 => '已关闭', 3 => '已完成'
    ],
    //用户关闭需求限制
    'cancel_limit' => [
        'count' => 200000,//规定时间内最多可以关闭多少个需求
        'time' => 60 * 60, //一个小时
        'percent' => 100,//退款百分比(请填写1到100之间的数)
        'auto_cancel' => 60 * 60 * 24 * 7//自动关闭需求时间
    ],
    //关闭需求原因
    'close_reason' => [
        ['reason' => 'change_mind', 'content' => '计划有变，不想发了'],
        ['reason' => 'mistake', 'content' => '需求填写有误'],
        ['reason' => 'unsuitable', 'content' => '没有合适方案'],
        ['reason' => 'others', 'content' => '其他原因'],
    ],
    //最大投稿数
    'contribute_limit' => 3,
    'daily_max_limit' => 999
];