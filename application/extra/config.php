<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/25
 * Time: 16:26
 * Description: 记录关于bbs的配置
 */

return [
    'key' => [
        'tutorial_recommend' => '教程推荐',
        'store_recommend' => '商城推荐',
        'customeized_discount' => '定制间折扣',
        'three_package_version' => '三维插件更新'
    ],
    'input_type' => [
        'img_src' => 'img',
        'video_src' => 'video',
        'product_id' => 'text',
        'discount' => 'text',
        'force' => 'text',
        'download_url' => 'text',
        'version_code' => '版本号'
    ],
    'input_name' => [
        'img_src' => '图片',
        'video_src' => '视频',
        'product_id' => '商品id',
        'discount' => '折扣',
        'force' => "强制更新",
        'version_code' => '版本号',
        'download_url' => '下载地址'
    ],
    'shop_anonymous' =>[
        'username' => '匿名用户',
        'avatar' => '808CF5458B9DB82F94BA2EEBDC9554AE.png'
    ]
];