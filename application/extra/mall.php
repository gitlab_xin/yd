<?php
/**
 * Created by PhpStorm.
 * User: Rudy
 * Date: 2017/7/14
 * Time: 15:26
 * Description: 记录关于商城的配置信息,如轮播图的ENUM
 */

return [
    'user'=>[
        'status' => [
            0 => '已删除',
            1 => '正常',
            2 => '封号中'
        ]
    ],
    'banner' => [//轮播图
        'module_type' => [//轮播图类型
            'store' => '商城',
            'home' => '首页',
            'forum' => '论坛',
            'case' => '案例',
            'activity' => '活动',
            'demand' => '定制需求',
            'customroom' => '定制间',
        ],
        'device_type' => [//轮播图适用终端
            'app' => '移动端',
            'web' => 'PC端'
        ],
        'max_pic' => 5//最多有多少张轮播图
    ],
    'order' => [
        'pay_type' => [
            'alipay' => '支付宝支付',
            'wechat' => '微信支付'
        ],
        'status' => ['已取消','已完成','待评价','待收货','待发货','未支付','退款中','已退款'],
        'deadline' => 60 * 15,//未付款自动取消订单  15分钟
        'auto_confirm' => 60 * 60 * 24 * 14//自动确认收货 14天 60 * 60 * 24 * 14
    ],
    'logistics'=>[
        'type' => ['official' => '官方物流',
            'SF'=>'顺丰快递','HTKY'=>'百世快递','ZTO'=>'中通快递','STO'=>'申通快递',
            'YTO'=>'圆通快递','YD'=>'韵达快递','YZPY'=>'邮政平邮','EMS'=>'EMS',
            'HHTT'=>'天天快递','JD'=>'京东快递','QFKD'=>'全峰快递','GTO'=>'国通快递',
            'UC'=>'优速快递','DBL'=>'德邦快递','FAST'=>'快捷快递','AMAZON'=>'亚马逊',
            'ZJS'=>'宅急送','others'=>'其它物流'],
        'status'=> ['已发货','派送中','已签收']
    ],
    'works' => [
        'type' => [
            0 => '未审核',
            1 => '已通过',
            2 => '已拒绝'
        ]
    ],
    'product' => [
        'max_standard' => 8
    ]
];