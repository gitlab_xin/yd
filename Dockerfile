FROM registry.cn-shenzhen.aliyuncs.com/zhaoxin_space/nginx-php:7
#
ENV VIRTUAL_HOST yd.api.birdnight.cn

COPY . /app
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

RUN chmod -R 777 /app/runtime
RUN chmod -R 777 /app/extend

